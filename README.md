# openbmc_sdk
ref : https://github.com/openbmc/docs/blob/master/development/dev-environment.md


wget https://jenkins.openbmc.org/job/latest-master-sdk/label=docker-builder,target=romulus/lastSuccessfulBuild/artifact/deploy/sdk/oecore-x86_64-arm1176jzs-toolchain-nodistro.0.sh

chmod u+x oecore-x86_64-arm1176jzs-toolchain-nodistro.0.sh

mkdir -p ./romulus2600-`date +%F`

pwd   ==>   (will set to oecore-x86_64-arm1176jzs-toolchain-nodistro.0.sh )

/workspace/openbmc_sdk/romulus2600-2022-10-14

## install openbmc sdk into /workspace/openbmc_sdk/romulus2600-2022-10-14

./oecore-x86_64-arm1176jzs-toolchain-nodistro.0.sh

chmod u+x /workspace/openbmc_sdk/romulus2600-2022-10-14/environment-setup-arm1176jzs-openbmc-linux-gnueabi

### set environment , must have . /....../environment-xxx
. /workspace/openbmc_sdk/romulus2600-2022-10-14/environment-setup-arm1176jzs-openbmc-linux-gnueabi

echo $OECORE_TARGET_ARCH   ==> will show "arm"

## hello.c -- way 1
ref : https://kevinleeblog.github.io/project1/2019/11/05/openbmc-study-two/

## write hello.c code 

## use " $CC hello.c -o my_hello " to compile 

## verify

./romulus2600-2022-10-14/sysroots/x86_64-oesdk-linux/usr/bin/qemu-arm ./romulus2600-2022-10-14/sysroots/arm1176jzs-openbmc-linux-gnueabi/lib/ld-linux.so.3 --library-path ./romulus2600-2022-10-14/sysroots/arm1176jzs-openbmc-linux-gnueabi/lib ./my_hello

##  use hello-2.1.1 , way 2

wget https://ftp.gnu.org/gnu/hello/hello-2.10.tar.gz

tar -xf hello-2.10.tar.gz

cd hello-2.10

./configure --host=arm-openbmc-linux-gnueabi

make 

## verify 
./romulus2600-2022-10-14/sysroots/x86_64-oesdk-linux/usr/bin/qemu-arm ./romulus2600-2022-10-14/sysroots/arm1176jzs-openbmc-linux-gnueabi/lib/ld-linux.so.3 --library-path ./romulus2600-2022-10-14/sysroots/arm1176jzs-openbmc-linux-gnueabi/lib ./hello 

## hello  , way 3 : use meson to build hello meson example in meson_c_hello folder 

../romulus2600-2022-10-14/sysroots/x86_64-oesdk-linux/usr/bin/meson build

../romulus2600-2022-10-14/sysroots/x86_64-oesdk-linux/usr/bin/ninja -C build


## download qemu and romulus image

wget https://jenkins.openbmc.org/job/latest-qemu-x86/lastSuccessfulBuild/artifact/qemu/build/qemu-system-arm

chmod u+x qemu-system-arm

wget https://jenkins.openbmc.org/job/latest-master/label=docker-builder,target=romulus/lastSuccessfulBuild/artifact/openbmc/build/tmp/deploy/images/romulus/obmc-phosphor-image-romulus.static.mtd

./qemu-system-arm -m 256 -M romulus-bmc -nographic -drive file=./obmc-phosphor-image-romulus.static.mtd,format=raw,if=mtd -net nic -net user,hostfwd=:127.0.0.1:2222-:22,hostfwd=:127.0.0.1:2443-:443,hostfwd=udp:127.0.0.1:2623-:623,hostname=qemu

./qemu-system-arm -m 256 -machine romulus-bmc -nographic -drive file=./obmc-phosphor-image-romulus.static.mtd,format=raw,if=mtd -net nic -net user,hostfwd=:127.0.0.1:22-:22,hostfwd=:127.0.0.1:443-:443,hostfwd=tcp:127.0.0.1:80-:80,hostfwd=tcp:127.0.0.1:2200-:2200,hostfwd=udp:127.0.0.1:623-:623,hostfwd=udp:127.0.0.1:664-:664,hostname=qemu

## in romulus image qemu to verify different hello examples 

## hello.c 
ref : https://kevinleeblog.github.io/project1/2019/11/05/openbmc-study-two/

##   " $CC hello.c -o my_hello "

write hello.c code or 

##  use hello-2.1.1 


##  start qemu with romulus image

scp -P 2222 my_hello root@127.0.0.1:/tmp

## in romulus qemu 

/tmp/my_hello

## for hello-2.10 and meson hello use the same way 
 
