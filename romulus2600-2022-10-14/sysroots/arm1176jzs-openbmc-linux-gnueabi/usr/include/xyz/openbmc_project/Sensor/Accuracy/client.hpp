#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace client
{
namespace Accuracy
{

static constexpr auto interface = "xyz.openbmc_project.Sensor.Accuracy";

} // namespace Accuracy
} // namespace client
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

