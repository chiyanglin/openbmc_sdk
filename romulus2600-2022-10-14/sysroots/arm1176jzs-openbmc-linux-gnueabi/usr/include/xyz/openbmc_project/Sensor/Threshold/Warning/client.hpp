#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace client
{
namespace Warning
{

static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.Warning";

} // namespace Warning
} // namespace client
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

