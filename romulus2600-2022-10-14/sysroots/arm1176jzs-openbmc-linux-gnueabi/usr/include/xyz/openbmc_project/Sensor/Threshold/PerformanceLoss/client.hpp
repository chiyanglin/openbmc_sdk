#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace client
{
namespace PerformanceLoss
{

static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.PerformanceLoss";

} // namespace PerformanceLoss
} // namespace client
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

