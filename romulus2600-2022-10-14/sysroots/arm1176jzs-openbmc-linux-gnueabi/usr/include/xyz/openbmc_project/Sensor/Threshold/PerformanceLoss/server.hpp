#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

class PerformanceLoss
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PerformanceLoss() = delete;
        PerformanceLoss(const PerformanceLoss&) = delete;
        PerformanceLoss& operator=(const PerformanceLoss&) = delete;
        PerformanceLoss(PerformanceLoss&&) = delete;
        PerformanceLoss& operator=(PerformanceLoss&&) = delete;
        virtual ~PerformanceLoss() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PerformanceLoss(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PerformanceLoss(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** @brief Send signal 'PerformanceLossHighAlarmAsserted'
         *
         *  The high threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void performanceLossHighAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'PerformanceLossHighAlarmDeasserted'
         *
         *  The high threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void performanceLossHighAlarmDeasserted(
            double sensorValue);

        /** @brief Send signal 'PerformanceLossLowAlarmAsserted'
         *
         *  The low threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void performanceLossLowAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'PerformanceLossLowAlarmDeasserted'
         *
         *  The low threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void performanceLossLowAlarmDeasserted(
            double sensorValue);

        /** Get value of PerformanceLossHigh */
        virtual double performanceLossHigh() const;
        /** Set value of PerformanceLossHigh with option to skip sending signal */
        virtual double performanceLossHigh(double value,
               bool skipSignal);
        /** Set value of PerformanceLossHigh */
        virtual double performanceLossHigh(double value);
        /** Get value of PerformanceLossLow */
        virtual double performanceLossLow() const;
        /** Set value of PerformanceLossLow with option to skip sending signal */
        virtual double performanceLossLow(double value,
               bool skipSignal);
        /** Set value of PerformanceLossLow */
        virtual double performanceLossLow(double value);
        /** Get value of PerformanceLossAlarmHigh */
        virtual bool performanceLossAlarmHigh() const;
        /** Set value of PerformanceLossAlarmHigh with option to skip sending signal */
        virtual bool performanceLossAlarmHigh(bool value,
               bool skipSignal);
        /** Set value of PerformanceLossAlarmHigh */
        virtual bool performanceLossAlarmHigh(bool value);
        /** Get value of PerformanceLossAlarmLow */
        virtual bool performanceLossAlarmLow() const;
        /** Set value of PerformanceLossAlarmLow with option to skip sending signal */
        virtual bool performanceLossAlarmLow(bool value,
               bool skipSignal);
        /** Set value of PerformanceLossAlarmLow */
        virtual bool performanceLossAlarmLow(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.PerformanceLoss";

    private:

        /** @brief sd-bus callback for get-property 'PerformanceLossHigh' */
        static int _callback_get_PerformanceLossHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PerformanceLossHigh' */
        static int _callback_set_PerformanceLossHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PerformanceLossLow' */
        static int _callback_get_PerformanceLossLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PerformanceLossLow' */
        static int _callback_set_PerformanceLossLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PerformanceLossAlarmHigh' */
        static int _callback_get_PerformanceLossAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PerformanceLossAlarmHigh' */
        static int _callback_set_PerformanceLossAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PerformanceLossAlarmLow' */
        static int _callback_get_PerformanceLossAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PerformanceLossAlarmLow' */
        static int _callback_set_PerformanceLossAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface;
        sdbusplus::SdBusInterface *_intf;

        double _performanceLossHigh = std::numeric_limits<double>::quiet_NaN();
        double _performanceLossLow = std::numeric_limits<double>::quiet_NaN();
        bool _performanceLossAlarmHigh{};
        bool _performanceLossAlarmLow{};

};


} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

