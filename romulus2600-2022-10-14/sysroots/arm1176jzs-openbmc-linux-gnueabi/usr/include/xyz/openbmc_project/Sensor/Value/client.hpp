#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace client
{
namespace Value
{

static constexpr auto interface = "xyz.openbmc_project.Sensor.Value";

} // namespace Value
} // namespace client
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

