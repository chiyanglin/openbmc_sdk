#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace client
{
namespace ValueMutability
{

static constexpr auto interface = "xyz.openbmc_project.Sensor.ValueMutability";

} // namespace ValueMutability
} // namespace client
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

