#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

class HardShutdown
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        HardShutdown() = delete;
        HardShutdown(const HardShutdown&) = delete;
        HardShutdown& operator=(const HardShutdown&) = delete;
        HardShutdown(HardShutdown&&) = delete;
        HardShutdown& operator=(HardShutdown&&) = delete;
        virtual ~HardShutdown() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        HardShutdown(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        HardShutdown(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** @brief Send signal 'HardShutdownHighAlarmAsserted'
         *
         *  The high threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void hardShutdownHighAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'HardShutdownHighAlarmDeasserted'
         *
         *  The high threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void hardShutdownHighAlarmDeasserted(
            double sensorValue);

        /** @brief Send signal 'HardShutdownLowAlarmAsserted'
         *
         *  The low threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void hardShutdownLowAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'HardShutdownLowAlarmDeasserted'
         *
         *  The low threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void hardShutdownLowAlarmDeasserted(
            double sensorValue);

        /** Get value of HardShutdownHigh */
        virtual double hardShutdownHigh() const;
        /** Set value of HardShutdownHigh with option to skip sending signal */
        virtual double hardShutdownHigh(double value,
               bool skipSignal);
        /** Set value of HardShutdownHigh */
        virtual double hardShutdownHigh(double value);
        /** Get value of HardShutdownLow */
        virtual double hardShutdownLow() const;
        /** Set value of HardShutdownLow with option to skip sending signal */
        virtual double hardShutdownLow(double value,
               bool skipSignal);
        /** Set value of HardShutdownLow */
        virtual double hardShutdownLow(double value);
        /** Get value of HardShutdownAlarmHigh */
        virtual bool hardShutdownAlarmHigh() const;
        /** Set value of HardShutdownAlarmHigh with option to skip sending signal */
        virtual bool hardShutdownAlarmHigh(bool value,
               bool skipSignal);
        /** Set value of HardShutdownAlarmHigh */
        virtual bool hardShutdownAlarmHigh(bool value);
        /** Get value of HardShutdownAlarmLow */
        virtual bool hardShutdownAlarmLow() const;
        /** Set value of HardShutdownAlarmLow with option to skip sending signal */
        virtual bool hardShutdownAlarmLow(bool value,
               bool skipSignal);
        /** Set value of HardShutdownAlarmLow */
        virtual bool hardShutdownAlarmLow(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.HardShutdown";

    private:

        /** @brief sd-bus callback for get-property 'HardShutdownHigh' */
        static int _callback_get_HardShutdownHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HardShutdownHigh' */
        static int _callback_set_HardShutdownHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HardShutdownLow' */
        static int _callback_get_HardShutdownLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HardShutdownLow' */
        static int _callback_set_HardShutdownLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HardShutdownAlarmHigh' */
        static int _callback_get_HardShutdownAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HardShutdownAlarmHigh' */
        static int _callback_set_HardShutdownAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HardShutdownAlarmLow' */
        static int _callback_get_HardShutdownAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HardShutdownAlarmLow' */
        static int _callback_set_HardShutdownAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface;
        sdbusplus::SdBusInterface *_intf;

        double _hardShutdownHigh = std::numeric_limits<double>::quiet_NaN();
        double _hardShutdownLow = std::numeric_limits<double>::quiet_NaN();
        bool _hardShutdownAlarmHigh{};
        bool _hardShutdownAlarmLow{};

};


} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

