#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace server
{

class Value
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Value() = delete;
        Value(const Value&) = delete;
        Value& operator=(const Value&) = delete;
        Value(Value&&) = delete;
        Value& operator=(Value&&) = delete;
        virtual ~Value() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Value(bus_t& bus, const char* path);

        enum class Unit
        {
            Amperes,
            CFM,
            DegreesC,
            Joules,
            Meters,
            Percent,
            PercentRH,
            Pascals,
            RPMS,
            Volts,
            Watts,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Unit,
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Value(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Value */
        virtual double value() const;
        /** Set value of Value with option to skip sending signal */
        virtual double value(double value,
               bool skipSignal);
        /** Set value of Value */
        virtual double value(double value);
        /** Get value of MaxValue */
        virtual double maxValue() const;
        /** Set value of MaxValue with option to skip sending signal */
        virtual double maxValue(double value,
               bool skipSignal);
        /** Set value of MaxValue */
        virtual double maxValue(double value);
        /** Get value of MinValue */
        virtual double minValue() const;
        /** Set value of MinValue with option to skip sending signal */
        virtual double minValue(double value,
               bool skipSignal);
        /** Set value of MinValue */
        virtual double minValue(double value);
        /** Get value of Unit */
        virtual Unit unit() const;
        /** Set value of Unit with option to skip sending signal */
        virtual Unit unit(Unit value,
               bool skipSignal);
        /** Set value of Unit */
        virtual Unit unit(Unit value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Sensor.Value.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Unit convertUnitFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Sensor.Value.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Unit> convertStringToUnit(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Sensor.Value.<value name>"
         */
        static std::string convertUnitToString(Unit e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Sensor_Value_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Sensor_Value_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Sensor.Value";

    private:

        /** @brief sd-bus callback for get-property 'Value' */
        static int _callback_get_Value(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Value' */
        static int _callback_set_Value(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxValue' */
        static int _callback_get_MaxValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxValue' */
        static int _callback_set_MaxValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MinValue' */
        static int _callback_get_MinValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MinValue' */
        static int _callback_set_MinValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Unit' */
        static int _callback_get_Unit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Unit' */
        static int _callback_set_Unit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Sensor_Value_interface;
        sdbusplus::SdBusInterface *_intf;

        double _value{};
        double _maxValue = std::numeric_limits<double>::infinity();
        double _minValue = -std::numeric_limits<double>::infinity();
        Unit _unit{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Value::Unit.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Value::Unit e)
{
    return Value::convertUnitToString(e);
}

} // namespace server
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Sensor::server::Value::Unit>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Sensor::server::Value::convertStringToUnit(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Sensor::server::Value::Unit>
{
    static std::string op(xyz::openbmc_project::Sensor::server::Value::Unit value)
    {
        return xyz::openbmc_project::Sensor::server::Value::convertUnitToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

