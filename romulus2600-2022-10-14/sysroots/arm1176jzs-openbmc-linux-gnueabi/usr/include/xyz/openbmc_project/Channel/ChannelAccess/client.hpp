#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Channel
{
namespace client
{
namespace ChannelAccess
{

static constexpr auto interface = "xyz.openbmc_project.Channel.ChannelAccess";

} // namespace ChannelAccess
} // namespace client
} // namespace Channel
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

