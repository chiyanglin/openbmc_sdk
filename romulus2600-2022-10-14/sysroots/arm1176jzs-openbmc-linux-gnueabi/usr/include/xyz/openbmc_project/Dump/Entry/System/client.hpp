#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Entry
{
namespace client
{
namespace System
{

static constexpr auto interface = "xyz.openbmc_project.Dump.Entry.System";

} // namespace System
} // namespace client
} // namespace Entry
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

