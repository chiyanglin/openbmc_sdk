#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace client
{
namespace NewDump
{

static constexpr auto interface = "xyz.openbmc_project.Dump.NewDump";

} // namespace NewDump
} // namespace client
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

