#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.Dump.Create";

} // namespace Create
} // namespace client
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

