#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace server
{

class NewDump
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        NewDump() = delete;
        NewDump(const NewDump&) = delete;
        NewDump& operator=(const NewDump&) = delete;
        NewDump(NewDump&&) = delete;
        NewDump& operator=(NewDump&&) = delete;
        virtual ~NewDump() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        NewDump(bus_t& bus, const char* path);



        /** @brief Implementation for Notify
         *  Create a dump entry based on the parameters.
         *
         *  @param[in] sourceDumpId - The dump id provided by the source of the dump. There are dumps which get generated outside the BMC, like a system dump which gets generated and stored in the host memory. All dumps will have a  unique id  but when communicating to the source of the dump the SourceDumpId will be used.
         *  @param[in] size - Size of the dump in bytes
         */
        virtual void notify(
            uint32_t sourceDumpId,
            uint64_t size) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Dump_NewDump_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Dump_NewDump_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Dump.NewDump";

    private:

        /** @brief sd-bus callback for Notify
         */
        static int _callback_Notify(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Dump_NewDump_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

