#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Internal
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);

        enum class Type
        {
            ApplicationCored,
            UserRequested,
            InternalFailure,
            Checkstop,
            Ramoops,
        };


        /** @brief Implementation for Create
         *  Create BMC Dump based on the Dump type.
         *
         *  @param[in] type - Type of the Dump.
         *  @param[in] fullPaths - A list of paths (file paths or d-bus object paths) that must be processed to derive the dump content.
         */
        virtual void create(
            Type type,
            std::vector<std::string> fullPaths) = 0;



        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Dump.Internal.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Type convertTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Dump.Internal.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Type> convertStringToType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Dump.Internal.Create.<value name>"
         */
        static std::string convertTypeToString(Type e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Dump_Internal_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Dump_Internal_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Dump.Internal.Create";

    private:

        /** @brief sd-bus callback for Create
         */
        static int _callback_Create(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Dump_Internal_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::Type.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::Type e)
{
    return Create::convertTypeToString(e);
}

} // namespace server
} // namespace Internal
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Dump::Internal::server::Create::Type>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Dump::Internal::server::Create::convertStringToType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Dump::Internal::server::Create::Type>
{
    static std::string op(xyz::openbmc_project::Dump::Internal::server::Create::Type value)
    {
        return xyz::openbmc_project::Dump::Internal::server::Create::convertTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

