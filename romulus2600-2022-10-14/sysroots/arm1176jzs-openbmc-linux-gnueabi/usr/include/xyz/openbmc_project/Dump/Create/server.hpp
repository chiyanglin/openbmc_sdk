#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);

        enum class CreateParameters
        {
            OriginatorId,
            OriginatorType,
        };


        /** @brief Implementation for CreateDump
         *  Method to create a manual Dump.
         *
         *  @param[in] additionalData - The additional data, if any, for initiating the dump. The key in this case should be an implementation specific enum defined for the specific type of dump either in xyz or in a domain. The values can be either a string or a 64 bit number. The enum-format string is required to come from a parallel class with this specific Enum name. All of the Enum strings should be in the format 'domain.Dump.Create.CreateParameters.ParamName'. e.g.:
  {
    "key1": "value1",
    "key2": "value2"
  }
ends up in AdditionaData like:
  ["KEY1=value1", "KEY2=value2"]
         *
         *  @return path[sdbusplus::message::object_path] - The object path of the object that implements xyz.openbmc_project.Common.Progress to track the progress of the dump
         */
        virtual sdbusplus::message::object_path createDump(
            std::map<std::string, std::variant<std::string, uint64_t>> additionalData) = 0;



        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Dump.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static CreateParameters convertCreateParametersFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Dump.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<CreateParameters> convertStringToCreateParameters(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Dump.Create.<value name>"
         */
        static std::string convertCreateParametersToString(CreateParameters e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Dump_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Dump_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Dump.Create";

    private:

        /** @brief sd-bus callback for CreateDump
         */
        static int _callback_CreateDump(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Dump_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::CreateParameters.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::CreateParameters e)
{
    return Create::convertCreateParametersToString(e);
}

} // namespace server
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Dump::server::Create::CreateParameters>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Dump::server::Create::convertStringToCreateParameters(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Dump::server::Create::CreateParameters>
{
    static std::string op(xyz::openbmc_project::Dump::server::Create::CreateParameters value)
    {
        return xyz::openbmc_project::Dump::server::Create::convertCreateParametersToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

