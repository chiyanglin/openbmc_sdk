#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace server
{

class Entry
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Entry() = delete;
        Entry(const Entry&) = delete;
        Entry& operator=(const Entry&) = delete;
        Entry(Entry&&) = delete;
        Entry& operator=(Entry&&) = delete;
        virtual ~Entry() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Entry(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Entry(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for InitiateOffload
         *  This method initiates the offload of the dump from the dump storage. The transfer will be initiated to the target provide. The caller of this function should set up a method to transfer and pass the URI to to this function. If there is no exceptions the caller can assume the transfer is successfully initiated. Once the transfer is completed. The application which is transferring the dump should set offloaded property to true to indicate offload is completed.
         *
         *  @param[in] offloadUri - The location to offload dump file, error InvalidArgument will be returned if the URI is not well formated.
         */
        virtual void initiateOffload(
            std::string offloadUri) = 0;


        /** Get value of Size */
        virtual uint64_t size() const;
        /** Set value of Size with option to skip sending signal */
        virtual uint64_t size(uint64_t value,
               bool skipSignal);
        /** Set value of Size */
        virtual uint64_t size(uint64_t value);
        /** Get value of Offloaded */
        virtual bool offloaded() const;
        /** Set value of Offloaded with option to skip sending signal */
        virtual bool offloaded(bool value,
               bool skipSignal);
        /** Set value of Offloaded */
        virtual bool offloaded(bool value);
        /** Get value of OffloadUri */
        virtual std::string offloadUri() const;
        /** Set value of OffloadUri with option to skip sending signal */
        virtual std::string offloadUri(std::string value,
               bool skipSignal);
        /** Set value of OffloadUri */
        virtual std::string offloadUri(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Dump_Entry_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Dump_Entry_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Dump.Entry";

    private:

        /** @brief sd-bus callback for InitiateOffload
         */
        static int _callback_InitiateOffload(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Size' */
        static int _callback_get_Size(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Size' */
        static int _callback_set_Size(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Offloaded' */
        static int _callback_get_Offloaded(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Offloaded' */
        static int _callback_set_Offloaded(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'OffloadUri' */
        static int _callback_get_OffloadUri(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'OffloadUri' */
        static int _callback_set_OffloadUri(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Dump_Entry_interface;
        sdbusplus::SdBusInterface *_intf;

        uint64_t _size{};
        bool _offloaded{};
        std::string _offloadUri{};

};


} // namespace server
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

