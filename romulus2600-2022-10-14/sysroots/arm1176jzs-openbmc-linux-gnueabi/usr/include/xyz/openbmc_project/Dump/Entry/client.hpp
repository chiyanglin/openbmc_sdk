#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace client
{
namespace Entry
{

static constexpr auto interface = "xyz.openbmc_project.Dump.Entry";

} // namespace Entry
} // namespace client
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

