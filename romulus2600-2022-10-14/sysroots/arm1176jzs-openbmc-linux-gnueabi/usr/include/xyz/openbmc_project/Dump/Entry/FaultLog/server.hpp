#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Entry
{
namespace server
{

class FaultLog
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        FaultLog() = delete;
        FaultLog(const FaultLog&) = delete;
        FaultLog& operator=(const FaultLog&) = delete;
        FaultLog(FaultLog&&) = delete;
        FaultLog& operator=(FaultLog&&) = delete;
        virtual ~FaultLog() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        FaultLog(bus_t& bus, const char* path);

        enum class FaultDataType
        {
            CPER,
            Crashdump,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                FaultDataType,
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        FaultLog(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Type */
        virtual FaultDataType type() const;
        /** Set value of Type with option to skip sending signal */
        virtual FaultDataType type(FaultDataType value,
               bool skipSignal);
        /** Set value of Type */
        virtual FaultDataType type(FaultDataType value);
        /** Get value of AdditionalTypeName */
        virtual std::string additionalTypeName() const;
        /** Set value of AdditionalTypeName with option to skip sending signal */
        virtual std::string additionalTypeName(std::string value,
               bool skipSignal);
        /** Set value of AdditionalTypeName */
        virtual std::string additionalTypeName(std::string value);
        /** Get value of PrimaryLogId */
        virtual std::string primaryLogId() const;
        /** Set value of PrimaryLogId with option to skip sending signal */
        virtual std::string primaryLogId(std::string value,
               bool skipSignal);
        /** Set value of PrimaryLogId */
        virtual std::string primaryLogId(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Dump.Entry.FaultLog.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static FaultDataType convertFaultDataTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Dump.Entry.FaultLog.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<FaultDataType> convertStringToFaultDataType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Dump.Entry.FaultLog.<value name>"
         */
        static std::string convertFaultDataTypeToString(FaultDataType e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Dump_Entry_FaultLog_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Dump_Entry_FaultLog_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Dump.Entry.FaultLog";

    private:

        /** @brief sd-bus callback for get-property 'Type' */
        static int _callback_get_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AdditionalTypeName' */
        static int _callback_get_AdditionalTypeName(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PrimaryLogId' */
        static int _callback_get_PrimaryLogId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Dump_Entry_FaultLog_interface;
        sdbusplus::SdBusInterface *_intf;

        FaultDataType _type{};
        std::string _additionalTypeName{};
        std::string _primaryLogId{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type FaultLog::FaultDataType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(FaultLog::FaultDataType e)
{
    return FaultLog::convertFaultDataTypeToString(e);
}

} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Dump::Entry::server::FaultLog::FaultDataType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Dump::Entry::server::FaultLog::convertStringToFaultDataType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Dump::Entry::server::FaultLog::FaultDataType>
{
    static std::string op(xyz::openbmc_project::Dump::Entry::server::FaultLog::FaultDataType value)
    {
        return xyz::openbmc_project::Dump::Entry::server::FaultLog::convertFaultDataTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

