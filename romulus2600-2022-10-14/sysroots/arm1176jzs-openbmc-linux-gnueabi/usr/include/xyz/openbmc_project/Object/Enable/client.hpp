#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Object
{
namespace client
{
namespace Enable
{

static constexpr auto interface = "xyz.openbmc_project.Object.Enable";

} // namespace Enable
} // namespace client
} // namespace Object
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

