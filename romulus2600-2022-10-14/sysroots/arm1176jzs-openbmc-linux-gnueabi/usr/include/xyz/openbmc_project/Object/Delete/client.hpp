#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Object
{
namespace client
{
namespace Delete
{

static constexpr auto interface = "xyz.openbmc_project.Object.Delete";

} // namespace Delete
} // namespace client
} // namespace Object
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

