#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace ObjectPath
{

static constexpr auto interface = "xyz.openbmc_project.Common.ObjectPath";

} // namespace ObjectPath
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

