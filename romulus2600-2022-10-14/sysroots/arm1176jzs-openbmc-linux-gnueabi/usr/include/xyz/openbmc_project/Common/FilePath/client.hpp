#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace FilePath
{

static constexpr auto interface = "xyz.openbmc_project.Common.FilePath";

} // namespace FilePath
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

