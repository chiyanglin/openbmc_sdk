#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace TFTP
{

static constexpr auto interface = "xyz.openbmc_project.Common.TFTP";

} // namespace TFTP
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

