#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace FactoryReset
{

static constexpr auto interface = "xyz.openbmc_project.Common.FactoryReset";

} // namespace FactoryReset
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

