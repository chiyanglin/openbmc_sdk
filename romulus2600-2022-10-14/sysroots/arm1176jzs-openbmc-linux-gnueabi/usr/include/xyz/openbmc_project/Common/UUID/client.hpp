#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace UUID
{

static constexpr auto interface = "xyz.openbmc_project.Common.UUID";

} // namespace UUID
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

