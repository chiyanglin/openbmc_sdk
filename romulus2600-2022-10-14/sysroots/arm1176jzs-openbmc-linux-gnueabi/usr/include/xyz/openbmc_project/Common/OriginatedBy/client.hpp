#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace OriginatedBy
{

static constexpr auto interface = "xyz.openbmc_project.Common.OriginatedBy";

} // namespace OriginatedBy
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

