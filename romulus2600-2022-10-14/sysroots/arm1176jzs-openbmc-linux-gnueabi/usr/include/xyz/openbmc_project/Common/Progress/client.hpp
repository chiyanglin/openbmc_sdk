#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace client
{
namespace Progress
{

static constexpr auto interface = "xyz.openbmc_project.Common.Progress";

} // namespace Progress
} // namespace client
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

