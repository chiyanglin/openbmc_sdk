#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Led
{
namespace client
{
namespace Group
{

static constexpr auto interface = "xyz.openbmc_project.Led.Group";

} // namespace Group
} // namespace client
} // namespace Led
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

