#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Led
{
namespace client
{
namespace Physical
{

static constexpr auto interface = "xyz.openbmc_project.Led.Physical";

} // namespace Physical
} // namespace client
} // namespace Led
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

