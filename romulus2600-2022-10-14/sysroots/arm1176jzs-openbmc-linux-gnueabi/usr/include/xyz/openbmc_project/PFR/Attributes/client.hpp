#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PFR
{
namespace client
{
namespace Attributes
{

static constexpr auto interface = "xyz.openbmc_project.PFR.Attributes";

} // namespace Attributes
} // namespace client
} // namespace PFR
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

