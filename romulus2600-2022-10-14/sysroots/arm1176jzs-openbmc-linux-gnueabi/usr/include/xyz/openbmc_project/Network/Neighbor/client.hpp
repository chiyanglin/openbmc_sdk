#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace client
{
namespace Neighbor
{

static constexpr auto interface = "xyz.openbmc_project.Network.Neighbor";

} // namespace Neighbor
} // namespace client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

