#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

class Neighbor
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Neighbor() = delete;
        Neighbor(const Neighbor&) = delete;
        Neighbor& operator=(const Neighbor&) = delete;
        Neighbor(Neighbor&&) = delete;
        Neighbor& operator=(Neighbor&&) = delete;
        virtual ~Neighbor() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Neighbor(bus_t& bus, const char* path);

        enum class State
        {
            Incomplete,
            Reachable,
            Stale,
            Invalid,
            Permanent,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                State,
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Neighbor(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of IPAddress */
        virtual std::string ipAddress() const;
        /** Set value of IPAddress with option to skip sending signal */
        virtual std::string ipAddress(std::string value,
               bool skipSignal);
        /** Set value of IPAddress */
        virtual std::string ipAddress(std::string value);
        /** Get value of MACAddress */
        virtual std::string macAddress() const;
        /** Set value of MACAddress with option to skip sending signal */
        virtual std::string macAddress(std::string value,
               bool skipSignal);
        /** Set value of MACAddress */
        virtual std::string macAddress(std::string value);
        /** Get value of State */
        virtual State state() const;
        /** Set value of State with option to skip sending signal */
        virtual State state(State value,
               bool skipSignal);
        /** Set value of State */
        virtual State state(State value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.Neighbor.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static State convertStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.Neighbor.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<State> convertStringToState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Network.Neighbor.<value name>"
         */
        static std::string convertStateToString(State e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Network_Neighbor_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Network_Neighbor_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Network.Neighbor";

    private:

        /** @brief sd-bus callback for get-property 'IPAddress' */
        static int _callback_get_IPAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IPAddress' */
        static int _callback_set_IPAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MACAddress' */
        static int _callback_get_MACAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MACAddress' */
        static int _callback_set_MACAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'State' */
        static int _callback_get_State(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'State' */
        static int _callback_set_State(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Network_Neighbor_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _ipAddress{};
        std::string _macAddress{};
        State _state{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Neighbor::State.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Neighbor::State e)
{
    return Neighbor::convertStateToString(e);
}

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Network::server::Neighbor::State>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Network::server::Neighbor::convertStringToState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Network::server::Neighbor::State>
{
    static std::string op(xyz::openbmc_project::Network::server::Neighbor::State value)
    {
        return xyz::openbmc_project::Network::server::Neighbor::convertStateToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

