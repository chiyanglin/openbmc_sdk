#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace client
{
namespace SystemConfiguration
{

static constexpr auto interface = "xyz.openbmc_project.Network.SystemConfiguration";

} // namespace SystemConfiguration
} // namespace client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

