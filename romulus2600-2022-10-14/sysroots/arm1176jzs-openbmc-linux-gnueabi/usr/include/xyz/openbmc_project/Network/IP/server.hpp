#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

class IP
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        IP() = delete;
        IP(const IP&) = delete;
        IP& operator=(const IP&) = delete;
        IP(IP&&) = delete;
        IP& operator=(IP&&) = delete;
        virtual ~IP() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        IP(bus_t& bus, const char* path);

        enum class Protocol
        {
            IPv4,
            IPv6,
        };
        enum class AddressOrigin
        {
            Static,
            DHCP,
            LinkLocal,
            SLAAC,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                AddressOrigin,
                Protocol,
                std::string,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        IP(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Address */
        virtual std::string address() const;
        /** Set value of Address with option to skip sending signal */
        virtual std::string address(std::string value,
               bool skipSignal);
        /** Set value of Address */
        virtual std::string address(std::string value);
        /** Get value of PrefixLength */
        virtual uint8_t prefixLength() const;
        /** Set value of PrefixLength with option to skip sending signal */
        virtual uint8_t prefixLength(uint8_t value,
               bool skipSignal);
        /** Set value of PrefixLength */
        virtual uint8_t prefixLength(uint8_t value);
        /** Get value of Origin */
        virtual AddressOrigin origin() const;
        /** Set value of Origin with option to skip sending signal */
        virtual AddressOrigin origin(AddressOrigin value,
               bool skipSignal);
        /** Set value of Origin */
        virtual AddressOrigin origin(AddressOrigin value);
        /** Get value of Gateway */
        virtual std::string gateway() const;
        /** Set value of Gateway with option to skip sending signal */
        virtual std::string gateway(std::string value,
               bool skipSignal);
        /** Set value of Gateway */
        virtual std::string gateway(std::string value);
        /** Get value of Type */
        virtual Protocol type() const;
        /** Set value of Type with option to skip sending signal */
        virtual Protocol type(Protocol value,
               bool skipSignal);
        /** Set value of Type */
        virtual Protocol type(Protocol value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.IP.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Protocol convertProtocolFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.IP.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Protocol> convertStringToProtocol(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Network.IP.<value name>"
         */
        static std::string convertProtocolToString(Protocol e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.IP.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static AddressOrigin convertAddressOriginFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.IP.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<AddressOrigin> convertStringToAddressOrigin(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Network.IP.<value name>"
         */
        static std::string convertAddressOriginToString(AddressOrigin e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Network_IP_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Network_IP_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Network.IP";

    private:

        /** @brief sd-bus callback for get-property 'Address' */
        static int _callback_get_Address(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Address' */
        static int _callback_set_Address(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PrefixLength' */
        static int _callback_get_PrefixLength(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PrefixLength' */
        static int _callback_set_PrefixLength(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Origin' */
        static int _callback_get_Origin(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Origin' */
        static int _callback_set_Origin(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Gateway' */
        static int _callback_get_Gateway(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Gateway' */
        static int _callback_set_Gateway(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Type' */
        static int _callback_get_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Type' */
        static int _callback_set_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Network_IP_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _address{};
        uint8_t _prefixLength{};
        AddressOrigin _origin{};
        std::string _gateway{};
        Protocol _type{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type IP::Protocol.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(IP::Protocol e)
{
    return IP::convertProtocolToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type IP::AddressOrigin.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(IP::AddressOrigin e)
{
    return IP::convertAddressOriginToString(e);
}

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Network::server::IP::Protocol>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Network::server::IP::convertStringToProtocol(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Network::server::IP::Protocol>
{
    static std::string op(xyz::openbmc_project::Network::server::IP::Protocol value)
    {
        return xyz::openbmc_project::Network::server::IP::convertProtocolToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Network::server::IP::AddressOrigin>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Network::server::IP::convertStringToAddressOrigin(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Network::server::IP::AddressOrigin>
{
    static std::string op(xyz::openbmc_project::Network::server::IP::AddressOrigin value)
    {
        return xyz::openbmc_project::Network::server::IP::convertAddressOriginToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

