#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace client
{
namespace VLAN
{

static constexpr auto interface = "xyz.openbmc_project.Network.VLAN";

} // namespace VLAN
} // namespace client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

