#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Experimental
{
namespace client
{
namespace Bond
{

static constexpr auto interface = "xyz.openbmc_project.Network.Experimental.Bond";

} // namespace Bond
} // namespace client
} // namespace Experimental
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

