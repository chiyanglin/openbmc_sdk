#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace client
{
namespace IP
{

static constexpr auto interface = "xyz.openbmc_project.Network.IP";

} // namespace IP
} // namespace client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

