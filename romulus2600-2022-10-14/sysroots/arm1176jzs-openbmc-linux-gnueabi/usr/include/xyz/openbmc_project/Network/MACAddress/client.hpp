#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace client
{
namespace MACAddress
{

static constexpr auto interface = "xyz.openbmc_project.Network.MACAddress";

} // namespace MACAddress
} // namespace client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

