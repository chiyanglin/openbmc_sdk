#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace client
{
namespace EthernetInterface
{

static constexpr auto interface = "xyz.openbmc_project.Network.EthernetInterface";

} // namespace EthernetInterface
} // namespace client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

