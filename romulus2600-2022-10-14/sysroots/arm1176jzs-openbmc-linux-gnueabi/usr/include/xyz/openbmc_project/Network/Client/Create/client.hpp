#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Client
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.Network.Client.Create";

} // namespace Create
} // namespace client
} // namespace Client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

