#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#include <xyz/openbmc_project/HardwareIsolation/Entry/server.hpp>

#include <xyz/openbmc_project/HardwareIsolation/Entry/server.hpp>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);



        /** @brief Implementation for Create
         *  Create an xyz.openbmc_project.HardwareIsolation.Entry object if any of the hardware, needs to be isolated. This interface can be used if want to isolate hardware without an error log, for example, the user voluntarily tried to isolate hardware.
         *
         *  @param[in] isolateHardware - The hardware inventory path which is needs to isolate.
         *  @param[in] severity - The severity of hardware isolation.
         *
         *  @return path[sdbusplus::message::object_path] - The path of created xyz.openbmc_project.HardwareIsolation.Entry object.
         */
        virtual sdbusplus::message::object_path create(
            sdbusplus::message::object_path isolateHardware,
            xyz::openbmc_project::HardwareIsolation::server::Entry::Type severity) = 0;

        /** @brief Implementation for CreateWithErrorLog
         *  Create an xyz.openbmc_project.HardwareIsolation.Entry object if any of the hardware, needs to be isolated. This interface can be used if the system wants to isolate hardware with an error log that is caused to isolate hardware. This method is not going to create an error log and the consumer of this method need to pass the bmc error log which caused the isolation.
         *
         *  @param[in] isolateHardware - The hardware inventory path which is needs to isolate.
         *  @param[in] severity - The severity of hardware isolation.
         *  @param[in] bmcErrorLog - The BMC error log caused the isolation of hardware.
         *
         *  @return path[sdbusplus::message::object_path] - The path of created xyz.openbmc_project.HardwareIsolation.Entry object.
         */
        virtual sdbusplus::message::object_path createWithErrorLog(
            sdbusplus::message::object_path isolateHardware,
            xyz::openbmc_project::HardwareIsolation::server::Entry::Type severity,
            sdbusplus::message::object_path bmcErrorLog) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_HardwareIsolation_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_HardwareIsolation_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.HardwareIsolation.Create";

    private:

        /** @brief sd-bus callback for Create
         */
        static int _callback_Create(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for CreateWithErrorLog
         */
        static int _callback_CreateWithErrorLog(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_HardwareIsolation_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

