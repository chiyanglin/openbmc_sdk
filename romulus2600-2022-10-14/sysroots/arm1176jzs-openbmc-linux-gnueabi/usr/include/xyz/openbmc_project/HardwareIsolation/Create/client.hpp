#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.HardwareIsolation.Create";

} // namespace Create
} // namespace client
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

