#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace server
{

class Entry
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Entry() = delete;
        Entry(const Entry&) = delete;
        Entry& operator=(const Entry&) = delete;
        Entry(Entry&&) = delete;
        Entry& operator=(Entry&&) = delete;
        virtual ~Entry() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Entry(bus_t& bus, const char* path);

        enum class Type
        {
            Critical,
            Warning,
            Manual,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Type,
                bool>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Entry(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Severity */
        virtual Type severity() const;
        /** Set value of Severity with option to skip sending signal */
        virtual Type severity(Type value,
               bool skipSignal);
        /** Set value of Severity */
        virtual Type severity(Type value);
        /** Get value of Resolved */
        virtual bool resolved() const;
        /** Set value of Resolved with option to skip sending signal */
        virtual bool resolved(bool value,
               bool skipSignal);
        /** Set value of Resolved */
        virtual bool resolved(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.HardwareIsolation.Entry.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Type convertTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.HardwareIsolation.Entry.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Type> convertStringToType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.HardwareIsolation.Entry.<value name>"
         */
        static std::string convertTypeToString(Type e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_HardwareIsolation_Entry_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_HardwareIsolation_Entry_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.HardwareIsolation.Entry";

    private:

        /** @brief sd-bus callback for get-property 'Severity' */
        static int _callback_get_Severity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Severity' */
        static int _callback_set_Severity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Resolved' */
        static int _callback_get_Resolved(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Resolved' */
        static int _callback_set_Resolved(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_HardwareIsolation_Entry_interface;
        sdbusplus::SdBusInterface *_intf;

        Type _severity{};
        bool _resolved = false;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Entry::Type.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Entry::Type e)
{
    return Entry::convertTypeToString(e);
}

} // namespace server
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::HardwareIsolation::server::Entry::Type>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::HardwareIsolation::server::Entry::convertStringToType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::HardwareIsolation::server::Entry::Type>
{
    static std::string op(xyz::openbmc_project::HardwareIsolation::server::Entry::Type value)
    {
        return xyz::openbmc_project::HardwareIsolation::server::Entry::convertTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

