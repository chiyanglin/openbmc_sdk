#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace client
{
namespace Entry
{

static constexpr auto interface = "xyz.openbmc_project.HardwareIsolation.Entry";

} // namespace Entry
} // namespace client
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

