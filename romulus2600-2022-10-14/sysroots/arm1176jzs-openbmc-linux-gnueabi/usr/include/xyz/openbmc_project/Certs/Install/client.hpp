#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace client
{
namespace Install
{

static constexpr auto interface = "xyz.openbmc_project.Certs.Install";

} // namespace Install
} // namespace client
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

