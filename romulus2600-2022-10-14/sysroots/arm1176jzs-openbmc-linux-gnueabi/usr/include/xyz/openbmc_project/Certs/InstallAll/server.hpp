#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

class InstallAll
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        InstallAll() = delete;
        InstallAll(const InstallAll&) = delete;
        InstallAll& operator=(const InstallAll&) = delete;
        InstallAll(InstallAll&&) = delete;
        InstallAll& operator=(InstallAll&&) = delete;
        virtual ~InstallAll() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        InstallAll(bus_t& bus, const char* path);



        /** @brief Implementation for InstallAll
         *  Install the authority list and restart the associated services.
         *
         *  @param[in] path - Path of the file that contains a list of root certificates.
         *
         *  @return objectPath[std::vector<sdbusplus::message::object_path>] - D-Bus object path to created objects.
         */
        virtual std::vector<sdbusplus::message::object_path> installAll(
            std::string path) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Certs_InstallAll_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Certs_InstallAll_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Certs.InstallAll";

    private:

        /** @brief sd-bus callback for InstallAll
         */
        static int _callback_InstallAll(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Certs_InstallAll_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

