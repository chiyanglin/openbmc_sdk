#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace client
{
namespace Certificate
{

static constexpr auto interface = "xyz.openbmc_project.Certs.Certificate";

} // namespace Certificate
} // namespace client
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

