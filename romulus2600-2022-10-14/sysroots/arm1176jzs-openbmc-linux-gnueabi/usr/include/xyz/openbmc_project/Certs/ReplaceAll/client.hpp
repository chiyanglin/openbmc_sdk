#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace client
{
namespace ReplaceAll
{

static constexpr auto interface = "xyz.openbmc_project.Certs.ReplaceAll";

} // namespace ReplaceAll
} // namespace client
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

