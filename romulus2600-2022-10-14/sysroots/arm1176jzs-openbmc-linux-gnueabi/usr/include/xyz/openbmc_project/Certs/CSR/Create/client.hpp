#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace CSR
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.Certs.CSR.Create";

} // namespace Create
} // namespace client
} // namespace CSR
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

