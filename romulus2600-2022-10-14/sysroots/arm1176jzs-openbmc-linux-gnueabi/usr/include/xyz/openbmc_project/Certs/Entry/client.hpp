#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace client
{
namespace Entry
{

static constexpr auto interface = "xyz.openbmc_project.Certs.Entry";

} // namespace Entry
} // namespace client
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

