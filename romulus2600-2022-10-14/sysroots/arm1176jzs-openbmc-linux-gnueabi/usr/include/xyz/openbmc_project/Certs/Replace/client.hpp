#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace client
{
namespace Replace
{

static constexpr auto interface = "xyz.openbmc_project.Certs.Replace";

} // namespace Replace
} // namespace client
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

