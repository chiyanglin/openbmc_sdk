#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace client
{
namespace Association
{

static constexpr auto interface = "xyz.openbmc_project.Association";

} // namespace Association
} // namespace client
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

