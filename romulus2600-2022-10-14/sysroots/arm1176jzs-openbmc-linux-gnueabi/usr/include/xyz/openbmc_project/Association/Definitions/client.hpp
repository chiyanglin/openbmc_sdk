#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Association
{
namespace client
{
namespace Definitions
{

static constexpr auto interface = "xyz.openbmc_project.Association.Definitions";

} // namespace Definitions
} // namespace client
} // namespace Association
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

