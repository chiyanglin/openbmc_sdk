#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Condition
{
namespace client
{
namespace HostFirmware
{

static constexpr auto interface = "xyz.openbmc_project.Condition.HostFirmware";

} // namespace HostFirmware
} // namespace client
} // namespace Condition
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

