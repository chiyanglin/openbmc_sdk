#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Time
{
namespace client
{
namespace Synchronization
{

static constexpr auto interface = "xyz.openbmc_project.Time.Synchronization";

} // namespace Synchronization
} // namespace client
} // namespace Time
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

