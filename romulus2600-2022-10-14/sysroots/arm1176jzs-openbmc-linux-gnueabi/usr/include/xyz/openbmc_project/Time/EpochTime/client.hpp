#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Time
{
namespace client
{
namespace EpochTime
{

static constexpr auto interface = "xyz.openbmc_project.Time.EpochTime";

} // namespace EpochTime
} // namespace client
} // namespace Time
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

