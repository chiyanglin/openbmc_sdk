#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace client
{
namespace Manager
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Manager";

} // namespace Manager
} // namespace client
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

