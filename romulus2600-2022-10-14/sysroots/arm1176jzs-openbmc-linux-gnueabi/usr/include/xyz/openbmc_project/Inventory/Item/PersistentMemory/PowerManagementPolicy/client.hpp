#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace client
{
namespace PowerManagementPolicy
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PersistentMemory.PowerManagementPolicy";

} // namespace PowerManagementPolicy
} // namespace client
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

