#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace Replaceable
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.Replaceable";

} // namespace Replaceable
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

