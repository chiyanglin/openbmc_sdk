#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Dimm
{
namespace client
{
namespace MemoryLocation
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Dimm.MemoryLocation";

} // namespace MemoryLocation
} // namespace client
} // namespace Dimm
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

