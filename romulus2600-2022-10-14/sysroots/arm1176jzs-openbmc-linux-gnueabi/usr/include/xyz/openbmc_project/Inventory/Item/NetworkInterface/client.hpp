#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace NetworkInterface
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.NetworkInterface";

} // namespace NetworkInterface
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

