#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace MeetsMinimumShipLevel
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.MeetsMinimumShipLevel";

} // namespace MeetsMinimumShipLevel
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

