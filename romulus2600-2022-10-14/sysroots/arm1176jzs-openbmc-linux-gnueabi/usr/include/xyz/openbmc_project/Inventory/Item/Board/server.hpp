#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Board
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Board() = delete;
        Board(const Board&) = delete;
        Board& operator=(const Board&) = delete;
        Board(Board&&) = delete;
        Board& operator=(Board&&) = delete;
        virtual ~Board() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Board(bus_t& bus, const char* path);






        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Board_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Board_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Board";

    private:


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Board_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

