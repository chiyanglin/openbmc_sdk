#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Connector
{
namespace client
{
namespace Slot
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Connector.Slot";

} // namespace Slot
} // namespace client
} // namespace Connector
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

