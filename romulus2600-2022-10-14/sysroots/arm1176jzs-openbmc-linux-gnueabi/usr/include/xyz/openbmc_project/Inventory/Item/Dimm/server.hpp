#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>
















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Dimm
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Dimm() = delete;
        Dimm(const Dimm&) = delete;
        Dimm& operator=(const Dimm&) = delete;
        Dimm(Dimm&&) = delete;
        Dimm& operator=(Dimm&&) = delete;
        virtual ~Dimm() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Dimm(bus_t& bus, const char* path);

        enum class Ecc
        {
            NoECC,
            SingleBitECC,
            MultiBitECC,
            AddressParity,
        };
        enum class MemoryTech
        {
            Other,
            Unknown,
            DRAM,
            NVDIMM_N,
            NVDIMM_F,
            NVDIMM_P,
            IntelOptane,
        };
        enum class DeviceType
        {
            Other,
            Unknown,
            DRAM,
            EDRAM,
            VRAM,
            SRAM,
            RAM,
            ROM,
            FLASH,
            EEPROM,
            FEPROM,
            EPROM,
            CDRAM,
            ThreeDRAM,
            SDRAM,
            DDR_SGRAM,
            RDRAM,
            DDR,
            DDR2,
            DDR2_SDRAM_FB_DIMM,
            EDO,
            FastPageMode,
            PipelinedNibble,
            DDR3,
            FBD2,
            DDR4,
            LPDDR_SDRAM,
            LPDDR2_SDRAM,
            LPDDR3_SDRAM,
            LPDDR4_SDRAM,
            Logical,
            HBM,
            HBM2,
            HBM3,
            DDR2_SDRAM_FB_DIMM_PROB,
            DDR4E_SDRAM,
            DDR5,
            LPDDR5_SDRAM,
        };
        enum class FormFactor
        {
            RDIMM,
            UDIMM,
            SO_DIMM,
            LRDIMM,
            Mini_RDIMM,
            Mini_UDIMM,
            SO_RDIMM_72b,
            SO_UDIMM_72b,
            SO_DIMM_16b,
            SO_DIMM_32b,
            Die,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                DeviceType,
                Ecc,
                FormFactor,
                MemoryTech,
                size_t,
                std::string,
                std::vector<uint16_t>,
                uint16_t,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Dimm(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of MemoryDataWidth */
        virtual uint16_t memoryDataWidth() const;
        /** Set value of MemoryDataWidth with option to skip sending signal */
        virtual uint16_t memoryDataWidth(uint16_t value,
               bool skipSignal);
        /** Set value of MemoryDataWidth */
        virtual uint16_t memoryDataWidth(uint16_t value);
        /** Get value of MemorySizeInKB */
        virtual size_t memorySizeInKB() const;
        /** Set value of MemorySizeInKB with option to skip sending signal */
        virtual size_t memorySizeInKB(size_t value,
               bool skipSignal);
        /** Set value of MemorySizeInKB */
        virtual size_t memorySizeInKB(size_t value);
        /** Get value of MemoryDeviceLocator */
        virtual std::string memoryDeviceLocator() const;
        /** Set value of MemoryDeviceLocator with option to skip sending signal */
        virtual std::string memoryDeviceLocator(std::string value,
               bool skipSignal);
        /** Set value of MemoryDeviceLocator */
        virtual std::string memoryDeviceLocator(std::string value);
        /** Get value of MemoryType */
        virtual DeviceType memoryType() const;
        /** Set value of MemoryType with option to skip sending signal */
        virtual DeviceType memoryType(DeviceType value,
               bool skipSignal);
        /** Set value of MemoryType */
        virtual DeviceType memoryType(DeviceType value);
        /** Get value of MemoryTypeDetail */
        virtual std::string memoryTypeDetail() const;
        /** Set value of MemoryTypeDetail with option to skip sending signal */
        virtual std::string memoryTypeDetail(std::string value,
               bool skipSignal);
        /** Set value of MemoryTypeDetail */
        virtual std::string memoryTypeDetail(std::string value);
        /** Get value of MaxMemorySpeedInMhz */
        virtual uint16_t maxMemorySpeedInMhz() const;
        /** Set value of MaxMemorySpeedInMhz with option to skip sending signal */
        virtual uint16_t maxMemorySpeedInMhz(uint16_t value,
               bool skipSignal);
        /** Set value of MaxMemorySpeedInMhz */
        virtual uint16_t maxMemorySpeedInMhz(uint16_t value);
        /** Get value of MemoryAttributes */
        virtual uint8_t memoryAttributes() const;
        /** Set value of MemoryAttributes with option to skip sending signal */
        virtual uint8_t memoryAttributes(uint8_t value,
               bool skipSignal);
        /** Set value of MemoryAttributes */
        virtual uint8_t memoryAttributes(uint8_t value);
        /** Get value of MemoryConfiguredSpeedInMhz */
        virtual uint16_t memoryConfiguredSpeedInMhz() const;
        /** Set value of MemoryConfiguredSpeedInMhz with option to skip sending signal */
        virtual uint16_t memoryConfiguredSpeedInMhz(uint16_t value,
               bool skipSignal);
        /** Set value of MemoryConfiguredSpeedInMhz */
        virtual uint16_t memoryConfiguredSpeedInMhz(uint16_t value);
        /** Get value of ECC */
        virtual Ecc ecc() const;
        /** Set value of ECC with option to skip sending signal */
        virtual Ecc ecc(Ecc value,
               bool skipSignal);
        /** Set value of ECC */
        virtual Ecc ecc(Ecc value);
        /** Get value of CASLatencies */
        virtual uint16_t casLatencies() const;
        /** Set value of CASLatencies with option to skip sending signal */
        virtual uint16_t casLatencies(uint16_t value,
               bool skipSignal);
        /** Set value of CASLatencies */
        virtual uint16_t casLatencies(uint16_t value);
        /** Get value of RevisionCode */
        virtual uint16_t revisionCode() const;
        /** Set value of RevisionCode with option to skip sending signal */
        virtual uint16_t revisionCode(uint16_t value,
               bool skipSignal);
        /** Set value of RevisionCode */
        virtual uint16_t revisionCode(uint16_t value);
        /** Get value of FormFactor */
        virtual FormFactor formFactor() const;
        /** Set value of FormFactor with option to skip sending signal */
        virtual FormFactor formFactor(FormFactor value,
               bool skipSignal);
        /** Set value of FormFactor */
        virtual FormFactor formFactor(FormFactor value);
        /** Get value of MemoryTotalWidth */
        virtual uint16_t memoryTotalWidth() const;
        /** Set value of MemoryTotalWidth with option to skip sending signal */
        virtual uint16_t memoryTotalWidth(uint16_t value,
               bool skipSignal);
        /** Set value of MemoryTotalWidth */
        virtual uint16_t memoryTotalWidth(uint16_t value);
        /** Get value of AllowedSpeedsMT */
        virtual std::vector<uint16_t> allowedSpeedsMT() const;
        /** Set value of AllowedSpeedsMT with option to skip sending signal */
        virtual std::vector<uint16_t> allowedSpeedsMT(std::vector<uint16_t> value,
               bool skipSignal);
        /** Set value of AllowedSpeedsMT */
        virtual std::vector<uint16_t> allowedSpeedsMT(std::vector<uint16_t> value);
        /** Get value of MemoryMedia */
        virtual MemoryTech memoryMedia() const;
        /** Set value of MemoryMedia with option to skip sending signal */
        virtual MemoryTech memoryMedia(MemoryTech value,
               bool skipSignal);
        /** Set value of MemoryMedia */
        virtual MemoryTech memoryMedia(MemoryTech value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Ecc convertEccFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Ecc> convertStringToEcc(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         */
        static std::string convertEccToString(Ecc e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static MemoryTech convertMemoryTechFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<MemoryTech> convertStringToMemoryTech(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         */
        static std::string convertMemoryTechToString(MemoryTech e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DeviceType convertDeviceTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DeviceType> convertStringToDeviceType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         */
        static std::string convertDeviceTypeToString(DeviceType e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static FormFactor convertFormFactorFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<FormFactor> convertStringToFormFactor(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Dimm.<value name>"
         */
        static std::string convertFormFactorToString(FormFactor e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Dimm";

    private:

        /** @brief sd-bus callback for get-property 'MemoryDataWidth' */
        static int _callback_get_MemoryDataWidth(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryDataWidth' */
        static int _callback_set_MemoryDataWidth(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemorySizeInKB' */
        static int _callback_get_MemorySizeInKB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemorySizeInKB' */
        static int _callback_set_MemorySizeInKB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryDeviceLocator' */
        static int _callback_get_MemoryDeviceLocator(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryDeviceLocator' */
        static int _callback_set_MemoryDeviceLocator(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryType' */
        static int _callback_get_MemoryType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryType' */
        static int _callback_set_MemoryType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryTypeDetail' */
        static int _callback_get_MemoryTypeDetail(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryTypeDetail' */
        static int _callback_set_MemoryTypeDetail(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxMemorySpeedInMhz' */
        static int _callback_get_MaxMemorySpeedInMhz(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxMemorySpeedInMhz' */
        static int _callback_set_MaxMemorySpeedInMhz(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryAttributes' */
        static int _callback_get_MemoryAttributes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryAttributes' */
        static int _callback_set_MemoryAttributes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryConfiguredSpeedInMhz' */
        static int _callback_get_MemoryConfiguredSpeedInMhz(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryConfiguredSpeedInMhz' */
        static int _callback_set_MemoryConfiguredSpeedInMhz(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ECC' */
        static int _callback_get_ECC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ECC' */
        static int _callback_set_ECC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CASLatencies' */
        static int _callback_get_CASLatencies(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CASLatencies' */
        static int _callback_set_CASLatencies(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RevisionCode' */
        static int _callback_get_RevisionCode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RevisionCode' */
        static int _callback_set_RevisionCode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'FormFactor' */
        static int _callback_get_FormFactor(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'FormFactor' */
        static int _callback_set_FormFactor(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryTotalWidth' */
        static int _callback_get_MemoryTotalWidth(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryTotalWidth' */
        static int _callback_set_MemoryTotalWidth(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AllowedSpeedsMT' */
        static int _callback_get_AllowedSpeedsMT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AllowedSpeedsMT' */
        static int _callback_set_AllowedSpeedsMT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryMedia' */
        static int _callback_get_MemoryMedia(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryMedia' */
        static int _callback_set_MemoryMedia(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Dimm_interface;
        sdbusplus::SdBusInterface *_intf;

        uint16_t _memoryDataWidth{};
        size_t _memorySizeInKB{};
        std::string _memoryDeviceLocator{};
        DeviceType _memoryType{};
        std::string _memoryTypeDetail{};
        uint16_t _maxMemorySpeedInMhz{};
        uint8_t _memoryAttributes{};
        uint16_t _memoryConfiguredSpeedInMhz{};
        Ecc _ecc{};
        uint16_t _casLatencies{};
        uint16_t _revisionCode{};
        FormFactor _formFactor{};
        uint16_t _memoryTotalWidth{};
        std::vector<uint16_t> _allowedSpeedsMT{};
        MemoryTech _memoryMedia = MemoryTech::Unknown;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Dimm::Ecc.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Dimm::Ecc e)
{
    return Dimm::convertEccToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Dimm::MemoryTech.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Dimm::MemoryTech e)
{
    return Dimm::convertMemoryTechToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Dimm::DeviceType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Dimm::DeviceType e)
{
    return Dimm::convertDeviceTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Dimm::FormFactor.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Dimm::FormFactor e)
{
    return Dimm::convertFormFactorToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Dimm::Ecc>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertStringToEcc(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Dimm::Ecc>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Dimm::Ecc value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertEccToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Dimm::MemoryTech>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertStringToMemoryTech(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Dimm::MemoryTech>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Dimm::MemoryTech value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertMemoryTechToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Dimm::DeviceType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertStringToDeviceType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Dimm::DeviceType>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Dimm::DeviceType value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertDeviceTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Dimm::FormFactor>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertStringToFormFactor(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Dimm::FormFactor>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Dimm::FormFactor value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Dimm::convertFormFactorToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

