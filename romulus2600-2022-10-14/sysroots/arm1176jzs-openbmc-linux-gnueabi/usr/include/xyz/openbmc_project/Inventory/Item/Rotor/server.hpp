#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Rotor
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Rotor() = delete;
        Rotor(const Rotor&) = delete;
        Rotor& operator=(const Rotor&) = delete;
        Rotor(Rotor&&) = delete;
        Rotor& operator=(Rotor&&) = delete;
        virtual ~Rotor() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Rotor(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Rotor(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of NominalVoltage */
        virtual double nominalVoltage() const;
        /** Set value of NominalVoltage with option to skip sending signal */
        virtual double nominalVoltage(double value,
               bool skipSignal);
        /** Set value of NominalVoltage */
        virtual double nominalVoltage(double value);
        /** Get value of NominalCurrent */
        virtual double nominalCurrent() const;
        /** Set value of NominalCurrent with option to skip sending signal */
        virtual double nominalCurrent(double value,
               bool skipSignal);
        /** Set value of NominalCurrent */
        virtual double nominalCurrent(double value);
        /** Get value of NominalRPM */
        virtual double nominalRPM() const;
        /** Set value of NominalRPM with option to skip sending signal */
        virtual double nominalRPM(double value,
               bool skipSignal);
        /** Set value of NominalRPM */
        virtual double nominalRPM(double value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Rotor_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Rotor_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Rotor";

    private:

        /** @brief sd-bus callback for get-property 'NominalVoltage' */
        static int _callback_get_NominalVoltage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NominalVoltage' */
        static int _callback_set_NominalVoltage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'NominalCurrent' */
        static int _callback_get_NominalCurrent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NominalCurrent' */
        static int _callback_set_NominalCurrent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'NominalRPM' */
        static int _callback_get_NominalRPM(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NominalRPM' */
        static int _callback_set_NominalRPM(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Rotor_interface;
        sdbusplus::SdBusInterface *_intf;

        double _nominalVoltage{};
        double _nominalCurrent{};
        double _nominalRPM{};

};


} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

