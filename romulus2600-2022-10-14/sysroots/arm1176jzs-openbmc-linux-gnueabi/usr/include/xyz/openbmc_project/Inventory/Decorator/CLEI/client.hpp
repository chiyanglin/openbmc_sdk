#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace CLEI
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.CLEI";

} // namespace CLEI
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

