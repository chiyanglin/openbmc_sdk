#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace CoolingType
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.CoolingType";

} // namespace CoolingType
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

