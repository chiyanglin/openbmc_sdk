#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Board
{
namespace client
{
namespace IOBoard
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Board.IOBoard";

} // namespace IOBoard
} // namespace client
} // namespace Board
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

