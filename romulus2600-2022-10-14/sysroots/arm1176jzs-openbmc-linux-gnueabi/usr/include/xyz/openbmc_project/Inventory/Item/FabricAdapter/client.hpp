#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace FabricAdapter
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.FabricAdapter";

} // namespace FabricAdapter
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

