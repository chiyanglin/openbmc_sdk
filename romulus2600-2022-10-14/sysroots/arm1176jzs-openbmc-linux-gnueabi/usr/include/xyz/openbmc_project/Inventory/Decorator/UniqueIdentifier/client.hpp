#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace UniqueIdentifier
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.UniqueIdentifier";

} // namespace UniqueIdentifier
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

