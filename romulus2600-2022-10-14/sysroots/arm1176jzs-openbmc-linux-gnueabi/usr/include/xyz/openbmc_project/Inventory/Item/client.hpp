#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace client
{
namespace Item
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item";

} // namespace Item
} // namespace client
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

