#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace I2CDevice
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.I2CDevice";

} // namespace I2CDevice
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

