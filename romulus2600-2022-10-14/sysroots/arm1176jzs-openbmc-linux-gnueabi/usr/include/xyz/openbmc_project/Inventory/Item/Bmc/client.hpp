#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace Bmc
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Bmc";

} // namespace Bmc
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

