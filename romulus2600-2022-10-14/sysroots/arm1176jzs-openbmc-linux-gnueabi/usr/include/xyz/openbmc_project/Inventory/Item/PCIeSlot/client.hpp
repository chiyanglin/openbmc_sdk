#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace PCIeSlot
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PCIeSlot";

} // namespace PCIeSlot
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

