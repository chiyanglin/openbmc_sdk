#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace DiskBackplane
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.DiskBackplane";

} // namespace DiskBackplane
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

