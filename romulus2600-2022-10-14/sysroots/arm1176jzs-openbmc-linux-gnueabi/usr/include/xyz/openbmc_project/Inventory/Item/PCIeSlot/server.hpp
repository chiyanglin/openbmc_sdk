#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class PCIeSlot
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PCIeSlot() = delete;
        PCIeSlot(const PCIeSlot&) = delete;
        PCIeSlot& operator=(const PCIeSlot&) = delete;
        PCIeSlot(PCIeSlot&&) = delete;
        PCIeSlot& operator=(PCIeSlot&&) = delete;
        virtual ~PCIeSlot() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PCIeSlot(bus_t& bus, const char* path);

        enum class Generations
        {
            Gen1,
            Gen2,
            Gen3,
            Gen4,
            Gen5,
            Unknown,
        };
        enum class SlotTypes
        {
            FullLength,
            HalfLength,
            LowProfile,
            Mini,
            M_2,
            OEM,
            OCP3Small,
            OCP3Large,
            U_2,
            Unknown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Generations,
                SlotTypes,
                bool,
                size_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PCIeSlot(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Generation */
        virtual Generations generation() const;
        /** Set value of Generation with option to skip sending signal */
        virtual Generations generation(Generations value,
               bool skipSignal);
        /** Set value of Generation */
        virtual Generations generation(Generations value);
        /** Get value of Lanes */
        virtual size_t lanes() const;
        /** Set value of Lanes with option to skip sending signal */
        virtual size_t lanes(size_t value,
               bool skipSignal);
        /** Set value of Lanes */
        virtual size_t lanes(size_t value);
        /** Get value of SlotType */
        virtual SlotTypes slotType() const;
        /** Set value of SlotType with option to skip sending signal */
        virtual SlotTypes slotType(SlotTypes value,
               bool skipSignal);
        /** Set value of SlotType */
        virtual SlotTypes slotType(SlotTypes value);
        /** Get value of HotPluggable */
        virtual bool hotPluggable() const;
        /** Set value of HotPluggable with option to skip sending signal */
        virtual bool hotPluggable(bool value,
               bool skipSignal);
        /** Set value of HotPluggable */
        virtual bool hotPluggable(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.PCIeSlot.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Generations convertGenerationsFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.PCIeSlot.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Generations> convertStringToGenerations(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.PCIeSlot.<value name>"
         */
        static std::string convertGenerationsToString(Generations e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.PCIeSlot.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static SlotTypes convertSlotTypesFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.PCIeSlot.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<SlotTypes> convertStringToSlotTypes(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.PCIeSlot.<value name>"
         */
        static std::string convertSlotTypesToString(SlotTypes e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PCIeSlot";

    private:

        /** @brief sd-bus callback for get-property 'Generation' */
        static int _callback_get_Generation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Generation' */
        static int _callback_set_Generation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Lanes' */
        static int _callback_get_Lanes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Lanes' */
        static int _callback_set_Lanes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SlotType' */
        static int _callback_get_SlotType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SlotType' */
        static int _callback_set_SlotType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HotPluggable' */
        static int _callback_get_HotPluggable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HotPluggable' */
        static int _callback_set_HotPluggable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface;
        sdbusplus::SdBusInterface *_intf;

        Generations _generation = Generations::Unknown;
        size_t _lanes{};
        SlotTypes _slotType = SlotTypes::Unknown;
        bool _hotPluggable{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PCIeSlot::Generations.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PCIeSlot::Generations e)
{
    return PCIeSlot::convertGenerationsToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PCIeSlot::SlotTypes.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PCIeSlot::SlotTypes e)
{
    return PCIeSlot::convertSlotTypesToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::PCIeSlot::convertStringToGenerations(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations value)
    {
        return xyz::openbmc_project::Inventory::Item::server::PCIeSlot::convertGenerationsToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::PCIeSlot::SlotTypes>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::PCIeSlot::convertStringToSlotTypes(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::PCIeSlot::SlotTypes>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::PCIeSlot::SlotTypes value)
    {
        return xyz::openbmc_project::Inventory::Item::server::PCIeSlot::convertSlotTypesToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

