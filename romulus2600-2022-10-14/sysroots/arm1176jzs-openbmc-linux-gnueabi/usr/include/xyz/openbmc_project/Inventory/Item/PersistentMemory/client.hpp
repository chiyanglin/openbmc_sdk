#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace PersistentMemory
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PersistentMemory";

} // namespace PersistentMemory
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

