#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Source
{
namespace PLDM
{
namespace client
{
namespace Entity
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Source.PLDM.Entity";

} // namespace Entity
} // namespace client
} // namespace PLDM
} // namespace Source
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

