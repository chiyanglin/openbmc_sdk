#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace server
{

class Partition
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Partition() = delete;
        Partition(const Partition&) = delete;
        Partition& operator=(const Partition&) = delete;
        Partition(Partition&&) = delete;
        Partition& operator=(Partition&&) = delete;
        virtual ~Partition() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Partition(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Partition(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PartitionId */
        virtual std::string partitionId() const;
        /** Set value of PartitionId with option to skip sending signal */
        virtual std::string partitionId(std::string value,
               bool skipSignal);
        /** Set value of PartitionId */
        virtual std::string partitionId(std::string value);
        /** Get value of SizeInKiB */
        virtual uint64_t sizeInKiB() const;
        /** Set value of SizeInKiB with option to skip sending signal */
        virtual uint64_t sizeInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of SizeInKiB */
        virtual uint64_t sizeInKiB(uint64_t value);
        /** Get value of MemoryClassification */
        virtual std::string memoryClassification() const;
        /** Set value of MemoryClassification with option to skip sending signal */
        virtual std::string memoryClassification(std::string value,
               bool skipSignal);
        /** Set value of MemoryClassification */
        virtual std::string memoryClassification(std::string value);
        /** Get value of OffsetInKiB */
        virtual uint64_t offsetInKiB() const;
        /** Set value of OffsetInKiB with option to skip sending signal */
        virtual uint64_t offsetInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of OffsetInKiB */
        virtual uint64_t offsetInKiB(uint64_t value);
        /** Get value of PassphraseState */
        virtual bool passphraseState() const;
        /** Set value of PassphraseState with option to skip sending signal */
        virtual bool passphraseState(bool value,
               bool skipSignal);
        /** Set value of PassphraseState */
        virtual bool passphraseState(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PersistentMemory.Partition";

    private:

        /** @brief sd-bus callback for get-property 'PartitionId' */
        static int _callback_get_PartitionId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PartitionId' */
        static int _callback_set_PartitionId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SizeInKiB' */
        static int _callback_get_SizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SizeInKiB' */
        static int _callback_set_SizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryClassification' */
        static int _callback_get_MemoryClassification(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryClassification' */
        static int _callback_set_MemoryClassification(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'OffsetInKiB' */
        static int _callback_get_OffsetInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'OffsetInKiB' */
        static int _callback_set_OffsetInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PassphraseState' */
        static int _callback_get_PassphraseState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PassphraseState' */
        static int _callback_set_PassphraseState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _partitionId{};
        uint64_t _sizeInKiB{};
        std::string _memoryClassification{};
        uint64_t _offsetInKiB{};
        bool _passphraseState{};

};


} // namespace server
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

