#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace server
{

class SecurityCapabilities
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        SecurityCapabilities() = delete;
        SecurityCapabilities(const SecurityCapabilities&) = delete;
        SecurityCapabilities& operator=(const SecurityCapabilities&) = delete;
        SecurityCapabilities(SecurityCapabilities&&) = delete;
        SecurityCapabilities& operator=(SecurityCapabilities&&) = delete;
        virtual ~SecurityCapabilities() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        SecurityCapabilities(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        SecurityCapabilities(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of MaxPassphraseCount */
        virtual uint32_t maxPassphraseCount() const;
        /** Set value of MaxPassphraseCount with option to skip sending signal */
        virtual uint32_t maxPassphraseCount(uint32_t value,
               bool skipSignal);
        /** Set value of MaxPassphraseCount */
        virtual uint32_t maxPassphraseCount(uint32_t value);
        /** Get value of PassphraseCapable */
        virtual bool passphraseCapable() const;
        /** Set value of PassphraseCapable with option to skip sending signal */
        virtual bool passphraseCapable(bool value,
               bool skipSignal);
        /** Set value of PassphraseCapable */
        virtual bool passphraseCapable(bool value);
        /** Get value of ConfigurationLockCapable */
        virtual bool configurationLockCapable() const;
        /** Set value of ConfigurationLockCapable with option to skip sending signal */
        virtual bool configurationLockCapable(bool value,
               bool skipSignal);
        /** Set value of ConfigurationLockCapable */
        virtual bool configurationLockCapable(bool value);
        /** Get value of DataLockCapable */
        virtual bool dataLockCapable() const;
        /** Set value of DataLockCapable with option to skip sending signal */
        virtual bool dataLockCapable(bool value,
               bool skipSignal);
        /** Set value of DataLockCapable */
        virtual bool dataLockCapable(bool value);
        /** Get value of PassphraseLockLimit */
        virtual uint32_t passphraseLockLimit() const;
        /** Set value of PassphraseLockLimit with option to skip sending signal */
        virtual uint32_t passphraseLockLimit(uint32_t value,
               bool skipSignal);
        /** Set value of PassphraseLockLimit */
        virtual uint32_t passphraseLockLimit(uint32_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PersistentMemory.SecurityCapabilities";

    private:

        /** @brief sd-bus callback for get-property 'MaxPassphraseCount' */
        static int _callback_get_MaxPassphraseCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxPassphraseCount' */
        static int _callback_set_MaxPassphraseCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PassphraseCapable' */
        static int _callback_get_PassphraseCapable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PassphraseCapable' */
        static int _callback_set_PassphraseCapable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ConfigurationLockCapable' */
        static int _callback_get_ConfigurationLockCapable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ConfigurationLockCapable' */
        static int _callback_set_ConfigurationLockCapable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DataLockCapable' */
        static int _callback_get_DataLockCapable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DataLockCapable' */
        static int _callback_set_DataLockCapable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PassphraseLockLimit' */
        static int _callback_get_PassphraseLockLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PassphraseLockLimit' */
        static int _callback_set_PassphraseLockLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface;
        sdbusplus::SdBusInterface *_intf;

        uint32_t _maxPassphraseCount{};
        bool _passphraseCapable{};
        bool _configurationLockCapable{};
        bool _dataLockCapable{};
        uint32_t _passphraseLockLimit{};

};


} // namespace server
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

