#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace client
{
namespace Fan
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Fan";

} // namespace Fan
} // namespace client
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

