#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Dimm
{
namespace server
{

class MemoryLocation
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        MemoryLocation() = delete;
        MemoryLocation(const MemoryLocation&) = delete;
        MemoryLocation& operator=(const MemoryLocation&) = delete;
        MemoryLocation(MemoryLocation&&) = delete;
        MemoryLocation& operator=(MemoryLocation&&) = delete;
        virtual ~MemoryLocation() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        MemoryLocation(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        MemoryLocation(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Socket */
        virtual uint8_t socket() const;
        /** Set value of Socket with option to skip sending signal */
        virtual uint8_t socket(uint8_t value,
               bool skipSignal);
        /** Set value of Socket */
        virtual uint8_t socket(uint8_t value);
        /** Get value of MemoryController */
        virtual uint8_t memoryController() const;
        /** Set value of MemoryController with option to skip sending signal */
        virtual uint8_t memoryController(uint8_t value,
               bool skipSignal);
        /** Set value of MemoryController */
        virtual uint8_t memoryController(uint8_t value);
        /** Get value of Channel */
        virtual uint8_t channel() const;
        /** Set value of Channel with option to skip sending signal */
        virtual uint8_t channel(uint8_t value,
               bool skipSignal);
        /** Set value of Channel */
        virtual uint8_t channel(uint8_t value);
        /** Get value of Slot */
        virtual uint8_t slot() const;
        /** Set value of Slot with option to skip sending signal */
        virtual uint8_t slot(uint8_t value,
               bool skipSignal);
        /** Set value of Slot */
        virtual uint8_t slot(uint8_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Dimm.MemoryLocation";

    private:

        /** @brief sd-bus callback for get-property 'Socket' */
        static int _callback_get_Socket(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Socket' */
        static int _callback_set_Socket(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MemoryController' */
        static int _callback_get_MemoryController(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MemoryController' */
        static int _callback_set_MemoryController(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Channel' */
        static int _callback_get_Channel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Channel' */
        static int _callback_set_Channel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Slot' */
        static int _callback_get_Slot(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Slot' */
        static int _callback_set_Slot(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface;
        sdbusplus::SdBusInterface *_intf;

        uint8_t _socket{};
        uint8_t _memoryController{};
        uint8_t _channel{};
        uint8_t _slot{};

};


} // namespace server
} // namespace Dimm
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

