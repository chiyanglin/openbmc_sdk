#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Cpu
{
namespace client
{
namespace OperatingConfig
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Cpu.OperatingConfig";

} // namespace OperatingConfig
} // namespace client
} // namespace Cpu
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

