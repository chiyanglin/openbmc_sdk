#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace LocationCode
{

static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.LocationCode";

} // namespace LocationCode
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

