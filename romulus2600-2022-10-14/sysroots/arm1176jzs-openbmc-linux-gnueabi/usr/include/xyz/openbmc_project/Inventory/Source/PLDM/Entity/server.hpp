#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Source
{
namespace PLDM
{
namespace server
{

class Entity
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Entity() = delete;
        Entity(const Entity&) = delete;
        Entity& operator=(const Entity&) = delete;
        Entity(Entity&&) = delete;
        Entity& operator=(Entity&&) = delete;
        virtual ~Entity() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Entity(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint16_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Entity(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of EntityType */
        virtual uint16_t entityType() const;
        /** Set value of EntityType with option to skip sending signal */
        virtual uint16_t entityType(uint16_t value,
               bool skipSignal);
        /** Set value of EntityType */
        virtual uint16_t entityType(uint16_t value);
        /** Get value of EntityInstanceNumber */
        virtual uint16_t entityInstanceNumber() const;
        /** Set value of EntityInstanceNumber with option to skip sending signal */
        virtual uint16_t entityInstanceNumber(uint16_t value,
               bool skipSignal);
        /** Set value of EntityInstanceNumber */
        virtual uint16_t entityInstanceNumber(uint16_t value);
        /** Get value of ContainerID */
        virtual uint16_t containerID() const;
        /** Set value of ContainerID with option to skip sending signal */
        virtual uint16_t containerID(uint16_t value,
               bool skipSignal);
        /** Set value of ContainerID */
        virtual uint16_t containerID(uint16_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Source.PLDM.Entity";

    private:

        /** @brief sd-bus callback for get-property 'EntityType' */
        static int _callback_get_EntityType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EntityType' */
        static int _callback_set_EntityType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EntityInstanceNumber' */
        static int _callback_get_EntityInstanceNumber(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EntityInstanceNumber' */
        static int _callback_set_EntityInstanceNumber(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ContainerID' */
        static int _callback_get_ContainerID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ContainerID' */
        static int _callback_set_ContainerID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface;
        sdbusplus::SdBusInterface *_intf;

        uint16_t _entityType{};
        uint16_t _entityInstanceNumber{};
        uint16_t _containerID{};

};


} // namespace server
} // namespace PLDM
} // namespace Source
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

