#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace client
{
namespace Manager
{

static constexpr auto interface = "xyz.openbmc_project.User.Manager";

} // namespace Manager
} // namespace client
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

