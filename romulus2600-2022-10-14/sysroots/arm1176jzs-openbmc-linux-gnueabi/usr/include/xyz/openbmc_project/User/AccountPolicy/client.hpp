#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace client
{
namespace AccountPolicy
{

static constexpr auto interface = "xyz.openbmc_project.User.AccountPolicy";

} // namespace AccountPolicy
} // namespace client
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

