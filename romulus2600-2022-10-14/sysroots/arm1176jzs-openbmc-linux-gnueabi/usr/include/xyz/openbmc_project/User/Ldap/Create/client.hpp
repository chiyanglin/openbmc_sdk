#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Ldap
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.User.Ldap.Create";

} // namespace Create
} // namespace client
} // namespace Ldap
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

