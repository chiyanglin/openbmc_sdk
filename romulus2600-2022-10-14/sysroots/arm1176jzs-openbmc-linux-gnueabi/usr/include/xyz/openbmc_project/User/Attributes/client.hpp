#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace client
{
namespace Attributes
{

static constexpr auto interface = "xyz.openbmc_project.User.Attributes";

} // namespace Attributes
} // namespace client
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

