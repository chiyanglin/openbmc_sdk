#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace client
{
namespace PrivilegeMapper
{

static constexpr auto interface = "xyz.openbmc_project.User.PrivilegeMapper";

} // namespace PrivilegeMapper
} // namespace client
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

