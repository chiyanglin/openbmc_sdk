#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Ldap
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);

        enum class SearchScope
        {
            sub,
            one,
            base,
        };
        enum class Type
        {
            ActiveDirectory,
            OpenLdap,
        };


        /** @brief Implementation for CreateConfig
         *  This method always creates a new config file as well as a D-Bus  object to represent the config, it will destroy an existing one, if found. In other words, this is not an update API. Individual properties can be updated as per the xyz/openbmc_project/User/Ldap/Config.interface.yaml.
         *
         *  @param[in] ldapServerURI - Specifies the LDAP URI of the server to connect to.
         *  @param[in] ldapBindDN - Specifies the distinguished name with which to bind to the directory server for lookups.
         *  @param[in] ldapBaseDN - Specifies the base distinguished name to use as search base.
         *  @param[in] ldapbinddNpassword - Specifies the clear text credentials with which to bind. This option is only applicable when used with LDAPBindDN.
         *  @param[in] ldapSearchScope - Specifies the search scope:subtree, one level or base object.
         *  @param[in] ldapType - Specifies the the configured server is ActiveDirectory(AD) or OpenLdap. It's just an indication for the LDAP stack running on the BMC, in case the app is implemented in such a way that it has to react differently for AD vs openldap.
         *  @param[in] groupNameAttribute - Specifies the attribute name that contains the name of the Group in the LDAP server.
         *  @param[in] usernameAttribute - Specifies the attribute name that contains the username in the LDAP server.
         *
         *  @return path[std::string] - The object path of the D-Bus object representing the config.
         */
        virtual std::string createConfig(
            std::string ldapServerURI,
            std::string ldapBindDN,
            std::string ldapBaseDN,
            std::string ldapbinddNpassword,
            SearchScope ldapSearchScope,
            Type ldapType,
            std::string groupNameAttribute,
            std::string usernameAttribute) = 0;



        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static SearchScope convertSearchScopeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<SearchScope> convertStringToSearchScope(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.User.Ldap.Create.<value name>"
         */
        static std::string convertSearchScopeToString(SearchScope e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Type convertTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Type> convertStringToType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.User.Ldap.Create.<value name>"
         */
        static std::string convertTypeToString(Type e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_User_Ldap_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_User_Ldap_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.User.Ldap.Create";

    private:

        /** @brief sd-bus callback for CreateConfig
         */
        static int _callback_CreateConfig(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_User_Ldap_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::SearchScope.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::SearchScope e)
{
    return Create::convertSearchScopeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::Type.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::Type e)
{
    return Create::convertTypeToString(e);
}

} // namespace server
} // namespace Ldap
} // namespace User
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::User::Ldap::server::Create::SearchScope>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::User::Ldap::server::Create::convertStringToSearchScope(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::User::Ldap::server::Create::SearchScope>
{
    static std::string op(xyz::openbmc_project::User::Ldap::server::Create::SearchScope value)
    {
        return xyz::openbmc_project::User::Ldap::server::Create::convertSearchScopeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::User::Ldap::server::Create::Type>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::User::Ldap::server::Create::convertStringToType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::User::Ldap::server::Create::Type>
{
    static std::string op(xyz::openbmc_project::User::Ldap::server::Create::Type value)
    {
        return xyz::openbmc_project::User::Ldap::server::Create::convertTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

