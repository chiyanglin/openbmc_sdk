#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Ldap
{
namespace client
{
namespace Config
{

static constexpr auto interface = "xyz.openbmc_project.User.Ldap.Config";

} // namespace Config
} // namespace client
} // namespace Ldap
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

