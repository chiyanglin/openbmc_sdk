#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace client
{
namespace PrivilegeMapperEntry
{

static constexpr auto interface = "xyz.openbmc_project.User.PrivilegeMapperEntry";

} // namespace PrivilegeMapperEntry
} // namespace client
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

