#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Ipmi
{
namespace client
{
namespace SOL
{

static constexpr auto interface = "xyz.openbmc_project.Ipmi.SOL";

} // namespace SOL
} // namespace client
} // namespace Ipmi
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

