#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Ipmi
{
namespace Internal
{
namespace server
{

class SoftPowerOff
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        SoftPowerOff() = delete;
        SoftPowerOff(const SoftPowerOff&) = delete;
        SoftPowerOff& operator=(const SoftPowerOff&) = delete;
        SoftPowerOff(SoftPowerOff&&) = delete;
        SoftPowerOff& operator=(SoftPowerOff&&) = delete;
        virtual ~SoftPowerOff() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        SoftPowerOff(bus_t& bus, const char* path);

        enum class HostResponse
        {
            NotApplicable,
            SoftOffReceived,
            HostShutdown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                HostResponse>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        SoftPowerOff(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ResponseReceived */
        virtual HostResponse responseReceived() const;
        /** Set value of ResponseReceived with option to skip sending signal */
        virtual HostResponse responseReceived(HostResponse value,
               bool skipSignal);
        /** Set value of ResponseReceived */
        virtual HostResponse responseReceived(HostResponse value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static HostResponse convertHostResponseFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<HostResponse> convertStringToHostResponse(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff.<value name>"
         */
        static std::string convertHostResponseToString(HostResponse e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Ipmi_Internal_SoftPowerOff_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Ipmi_Internal_SoftPowerOff_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff";

    private:

        /** @brief sd-bus callback for get-property 'ResponseReceived' */
        static int _callback_get_ResponseReceived(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ResponseReceived' */
        static int _callback_set_ResponseReceived(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Ipmi_Internal_SoftPowerOff_interface;
        sdbusplus::SdBusInterface *_intf;

        HostResponse _responseReceived = HostResponse::NotApplicable;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type SoftPowerOff::HostResponse.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(SoftPowerOff::HostResponse e)
{
    return SoftPowerOff::convertHostResponseToString(e);
}

} // namespace server
} // namespace Internal
} // namespace Ipmi
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Ipmi::Internal::server::SoftPowerOff::HostResponse>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Ipmi::Internal::server::SoftPowerOff::convertStringToHostResponse(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Ipmi::Internal::server::SoftPowerOff::HostResponse>
{
    static std::string op(xyz::openbmc_project::Ipmi::Internal::server::SoftPowerOff::HostResponse value)
    {
        return xyz::openbmc_project::Ipmi::Internal::server::SoftPowerOff::convertHostResponseToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

