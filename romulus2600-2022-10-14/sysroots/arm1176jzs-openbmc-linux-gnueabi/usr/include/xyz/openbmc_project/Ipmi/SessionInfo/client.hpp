#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Ipmi
{
namespace client
{
namespace SessionInfo
{

static constexpr auto interface = "xyz.openbmc_project.Ipmi.SessionInfo";

} // namespace SessionInfo
} // namespace client
} // namespace Ipmi
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

