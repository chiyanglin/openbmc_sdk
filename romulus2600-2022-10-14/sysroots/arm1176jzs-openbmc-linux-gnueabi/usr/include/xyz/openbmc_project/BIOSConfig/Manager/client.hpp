#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace client
{
namespace Manager
{

static constexpr auto interface = "xyz.openbmc_project.BIOSConfig.Manager";

} // namespace Manager
} // namespace client
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

