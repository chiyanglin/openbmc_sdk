#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace client
{
namespace Password
{

static constexpr auto interface = "xyz.openbmc_project.BIOSConfig.Password";

} // namespace Password
} // namespace client
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

