#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace Common
{
namespace Error
{

struct AttributeNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.BIOSConfig.Common.Error.AttributeNotFound";
    static constexpr auto errDesc =
            "Specified Attribute is not found.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.BIOSConfig.Common.Error.AttributeNotFound: Specified Attribute is not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct AttributeReadOnly final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.BIOSConfig.Common.Error.AttributeReadOnly";
    static constexpr auto errDesc =
            "Specified Attribute is ReadOnly.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.BIOSConfig.Common.Error.AttributeReadOnly: Specified Attribute is ReadOnly.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InvalidCurrentPassword final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.BIOSConfig.Common.Error.InvalidCurrentPassword";
    static constexpr auto errDesc =
            "CurrentPassword verification failed.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.BIOSConfig.Common.Error.InvalidCurrentPassword: CurrentPassword verification failed.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct PasswordNotSettable final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.BIOSConfig.Common.Error.PasswordNotSettable";
    static constexpr auto errDesc =
            "Unable to set the new password.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.BIOSConfig.Common.Error.PasswordNotSettable: Unable to set the new password.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Common
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

