#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace MCTP
{
namespace client
{
namespace Endpoint
{

static constexpr auto interface = "xyz.openbmc_project.MCTP.Endpoint";

} // namespace Endpoint
} // namespace client
} // namespace MCTP
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

