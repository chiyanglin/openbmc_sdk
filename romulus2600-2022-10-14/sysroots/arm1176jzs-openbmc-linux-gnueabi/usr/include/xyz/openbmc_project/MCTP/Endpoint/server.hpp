#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace MCTP
{
namespace server
{

class Endpoint
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Endpoint() = delete;
        Endpoint(const Endpoint&) = delete;
        Endpoint& operator=(const Endpoint&) = delete;
        Endpoint(Endpoint&&) = delete;
        Endpoint& operator=(Endpoint&&) = delete;
        virtual ~Endpoint() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Endpoint(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                size_t,
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Endpoint(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of NetworkId */
        virtual size_t networkId() const;
        /** Set value of NetworkId with option to skip sending signal */
        virtual size_t networkId(size_t value,
               bool skipSignal);
        /** Set value of NetworkId */
        virtual size_t networkId(size_t value);
        /** Get value of EID */
        virtual size_t eid() const;
        /** Set value of EID with option to skip sending signal */
        virtual size_t eid(size_t value,
               bool skipSignal);
        /** Set value of EID */
        virtual size_t eid(size_t value);
        /** Get value of SupportedMessageTypes */
        virtual std::vector<uint8_t> supportedMessageTypes() const;
        /** Set value of SupportedMessageTypes with option to skip sending signal */
        virtual std::vector<uint8_t> supportedMessageTypes(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of SupportedMessageTypes */
        virtual std::vector<uint8_t> supportedMessageTypes(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_MCTP_Endpoint_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_MCTP_Endpoint_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.MCTP.Endpoint";

    private:

        /** @brief sd-bus callback for get-property 'NetworkId' */
        static int _callback_get_NetworkId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NetworkId' */
        static int _callback_set_NetworkId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EID' */
        static int _callback_get_EID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EID' */
        static int _callback_set_EID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SupportedMessageTypes' */
        static int _callback_get_SupportedMessageTypes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SupportedMessageTypes' */
        static int _callback_set_SupportedMessageTypes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_MCTP_Endpoint_interface;
        sdbusplus::SdBusInterface *_intf;

        size_t _networkId{};
        size_t _eid{};
        std::vector<uint8_t> _supportedMessageTypes{};

};


} // namespace server
} // namespace MCTP
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

