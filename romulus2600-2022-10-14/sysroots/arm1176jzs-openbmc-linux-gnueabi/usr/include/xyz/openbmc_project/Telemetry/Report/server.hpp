#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>














#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

class Report
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Report() = delete;
        Report(const Report&) = delete;
        Report& operator=(const Report&) = delete;
        Report(Report&&) = delete;
        Report& operator=(Report&&) = delete;
        virtual ~Report() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Report(bus_t& bus, const char* path);

        enum class OperationType
        {
            Maximum,
            Minimum,
            Average,
            Summation,
        };
        enum class ReportingType
        {
            OnChange,
            OnRequest,
            Periodic,
        };
        enum class ReportUpdates
        {
            Overwrite,
            AppendWrapsWhenFull,
            AppendStopsWhenFull,
        };
        enum class CollectionTimescope
        {
            Point,
            Interval,
            StartupInterval,
        };
        enum class ErrorType
        {
            PropertyConflict,
        };
        enum class ReportActions
        {
            EmitsReadingsUpdate,
            LogToMetricReportsCollection,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                ReportUpdates,
                ReportingType,
                bool,
                size_t,
                std::string,
                std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>>,
                std::vector<ReportActions>,
                std::vector<sdbusplus::message::object_path>,
                std::vector<std::tuple<ErrorType, std::string>>,
                std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>>,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Report(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for Update
         *  The Update method is defined for the on demand metric report update. It triggers update of the Readings property. If ReportingType is not set to OnRequest then method does nothing.
         */
        virtual void update(
            ) = 0;


        /** Get value of Persistency */
        virtual bool persistency() const;
        /** Set value of Persistency with option to skip sending signal */
        virtual bool persistency(bool value,
               bool skipSignal);
        /** Set value of Persistency */
        virtual bool persistency(bool value);
        /** Get value of ReadingParameters */
        virtual std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> readingParameters() const;
        /** Set value of ReadingParameters with option to skip sending signal */
        virtual std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> readingParameters(std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> value,
               bool skipSignal);
        /** Set value of ReadingParameters */
        virtual std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> readingParameters(std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> value);
        /** Get value of Readings */
        virtual std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> readings() const;
        /** Set value of Readings with option to skip sending signal */
        virtual std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> readings(std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> value,
               bool skipSignal);
        /** Set value of Readings */
        virtual std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> readings(std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> value);
        /** Get value of ReportingType */
        virtual ReportingType reportingType() const;
        /** Set value of ReportingType with option to skip sending signal */
        virtual ReportingType reportingType(ReportingType value,
               bool skipSignal);
        /** Set value of ReportingType */
        virtual ReportingType reportingType(ReportingType value);
        /** Get value of ReportUpdates */
        virtual ReportUpdates reportUpdates() const;
        /** Set value of ReportUpdates with option to skip sending signal */
        virtual ReportUpdates reportUpdates(ReportUpdates value,
               bool skipSignal);
        /** Set value of ReportUpdates */
        virtual ReportUpdates reportUpdates(ReportUpdates value);
        /** Get value of AppendLimit */
        virtual size_t appendLimit() const;
        /** Set value of AppendLimit with option to skip sending signal */
        virtual size_t appendLimit(size_t value,
               bool skipSignal);
        /** Set value of AppendLimit */
        virtual size_t appendLimit(size_t value);
        /** Get value of Interval */
        virtual uint64_t interval() const;
        /** Set value of Interval with option to skip sending signal */
        virtual uint64_t interval(uint64_t value,
               bool skipSignal);
        /** Set value of Interval */
        virtual uint64_t interval(uint64_t value);
        /** Get value of Enabled */
        virtual bool enabled() const;
        /** Set value of Enabled with option to skip sending signal */
        virtual bool enabled(bool value,
               bool skipSignal);
        /** Set value of Enabled */
        virtual bool enabled(bool value);
        /** Get value of ErrorMesages */
        virtual std::vector<std::tuple<ErrorType, std::string>> errorMesages() const;
        /** Set value of ErrorMesages with option to skip sending signal */
        virtual std::vector<std::tuple<ErrorType, std::string>> errorMesages(std::vector<std::tuple<ErrorType, std::string>> value,
               bool skipSignal);
        /** Set value of ErrorMesages */
        virtual std::vector<std::tuple<ErrorType, std::string>> errorMesages(std::vector<std::tuple<ErrorType, std::string>> value);
        /** Get value of Name */
        virtual std::string name() const;
        /** Set value of Name with option to skip sending signal */
        virtual std::string name(std::string value,
               bool skipSignal);
        /** Set value of Name */
        virtual std::string name(std::string value);
        /** Get value of ReportActions */
        virtual std::vector<ReportActions> reportActions() const;
        /** Set value of ReportActions with option to skip sending signal */
        virtual std::vector<ReportActions> reportActions(std::vector<ReportActions> value,
               bool skipSignal);
        /** Set value of ReportActions */
        virtual std::vector<ReportActions> reportActions(std::vector<ReportActions> value);
        /** Get value of Triggers */
        virtual std::vector<sdbusplus::message::object_path> triggers() const;
        /** Set value of Triggers with option to skip sending signal */
        virtual std::vector<sdbusplus::message::object_path> triggers(std::vector<sdbusplus::message::object_path> value,
               bool skipSignal);
        /** Set value of Triggers */
        virtual std::vector<sdbusplus::message::object_path> triggers(std::vector<sdbusplus::message::object_path> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static OperationType convertOperationTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<OperationType> convertStringToOperationType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Report.<value name>"
         */
        static std::string convertOperationTypeToString(OperationType e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ReportingType convertReportingTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ReportingType> convertStringToReportingType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Report.<value name>"
         */
        static std::string convertReportingTypeToString(ReportingType e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ReportUpdates convertReportUpdatesFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ReportUpdates> convertStringToReportUpdates(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Report.<value name>"
         */
        static std::string convertReportUpdatesToString(ReportUpdates e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static CollectionTimescope convertCollectionTimescopeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<CollectionTimescope> convertStringToCollectionTimescope(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Report.<value name>"
         */
        static std::string convertCollectionTimescopeToString(CollectionTimescope e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ErrorType convertErrorTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ErrorType> convertStringToErrorType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Report.<value name>"
         */
        static std::string convertErrorTypeToString(ErrorType e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ReportActions convertReportActionsFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Report.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ReportActions> convertStringToReportActions(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Report.<value name>"
         */
        static std::string convertReportActionsToString(ReportActions e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Telemetry_Report_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Telemetry_Report_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Telemetry.Report";

    private:

        /** @brief sd-bus callback for Update
         */
        static int _callback_Update(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Persistency' */
        static int _callback_get_Persistency(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Persistency' */
        static int _callback_set_Persistency(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ReadingParameters' */
        static int _callback_get_ReadingParameters(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ReadingParameters' */
        static int _callback_set_ReadingParameters(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Readings' */
        static int _callback_get_Readings(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ReportingType' */
        static int _callback_get_ReportingType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ReportingType' */
        static int _callback_set_ReportingType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ReportUpdates' */
        static int _callback_get_ReportUpdates(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ReportUpdates' */
        static int _callback_set_ReportUpdates(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AppendLimit' */
        static int _callback_get_AppendLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Interval' */
        static int _callback_get_Interval(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Interval' */
        static int _callback_set_Interval(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Enabled' */
        static int _callback_get_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Enabled' */
        static int _callback_set_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ErrorMesages' */
        static int _callback_get_ErrorMesages(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Name' */
        static int _callback_get_Name(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ReportActions' */
        static int _callback_get_ReportActions(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ReportActions' */
        static int _callback_set_ReportActions(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Triggers' */
        static int _callback_get_Triggers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Telemetry_Report_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _persistency{};
        std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> _readingParameters{};
        std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> _readings{};
        ReportingType _reportingType{};
        ReportUpdates _reportUpdates{};
        size_t _appendLimit{};
        uint64_t _interval{};
        bool _enabled{};
        std::vector<std::tuple<ErrorType, std::string>> _errorMesages{};
        std::string _name{};
        std::vector<ReportActions> _reportActions{};
        std::vector<sdbusplus::message::object_path> _triggers{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Report::OperationType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Report::OperationType e)
{
    return Report::convertOperationTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Report::ReportingType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Report::ReportingType e)
{
    return Report::convertReportingTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Report::ReportUpdates.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Report::ReportUpdates e)
{
    return Report::convertReportUpdatesToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Report::CollectionTimescope.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Report::CollectionTimescope e)
{
    return Report::convertCollectionTimescopeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Report::ErrorType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Report::ErrorType e)
{
    return Report::convertErrorTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Report::ReportActions.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Report::ReportActions e)
{
    return Report::convertReportActionsToString(e);
}

} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Report::OperationType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertStringToOperationType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Report::OperationType>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Report::OperationType value)
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertOperationTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Report::ReportingType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertStringToReportingType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Report::ReportingType>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Report::ReportingType value)
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertReportingTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Report::ReportUpdates>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertStringToReportUpdates(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Report::ReportUpdates>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Report::ReportUpdates value)
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertReportUpdatesToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertStringToCollectionTimescope(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope value)
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertCollectionTimescopeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Report::ErrorType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertStringToErrorType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Report::ErrorType>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Report::ErrorType value)
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertErrorTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Report::ReportActions>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertStringToReportActions(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Report::ReportActions>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Report::ReportActions value)
    {
        return xyz::openbmc_project::Telemetry::server::Report::convertReportActionsToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

