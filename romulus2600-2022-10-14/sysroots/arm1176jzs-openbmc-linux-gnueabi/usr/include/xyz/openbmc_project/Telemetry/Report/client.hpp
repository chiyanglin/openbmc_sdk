#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace client
{
namespace Report
{

static constexpr auto interface = "xyz.openbmc_project.Telemetry.Report";

} // namespace Report
} // namespace client
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

