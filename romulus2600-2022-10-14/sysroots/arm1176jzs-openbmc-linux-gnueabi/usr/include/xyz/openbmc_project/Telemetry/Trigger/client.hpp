#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace client
{
namespace Trigger
{

static constexpr auto interface = "xyz.openbmc_project.Telemetry.Trigger";

} // namespace Trigger
} // namespace client
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

