#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace client
{
namespace TriggerManager
{

static constexpr auto interface = "xyz.openbmc_project.Telemetry.TriggerManager";

} // namespace TriggerManager
} // namespace client
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

