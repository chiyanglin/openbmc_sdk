#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#include <xyz/openbmc_project/Telemetry/Report/server.hpp>



#include <xyz/openbmc_project/Telemetry/Report/server.hpp>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

class ReportManager
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        ReportManager() = delete;
        ReportManager(const ReportManager&) = delete;
        ReportManager& operator=(const ReportManager&) = delete;
        ReportManager(ReportManager&&) = delete;
        ReportManager& operator=(ReportManager&&) = delete;
        virtual ~ReportManager() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        ReportManager(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                size_t,
                std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType>,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        ReportManager(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for AddReport
         *  Create new object that represents Report with xyz.openbmc_project.Telemetry.Report interface stored in path /xyz/openbmc_project/Telemetry/Reports/{properties[Id]} where properties[Id] is passed into this method as a parameter.
         *
         *  @param[in] properties - Map of properties defining added report object. Key is the string identifier of property. Value is a variant, whose underlying type is dependent from the key. Possible key values are: Id, Name, ReportingType, ReportActions, Interval, AppendLimit, ReportUpdates, MetricParams, Enabled. All keys are optional, therefore it is acceptable to pass an empty map here.
Id (string) - unique identifier of created Report object to be exposed over D-Bus. Acceptable formats are: "{Id}", "{Prefix}/{SubId}", "{Prefix}/". If the last variant is used, service will generate unique SubId value by itself.
Name (string) - user friendly name of created Report object.
ReportingType (enum[ xyz.openbmc_project.Telemetry.Report.ReportingType]) - indicates when readings should be updated. Possible values are listed in Report interface documentation.
ReportActions (array[enum[ xyz.openbmc_project.Telemetry.Report.ReportActions]]) - possible additional actions to trigger after readings' update, as specified in Report interface documentation.
Interval (uint64) - period of time in milliseconds when Report is updated with new readings. Minimal interval is defined in MinInterval property.
AppendLimit (size) - maximum number of entries in 'Readings' property.
ReportUpdates (enum[ xyz.openbmc_project.Telemetry.Report.ReportUpdates]) - indicates how readings should be updated. Possible values are listed in Report interface documentation.
MetricParams (array[struct[array[struct[object_path,string]], enum[xyz.openbmc_project.Telemetry.Report.OperationType],string, enum[xyz.openbmc_project.Telemetry.Report.CollectionTimescope], uint64]]) - array of metric parameters, which are stored in a way specified in description of ReadingParameters property from Report interface documentation.
Enabled (boolean) - indicates if readings in report will be updated.
         *
         *  @return reportPath[sdbusplus::message::object_path] - Path to new report ->
  /xyz/openbmc_project/Telemetry/Reports/{parameters[Id]}.
         */
        virtual sdbusplus::message::object_path addReport(
            std::vector<std::map<std::string, std::variant<bool, uint32_t, uint64_t, std::string, xyz::openbmc_project::Telemetry::server::Report::ReportingType, xyz::openbmc_project::Telemetry::server::Report::ReportUpdates, std::vector<xyz::openbmc_project::Telemetry::server::Report::ReportActions>, std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, xyz::openbmc_project::Telemetry::server::Report::OperationType, std::string, xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope, uint64_t>>>>> properties) = 0;


        /** Get value of MaxReports */
        virtual size_t maxReports() const;
        /** Set value of MaxReports with option to skip sending signal */
        virtual size_t maxReports(size_t value,
               bool skipSignal);
        /** Set value of MaxReports */
        virtual size_t maxReports(size_t value);
        /** Get value of MinInterval */
        virtual uint64_t minInterval() const;
        /** Set value of MinInterval with option to skip sending signal */
        virtual uint64_t minInterval(uint64_t value,
               bool skipSignal);
        /** Set value of MinInterval */
        virtual uint64_t minInterval(uint64_t value);
        /** Get value of SupportedOperationTypes */
        virtual std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> supportedOperationTypes() const;
        /** Set value of SupportedOperationTypes with option to skip sending signal */
        virtual std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> supportedOperationTypes(std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> value,
               bool skipSignal);
        /** Set value of SupportedOperationTypes */
        virtual std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> supportedOperationTypes(std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Telemetry_ReportManager_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Telemetry_ReportManager_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Telemetry.ReportManager";

    private:

        /** @brief sd-bus callback for AddReport
         */
        static int _callback_AddReport(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxReports' */
        static int _callback_get_MaxReports(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MinInterval' */
        static int _callback_get_MinInterval(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SupportedOperationTypes' */
        static int _callback_get_SupportedOperationTypes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Telemetry_ReportManager_interface;
        sdbusplus::SdBusInterface *_intf;

        size_t _maxReports{};
        uint64_t _minInterval{};
        std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> _supportedOperationTypes{};

};


} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

