#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace client
{
namespace ReportManager
{

static constexpr auto interface = "xyz.openbmc_project.Telemetry.ReportManager";

} // namespace ReportManager
} // namespace client
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

