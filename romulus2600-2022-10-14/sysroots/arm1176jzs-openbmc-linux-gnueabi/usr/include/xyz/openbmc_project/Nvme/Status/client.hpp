#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Nvme
{
namespace client
{
namespace Status
{

static constexpr auto interface = "xyz.openbmc_project.Nvme.Status";

} // namespace Status
} // namespace client
} // namespace Nvme
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

