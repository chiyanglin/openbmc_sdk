#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace client
{
namespace PDR
{

static constexpr auto interface = "xyz.openbmc_project.PLDM.PDR";

} // namespace PDR
} // namespace client
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

