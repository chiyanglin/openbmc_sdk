#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace Provider
{
namespace Certs
{
namespace Authority
{
namespace client
{
namespace CSR
{

static constexpr auto interface = "xyz.openbmc_project.PLDM.Provider.Certs.Authority.CSR";

} // namespace CSR
} // namespace client
} // namespace Authority
} // namespace Certs
} // namespace Provider
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

