#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace client
{
namespace Requester
{

static constexpr auto interface = "xyz.openbmc_project.PLDM.Requester";

} // namespace Requester
} // namespace client
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

