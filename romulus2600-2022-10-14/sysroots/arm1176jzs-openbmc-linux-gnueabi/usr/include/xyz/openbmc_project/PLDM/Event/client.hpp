#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace client
{
namespace Event
{

static constexpr auto interface = "xyz.openbmc_project.PLDM.Event";

} // namespace Event
} // namespace client
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

