#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Syslog
{
namespace Destination
{
namespace client
{
namespace Mail
{

static constexpr auto interface = "xyz.openbmc_project.Logging.Syslog.Destination.Mail";

} // namespace Mail
} // namespace client
} // namespace Destination
} // namespace Syslog
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

