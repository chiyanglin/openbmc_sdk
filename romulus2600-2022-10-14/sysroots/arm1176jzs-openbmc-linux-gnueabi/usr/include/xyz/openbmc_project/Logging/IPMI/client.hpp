#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace client
{
namespace IPMI
{

static constexpr auto interface = "xyz.openbmc_project.Logging.IPMI";

} // namespace IPMI
} // namespace client
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

