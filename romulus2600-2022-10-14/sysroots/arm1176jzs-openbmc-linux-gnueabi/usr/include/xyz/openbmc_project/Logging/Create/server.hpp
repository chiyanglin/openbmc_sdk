#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#include <xyz/openbmc_project/Logging/Entry/server.hpp>

#include <xyz/openbmc_project/Logging/Entry/server.hpp>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);

        enum class FFDCFormat
        {
            JSON,
            CBOR,
            Text,
            Custom,
        };


        /** @brief Implementation for Create
         *  Create a xyz.openbmc_project.Logging.Entry object.
         *
         *  @param[in] message - The Message property of the event entry.
         *  @param[in] severity - The Severity property of the event entry.
         *  @param[in] additionalData - The AdditionalData property of the event entry. e.g.:
  {
    "key1": "value1",
    "key2": "value2"
  }
ends up in AdditionaData like:
  ["KEY1=value1", "KEY2=value2"]
         */
        virtual void create(
            std::string message,
            xyz::openbmc_project::Logging::server::Entry::Level severity,
            std::map<std::string, std::string> additionalData) = 0;

        /** @brief Implementation for CreateWithFFDCFiles
         *  Create an xyz.openbmc_project.Logging.Entry object and pass in an array of file descriptors for files that contain FFDC (first failure data capture) data which may be used by event log extensions that support storing it with their event logs.  The other arguments are the same as with Create().  The FFDC argument is ignored by the base phosphor-logging event logs.
When the method call is complete the descriptors must be closed and the files can be deleted if desired.
         *
         *  @param[in] message - The Message property of the event entry.
         *  @param[in] severity - The Severity property of the event entry.
         *  @param[in] additionalData - The AdditionalData property of the event entry. e.g.:
  {
    "key1": "value1",
    "key2": "value2"
  }
ends up in AdditionaData like:
  ["KEY1=value1", "KEY2=value2"]
         *  @param[in] ffdc - File descriptors for any files containing FFDC, along with metadata about the contents:

  FFDCFormat- The format type of the contained data.
  subType - The format subtype, used for the 'Custom' type.
  version - The version of the data format, used for the 'Custom'
            type.
  unixfd - The file descriptor to the data file.

e.g.: [
  {"xyz.openbmc_project.Logging.Create.FFDCFormat.JSON", 0, 0, 5},
  {"xyz.openbmc_project.Logging.Create.FFDCFormat.Custom", 1, 2, 6}
]
         */
        virtual void createWithFFDCFiles(
            std::string message,
            xyz::openbmc_project::Logging::server::Entry::Level severity,
            std::map<std::string, std::string> additionalData,
            std::vector<std::tuple<FFDCFormat, uint8_t, uint8_t, sdbusplus::message::unix_fd>> ffdc) = 0;



        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Logging.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static FFDCFormat convertFFDCFormatFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Logging.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<FFDCFormat> convertStringToFFDCFormat(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Logging.Create.<value name>"
         */
        static std::string convertFFDCFormatToString(FFDCFormat e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Logging_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Logging_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Logging.Create";

    private:

        /** @brief sd-bus callback for Create
         */
        static int _callback_Create(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for CreateWithFFDCFiles
         */
        static int _callback_CreateWithFFDCFiles(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Logging_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::FFDCFormat.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::FFDCFormat e)
{
    return Create::convertFFDCFormatToString(e);
}

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Logging::server::Create::FFDCFormat>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Logging::server::Create::convertStringToFFDCFormat(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Logging::server::Create::FFDCFormat>
{
    static std::string op(xyz::openbmc_project::Logging::server::Create::FFDCFormat value)
    {
        return xyz::openbmc_project::Logging::server::Create::convertFFDCFormatToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

