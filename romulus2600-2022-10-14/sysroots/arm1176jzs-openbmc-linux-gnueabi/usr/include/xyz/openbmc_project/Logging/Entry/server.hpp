#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>












#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

class Entry
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Entry() = delete;
        Entry(const Entry&) = delete;
        Entry& operator=(const Entry&) = delete;
        Entry(Entry&&) = delete;
        Entry& operator=(Entry&&) = delete;
        virtual ~Entry() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Entry(bus_t& bus, const char* path);

        enum class Level
        {
            Emergency,
            Alert,
            Critical,
            Error,
            Warning,
            Notice,
            Informational,
            Debug,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Level,
                bool,
                std::string,
                std::vector<std::string>,
                uint32_t,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Entry(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for GetEntry
         *  Returns the file descriptor to the raw Entry file, which is a binary blob. The mode of the file descriptor is to be reaad-only.
         *
         *  @return fd[sdbusplus::message::unix_fd] - The file descriptor to the Entry file.
         */
        virtual sdbusplus::message::unix_fd getEntry(
            ) = 0;


        /** Get value of Id */
        virtual uint32_t id() const;
        /** Set value of Id with option to skip sending signal */
        virtual uint32_t id(uint32_t value,
               bool skipSignal);
        /** Set value of Id */
        virtual uint32_t id(uint32_t value);
        /** Get value of Timestamp */
        virtual uint64_t timestamp() const;
        /** Set value of Timestamp with option to skip sending signal */
        virtual uint64_t timestamp(uint64_t value,
               bool skipSignal);
        /** Set value of Timestamp */
        virtual uint64_t timestamp(uint64_t value);
        /** Get value of Severity */
        virtual Level severity() const;
        /** Set value of Severity with option to skip sending signal */
        virtual Level severity(Level value,
               bool skipSignal);
        /** Set value of Severity */
        virtual Level severity(Level value);
        /** Get value of Message */
        virtual std::string message() const;
        /** Set value of Message with option to skip sending signal */
        virtual std::string message(std::string value,
               bool skipSignal);
        /** Set value of Message */
        virtual std::string message(std::string value);
        /** Get value of EventId */
        virtual std::string eventId() const;
        /** Set value of EventId with option to skip sending signal */
        virtual std::string eventId(std::string value,
               bool skipSignal);
        /** Set value of EventId */
        virtual std::string eventId(std::string value);
        /** Get value of AdditionalData */
        virtual std::vector<std::string> additionalData() const;
        /** Set value of AdditionalData with option to skip sending signal */
        virtual std::vector<std::string> additionalData(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of AdditionalData */
        virtual std::vector<std::string> additionalData(std::vector<std::string> value);
        /** Get value of Resolution */
        virtual std::string resolution() const;
        /** Set value of Resolution with option to skip sending signal */
        virtual std::string resolution(std::string value,
               bool skipSignal);
        /** Set value of Resolution */
        virtual std::string resolution(std::string value);
        /** Get value of Resolved */
        virtual bool resolved() const;
        /** Set value of Resolved with option to skip sending signal */
        virtual bool resolved(bool value,
               bool skipSignal);
        /** Set value of Resolved */
        virtual bool resolved(bool value);
        /** Get value of ServiceProviderNotify */
        virtual bool serviceProviderNotify() const;
        /** Set value of ServiceProviderNotify with option to skip sending signal */
        virtual bool serviceProviderNotify(bool value,
               bool skipSignal);
        /** Set value of ServiceProviderNotify */
        virtual bool serviceProviderNotify(bool value);
        /** Get value of UpdateTimestamp */
        virtual uint64_t updateTimestamp() const;
        /** Set value of UpdateTimestamp with option to skip sending signal */
        virtual uint64_t updateTimestamp(uint64_t value,
               bool skipSignal);
        /** Set value of UpdateTimestamp */
        virtual uint64_t updateTimestamp(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Logging.Entry.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Level convertLevelFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Logging.Entry.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Level> convertStringToLevel(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Logging.Entry.<value name>"
         */
        static std::string convertLevelToString(Level e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Logging_Entry_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Logging_Entry_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Logging.Entry";

    private:

        /** @brief sd-bus callback for GetEntry
         */
        static int _callback_GetEntry(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Id' */
        static int _callback_get_Id(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Id' */
        static int _callback_set_Id(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Timestamp' */
        static int _callback_get_Timestamp(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Timestamp' */
        static int _callback_set_Timestamp(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Severity' */
        static int _callback_get_Severity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Severity' */
        static int _callback_set_Severity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Message' */
        static int _callback_get_Message(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Message' */
        static int _callback_set_Message(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EventId' */
        static int _callback_get_EventId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EventId' */
        static int _callback_set_EventId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AdditionalData' */
        static int _callback_get_AdditionalData(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AdditionalData' */
        static int _callback_set_AdditionalData(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Resolution' */
        static int _callback_get_Resolution(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Resolution' */
        static int _callback_set_Resolution(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Resolved' */
        static int _callback_get_Resolved(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Resolved' */
        static int _callback_set_Resolved(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ServiceProviderNotify' */
        static int _callback_get_ServiceProviderNotify(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ServiceProviderNotify' */
        static int _callback_set_ServiceProviderNotify(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UpdateTimestamp' */
        static int _callback_get_UpdateTimestamp(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UpdateTimestamp' */
        static int _callback_set_UpdateTimestamp(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Logging_Entry_interface;
        sdbusplus::SdBusInterface *_intf;

        uint32_t _id{};
        uint64_t _timestamp{};
        Level _severity{};
        std::string _message{};
        std::string _eventId{};
        std::vector<std::string> _additionalData{};
        std::string _resolution{};
        bool _resolved{};
        bool _serviceProviderNotify = false;
        uint64_t _updateTimestamp{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Entry::Level.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Entry::Level e)
{
    return Entry::convertLevelToString(e);
}

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Logging::server::Entry::Level>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Logging::server::Entry::convertStringToLevel(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Logging::server::Entry::Level>
{
    static std::string op(xyz::openbmc_project::Logging::server::Entry::Level value)
    {
        return xyz::openbmc_project::Logging::server::Entry::convertLevelToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

