#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace client
{
namespace ErrorBlocksTransition
{

static constexpr auto interface = "xyz.openbmc_project.Logging.ErrorBlocksTransition";

} // namespace ErrorBlocksTransition
} // namespace client
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

