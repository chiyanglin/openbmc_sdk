#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Syslog
{
namespace Destination
{
namespace Mail
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.Logging.Syslog.Destination.Mail.Create";

} // namespace Create
} // namespace client
} // namespace Mail
} // namespace Destination
} // namespace Syslog
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

