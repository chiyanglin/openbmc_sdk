#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace client
{
namespace Create
{

static constexpr auto interface = "xyz.openbmc_project.Logging.Create";

} // namespace Create
} // namespace client
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

