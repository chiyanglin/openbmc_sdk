#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace SEL
{
namespace Error
{

struct Created final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Logging.SEL.Error.Created";
    static constexpr auto errDesc =
            "A System Event Log is created";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Logging.SEL.Error.Created: A System Event Log is created";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace SEL
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

