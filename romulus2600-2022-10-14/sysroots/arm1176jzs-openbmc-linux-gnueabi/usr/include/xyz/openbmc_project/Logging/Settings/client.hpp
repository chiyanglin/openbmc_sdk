#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace client
{
namespace Settings
{

static constexpr auto interface = "xyz.openbmc_project.Logging.Settings";

} // namespace Settings
} // namespace client
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

