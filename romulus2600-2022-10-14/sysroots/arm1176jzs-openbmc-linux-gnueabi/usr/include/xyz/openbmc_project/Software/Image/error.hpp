#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace Image
{
namespace Error
{

struct UnTarFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Image.Error.UnTarFailure";
    static constexpr auto errDesc =
            "An error occurred during untar.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Image.Error.UnTarFailure: An error occurred during untar.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct ManifestFileFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Image.Error.ManifestFileFailure";
    static constexpr auto errDesc =
            "An error when reading the Manifest file.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Image.Error.ManifestFileFailure: An error when reading the Manifest file.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InternalFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Image.Error.InternalFailure";
    static constexpr auto errDesc =
            "The operation failed internally during processing the image.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Image.Error.InternalFailure: The operation failed internally during processing the image.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct ImageFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Image.Error.ImageFailure";
    static constexpr auto errDesc =
            "An error occured processing the image.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Image.Error.ImageFailure: An error occured processing the image.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct BusyFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Image.Error.BusyFailure";
    static constexpr auto errDesc =
            "The device is busy during the update.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Image.Error.BusyFailure: The device is busy during the update.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Image
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

