#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace ActivationBlocksTransition
{

static constexpr auto interface = "xyz.openbmc_project.Software.ActivationBlocksTransition";

} // namespace ActivationBlocksTransition
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

