#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace ActivationProgress
{

static constexpr auto interface = "xyz.openbmc_project.Software.ActivationProgress";

} // namespace ActivationProgress
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

