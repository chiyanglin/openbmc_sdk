#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

class Activation
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Activation() = delete;
        Activation(const Activation&) = delete;
        Activation& operator=(const Activation&) = delete;
        Activation(Activation&&) = delete;
        Activation& operator=(Activation&&) = delete;
        virtual ~Activation() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Activation(bus_t& bus, const char* path);

        enum class Activations
        {
            NotReady,
            Invalid,
            Ready,
            Activating,
            Active,
            Failed,
            Staged,
            Staging,
        };
        enum class RequestedActivations
        {
            None,
            Active,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Activations,
                RequestedActivations>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Activation(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Activation */
        virtual Activations activation() const;
        /** Set value of Activation with option to skip sending signal */
        virtual Activations activation(Activations value,
               bool skipSignal);
        /** Set value of Activation */
        virtual Activations activation(Activations value);
        /** Get value of RequestedActivation */
        virtual RequestedActivations requestedActivation() const;
        /** Set value of RequestedActivation with option to skip sending signal */
        virtual RequestedActivations requestedActivation(RequestedActivations value,
               bool skipSignal);
        /** Set value of RequestedActivation */
        virtual RequestedActivations requestedActivation(RequestedActivations value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.Activation.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Activations convertActivationsFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.Activation.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Activations> convertStringToActivations(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Software.Activation.<value name>"
         */
        static std::string convertActivationsToString(Activations e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.Activation.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static RequestedActivations convertRequestedActivationsFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.Activation.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<RequestedActivations> convertStringToRequestedActivations(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Software.Activation.<value name>"
         */
        static std::string convertRequestedActivationsToString(RequestedActivations e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Software_Activation_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Software_Activation_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Software.Activation";

    private:

        /** @brief sd-bus callback for get-property 'Activation' */
        static int _callback_get_Activation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Activation' */
        static int _callback_set_Activation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RequestedActivation' */
        static int _callback_get_RequestedActivation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RequestedActivation' */
        static int _callback_set_RequestedActivation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Software_Activation_interface;
        sdbusplus::SdBusInterface *_intf;

        Activations _activation{};
        RequestedActivations _requestedActivation{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Activation::Activations.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Activation::Activations e)
{
    return Activation::convertActivationsToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Activation::RequestedActivations.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Activation::RequestedActivations e)
{
    return Activation::convertRequestedActivationsToString(e);
}

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Software::server::Activation::Activations>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Software::server::Activation::convertStringToActivations(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Software::server::Activation::Activations>
{
    static std::string op(xyz::openbmc_project::Software::server::Activation::Activations value)
    {
        return xyz::openbmc_project::Software::server::Activation::convertActivationsToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Software::server::Activation::RequestedActivations>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Software::server::Activation::convertStringToRequestedActivations(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Software::server::Activation::RequestedActivations>
{
    static std::string op(xyz::openbmc_project::Software::server::Activation::RequestedActivations value)
    {
        return xyz::openbmc_project::Software::server::Activation::convertRequestedActivationsToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

