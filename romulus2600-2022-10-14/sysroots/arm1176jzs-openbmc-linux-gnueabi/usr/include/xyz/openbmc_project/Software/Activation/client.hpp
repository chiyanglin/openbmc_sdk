#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace Activation
{

static constexpr auto interface = "xyz.openbmc_project.Software.Activation";

} // namespace Activation
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

