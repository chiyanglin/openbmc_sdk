#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace RedundancyPriority
{

static constexpr auto interface = "xyz.openbmc_project.Software.RedundancyPriority";

} // namespace RedundancyPriority
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

