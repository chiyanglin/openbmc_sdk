#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace Version
{

static constexpr auto interface = "xyz.openbmc_project.Software.Version";

} // namespace Version
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

