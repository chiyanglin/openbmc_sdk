#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace RequestedRedundancyPriority
{

static constexpr auto interface = "xyz.openbmc_project.Software.RequestedRedundancyPriority";

} // namespace RequestedRedundancyPriority
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

