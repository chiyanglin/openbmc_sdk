#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace ExtendedVersion
{

static constexpr auto interface = "xyz.openbmc_project.Software.ExtendedVersion";

} // namespace ExtendedVersion
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

