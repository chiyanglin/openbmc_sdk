#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace ApplyTime
{

static constexpr auto interface = "xyz.openbmc_project.Software.ApplyTime";

} // namespace ApplyTime
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

