#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace client
{
namespace ApplyOptions
{

static constexpr auto interface = "xyz.openbmc_project.Software.ApplyOptions";

} // namespace ApplyOptions
} // namespace client
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

