#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

class ActivationBlocksTransition
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        ActivationBlocksTransition() = delete;
        ActivationBlocksTransition(const ActivationBlocksTransition&) = delete;
        ActivationBlocksTransition& operator=(const ActivationBlocksTransition&) = delete;
        ActivationBlocksTransition(ActivationBlocksTransition&&) = delete;
        ActivationBlocksTransition& operator=(ActivationBlocksTransition&&) = delete;
        virtual ~ActivationBlocksTransition() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        ActivationBlocksTransition(bus_t& bus, const char* path);






        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Software_ActivationBlocksTransition_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Software_ActivationBlocksTransition_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Software.ActivationBlocksTransition";

    private:


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Software_ActivationBlocksTransition_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

