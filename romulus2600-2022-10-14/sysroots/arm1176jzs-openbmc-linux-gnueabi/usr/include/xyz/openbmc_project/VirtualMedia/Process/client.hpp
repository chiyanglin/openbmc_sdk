#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace client
{
namespace Process
{

static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Process";

} // namespace Process
} // namespace client
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

