#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace client
{
namespace Proxy
{

static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Proxy";

} // namespace Proxy
} // namespace client
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

