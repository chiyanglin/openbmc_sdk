#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace client
{
namespace Stats
{

static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Stats";

} // namespace Stats
} // namespace client
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

