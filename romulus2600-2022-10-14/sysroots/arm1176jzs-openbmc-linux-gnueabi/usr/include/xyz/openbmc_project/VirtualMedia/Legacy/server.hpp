#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

class Legacy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Legacy() = delete;
        Legacy(const Legacy&) = delete;
        Legacy& operator=(const Legacy&) = delete;
        Legacy(Legacy&&) = delete;
        Legacy& operator=(Legacy&&) = delete;
        virtual ~Legacy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Legacy(bus_t& bus, const char* path);



        /** @brief Implementation for Mount
         *  Perform an asynchronous operation of mounting to HOST on given object.
         *
         *  @param[in] imageURL - Url to image. It should start with either `smb://` or `https://` prefix
         *  @param[in] readWrite - False if the image should be read-only.
         *  @param[in] fileDescriptor - File descriptor of named pipe used for passing null-delimited secret data (username and password). When there is no data to pass `-1` should be passed as `INT`.
         *
         *  @return status[bool] - mounting status. True on success.
         */
        virtual bool mount(
            std::string imageURL,
            bool readWrite,
            std::variant<int32_t, sdbusplus::message::unix_fd> fileDescriptor) = 0;

        /** @brief Implementation for Unmount
         *  Perform an asynchronous operation of unmounting from HOST on given object.
         *
         *  @return status[bool] - the unmount status. True on success.
         */
        virtual bool unmount(
            ) = 0;


        /** @brief Send signal 'Completion'
         *
         *  Signal indicating completion of mount or unmount action.
         *
         *  @param[in] result - Returns 0 for success or errno on failure after background operation completes.
         */
        void completion(
            int32_t result);



        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_VirtualMedia_Legacy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_VirtualMedia_Legacy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Legacy";

    private:

        /** @brief sd-bus callback for Mount
         */
        static int _callback_Mount(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for Unmount
         */
        static int _callback_Unmount(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_VirtualMedia_Legacy_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

