#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace client
{
namespace MountPoint
{

static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.MountPoint";

} // namespace MountPoint
} // namespace client
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

