#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>










#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

class MountPoint
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        MountPoint() = delete;
        MountPoint(const MountPoint&) = delete;
        MountPoint& operator=(const MountPoint&) = delete;
        MountPoint(MountPoint&&) = delete;
        MountPoint& operator=(MountPoint&&) = delete;
        virtual ~MountPoint() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        MountPoint(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string,
                uint16_t,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        MountPoint(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of EndPointId */
        virtual std::string endPointId() const;
        /** Set value of EndPointId with option to skip sending signal */
        virtual std::string endPointId(std::string value,
               bool skipSignal);
        /** Set value of EndPointId */
        virtual std::string endPointId(std::string value);
        /** Get value of Mode */
        virtual uint8_t mode() const;
        /** Set value of Mode with option to skip sending signal */
        virtual uint8_t mode(uint8_t value,
               bool skipSignal);
        /** Set value of Mode */
        virtual uint8_t mode(uint8_t value);
        /** Get value of Device */
        virtual std::string device() const;
        /** Set value of Device with option to skip sending signal */
        virtual std::string device(std::string value,
               bool skipSignal);
        /** Set value of Device */
        virtual std::string device(std::string value);
        /** Get value of Socket */
        virtual std::string socket() const;
        /** Set value of Socket with option to skip sending signal */
        virtual std::string socket(std::string value,
               bool skipSignal);
        /** Set value of Socket */
        virtual std::string socket(std::string value);
        /** Get value of Timeout */
        virtual uint16_t timeout() const;
        /** Set value of Timeout with option to skip sending signal */
        virtual uint16_t timeout(uint16_t value,
               bool skipSignal);
        /** Set value of Timeout */
        virtual uint16_t timeout(uint16_t value);
        /** Get value of BlockSize */
        virtual uint16_t blockSize() const;
        /** Set value of BlockSize with option to skip sending signal */
        virtual uint16_t blockSize(uint16_t value,
               bool skipSignal);
        /** Set value of BlockSize */
        virtual uint16_t blockSize(uint16_t value);
        /** Get value of RemainingInactivityTimeout */
        virtual uint16_t remainingInactivityTimeout() const;
        /** Set value of RemainingInactivityTimeout with option to skip sending signal */
        virtual uint16_t remainingInactivityTimeout(uint16_t value,
               bool skipSignal);
        /** Set value of RemainingInactivityTimeout */
        virtual uint16_t remainingInactivityTimeout(uint16_t value);
        /** Get value of ImageURL */
        virtual std::string imageURL() const;
        /** Set value of ImageURL with option to skip sending signal */
        virtual std::string imageURL(std::string value,
               bool skipSignal);
        /** Set value of ImageURL */
        virtual std::string imageURL(std::string value);
        /** Get value of WriteProtected */
        virtual bool writeProtected() const;
        /** Set value of WriteProtected with option to skip sending signal */
        virtual bool writeProtected(bool value,
               bool skipSignal);
        /** Set value of WriteProtected */
        virtual bool writeProtected(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.MountPoint";

    private:

        /** @brief sd-bus callback for get-property 'EndPointId' */
        static int _callback_get_EndPointId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Mode' */
        static int _callback_get_Mode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Device' */
        static int _callback_get_Device(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Socket' */
        static int _callback_get_Socket(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Timeout' */
        static int _callback_get_Timeout(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'BlockSize' */
        static int _callback_get_BlockSize(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RemainingInactivityTimeout' */
        static int _callback_get_RemainingInactivityTimeout(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ImageURL' */
        static int _callback_get_ImageURL(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WriteProtected' */
        static int _callback_get_WriteProtected(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_VirtualMedia_MountPoint_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _endPointId{};
        uint8_t _mode{};
        std::string _device{};
        std::string _socket{};
        uint16_t _timeout{};
        uint16_t _blockSize{};
        uint16_t _remainingInactivityTimeout{};
        std::string _imageURL{};
        bool _writeProtected{};

};


} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

