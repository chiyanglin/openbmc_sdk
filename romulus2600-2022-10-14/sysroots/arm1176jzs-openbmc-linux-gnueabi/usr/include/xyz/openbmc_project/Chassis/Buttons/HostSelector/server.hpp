#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace server
{

class HostSelector
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        HostSelector() = delete;
        HostSelector(const HostSelector&) = delete;
        HostSelector& operator=(const HostSelector&) = delete;
        HostSelector(HostSelector&&) = delete;
        HostSelector& operator=(HostSelector&&) = delete;
        virtual ~HostSelector() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        HostSelector(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                size_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        HostSelector(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Position */
        virtual size_t position() const;
        /** Set value of Position with option to skip sending signal */
        virtual size_t position(size_t value,
               bool skipSignal);
        /** Set value of Position */
        virtual size_t position(size_t value);
        /** Get value of MaxPosition */
        virtual size_t maxPosition() const;
        /** Set value of MaxPosition with option to skip sending signal */
        virtual size_t maxPosition(size_t value,
               bool skipSignal);
        /** Set value of MaxPosition */
        virtual size_t maxPosition(size_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Chassis_Buttons_HostSelector_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Chassis_Buttons_HostSelector_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Chassis.Buttons.HostSelector";

    private:

        /** @brief sd-bus callback for get-property 'Position' */
        static int _callback_get_Position(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Position' */
        static int _callback_set_Position(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxPosition' */
        static int _callback_get_MaxPosition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Chassis_Buttons_HostSelector_interface;
        sdbusplus::SdBusInterface *_intf;

        size_t _position = 0;
        size_t _maxPosition = 0;

};


} // namespace server
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

