#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace client
{
namespace Intrusion
{

static constexpr auto interface = "xyz.openbmc_project.Chassis.Intrusion";

} // namespace Intrusion
} // namespace client
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

