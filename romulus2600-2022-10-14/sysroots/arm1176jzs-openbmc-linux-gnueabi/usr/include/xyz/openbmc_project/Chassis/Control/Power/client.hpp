#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Control
{
namespace client
{
namespace Power
{

static constexpr auto interface = "xyz.openbmc_project.Chassis.Control.Power";

} // namespace Power
} // namespace client
} // namespace Control
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

