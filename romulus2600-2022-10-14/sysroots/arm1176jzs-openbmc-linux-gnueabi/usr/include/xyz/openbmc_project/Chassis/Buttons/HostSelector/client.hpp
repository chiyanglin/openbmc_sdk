#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace client
{
namespace HostSelector
{

static constexpr auto interface = "xyz.openbmc_project.Chassis.Buttons.HostSelector";

} // namespace HostSelector
} // namespace client
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

