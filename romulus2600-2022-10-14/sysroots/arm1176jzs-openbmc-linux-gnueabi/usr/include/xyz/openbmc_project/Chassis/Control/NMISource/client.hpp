#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Control
{
namespace client
{
namespace NMISource
{

static constexpr auto interface = "xyz.openbmc_project.Chassis.Control.NMISource";

} // namespace NMISource
} // namespace client
} // namespace Control
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

