#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Smbios
{
namespace client
{
namespace MDR_V2
{

static constexpr auto interface = "xyz.openbmc_project.Smbios.MDR_V2";

} // namespace MDR_V2
} // namespace client
} // namespace Smbios
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

