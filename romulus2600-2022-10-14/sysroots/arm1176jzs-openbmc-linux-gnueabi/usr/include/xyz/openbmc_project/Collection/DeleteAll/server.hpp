#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Collection
{
namespace server
{

class DeleteAll
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        DeleteAll() = delete;
        DeleteAll(const DeleteAll&) = delete;
        DeleteAll& operator=(const DeleteAll&) = delete;
        DeleteAll(DeleteAll&&) = delete;
        DeleteAll& operator=(DeleteAll&&) = delete;
        virtual ~DeleteAll() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        DeleteAll(bus_t& bus, const char* path);



        /** @brief Implementation for DeleteAll
         *  Delete all objects in the collection.
         */
        virtual void deleteAll(
            ) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Collection_DeleteAll_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Collection_DeleteAll_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Collection.DeleteAll";

    private:

        /** @brief sd-bus callback for DeleteAll
         */
        static int _callback_DeleteAll(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Collection_DeleteAll_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Collection
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

