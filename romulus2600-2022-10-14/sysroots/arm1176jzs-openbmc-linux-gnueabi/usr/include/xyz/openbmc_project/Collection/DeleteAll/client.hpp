#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Collection
{
namespace client
{
namespace DeleteAll
{

static constexpr auto interface = "xyz.openbmc_project.Collection.DeleteAll";

} // namespace DeleteAll
} // namespace client
} // namespace Collection
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

