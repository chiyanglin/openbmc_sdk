#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Memory
{
namespace server
{

class MemoryECC
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        MemoryECC() = delete;
        MemoryECC(const MemoryECC&) = delete;
        MemoryECC& operator=(const MemoryECC&) = delete;
        MemoryECC(MemoryECC&&) = delete;
        MemoryECC& operator=(MemoryECC&&) = delete;
        virtual ~MemoryECC() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        MemoryECC(bus_t& bus, const char* path);

        enum class ECCStatus
        {
            ok,
            CE,
            UE,
            LogFull,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                ECCStatus,
                bool,
                int64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        MemoryECC(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of isLoggingLimitReached */
        virtual bool isLoggingLimitReached() const;
        /** Set value of isLoggingLimitReached with option to skip sending signal */
        virtual bool isLoggingLimitReached(bool value,
               bool skipSignal);
        /** Set value of isLoggingLimitReached */
        virtual bool isLoggingLimitReached(bool value);
        /** Get value of ceCount */
        virtual int64_t ceCount() const;
        /** Set value of ceCount with option to skip sending signal */
        virtual int64_t ceCount(int64_t value,
               bool skipSignal);
        /** Set value of ceCount */
        virtual int64_t ceCount(int64_t value);
        /** Get value of ueCount */
        virtual int64_t ueCount() const;
        /** Set value of ueCount with option to skip sending signal */
        virtual int64_t ueCount(int64_t value,
               bool skipSignal);
        /** Set value of ueCount */
        virtual int64_t ueCount(int64_t value);
        /** Get value of state */
        virtual ECCStatus state() const;
        /** Set value of state with option to skip sending signal */
        virtual ECCStatus state(ECCStatus value,
               bool skipSignal);
        /** Set value of state */
        virtual ECCStatus state(ECCStatus value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Memory.MemoryECC.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ECCStatus convertECCStatusFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Memory.MemoryECC.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ECCStatus> convertStringToECCStatus(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Memory.MemoryECC.<value name>"
         */
        static std::string convertECCStatusToString(ECCStatus e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Memory_MemoryECC_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Memory_MemoryECC_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Memory.MemoryECC";

    private:

        /** @brief sd-bus callback for get-property 'isLoggingLimitReached' */
        static int _callback_get_isLoggingLimitReached(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'isLoggingLimitReached' */
        static int _callback_set_isLoggingLimitReached(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ceCount' */
        static int _callback_get_ceCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ceCount' */
        static int _callback_set_ceCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ueCount' */
        static int _callback_get_ueCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ueCount' */
        static int _callback_set_ueCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'state' */
        static int _callback_get_state(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'state' */
        static int _callback_set_state(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Memory_MemoryECC_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _isLoggingLimitReached{};
        int64_t _ceCount{};
        int64_t _ueCount{};
        ECCStatus _state = ECCStatus::ok;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type MemoryECC::ECCStatus.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(MemoryECC::ECCStatus e)
{
    return MemoryECC::convertECCStatusToString(e);
}

} // namespace server
} // namespace Memory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Memory::server::MemoryECC::ECCStatus>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Memory::server::MemoryECC::convertStringToECCStatus(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Memory::server::MemoryECC::ECCStatus>
{
    static std::string op(xyz::openbmc_project::Memory::server::MemoryECC::ECCStatus value)
    {
        return xyz::openbmc_project::Memory::server::MemoryECC::convertECCStatusToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

