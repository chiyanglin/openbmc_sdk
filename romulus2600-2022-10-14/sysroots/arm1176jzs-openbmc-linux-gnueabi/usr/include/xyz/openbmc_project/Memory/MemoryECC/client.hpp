#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Memory
{
namespace client
{
namespace MemoryECC
{

static constexpr auto interface = "xyz.openbmc_project.Memory.MemoryECC";

} // namespace MemoryECC
} // namespace client
} // namespace Memory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

