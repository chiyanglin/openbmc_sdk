#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace client
{
namespace Mode
{

static constexpr auto interface = "xyz.openbmc_project.Control.Boot.Mode";

} // namespace Mode
} // namespace client
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

