#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>











#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

class ChassisCapabilities
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        ChassisCapabilities() = delete;
        ChassisCapabilities(const ChassisCapabilities&) = delete;
        ChassisCapabilities& operator=(const ChassisCapabilities&) = delete;
        ChassisCapabilities(ChassisCapabilities&&) = delete;
        ChassisCapabilities& operator=(ChassisCapabilities&&) = delete;
        virtual ~ChassisCapabilities() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        ChassisCapabilities(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        ChassisCapabilities(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of CapabilitiesFlags */
        virtual uint8_t capabilitiesFlags() const;
        /** Set value of CapabilitiesFlags with option to skip sending signal */
        virtual uint8_t capabilitiesFlags(uint8_t value,
               bool skipSignal);
        /** Set value of CapabilitiesFlags */
        virtual uint8_t capabilitiesFlags(uint8_t value);
        /** Get value of ChassisIntrusionEnabled */
        virtual bool chassisIntrusionEnabled() const;
        /** Set value of ChassisIntrusionEnabled with option to skip sending signal */
        virtual bool chassisIntrusionEnabled(bool value,
               bool skipSignal);
        /** Set value of ChassisIntrusionEnabled */
        virtual bool chassisIntrusionEnabled(bool value);
        /** Get value of ChassisFrontPanelLockoutEnabled */
        virtual bool chassisFrontPanelLockoutEnabled() const;
        /** Set value of ChassisFrontPanelLockoutEnabled with option to skip sending signal */
        virtual bool chassisFrontPanelLockoutEnabled(bool value,
               bool skipSignal);
        /** Set value of ChassisFrontPanelLockoutEnabled */
        virtual bool chassisFrontPanelLockoutEnabled(bool value);
        /** Get value of ChassisNMIEnabled */
        virtual bool chassisNMIEnabled() const;
        /** Set value of ChassisNMIEnabled with option to skip sending signal */
        virtual bool chassisNMIEnabled(bool value,
               bool skipSignal);
        /** Set value of ChassisNMIEnabled */
        virtual bool chassisNMIEnabled(bool value);
        /** Get value of ChassisPowerInterlockEnabled */
        virtual bool chassisPowerInterlockEnabled() const;
        /** Set value of ChassisPowerInterlockEnabled with option to skip sending signal */
        virtual bool chassisPowerInterlockEnabled(bool value,
               bool skipSignal);
        /** Set value of ChassisPowerInterlockEnabled */
        virtual bool chassisPowerInterlockEnabled(bool value);
        /** Get value of FRUDeviceAddress */
        virtual uint8_t fruDeviceAddress() const;
        /** Set value of FRUDeviceAddress with option to skip sending signal */
        virtual uint8_t fruDeviceAddress(uint8_t value,
               bool skipSignal);
        /** Set value of FRUDeviceAddress */
        virtual uint8_t fruDeviceAddress(uint8_t value);
        /** Get value of SDRDeviceAddress */
        virtual uint8_t sdrDeviceAddress() const;
        /** Set value of SDRDeviceAddress with option to skip sending signal */
        virtual uint8_t sdrDeviceAddress(uint8_t value,
               bool skipSignal);
        /** Set value of SDRDeviceAddress */
        virtual uint8_t sdrDeviceAddress(uint8_t value);
        /** Get value of SELDeviceAddress */
        virtual uint8_t selDeviceAddress() const;
        /** Set value of SELDeviceAddress with option to skip sending signal */
        virtual uint8_t selDeviceAddress(uint8_t value,
               bool skipSignal);
        /** Set value of SELDeviceAddress */
        virtual uint8_t selDeviceAddress(uint8_t value);
        /** Get value of SMDeviceAddress */
        virtual uint8_t smDeviceAddress() const;
        /** Set value of SMDeviceAddress with option to skip sending signal */
        virtual uint8_t smDeviceAddress(uint8_t value,
               bool skipSignal);
        /** Set value of SMDeviceAddress */
        virtual uint8_t smDeviceAddress(uint8_t value);
        /** Get value of BridgeDeviceAddress */
        virtual uint8_t bridgeDeviceAddress() const;
        /** Set value of BridgeDeviceAddress with option to skip sending signal */
        virtual uint8_t bridgeDeviceAddress(uint8_t value,
               bool skipSignal);
        /** Set value of BridgeDeviceAddress */
        virtual uint8_t bridgeDeviceAddress(uint8_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.ChassisCapabilities";

    private:

        /** @brief sd-bus callback for get-property 'CapabilitiesFlags' */
        static int _callback_get_CapabilitiesFlags(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CapabilitiesFlags' */
        static int _callback_set_CapabilitiesFlags(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ChassisIntrusionEnabled' */
        static int _callback_get_ChassisIntrusionEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ChassisIntrusionEnabled' */
        static int _callback_set_ChassisIntrusionEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ChassisFrontPanelLockoutEnabled' */
        static int _callback_get_ChassisFrontPanelLockoutEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ChassisFrontPanelLockoutEnabled' */
        static int _callback_set_ChassisFrontPanelLockoutEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ChassisNMIEnabled' */
        static int _callback_get_ChassisNMIEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ChassisNMIEnabled' */
        static int _callback_set_ChassisNMIEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ChassisPowerInterlockEnabled' */
        static int _callback_get_ChassisPowerInterlockEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ChassisPowerInterlockEnabled' */
        static int _callback_set_ChassisPowerInterlockEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'FRUDeviceAddress' */
        static int _callback_get_FRUDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'FRUDeviceAddress' */
        static int _callback_set_FRUDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SDRDeviceAddress' */
        static int _callback_get_SDRDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SDRDeviceAddress' */
        static int _callback_set_SDRDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SELDeviceAddress' */
        static int _callback_get_SELDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SELDeviceAddress' */
        static int _callback_set_SELDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SMDeviceAddress' */
        static int _callback_get_SMDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SMDeviceAddress' */
        static int _callback_set_SMDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'BridgeDeviceAddress' */
        static int _callback_get_BridgeDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'BridgeDeviceAddress' */
        static int _callback_set_BridgeDeviceAddress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_ChassisCapabilities_interface;
        sdbusplus::SdBusInterface *_intf;

        uint8_t _capabilitiesFlags{};
        bool _chassisIntrusionEnabled{};
        bool _chassisFrontPanelLockoutEnabled{};
        bool _chassisNMIEnabled{};
        bool _chassisPowerInterlockEnabled{};
        uint8_t _fruDeviceAddress{};
        uint8_t _sdrDeviceAddress{};
        uint8_t _selDeviceAddress{};
        uint8_t _smDeviceAddress{};
        uint8_t _bridgeDeviceAddress{};

};


} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

