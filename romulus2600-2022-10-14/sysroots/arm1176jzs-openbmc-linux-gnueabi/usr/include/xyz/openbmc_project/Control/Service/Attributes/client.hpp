#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Service
{
namespace client
{
namespace Attributes
{

static constexpr auto interface = "xyz.openbmc_project.Control.Service.Attributes";

} // namespace Attributes
} // namespace client
} // namespace Service
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

