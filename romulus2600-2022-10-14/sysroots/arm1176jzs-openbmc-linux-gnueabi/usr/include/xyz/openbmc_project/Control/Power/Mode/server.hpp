#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

class Mode
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Mode() = delete;
        Mode(const Mode&) = delete;
        Mode& operator=(const Mode&) = delete;
        Mode(Mode&&) = delete;
        Mode& operator=(Mode&&) = delete;
        virtual ~Mode() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Mode(bus_t& bus, const char* path);

        enum class PowerMode
        {
            Static,
            PowerSaving,
            MaximumPerformance,
            OEM,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                PowerMode,
                bool>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Mode(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PowerMode */
        virtual PowerMode powerMode() const;
        /** Set value of PowerMode with option to skip sending signal */
        virtual PowerMode powerMode(PowerMode value,
               bool skipSignal);
        /** Set value of PowerMode */
        virtual PowerMode powerMode(PowerMode value);
        /** Get value of SafeMode */
        virtual bool safeMode() const;
        /** Set value of SafeMode with option to skip sending signal */
        virtual bool safeMode(bool value,
               bool skipSignal);
        /** Set value of SafeMode */
        virtual bool safeMode(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Power.Mode.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static PowerMode convertPowerModeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Power.Mode.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<PowerMode> convertStringToPowerMode(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.Power.Mode.<value name>"
         */
        static std::string convertPowerModeToString(PowerMode e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Power_Mode_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Power_Mode_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Power.Mode";

    private:

        /** @brief sd-bus callback for get-property 'PowerMode' */
        static int _callback_get_PowerMode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerMode' */
        static int _callback_set_PowerMode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SafeMode' */
        static int _callback_get_SafeMode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Power_Mode_interface;
        sdbusplus::SdBusInterface *_intf;

        PowerMode _powerMode{};
        bool _safeMode = false;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Mode::PowerMode.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Mode::PowerMode e)
{
    return Mode::convertPowerModeToString(e);
}

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Control::Power::server::Mode::PowerMode>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::Power::server::Mode::convertStringToPowerMode(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::Power::server::Mode::PowerMode>
{
    static std::string op(xyz::openbmc_project::Control::Power::server::Mode::PowerMode value)
    {
        return xyz::openbmc_project::Control::Power::server::Mode::convertPowerModeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

