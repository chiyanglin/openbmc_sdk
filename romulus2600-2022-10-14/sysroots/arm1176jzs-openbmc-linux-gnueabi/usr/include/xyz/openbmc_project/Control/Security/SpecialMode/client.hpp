#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Security
{
namespace client
{
namespace SpecialMode
{

static constexpr auto interface = "xyz.openbmc_project.Control.Security.SpecialMode";

} // namespace SpecialMode
} // namespace client
} // namespace Security
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

