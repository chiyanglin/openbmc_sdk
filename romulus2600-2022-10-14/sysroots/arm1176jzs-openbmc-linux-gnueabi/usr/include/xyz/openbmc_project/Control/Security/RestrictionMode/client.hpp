#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Security
{
namespace client
{
namespace RestrictionMode
{

static constexpr auto interface = "xyz.openbmc_project.Control.Security.RestrictionMode";

} // namespace RestrictionMode
} // namespace client
} // namespace Security
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

