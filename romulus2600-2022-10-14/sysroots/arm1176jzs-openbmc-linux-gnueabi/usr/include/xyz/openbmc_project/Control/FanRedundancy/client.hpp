#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace FanRedundancy
{

static constexpr auto interface = "xyz.openbmc_project.Control.FanRedundancy";

} // namespace FanRedundancy
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

