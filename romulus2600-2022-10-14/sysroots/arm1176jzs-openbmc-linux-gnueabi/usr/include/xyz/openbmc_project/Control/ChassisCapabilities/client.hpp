#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace ChassisCapabilities
{

static constexpr auto interface = "xyz.openbmc_project.Control.ChassisCapabilities";

} // namespace ChassisCapabilities
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

