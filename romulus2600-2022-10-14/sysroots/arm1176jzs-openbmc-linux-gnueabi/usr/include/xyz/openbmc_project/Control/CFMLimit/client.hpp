#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace CFMLimit
{

static constexpr auto interface = "xyz.openbmc_project.Control.CFMLimit";

} // namespace CFMLimit
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

