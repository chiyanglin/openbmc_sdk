#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Processor
{
namespace client
{
namespace CurrentOperatingConfig
{

static constexpr auto interface = "xyz.openbmc_project.Control.Processor.CurrentOperatingConfig";

} // namespace CurrentOperatingConfig
} // namespace client
} // namespace Processor
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

