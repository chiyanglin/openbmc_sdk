#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace client
{
namespace RestorePolicy
{

static constexpr auto interface = "xyz.openbmc_project.Control.Power.RestorePolicy";

} // namespace RestorePolicy
} // namespace client
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

