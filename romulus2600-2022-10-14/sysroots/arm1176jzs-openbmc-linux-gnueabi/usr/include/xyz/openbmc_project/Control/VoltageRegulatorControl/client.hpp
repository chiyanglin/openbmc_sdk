#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace VoltageRegulatorControl
{

static constexpr auto interface = "xyz.openbmc_project.Control.VoltageRegulatorControl";

} // namespace VoltageRegulatorControl
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

