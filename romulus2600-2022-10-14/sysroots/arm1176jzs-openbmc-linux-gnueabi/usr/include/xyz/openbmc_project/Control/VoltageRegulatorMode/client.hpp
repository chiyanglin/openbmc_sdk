#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace VoltageRegulatorMode
{

static constexpr auto interface = "xyz.openbmc_project.Control.VoltageRegulatorMode";

} // namespace VoltageRegulatorMode
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

