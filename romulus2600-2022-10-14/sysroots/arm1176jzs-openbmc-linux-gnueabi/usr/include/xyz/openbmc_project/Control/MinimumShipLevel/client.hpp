#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace MinimumShipLevel
{

static constexpr auto interface = "xyz.openbmc_project.Control.MinimumShipLevel";

} // namespace MinimumShipLevel
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

