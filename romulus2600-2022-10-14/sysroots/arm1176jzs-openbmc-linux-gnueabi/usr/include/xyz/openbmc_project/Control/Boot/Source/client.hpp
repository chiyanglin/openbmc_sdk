#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace client
{
namespace Source
{

static constexpr auto interface = "xyz.openbmc_project.Control.Boot.Source";

} // namespace Source
} // namespace client
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

