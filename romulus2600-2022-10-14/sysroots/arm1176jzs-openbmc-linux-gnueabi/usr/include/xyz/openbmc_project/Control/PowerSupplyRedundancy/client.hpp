#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace client
{
namespace PowerSupplyRedundancy
{

static constexpr auto interface = "xyz.openbmc_project.Control.PowerSupplyRedundancy";

} // namespace PowerSupplyRedundancy
} // namespace client
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

