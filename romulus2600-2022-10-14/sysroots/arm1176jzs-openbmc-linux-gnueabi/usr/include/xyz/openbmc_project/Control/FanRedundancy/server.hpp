#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

class FanRedundancy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        FanRedundancy() = delete;
        FanRedundancy(const FanRedundancy&) = delete;
        FanRedundancy& operator=(const FanRedundancy&) = delete;
        FanRedundancy(FanRedundancy&&) = delete;
        FanRedundancy& operator=(FanRedundancy&&) = delete;
        virtual ~FanRedundancy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        FanRedundancy(bus_t& bus, const char* path);

        enum class State
        {
            Full,
            Degraded,
            Failed,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                State,
                std::vector<sdbusplus::message::object_path>,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        FanRedundancy(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of AllowedFailures */
        virtual uint8_t allowedFailures() const;
        /** Set value of AllowedFailures with option to skip sending signal */
        virtual uint8_t allowedFailures(uint8_t value,
               bool skipSignal);
        /** Set value of AllowedFailures */
        virtual uint8_t allowedFailures(uint8_t value);
        /** Get value of Collection */
        virtual std::vector<sdbusplus::message::object_path> collection() const;
        /** Set value of Collection with option to skip sending signal */
        virtual std::vector<sdbusplus::message::object_path> collection(std::vector<sdbusplus::message::object_path> value,
               bool skipSignal);
        /** Set value of Collection */
        virtual std::vector<sdbusplus::message::object_path> collection(std::vector<sdbusplus::message::object_path> value);
        /** Get value of Status */
        virtual State status() const;
        /** Set value of Status with option to skip sending signal */
        virtual State status(State value,
               bool skipSignal);
        /** Set value of Status */
        virtual State status(State value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.FanRedundancy.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static State convertStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.FanRedundancy.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<State> convertStringToState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.FanRedundancy.<value name>"
         */
        static std::string convertStateToString(State e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_FanRedundancy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_FanRedundancy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.FanRedundancy";

    private:

        /** @brief sd-bus callback for get-property 'AllowedFailures' */
        static int _callback_get_AllowedFailures(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Collection' */
        static int _callback_get_Collection(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Status' */
        static int _callback_get_Status(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_FanRedundancy_interface;
        sdbusplus::SdBusInterface *_intf;

        uint8_t _allowedFailures{};
        std::vector<sdbusplus::message::object_path> _collection{};
        State _status{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type FanRedundancy::State.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(FanRedundancy::State e)
{
    return FanRedundancy::convertStateToString(e);
}

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Control::server::FanRedundancy::State>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::server::FanRedundancy::convertStringToState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::server::FanRedundancy::State>
{
    static std::string op(xyz::openbmc_project::Control::server::FanRedundancy::State value)
    {
        return xyz::openbmc_project::Control::server::FanRedundancy::convertStateToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

