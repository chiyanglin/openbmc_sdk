#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Host
{
namespace server
{

class NMI
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        NMI() = delete;
        NMI(const NMI&) = delete;
        NMI& operator=(const NMI&) = delete;
        NMI(NMI&&) = delete;
        NMI& operator=(NMI&&) = delete;
        virtual ~NMI() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        NMI(bus_t& bus, const char* path);



        /** @brief Implementation for NMI
         *  Generic method to initiate non maskable interrupt on all host processors.
         */
        virtual void nmi(
            ) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Host_NMI_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Host_NMI_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Host.NMI";

    private:

        /** @brief sd-bus callback for NMI
         */
        static int _callback_NMI(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Host_NMI_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Host
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

