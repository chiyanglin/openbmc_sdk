#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Host
{
namespace client
{
namespace NMI
{

static constexpr auto interface = "xyz.openbmc_project.Control.Host.NMI";

} // namespace NMI
} // namespace client
} // namespace Host
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

