#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Host
{
namespace client
{
namespace TurboAllowed
{

static constexpr auto interface = "xyz.openbmc_project.Control.Host.TurboAllowed";

} // namespace TurboAllowed
} // namespace client
} // namespace Host
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

