#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Service
{
namespace client
{
namespace SocketAttributes
{

static constexpr auto interface = "xyz.openbmc_project.Control.Service.SocketAttributes";

} // namespace SocketAttributes
} // namespace client
} // namespace Service
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

