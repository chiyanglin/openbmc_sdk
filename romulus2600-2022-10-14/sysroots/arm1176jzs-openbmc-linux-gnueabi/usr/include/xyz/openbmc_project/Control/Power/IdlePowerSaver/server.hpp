#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>







#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

class IdlePowerSaver
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        IdlePowerSaver() = delete;
        IdlePowerSaver(const IdlePowerSaver&) = delete;
        IdlePowerSaver& operator=(const IdlePowerSaver&) = delete;
        IdlePowerSaver(IdlePowerSaver&&) = delete;
        IdlePowerSaver& operator=(IdlePowerSaver&&) = delete;
        virtual ~IdlePowerSaver() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        IdlePowerSaver(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                uint64_t,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        IdlePowerSaver(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Enabled */
        virtual bool enabled() const;
        /** Set value of Enabled with option to skip sending signal */
        virtual bool enabled(bool value,
               bool skipSignal);
        /** Set value of Enabled */
        virtual bool enabled(bool value);
        /** Get value of EnterUtilizationPercent */
        virtual uint8_t enterUtilizationPercent() const;
        /** Set value of EnterUtilizationPercent with option to skip sending signal */
        virtual uint8_t enterUtilizationPercent(uint8_t value,
               bool skipSignal);
        /** Set value of EnterUtilizationPercent */
        virtual uint8_t enterUtilizationPercent(uint8_t value);
        /** Get value of EnterDwellTime */
        virtual uint64_t enterDwellTime() const;
        /** Set value of EnterDwellTime with option to skip sending signal */
        virtual uint64_t enterDwellTime(uint64_t value,
               bool skipSignal);
        /** Set value of EnterDwellTime */
        virtual uint64_t enterDwellTime(uint64_t value);
        /** Get value of ExitUtilizationPercent */
        virtual uint8_t exitUtilizationPercent() const;
        /** Set value of ExitUtilizationPercent with option to skip sending signal */
        virtual uint8_t exitUtilizationPercent(uint8_t value,
               bool skipSignal);
        /** Set value of ExitUtilizationPercent */
        virtual uint8_t exitUtilizationPercent(uint8_t value);
        /** Get value of ExitDwellTime */
        virtual uint64_t exitDwellTime() const;
        /** Set value of ExitDwellTime with option to skip sending signal */
        virtual uint64_t exitDwellTime(uint64_t value,
               bool skipSignal);
        /** Set value of ExitDwellTime */
        virtual uint64_t exitDwellTime(uint64_t value);
        /** Get value of Active */
        virtual bool active() const;
        /** Set value of Active with option to skip sending signal */
        virtual bool active(bool value,
               bool skipSignal);
        /** Set value of Active */
        virtual bool active(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Power.IdlePowerSaver";

    private:

        /** @brief sd-bus callback for get-property 'Enabled' */
        static int _callback_get_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Enabled' */
        static int _callback_set_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EnterUtilizationPercent' */
        static int _callback_get_EnterUtilizationPercent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EnterUtilizationPercent' */
        static int _callback_set_EnterUtilizationPercent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EnterDwellTime' */
        static int _callback_get_EnterDwellTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EnterDwellTime' */
        static int _callback_set_EnterDwellTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ExitUtilizationPercent' */
        static int _callback_get_ExitUtilizationPercent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ExitUtilizationPercent' */
        static int _callback_set_ExitUtilizationPercent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ExitDwellTime' */
        static int _callback_get_ExitDwellTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ExitDwellTime' */
        static int _callback_set_ExitDwellTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Active' */
        static int _callback_get_Active(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _enabled{};
        uint8_t _enterUtilizationPercent{};
        uint64_t _enterDwellTime{};
        uint8_t _exitUtilizationPercent{};
        uint64_t _exitDwellTime{};
        bool _active{};

};


} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

