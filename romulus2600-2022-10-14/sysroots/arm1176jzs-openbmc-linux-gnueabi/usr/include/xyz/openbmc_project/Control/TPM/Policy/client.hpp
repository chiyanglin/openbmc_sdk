#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace TPM
{
namespace client
{
namespace Policy
{

static constexpr auto interface = "xyz.openbmc_project.Control.TPM.Policy";

} // namespace Policy
} // namespace client
} // namespace TPM
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

