#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace OperatingSystem
{
namespace client
{
namespace Status
{

static constexpr auto interface = "xyz.openbmc_project.State.OperatingSystem.Status";

} // namespace Status
} // namespace client
} // namespace OperatingSystem
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

