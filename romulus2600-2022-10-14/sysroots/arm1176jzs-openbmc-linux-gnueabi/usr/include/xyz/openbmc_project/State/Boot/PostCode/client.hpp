#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace client
{
namespace PostCode
{

static constexpr auto interface = "xyz.openbmc_project.State.Boot.PostCode";

} // namespace PostCode
} // namespace client
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

