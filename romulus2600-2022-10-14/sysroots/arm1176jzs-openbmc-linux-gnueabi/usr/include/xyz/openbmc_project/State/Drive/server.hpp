#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

class Drive
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Drive() = delete;
        Drive(const Drive&) = delete;
        Drive& operator=(const Drive&) = delete;
        Drive(Drive&&) = delete;
        Drive& operator=(Drive&&) = delete;
        virtual ~Drive() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Drive(bus_t& bus, const char* path);

        enum class Transition
        {
            Reboot,
            HardReboot,
            Powercycle,
            None,
            NotSupported,
        };
        enum class DriveState
        {
            Unknown,
            Ready,
            NotReady,
            Offline,
            Debug,
            UpdateInProgress,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                DriveState,
                Transition,
                bool,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Drive(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Rebuilding */
        virtual bool rebuilding() const;
        /** Set value of Rebuilding with option to skip sending signal */
        virtual bool rebuilding(bool value,
               bool skipSignal);
        /** Set value of Rebuilding */
        virtual bool rebuilding(bool value);
        /** Get value of RequestedDriveTransition */
        virtual Transition requestedDriveTransition() const;
        /** Set value of RequestedDriveTransition with option to skip sending signal */
        virtual Transition requestedDriveTransition(Transition value,
               bool skipSignal);
        /** Set value of RequestedDriveTransition */
        virtual Transition requestedDriveTransition(Transition value);
        /** Get value of CurrentDriveState */
        virtual DriveState currentDriveState() const;
        /** Set value of CurrentDriveState with option to skip sending signal */
        virtual DriveState currentDriveState(DriveState value,
               bool skipSignal);
        /** Set value of CurrentDriveState */
        virtual DriveState currentDriveState(DriveState value);
        /** Get value of LastRebootTime */
        virtual uint64_t lastRebootTime() const;
        /** Set value of LastRebootTime with option to skip sending signal */
        virtual uint64_t lastRebootTime(uint64_t value,
               bool skipSignal);
        /** Set value of LastRebootTime */
        virtual uint64_t lastRebootTime(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Drive.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Transition convertTransitionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Drive.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Transition> convertStringToTransition(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Drive.<value name>"
         */
        static std::string convertTransitionToString(Transition e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Drive.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DriveState convertDriveStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Drive.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DriveState> convertStringToDriveState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Drive.<value name>"
         */
        static std::string convertDriveStateToString(DriveState e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_Drive_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_Drive_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.Drive";

    private:

        /** @brief sd-bus callback for get-property 'Rebuilding' */
        static int _callback_get_Rebuilding(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Rebuilding' */
        static int _callback_set_Rebuilding(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RequestedDriveTransition' */
        static int _callback_get_RequestedDriveTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RequestedDriveTransition' */
        static int _callback_set_RequestedDriveTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentDriveState' */
        static int _callback_get_CurrentDriveState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LastRebootTime' */
        static int _callback_get_LastRebootTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_Drive_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _rebuilding = false;
        Transition _requestedDriveTransition = Transition::NotSupported;
        DriveState _currentDriveState = DriveState::Unknown;
        uint64_t _lastRebootTime = std::numeric_limits<uint64_t>::max();

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Drive::Transition.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Drive::Transition e)
{
    return Drive::convertTransitionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Drive::DriveState.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Drive::DriveState e)
{
    return Drive::convertDriveStateToString(e);
}

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Drive::Transition>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Drive::convertStringToTransition(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Drive::Transition>
{
    static std::string op(xyz::openbmc_project::State::server::Drive::Transition value)
    {
        return xyz::openbmc_project::State::server::Drive::convertTransitionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Drive::DriveState>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Drive::convertStringToDriveState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Drive::DriveState>
{
    static std::string op(xyz::openbmc_project::State::server::Drive::DriveState value)
    {
        return xyz::openbmc_project::State::server::Drive::convertDriveStateToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

