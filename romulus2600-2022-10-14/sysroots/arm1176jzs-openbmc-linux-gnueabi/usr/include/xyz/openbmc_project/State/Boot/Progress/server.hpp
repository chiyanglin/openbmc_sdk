#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace server
{

class Progress
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Progress() = delete;
        Progress(const Progress&) = delete;
        Progress& operator=(const Progress&) = delete;
        Progress(Progress&&) = delete;
        Progress& operator=(Progress&&) = delete;
        virtual ~Progress() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Progress(bus_t& bus, const char* path);

        enum class ProgressStages
        {
            Unspecified,
            PrimaryProcInit,
            BusInit,
            MemoryInit,
            SecondaryProcInit,
            PCIInit,
            SystemInitComplete,
            OSStart,
            OSRunning,
            SystemSetup,
            MotherboardInit,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                ProgressStages,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Progress(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of BootProgress */
        virtual ProgressStages bootProgress() const;
        /** Set value of BootProgress with option to skip sending signal */
        virtual ProgressStages bootProgress(ProgressStages value,
               bool skipSignal);
        /** Set value of BootProgress */
        virtual ProgressStages bootProgress(ProgressStages value);
        /** Get value of BootProgressLastUpdate */
        virtual uint64_t bootProgressLastUpdate() const;
        /** Set value of BootProgressLastUpdate with option to skip sending signal */
        virtual uint64_t bootProgressLastUpdate(uint64_t value,
               bool skipSignal);
        /** Set value of BootProgressLastUpdate */
        virtual uint64_t bootProgressLastUpdate(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Boot.Progress.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ProgressStages convertProgressStagesFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Boot.Progress.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ProgressStages> convertStringToProgressStages(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Boot.Progress.<value name>"
         */
        static std::string convertProgressStagesToString(ProgressStages e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_Boot_Progress_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_Boot_Progress_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.Boot.Progress";

    private:

        /** @brief sd-bus callback for get-property 'BootProgress' */
        static int _callback_get_BootProgress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'BootProgress' */
        static int _callback_set_BootProgress(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'BootProgressLastUpdate' */
        static int _callback_get_BootProgressLastUpdate(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'BootProgressLastUpdate' */
        static int _callback_set_BootProgressLastUpdate(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_Boot_Progress_interface;
        sdbusplus::SdBusInterface *_intf;

        ProgressStages _bootProgress = ProgressStages::Unspecified;
        uint64_t _bootProgressLastUpdate = 0;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Progress::ProgressStages.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Progress::ProgressStages e)
{
    return Progress::convertProgressStagesToString(e);
}

} // namespace server
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::State::Boot::server::Progress::ProgressStages>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::Boot::server::Progress::convertStringToProgressStages(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::Boot::server::Progress::ProgressStages>
{
    static std::string op(xyz::openbmc_project::State::Boot::server::Progress::ProgressStages value)
    {
        return xyz::openbmc_project::State::Boot::server::Progress::convertProgressStagesToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

