#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace Watchdog
{

static constexpr auto interface = "xyz.openbmc_project.State.Watchdog";

} // namespace Watchdog
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

