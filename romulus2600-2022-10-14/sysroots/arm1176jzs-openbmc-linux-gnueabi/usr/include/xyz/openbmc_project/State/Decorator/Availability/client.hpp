#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Decorator
{
namespace client
{
namespace Availability
{

static constexpr auto interface = "xyz.openbmc_project.State.Decorator.Availability";

} // namespace Availability
} // namespace client
} // namespace Decorator
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

