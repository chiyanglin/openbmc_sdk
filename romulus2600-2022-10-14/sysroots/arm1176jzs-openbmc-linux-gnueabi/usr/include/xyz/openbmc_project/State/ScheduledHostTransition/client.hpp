#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace ScheduledHostTransition
{

static constexpr auto interface = "xyz.openbmc_project.State.ScheduledHostTransition";

} // namespace ScheduledHostTransition
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

