#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace server
{

class PostCode
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PostCode() = delete;
        PostCode(const PostCode&) = delete;
        PostCode& operator=(const PostCode&) = delete;
        PostCode(PostCode&&) = delete;
        PostCode& operator=(PostCode&&) = delete;
        virtual ~PostCode() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PostCode(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint16_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PostCode(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for GetPostCodesWithTimeStamp
         *  Method to get the cached post codes of the indicated boot cycle with timestamp.
         *
         *  @param[in] index - Index indicates which boot cycle of post codes is requested. 1 is for the most recent boot cycle. CurrentBootCycleCount is for the oldest boot cycle.
         *
         *  @return codes[std::map<uint64_t, std::tuple<uint64_t, std::vector<uint8_t>>>] - An array of post codes and timestamp in microseconds since epoch
         */
        virtual std::map<uint64_t, std::tuple<uint64_t, std::vector<uint8_t>>> getPostCodesWithTimeStamp(
            uint16_t index) = 0;

        /** @brief Implementation for GetPostCodes
         *  Method to get the cached post codes of the indicated boot cycle.
         *
         *  @param[in] index - Index indicates which boot cycle of post codes is requested. 1 is for the most recent boot cycle. CurrentBootCycleCount is for the oldest boot cycle.
         *
         *  @return codes[std::vector<std::tuple<uint64_t, std::vector<uint8_t>>>] - An array of post codes of one boot cycle.
         */
        virtual std::vector<std::tuple<uint64_t, std::vector<uint8_t>>> getPostCodes(
            uint16_t index) = 0;


        /** Get value of CurrentBootCycleCount */
        virtual uint16_t currentBootCycleCount() const;
        /** Set value of CurrentBootCycleCount with option to skip sending signal */
        virtual uint16_t currentBootCycleCount(uint16_t value,
               bool skipSignal);
        /** Set value of CurrentBootCycleCount */
        virtual uint16_t currentBootCycleCount(uint16_t value);
        /** Get value of MaxBootCycleNum */
        virtual uint16_t maxBootCycleNum() const;
        /** Set value of MaxBootCycleNum with option to skip sending signal */
        virtual uint16_t maxBootCycleNum(uint16_t value,
               bool skipSignal);
        /** Set value of MaxBootCycleNum */
        virtual uint16_t maxBootCycleNum(uint16_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_Boot_PostCode_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_Boot_PostCode_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.Boot.PostCode";

    private:

        /** @brief sd-bus callback for GetPostCodesWithTimeStamp
         */
        static int _callback_GetPostCodesWithTimeStamp(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetPostCodes
         */
        static int _callback_GetPostCodes(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentBootCycleCount' */
        static int _callback_get_CurrentBootCycleCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CurrentBootCycleCount' */
        static int _callback_set_CurrentBootCycleCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxBootCycleNum' */
        static int _callback_get_MaxBootCycleNum(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxBootCycleNum' */
        static int _callback_set_MaxBootCycleNum(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_Boot_PostCode_interface;
        sdbusplus::SdBusInterface *_intf;

        uint16_t _currentBootCycleCount{};
        uint16_t _maxBootCycleNum{};

};


} // namespace server
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

