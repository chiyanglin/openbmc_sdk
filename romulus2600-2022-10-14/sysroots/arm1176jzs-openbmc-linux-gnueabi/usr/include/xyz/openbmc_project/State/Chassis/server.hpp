#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

class Chassis
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Chassis() = delete;
        Chassis(const Chassis&) = delete;
        Chassis& operator=(const Chassis&) = delete;
        Chassis(Chassis&&) = delete;
        Chassis& operator=(Chassis&&) = delete;
        virtual ~Chassis() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Chassis(bus_t& bus, const char* path);

        enum class Transition
        {
            Off,
            On,
            PowerCycle,
        };
        enum class PowerState
        {
            Off,
            TransitioningToOff,
            On,
            TransitioningToOn,
        };
        enum class PowerStatus
        {
            Undefined,
            BrownOut,
            UninterruptiblePowerSupply,
            Good,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                PowerState,
                PowerStatus,
                Transition,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Chassis(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RequestedPowerTransition */
        virtual Transition requestedPowerTransition() const;
        /** Set value of RequestedPowerTransition with option to skip sending signal */
        virtual Transition requestedPowerTransition(Transition value,
               bool skipSignal);
        /** Set value of RequestedPowerTransition */
        virtual Transition requestedPowerTransition(Transition value);
        /** Get value of CurrentPowerState */
        virtual PowerState currentPowerState() const;
        /** Set value of CurrentPowerState with option to skip sending signal */
        virtual PowerState currentPowerState(PowerState value,
               bool skipSignal);
        /** Set value of CurrentPowerState */
        virtual PowerState currentPowerState(PowerState value);
        /** Get value of CurrentPowerStatus */
        virtual PowerStatus currentPowerStatus() const;
        /** Set value of CurrentPowerStatus with option to skip sending signal */
        virtual PowerStatus currentPowerStatus(PowerStatus value,
               bool skipSignal);
        /** Set value of CurrentPowerStatus */
        virtual PowerStatus currentPowerStatus(PowerStatus value);
        /** Get value of LastStateChangeTime */
        virtual uint64_t lastStateChangeTime() const;
        /** Set value of LastStateChangeTime with option to skip sending signal */
        virtual uint64_t lastStateChangeTime(uint64_t value,
               bool skipSignal);
        /** Set value of LastStateChangeTime */
        virtual uint64_t lastStateChangeTime(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Chassis.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Transition convertTransitionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Chassis.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Transition> convertStringToTransition(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Chassis.<value name>"
         */
        static std::string convertTransitionToString(Transition e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Chassis.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static PowerState convertPowerStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Chassis.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<PowerState> convertStringToPowerState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Chassis.<value name>"
         */
        static std::string convertPowerStateToString(PowerState e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Chassis.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static PowerStatus convertPowerStatusFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Chassis.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<PowerStatus> convertStringToPowerStatus(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Chassis.<value name>"
         */
        static std::string convertPowerStatusToString(PowerStatus e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_Chassis_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_Chassis_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.Chassis";

    private:

        /** @brief sd-bus callback for get-property 'RequestedPowerTransition' */
        static int _callback_get_RequestedPowerTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RequestedPowerTransition' */
        static int _callback_set_RequestedPowerTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentPowerState' */
        static int _callback_get_CurrentPowerState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CurrentPowerState' */
        static int _callback_set_CurrentPowerState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentPowerStatus' */
        static int _callback_get_CurrentPowerStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CurrentPowerStatus' */
        static int _callback_set_CurrentPowerStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LastStateChangeTime' */
        static int _callback_get_LastStateChangeTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LastStateChangeTime' */
        static int _callback_set_LastStateChangeTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_Chassis_interface;
        sdbusplus::SdBusInterface *_intf;

        Transition _requestedPowerTransition = Transition::Off;
        PowerState _currentPowerState{};
        PowerStatus _currentPowerStatus{};
        uint64_t _lastStateChangeTime{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Chassis::Transition.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Chassis::Transition e)
{
    return Chassis::convertTransitionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Chassis::PowerState.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Chassis::PowerState e)
{
    return Chassis::convertPowerStateToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Chassis::PowerStatus.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Chassis::PowerStatus e)
{
    return Chassis::convertPowerStatusToString(e);
}

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Chassis::Transition>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Chassis::convertStringToTransition(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Chassis::Transition>
{
    static std::string op(xyz::openbmc_project::State::server::Chassis::Transition value)
    {
        return xyz::openbmc_project::State::server::Chassis::convertTransitionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Chassis::PowerState>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Chassis::convertStringToPowerState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Chassis::PowerState>
{
    static std::string op(xyz::openbmc_project::State::server::Chassis::PowerState value)
    {
        return xyz::openbmc_project::State::server::Chassis::convertPowerStateToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Chassis::PowerStatus>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Chassis::convertStringToPowerStatus(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Chassis::PowerStatus>
{
    static std::string op(xyz::openbmc_project::State::server::Chassis::PowerStatus value)
    {
        return xyz::openbmc_project::State::server::Chassis::convertPowerStatusToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

