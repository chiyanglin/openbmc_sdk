#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace client
{
namespace Progress
{

static constexpr auto interface = "xyz.openbmc_project.State.Boot.Progress";

} // namespace Progress
} // namespace client
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

