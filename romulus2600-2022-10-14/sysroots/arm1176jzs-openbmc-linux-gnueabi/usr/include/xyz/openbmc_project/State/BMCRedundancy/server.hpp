#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

class BMCRedundancy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        BMCRedundancy() = delete;
        BMCRedundancy(const BMCRedundancy&) = delete;
        BMCRedundancy& operator=(const BMCRedundancy&) = delete;
        BMCRedundancy(BMCRedundancy&&) = delete;
        BMCRedundancy& operator=(BMCRedundancy&&) = delete;
        virtual ~BMCRedundancy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        BMCRedundancy(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                sdbusplus::message::object_path>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        BMCRedundancy(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ActivePrimaryBMC */
        virtual sdbusplus::message::object_path activePrimaryBMC() const;
        /** Set value of ActivePrimaryBMC with option to skip sending signal */
        virtual sdbusplus::message::object_path activePrimaryBMC(sdbusplus::message::object_path value,
               bool skipSignal);
        /** Set value of ActivePrimaryBMC */
        virtual sdbusplus::message::object_path activePrimaryBMC(sdbusplus::message::object_path value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_BMCRedundancy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_BMCRedundancy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.BMCRedundancy";

    private:

        /** @brief sd-bus callback for get-property 'ActivePrimaryBMC' */
        static int _callback_get_ActivePrimaryBMC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ActivePrimaryBMC' */
        static int _callback_set_ActivePrimaryBMC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_BMCRedundancy_interface;
        sdbusplus::SdBusInterface *_intf;

        sdbusplus::message::object_path _activePrimaryBMC{};

};


} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

