#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace Drive
{

static constexpr auto interface = "xyz.openbmc_project.State.Drive";

} // namespace Drive
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

