#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace Host
{

static constexpr auto interface = "xyz.openbmc_project.State.Host";

} // namespace Host
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

