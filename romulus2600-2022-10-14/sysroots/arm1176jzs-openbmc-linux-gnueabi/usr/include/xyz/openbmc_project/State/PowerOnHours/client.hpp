#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace PowerOnHours
{

static constexpr auto interface = "xyz.openbmc_project.State.PowerOnHours";

} // namespace PowerOnHours
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

