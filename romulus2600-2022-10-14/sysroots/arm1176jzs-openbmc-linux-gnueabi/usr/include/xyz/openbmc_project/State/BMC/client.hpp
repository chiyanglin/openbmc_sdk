#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace BMC
{

static constexpr auto interface = "xyz.openbmc_project.State.BMC";

} // namespace BMC
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

