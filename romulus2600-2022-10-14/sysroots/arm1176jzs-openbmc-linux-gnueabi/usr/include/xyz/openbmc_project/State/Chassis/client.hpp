#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace client
{
namespace Chassis
{

static constexpr auto interface = "xyz.openbmc_project.State.Chassis";

} // namespace Chassis
} // namespace client
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

