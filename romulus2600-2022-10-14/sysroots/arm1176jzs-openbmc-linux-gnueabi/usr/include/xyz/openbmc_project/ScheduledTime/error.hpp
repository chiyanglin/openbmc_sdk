#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace ScheduledTime
{
namespace Error
{

struct InvalidTime final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.ScheduledTime.Error.InvalidTime";
    static constexpr auto errDesc =
            "Scheduled time is in the past";
    static constexpr auto errWhat =
            "xyz.openbmc_project.ScheduledTime.Error.InvalidTime: Scheduled time is in the past";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace ScheduledTime
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

