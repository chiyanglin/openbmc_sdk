#pragma once


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace client
{
namespace ObjectMapper
{

static constexpr auto interface = "xyz.openbmc_project.ObjectMapper";

} // namespace ObjectMapper
} // namespace client
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

