#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Inventory
{
namespace Decorator
{
namespace client
{
namespace Asset
{

static constexpr auto interface = "org.open_power.Inventory.Decorator.Asset";

} // namespace Asset
} // namespace client
} // namespace Decorator
} // namespace Inventory
} // namespace open_power
} // namespace org
} // namespace sdbusplus

