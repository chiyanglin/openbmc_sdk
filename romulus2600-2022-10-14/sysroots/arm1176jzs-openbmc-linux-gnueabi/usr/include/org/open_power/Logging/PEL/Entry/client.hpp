#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Logging
{
namespace PEL
{
namespace client
{
namespace Entry
{

static constexpr auto interface = "org.open_power.Logging.PEL.Entry";

} // namespace Entry
} // namespace client
} // namespace PEL
} // namespace Logging
} // namespace open_power
} // namespace org
} // namespace sdbusplus

