#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Logging
{
namespace client
{
namespace PEL
{

static constexpr auto interface = "org.open_power.Logging.PEL";

} // namespace PEL
} // namespace client
} // namespace Logging
} // namespace open_power
} // namespace org
} // namespace sdbusplus

