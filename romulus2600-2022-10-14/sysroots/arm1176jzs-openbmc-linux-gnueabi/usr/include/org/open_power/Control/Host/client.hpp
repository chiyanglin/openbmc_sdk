#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Control
{
namespace client
{
namespace Host
{

static constexpr auto interface = "org.open_power.Control.Host";

} // namespace Host
} // namespace client
} // namespace Control
} // namespace open_power
} // namespace org
} // namespace sdbusplus

