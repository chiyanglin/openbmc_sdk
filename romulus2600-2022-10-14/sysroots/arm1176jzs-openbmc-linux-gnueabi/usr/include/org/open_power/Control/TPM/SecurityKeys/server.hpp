#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Control
{
namespace TPM
{
namespace server
{

class SecurityKeys
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        SecurityKeys() = delete;
        SecurityKeys(const SecurityKeys&) = delete;
        SecurityKeys& operator=(const SecurityKeys&) = delete;
        SecurityKeys(SecurityKeys&&) = delete;
        SecurityKeys& operator=(SecurityKeys&&) = delete;
        virtual ~SecurityKeys() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        SecurityKeys(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        SecurityKeys(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ClearHostSecurityKeys */
        virtual uint8_t clearHostSecurityKeys() const;
        /** Set value of ClearHostSecurityKeys with option to skip sending signal */
        virtual uint8_t clearHostSecurityKeys(uint8_t value,
               bool skipSignal);
        /** Set value of ClearHostSecurityKeys */
        virtual uint8_t clearHostSecurityKeys(uint8_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _org_open_power_Control_TPM_SecurityKeys_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _org_open_power_Control_TPM_SecurityKeys_interface.emit_removed();
        }

        static constexpr auto interface = "org.open_power.Control.TPM.SecurityKeys";

    private:

        /** @brief sd-bus callback for get-property 'ClearHostSecurityKeys' */
        static int _callback_get_ClearHostSecurityKeys(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ClearHostSecurityKeys' */
        static int _callback_set_ClearHostSecurityKeys(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _org_open_power_Control_TPM_SecurityKeys_interface;
        sdbusplus::SdBusInterface *_intf;

        uint8_t _clearHostSecurityKeys{};

};


} // namespace server
} // namespace TPM
} // namespace Control
} // namespace open_power
} // namespace org

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

