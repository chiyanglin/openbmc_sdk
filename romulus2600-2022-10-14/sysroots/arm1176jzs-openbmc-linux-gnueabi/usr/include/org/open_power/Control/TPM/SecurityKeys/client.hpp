#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Control
{
namespace TPM
{
namespace client
{
namespace SecurityKeys
{

static constexpr auto interface = "org.open_power.Control.TPM.SecurityKeys";

} // namespace SecurityKeys
} // namespace client
} // namespace TPM
} // namespace Control
} // namespace open_power
} // namespace org
} // namespace sdbusplus

