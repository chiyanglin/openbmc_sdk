#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace OCC
{
namespace client
{
namespace PassThrough
{

static constexpr auto interface = "org.open_power.OCC.PassThrough";

} // namespace PassThrough
} // namespace client
} // namespace OCC
} // namespace open_power
} // namespace org
} // namespace sdbusplus

