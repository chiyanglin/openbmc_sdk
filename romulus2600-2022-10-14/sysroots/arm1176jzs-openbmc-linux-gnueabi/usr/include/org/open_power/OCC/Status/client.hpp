#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace OCC
{
namespace client
{
namespace Status
{

static constexpr auto interface = "org.open_power.OCC.Status";

} // namespace Status
} // namespace client
} // namespace OCC
} // namespace open_power
} // namespace org
} // namespace sdbusplus

