#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Sensor
{
namespace Aggregation
{
namespace History
{
namespace client
{
namespace Maximum
{

static constexpr auto interface = "org.open_power.Sensor.Aggregation.History.Maximum";

} // namespace Maximum
} // namespace client
} // namespace History
} // namespace Aggregation
} // namespace Sensor
} // namespace open_power
} // namespace org
} // namespace sdbusplus

