#pragma once


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Sensor
{
namespace Aggregation
{
namespace History
{
namespace client
{
namespace Average
{

static constexpr auto interface = "org.open_power.Sensor.Aggregation.History.Average";

} // namespace Average
} // namespace client
} // namespace History
} // namespace Aggregation
} // namespace Sensor
} // namespace open_power
} // namespace org
} // namespace sdbusplus

