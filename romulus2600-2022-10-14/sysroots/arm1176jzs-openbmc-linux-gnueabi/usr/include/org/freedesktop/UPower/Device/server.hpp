#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

































#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace org
{
namespace freedesktop
{
namespace UPower
{
namespace server
{

class Device
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Device() = delete;
        Device(const Device&) = delete;
        Device& operator=(const Device&) = delete;
        Device(Device&&) = delete;
        Device& operator=(Device&&) = delete;
        virtual ~Device() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Device(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                double,
                int64_t,
                std::string,
                uint32_t,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Device(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for Refresh
         *  "Refreshes the data collected from the power source. Callers need the
 org.freedesktop.upower.refresh-power-source authorization"
         */
        virtual void refresh(
            ) = 0;

        /** @brief Implementation for GetHistory
         *  "Gets history for the power device that is persistent across
 reboots."
         *
         *  @param[in] type - "The type of history. Valid types are rate or charge."
         *  @param[in] timespan - "The amount of data to return in seconds, or 0 for all."
         *  @param[in] resolution - "The approximate number of points to return. A higher resolution is
 more accurate, at the expense of plotting speed."
         *
         *  @return data[std::vector<std::tuple<uint32_t, double, uint32_t>>] - "The history data for the power device, if the device supports
 history. Data is ordered from the earliest in time, to the newest
 data point. Each element contains the following members:
   time: The time value in seconds from the gettimeofday() method.
   value: The data value, for instance the rate in W or the charge
          in %.
   state: The state of the device, for instance charging or
          discharging."
         */
        virtual std::vector<std::tuple<uint32_t, double, uint32_t>> getHistory(
            std::string type,
            uint32_t timespan,
            uint32_t resolution) = 0;

        /** @brief Implementation for GetStatistics
         *  "Gets statistics for the power device that may be interesting to show
 on a graph in the session."
         *
         *  @param[in] type - "The mode for the statistics. Valid types are charging or
 discharging."
         *
         *  @return data[std::vector<std::tuple<double, double>>] - "The statistics data for the power device. Each element contains the
 following members:
   value: The value of the percentage point, usually in seconds.
   accuracy: The accuracy of the prediction in percent."
         */
        virtual std::vector<std::tuple<double, double>> getStatistics(
            std::string type) = 0;


        /** Get value of NativePath */
        virtual std::string nativePath() const;
        /** Set value of NativePath with option to skip sending signal */
        virtual std::string nativePath(std::string value,
               bool skipSignal);
        /** Set value of NativePath */
        virtual std::string nativePath(std::string value);
        /** Get value of Vendor */
        virtual std::string vendor() const;
        /** Set value of Vendor with option to skip sending signal */
        virtual std::string vendor(std::string value,
               bool skipSignal);
        /** Set value of Vendor */
        virtual std::string vendor(std::string value);
        /** Get value of Model */
        virtual std::string model() const;
        /** Set value of Model with option to skip sending signal */
        virtual std::string model(std::string value,
               bool skipSignal);
        /** Set value of Model */
        virtual std::string model(std::string value);
        /** Get value of Serial */
        virtual std::string serial() const;
        /** Set value of Serial with option to skip sending signal */
        virtual std::string serial(std::string value,
               bool skipSignal);
        /** Set value of Serial */
        virtual std::string serial(std::string value);
        /** Get value of UpdateTime */
        virtual uint64_t updateTime() const;
        /** Set value of UpdateTime with option to skip sending signal */
        virtual uint64_t updateTime(uint64_t value,
               bool skipSignal);
        /** Set value of UpdateTime */
        virtual uint64_t updateTime(uint64_t value);
        /** Get value of Type */
        virtual uint32_t type() const;
        /** Set value of Type with option to skip sending signal */
        virtual uint32_t type(uint32_t value,
               bool skipSignal);
        /** Set value of Type */
        virtual uint32_t type(uint32_t value);
        /** Get value of PowerSupply */
        virtual bool powerSupply() const;
        /** Set value of PowerSupply with option to skip sending signal */
        virtual bool powerSupply(bool value,
               bool skipSignal);
        /** Set value of PowerSupply */
        virtual bool powerSupply(bool value);
        /** Get value of HasHistory */
        virtual bool hasHistory() const;
        /** Set value of HasHistory with option to skip sending signal */
        virtual bool hasHistory(bool value,
               bool skipSignal);
        /** Set value of HasHistory */
        virtual bool hasHistory(bool value);
        /** Get value of HasStatistics */
        virtual bool hasStatistics() const;
        /** Set value of HasStatistics with option to skip sending signal */
        virtual bool hasStatistics(bool value,
               bool skipSignal);
        /** Set value of HasStatistics */
        virtual bool hasStatistics(bool value);
        /** Get value of Online */
        virtual bool online() const;
        /** Set value of Online with option to skip sending signal */
        virtual bool online(bool value,
               bool skipSignal);
        /** Set value of Online */
        virtual bool online(bool value);
        /** Get value of Energy */
        virtual double energy() const;
        /** Set value of Energy with option to skip sending signal */
        virtual double energy(double value,
               bool skipSignal);
        /** Set value of Energy */
        virtual double energy(double value);
        /** Get value of EnergyEmpty */
        virtual double energyEmpty() const;
        /** Set value of EnergyEmpty with option to skip sending signal */
        virtual double energyEmpty(double value,
               bool skipSignal);
        /** Set value of EnergyEmpty */
        virtual double energyEmpty(double value);
        /** Get value of EnergyFull */
        virtual double energyFull() const;
        /** Set value of EnergyFull with option to skip sending signal */
        virtual double energyFull(double value,
               bool skipSignal);
        /** Set value of EnergyFull */
        virtual double energyFull(double value);
        /** Get value of EnergyFullDesign */
        virtual double energyFullDesign() const;
        /** Set value of EnergyFullDesign with option to skip sending signal */
        virtual double energyFullDesign(double value,
               bool skipSignal);
        /** Set value of EnergyFullDesign */
        virtual double energyFullDesign(double value);
        /** Get value of EnergyRate */
        virtual double energyRate() const;
        /** Set value of EnergyRate with option to skip sending signal */
        virtual double energyRate(double value,
               bool skipSignal);
        /** Set value of EnergyRate */
        virtual double energyRate(double value);
        /** Get value of Voltage */
        virtual double voltage() const;
        /** Set value of Voltage with option to skip sending signal */
        virtual double voltage(double value,
               bool skipSignal);
        /** Set value of Voltage */
        virtual double voltage(double value);
        /** Get value of Luminosity */
        virtual double luminosity() const;
        /** Set value of Luminosity with option to skip sending signal */
        virtual double luminosity(double value,
               bool skipSignal);
        /** Set value of Luminosity */
        virtual double luminosity(double value);
        /** Get value of TimeToEmpty */
        virtual int64_t timeToEmpty() const;
        /** Set value of TimeToEmpty with option to skip sending signal */
        virtual int64_t timeToEmpty(int64_t value,
               bool skipSignal);
        /** Set value of TimeToEmpty */
        virtual int64_t timeToEmpty(int64_t value);
        /** Get value of TimeToFull */
        virtual int64_t timeToFull() const;
        /** Set value of TimeToFull with option to skip sending signal */
        virtual int64_t timeToFull(int64_t value,
               bool skipSignal);
        /** Set value of TimeToFull */
        virtual int64_t timeToFull(int64_t value);
        /** Get value of Percentage */
        virtual double percentage() const;
        /** Set value of Percentage with option to skip sending signal */
        virtual double percentage(double value,
               bool skipSignal);
        /** Set value of Percentage */
        virtual double percentage(double value);
        /** Get value of Temperature */
        virtual double temperature() const;
        /** Set value of Temperature with option to skip sending signal */
        virtual double temperature(double value,
               bool skipSignal);
        /** Set value of Temperature */
        virtual double temperature(double value);
        /** Get value of IsPresent */
        virtual bool isPresent() const;
        /** Set value of IsPresent with option to skip sending signal */
        virtual bool isPresent(bool value,
               bool skipSignal);
        /** Set value of IsPresent */
        virtual bool isPresent(bool value);
        /** Get value of State */
        virtual uint32_t state() const;
        /** Set value of State with option to skip sending signal */
        virtual uint32_t state(uint32_t value,
               bool skipSignal);
        /** Set value of State */
        virtual uint32_t state(uint32_t value);
        /** Get value of IsRechargeable */
        virtual bool isRechargeable() const;
        /** Set value of IsRechargeable with option to skip sending signal */
        virtual bool isRechargeable(bool value,
               bool skipSignal);
        /** Set value of IsRechargeable */
        virtual bool isRechargeable(bool value);
        /** Get value of Capacity */
        virtual double capacity() const;
        /** Set value of Capacity with option to skip sending signal */
        virtual double capacity(double value,
               bool skipSignal);
        /** Set value of Capacity */
        virtual double capacity(double value);
        /** Get value of Technology */
        virtual uint32_t technology() const;
        /** Set value of Technology with option to skip sending signal */
        virtual uint32_t technology(uint32_t value,
               bool skipSignal);
        /** Set value of Technology */
        virtual uint32_t technology(uint32_t value);
        /** Get value of WarningLevel */
        virtual uint32_t warningLevel() const;
        /** Set value of WarningLevel with option to skip sending signal */
        virtual uint32_t warningLevel(uint32_t value,
               bool skipSignal);
        /** Set value of WarningLevel */
        virtual uint32_t warningLevel(uint32_t value);
        /** Get value of BatteryLevel */
        virtual uint32_t batteryLevel() const;
        /** Set value of BatteryLevel with option to skip sending signal */
        virtual uint32_t batteryLevel(uint32_t value,
               bool skipSignal);
        /** Set value of BatteryLevel */
        virtual uint32_t batteryLevel(uint32_t value);
        /** Get value of IconName */
        virtual std::string iconName() const;
        /** Set value of IconName with option to skip sending signal */
        virtual std::string iconName(std::string value,
               bool skipSignal);
        /** Set value of IconName */
        virtual std::string iconName(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _org_freedesktop_UPower_Device_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _org_freedesktop_UPower_Device_interface.emit_removed();
        }

        static constexpr auto interface = "org.freedesktop.UPower.Device";

    private:

        /** @brief sd-bus callback for Refresh
         */
        static int _callback_Refresh(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetHistory
         */
        static int _callback_GetHistory(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetStatistics
         */
        static int _callback_GetStatistics(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'NativePath' */
        static int _callback_get_NativePath(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NativePath' */
        static int _callback_set_NativePath(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Vendor' */
        static int _callback_get_Vendor(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Vendor' */
        static int _callback_set_Vendor(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Model' */
        static int _callback_get_Model(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Model' */
        static int _callback_set_Model(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Serial' */
        static int _callback_get_Serial(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Serial' */
        static int _callback_set_Serial(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UpdateTime' */
        static int _callback_get_UpdateTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UpdateTime' */
        static int _callback_set_UpdateTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Type' */
        static int _callback_get_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Type' */
        static int _callback_set_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PowerSupply' */
        static int _callback_get_PowerSupply(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerSupply' */
        static int _callback_set_PowerSupply(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HasHistory' */
        static int _callback_get_HasHistory(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HasHistory' */
        static int _callback_set_HasHistory(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HasStatistics' */
        static int _callback_get_HasStatistics(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HasStatistics' */
        static int _callback_set_HasStatistics(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Online' */
        static int _callback_get_Online(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Online' */
        static int _callback_set_Online(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Energy' */
        static int _callback_get_Energy(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Energy' */
        static int _callback_set_Energy(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EnergyEmpty' */
        static int _callback_get_EnergyEmpty(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EnergyEmpty' */
        static int _callback_set_EnergyEmpty(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EnergyFull' */
        static int _callback_get_EnergyFull(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EnergyFull' */
        static int _callback_set_EnergyFull(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EnergyFullDesign' */
        static int _callback_get_EnergyFullDesign(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EnergyFullDesign' */
        static int _callback_set_EnergyFullDesign(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EnergyRate' */
        static int _callback_get_EnergyRate(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EnergyRate' */
        static int _callback_set_EnergyRate(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Voltage' */
        static int _callback_get_Voltage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Voltage' */
        static int _callback_set_Voltage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Luminosity' */
        static int _callback_get_Luminosity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Luminosity' */
        static int _callback_set_Luminosity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TimeToEmpty' */
        static int _callback_get_TimeToEmpty(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TimeToEmpty' */
        static int _callback_set_TimeToEmpty(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TimeToFull' */
        static int _callback_get_TimeToFull(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TimeToFull' */
        static int _callback_set_TimeToFull(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Percentage' */
        static int _callback_get_Percentage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Percentage' */
        static int _callback_set_Percentage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Temperature' */
        static int _callback_get_Temperature(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Temperature' */
        static int _callback_set_Temperature(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IsPresent' */
        static int _callback_get_IsPresent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IsPresent' */
        static int _callback_set_IsPresent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'State' */
        static int _callback_get_State(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'State' */
        static int _callback_set_State(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IsRechargeable' */
        static int _callback_get_IsRechargeable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IsRechargeable' */
        static int _callback_set_IsRechargeable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Capacity' */
        static int _callback_get_Capacity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Capacity' */
        static int _callback_set_Capacity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Technology' */
        static int _callback_get_Technology(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Technology' */
        static int _callback_set_Technology(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WarningLevel' */
        static int _callback_get_WarningLevel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'WarningLevel' */
        static int _callback_set_WarningLevel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'BatteryLevel' */
        static int _callback_get_BatteryLevel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'BatteryLevel' */
        static int _callback_set_BatteryLevel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IconName' */
        static int _callback_get_IconName(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IconName' */
        static int _callback_set_IconName(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _org_freedesktop_UPower_Device_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _nativePath = "";
        std::string _vendor = "";
        std::string _model = "";
        std::string _serial = "";
        uint64_t _updateTime = 0;
        uint32_t _type = 0;
        bool _powerSupply = false;
        bool _hasHistory = false;
        bool _hasStatistics = false;
        bool _online = false;
        double _energy = 0.0;
        double _energyEmpty = 0.0;
        double _energyFull = 0.0;
        double _energyFullDesign = 0.0;
        double _energyRate = 0.0;
        double _voltage = 0.0;
        double _luminosity = 0.0;
        int64_t _timeToEmpty = 0;
        int64_t _timeToFull = 0;
        double _percentage = 0.0;
        double _temperature = 0.0;
        bool _isPresent = false;
        uint32_t _state = 0;
        bool _isRechargeable = false;
        double _capacity = 0.0;
        uint32_t _technology = 0;
        uint32_t _warningLevel = 0;
        uint32_t _batteryLevel = 0;
        std::string _iconName = "";

};


} // namespace server
} // namespace UPower
} // namespace freedesktop
} // namespace org

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

