#pragma once


namespace sdbusplus
{
namespace org
{
namespace freedesktop
{
namespace UPower
{
namespace client
{
namespace Device
{

static constexpr auto interface = "org.freedesktop.UPower.Device";

} // namespace Device
} // namespace client
} // namespace UPower
} // namespace freedesktop
} // namespace org
} // namespace sdbusplus

