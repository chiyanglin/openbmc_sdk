#pragma once


namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Protocol
{
namespace PECI
{
namespace client
{
namespace Raw
{

static constexpr auto interface = "com.intel.Protocol.PECI.Raw";

} // namespace Raw
} // namespace client
} // namespace PECI
} // namespace Protocol
} // namespace intel
} // namespace com
} // namespace sdbusplus

