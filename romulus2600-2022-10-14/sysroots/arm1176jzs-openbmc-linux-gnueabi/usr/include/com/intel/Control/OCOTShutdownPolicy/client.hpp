#pragma once


namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Control
{
namespace client
{
namespace OCOTShutdownPolicy
{

static constexpr auto interface = "com.intel.Control.OCOTShutdownPolicy";

} // namespace OCOTShutdownPolicy
} // namespace client
} // namespace Control
} // namespace intel
} // namespace com
} // namespace sdbusplus

