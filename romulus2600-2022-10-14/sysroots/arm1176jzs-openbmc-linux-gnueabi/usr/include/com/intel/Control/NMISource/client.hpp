#pragma once


namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Control
{
namespace client
{
namespace NMISource
{

static constexpr auto interface = "com.intel.Control.NMISource";

} // namespace NMISource
} // namespace client
} // namespace Control
} // namespace intel
} // namespace com
} // namespace sdbusplus

