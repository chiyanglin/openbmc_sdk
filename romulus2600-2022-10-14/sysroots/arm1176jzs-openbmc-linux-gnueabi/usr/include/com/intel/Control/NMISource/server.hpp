#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Control
{
namespace server
{

class NMISource
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        NMISource() = delete;
        NMISource(const NMISource&) = delete;
        NMISource& operator=(const NMISource&) = delete;
        NMISource(NMISource&&) = delete;
        NMISource& operator=(NMISource&&) = delete;
        virtual ~NMISource() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        NMISource(bus_t& bus, const char* path);

        enum class BMCSourceSignal
        {
            None,
            FpBtn,
            WdPreTimeout,
            PefMatch,
            ChassisCmd,
            MemoryError,
            PciSerrPerr,
            SouthbridgeNmi,
            ChipsetNmi,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                BMCSourceSignal,
                bool>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        NMISource(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of BMCSource */
        virtual BMCSourceSignal bmcSource() const;
        /** Set value of BMCSource with option to skip sending signal */
        virtual BMCSourceSignal bmcSource(BMCSourceSignal value,
               bool skipSignal);
        /** Set value of BMCSource */
        virtual BMCSourceSignal bmcSource(BMCSourceSignal value);
        /** Get value of Enabled */
        virtual bool enabled() const;
        /** Set value of Enabled with option to skip sending signal */
        virtual bool enabled(bool value,
               bool skipSignal);
        /** Set value of Enabled */
        virtual bool enabled(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.intel.Control.NMISource.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static BMCSourceSignal convertBMCSourceSignalFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.intel.Control.NMISource.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<BMCSourceSignal> convertStringToBMCSourceSignal(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "com.intel.Control.NMISource.<value name>"
         */
        static std::string convertBMCSourceSignalToString(BMCSourceSignal e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _com_intel_Control_NMISource_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_intel_Control_NMISource_interface.emit_removed();
        }

        static constexpr auto interface = "com.intel.Control.NMISource";

    private:

        /** @brief sd-bus callback for get-property 'BMCSource' */
        static int _callback_get_BMCSource(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'BMCSource' */
        static int _callback_set_BMCSource(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Enabled' */
        static int _callback_get_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Enabled' */
        static int _callback_set_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_intel_Control_NMISource_interface;
        sdbusplus::SdBusInterface *_intf;

        BMCSourceSignal _bmcSource = BMCSourceSignal::None;
        bool _enabled = true;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type NMISource::BMCSourceSignal.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(NMISource::BMCSourceSignal e)
{
    return NMISource::convertBMCSourceSignalToString(e);
}

} // namespace server
} // namespace Control
} // namespace intel
} // namespace com

namespace message::details
{
template <>
struct convert_from_string<com::intel::Control::server::NMISource::BMCSourceSignal>
{
    static auto op(const std::string& value) noexcept
    {
        return com::intel::Control::server::NMISource::convertStringToBMCSourceSignal(value);
    }
};

template <>
struct convert_to_string<com::intel::Control::server::NMISource::BMCSourceSignal>
{
    static std::string op(com::intel::Control::server::NMISource::BMCSourceSignal value)
    {
        return com::intel::Control::server::NMISource::convertBMCSourceSignalToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

