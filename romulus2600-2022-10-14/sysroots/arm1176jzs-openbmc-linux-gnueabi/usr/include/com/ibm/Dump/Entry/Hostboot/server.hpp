#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace Entry
{
namespace server
{

class Hostboot
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Hostboot() = delete;
        Hostboot(const Hostboot&) = delete;
        Hostboot& operator=(const Hostboot&) = delete;
        Hostboot(Hostboot&&) = delete;
        Hostboot& operator=(Hostboot&&) = delete;
        virtual ~Hostboot() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Hostboot(bus_t& bus, const char* path);






        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_Dump_Entry_Hostboot_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_Dump_Entry_Hostboot_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.Dump.Entry.Hostboot";

    private:


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_Dump_Entry_Hostboot_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

