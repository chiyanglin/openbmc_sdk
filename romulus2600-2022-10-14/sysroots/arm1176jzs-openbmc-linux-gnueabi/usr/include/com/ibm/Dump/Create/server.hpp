#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);

        enum class CreateParameters
        {
            VSPString,
            Password,
            ErrorLogId,
            DumpType,
            FailingUnitId,
        };
        enum class DumpType
        {
            Hostboot,
            Hardware,
        };




        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.ibm.Dump.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static CreateParameters convertCreateParametersFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.ibm.Dump.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<CreateParameters> convertStringToCreateParameters(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "com.ibm.Dump.Create.<value name>"
         */
        static std::string convertCreateParametersToString(CreateParameters e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.ibm.Dump.Create.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DumpType convertDumpTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.ibm.Dump.Create.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DumpType> convertStringToDumpType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "com.ibm.Dump.Create.<value name>"
         */
        static std::string convertDumpTypeToString(DumpType e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_Dump_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_Dump_Create_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.Dump.Create";

    private:


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_Dump_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::CreateParameters.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::CreateParameters e)
{
    return Create::convertCreateParametersToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Create::DumpType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Create::DumpType e)
{
    return Create::convertDumpTypeToString(e);
}

} // namespace server
} // namespace Dump
} // namespace ibm
} // namespace com

namespace message::details
{
template <>
struct convert_from_string<com::ibm::Dump::server::Create::CreateParameters>
{
    static auto op(const std::string& value) noexcept
    {
        return com::ibm::Dump::server::Create::convertStringToCreateParameters(value);
    }
};

template <>
struct convert_to_string<com::ibm::Dump::server::Create::CreateParameters>
{
    static std::string op(com::ibm::Dump::server::Create::CreateParameters value)
    {
        return com::ibm::Dump::server::Create::convertCreateParametersToString(value);
    }
};
template <>
struct convert_from_string<com::ibm::Dump::server::Create::DumpType>
{
    static auto op(const std::string& value) noexcept
    {
        return com::ibm::Dump::server::Create::convertStringToDumpType(value);
    }
};

template <>
struct convert_to_string<com::ibm::Dump::server::Create::DumpType>
{
    static std::string op(com::ibm::Dump::server::Create::DumpType value)
    {
        return com::ibm::Dump::server::Create::convertDumpTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

