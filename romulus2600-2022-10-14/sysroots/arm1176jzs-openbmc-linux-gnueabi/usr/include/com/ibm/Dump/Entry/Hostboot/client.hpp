#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace Entry
{
namespace client
{
namespace Hostboot
{

static constexpr auto interface = "com.ibm.Dump.Entry.Hostboot";

} // namespace Hostboot
} // namespace client
} // namespace Entry
} // namespace Dump
} // namespace ibm
} // namespace com
} // namespace sdbusplus

