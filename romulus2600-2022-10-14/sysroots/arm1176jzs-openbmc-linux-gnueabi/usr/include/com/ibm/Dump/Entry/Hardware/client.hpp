#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace Entry
{
namespace client
{
namespace Hardware
{

static constexpr auto interface = "com.ibm.Dump.Entry.Hardware";

} // namespace Hardware
} // namespace client
} // namespace Entry
} // namespace Dump
} // namespace ibm
} // namespace com
} // namespace sdbusplus

