#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace client
{
namespace Create
{

static constexpr auto interface = "com.ibm.Dump.Create";

} // namespace Create
} // namespace client
} // namespace Dump
} // namespace ibm
} // namespace com
} // namespace sdbusplus

