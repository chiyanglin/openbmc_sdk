#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace VPD
{
namespace client
{
namespace Manager
{

static constexpr auto interface = "com.ibm.VPD.Manager";

} // namespace Manager
} // namespace client
} // namespace VPD
} // namespace ibm
} // namespace com
} // namespace sdbusplus

