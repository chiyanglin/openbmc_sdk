#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace LWP1
{

static constexpr auto interface = "com.ibm.ipzvpd.LWP1";

} // namespace LWP1
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

