#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>








#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class VCEN
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        VCEN() = delete;
        VCEN(const VCEN&) = delete;
        VCEN& operator=(const VCEN&) = delete;
        VCEN(VCEN&&) = delete;
        VCEN& operator=(VCEN&&) = delete;
        virtual ~VCEN() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        VCEN(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        VCEN(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of DR */
        virtual std::vector<uint8_t> dr() const;
        /** Set value of DR with option to skip sending signal */
        virtual std::vector<uint8_t> dr(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DR */
        virtual std::vector<uint8_t> dr(std::vector<uint8_t> value);
        /** Get value of SE */
        virtual std::vector<uint8_t> se() const;
        /** Set value of SE with option to skip sending signal */
        virtual std::vector<uint8_t> se(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of SE */
        virtual std::vector<uint8_t> se(std::vector<uint8_t> value);
        /** Get value of TM */
        virtual std::vector<uint8_t> tm() const;
        /** Set value of TM with option to skip sending signal */
        virtual std::vector<uint8_t> tm(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of TM */
        virtual std::vector<uint8_t> tm(std::vector<uint8_t> value);
        /** Get value of FC */
        virtual std::vector<uint8_t> fc() const;
        /** Set value of FC with option to skip sending signal */
        virtual std::vector<uint8_t> fc(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of FC */
        virtual std::vector<uint8_t> fc(std::vector<uint8_t> value);
        /** Get value of RG */
        virtual std::vector<uint8_t> rg() const;
        /** Set value of RG with option to skip sending signal */
        virtual std::vector<uint8_t> rg(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RG */
        virtual std::vector<uint8_t> rg(std::vector<uint8_t> value);
        /** Get value of RB */
        virtual std::vector<uint8_t> rb() const;
        /** Set value of RB with option to skip sending signal */
        virtual std::vector<uint8_t> rb(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RB */
        virtual std::vector<uint8_t> rb(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_VCEN_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_VCEN_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.VCEN";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DR' */
        static int _callback_get_DR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DR' */
        static int _callback_set_DR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SE' */
        static int _callback_get_SE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SE' */
        static int _callback_set_SE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TM' */
        static int _callback_get_TM(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TM' */
        static int _callback_set_TM(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'FC' */
        static int _callback_get_FC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'FC' */
        static int _callback_set_FC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RG' */
        static int _callback_get_RG(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RG' */
        static int _callback_set_RG(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RB' */
        static int _callback_get_RB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RB' */
        static int _callback_set_RB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_VCEN_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _dr{};
        std::vector<uint8_t> _se{};
        std::vector<uint8_t> _tm{};
        std::vector<uint8_t> _fc{};
        std::vector<uint8_t> _rg{};
        std::vector<uint8_t> _rb{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

