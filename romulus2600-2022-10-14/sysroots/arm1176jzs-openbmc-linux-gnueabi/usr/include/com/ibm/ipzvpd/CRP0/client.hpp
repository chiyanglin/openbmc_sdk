#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace CRP0
{

static constexpr auto interface = "com.ibm.ipzvpd.CRP0";

} // namespace CRP0
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

