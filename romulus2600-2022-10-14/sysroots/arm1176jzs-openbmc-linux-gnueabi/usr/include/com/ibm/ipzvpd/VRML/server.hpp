#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>








#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class VRML
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        VRML() = delete;
        VRML(const VRML&) = delete;
        VRML& operator=(const VRML&) = delete;
        VRML(VRML&&) = delete;
        VRML& operator=(VRML&&) = delete;
        virtual ~VRML() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        VRML(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        VRML(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of VD */
        virtual std::vector<uint8_t> vd() const;
        /** Set value of VD with option to skip sending signal */
        virtual std::vector<uint8_t> vd(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of VD */
        virtual std::vector<uint8_t> vd(std::vector<uint8_t> value);
        /** Get value of PN */
        virtual std::vector<uint8_t> pn() const;
        /** Set value of PN with option to skip sending signal */
        virtual std::vector<uint8_t> pn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PN */
        virtual std::vector<uint8_t> pn(std::vector<uint8_t> value);
        /** Get value of SN */
        virtual std::vector<uint8_t> sn() const;
        /** Set value of SN with option to skip sending signal */
        virtual std::vector<uint8_t> sn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of SN */
        virtual std::vector<uint8_t> sn(std::vector<uint8_t> value);
        /** Get value of TV */
        virtual std::vector<uint8_t> tv() const;
        /** Set value of TV with option to skip sending signal */
        virtual std::vector<uint8_t> tv(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of TV */
        virtual std::vector<uint8_t> tv(std::vector<uint8_t> value);
        /** Get value of D4 */
        virtual std::vector<uint8_t> d4() const;
        /** Set value of D4 with option to skip sending signal */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D4 */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value);
        /** Get value of F5 */
        virtual std::vector<uint8_t> f5() const;
        /** Set value of F5 with option to skip sending signal */
        virtual std::vector<uint8_t> f5(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F5 */
        virtual std::vector<uint8_t> f5(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_VRML_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_VRML_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.VRML";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VD' */
        static int _callback_get_VD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VD' */
        static int _callback_set_VD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PN' */
        static int _callback_get_PN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PN' */
        static int _callback_set_PN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SN' */
        static int _callback_get_SN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SN' */
        static int _callback_set_SN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TV' */
        static int _callback_get_TV(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TV' */
        static int _callback_set_TV(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D4' */
        static int _callback_get_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D4' */
        static int _callback_set_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F5' */
        static int _callback_get_F5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F5' */
        static int _callback_set_F5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_VRML_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _vd{};
        std::vector<uint8_t> _pn{};
        std::vector<uint8_t> _sn{};
        std::vector<uint8_t> _tv{};
        std::vector<uint8_t> _d4{};
        std::vector<uint8_t> _f5{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

