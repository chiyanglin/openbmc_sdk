#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace LWP4
{

static constexpr auto interface = "com.ibm.ipzvpd.LWP4";

} // namespace LWP4
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

