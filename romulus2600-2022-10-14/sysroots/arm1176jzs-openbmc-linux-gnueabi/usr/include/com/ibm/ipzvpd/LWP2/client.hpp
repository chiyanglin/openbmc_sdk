#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace LWP2
{

static constexpr auto interface = "com.ibm.ipzvpd.LWP2";

} // namespace LWP2
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

