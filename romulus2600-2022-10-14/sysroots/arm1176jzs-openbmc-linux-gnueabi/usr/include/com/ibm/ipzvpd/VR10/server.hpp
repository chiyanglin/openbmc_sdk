#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class VR10
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        VR10() = delete;
        VR10(const VR10&) = delete;
        VR10& operator=(const VR10&) = delete;
        VR10(VR10&&) = delete;
        VR10& operator=(VR10&&) = delete;
        virtual ~VR10() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        VR10(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        VR10(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of DR */
        virtual std::vector<uint8_t> dr() const;
        /** Set value of DR with option to skip sending signal */
        virtual std::vector<uint8_t> dr(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DR */
        virtual std::vector<uint8_t> dr(std::vector<uint8_t> value);
        /** Get value of DC */
        virtual std::vector<uint8_t> dc() const;
        /** Set value of DC with option to skip sending signal */
        virtual std::vector<uint8_t> dc(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DC */
        virtual std::vector<uint8_t> dc(std::vector<uint8_t> value);
        /** Get value of FL */
        virtual std::vector<uint8_t> fl() const;
        /** Set value of FL with option to skip sending signal */
        virtual std::vector<uint8_t> fl(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of FL */
        virtual std::vector<uint8_t> fl(std::vector<uint8_t> value);
        /** Get value of WA */
        virtual std::vector<uint8_t> wa() const;
        /** Set value of WA with option to skip sending signal */
        virtual std::vector<uint8_t> wa(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of WA */
        virtual std::vector<uint8_t> wa(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_VR10_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_VR10_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.VR10";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DR' */
        static int _callback_get_DR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DR' */
        static int _callback_set_DR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DC' */
        static int _callback_get_DC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DC' */
        static int _callback_set_DC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'FL' */
        static int _callback_get_FL(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'FL' */
        static int _callback_set_FL(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WA' */
        static int _callback_get_WA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'WA' */
        static int _callback_set_WA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_VR10_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _dr{};
        std::vector<uint8_t> _dc{};
        std::vector<uint8_t> _fl{};
        std::vector<uint8_t> _wa{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

