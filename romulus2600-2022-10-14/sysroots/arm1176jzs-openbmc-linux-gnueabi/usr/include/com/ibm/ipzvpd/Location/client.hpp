#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace Location
{

static constexpr auto interface = "com.ibm.ipzvpd.Location";

} // namespace Location
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

