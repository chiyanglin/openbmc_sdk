#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace LWP0
{

static constexpr auto interface = "com.ibm.ipzvpd.LWP0";

} // namespace LWP0
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

