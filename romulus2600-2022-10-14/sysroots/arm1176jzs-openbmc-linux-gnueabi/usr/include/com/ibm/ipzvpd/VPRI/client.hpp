#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace VPRI
{

static constexpr auto interface = "com.ibm.ipzvpd.VPRI";

} // namespace VPRI
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

