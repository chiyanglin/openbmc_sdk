#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace LRP3
{

static constexpr auto interface = "com.ibm.ipzvpd.LRP3";

} // namespace LRP3
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

