#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace UTIL
{

static constexpr auto interface = "com.ibm.ipzvpd.UTIL";

} // namespace UTIL
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

