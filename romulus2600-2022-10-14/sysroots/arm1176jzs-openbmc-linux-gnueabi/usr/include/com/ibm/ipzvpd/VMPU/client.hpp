#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace VMPU
{

static constexpr auto interface = "com.ibm.ipzvpd.VMPU";

} // namespace VMPU
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

