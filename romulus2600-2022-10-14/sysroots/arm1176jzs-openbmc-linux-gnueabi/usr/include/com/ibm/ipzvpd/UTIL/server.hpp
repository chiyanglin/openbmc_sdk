#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class UTIL
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        UTIL() = delete;
        UTIL(const UTIL&) = delete;
        UTIL& operator=(const UTIL&) = delete;
        UTIL(UTIL&&) = delete;
        UTIL& operator=(UTIL&&) = delete;
        virtual ~UTIL() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        UTIL(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        UTIL(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of D0 */
        virtual std::vector<uint8_t> d0() const;
        /** Set value of D0 with option to skip sending signal */
        virtual std::vector<uint8_t> d0(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D0 */
        virtual std::vector<uint8_t> d0(std::vector<uint8_t> value);
        /** Get value of D1 */
        virtual std::vector<uint8_t> d1() const;
        /** Set value of D1 with option to skip sending signal */
        virtual std::vector<uint8_t> d1(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D1 */
        virtual std::vector<uint8_t> d1(std::vector<uint8_t> value);
        /** Get value of D2 */
        virtual std::vector<uint8_t> d2() const;
        /** Set value of D2 with option to skip sending signal */
        virtual std::vector<uint8_t> d2(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D2 */
        virtual std::vector<uint8_t> d2(std::vector<uint8_t> value);
        /** Get value of D3 */
        virtual std::vector<uint8_t> d3() const;
        /** Set value of D3 with option to skip sending signal */
        virtual std::vector<uint8_t> d3(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D3 */
        virtual std::vector<uint8_t> d3(std::vector<uint8_t> value);
        /** Get value of D4 */
        virtual std::vector<uint8_t> d4() const;
        /** Set value of D4 with option to skip sending signal */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D4 */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value);
        /** Get value of D5 */
        virtual std::vector<uint8_t> d5() const;
        /** Set value of D5 with option to skip sending signal */
        virtual std::vector<uint8_t> d5(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D5 */
        virtual std::vector<uint8_t> d5(std::vector<uint8_t> value);
        /** Get value of D6 */
        virtual std::vector<uint8_t> d6() const;
        /** Set value of D6 with option to skip sending signal */
        virtual std::vector<uint8_t> d6(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D6 */
        virtual std::vector<uint8_t> d6(std::vector<uint8_t> value);
        /** Get value of D7 */
        virtual std::vector<uint8_t> d7() const;
        /** Set value of D7 with option to skip sending signal */
        virtual std::vector<uint8_t> d7(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D7 */
        virtual std::vector<uint8_t> d7(std::vector<uint8_t> value);
        /** Get value of D8 */
        virtual std::vector<uint8_t> d8() const;
        /** Set value of D8 with option to skip sending signal */
        virtual std::vector<uint8_t> d8(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D8 */
        virtual std::vector<uint8_t> d8(std::vector<uint8_t> value);
        /** Get value of D9 */
        virtual std::vector<uint8_t> d9() const;
        /** Set value of D9 with option to skip sending signal */
        virtual std::vector<uint8_t> d9(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D9 */
        virtual std::vector<uint8_t> d9(std::vector<uint8_t> value);
        /** Get value of F0 */
        virtual std::vector<uint8_t> f0() const;
        /** Set value of F0 with option to skip sending signal */
        virtual std::vector<uint8_t> f0(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F0 */
        virtual std::vector<uint8_t> f0(std::vector<uint8_t> value);
        /** Get value of F1 */
        virtual std::vector<uint8_t> f1() const;
        /** Set value of F1 with option to skip sending signal */
        virtual std::vector<uint8_t> f1(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F1 */
        virtual std::vector<uint8_t> f1(std::vector<uint8_t> value);
        /** Get value of F2 */
        virtual std::vector<uint8_t> f2() const;
        /** Set value of F2 with option to skip sending signal */
        virtual std::vector<uint8_t> f2(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F2 */
        virtual std::vector<uint8_t> f2(std::vector<uint8_t> value);
        /** Get value of F3 */
        virtual std::vector<uint8_t> f3() const;
        /** Set value of F3 with option to skip sending signal */
        virtual std::vector<uint8_t> f3(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F3 */
        virtual std::vector<uint8_t> f3(std::vector<uint8_t> value);
        /** Get value of F4 */
        virtual std::vector<uint8_t> f4() const;
        /** Set value of F4 with option to skip sending signal */
        virtual std::vector<uint8_t> f4(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F4 */
        virtual std::vector<uint8_t> f4(std::vector<uint8_t> value);
        /** Get value of F5 */
        virtual std::vector<uint8_t> f5() const;
        /** Set value of F5 with option to skip sending signal */
        virtual std::vector<uint8_t> f5(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F5 */
        virtual std::vector<uint8_t> f5(std::vector<uint8_t> value);
        /** Get value of F6 */
        virtual std::vector<uint8_t> f6() const;
        /** Set value of F6 with option to skip sending signal */
        virtual std::vector<uint8_t> f6(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F6 */
        virtual std::vector<uint8_t> f6(std::vector<uint8_t> value);
        /** Get value of F7 */
        virtual std::vector<uint8_t> f7() const;
        /** Set value of F7 with option to skip sending signal */
        virtual std::vector<uint8_t> f7(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F7 */
        virtual std::vector<uint8_t> f7(std::vector<uint8_t> value);
        /** Get value of F8 */
        virtual std::vector<uint8_t> f8() const;
        /** Set value of F8 with option to skip sending signal */
        virtual std::vector<uint8_t> f8(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F8 */
        virtual std::vector<uint8_t> f8(std::vector<uint8_t> value);
        /** Get value of F9 */
        virtual std::vector<uint8_t> f9() const;
        /** Set value of F9 with option to skip sending signal */
        virtual std::vector<uint8_t> f9(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F9 */
        virtual std::vector<uint8_t> f9(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_UTIL_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_UTIL_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.UTIL";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D0' */
        static int _callback_get_D0(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D0' */
        static int _callback_set_D0(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D1' */
        static int _callback_get_D1(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D1' */
        static int _callback_set_D1(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D2' */
        static int _callback_get_D2(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D2' */
        static int _callback_set_D2(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D3' */
        static int _callback_get_D3(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D3' */
        static int _callback_set_D3(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D4' */
        static int _callback_get_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D4' */
        static int _callback_set_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D5' */
        static int _callback_get_D5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D5' */
        static int _callback_set_D5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D6' */
        static int _callback_get_D6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D6' */
        static int _callback_set_D6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D7' */
        static int _callback_get_D7(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D7' */
        static int _callback_set_D7(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D8' */
        static int _callback_get_D8(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D8' */
        static int _callback_set_D8(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D9' */
        static int _callback_get_D9(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D9' */
        static int _callback_set_D9(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F0' */
        static int _callback_get_F0(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F0' */
        static int _callback_set_F0(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F1' */
        static int _callback_get_F1(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F1' */
        static int _callback_set_F1(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F2' */
        static int _callback_get_F2(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F2' */
        static int _callback_set_F2(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F3' */
        static int _callback_get_F3(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F3' */
        static int _callback_set_F3(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F4' */
        static int _callback_get_F4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F4' */
        static int _callback_set_F4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F5' */
        static int _callback_get_F5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F5' */
        static int _callback_set_F5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F6' */
        static int _callback_get_F6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F6' */
        static int _callback_set_F6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F7' */
        static int _callback_get_F7(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F7' */
        static int _callback_set_F7(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F8' */
        static int _callback_get_F8(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F8' */
        static int _callback_set_F8(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F9' */
        static int _callback_get_F9(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F9' */
        static int _callback_set_F9(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_UTIL_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _d0{};
        std::vector<uint8_t> _d1{};
        std::vector<uint8_t> _d2{};
        std::vector<uint8_t> _d3{};
        std::vector<uint8_t> _d4{};
        std::vector<uint8_t> _d5{};
        std::vector<uint8_t> _d6{};
        std::vector<uint8_t> _d7{};
        std::vector<uint8_t> _d8{};
        std::vector<uint8_t> _d9{};
        std::vector<uint8_t> _f0{};
        std::vector<uint8_t> _f1{};
        std::vector<uint8_t> _f2{};
        std::vector<uint8_t> _f3{};
        std::vector<uint8_t> _f4{};
        std::vector<uint8_t> _f5{};
        std::vector<uint8_t> _f6{};
        std::vector<uint8_t> _f7{};
        std::vector<uint8_t> _f8{};
        std::vector<uint8_t> _f9{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

