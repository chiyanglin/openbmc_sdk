#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace VSYS
{

static constexpr auto interface = "com.ibm.ipzvpd.VSYS";

} // namespace VSYS
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

