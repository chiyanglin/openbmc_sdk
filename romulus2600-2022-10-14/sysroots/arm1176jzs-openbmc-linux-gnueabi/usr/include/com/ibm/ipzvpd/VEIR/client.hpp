#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace client
{
namespace VEIR
{

static constexpr auto interface = "com.ibm.ipzvpd.VEIR";

} // namespace VEIR
} // namespace client
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

