#pragma once


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Logging
{
namespace client
{
namespace Policy
{

static constexpr auto interface = "com.ibm.Logging.Policy";

} // namespace Policy
} // namespace client
} // namespace Logging
} // namespace ibm
} // namespace com
} // namespace sdbusplus

