#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace com
{
namespace google
{
namespace gbmc
{
namespace Hoth
{
namespace Error
{

struct CommandFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.google.gbmc.Hoth.Error.CommandFailure";
    static constexpr auto errDesc =
            "Failed to send the command to Hoth.";
    static constexpr auto errWhat =
            "com.google.gbmc.Hoth.Error.CommandFailure: Failed to send the command to Hoth.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct ResponseFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.google.gbmc.Hoth.Error.ResponseFailure";
    static constexpr auto errDesc =
            "Failed to retrieve response from Hoth.";
    static constexpr auto errWhat =
            "com.google.gbmc.Hoth.Error.ResponseFailure: Failed to retrieve response from Hoth.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct FirmwareFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.google.gbmc.Hoth.Error.FirmwareFailure";
    static constexpr auto errDesc =
            "Failed to send firmware update to Hoth.";
    static constexpr auto errWhat =
            "com.google.gbmc.Hoth.Error.FirmwareFailure: Failed to send firmware update to Hoth.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct ResponseNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.google.gbmc.Hoth.Error.ResponseNotFound";
    static constexpr auto errDesc =
            "Could not find a response. The command may not have completed yet.";
    static constexpr auto errWhat =
            "com.google.gbmc.Hoth.Error.ResponseNotFound: Could not find a response. The command may not have completed yet.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InterfaceError final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.google.gbmc.Hoth.Error.InterfaceError";
    static constexpr auto errDesc =
            "A system error occured with the Hoth interface.";
    static constexpr auto errWhat =
            "com.google.gbmc.Hoth.Error.InterfaceError: A system error occured with the Hoth interface.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct ExpectedInfoNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.google.gbmc.Hoth.Error.ExpectedInfoNotFound";
    static constexpr auto errDesc =
            "Could not find the expected information in the response.";
    static constexpr auto errWhat =
            "com.google.gbmc.Hoth.Error.ExpectedInfoNotFound: Could not find the expected information in the response.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Hoth
} // namespace gbmc
} // namespace google
} // namespace com
} // namespace sdbusplus

