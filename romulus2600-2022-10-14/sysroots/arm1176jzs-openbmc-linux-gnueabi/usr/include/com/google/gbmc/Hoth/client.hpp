#pragma once


namespace sdbusplus
{
namespace com
{
namespace google
{
namespace gbmc
{
namespace client
{
namespace Hoth
{

static constexpr auto interface = "com.google.gbmc.Hoth";

} // namespace Hoth
} // namespace client
} // namespace gbmc
} // namespace google
} // namespace com
} // namespace sdbusplus

