/*
 * et-c-kadm_err.c:
 * This file is automatically generated; please do not edit it.
 */

#include <stdlib.h>

#define N_(a) a

static const char * const text[] = {
	N_("Operation failed for unspecified reason"),
	N_("Operation requires ``get'' privilege"),
	N_("Operation requires ``add'' privilege"),
	N_("Operation requires ``modify'' privilege"),
	N_("Operation requires ``delete'' privilege"),
	N_("Insufficient authorization for operation"),
	N_("Database inconsistency detected"),
	N_("Principal or policy already exists"),
	N_("Communication failure with server"),
	N_("No administration server found for realm"),
	N_("Password history principal key version mismatch"),
	N_("Connection to server not initialized"),
	N_("Principal does not exist"),
	N_("Policy does not exist"),
	N_("Invalid field mask for operation"),
	N_("Invalid number of character classes"),
	N_("Invalid password length"),
	N_("Illegal policy name"),
	N_("Illegal principal name"),
	N_("Invalid auxillary attributes"),
	N_("Invalid password history count"),
	N_("Password minimum life is greater than password maximum life"),
	N_("Password is too short"),
	N_("Password does not contain enough character classes"),
	N_("Password is in the password dictionary"),
	N_("Cannot reuse password"),
	N_("Current password's minimum life has not expired"),
	N_("Policy is in use"),
	N_("Connection to server already initialized"),
	N_("Incorrect password"),
	N_("Cannot change protected principal"),
	N_("Programmer error! Bad Admin server handle"),
	N_("Programmer error! Bad API structure version"),
	N_("API structure version specified by application is no longer supported (to fix, recompile application against current KADM5 API header files and libraries)"),
	N_("API structure version specified by application is unknown to libraries (to fix, obtain current KADM5 API header files and libraries and recompile application)"),
	N_("Programmer error! Bad API version"),
	N_("API version specified by application is no longer supported by libraries (to fix, update application to adhere to current API version and recompile)"),
	N_("API version specified by application is no longer supported by server (to fix, update application to adhere to current API version and recompile)"),
	N_("API version specified by application is unknown to libraries (to fix, obtain current KADM5 API header files and libraries and recompile application)"),
	N_("API version specified by application is unknown to server (to fix, obtain and install newest KADM5 Admin Server)"),
	N_("Database error! Required KADM5 principal missing"),
	N_("The salt type of the specified principal does not support renaming"),
	N_("Illegal configuration parameter for remote KADM5 client"),
	N_("Illegal configuration parameter for local KADM5 client"),
	N_("Operation requires ``list'' privilege"),
	N_("Operation requires ``change-password'' privilege"),
	N_("GSS-API (or Kerberos) error"),
	N_("Programmer error! Illegal tagged data list type"),
	N_("Required parameters in kdc.conf missing"),
	N_("Bad krb5 admin server hostname"),
	N_("Operation requires ``set-key'' privilege"),
	N_("Multiple values for single or folded enctype"),
	N_("Invalid enctype for setv4key"),
	N_("Mismatched enctypes for setkey3"),
	N_("Missing parameters in krb5.conf required for kadmin client"),
	N_("XDR encoding error"),
	N_("Cannot resolve network address for admin server in requested realm"),
	N_("Unspecified password quality failure"),
	N_("Invalid key/salt tuples"),
	N_("Invalid multiple or duplicate kvnos in setkey operation"),
	N_("Operation requires ``extract-keys'' privilege"),
	N_("Principal keys are locked down"),
	N_("Operation requires initial ticket"),
    0
};

struct error_table {
    char const * const * msgs;
    long base;
    int n_msgs;
};
struct et_list {
    struct et_list *next;
    const struct error_table * table;
};
extern struct et_list *_et_list;

const struct error_table et_ovk_error_table = { text, 43787520L, 63 };

static struct et_list link = { 0, 0 };

void initialize_ovk_error_table_r(struct et_list **list);
void initialize_ovk_error_table(void);

void initialize_ovk_error_table(void) {
    initialize_ovk_error_table_r(&_et_list);
}

/* For Heimdal compatibility */
void initialize_ovk_error_table_r(struct et_list **list)
{
    struct et_list *et, **end;

    for (end = list, et = *list; et; end = &et->next, et = et->next)
        if (et->table->msgs == text)
            return;
    et = malloc(sizeof(struct et_list));
    if (et == 0) {
        if (!link.table)
            et = &link;
        else
            return;
    }
    et->table = &et_ovk_error_table;
    et->next = 0;
    *end = et;
}
