#include <xyz/openbmc_project/Software/Image/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace Image
{
namespace Error
{
const char* UnTarFailure::name() const noexcept
{
    return errName;
}
const char* UnTarFailure::description() const noexcept
{
    return errDesc;
}
const char* UnTarFailure::what() const noexcept
{
    return errWhat;
}
const char* ManifestFileFailure::name() const noexcept
{
    return errName;
}
const char* ManifestFileFailure::description() const noexcept
{
    return errDesc;
}
const char* ManifestFileFailure::what() const noexcept
{
    return errWhat;
}
const char* InternalFailure::name() const noexcept
{
    return errName;
}
const char* InternalFailure::description() const noexcept
{
    return errDesc;
}
const char* InternalFailure::what() const noexcept
{
    return errWhat;
}
const char* ImageFailure::name() const noexcept
{
    return errName;
}
const char* ImageFailure::description() const noexcept
{
    return errDesc;
}
const char* ImageFailure::what() const noexcept
{
    return errWhat;
}
const char* BusyFailure::name() const noexcept
{
    return errName;
}
const char* BusyFailure::description() const noexcept
{
    return errDesc;
}
const char* BusyFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Image
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

