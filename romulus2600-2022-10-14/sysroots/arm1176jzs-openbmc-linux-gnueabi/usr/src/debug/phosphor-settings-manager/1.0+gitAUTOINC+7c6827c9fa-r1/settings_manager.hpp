// WARNING: Generated header. Do not edit!
#pragma once

#include <cereal/archives/json.hpp>
#include <cereal/types/vector.hpp>
#include <fstream>
#include <utility>
#include <filesystem>
#include <regex>
#include <phosphor-logging/elog.hpp>
#include <phosphor-logging/elog-errors.hpp>
#include <phosphor-logging/log.hpp>
#include "config.h"
#include <xyz/openbmc_project/Common/error.hpp>
using namespace phosphor::logging;

#include "xyz/openbmc_project/Control/Boot/Type/server.hpp"
#include "xyz/openbmc_project/Network/MACAddress/server.hpp"
#include "xyz/openbmc_project/Control/TPM/Policy/server.hpp"
#include "xyz/openbmc_project/Control/Host/TurboAllowed/server.hpp"
#include "xyz/openbmc_project/Control/MinimumShipLevel/server.hpp"
#include "xyz/openbmc_project/Software/ApplyTime/server.hpp"
#include "xyz/openbmc_project/Control/Boot/RebootPolicy/server.hpp"
#include "xyz/openbmc_project/Control/Power/Cap/server.hpp"
#include "xyz/openbmc_project/Control/PowerSupplyRedundancy/server.hpp"
#include "xyz/openbmc_project/Control/Boot/Mode/server.hpp"
#include "xyz/openbmc_project/Control/Power/RestorePolicy/server.hpp"
#include "xyz/openbmc_project/Object/Enable/server.hpp"
#include "xyz/openbmc_project/Time/Synchronization/server.hpp"
#include "xyz/openbmc_project/Logging/Settings/server.hpp"
#include "xyz/openbmc_project/Control/PowerSupplyAttributes/server.hpp"
#include "xyz/openbmc_project/Network/IP/server.hpp"
#include "xyz/openbmc_project/Control/Security/RestrictionMode/server.hpp"
#include "xyz/openbmc_project/Control/Boot/Source/server.hpp"

namespace phosphor
{
namespace settings
{

namespace fs = std::filesystem;

namespace persistent
{

// A setting d-bus object /foo/bar/baz is persisted in the filesystem with the
// same path. This eases re-construction of settings objects when we restore
// from the filesystem. This can be a problem though when you have two objects
// such as - /foo/bar and /foo/bar/baz. This is because 'bar' will be treated as
// a file in the first case, and a subdir in the second. To solve this, suffix
// files with a trailing __. The __ is a safe character sequence to use, because
// we won't have d-bus object paths ending with this.
// With this the objects would be persisted as - /foo/bar__ and /foo/bar/baz__.
constexpr auto fileSuffix = "__";

}

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace minimum_ship_level_required
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().minimumShipLevelRequired()) minimumShipLevelRequired(decltype(std::declval<Iface0>().minimumShipLevelRequired()) value) override
        {
            auto result = Iface0::minimumShipLevelRequired();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::minimumShipLevelRequired(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::minimumShipLevelRequired;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.minimumShipLevelRequired());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.minimumShipLevelRequired()) MinimumShipLevelRequired{};

    a(MinimumShipLevelRequired);


    setting.minimumShipLevelRequired(MinimumShipLevelRequired);
}

} // namespace minimum_ship_level_required
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace time
{
namespace sync_method
{

using Iface0 = sdbusplus::xyz::openbmc_project::Time::server::Synchronization;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().timeSyncMethod()) timeSyncMethod(decltype(std::declval<Iface0>().timeSyncMethod()) value) override
        {
            auto result = Iface0::timeSyncMethod();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::timeSyncMethod(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::timeSyncMethod;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.timeSyncMethod());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.timeSyncMethod()) TimeSyncMethod{};

    a(TimeSyncMethod);


    setting.timeSyncMethod(TimeSyncMethod);
}

} // namespace sync_method
} // namespace time
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace power_supply_attributes
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().deratingFactor()) deratingFactor(decltype(std::declval<Iface0>().deratingFactor()) value) override
        {
            auto result = Iface0::deratingFactor();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::deratingFactor(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::deratingFactor;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.deratingFactor());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.deratingFactor()) DeratingFactor{};

    a(DeratingFactor);


    setting.deratingFactor(DeratingFactor);
}

} // namespace power_supply_attributes
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace power_supply_redundancy
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().powerSupplyRedundancyEnabled()) powerSupplyRedundancyEnabled(decltype(std::declval<Iface0>().powerSupplyRedundancyEnabled()) value) override
        {
            auto result = Iface0::powerSupplyRedundancyEnabled();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerSupplyRedundancyEnabled(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerSupplyRedundancyEnabled;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.powerSupplyRedundancyEnabled());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.powerSupplyRedundancyEnabled()) PowerSupplyRedundancyEnabled{};

    a(PowerSupplyRedundancyEnabled);


    setting.powerSupplyRedundancyEnabled(PowerSupplyRedundancyEnabled);
}

} // namespace power_supply_redundancy
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace logging
{
namespace rest_api_logs
{

using Iface0 = sdbusplus::xyz::openbmc_project::Object::server::Enable;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().enabled()) enabled(decltype(std::declval<Iface0>().enabled()) value) override
        {
            auto result = Iface0::enabled();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::enabled(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::enabled;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.enabled());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.enabled()) Enabled{};

    a(Enabled);


    setting.enabled(Enabled);
}

} // namespace rest_api_logs
} // namespace logging
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace logging
{
namespace settings
{

using Iface0 = sdbusplus::xyz::openbmc_project::Logging::server::Settings;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().quiesceOnHwError()) quiesceOnHwError(decltype(std::declval<Iface0>().quiesceOnHwError()) value) override
        {
            auto result = Iface0::quiesceOnHwError();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::quiesceOnHwError(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::quiesceOnHwError;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.quiesceOnHwError());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.quiesceOnHwError()) QuiesceOnHwError{};

    a(QuiesceOnHwError);


    setting.quiesceOnHwError(QuiesceOnHwError);
}

} // namespace settings
} // namespace logging
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace software
{
namespace apply_time
{

using Iface0 = sdbusplus::xyz::openbmc_project::Software::server::ApplyTime;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().requestedApplyTime()) requestedApplyTime(decltype(std::declval<Iface0>().requestedApplyTime()) value) override
        {
            auto result = Iface0::requestedApplyTime();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::requestedApplyTime(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::requestedApplyTime;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.requestedApplyTime());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.requestedApplyTime()) RequestedApplyTime{};

    a(RequestedApplyTime);


    setting.requestedApplyTime(RequestedApplyTime);
}

} // namespace apply_time
} // namespace software
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace auto_reboot
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().autoReboot()) autoReboot(decltype(std::declval<Iface0>().autoReboot()) value) override
        {
            auto result = Iface0::autoReboot();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::autoReboot(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::autoReboot;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.autoReboot());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.autoReboot()) AutoReboot{};

    a(AutoReboot);


    setting.autoReboot(AutoReboot);
}

} // namespace auto_reboot
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace auto_reboot
{
namespace one_time
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().autoReboot()) autoReboot(decltype(std::declval<Iface0>().autoReboot()) value) override
        {
            auto result = Iface0::autoReboot();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::autoReboot(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::autoReboot;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.autoReboot());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.autoReboot()) AutoReboot{};

    a(AutoReboot);


    setting.autoReboot(AutoReboot);
}

} // namespace one_time
} // namespace auto_reboot
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace boot
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Boot::server::Source;
using Iface1 = sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode;
using Iface2 = sdbusplus::xyz::openbmc_project::Control::Boot::server::Type;
using Iface3 = sdbusplus::xyz::openbmc_project::Object::server::Enable;
using Parent = sdbusplus::server::object_t<Iface0, Iface1, Iface2, Iface3>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().bootSource()) bootSource(decltype(std::declval<Iface0>().bootSource()) value) override
        {
            auto result = Iface0::bootSource();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::bootSource(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::bootSource;

        decltype(std::declval<Iface1>().bootMode()) bootMode(decltype(std::declval<Iface1>().bootMode()) value) override
        {
            auto result = Iface1::bootMode();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface1::bootMode(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface1::bootMode;

        decltype(std::declval<Iface2>().bootType()) bootType(decltype(std::declval<Iface2>().bootType()) value) override
        {
            auto result = Iface2::bootType();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface2::bootType(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface2::bootType;

        decltype(std::declval<Iface3>().enabled()) enabled(decltype(std::declval<Iface3>().enabled()) value) override
        {
            auto result = Iface3::enabled();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface3::enabled(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface3::enabled;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.bootSource(), setting.bootMode(), setting.bootType(), setting.enabled());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.bootSource()) BootSource{};
    decltype(setting.bootMode()) BootMode{};
    decltype(setting.bootType()) BootType{};
    decltype(setting.enabled()) Enabled{};

    a(BootSource, BootMode, BootType, Enabled);


    setting.bootSource(BootSource);
    setting.bootMode(BootMode);
    setting.bootType(BootType);
    setting.enabled(Enabled);
}

} // namespace boot
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace boot
{
namespace one_time
{

using Iface0 = sdbusplus::xyz::openbmc_project::Object::server::Enable;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().enabled()) enabled(decltype(std::declval<Iface0>().enabled()) value) override
        {
            auto result = Iface0::enabled();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::enabled(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::enabled;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.enabled());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.enabled()) Enabled{};

    a(Enabled);


    setting.enabled(Enabled);
}

} // namespace one_time
} // namespace boot
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace power_cap
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Power::server::Cap;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().powerCap()) powerCap(decltype(std::declval<Iface0>().powerCap()) value) override
        {
            auto result = Iface0::powerCap();
            if (value != result)
            {
                if (!validatePowerCap(value))
                {
                    namespace error =
                        sdbusplus::xyz::openbmc_project::Common::Error;
                    namespace metadata =
                        phosphor::logging::xyz::openbmc_project::Common;
                    phosphor::logging::report<error::InvalidArgument>(
                        metadata::InvalidArgument::ARGUMENT_NAME("powerCap"),
                        metadata::InvalidArgument::ARGUMENT_VALUE(std::to_string(value).c_str()));
                    return result;
                }
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerCap(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerCap;

        decltype(std::declval<Iface0>().powerCapEnable()) powerCapEnable(decltype(std::declval<Iface0>().powerCapEnable()) value) override
        {
            auto result = Iface0::powerCapEnable();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerCapEnable(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerCapEnable;

    private:
        fs::path path;

        bool validatePowerCap(decltype(std::declval<Iface0>().powerCap()) value)
        {
            bool matched = false;
            if ((value <= 1000) && (value >= 0))
            {
                matched = true;
            }
            else
            {
                std::string err = "Input parameter for PowerCap is invalid "
                    "Input: " + std::to_string(value) + "in uint: "
                    "Watts is not in range:0..1000";
                using namespace phosphor::logging;
                log<level::ERR>(err.c_str());
            }
            return matched;
        }
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.powerCap(), setting.powerCapEnable());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.powerCap()) PowerCap{};
    decltype(setting.powerCapEnable()) PowerCapEnable{};

    a(PowerCap, PowerCapEnable);


    setting.powerCap(PowerCap);
    setting.powerCapEnable(PowerCapEnable);
}

} // namespace power_cap
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace power_restore_policy
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().powerRestorePolicy()) powerRestorePolicy(decltype(std::declval<Iface0>().powerRestorePolicy()) value) override
        {
            auto result = Iface0::powerRestorePolicy();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerRestorePolicy(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerRestorePolicy;

        decltype(std::declval<Iface0>().powerRestoreDelay()) powerRestoreDelay(decltype(std::declval<Iface0>().powerRestoreDelay()) value) override
        {
            auto result = Iface0::powerRestoreDelay();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerRestoreDelay(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerRestoreDelay;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.powerRestorePolicy(), setting.powerRestoreDelay());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.powerRestorePolicy()) PowerRestorePolicy{};
    decltype(setting.powerRestoreDelay()) PowerRestoreDelay{};

    a(PowerRestorePolicy, PowerRestoreDelay);


    setting.powerRestorePolicy(PowerRestorePolicy);
    setting.powerRestoreDelay(PowerRestoreDelay);
}

} // namespace power_restore_policy
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace power_restore_policy
{
namespace one_time
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().powerRestorePolicy()) powerRestorePolicy(decltype(std::declval<Iface0>().powerRestorePolicy()) value) override
        {
            auto result = Iface0::powerRestorePolicy();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerRestorePolicy(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerRestorePolicy;

        decltype(std::declval<Iface0>().powerRestoreDelay()) powerRestoreDelay(decltype(std::declval<Iface0>().powerRestoreDelay()) value) override
        {
            auto result = Iface0::powerRestoreDelay();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::powerRestoreDelay(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::powerRestoreDelay;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.powerRestorePolicy(), setting.powerRestoreDelay());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.powerRestorePolicy()) PowerRestorePolicy{};
    decltype(setting.powerRestoreDelay()) PowerRestoreDelay{};

    a(PowerRestorePolicy, PowerRestoreDelay);


    setting.powerRestorePolicy(PowerRestorePolicy);
    setting.powerRestoreDelay(PowerRestoreDelay);
}

} // namespace one_time
} // namespace power_restore_policy
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace restriction_mode
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().restrictionMode()) restrictionMode(decltype(std::declval<Iface0>().restrictionMode()) value) override
        {
            auto result = Iface0::restrictionMode();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::restrictionMode(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::restrictionMode;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.restrictionMode());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.restrictionMode()) RestrictionMode{};

    a(RestrictionMode);


    setting.restrictionMode(RestrictionMode);
}

} // namespace restriction_mode
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace TPMEnable
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().tpmEnable()) tpmEnable(decltype(std::declval<Iface0>().tpmEnable()) value) override
        {
            auto result = Iface0::tpmEnable();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::tpmEnable(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::tpmEnable;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.tpmEnable());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.tpmEnable()) TPMEnable{};

    a(TPMEnable);


    setting.tpmEnable(TPMEnable);
}

} // namespace TPMEnable
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace control
{
namespace host0
{
namespace turbo_allowed
{

using Iface0 = sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().turboAllowed()) turboAllowed(decltype(std::declval<Iface0>().turboAllowed()) value) override
        {
            auto result = Iface0::turboAllowed();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::turboAllowed(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::turboAllowed;

    private:
        fs::path path;
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.turboAllowed());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.turboAllowed()) TurboAllowed{};

    a(TurboAllowed);


    setting.turboAllowed(TurboAllowed);
}

} // namespace turbo_allowed
} // namespace host0
} // namespace control
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace network
{
namespace host0
{
namespace intf
{

using Iface0 = sdbusplus::xyz::openbmc_project::Network::server::MACAddress;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().macAddress()) macAddress(decltype(std::declval<Iface0>().macAddress()) value) override
        {
            auto result = Iface0::macAddress();
            if (value != result)
            {
                if (!validateMACAddress(value))
                {
                    namespace error =
                        sdbusplus::xyz::openbmc_project::Common::Error;
                    namespace metadata =
                        phosphor::logging::xyz::openbmc_project::Common;
                    phosphor::logging::report<error::InvalidArgument>(
                        metadata::InvalidArgument::ARGUMENT_NAME("macAddress"),
                        metadata::InvalidArgument::ARGUMENT_VALUE(value.c_str()));
                    return result;
                }
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::macAddress(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::macAddress;

    private:
        fs::path path;

        bool validateMACAddress(decltype(std::declval<Iface0>().macAddress()) value)
        {
            bool matched = false;
            std::regex regexToCheck("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$");
            matched = std::regex_search(value, regexToCheck);
            if (!matched)
            {
                std::string err = "Input parameter for MACAddress is invalid "
                    "Input: " + value + " not in the format of this regex: "
                    "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";
                using namespace phosphor::logging;
                log<level::ERR>(err.c_str());
            }
            return matched;
        }
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.macAddress());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.macAddress()) MACAddress{};

    a(MACAddress);


    setting.macAddress(MACAddress);
}

} // namespace intf
} // namespace host0
} // namespace network
} // namespace openbmc_project
} // namespace xyz

namespace xyz
{
namespace openbmc_project
{
namespace network
{
namespace host0
{
namespace intf
{
namespace addr
{

using Iface0 = sdbusplus::xyz::openbmc_project::Network::server::IP;
using Parent = sdbusplus::server::object_t<Iface0>;

class Impl : public Parent
{
    public:
        Impl(sdbusplus::bus_t& bus, const char* path):
            Parent(bus, path, Parent::action::defer_emit),
            path(path)
        {
        }
        virtual ~Impl() = default;

        decltype(std::declval<Iface0>().address()) address(decltype(std::declval<Iface0>().address()) value) override
        {
            auto result = Iface0::address();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::address(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::address;

        decltype(std::declval<Iface0>().prefixLength()) prefixLength(decltype(std::declval<Iface0>().prefixLength()) value) override
        {
            auto result = Iface0::prefixLength();
            if (value != result)
            {
                if (!validatePrefixLength(value))
                {
                    namespace error =
                        sdbusplus::xyz::openbmc_project::Common::Error;
                    namespace metadata =
                        phosphor::logging::xyz::openbmc_project::Common;
                    phosphor::logging::report<error::InvalidArgument>(
                        metadata::InvalidArgument::ARGUMENT_NAME("prefixLength"),
                        metadata::InvalidArgument::ARGUMENT_VALUE(std::to_string(value).c_str()));
                    return result;
                }
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::prefixLength(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::prefixLength;

        decltype(std::declval<Iface0>().origin()) origin(decltype(std::declval<Iface0>().origin()) value) override
        {
            auto result = Iface0::origin();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::origin(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::origin;

        decltype(std::declval<Iface0>().gateway()) gateway(decltype(std::declval<Iface0>().gateway()) value) override
        {
            auto result = Iface0::gateway();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::gateway(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::gateway;

        decltype(std::declval<Iface0>().type()) type(decltype(std::declval<Iface0>().type()) value) override
        {
            auto result = Iface0::type();
            if (value != result)
            {
                fs::path p(SETTINGS_PERSIST_PATH);
                p /= path.relative_path();
                p += persistent::fileSuffix;
                fs::create_directories(p.parent_path());
                std::ofstream os(p.c_str(), std::ios::binary);
                cereal::JSONOutputArchive oarchive(os);
                result = Iface0::type(value);
                oarchive(*this);
            }
            return result;
        }
        using Iface0::type;

    private:
        fs::path path;

        bool validatePrefixLength(decltype(std::declval<Iface0>().prefixLength()) value)
        {
            bool matched = false;
            if ((value <= 128) && (value >= 0))
            {
                matched = true;
            }
            else
            {
                std::string err = "Input parameter for PrefixLength is invalid "
                    "Input: " + std::to_string(value) + "in uint: "
                    "bits is not in range:0..128";
                using namespace phosphor::logging;
                log<level::ERR>(err.c_str());
            }
            return matched;
        }
};

template<class Archive>
void save(Archive& a,
          const Impl& setting,
          const std::uint32_t version)
{
    a(setting.address(), setting.prefixLength(), setting.origin(), setting.gateway(), setting.type());
}

template<class Archive>
void load(Archive& a,
          Impl& setting,
          const std::uint32_t version)
{
    decltype(setting.address()) Address{};
    decltype(setting.prefixLength()) PrefixLength{};
    decltype(setting.origin()) Origin{};
    decltype(setting.gateway()) Gateway{};
    decltype(setting.type()) Type{};

    a(Address, PrefixLength, Origin, Gateway, Type);


    setting.address(Address);
    setting.prefixLength(PrefixLength);
    setting.origin(Origin);
    setting.gateway(Gateway);
    setting.type(Type);
}

} // namespace addr
} // namespace intf
} // namespace host0
} // namespace network
} // namespace openbmc_project
} // namespace xyz


/** @class Manager
 *
 *  @brief Compose settings objects and put them on the bus.
 */
class Manager
{
    public:
        Manager() = delete;
        Manager(const Manager&) = delete;
        Manager& operator=(const Manager&) = delete;
        Manager(Manager&&) = delete;
        Manager& operator=(Manager&&) = delete;
        virtual ~Manager() = default;

        /** @brief Constructor to put settings objects on to the bus.
         *  @param[in] bus - Bus to attach to.
         */
        explicit Manager(sdbusplus::bus_t& bus) :
            settings(
                std::make_tuple(
                    std::make_unique<xyz::openbmc_project::control::minimum_ship_level_required::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/minimum_ship_level_required"),
                    std::make_unique<xyz::openbmc_project::time::sync_method::Impl>(
                        bus,
                        "/xyz/openbmc_project/time/sync_method"),
                    std::make_unique<xyz::openbmc_project::control::power_supply_attributes::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/power_supply_attributes"),
                    std::make_unique<xyz::openbmc_project::control::power_supply_redundancy::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/power_supply_redundancy"),
                    std::make_unique<xyz::openbmc_project::logging::rest_api_logs::Impl>(
                        bus,
                        "/xyz/openbmc_project/logging/rest_api_logs"),
                    std::make_unique<xyz::openbmc_project::logging::settings::Impl>(
                        bus,
                        "/xyz/openbmc_project/logging/settings"),
                    std::make_unique<xyz::openbmc_project::software::apply_time::Impl>(
                        bus,
                        "/xyz/openbmc_project/software/apply_time"),
                    std::make_unique<xyz::openbmc_project::control::host0::auto_reboot::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/auto_reboot"),
                    std::make_unique<xyz::openbmc_project::control::host0::auto_reboot::one_time::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/auto_reboot/one_time"),
                    std::make_unique<xyz::openbmc_project::control::host0::boot::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/boot"),
                    std::make_unique<xyz::openbmc_project::control::host0::boot::one_time::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/boot/one_time"),
                    std::make_unique<xyz::openbmc_project::control::host0::power_cap::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/power_cap"),
                    std::make_unique<xyz::openbmc_project::control::host0::power_restore_policy::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/power_restore_policy"),
                    std::make_unique<xyz::openbmc_project::control::host0::power_restore_policy::one_time::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/power_restore_policy/one_time"),
                    std::make_unique<xyz::openbmc_project::control::host0::restriction_mode::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/restriction_mode"),
                    std::make_unique<xyz::openbmc_project::control::host0::TPMEnable::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/TPMEnable"),
                    std::make_unique<xyz::openbmc_project::control::host0::turbo_allowed::Impl>(
                        bus,
                        "/xyz/openbmc_project/control/host0/turbo_allowed"),
                    std::make_unique<xyz::openbmc_project::network::host0::intf::Impl>(
                        bus,
                        "/xyz/openbmc_project/network/host0/intf"),
                    std::make_unique<xyz::openbmc_project::network::host0::intf::addr::Impl>(
                        bus,
                        "/xyz/openbmc_project/network/host0/intf/addr")
                )
            )
        {

            fs::path path{};
            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/minimum_ship_level_required";
            path += persistent::fileSuffix;
            auto initSetting0 = [&]()
            {
                std::get<0>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel::minimumShipLevelRequired(true);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<0>(settings));
                }
                else
                {
                    initSetting0();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting0();
            }
            std::get<0>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/time/sync_method";
            path += persistent::fileSuffix;
            auto initSetting1 = [&]()
            {
                std::get<1>(settings)->
                  sdbusplus::xyz::openbmc_project::Time::server::Synchronization::timeSyncMethod(sdbusplus::xyz::openbmc_project::Time::server::Synchronization::Method::NTP);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<1>(settings));
                }
                else
                {
                    initSetting1();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting1();
            }
            std::get<1>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/power_supply_attributes";
            path += persistent::fileSuffix;
            auto initSetting2 = [&]()
            {
                std::get<2>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes::deratingFactor(90);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<2>(settings));
                }
                else
                {
                    initSetting2();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting2();
            }
            std::get<2>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/power_supply_redundancy";
            path += persistent::fileSuffix;
            auto initSetting3 = [&]()
            {
                std::get<3>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy::powerSupplyRedundancyEnabled(true);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<3>(settings));
                }
                else
                {
                    initSetting3();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting3();
            }
            std::get<3>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/logging/rest_api_logs";
            path += persistent::fileSuffix;
            auto initSetting4 = [&]()
            {
                std::get<4>(settings)->
                  sdbusplus::xyz::openbmc_project::Object::server::Enable::enabled(false);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<4>(settings));
                }
                else
                {
                    initSetting4();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting4();
            }
            std::get<4>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/logging/settings";
            path += persistent::fileSuffix;
            auto initSetting5 = [&]()
            {
                std::get<5>(settings)->
                  sdbusplus::xyz::openbmc_project::Logging::server::Settings::quiesceOnHwError(false);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<5>(settings));
                }
                else
                {
                    initSetting5();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting5();
            }
            std::get<5>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/software/apply_time";
            path += persistent::fileSuffix;
            auto initSetting6 = [&]()
            {
                std::get<6>(settings)->
                  sdbusplus::xyz::openbmc_project::Software::server::ApplyTime::requestedApplyTime(sdbusplus::xyz::openbmc_project::Software::server::ApplyTime::RequestedApplyTimes::OnReset);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<6>(settings));
                }
                else
                {
                    initSetting6();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting6();
            }
            std::get<6>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/auto_reboot";
            path += persistent::fileSuffix;
            auto initSetting7 = [&]()
            {
                std::get<7>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy::autoReboot(true);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<7>(settings));
                }
                else
                {
                    initSetting7();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting7();
            }
            std::get<7>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/auto_reboot/one_time";
            path += persistent::fileSuffix;
            auto initSetting8 = [&]()
            {
                std::get<8>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy::autoReboot(true);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<8>(settings));
                }
                else
                {
                    initSetting8();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting8();
            }
            std::get<8>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/boot";
            path += persistent::fileSuffix;
            auto initSetting9 = [&]()
            {
                std::get<9>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Boot::server::Source::bootSource(sdbusplus::xyz::openbmc_project::Control::Boot::server::Source::Sources::Default);
                std::get<9>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode::bootMode(sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode::Modes::Regular);
                std::get<9>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Boot::server::Type::bootType(sdbusplus::xyz::openbmc_project::Control::Boot::server::Type::Types::EFI);
                std::get<9>(settings)->
                  sdbusplus::xyz::openbmc_project::Object::server::Enable::enabled(false);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<9>(settings));
                }
                else
                {
                    initSetting9();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting9();
            }
            std::get<9>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/boot/one_time";
            path += persistent::fileSuffix;
            auto initSetting10 = [&]()
            {
                std::get<10>(settings)->
                  sdbusplus::xyz::openbmc_project::Object::server::Enable::enabled(false);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<10>(settings));
                }
                else
                {
                    initSetting10();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting10();
            }
            std::get<10>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/power_cap";
            path += persistent::fileSuffix;
            auto initSetting11 = [&]()
            {
                std::get<11>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Power::server::Cap::powerCap(0);
                std::get<11>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Power::server::Cap::powerCapEnable(false);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<11>(settings));
                }
                else
                {
                    initSetting11();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting11();
            }
            std::get<11>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/power_restore_policy";
            path += persistent::fileSuffix;
            auto initSetting12 = [&]()
            {
                std::get<12>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::powerRestorePolicy(sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::Policy::AlwaysOff);
                std::get<12>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::powerRestoreDelay(0);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<12>(settings));
                }
                else
                {
                    initSetting12();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting12();
            }
            std::get<12>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/power_restore_policy/one_time";
            path += persistent::fileSuffix;
            auto initSetting13 = [&]()
            {
                std::get<13>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::powerRestorePolicy(sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::Policy::None);
                std::get<13>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::powerRestoreDelay(0);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<13>(settings));
                }
                else
                {
                    initSetting13();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting13();
            }
            std::get<13>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/restriction_mode";
            path += persistent::fileSuffix;
            auto initSetting14 = [&]()
            {
                std::get<14>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode::restrictionMode(sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode::Modes::None);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<14>(settings));
                }
                else
                {
                    initSetting14();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting14();
            }
            std::get<14>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/TPMEnable";
            path += persistent::fileSuffix;
            auto initSetting15 = [&]()
            {
                std::get<15>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy::tpmEnable(false);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<15>(settings));
                }
                else
                {
                    initSetting15();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting15();
            }
            std::get<15>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/control/host0/turbo_allowed";
            path += persistent::fileSuffix;
            auto initSetting16 = [&]()
            {
                std::get<16>(settings)->
                  sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed::turboAllowed(true);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<16>(settings));
                }
                else
                {
                    initSetting16();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting16();
            }
            std::get<16>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/network/host0/intf";
            path += persistent::fileSuffix;
            auto initSetting17 = [&]()
            {
                std::get<17>(settings)->
                  sdbusplus::xyz::openbmc_project::Network::server::MACAddress::macAddress("00:00:00:00:00:00");
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<17>(settings));
                }
                else
                {
                    initSetting17();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting17();
            }
            std::get<17>(settings)->emit_object_added();

            path = fs::path(SETTINGS_PERSIST_PATH) / "xyz/openbmc_project/network/host0/intf/addr";
            path += persistent::fileSuffix;
            auto initSetting18 = [&]()
            {
                std::get<18>(settings)->
                  sdbusplus::xyz::openbmc_project::Network::server::IP::address("0.0.0.0");
                std::get<18>(settings)->
                  sdbusplus::xyz::openbmc_project::Network::server::IP::prefixLength(0);
                std::get<18>(settings)->
                  sdbusplus::xyz::openbmc_project::Network::server::IP::origin(sdbusplus::xyz::openbmc_project::Network::server::IP::AddressOrigin::Static);
                std::get<18>(settings)->
                  sdbusplus::xyz::openbmc_project::Network::server::IP::gateway("0.0.0.0");
                std::get<18>(settings)->
                  sdbusplus::xyz::openbmc_project::Network::server::IP::type(sdbusplus::xyz::openbmc_project::Network::server::IP::Protocol::IPv4);
            };

            try
            {
                if (fs::exists(path))
                {
                    std::ifstream is(path.c_str(), std::ios::in);
                    cereal::JSONInputArchive iarchive(is);
                    iarchive(*std::get<18>(settings));
                }
                else
                {
                    initSetting18();
                }
            }
            catch (const cereal::Exception& e)
            {
                log<level::ERR>(e.what());
                fs::remove(path);
                initSetting18();
            }
            std::get<18>(settings)->emit_object_added();

        }

    private:
        /* @brief Composition of settings objects. */
        std::tuple<
            std::unique_ptr<xyz::openbmc_project::control::minimum_ship_level_required::Impl>,
            std::unique_ptr<xyz::openbmc_project::time::sync_method::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::power_supply_attributes::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::power_supply_redundancy::Impl>,
            std::unique_ptr<xyz::openbmc_project::logging::rest_api_logs::Impl>,
            std::unique_ptr<xyz::openbmc_project::logging::settings::Impl>,
            std::unique_ptr<xyz::openbmc_project::software::apply_time::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::auto_reboot::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::auto_reboot::one_time::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::boot::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::boot::one_time::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::power_cap::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::power_restore_policy::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::power_restore_policy::one_time::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::restriction_mode::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::TPMEnable::Impl>,
            std::unique_ptr<xyz::openbmc_project::control::host0::turbo_allowed::Impl>,
            std::unique_ptr<xyz::openbmc_project::network::host0::intf::Impl>,
            std::unique_ptr<xyz::openbmc_project::network::host0::intf::addr::Impl>> settings;
};

} // namespace settings
} // namespace phosphor

// Now register the class version with Cereal
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::minimum_ship_level_required::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::time::sync_method::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::power_supply_attributes::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::power_supply_redundancy::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::logging::rest_api_logs::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::logging::settings::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::software::apply_time::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::auto_reboot::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::auto_reboot::one_time::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::boot::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::boot::one_time::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::power_cap::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::power_restore_policy::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::power_restore_policy::one_time::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::restriction_mode::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::TPMEnable::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::control::host0::turbo_allowed::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::network::host0::intf::Impl, CLASS_VERSION);
CEREAL_CLASS_VERSION(phosphor::settings::xyz::openbmc_project::network::host0::intf::addr::Impl, CLASS_VERSION);
