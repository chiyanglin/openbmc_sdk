/* This is a generated file. */
#include "fan_defs.hpp"
#include "types.hpp"
#include "groups.hpp"
#include "conditions.hpp"

using namespace phosphor::fan::monitor;
using namespace phosphor::fan::trust;

const std::vector<FanDefinition> fanDefinitions
{
    FanDefinition{"/system/chassis/motherboard/fan0",
                  {},
                  0,
                  30,
                  15,
                  1,
                  0, // Monitor start delay - not used in YAML configs
                  0, // Count interval - not used in YAML configs
                  std::nullopt, // nonfuncRotorErrorDelay - also not used here
                  std::nullopt, // fanMissingErrorDelay - also not used here
                  std::vector<SensorDefinition>{
                                         SensorDefinition{"fan0",
                                       true,
                                       "xyz.openbmc_project.Control.FanPwm",
                                       "",
                                       21,
                                       1600,
                                       1,
                                       false},
                  },
                  {},
                  false // set_func_on_present. Hardcoded to false.
    },
    FanDefinition{"/system/chassis/motherboard/fan1",
                  {},
                  0,
                  30,
                  15,
                  1,
                  0, // Monitor start delay - not used in YAML configs
                  0, // Count interval - not used in YAML configs
                  std::nullopt, // nonfuncRotorErrorDelay - also not used here
                  std::nullopt, // fanMissingErrorDelay - also not used here
                  std::vector<SensorDefinition>{
                                         SensorDefinition{"fan1",
                                       true,
                                       "xyz.openbmc_project.Control.FanPwm",
                                       "",
                                       21,
                                       1600,
                                       1,
                                       false},
                  },
                  {},
                  false // set_func_on_present. Hardcoded to false.
    },
    FanDefinition{"/system/chassis/motherboard/fan2",
                  {},
                  0,
                  30,
                  15,
                  1,
                  0, // Monitor start delay - not used in YAML configs
                  0, // Count interval - not used in YAML configs
                  std::nullopt, // nonfuncRotorErrorDelay - also not used here
                  std::nullopt, // fanMissingErrorDelay - also not used here
                  std::vector<SensorDefinition>{
                                         SensorDefinition{"fan2",
                                       true,
                                       "xyz.openbmc_project.Control.FanPwm",
                                       "",
                                       21,
                                       1600,
                                       1,
                                       false},
                  },
                  {},
                  false // set_func_on_present. Hardcoded to false.
    },
};


const std::vector<CreateGroupFunction> trustGroups
{
};
