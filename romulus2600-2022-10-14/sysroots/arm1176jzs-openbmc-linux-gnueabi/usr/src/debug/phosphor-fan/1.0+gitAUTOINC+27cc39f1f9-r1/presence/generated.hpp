/* This is an auto generated file. Do not edit. */
#pragma once

#include <array>
#include <memory>
#include <optional>
#include <string>
#include "anyof.hpp"
#include "fallback.hpp"
#include "fan.hpp"
#include "gpio.hpp"
#include "tach.hpp"

using namespace std::string_literals;

namespace phosphor
{
namespace fan
{
namespace presence
{

struct ConfigPolicy;

struct ConfigSensors
{
    using Sensors = std::array<std::unique_ptr<PresenceSensor>, 3>;

    static auto& get()
    {
        static const Sensors sensors =
        {
            std::make_unique<PolicyAccess<Tach, ConfigPolicy>>(
                0, std::vector<std::string>{"fan0",}),
            std::make_unique<PolicyAccess<Tach, ConfigPolicy>>(
                1, std::vector<std::string>{"fan1",}),
            std::make_unique<PolicyAccess<Tach, ConfigPolicy>>(
                2, std::vector<std::string>{"fan2",}),
        };
        return sensors;
    }
};

struct ConfigFans
{
    using Fans = std::array<Fan, 3>;

    static auto& get()
    {
        static const Fans fans =
        {
            {
                Fans::value_type{
                    "fan0"s,
                    "/system/chassis/motherboard/fan0"s,
                    std::nullopt
                },
                Fans::value_type{
                    "fan1"s,
                    "/system/chassis/motherboard/fan1"s,
                    std::nullopt
                },
                Fans::value_type{
                    "fan2"s,
                    "/system/chassis/motherboard/fan2"s,
                    std::nullopt
                },
            }
        };
        return fans;
    }
};

struct ConfigPolicy
{
    using Policies = std::array<std::unique_ptr<RedundancyPolicy>, 3>;

    static auto& get()
    {
        static const Policies policies =
        {
            std::make_unique<AnyOf>(
                ConfigFans::get()[0],
                std::vector<std::reference_wrapper<PresenceSensor>>{
                    *ConfigSensors::get()[0],
                }),
            std::make_unique<AnyOf>(
                ConfigFans::get()[1],
                std::vector<std::reference_wrapper<PresenceSensor>>{
                    *ConfigSensors::get()[1],
                }),
            std::make_unique<AnyOf>(
                ConfigFans::get()[2],
                std::vector<std::reference_wrapper<PresenceSensor>>{
                    *ConfigSensors::get()[2],
                }),
        };
        return policies;
    }
};
} // namespace presence
} // namespace fan
} // namespace phosphor
