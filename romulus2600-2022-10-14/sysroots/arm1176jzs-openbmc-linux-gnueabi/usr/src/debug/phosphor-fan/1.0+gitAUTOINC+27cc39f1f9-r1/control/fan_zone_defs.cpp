



/* This is a generated file. */
#include "manager.hpp"
#include "functor.hpp"
#include "actions.hpp"
#include "handlers.hpp"
#include "preconditions.hpp"
#include "matches.hpp"
#include "triggers.hpp"

using namespace phosphor::fan::control;

const unsigned int Manager::_powerOnDelay{20};

const std::vector<ZoneGroup> Manager::_zoneLayouts
{
    ZoneGroup{
        std::vector<Condition>{
        },
        std::vector<ZoneDefinition>{
            ZoneDefinition{
                0,
                255,
                195,
                5,
                30,
                std::vector<ZoneHandler>{
                },
                std::vector<FanDefinition>{
                    FanDefinition{
                        "/system/chassis/motherboard/fan0",
                        std::vector<std::string>{
                            "fan0",
                        },
                        "xyz.openbmc_project.Control.FanPwm",
                        "/xyz/openbmc_project/sensors/fan_tach/"
                    },
                    FanDefinition{
                        "/system/chassis/motherboard/fan1",
                        std::vector<std::string>{
                            "fan1",
                        },
                        "xyz.openbmc_project.Control.FanPwm",
                        "/xyz/openbmc_project/sensors/fan_tach/"
                    },
                    FanDefinition{
                        "/system/chassis/motherboard/fan2",
                        std::vector<std::string>{
                            "fan2",
                        },
                        "xyz.openbmc_project.Control.FanPwm",
                        "/xyz/openbmc_project/sensors/fan_tach/"
                    },
                },
                std::vector<SetSpeedEvent>{
                    SetSpeedEvent{
                        "default_fan_floor_on_service_fail",
                        Group
                        {
                            {"/xyz/openbmc_project/sensors/temperature/outlet",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                        },
                        ActionData{
                        {Group{},
                        std::vector<Action>{
                        make_action(
                            precondition::services_missing_owner(
                    std::vector<SetSpeedEvent>{
                    SetSpeedEvent{
                        "default_fan_floor",                                            
                        Group
                        {
                            {"/xyz/openbmc_project/sensors/temperature/outlet",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                        },
                        ActionData{
                        {Group{
                        },
                        std::vector<Action>{
                        make_action(action::default_floor_on_missing_owner
                        ),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::timer(TimerConf{
                            static_cast<std::chrono::microseconds>(5000000),
                            TimerType::oneshot
                            })),
                        },
                    },
                    }
                        )),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::signal(
                                match::nameOwnerChanged(
                                    "/xyz/openbmc_project/sensors/temperature/outlet",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(                                                                    
                                                nameOwnerChanged(
                                    handler::setService(
                                    Group
                                    {
                                    {"/xyz/openbmc_project/sensors/temperature/outlet",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value"},
                                    }
                                    )
                                    
                                    )
                                )
                            )),
                            make_trigger(trigger::init(
                                make_handler<MethodHandler>(                                            
            nameHasOwner(
            
            handler::setService(
            Group
            {
            {"/xyz/openbmc_project/sensors/temperature/outlet",
            "xyz.openbmc_project.Sensor.Value",
            "Value"},
            }
            )
            )
                                )
                            )),
                        },
                    },
                    SetSpeedEvent{
                        "high_speed_on_occ_service_fail",                                            
                        Group
                        {
                            {"/org/open_power/control/occ0",
                            "org.open_power.OCC.Status",
                            "OccActive"},
                            {"/org/open_power/control/occ1",
                            "org.open_power.OCC.Status",
                            "OccActive"},
                        },
                        ActionData{
                        {Group{
                        },
                        std::vector<Action>{
                        make_action(action::call_actions_based_on_timer(
                            TimerConf{static_cast<std::chrono::microseconds>(5000000),TimerType::oneshot},
                            std::vector<Action>{make_action(action::set_speed_on_missing_owner(
                        static_cast<uint64_t>(255))),}
                        )),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::signal(
                                match::nameOwnerChanged(
                                    "/org/open_power/control/occ0",
                                    "org.open_power.OCC.Status"
                                ),
                                make_handler<SignalHandler>(        
                                    nameOwnerChanged(
                        handler::setService(
                        Group
                        {
                        {"/org/open_power/control/occ0",
                        "org.open_power.OCC.Status",
                        "OccActive"},
                        {"/org/open_power/control/occ1",
                        "org.open_power.OCC.Status",
                        "OccActive"},
                        }
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::nameOwnerChanged(
                                    "/org/open_power/control/occ1",
                                    "org.open_power.OCC.Status"
                                ),
                                make_handler<SignalHandler>(        
                                    nameOwnerChanged(
                        handler::setService(
                        Group
                        {
                        {"/org/open_power/control/occ0",
                        "org.open_power.OCC.Status",
                        "OccActive"},
                        {"/org/open_power/control/occ1",
                        "org.open_power.OCC.Status",
                        "OccActive"},
                        }
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::init(
                                make_handler<MethodHandler>(                    
                                    nameHasOwner(
                                    
                                    handler::setService(
                                    Group
                                    {
                                    {"/org/open_power/control/occ0",
                                    "org.open_power.OCC.Status",
                                    "OccActive"},
                                    {"/org/open_power/control/occ1",
                                    "org.open_power.OCC.Status",
                                    "OccActive"},
                                    }
                                    )
                                    )
                                )
                            )),
                        },

                    },
                    SetSpeedEvent{
                        "missing_or_fails_before_high_speed_air",                                            
                        Group
                        {
                            {"/xyz/openbmc_project/inventory/system/chassis/motherboard/fan0",
                            "xyz.openbmc_project.Inventory.Item",
                            "Present"},
                            {"/xyz/openbmc_project/inventory/system/chassis/motherboard/fan1",
                            "xyz.openbmc_project.Inventory.Item",
                            "Present"},
                            {"/xyz/openbmc_project/inventory/system/chassis/motherboard/fan2",
                            "xyz.openbmc_project.Inventory.Item",
                            "Present"},
                            {"/xyz/openbmc_project/inventory/system/chassis/motherboard/fan0",
                            "xyz.openbmc_project.State.Decorator.OperationalStatus",
                            "Functional"},
                            {"/xyz/openbmc_project/inventory/system/chassis/motherboard/fan1",
                            "xyz.openbmc_project.State.Decorator.OperationalStatus",
                            "Functional"},
                            {"/xyz/openbmc_project/inventory/system/chassis/motherboard/fan2",
                            "xyz.openbmc_project.State.Decorator.OperationalStatus",
                            "Functional"},
                        },
                        ActionData{
                        {Group{
                        },
                        std::vector<Action>{
                        make_action(action::count_state_before_speed(
                            static_cast<size_t>(1),
                            static_cast<bool>(false),
                            static_cast<uint64_t>(255)
                        )),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan0",
                                    "xyz.openbmc_project.Inventory.Item"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<bool>(            "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan0",
                                    "xyz.openbmc_project.Inventory.Item",
                                    "Present",
                        
                        handler::setProperty<bool>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan1",
                                    "xyz.openbmc_project.Inventory.Item"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<bool>(            "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan1",
                                    "xyz.openbmc_project.Inventory.Item",
                                    "Present",
                        
                        handler::setProperty<bool>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan2",
                                    "xyz.openbmc_project.Inventory.Item"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<bool>(            "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan2",
                                    "xyz.openbmc_project.Inventory.Item",
                                    "Present",
                        
                        handler::setProperty<bool>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan0",
                                    "xyz.openbmc_project.State.Decorator.OperationalStatus"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<bool>(            "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan0",
                                    "xyz.openbmc_project.State.Decorator.OperationalStatus",
                                    "Functional",
                        
                        handler::setProperty<bool>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan1",
                                    "xyz.openbmc_project.State.Decorator.OperationalStatus"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<bool>(            "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan1",
                                    "xyz.openbmc_project.State.Decorator.OperationalStatus",
                                    "Functional",
                        
                        handler::setProperty<bool>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan2",
                                    "xyz.openbmc_project.State.Decorator.OperationalStatus"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<bool>(            "/xyz/openbmc_project/inventory/system/chassis/motherboard/fan2",
                                    "xyz.openbmc_project.State.Decorator.OperationalStatus",
                                    "Functional",
                        
                        handler::setProperty<bool>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::init(
                                make_handler<MethodHandler>(                    
                                    getProperties<bool>(
                                    
                                    handler::setProperty<bool>(
                                    )
                                    )
                                )
                            )),
                        },

                    },
                    SetSpeedEvent{
                        "set_air_cooled_speed_boundaries_based_on_ambient",                                            
                        Group
                        {
                            {"/xyz/openbmc_project/sensors/temperature/outlet",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                        },
                        ActionData{
                        {Group{
                        },
                        std::vector<Action>{
                        make_action(action::set_floor_from_average_sensor_value(
                            static_cast<std::map<int64_t, uint64_t>>(std::map<int64_t, uint64_t>{{27000, 85}, {32000, 112}, {37000, 126}, {40000, 141}})
                        )),
                        make_action(action::set_ceiling_from_average_sensor_value(
                            static_cast<std::map<int64_t, uint64_t>>(std::map<int64_t, uint64_t>{{25000, 175}, {27000, 255}})
                        )),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/outlet",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/outlet",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::init(
                                make_handler<MethodHandler>(                    
                                    getProperties<int64_t>(
                                    
                                    handler::setProperty<int64_t>(
                                    )
                                    )
                                )
                            )),
                        },

                    },
                    SetSpeedEvent{
                        "occ_active_speed_changes",
                        Group
                        {
                            {"/org/open_power/control/occ0",
                            "org.open_power.OCC.Status",
                            "OccActive"},
                            {"/org/open_power/control/occ1",
                            "org.open_power.OCC.Status",
                            "OccActive"},
                        },
                        ActionData{
                        {Group{},
                        std::vector<Action>{
                        make_action(
                            precondition::property_states_match(
                        std::vector<PrecondGroup>{
                            PrecondGroup{"/org/open_power/control/occ0","org.open_power.OCC.Status","OccActive",static_cast<bool>(true)},
                            PrecondGroup{"/org/open_power/control/occ1","org.open_power.OCC.Status","OccActive",static_cast<bool>(true)}
                        },
                    std::vector<SetSpeedEvent>{
                    SetSpeedEvent{
                        "speed_control_sensors",                                            
                        Group
                        {
                            {"/xyz/openbmc_project/sensors/temperature/p0_core0_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core1_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core2_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core3_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core4_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core5_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core6_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core7_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core8_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core9_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core10_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core11_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core12_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core13_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core14_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core15_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core16_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core17_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core18_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core19_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core20_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core21_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core22_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core23_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core0_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core1_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core2_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core3_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core4_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core5_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core6_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core7_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core8_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core9_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core10_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core11_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core12_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core13_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core14_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core15_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core16_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core17_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core18_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core19_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core20_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core21_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core22_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core23_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm0_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm1_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm2_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm3_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm4_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm5_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm6_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm7_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm8_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm9_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm10_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm11_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm12_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm13_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm14_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm15_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                        },
                        ActionData{
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core0_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core0_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core1_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core1_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core2_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core2_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core3_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core3_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core4_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core4_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core5_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core5_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core6_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core6_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core7_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core7_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core8_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core8_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core9_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core9_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core10_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core10_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core11_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core11_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core12_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core12_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core13_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core13_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core14_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core14_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core15_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core15_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core16_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core16_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core17_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core17_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core18_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core18_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core19_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core19_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core20_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core20_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core21_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core21_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core22_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core22_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core23_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core23_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core0_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core0_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core1_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core1_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core2_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core2_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core3_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core3_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core4_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core4_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core5_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core5_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core6_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core6_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core7_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core7_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core8_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core8_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core9_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core9_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core10_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core10_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core11_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core11_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core12_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core12_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core13_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core13_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core14_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core14_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core15_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core15_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core16_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core16_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core17_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core17_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core18_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core18_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core19_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core19_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core20_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core20_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core21_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core21_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core22_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core22_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core23_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core23_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm0_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm0_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm1_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm1_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm2_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm2_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm3_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm3_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm4_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm4_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm5_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm5_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm6_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm6_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm7_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm7_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm8_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm8_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm9_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm9_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm10_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm10_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm11_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm11_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm12_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm12_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm13_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm13_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm14_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm14_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/xyz/openbmc_project/sensors/temperature/dimm15_temp"
                                ),
                                make_handler<SignalHandler>(        
                                    interfacesAdded<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm15_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core0_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core0_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core1_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core1_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core2_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core2_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core3_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core3_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core4_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core4_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core5_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core5_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core6_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core6_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core7_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core7_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core8_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core8_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core9_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core9_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core10_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core10_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core11_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core11_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core12_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core12_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core13_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core13_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core14_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core14_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core15_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core15_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core16_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core16_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core17_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core17_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core18_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core18_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core19_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core19_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core20_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core20_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core21_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core21_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core22_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core22_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p0_core23_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p0_core23_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core0_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core0_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core1_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core1_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core2_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core2_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core3_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core3_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core4_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core4_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core5_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core5_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core6_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core6_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core7_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core7_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core8_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core8_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core9_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core9_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core10_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core10_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core11_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core11_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core12_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core12_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core13_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core13_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core14_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core14_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core15_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core15_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core16_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core16_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core17_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core17_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core18_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core18_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core19_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core19_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core20_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core20_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core21_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core21_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core22_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core22_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/p1_core23_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/p1_core23_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm0_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm0_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm1_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm1_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm2_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm2_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm3_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm3_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm4_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm4_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm5_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm5_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm6_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm6_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm7_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm7_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm8_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm8_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm9_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm9_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm10_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm10_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm11_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm11_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm12_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm12_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm13_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm13_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm14_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm14_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/xyz/openbmc_project/sensors/temperature/dimm15_temp",
                                    "xyz.openbmc_project.Sensor.Value"
                                ),
                                make_handler<SignalHandler>(        
                                    propertiesChanged<int64_t>(            "/xyz/openbmc_project/sensors/temperature/dimm15_temp",
                                    "xyz.openbmc_project.Sensor.Value",
                                    "Value",
                        
                        handler::setProperty<int64_t>(
                        )
                        
                        )
                        
                                )
                            )),
                            make_trigger(trigger::init(
                                make_handler<MethodHandler>(                    
                                    getProperties<int64_t>(
                                    
                                    handler::setProperty<int64_t>(
                                    )
                                    )
                                )
                            )),
                        },
                    },
                    SetSpeedEvent{
                        "1sec_speed_change_request_sampling",                                            
                        Group
                        {
                        },
                        ActionData{
                        {Group{
                            {"/xyz/openbmc_project/sensors/temperature/p0_core0_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core1_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core2_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core3_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core4_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core5_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core6_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core7_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core8_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core9_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core10_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core11_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core12_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core13_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core14_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core15_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core16_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core17_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core18_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core19_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core20_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core21_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core22_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p0_core23_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core0_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core1_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core2_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core3_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core4_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core5_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core6_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core7_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core8_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core9_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core10_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core11_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core12_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core13_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core14_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core15_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core16_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core17_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core18_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core19_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core20_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core21_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core22_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/p1_core23_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                        },
                        std::vector<Action>{
                        make_action(action::set_net_increase_speed(
                            static_cast<int64_t>(73000),
                            static_cast<int64_t>(1000),
                            static_cast<uint64_t>(11)
                        )),
                        make_action(action::set_net_decrease_speed(
                            static_cast<int64_t>(70000),
                            static_cast<int64_t>(1000),
                            static_cast<uint64_t>(11)
                        )),
                        }},
                        {Group{
                            {"/xyz/openbmc_project/sensors/temperature/dimm0_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm1_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm2_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm3_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm4_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm5_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm6_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm7_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm8_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm9_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm10_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm11_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm12_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm13_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm14_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                            {"/xyz/openbmc_project/sensors/temperature/dimm15_temp",
                            "xyz.openbmc_project.Sensor.Value",
                            "Value"},
                        },
                        std::vector<Action>{
                        make_action(action::set_net_increase_speed(
                            static_cast<int64_t>(64000),
                            static_cast<int64_t>(1000),
                            static_cast<uint64_t>(6)
                        )),
                        make_action(action::set_net_decrease_speed(
                            static_cast<int64_t>(61000),
                            static_cast<int64_t>(1000),
                            static_cast<uint64_t>(6)
                        )),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::timer(TimerConf{
                            static_cast<std::chrono::microseconds>(1000000),
                            TimerType::repeating
                            })),
                        },
                    },
                    }
                        )),
                        }},
                        },
                        std::vector<Trigger>{
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/org/open_power/control/occ0"
                                ),
                                make_handler<SignalHandler>(                                                                    
                                                interfacesAdded<bool>(            "/org/open_power/control/occ0",
                                                "org.open_power.OCC.Status",
                                                "OccActive",
                                    
                                    handler::setProperty<bool>(
                                    )
                                    
                                    )
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::interfacesAdded(
                                    "/org/open_power/control/occ1"
                                ),
                                make_handler<SignalHandler>(                                                                    
                                                interfacesAdded<bool>(            "/org/open_power/control/occ1",
                                                "org.open_power.OCC.Status",
                                                "OccActive",
                                    
                                    handler::setProperty<bool>(
                                    )
                                    
                                    )
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/org/open_power/control/occ0",
                                    "org.open_power.OCC.Status"
                                ),
                                make_handler<SignalHandler>(                                                                    
                                                propertiesChanged<bool>(            "/org/open_power/control/occ0",
                                                "org.open_power.OCC.Status",
                                                "OccActive",
                                    
                                    handler::setProperty<bool>(
                                    )
                                    
                                    )
                                )
                            )),
                            make_trigger(trigger::signal(
                                match::propertiesChanged(
                                    "/org/open_power/control/occ1",
                                    "org.open_power.OCC.Status"
                                ),
                                make_handler<SignalHandler>(                                                                    
                                                propertiesChanged<bool>(            "/org/open_power/control/occ1",
                                                "org.open_power.OCC.Status",
                                                "OccActive",
                                    
                                    handler::setProperty<bool>(
                                    )
                                    
                                    )
                                )
                            )),
                            make_trigger(trigger::init(
                                make_handler<MethodHandler>(                                            
            getProperties<bool>(
            
            handler::setProperty<bool>(
            )
            )
                                )
                            )),
                        },
                    },
                }
            },
        }
    },
};
