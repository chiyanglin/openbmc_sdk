#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Internal/Manager/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Internal
{
namespace server
{

Manager::Manager(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Internal_Manager_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Manager::_callback_Commit(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint64_t&& transactionId, std::string&& errMsg)
                    {
                        return o->commit(
                                transactionId, errMsg);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_Commit =
        utility::tuple_to_array(message::types::type_id<
                uint64_t, std::string>());
static const auto _return_Commit =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int Manager::_callback_CommitWithLvl(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint64_t&& transactionId, std::string&& errMsg, uint32_t&& errLvl)
                    {
                        return o->commitWithLvl(
                                transactionId, errMsg, errLvl);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_CommitWithLvl =
        utility::tuple_to_array(message::types::type_id<
                uint64_t, std::string, uint32_t>());
static const auto _return_CommitWithLvl =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}




const vtable_t Manager::_vtable[] = {
    vtable::start(),

    vtable::method("Commit",
                   details::Manager::_param_Commit
                        .data(),
                   details::Manager::_return_Commit
                        .data(),
                   _callback_Commit),

    vtable::method("CommitWithLvl",
                   details::Manager::_param_CommitWithLvl
                        .data(),
                   details::Manager::_return_CommitWithLvl
                        .data(),
                   _callback_CommitWithLvl),
    vtable::end()
};

} // namespace server
} // namespace Internal
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

