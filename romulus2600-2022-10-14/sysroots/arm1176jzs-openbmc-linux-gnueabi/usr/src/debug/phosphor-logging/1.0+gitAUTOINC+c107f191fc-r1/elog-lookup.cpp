// This file was autogenerated.  Do not edit!
// See elog-gen.py for more details
#include <map>
#include <vector>
#include <log_manager.hpp>
#include <phosphor-logging/log.hpp>

namespace phosphor
{

namespace logging
{

const std::map<std::string,std::vector<std::string>> g_errMetaMap = {
    {"com.ibm.VPD.Error.LocationNotFound",{}},
    {"com.ibm.VPD.Error.NodeNotFound",{}},
    {"com.ibm.VPD.Error.PathNotFound",{}},
    {"com.ibm.VPD.Error.RecordNotFound",{}},
    {"com.ibm.VPD.Error.KeywordNotFound",{}},
    {"com.ibm.VPD.Error.BlankSystemVPD",{}},
    {"com.ibm.VPD.Error.InvalidEepromPath",{}},
    {"com.ibm.VPD.Error.InvalidVPD",{}},
    {"com.ibm.VPD.Error.EccCheckFailed",{}},
    {"com.ibm.VPD.Error.InvalidJson",{}},
    {"com.ibm.VPD.Error.DbusFailure",{}},
    {"com.google.gbmc.Hoth.Error.CommandFailure",{}},
    {"com.google.gbmc.Hoth.Error.ResponseFailure",{}},
    {"com.google.gbmc.Hoth.Error.FirmwareFailure",{}},
    {"com.google.gbmc.Hoth.Error.ResponseNotFound",{}},
    {"com.google.gbmc.Hoth.Error.InterfaceError",{}},
    {"com.google.gbmc.Hoth.Error.ExpectedInfoNotFound",{}},
    {"org.open_power.Common.Callout.Error.Procedure",{"PROCEDURE"}},
    {"org.open_power.Host.Boot.Error.Checkstop",{}},
    {"org.open_power.Host.Boot.Error.WatchdogTimedOut",{}},
    {"org.open_power.OCC.Metrics.Error.Event",{"ESEL"}},
    {"org.open_power.Proc.FSI.Error.SlaveDetectionFailure",{"ERRNO"}},
    {"xyz.openbmc_project.Certs.Error.InvalidCertificate",{"REASON"}},
    {"xyz.openbmc_project.Common.Error.Timeout",{"TIMEOUT_IN_MSEC"}},
    {"xyz.openbmc_project.Common.Error.InternalFailure",{}},
    {"xyz.openbmc_project.Common.Error.InvalidArgument",{"ARGUMENT_NAME","ARGUMENT_VALUE"}},
    {"xyz.openbmc_project.Common.Error.InsufficientPermission",{}},
    {"xyz.openbmc_project.Common.Error.NotAllowed",{"REASON"}},
    {"xyz.openbmc_project.Common.Error.NoCACertificate",{}},
    {"xyz.openbmc_project.Common.Error.TooManyResources",{}},
    {"xyz.openbmc_project.Common.Error.ResourceNotFound",{"RESOURCE"}},
    {"xyz.openbmc_project.Common.Error.Unavailable",{}},
    {"xyz.openbmc_project.Common.Error.UnsupportedRequest",{"REASON"}},
    {"xyz.openbmc_project.HardwareIsolation.Error.IsolatedAlready",{}},
    {"xyz.openbmc_project.ScheduledTime.Error.InvalidTime",{"REASON"}},
    {"xyz.openbmc_project.Time.Error.NotAllowed",{"OWNER","SYNC_METHOD","REASON"}},
    {"xyz.openbmc_project.Time.Error.Failed",{"REASON"}},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.AttributeNotFound",{}},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.AttributeReadOnly",{"REASON"}},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.InvalidCurrentPassword",{}},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.PasswordNotSettable",{}},
    {"xyz.openbmc_project.Chassis.Common.Error.UnsupportedCommand",{}},
    {"xyz.openbmc_project.Chassis.Common.Error.IOError",{}},
    {"xyz.openbmc_project.Common.Callout.Error.Device",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Common.Callout.Error.GPIO",{"CALLOUT_GPIO_NUM","CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Common.Callout.Error.IIC",{"CALLOUT_IIC_BUS","CALLOUT_IIC_ADDR","CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Common.Callout.Error.Inventory",{"CALLOUT_INVENTORY_PATH"}},
    {"xyz.openbmc_project.Common.Callout.Error.IPMISensor",{"CALLOUT_IPMI_SENSOR_NUM"}},
    {"xyz.openbmc_project.Common.Device.Error.ReadFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Common.Device.Error.WriteFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Common.File.Error.Open",{"ERRNO","PATH"}},
    {"xyz.openbmc_project.Common.File.Error.Seek",{"OFFSET","WHENCE","ERRNO","PATH"}},
    {"xyz.openbmc_project.Common.File.Error.Write",{"ERRNO","PATH"}},
    {"xyz.openbmc_project.Common.File.Error.Read",{"ERRNO","PATH"}},
    {"xyz.openbmc_project.Control.Device.Error.WriteFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Control.Host.Error.CommandNotSupported",{}},
    {"xyz.openbmc_project.Dump.Create.Error.Disabled",{}},
    {"xyz.openbmc_project.Dump.Create.Error.QuotaExceeded",{"REASON"}},
    {"xyz.openbmc_project.Logging.SEL.Error.Created",{"RECORD_TYPE","GENERATOR_ID","SENSOR_DATA","EVENT_DIR","SENSOR_PATH"}},
    {"xyz.openbmc_project.Memory.MemoryECC.Error.isLoggingLimitReached",{}},
    {"xyz.openbmc_project.Memory.MemoryECC.Error.ceCount",{}},
    {"xyz.openbmc_project.Memory.MemoryECC.Error.ueCount",{}},
    {"xyz.openbmc_project.Nvme.Status.Error.CapacityFault",{}},
    {"xyz.openbmc_project.Nvme.Status.Error.TemperatureFault",{}},
    {"xyz.openbmc_project.Nvme.Status.Error.DegradesFault",{}},
    {"xyz.openbmc_project.Nvme.Status.Error.MediaFault",{}},
    {"xyz.openbmc_project.Nvme.Status.Error.BackupDeviceFault",{}},
    {"xyz.openbmc_project.Sensor.Device.Error.ReadFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Sensor.Threshold.Error.CriticalHigh",{"SENSOR_DATA"}},
    {"xyz.openbmc_project.Sensor.Threshold.Error.CriticalLow",{"SENSOR_DATA"}},
    {"xyz.openbmc_project.Smbios.MDR_V2.Error.InvalidParameter",{}},
    {"xyz.openbmc_project.Smbios.MDR_V2.Error.UpdateInProgress",{}},
    {"xyz.openbmc_project.Smbios.MDR_V2.Error.InvalidId",{}},
    {"xyz.openbmc_project.Software.Version.Error.Incompatible",{"MIN_VERSION","ACTUAL_VERSION","VERSION_PURPOSE"}},
    {"xyz.openbmc_project.Software.Version.Error.AlreadyExists",{"IMAGE_VERSION"}},
    {"xyz.openbmc_project.Software.Version.Error.InvalidSignature",{}},
    {"xyz.openbmc_project.Software.Image.Error.UnTarFailure",{"PATH"}},
    {"xyz.openbmc_project.Software.Image.Error.ManifestFileFailure",{"PATH"}},
    {"xyz.openbmc_project.Software.Image.Error.InternalFailure",{"FAIL"}},
    {"xyz.openbmc_project.Software.Image.Error.ImageFailure",{"FAIL","PATH"}},
    {"xyz.openbmc_project.Software.Image.Error.BusyFailure",{"PATH"}},
    {"xyz.openbmc_project.State.Host.Error.SoftOffTimeout",{"TIMEOUT_IN_MSEC"}},
    {"xyz.openbmc_project.State.SystemdTarget.Error.Failure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Shutdown.Inventory.Error.Fan",{}},
    {"xyz.openbmc_project.State.Shutdown.Power.Error.Fault",{}},
    {"xyz.openbmc_project.State.Shutdown.Power.Error.Blackout",{}},
    {"xyz.openbmc_project.State.Shutdown.Power.Error.Regulator",{}},
    {"xyz.openbmc_project.State.Shutdown.ThermalEvent.Error.Processor",{}},
    {"xyz.openbmc_project.State.Shutdown.ThermalEvent.Error.GPU",{}},
    {"xyz.openbmc_project.State.Shutdown.ThermalEvent.Error.Ambient",{}},
    {"xyz.openbmc_project.User.Common.Error.UserNameExists",{}},
    {"xyz.openbmc_project.User.Common.Error.UserNameDoesNotExist",{}},
    {"xyz.openbmc_project.User.Common.Error.UserNameGroupFail",{"REASON"}},
    {"xyz.openbmc_project.User.Common.Error.UserNamePrivFail",{"REASON"}},
    {"xyz.openbmc_project.User.Common.Error.NoResource",{"REASON"}},
    {"xyz.openbmc_project.User.Common.Error.PrivilegeMappingExists",{}},
    {"example.xyz.openbmc_project.Example.Device.Callout",{"CALLOUT_ERRNO_TEST","CALLOUT_DEVICE_PATH_TEST"}},
    {"example.xyz.openbmc_project.Example.Elog.TestErrorTwo",{"DEV_ADDR","DEV_ID","DEV_NAME"}},
    {"example.xyz.openbmc_project.Example.Elog.AutoTestSimple",{"STRING"}},
    {"example.xyz.openbmc_project.Example.Elog.TestCallout",{"DEV_ADDR","CALLOUT_ERRNO_TEST","CALLOUT_DEVICE_PATH_TEST"}},
    {"org.open_power.Host.Error.Event",{"ESEL","CALLOUT_INVENTORY_PATH"}},
    {"org.open_power.Host.Error.MaintenanceProcedure",{"ESEL","PROCEDURE"}},
    {"org.open_power.Host.Access.Error.WriteCFAM",{"ADDRESS","CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"org.open_power.Host.Access.Error.ReadCFAM",{"ADDRESS","CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"org.open_power.OCC.Device.Error.OpenFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"org.open_power.OCC.Device.Error.ReadFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"org.open_power.OCC.Device.Error.WriteFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"org.open_power.OCC.Device.Error.ConfigFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"org.open_power.Proc.FSI.Error.MasterDetectionFailure",{"CALLOUT_ERRNO","CALLOUT_DEVICE_PATH"}},
    {"xyz.openbmc_project.Inventory.Error.NotPresent",{"CALLOUT_INVENTORY_PATH"}},
    {"xyz.openbmc_project.Inventory.Error.Nonfunctional",{"CALLOUT_INVENTORY_PATH"}},
    {"xyz.openbmc_project.State.BMC.Error.MultiUserTargetFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Chassis.Error.PowerOnFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Chassis.Error.PowerOffFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Host.Error.HostStartFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Host.Error.HostMinStartFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Host.Error.HostShutdownFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Host.Error.HostStopFailure",{"SYSTEMD_RESULT"}},
    {"xyz.openbmc_project.State.Host.Error.HostRebootFailure",{"SYSTEMD_RESULT"}},
    {"example.xyz.openbmc_project.Example.Elog.TestErrorOne",{"ERRNUM","FILE_PATH","FILE_NAME","DEV_ADDR","DEV_ID","DEV_NAME"}},
    {"example.xyz.openbmc_project.Example.Foo.Foo",{"FOO_DATA","ERRNUM","FILE_PATH","FILE_NAME","DEV_ADDR","DEV_ID","DEV_NAME"}},
    {"example.xyz.openbmc_project.Example.Bar.Bar",{"BAR_DATA","FOO_DATA","ERRNUM","FILE_PATH","FILE_NAME","DEV_ADDR","DEV_ID","DEV_NAME"}},
};

const std::map<std::string,level> g_errLevelMap = {
    {"com.ibm.VPD.Error.LocationNotFound",level::ERR},
    {"com.ibm.VPD.Error.NodeNotFound",level::ERR},
    {"com.ibm.VPD.Error.PathNotFound",level::ERR},
    {"com.ibm.VPD.Error.RecordNotFound",level::ERR},
    {"com.ibm.VPD.Error.KeywordNotFound",level::ERR},
    {"com.ibm.VPD.Error.BlankSystemVPD",level::ERR},
    {"com.ibm.VPD.Error.InvalidEepromPath",level::ERR},
    {"com.ibm.VPD.Error.InvalidVPD",level::ERR},
    {"com.ibm.VPD.Error.EccCheckFailed",level::ERR},
    {"com.ibm.VPD.Error.InvalidJson",level::ERR},
    {"com.ibm.VPD.Error.DbusFailure",level::ERR},
    {"com.google.gbmc.Hoth.Error.CommandFailure",level::ERR},
    {"com.google.gbmc.Hoth.Error.ResponseFailure",level::ERR},
    {"com.google.gbmc.Hoth.Error.FirmwareFailure",level::ERR},
    {"com.google.gbmc.Hoth.Error.ResponseNotFound",level::ERR},
    {"com.google.gbmc.Hoth.Error.InterfaceError",level::ERR},
    {"com.google.gbmc.Hoth.Error.ExpectedInfoNotFound",level::ERR},
    {"org.open_power.Common.Callout.Error.Procedure",level::ERR},
    {"org.open_power.Host.Boot.Error.Checkstop",level::ERR},
    {"org.open_power.Host.Boot.Error.WatchdogTimedOut",level::ERR},
    {"org.open_power.OCC.Metrics.Error.Event",level::INFO},
    {"org.open_power.Proc.FSI.Error.SlaveDetectionFailure",level::ERR},
    {"xyz.openbmc_project.Certs.Error.InvalidCertificate",level::ERR},
    {"xyz.openbmc_project.Common.Error.Timeout",level::ERR},
    {"xyz.openbmc_project.Common.Error.InternalFailure",level::ERR},
    {"xyz.openbmc_project.Common.Error.InvalidArgument",level::ERR},
    {"xyz.openbmc_project.Common.Error.InsufficientPermission",level::INFO},
    {"xyz.openbmc_project.Common.Error.NotAllowed",level::INFO},
    {"xyz.openbmc_project.Common.Error.NoCACertificate",level::ERR},
    {"xyz.openbmc_project.Common.Error.TooManyResources",level::INFO},
    {"xyz.openbmc_project.Common.Error.ResourceNotFound",level::INFO},
    {"xyz.openbmc_project.Common.Error.Unavailable",level::ERR},
    {"xyz.openbmc_project.Common.Error.UnsupportedRequest",level::INFO},
    {"xyz.openbmc_project.HardwareIsolation.Error.IsolatedAlready",level::ERR},
    {"xyz.openbmc_project.ScheduledTime.Error.InvalidTime",level::ERR},
    {"xyz.openbmc_project.Time.Error.NotAllowed",level::ERR},
    {"xyz.openbmc_project.Time.Error.Failed",level::ERR},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.AttributeNotFound",level::ERR},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.AttributeReadOnly",level::ERR},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.InvalidCurrentPassword",level::ERR},
    {"xyz.openbmc_project.BIOSConfig.Common.Error.PasswordNotSettable",level::ERR},
    {"xyz.openbmc_project.Chassis.Common.Error.UnsupportedCommand",level::ERR},
    {"xyz.openbmc_project.Chassis.Common.Error.IOError",level::ERR},
    {"xyz.openbmc_project.Common.Callout.Error.Device",level::ERR},
    {"xyz.openbmc_project.Common.Callout.Error.GPIO",level::ERR},
    {"xyz.openbmc_project.Common.Callout.Error.IIC",level::ERR},
    {"xyz.openbmc_project.Common.Callout.Error.Inventory",level::ERR},
    {"xyz.openbmc_project.Common.Callout.Error.IPMISensor",level::ERR},
    {"xyz.openbmc_project.Common.Device.Error.ReadFailure",level::ERR},
    {"xyz.openbmc_project.Common.Device.Error.WriteFailure",level::ERR},
    {"xyz.openbmc_project.Common.File.Error.Open",level::ERR},
    {"xyz.openbmc_project.Common.File.Error.Seek",level::ERR},
    {"xyz.openbmc_project.Common.File.Error.Write",level::ERR},
    {"xyz.openbmc_project.Common.File.Error.Read",level::ERR},
    {"xyz.openbmc_project.Control.Device.Error.WriteFailure",level::ERR},
    {"xyz.openbmc_project.Control.Host.Error.CommandNotSupported",level::ERR},
    {"xyz.openbmc_project.Dump.Create.Error.Disabled",level::ERR},
    {"xyz.openbmc_project.Dump.Create.Error.QuotaExceeded",level::ERR},
    {"xyz.openbmc_project.Logging.SEL.Error.Created",level::INFO},
    {"xyz.openbmc_project.Memory.MemoryECC.Error.isLoggingLimitReached",level::ERR},
    {"xyz.openbmc_project.Memory.MemoryECC.Error.ceCount",level::ERR},
    {"xyz.openbmc_project.Memory.MemoryECC.Error.ueCount",level::ERR},
    {"xyz.openbmc_project.Nvme.Status.Error.CapacityFault",level::ERR},
    {"xyz.openbmc_project.Nvme.Status.Error.TemperatureFault",level::ERR},
    {"xyz.openbmc_project.Nvme.Status.Error.DegradesFault",level::ERR},
    {"xyz.openbmc_project.Nvme.Status.Error.MediaFault",level::ERR},
    {"xyz.openbmc_project.Nvme.Status.Error.BackupDeviceFault",level::ERR},
    {"xyz.openbmc_project.Sensor.Device.Error.ReadFailure",level::ERR},
    {"xyz.openbmc_project.Sensor.Threshold.Error.CriticalHigh",level::ERR},
    {"xyz.openbmc_project.Sensor.Threshold.Error.CriticalLow",level::ERR},
    {"xyz.openbmc_project.Smbios.MDR_V2.Error.InvalidParameter",level::ERR},
    {"xyz.openbmc_project.Smbios.MDR_V2.Error.UpdateInProgress",level::ERR},
    {"xyz.openbmc_project.Smbios.MDR_V2.Error.InvalidId",level::ERR},
    {"xyz.openbmc_project.Software.Version.Error.Incompatible",level::ERR},
    {"xyz.openbmc_project.Software.Version.Error.AlreadyExists",level::ERR},
    {"xyz.openbmc_project.Software.Version.Error.InvalidSignature",level::ERR},
    {"xyz.openbmc_project.Software.Image.Error.UnTarFailure",level::ERR},
    {"xyz.openbmc_project.Software.Image.Error.ManifestFileFailure",level::ERR},
    {"xyz.openbmc_project.Software.Image.Error.InternalFailure",level::ERR},
    {"xyz.openbmc_project.Software.Image.Error.ImageFailure",level::ERR},
    {"xyz.openbmc_project.Software.Image.Error.BusyFailure",level::ERR},
    {"xyz.openbmc_project.State.Host.Error.SoftOffTimeout",level::ERR},
    {"xyz.openbmc_project.State.SystemdTarget.Error.Failure",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.Inventory.Error.Fan",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.Power.Error.Fault",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.Power.Error.Blackout",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.Power.Error.Regulator",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.ThermalEvent.Error.Processor",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.ThermalEvent.Error.GPU",level::ERR},
    {"xyz.openbmc_project.State.Shutdown.ThermalEvent.Error.Ambient",level::ERR},
    {"xyz.openbmc_project.User.Common.Error.UserNameExists",level::ERR},
    {"xyz.openbmc_project.User.Common.Error.UserNameDoesNotExist",level::ERR},
    {"xyz.openbmc_project.User.Common.Error.UserNameGroupFail",level::ERR},
    {"xyz.openbmc_project.User.Common.Error.UserNamePrivFail",level::ERR},
    {"xyz.openbmc_project.User.Common.Error.NoResource",level::ERR},
    {"xyz.openbmc_project.User.Common.Error.PrivilegeMappingExists",level::ERR},
    {"example.xyz.openbmc_project.Example.Device.Callout",level::ERR},
    {"example.xyz.openbmc_project.Example.Elog.TestErrorTwo",level::ERR},
    {"example.xyz.openbmc_project.Example.Elog.AutoTestSimple",level::ERR},
    {"example.xyz.openbmc_project.Example.Elog.TestCallout",level::ERR},
    {"org.open_power.Host.Error.Event",level::ERR},
    {"org.open_power.Host.Error.MaintenanceProcedure",level::ERR},
    {"org.open_power.Host.Access.Error.WriteCFAM",level::ERR},
    {"org.open_power.Host.Access.Error.ReadCFAM",level::ERR},
    {"org.open_power.OCC.Device.Error.OpenFailure",level::ERR},
    {"org.open_power.OCC.Device.Error.ReadFailure",level::ERR},
    {"org.open_power.OCC.Device.Error.WriteFailure",level::ERR},
    {"org.open_power.OCC.Device.Error.ConfigFailure",level::ERR},
    {"org.open_power.Proc.FSI.Error.MasterDetectionFailure",level::ERR},
    {"xyz.openbmc_project.Inventory.Error.NotPresent",level::ERR},
    {"xyz.openbmc_project.Inventory.Error.Nonfunctional",level::ERR},
    {"xyz.openbmc_project.State.BMC.Error.MultiUserTargetFailure",level::ERR},
    {"xyz.openbmc_project.State.Chassis.Error.PowerOnFailure",level::ERR},
    {"xyz.openbmc_project.State.Chassis.Error.PowerOffFailure",level::ERR},
    {"xyz.openbmc_project.State.Host.Error.HostStartFailure",level::ERR},
    {"xyz.openbmc_project.State.Host.Error.HostMinStartFailure",level::ERR},
    {"xyz.openbmc_project.State.Host.Error.HostShutdownFailure",level::ERR},
    {"xyz.openbmc_project.State.Host.Error.HostStopFailure",level::ERR},
    {"xyz.openbmc_project.State.Host.Error.HostRebootFailure",level::ERR},
    {"example.xyz.openbmc_project.Example.Elog.TestErrorOne",level::INFO},
    {"example.xyz.openbmc_project.Example.Foo.Foo",level::INFO},
    {"example.xyz.openbmc_project.Example.Bar.Bar",level::INFO},
};

} // namespace logging

} // namespace phosphor
