// This file was auto generated.  Do not edit.
#include "config.h"
#include "manager.hpp"
#include "utils.hpp"
#include "functor.hpp"
#include <xyz/openbmc_project/Association/Definitions/server.hpp>
#include <xyz/openbmc_project/Certs/Certificate/server.hpp>
#include <xyz/openbmc_project/Certs/Entry/server.hpp>
#include <xyz/openbmc_project/Channel/ChannelAccess/server.hpp>
#include <xyz/openbmc_project/Chassis/Intrusion/server.hpp>
#include <xyz/openbmc_project/Chassis/Buttons/HostSelector/server.hpp>
#include <xyz/openbmc_project/Common/FilePath/server.hpp>
#include <xyz/openbmc_project/Common/ObjectPath/server.hpp>
#include <xyz/openbmc_project/Common/OriginatedBy/server.hpp>
#include <xyz/openbmc_project/Common/Progress/server.hpp>
#include <xyz/openbmc_project/Common/UUID/server.hpp>
#include <xyz/openbmc_project/Condition/HostFirmware/server.hpp>
#include <xyz/openbmc_project/Control/CFMLimit/server.hpp>
#include <xyz/openbmc_project/Control/ChassisCapabilities/server.hpp>
#include <xyz/openbmc_project/Control/FanPwm/server.hpp>
#include <xyz/openbmc_project/Control/FanSpeed/server.hpp>
#include <xyz/openbmc_project/Control/FieldMode/server.hpp>
#include <xyz/openbmc_project/Control/MinimumShipLevel/server.hpp>
#include <xyz/openbmc_project/Control/Mode/server.hpp>
#include <xyz/openbmc_project/Control/PowerSupplyAttributes/server.hpp>
#include <xyz/openbmc_project/Control/PowerSupplyRedundancy/server.hpp>
#include <xyz/openbmc_project/Control/ThermalMode/server.hpp>
#include <xyz/openbmc_project/Control/VoltageRegulatorControl/server.hpp>
#include <xyz/openbmc_project/Control/VoltageRegulatorMode/server.hpp>
#include <xyz/openbmc_project/Control/Boot/Mode/server.hpp>
#include <xyz/openbmc_project/Control/Boot/RebootAttempts/server.hpp>
#include <xyz/openbmc_project/Control/Boot/RebootPolicy/server.hpp>
#include <xyz/openbmc_project/Control/Boot/Source/server.hpp>
#include <xyz/openbmc_project/Control/Boot/Type/server.hpp>
#include <xyz/openbmc_project/Control/Host/TurboAllowed/server.hpp>
#include <xyz/openbmc_project/Control/Power/ACPIPowerState/server.hpp>
#include <xyz/openbmc_project/Control/Power/Cap/server.hpp>
#include <xyz/openbmc_project/Control/Power/IdlePowerSaver/server.hpp>
#include <xyz/openbmc_project/Control/Power/Mode/server.hpp>
#include <xyz/openbmc_project/Control/Power/RestorePolicy/server.hpp>
#include <xyz/openbmc_project/Control/Security/RestrictionMode/server.hpp>
#include <xyz/openbmc_project/Control/Security/SpecialMode/server.hpp>
#include <xyz/openbmc_project/Control/Service/Attributes/server.hpp>
#include <xyz/openbmc_project/Control/Service/SocketAttributes/server.hpp>
#include <xyz/openbmc_project/Control/TPM/Policy/server.hpp>
#include <xyz/openbmc_project/Dump/Entry/BMC/server.hpp>
#include <xyz/openbmc_project/Dump/Entry/FaultLog/server.hpp>
#include <xyz/openbmc_project/Dump/Entry/System/server.hpp>
#include <xyz/openbmc_project/HardwareIsolation/Entry/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/server.hpp>
#include <xyz/openbmc_project/Inventory/Connector/Embedded/server.hpp>
#include <xyz/openbmc_project/Inventory/Connector/Slot/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Asset/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/AssetTag/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/CLEI/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Cacheable/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Compatible/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/CoolingType/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Dimension/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/I2CDevice/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/LocationCode/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/ManufacturerExt/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/MeetsMinimumShipLevel/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Replaceable/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Revision/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Slot/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/UniqueIdentifier/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/VendorInformation/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/VoltageControl/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Accelerator/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Battery/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Bmc/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Board/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Cable/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Chassis/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Connector/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Cpu/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/CpuCore/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Dimm/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/DiskBackplane/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Drive/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Ethernet/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/FabricAdapter/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Fan/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Global/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/MemoryBuffer/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/NetworkInterface/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PCIeDevice/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PCIeSlot/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Panel/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PowerSupply/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Rotor/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Storage/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/StorageController/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/System/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Tpm/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Vrm/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Board/IOBoard/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Board/Motherboard/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Cpu/OperatingConfig/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Dimm/MemoryLocation/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/Partition/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/PowerManagementPolicy/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/SecurityCapabilities/server.hpp>
#include <xyz/openbmc_project/Inventory/Source/PLDM/Entity/server.hpp>
#include <xyz/openbmc_project/Inventory/Source/PLDM/FRU/server.hpp>
#include <xyz/openbmc_project/Ipmi/SOL/server.hpp>
#include <xyz/openbmc_project/Ipmi/SessionInfo/server.hpp>
#include <xyz/openbmc_project/Led/Group/server.hpp>
#include <xyz/openbmc_project/Led/Physical/server.hpp>
#include <xyz/openbmc_project/Logging/ErrorBlocksTransition/server.hpp>
#include <xyz/openbmc_project/Logging/Event/server.hpp>
#include <xyz/openbmc_project/Logging/Settings/server.hpp>
#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/server.hpp>
#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/Entry/server.hpp>
#include <xyz/openbmc_project/MCTP/Endpoint/server.hpp>
#include <xyz/openbmc_project/Memory/MemoryECC/server.hpp>
#include <xyz/openbmc_project/Network/Client/server.hpp>
#include <xyz/openbmc_project/Network/DHCPConfiguration/server.hpp>
#include <xyz/openbmc_project/Network/EthernetInterface/server.hpp>
#include <xyz/openbmc_project/Network/IP/server.hpp>
#include <xyz/openbmc_project/Network/MACAddress/server.hpp>
#include <xyz/openbmc_project/Network/Neighbor/server.hpp>
#include <xyz/openbmc_project/Network/SystemConfiguration/server.hpp>
#include <xyz/openbmc_project/Network/VLAN/server.hpp>
#include <xyz/openbmc_project/Network/Experimental/Bond/server.hpp>
#include <xyz/openbmc_project/Network/Experimental/Tunnel/server.hpp>
#include <xyz/openbmc_project/Nvme/Status/server.hpp>
#include <xyz/openbmc_project/Object/Enable/server.hpp>
#include <xyz/openbmc_project/PFR/Attributes/server.hpp>
#include <xyz/openbmc_project/PLDM/Event/server.hpp>
#include <xyz/openbmc_project/PLDM/Provider/Certs/Authority/CSR/server.hpp>
#include <xyz/openbmc_project/Sensor/Accuracy/server.hpp>
#include <xyz/openbmc_project/Sensor/Value/server.hpp>
#include <xyz/openbmc_project/Sensor/ValueMutability/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/Critical/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/HardShutdown/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/PerformanceLoss/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/SoftShutdown/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/Warning/server.hpp>
#include <xyz/openbmc_project/Software/Activation/server.hpp>
#include <xyz/openbmc_project/Software/ActivationBlocksTransition/server.hpp>
#include <xyz/openbmc_project/Software/ActivationProgress/server.hpp>
#include <xyz/openbmc_project/Software/ApplyOptions/server.hpp>
#include <xyz/openbmc_project/Software/ApplyTime/server.hpp>
#include <xyz/openbmc_project/Software/ExtendedVersion/server.hpp>
#include <xyz/openbmc_project/Software/RedundancyPriority/server.hpp>
#include <xyz/openbmc_project/Software/RequestedRedundancyPriority/server.hpp>
#include <xyz/openbmc_project/Software/Settings/server.hpp>
#include <xyz/openbmc_project/Software/Version/server.hpp>
#include <xyz/openbmc_project/State/BMC/server.hpp>
#include <xyz/openbmc_project/State/Chassis/server.hpp>
#include <xyz/openbmc_project/State/Drive/server.hpp>
#include <xyz/openbmc_project/State/Host/server.hpp>
#include <xyz/openbmc_project/State/PowerOnHours/server.hpp>
#include <xyz/openbmc_project/State/ScheduledHostTransition/server.hpp>
#include <xyz/openbmc_project/State/Boot/Progress/server.hpp>
#include <xyz/openbmc_project/State/Boot/Raw/server.hpp>
#include <xyz/openbmc_project/State/Decorator/Availability/server.hpp>
#include <xyz/openbmc_project/State/Decorator/OperationalStatus/server.hpp>
#include <xyz/openbmc_project/State/Decorator/PowerState/server.hpp>
#include <xyz/openbmc_project/State/Decorator/PowerSystemInputs/server.hpp>
#include <xyz/openbmc_project/State/OperatingSystem/Status/server.hpp>
#include <xyz/openbmc_project/Time/EpochTime/server.hpp>
#include <xyz/openbmc_project/Time/Synchronization/server.hpp>
#include <xyz/openbmc_project/User/AccountPolicy/server.hpp>
#include <xyz/openbmc_project/User/Attributes/server.hpp>
#include <xyz/openbmc_project/User/PrivilegeMapperEntry/server.hpp>
#include <xyz/openbmc_project/User/Ldap/Config/server.hpp>
#include <xyz/openbmc_project/VirtualMedia/MountPoint/server.hpp>
#include <xyz/openbmc_project/VirtualMedia/Process/server.hpp>
#include <xyz/openbmc_project/VirtualMedia/Stats/server.hpp>
#include "gen_serialization.hpp"

namespace phosphor
{
namespace inventory
{
namespace manager
{

using namespace std::literals::string_literals;

const Manager::Makers Manager::_makers{
    {
        "xyz.openbmc_project.Association.Definitions",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Association::server::Definitions>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Association::server::Definitions>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Association::server::Definitions>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Association::server::Definitions>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Association::server::Definitions>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Certs.Certificate",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Certificate>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Certificate>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Certificate>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Certificate>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Certificate>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Certs.Entry",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Entry>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Entry>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Entry>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Entry>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Certs::server::Entry>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Channel.ChannelAccess",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Chassis.Intrusion",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Chassis.Buttons.HostSelector",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Common.FilePath",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::FilePath>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::FilePath>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::FilePath>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::FilePath>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::FilePath>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Common.ObjectPath",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::ObjectPath>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::ObjectPath>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::ObjectPath>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::ObjectPath>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::ObjectPath>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Common.OriginatedBy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Common.Progress",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::Progress>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::Progress>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::Progress>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::Progress>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::Progress>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Common.UUID",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::UUID>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::UUID>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::UUID>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::UUID>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Common::server::UUID>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Condition.HostFirmware",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.CFMLimit",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::CFMLimit>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::CFMLimit>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::CFMLimit>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::CFMLimit>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::CFMLimit>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.ChassisCapabilities",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.FanPwm",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanPwm>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanPwm>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanPwm>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanPwm>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanPwm>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.FanSpeed",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanSpeed>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanSpeed>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanSpeed>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanSpeed>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FanSpeed>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.FieldMode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FieldMode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FieldMode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FieldMode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FieldMode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::FieldMode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.MinimumShipLevel",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Mode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::Mode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::Mode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::Mode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::Mode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::Mode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.PowerSupplyAttributes",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.PowerSupplyRedundancy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.ThermalMode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ThermalMode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ThermalMode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ThermalMode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ThermalMode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::ThermalMode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.VoltageRegulatorControl",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.VoltageRegulatorMode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Boot.Mode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Boot.RebootAttempts",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Boot.RebootPolicy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Boot.Source",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Source>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Source>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Source>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Source>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Source>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Boot.Type",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Type>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Type>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Type>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Type>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Boot::server::Type>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Host.TurboAllowed",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Power.ACPIPowerState",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Power.Cap",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Cap>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Cap>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Cap>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Cap>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Cap>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Power.IdlePowerSaver",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Power.Mode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Mode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Mode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Mode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Mode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::Mode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Power.RestorePolicy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Security.RestrictionMode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Security.SpecialMode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Service.Attributes",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.Service.SocketAttributes",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Control.TPM.Policy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Dump.Entry.BMC",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Dump.Entry.FaultLog",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Dump.Entry.System",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::System>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::System>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::System>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::System>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Dump::Entry::server::System>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.HardwareIsolation.Entry",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::server::Item>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::server::Item>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::server::Item>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::server::Item>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::server::Item>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Connector.Embedded",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Connector.Slot",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Asset",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.AssetTag",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.CLEI",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Cacheable",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Compatible",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.CoolingType",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Dimension",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.I2CDevice",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.LocationCode",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.ManufacturerExt",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.MeetsMinimumShipLevel",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Replaceable",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Revision",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.Slot",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.UniqueIdentifier",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.VendorInformation",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Decorator.VoltageControl",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Accelerator",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Battery",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Bmc",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Board",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Cable",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Chassis",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Connector",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Cpu",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.CpuCore",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Dimm",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.DiskBackplane",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Drive",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Ethernet",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.FabricAdapter",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Fan",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Global",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.MemoryBuffer",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.NetworkInterface",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PCIeDevice",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PCIeSlot",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Panel",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PersistentMemory",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PowerSupply",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Rotor",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Storage",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.StorageController",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.System",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::System>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::System>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::System>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::System>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::System>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Tpm",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Vrm",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Board.IOBoard",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Board.Motherboard",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Cpu.OperatingConfig",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.Dimm.MemoryLocation",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PersistentMemory.Partition",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PersistentMemory.PowerManagementPolicy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Item.PersistentMemory.SecurityCapabilities",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Source.PLDM.Entity",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Inventory.Source.PLDM.FRU",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Ipmi.SOL",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SOL>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SOL>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SOL>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SOL>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SOL>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Ipmi.SessionInfo",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Led.Group",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Group>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Group>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Group>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Group>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Group>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Led.Physical",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Physical>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Physical>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Physical>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Physical>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Led::server::Physical>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Logging.ErrorBlocksTransition",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Logging.Event",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Event>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Event>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Event>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Event>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Event>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Logging.Settings",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Settings>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Settings>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Settings>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Settings>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::server::Settings>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Logging.Syslog.Destination.Mail",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Logging.Syslog.Destination.Mail.Entry",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.MCTP.Endpoint",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Memory.MemoryECC",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.Client",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Client>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Client>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Client>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Client>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Client>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.DHCPConfiguration",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.EthernetInterface",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.IP",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::IP>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::IP>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::IP>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::IP>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::IP>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.MACAddress",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::MACAddress>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::MACAddress>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::MACAddress>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::MACAddress>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::MACAddress>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.Neighbor",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Neighbor>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Neighbor>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Neighbor>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Neighbor>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::Neighbor>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.SystemConfiguration",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.VLAN",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::VLAN>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::VLAN>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::VLAN>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::VLAN>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::server::VLAN>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.Experimental.Bond",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Network.Experimental.Tunnel",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Nvme.Status",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Nvme::server::Status>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Nvme::server::Status>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Nvme::server::Status>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Nvme::server::Status>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Nvme::server::Status>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Object.Enable",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Object::server::Enable>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Object::server::Enable>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Object::server::Enable>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Object::server::Enable>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Object::server::Enable>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.PFR.Attributes",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PFR::server::Attributes>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PFR::server::Attributes>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PFR::server::Attributes>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PFR::server::Attributes>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PFR::server::Attributes>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.PLDM.Event",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::server::Event>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::server::Event>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::server::Event>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::server::Event>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::server::Event>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.PLDM.Provider.Certs.Authority.CSR",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Accuracy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Value",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Value>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Value>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Value>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Value>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::Value>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.ValueMutability",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Threshold.Critical",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Threshold.HardShutdown",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Threshold.PerformanceLoss",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Threshold.SoftShutdown",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Sensor.Threshold.Warning",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.Activation",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Activation>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Activation>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Activation>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Activation>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Activation>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.ActivationBlocksTransition",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.ActivationProgress",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.ApplyOptions",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.ApplyTime",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyTime>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyTime>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyTime>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyTime>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ApplyTime>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.ExtendedVersion",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.RedundancyPriority",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.RequestedRedundancyPriority",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.Settings",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Settings>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Settings>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Settings>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Settings>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Settings>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Software.Version",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Version>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Version>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Version>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Version>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Software::server::Version>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.BMC",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::BMC>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::BMC>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::BMC>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::BMC>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::BMC>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Chassis",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Chassis>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Chassis>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Chassis>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Chassis>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Chassis>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Drive",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Drive>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Drive>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Drive>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Drive>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Drive>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Host",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Host>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Host>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Host>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Host>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::Host>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.PowerOnHours",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::PowerOnHours>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::PowerOnHours>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::PowerOnHours>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::PowerOnHours>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::PowerOnHours>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.ScheduledHostTransition",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Boot.Progress",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Progress>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Progress>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Progress>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Progress>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Progress>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Boot.Raw",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Raw>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Raw>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Raw>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Raw>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Boot::server::Raw>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Decorator.Availability",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Decorator.OperationalStatus",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Decorator.PowerState",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.Decorator.PowerSystemInputs",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.State.OperatingSystem.Status",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Time.EpochTime",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::EpochTime>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::EpochTime>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::EpochTime>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::EpochTime>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::EpochTime>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.Time.Synchronization",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::Synchronization>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::Synchronization>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::Synchronization>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::Synchronization>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::Time::server::Synchronization>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.User.AccountPolicy",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::AccountPolicy>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::AccountPolicy>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::AccountPolicy>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::AccountPolicy>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::AccountPolicy>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.User.Attributes",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::Attributes>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::Attributes>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::Attributes>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::Attributes>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::Attributes>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.User.PrivilegeMapperEntry",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.User.Ldap.Config",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::Ldap::server::Config>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::Ldap::server::Config>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::Ldap::server::Config>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::Ldap::server::Config>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::User::Ldap::server::Config>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.VirtualMedia.MountPoint",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.VirtualMedia.Process",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process>>::op
#endif
        )
    },
    {
        "xyz.openbmc_project.VirtualMedia.Stats",
        std::make_tuple(
            MakeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats>>::op,
            AssignInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats>>::op,
            SerializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats>, SerialOps>::op,
            DeserializeInterface<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats>, SerialOps>::op
#ifdef CREATE_ASSOCIATIONS
            , GetPropertyValue<
                ServerObject<
                    sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats>>::op
#endif
        )
    },
};

const Manager::Events Manager::_events{
    {
        // Add the chassis interface on the chassis inventory path
        std::make_tuple(
            std::vector<EventBasePtr>(
                {
                    std::make_shared<StartupEvent>(
                        std::vector<Filter>(
                            {
                                
                            }))
                }),
            std::vector<Action>(
                {
                    make_action(
                        functor::createObjects(
                            {
                                {
                                    "/system/chassis"s,
                                    {
                                        {
                                            "xyz.openbmc_project.Inventory.Item.Chassis",
                                            {
                                                {
                                                    "Type",
                                                    "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.RackMount"s
                                                }
                                            }
                                        }
                                    }
                                }
                            }))
                })),
    },
    {
        // Create /system at startup and populate the AssetTag property with empty string.
        std::make_tuple(
            std::vector<EventBasePtr>(
                {
                    std::make_shared<StartupEvent>(
                        std::vector<Filter>(
                            {
                                
                            }))
                }),
            std::vector<Action>(
                {
                    make_action(
                        functor::createObjects(
                            {
                                {
                                    "/system"s,
                                    {
                                        {
                                            "xyz.openbmc_project.Inventory.Decorator.AssetTag",
                                            {
                                                {
                                                    "AssetTag",
                                                    ""s
                                                }
                                            }
                                        }
                                    }
                                }
                            }))
                })),
    },
};

} // namespace manager
} // namespace inventory
} // namespace phosphor
