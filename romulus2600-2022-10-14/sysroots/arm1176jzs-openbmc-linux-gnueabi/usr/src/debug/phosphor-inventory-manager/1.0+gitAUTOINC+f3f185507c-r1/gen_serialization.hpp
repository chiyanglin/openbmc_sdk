// This file was auto generated.  Do not edit.

#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include "config.h"
#include <xyz/openbmc_project/Association/Definitions/server.hpp>
#include <xyz/openbmc_project/Certs/Certificate/server.hpp>
#include <xyz/openbmc_project/Certs/Entry/server.hpp>
#include <xyz/openbmc_project/Channel/ChannelAccess/server.hpp>
#include <xyz/openbmc_project/Chassis/Intrusion/server.hpp>
#include <xyz/openbmc_project/Chassis/Buttons/HostSelector/server.hpp>
#include <xyz/openbmc_project/Common/FilePath/server.hpp>
#include <xyz/openbmc_project/Common/ObjectPath/server.hpp>
#include <xyz/openbmc_project/Common/OriginatedBy/server.hpp>
#include <xyz/openbmc_project/Common/Progress/server.hpp>
#include <xyz/openbmc_project/Common/UUID/server.hpp>
#include <xyz/openbmc_project/Condition/HostFirmware/server.hpp>
#include <xyz/openbmc_project/Control/CFMLimit/server.hpp>
#include <xyz/openbmc_project/Control/ChassisCapabilities/server.hpp>
#include <xyz/openbmc_project/Control/FanPwm/server.hpp>
#include <xyz/openbmc_project/Control/FanSpeed/server.hpp>
#include <xyz/openbmc_project/Control/FieldMode/server.hpp>
#include <xyz/openbmc_project/Control/MinimumShipLevel/server.hpp>
#include <xyz/openbmc_project/Control/Mode/server.hpp>
#include <xyz/openbmc_project/Control/PowerSupplyAttributes/server.hpp>
#include <xyz/openbmc_project/Control/PowerSupplyRedundancy/server.hpp>
#include <xyz/openbmc_project/Control/ThermalMode/server.hpp>
#include <xyz/openbmc_project/Control/VoltageRegulatorControl/server.hpp>
#include <xyz/openbmc_project/Control/VoltageRegulatorMode/server.hpp>
#include <xyz/openbmc_project/Control/Boot/Mode/server.hpp>
#include <xyz/openbmc_project/Control/Boot/RebootAttempts/server.hpp>
#include <xyz/openbmc_project/Control/Boot/RebootPolicy/server.hpp>
#include <xyz/openbmc_project/Control/Boot/Source/server.hpp>
#include <xyz/openbmc_project/Control/Boot/Type/server.hpp>
#include <xyz/openbmc_project/Control/Host/TurboAllowed/server.hpp>
#include <xyz/openbmc_project/Control/Power/ACPIPowerState/server.hpp>
#include <xyz/openbmc_project/Control/Power/Cap/server.hpp>
#include <xyz/openbmc_project/Control/Power/IdlePowerSaver/server.hpp>
#include <xyz/openbmc_project/Control/Power/Mode/server.hpp>
#include <xyz/openbmc_project/Control/Power/RestorePolicy/server.hpp>
#include <xyz/openbmc_project/Control/Security/RestrictionMode/server.hpp>
#include <xyz/openbmc_project/Control/Security/SpecialMode/server.hpp>
#include <xyz/openbmc_project/Control/Service/Attributes/server.hpp>
#include <xyz/openbmc_project/Control/Service/SocketAttributes/server.hpp>
#include <xyz/openbmc_project/Control/TPM/Policy/server.hpp>
#include <xyz/openbmc_project/Dump/Entry/BMC/server.hpp>
#include <xyz/openbmc_project/Dump/Entry/FaultLog/server.hpp>
#include <xyz/openbmc_project/Dump/Entry/System/server.hpp>
#include <xyz/openbmc_project/HardwareIsolation/Entry/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/server.hpp>
#include <xyz/openbmc_project/Inventory/Connector/Embedded/server.hpp>
#include <xyz/openbmc_project/Inventory/Connector/Slot/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Asset/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/AssetTag/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/CLEI/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Cacheable/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Compatible/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/CoolingType/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Dimension/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/I2CDevice/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/LocationCode/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/ManufacturerExt/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/MeetsMinimumShipLevel/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Replaceable/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Revision/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/Slot/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/UniqueIdentifier/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/VendorInformation/server.hpp>
#include <xyz/openbmc_project/Inventory/Decorator/VoltageControl/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Accelerator/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Battery/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Bmc/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Board/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Cable/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Chassis/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Connector/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Cpu/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/CpuCore/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Dimm/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/DiskBackplane/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Drive/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Ethernet/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/FabricAdapter/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Fan/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Global/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/MemoryBuffer/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/NetworkInterface/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PCIeDevice/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PCIeSlot/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Panel/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PowerSupply/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Rotor/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Storage/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/StorageController/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/System/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Tpm/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Vrm/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Board/IOBoard/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Board/Motherboard/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Cpu/OperatingConfig/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/Dimm/MemoryLocation/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/Partition/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/PowerManagementPolicy/server.hpp>
#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/SecurityCapabilities/server.hpp>
#include <xyz/openbmc_project/Inventory/Source/PLDM/Entity/server.hpp>
#include <xyz/openbmc_project/Inventory/Source/PLDM/FRU/server.hpp>
#include <xyz/openbmc_project/Ipmi/SOL/server.hpp>
#include <xyz/openbmc_project/Ipmi/SessionInfo/server.hpp>
#include <xyz/openbmc_project/Led/Group/server.hpp>
#include <xyz/openbmc_project/Led/Physical/server.hpp>
#include <xyz/openbmc_project/Logging/ErrorBlocksTransition/server.hpp>
#include <xyz/openbmc_project/Logging/Event/server.hpp>
#include <xyz/openbmc_project/Logging/Settings/server.hpp>
#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/server.hpp>
#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/Entry/server.hpp>
#include <xyz/openbmc_project/MCTP/Endpoint/server.hpp>
#include <xyz/openbmc_project/Memory/MemoryECC/server.hpp>
#include <xyz/openbmc_project/Network/Client/server.hpp>
#include <xyz/openbmc_project/Network/DHCPConfiguration/server.hpp>
#include <xyz/openbmc_project/Network/EthernetInterface/server.hpp>
#include <xyz/openbmc_project/Network/IP/server.hpp>
#include <xyz/openbmc_project/Network/MACAddress/server.hpp>
#include <xyz/openbmc_project/Network/Neighbor/server.hpp>
#include <xyz/openbmc_project/Network/SystemConfiguration/server.hpp>
#include <xyz/openbmc_project/Network/VLAN/server.hpp>
#include <xyz/openbmc_project/Network/Experimental/Bond/server.hpp>
#include <xyz/openbmc_project/Network/Experimental/Tunnel/server.hpp>
#include <xyz/openbmc_project/Nvme/Status/server.hpp>
#include <xyz/openbmc_project/Object/Enable/server.hpp>
#include <xyz/openbmc_project/PFR/Attributes/server.hpp>
#include <xyz/openbmc_project/PLDM/Event/server.hpp>
#include <xyz/openbmc_project/PLDM/Provider/Certs/Authority/CSR/server.hpp>
#include <xyz/openbmc_project/Sensor/Accuracy/server.hpp>
#include <xyz/openbmc_project/Sensor/Value/server.hpp>
#include <xyz/openbmc_project/Sensor/ValueMutability/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/Critical/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/HardShutdown/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/PerformanceLoss/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/SoftShutdown/server.hpp>
#include <xyz/openbmc_project/Sensor/Threshold/Warning/server.hpp>
#include <xyz/openbmc_project/Software/Activation/server.hpp>
#include <xyz/openbmc_project/Software/ActivationBlocksTransition/server.hpp>
#include <xyz/openbmc_project/Software/ActivationProgress/server.hpp>
#include <xyz/openbmc_project/Software/ApplyOptions/server.hpp>
#include <xyz/openbmc_project/Software/ApplyTime/server.hpp>
#include <xyz/openbmc_project/Software/ExtendedVersion/server.hpp>
#include <xyz/openbmc_project/Software/RedundancyPriority/server.hpp>
#include <xyz/openbmc_project/Software/RequestedRedundancyPriority/server.hpp>
#include <xyz/openbmc_project/Software/Settings/server.hpp>
#include <xyz/openbmc_project/Software/Version/server.hpp>
#include <xyz/openbmc_project/State/BMC/server.hpp>
#include <xyz/openbmc_project/State/Chassis/server.hpp>
#include <xyz/openbmc_project/State/Drive/server.hpp>
#include <xyz/openbmc_project/State/Host/server.hpp>
#include <xyz/openbmc_project/State/PowerOnHours/server.hpp>
#include <xyz/openbmc_project/State/ScheduledHostTransition/server.hpp>
#include <xyz/openbmc_project/State/Boot/Progress/server.hpp>
#include <xyz/openbmc_project/State/Boot/Raw/server.hpp>
#include <xyz/openbmc_project/State/Decorator/Availability/server.hpp>
#include <xyz/openbmc_project/State/Decorator/OperationalStatus/server.hpp>
#include <xyz/openbmc_project/State/Decorator/PowerState/server.hpp>
#include <xyz/openbmc_project/State/Decorator/PowerSystemInputs/server.hpp>
#include <xyz/openbmc_project/State/OperatingSystem/Status/server.hpp>
#include <xyz/openbmc_project/Time/EpochTime/server.hpp>
#include <xyz/openbmc_project/Time/Synchronization/server.hpp>
#include <xyz/openbmc_project/User/AccountPolicy/server.hpp>
#include <xyz/openbmc_project/User/Attributes/server.hpp>
#include <xyz/openbmc_project/User/PrivilegeMapperEntry/server.hpp>
#include <xyz/openbmc_project/User/Ldap/Config/server.hpp>
#include <xyz/openbmc_project/VirtualMedia/MountPoint/server.hpp>
#include <xyz/openbmc_project/VirtualMedia/Process/server.hpp>
#include <xyz/openbmc_project/VirtualMedia/Stats/server.hpp>

CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Association::server::Definitions, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Certs::server::Certificate, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Certs::server::Entry, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Common::server::FilePath, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Common::server::ObjectPath, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Common::server::Progress, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Common::server::UUID, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::CFMLimit, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::FanPwm, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::FanSpeed, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::FieldMode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::Mode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::ThermalMode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Boot::server::Source, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Boot::server::Type, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Power::server::Cap, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Power::server::Mode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Dump::Entry::server::System, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::server::Item, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::System, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Ipmi::server::SOL, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Led::server::Group, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Led::server::Physical, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Logging::server::Event, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Logging::server::Settings, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::Client, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::IP, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::MACAddress, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::Neighbor, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::server::VLAN, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Nvme::server::Status, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Object::server::Enable, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::PFR::server::Attributes, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::PLDM::server::Event, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::server::Value, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::Activation, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::ApplyTime, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::Settings, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Software::server::Version, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::server::BMC, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::server::Chassis, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::server::Drive, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::server::Host, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::server::PowerOnHours, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::Boot::server::Progress, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::Boot::server::Raw, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Time::server::EpochTime, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::Time::server::Synchronization, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::User::server::AccountPolicy, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::User::server::Attributes, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::User::Ldap::server::Config, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process, CLASS_VERSION);
CEREAL_CLASS_VERSION(sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats, CLASS_VERSION);

// Emitting signals prior to claiming a well known DBus service name causes
// un-necessary DBus traffic and wakeups.  De-serialization only happens prior
// to claiming a well known name, so don't emit signals.
static constexpr auto skipSignals = true;
namespace cereal
{
// The version we started using cereal NVP from
static constexpr size_t CLASS_VERSION_WITH_NVP = 2;

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Association::server::Definitions& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Associations", object.associations()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Association::server::Definitions& object,
          const std::uint32_t version)
{
    decltype(object.associations()) Associations{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Associations);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Associations", Associations));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.associations(Associations, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Certs::server::Certificate& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CertificateString", object.certificateString()));
        a(cereal::make_nvp("KeyUsage", object.keyUsage()));
        a(cereal::make_nvp("Issuer", object.issuer()));
        a(cereal::make_nvp("Subject", object.subject()));
        a(cereal::make_nvp("ValidNotAfter", object.validNotAfter()));
        a(cereal::make_nvp("ValidNotBefore", object.validNotBefore()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Certs::server::Certificate& object,
          const std::uint32_t version)
{
    decltype(object.certificateString()) CertificateString{};
    decltype(object.keyUsage()) KeyUsage{};
    decltype(object.issuer()) Issuer{};
    decltype(object.subject()) Subject{};
    decltype(object.validNotAfter()) ValidNotAfter{};
    decltype(object.validNotBefore()) ValidNotBefore{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CertificateString, KeyUsage, Issuer, Subject, ValidNotAfter, ValidNotBefore);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CertificateString", CertificateString));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("KeyUsage", KeyUsage));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Issuer", Issuer));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Subject", Subject));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ValidNotAfter", ValidNotAfter));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ValidNotBefore", ValidNotBefore));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.certificateString(CertificateString, skipSignals);
    object.keyUsage(KeyUsage, skipSignals);
    object.issuer(Issuer, skipSignals);
    object.subject(Subject, skipSignals);
    object.validNotAfter(ValidNotAfter, skipSignals);
    object.validNotBefore(ValidNotBefore, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Certs::server::Entry& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ClientCertificate", object.clientCertificate()));
        a(cereal::make_nvp("Status", object.status()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Certs::server::Entry& object,
          const std::uint32_t version)
{
    decltype(object.clientCertificate()) ClientCertificate{};
    decltype(object.status()) Status{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ClientCertificate, Status);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ClientCertificate", ClientCertificate));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Status", Status));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.clientCertificate(ClientCertificate, skipSignals);
    object.status(Status, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MaxPrivilege", object.maxPrivilege()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Channel::server::ChannelAccess& object,
          const std::uint32_t version)
{
    decltype(object.maxPrivilege()) MaxPrivilege{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MaxPrivilege);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MaxPrivilege", MaxPrivilege));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.maxPrivilege(MaxPrivilege, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Status", object.status()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Chassis::server::Intrusion& object,
          const std::uint32_t version)
{
    decltype(object.status()) Status{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Status);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Status", Status));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.status(Status, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Position", object.position()));
        a(cereal::make_nvp("MaxPosition", object.maxPosition()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Chassis::Buttons::server::HostSelector& object,
          const std::uint32_t version)
{
    decltype(object.position()) Position{};
    decltype(object.maxPosition()) MaxPosition{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Position, MaxPosition);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Position", Position));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxPosition", MaxPosition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.position(Position, skipSignals);
    object.maxPosition(MaxPosition, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Common::server::FilePath& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Path", object.path()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Common::server::FilePath& object,
          const std::uint32_t version)
{
    decltype(object.path()) Path{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Path);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Path", Path));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.path(Path, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Common::server::ObjectPath& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Path", object.path()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Common::server::ObjectPath& object,
          const std::uint32_t version)
{
    decltype(object.path()) Path{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Path);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Path", Path));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.path(Path, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("OriginatorId", object.originatorId()));
        a(cereal::make_nvp("OriginatorType", object.originatorType()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy& object,
          const std::uint32_t version)
{
    decltype(object.originatorId()) OriginatorId{};
    decltype(object.originatorType()) OriginatorType{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(OriginatorId, OriginatorType);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("OriginatorId", OriginatorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("OriginatorType", OriginatorType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.originatorId(OriginatorId, skipSignals);
    object.originatorType(OriginatorType, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Common::server::Progress& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Status", object.status()));
        a(cereal::make_nvp("StartTime", object.startTime()));
        a(cereal::make_nvp("CompletedTime", object.completedTime()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Common::server::Progress& object,
          const std::uint32_t version)
{
    decltype(object.status()) Status{};
    decltype(object.startTime()) StartTime{};
    decltype(object.completedTime()) CompletedTime{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Status, StartTime, CompletedTime);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Status", Status));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("StartTime", StartTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CompletedTime", CompletedTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.status(Status, skipSignals);
    object.startTime(StartTime, skipSignals);
    object.completedTime(CompletedTime, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Common::server::UUID& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("UUID", object.uuid()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Common::server::UUID& object,
          const std::uint32_t version)
{
    decltype(object.uuid()) UUID{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(UUID);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("UUID", UUID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.uuid(UUID, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CurrentFirmwareCondition", object.currentFirmwareCondition()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware& object,
          const std::uint32_t version)
{
    decltype(object.currentFirmwareCondition()) CurrentFirmwareCondition{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CurrentFirmwareCondition);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CurrentFirmwareCondition", CurrentFirmwareCondition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.currentFirmwareCondition(CurrentFirmwareCondition, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::CFMLimit& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Limit", object.limit()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::CFMLimit& object,
          const std::uint32_t version)
{
    decltype(object.limit()) Limit{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Limit);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Limit", Limit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.limit(Limit, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CapabilitiesFlags", object.capabilitiesFlags()));
        a(cereal::make_nvp("ChassisIntrusionEnabled", object.chassisIntrusionEnabled()));
        a(cereal::make_nvp("ChassisFrontPanelLockoutEnabled", object.chassisFrontPanelLockoutEnabled()));
        a(cereal::make_nvp("ChassisNMIEnabled", object.chassisNMIEnabled()));
        a(cereal::make_nvp("ChassisPowerInterlockEnabled", object.chassisPowerInterlockEnabled()));
        a(cereal::make_nvp("FRUDeviceAddress", object.fruDeviceAddress()));
        a(cereal::make_nvp("SDRDeviceAddress", object.sdrDeviceAddress()));
        a(cereal::make_nvp("SELDeviceAddress", object.selDeviceAddress()));
        a(cereal::make_nvp("SMDeviceAddress", object.smDeviceAddress()));
        a(cereal::make_nvp("BridgeDeviceAddress", object.bridgeDeviceAddress()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::ChassisCapabilities& object,
          const std::uint32_t version)
{
    decltype(object.capabilitiesFlags()) CapabilitiesFlags{};
    decltype(object.chassisIntrusionEnabled()) ChassisIntrusionEnabled{};
    decltype(object.chassisFrontPanelLockoutEnabled()) ChassisFrontPanelLockoutEnabled{};
    decltype(object.chassisNMIEnabled()) ChassisNMIEnabled{};
    decltype(object.chassisPowerInterlockEnabled()) ChassisPowerInterlockEnabled{};
    decltype(object.fruDeviceAddress()) FRUDeviceAddress{};
    decltype(object.sdrDeviceAddress()) SDRDeviceAddress{};
    decltype(object.selDeviceAddress()) SELDeviceAddress{};
    decltype(object.smDeviceAddress()) SMDeviceAddress{};
    decltype(object.bridgeDeviceAddress()) BridgeDeviceAddress{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CapabilitiesFlags, ChassisIntrusionEnabled, ChassisFrontPanelLockoutEnabled, ChassisNMIEnabled, ChassisPowerInterlockEnabled, FRUDeviceAddress, SDRDeviceAddress, SELDeviceAddress, SMDeviceAddress, BridgeDeviceAddress);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CapabilitiesFlags", CapabilitiesFlags));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ChassisIntrusionEnabled", ChassisIntrusionEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ChassisFrontPanelLockoutEnabled", ChassisFrontPanelLockoutEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ChassisNMIEnabled", ChassisNMIEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ChassisPowerInterlockEnabled", ChassisPowerInterlockEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("FRUDeviceAddress", FRUDeviceAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SDRDeviceAddress", SDRDeviceAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SELDeviceAddress", SELDeviceAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SMDeviceAddress", SMDeviceAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BridgeDeviceAddress", BridgeDeviceAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.capabilitiesFlags(CapabilitiesFlags, skipSignals);
    object.chassisIntrusionEnabled(ChassisIntrusionEnabled, skipSignals);
    object.chassisFrontPanelLockoutEnabled(ChassisFrontPanelLockoutEnabled, skipSignals);
    object.chassisNMIEnabled(ChassisNMIEnabled, skipSignals);
    object.chassisPowerInterlockEnabled(ChassisPowerInterlockEnabled, skipSignals);
    object.fruDeviceAddress(FRUDeviceAddress, skipSignals);
    object.sdrDeviceAddress(SDRDeviceAddress, skipSignals);
    object.selDeviceAddress(SELDeviceAddress, skipSignals);
    object.smDeviceAddress(SMDeviceAddress, skipSignals);
    object.bridgeDeviceAddress(BridgeDeviceAddress, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::FanPwm& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Target", object.target()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::FanPwm& object,
          const std::uint32_t version)
{
    decltype(object.target()) Target{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Target);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Target", Target));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.target(Target, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::FanSpeed& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Target", object.target()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::FanSpeed& object,
          const std::uint32_t version)
{
    decltype(object.target()) Target{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Target);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Target", Target));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.target(Target, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::FieldMode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("FieldModeEnabled", object.fieldModeEnabled()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::FieldMode& object,
          const std::uint32_t version)
{
    decltype(object.fieldModeEnabled()) FieldModeEnabled{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(FieldModeEnabled);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("FieldModeEnabled", FieldModeEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.fieldModeEnabled(FieldModeEnabled, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MinimumShipLevelRequired", object.minimumShipLevelRequired()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::MinimumShipLevel& object,
          const std::uint32_t version)
{
    decltype(object.minimumShipLevelRequired()) MinimumShipLevelRequired{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MinimumShipLevelRequired);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MinimumShipLevelRequired", MinimumShipLevelRequired));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.minimumShipLevelRequired(MinimumShipLevelRequired, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::Mode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Manual", object.manual()));
        a(cereal::make_nvp("FailSafe", object.failSafe()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::Mode& object,
          const std::uint32_t version)
{
    decltype(object.manual()) Manual{};
    decltype(object.failSafe()) FailSafe{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Manual, FailSafe);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Manual", Manual));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("FailSafe", FailSafe));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.manual(Manual, skipSignals);
    object.failSafe(FailSafe, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("DeratingFactor", object.deratingFactor()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyAttributes& object,
          const std::uint32_t version)
{
    decltype(object.deratingFactor()) DeratingFactor{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(DeratingFactor);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("DeratingFactor", DeratingFactor));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.deratingFactor(DeratingFactor, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PowerSupplyRedundancyEnabled", object.powerSupplyRedundancyEnabled()));
        a(cereal::make_nvp("RotationEnabled", object.rotationEnabled()));
        a(cereal::make_nvp("RotationAlgorithm", object.rotationAlgorithm()));
        a(cereal::make_nvp("RotationRankOrder", object.rotationRankOrder()));
        a(cereal::make_nvp("PeriodOfRotation", object.periodOfRotation()));
        a(cereal::make_nvp("ColdRedundancyStatus", object.coldRedundancyStatus()));
        a(cereal::make_nvp("PSUNumber", object.psuNumber()));
        a(cereal::make_nvp("RedundantCount", object.redundantCount()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy& object,
          const std::uint32_t version)
{
    decltype(object.powerSupplyRedundancyEnabled()) PowerSupplyRedundancyEnabled{};
    decltype(object.rotationEnabled()) RotationEnabled{};
    decltype(object.rotationAlgorithm()) RotationAlgorithm{};
    decltype(object.rotationRankOrder()) RotationRankOrder{};
    decltype(object.periodOfRotation()) PeriodOfRotation{};
    decltype(object.coldRedundancyStatus()) ColdRedundancyStatus{};
    decltype(object.psuNumber()) PSUNumber{};
    decltype(object.redundantCount()) RedundantCount{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PowerSupplyRedundancyEnabled, RotationEnabled, RotationAlgorithm, RotationRankOrder, PeriodOfRotation, ColdRedundancyStatus, PSUNumber, RedundantCount);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PowerSupplyRedundancyEnabled", PowerSupplyRedundancyEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RotationEnabled", RotationEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RotationAlgorithm", RotationAlgorithm));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RotationRankOrder", RotationRankOrder));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PeriodOfRotation", PeriodOfRotation));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ColdRedundancyStatus", ColdRedundancyStatus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PSUNumber", PSUNumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RedundantCount", RedundantCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.powerSupplyRedundancyEnabled(PowerSupplyRedundancyEnabled, skipSignals);
    object.rotationEnabled(RotationEnabled, skipSignals);
    object.rotationAlgorithm(RotationAlgorithm, skipSignals);
    object.rotationRankOrder(RotationRankOrder, skipSignals);
    object.periodOfRotation(PeriodOfRotation, skipSignals);
    object.coldRedundancyStatus(ColdRedundancyStatus, skipSignals);
    object.psuNumber(PSUNumber, skipSignals);
    object.redundantCount(RedundantCount, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::ThermalMode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Supported", object.supported()));
        a(cereal::make_nvp("Current", object.current()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::ThermalMode& object,
          const std::uint32_t version)
{
    decltype(object.supported()) Supported{};
    decltype(object.current()) Current{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Supported, Current);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Supported", Supported));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Current", Current));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.supported(Supported, skipSignals);
    object.current(Current, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Voltage", object.voltage()));
        a(cereal::make_nvp("MaxValue", object.maxValue()));
        a(cereal::make_nvp("MinValue", object.minValue()));
        a(cereal::make_nvp("Resolution", object.resolution()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorControl& object,
          const std::uint32_t version)
{
    decltype(object.voltage()) Voltage{};
    decltype(object.maxValue()) MaxValue{};
    decltype(object.minValue()) MinValue{};
    decltype(object.resolution()) Resolution{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Voltage, MaxValue, MinValue, Resolution);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Voltage", Voltage));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxValue", MaxValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MinValue", MinValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Resolution", Resolution));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.voltage(Voltage, skipSignals);
    object.maxValue(MaxValue, skipSignals);
    object.minValue(MinValue, skipSignals);
    object.resolution(Resolution, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Supported", object.supported()));
        a(cereal::make_nvp("Selected", object.selected()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::server::VoltageRegulatorMode& object,
          const std::uint32_t version)
{
    decltype(object.supported()) Supported{};
    decltype(object.selected()) Selected{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Supported, Selected);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Supported", Supported));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Selected", Selected));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.supported(Supported, skipSignals);
    object.selected(Selected, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("BootMode", object.bootMode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode& object,
          const std::uint32_t version)
{
    decltype(object.bootMode()) BootMode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(BootMode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("BootMode", BootMode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.bootMode(BootMode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("AttemptsLeft", object.attemptsLeft()));
        a(cereal::make_nvp("RetryAttempts", object.retryAttempts()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootAttempts& object,
          const std::uint32_t version)
{
    decltype(object.attemptsLeft()) AttemptsLeft{};
    decltype(object.retryAttempts()) RetryAttempts{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(AttemptsLeft, RetryAttempts);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("AttemptsLeft", AttemptsLeft));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RetryAttempts", RetryAttempts));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.attemptsLeft(AttemptsLeft, skipSignals);
    object.retryAttempts(RetryAttempts, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("AutoReboot", object.autoReboot()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Boot::server::RebootPolicy& object,
          const std::uint32_t version)
{
    decltype(object.autoReboot()) AutoReboot{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(AutoReboot);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("AutoReboot", AutoReboot));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.autoReboot(AutoReboot, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Boot::server::Source& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("BootSource", object.bootSource()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Boot::server::Source& object,
          const std::uint32_t version)
{
    decltype(object.bootSource()) BootSource{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(BootSource);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("BootSource", BootSource));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.bootSource(BootSource, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Boot::server::Type& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("BootType", object.bootType()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Boot::server::Type& object,
          const std::uint32_t version)
{
    decltype(object.bootType()) BootType{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(BootType);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("BootType", BootType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.bootType(BootType, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("TurboAllowed", object.turboAllowed()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Host::server::TurboAllowed& object,
          const std::uint32_t version)
{
    decltype(object.turboAllowed()) TurboAllowed{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(TurboAllowed);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("TurboAllowed", TurboAllowed));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.turboAllowed(TurboAllowed, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SysACPIStatus", object.sysACPIStatus()));
        a(cereal::make_nvp("DevACPIStatus", object.devACPIStatus()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState& object,
          const std::uint32_t version)
{
    decltype(object.sysACPIStatus()) SysACPIStatus{};
    decltype(object.devACPIStatus()) DevACPIStatus{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SysACPIStatus, DevACPIStatus);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SysACPIStatus", SysACPIStatus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DevACPIStatus", DevACPIStatus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.sysACPIStatus(SysACPIStatus, skipSignals);
    object.devACPIStatus(DevACPIStatus, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Power::server::Cap& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PowerCap", object.powerCap()));
        a(cereal::make_nvp("PowerCapEnable", object.powerCapEnable()));
        a(cereal::make_nvp("MinPowerCapValue", object.minPowerCapValue()));
        a(cereal::make_nvp("MaxPowerCapValue", object.maxPowerCapValue()));
        a(cereal::make_nvp("MinSoftPowerCapValue", object.minSoftPowerCapValue()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Power::server::Cap& object,
          const std::uint32_t version)
{
    decltype(object.powerCap()) PowerCap{};
    decltype(object.powerCapEnable()) PowerCapEnable{};
    decltype(object.minPowerCapValue()) MinPowerCapValue{};
    decltype(object.maxPowerCapValue()) MaxPowerCapValue{};
    decltype(object.minSoftPowerCapValue()) MinSoftPowerCapValue{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PowerCap, PowerCapEnable, MinPowerCapValue, MaxPowerCapValue, MinSoftPowerCapValue);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PowerCap", PowerCap));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PowerCapEnable", PowerCapEnable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MinPowerCapValue", MinPowerCapValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxPowerCapValue", MaxPowerCapValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MinSoftPowerCapValue", MinSoftPowerCapValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.powerCap(PowerCap, skipSignals);
    object.powerCapEnable(PowerCapEnable, skipSignals);
    object.minPowerCapValue(MinPowerCapValue, skipSignals);
    object.maxPowerCapValue(MaxPowerCapValue, skipSignals);
    object.minSoftPowerCapValue(MinSoftPowerCapValue, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Enabled", object.enabled()));
        a(cereal::make_nvp("EnterUtilizationPercent", object.enterUtilizationPercent()));
        a(cereal::make_nvp("EnterDwellTime", object.enterDwellTime()));
        a(cereal::make_nvp("ExitUtilizationPercent", object.exitUtilizationPercent()));
        a(cereal::make_nvp("ExitDwellTime", object.exitDwellTime()));
        a(cereal::make_nvp("Active", object.active()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Power::server::IdlePowerSaver& object,
          const std::uint32_t version)
{
    decltype(object.enabled()) Enabled{};
    decltype(object.enterUtilizationPercent()) EnterUtilizationPercent{};
    decltype(object.enterDwellTime()) EnterDwellTime{};
    decltype(object.exitUtilizationPercent()) ExitUtilizationPercent{};
    decltype(object.exitDwellTime()) ExitDwellTime{};
    decltype(object.active()) Active{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Enabled, EnterUtilizationPercent, EnterDwellTime, ExitUtilizationPercent, ExitDwellTime, Active);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Enabled", Enabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EnterUtilizationPercent", EnterUtilizationPercent));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EnterDwellTime", EnterDwellTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ExitUtilizationPercent", ExitUtilizationPercent));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ExitDwellTime", ExitDwellTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Active", Active));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.enabled(Enabled, skipSignals);
    object.enterUtilizationPercent(EnterUtilizationPercent, skipSignals);
    object.enterDwellTime(EnterDwellTime, skipSignals);
    object.exitUtilizationPercent(ExitUtilizationPercent, skipSignals);
    object.exitDwellTime(ExitDwellTime, skipSignals);
    object.active(Active, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Power::server::Mode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PowerMode", object.powerMode()));
        a(cereal::make_nvp("SafeMode", object.safeMode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Power::server::Mode& object,
          const std::uint32_t version)
{
    decltype(object.powerMode()) PowerMode{};
    decltype(object.safeMode()) SafeMode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PowerMode, SafeMode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PowerMode", PowerMode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SafeMode", SafeMode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.powerMode(PowerMode, skipSignals);
    object.safeMode(SafeMode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PowerRestorePolicy", object.powerRestorePolicy()));
        a(cereal::make_nvp("PowerRestoreDelay", object.powerRestoreDelay()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy& object,
          const std::uint32_t version)
{
    decltype(object.powerRestorePolicy()) PowerRestorePolicy{};
    decltype(object.powerRestoreDelay()) PowerRestoreDelay{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PowerRestorePolicy, PowerRestoreDelay);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PowerRestorePolicy", PowerRestorePolicy));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PowerRestoreDelay", PowerRestoreDelay));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.powerRestorePolicy(PowerRestorePolicy, skipSignals);
    object.powerRestoreDelay(PowerRestoreDelay, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("RestrictionMode", object.restrictionMode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode& object,
          const std::uint32_t version)
{
    decltype(object.restrictionMode()) RestrictionMode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(RestrictionMode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("RestrictionMode", RestrictionMode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.restrictionMode(RestrictionMode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SpecialMode", object.specialMode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode& object,
          const std::uint32_t version)
{
    decltype(object.specialMode()) SpecialMode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SpecialMode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SpecialMode", SpecialMode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.specialMode(SpecialMode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Enabled", object.enabled()));
        a(cereal::make_nvp("Masked", object.masked()));
        a(cereal::make_nvp("Running", object.running()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Service::server::Attributes& object,
          const std::uint32_t version)
{
    decltype(object.enabled()) Enabled{};
    decltype(object.masked()) Masked{};
    decltype(object.running()) Running{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Enabled, Masked, Running);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Enabled", Enabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Masked", Masked));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Running", Running));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.enabled(Enabled, skipSignals);
    object.masked(Masked, skipSignals);
    object.running(Running, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Port", object.port()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::Service::server::SocketAttributes& object,
          const std::uint32_t version)
{
    decltype(object.port()) Port{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Port);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Port", Port));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.port(Port, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("TPMEnable", object.tpmEnable()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Control::TPM::server::Policy& object,
          const std::uint32_t version)
{
    decltype(object.tpmEnable()) TPMEnable{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(TPMEnable);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("TPMEnable", TPMEnable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.tpmEnable(TPMEnable, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Dump::Entry::server::BMC& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Type", object.type()));
        a(cereal::make_nvp("AdditionalTypeName", object.additionalTypeName()));
        a(cereal::make_nvp("PrimaryLogId", object.primaryLogId()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog& object,
          const std::uint32_t version)
{
    decltype(object.type()) Type{};
    decltype(object.additionalTypeName()) AdditionalTypeName{};
    decltype(object.primaryLogId()) PrimaryLogId{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Type, AdditionalTypeName, PrimaryLogId);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Type", Type));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AdditionalTypeName", AdditionalTypeName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PrimaryLogId", PrimaryLogId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.type(Type, skipSignals);
    object.additionalTypeName(AdditionalTypeName, skipSignals);
    object.primaryLogId(PrimaryLogId, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Dump::Entry::server::System& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SourceDumpId", object.sourceDumpId()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Dump::Entry::server::System& object,
          const std::uint32_t version)
{
    decltype(object.sourceDumpId()) SourceDumpId{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SourceDumpId);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SourceDumpId", SourceDumpId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.sourceDumpId(SourceDumpId, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Severity", object.severity()));
        a(cereal::make_nvp("Resolved", object.resolved()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry& object,
          const std::uint32_t version)
{
    decltype(object.severity()) Severity{};
    decltype(object.resolved()) Resolved{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Severity, Resolved);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Severity", Severity));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Resolved", Resolved));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.severity(Severity, skipSignals);
    object.resolved(Resolved, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::server::Item& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PrettyName", object.prettyName()));
        a(cereal::make_nvp("Present", object.present()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::server::Item& object,
          const std::uint32_t version)
{
    decltype(object.prettyName()) PrettyName{};
    decltype(object.present()) Present{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PrettyName, Present);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PrettyName", PrettyName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Present", Present));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.prettyName(PrettyName, skipSignals);
    object.present(Present, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Embedded& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Connector::server::Slot& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PartNumber", object.partNumber()));
        a(cereal::make_nvp("SerialNumber", object.serialNumber()));
        a(cereal::make_nvp("Manufacturer", object.manufacturer()));
        a(cereal::make_nvp("BuildDate", object.buildDate()));
        a(cereal::make_nvp("Model", object.model()));
        a(cereal::make_nvp("SubModel", object.subModel()));
        a(cereal::make_nvp("SparePartNumber", object.sparePartNumber()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Asset& object,
          const std::uint32_t version)
{
    decltype(object.partNumber()) PartNumber{};
    decltype(object.serialNumber()) SerialNumber{};
    decltype(object.manufacturer()) Manufacturer{};
    decltype(object.buildDate()) BuildDate{};
    decltype(object.model()) Model{};
    decltype(object.subModel()) SubModel{};
    decltype(object.sparePartNumber()) SparePartNumber{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PartNumber, SerialNumber, Manufacturer, BuildDate, Model, SubModel, SparePartNumber);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PartNumber", PartNumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SerialNumber", SerialNumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Manufacturer", Manufacturer));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BuildDate", BuildDate));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Model", Model));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SubModel", SubModel));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SparePartNumber", SparePartNumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.partNumber(PartNumber, skipSignals);
    object.serialNumber(SerialNumber, skipSignals);
    object.manufacturer(Manufacturer, skipSignals);
    object.buildDate(BuildDate, skipSignals);
    object.model(Model, skipSignals);
    object.subModel(SubModel, skipSignals);
    object.sparePartNumber(SparePartNumber, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("AssetTag", object.assetTag()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::AssetTag& object,
          const std::uint32_t version)
{
    decltype(object.assetTag()) AssetTag{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(AssetTag);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("AssetTag", AssetTag));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.assetTag(AssetTag, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CLEINumber", object.cleiNumber()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CLEI& object,
          const std::uint32_t version)
{
    decltype(object.cleiNumber()) CLEINumber{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CLEINumber);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CLEINumber", CLEINumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.cleiNumber(CLEINumber, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Cached", object.cached()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Cacheable& object,
          const std::uint32_t version)
{
    decltype(object.cached()) Cached{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Cached);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Cached", Cached));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.cached(Cached, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Names", object.names()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Compatible& object,
          const std::uint32_t version)
{
    decltype(object.names()) Names{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Names);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Names", Names));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.names(Names, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("AirCooled", object.airCooled()));
        a(cereal::make_nvp("WaterCooled", object.waterCooled()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::CoolingType& object,
          const std::uint32_t version)
{
    decltype(object.airCooled()) AirCooled{};
    decltype(object.waterCooled()) WaterCooled{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(AirCooled, WaterCooled);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("AirCooled", AirCooled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("WaterCooled", WaterCooled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.airCooled(AirCooled, skipSignals);
    object.waterCooled(WaterCooled, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Height", object.height()));
        a(cereal::make_nvp("Width", object.width()));
        a(cereal::make_nvp("Depth", object.depth()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Dimension& object,
          const std::uint32_t version)
{
    decltype(object.height()) Height{};
    decltype(object.width()) Width{};
    decltype(object.depth()) Depth{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Height, Width, Depth);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Height", Height));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Width", Width));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Depth", Depth));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.height(Height, skipSignals);
    object.width(Width, skipSignals);
    object.depth(Depth, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Bus", object.bus()));
        a(cereal::make_nvp("Address", object.address()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::I2CDevice& object,
          const std::uint32_t version)
{
    decltype(object.bus()) Bus{};
    decltype(object.address()) Address{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Bus, Address);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Bus", Bus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Address", Address));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.bus(Bus, skipSignals);
    object.address(Address, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("LocationCode", object.locationCode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::LocationCode& object,
          const std::uint32_t version)
{
    decltype(object.locationCode()) LocationCode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(LocationCode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("LocationCode", LocationCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.locationCode(LocationCode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ExtendedMFGData", object.extendedMFGData()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::ManufacturerExt& object,
          const std::uint32_t version)
{
    decltype(object.extendedMFGData()) ExtendedMFGData{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ExtendedMFGData);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ExtendedMFGData", ExtendedMFGData));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.extendedMFGData(ExtendedMFGData, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MeetsMinimumShipLevel", object.meetsMinimumShipLevel()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::MeetsMinimumShipLevel& object,
          const std::uint32_t version)
{
    decltype(object.meetsMinimumShipLevel()) MeetsMinimumShipLevel{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MeetsMinimumShipLevel);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MeetsMinimumShipLevel", MeetsMinimumShipLevel));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.meetsMinimumShipLevel(MeetsMinimumShipLevel, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("FieldReplaceable", object.fieldReplaceable()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Replaceable& object,
          const std::uint32_t version)
{
    decltype(object.fieldReplaceable()) FieldReplaceable{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(FieldReplaceable);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("FieldReplaceable", FieldReplaceable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.fieldReplaceable(FieldReplaceable, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Version", object.version()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Revision& object,
          const std::uint32_t version)
{
    decltype(object.version()) Version{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Version);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Version", Version));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.version(Version, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SlotNumber", object.slotNumber()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::Slot& object,
          const std::uint32_t version)
{
    decltype(object.slotNumber()) SlotNumber{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SlotNumber);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SlotNumber", SlotNumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.slotNumber(SlotNumber, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("UniqueIdentifier", object.uniqueIdentifier()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::UniqueIdentifier& object,
          const std::uint32_t version)
{
    decltype(object.uniqueIdentifier()) UniqueIdentifier{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(UniqueIdentifier);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("UniqueIdentifier", UniqueIdentifier));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.uniqueIdentifier(UniqueIdentifier, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CustomField1", object.customField1()));
        a(cereal::make_nvp("CustomField2", object.customField2()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VendorInformation& object,
          const std::uint32_t version)
{
    decltype(object.customField1()) CustomField1{};
    decltype(object.customField2()) CustomField2{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CustomField1, CustomField2);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CustomField1", CustomField1));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CustomField2", CustomField2));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.customField1(CustomField1, skipSignals);
    object.customField2(CustomField2, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Decorator::server::VoltageControl& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Type", object.type()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator& object,
          const std::uint32_t version)
{
    decltype(object.type()) Type{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Type);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Type", Type));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.type(Type, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Battery& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Bmc& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Board& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Length", object.length()));
        a(cereal::make_nvp("CableTypeDescription", object.cableTypeDescription()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cable& object,
          const std::uint32_t version)
{
    decltype(object.length()) Length{};
    decltype(object.cableTypeDescription()) CableTypeDescription{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Length, CableTypeDescription);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Length", Length));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CableTypeDescription", CableTypeDescription));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.length(Length, skipSignals);
    object.cableTypeDescription(CableTypeDescription, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Type", object.type()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis& object,
          const std::uint32_t version)
{
    decltype(object.type()) Type{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Type);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Type", Type));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.type(Type, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Connector& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Socket", object.socket()));
        a(cereal::make_nvp("Family", object.family()));
        a(cereal::make_nvp("EffectiveFamily", object.effectiveFamily()));
        a(cereal::make_nvp("EffectiveModel", object.effectiveModel()));
        a(cereal::make_nvp("Id", object.id()));
        a(cereal::make_nvp("MaxSpeedInMhz", object.maxSpeedInMhz()));
        a(cereal::make_nvp("Characteristics", object.characteristics()));
        a(cereal::make_nvp("CoreCount", object.coreCount()));
        a(cereal::make_nvp("ThreadCount", object.threadCount()));
        a(cereal::make_nvp("Step", object.step()));
        a(cereal::make_nvp("Microcode", object.microcode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu& object,
          const std::uint32_t version)
{
    decltype(object.socket()) Socket{};
    decltype(object.family()) Family{};
    decltype(object.effectiveFamily()) EffectiveFamily{};
    decltype(object.effectiveModel()) EffectiveModel{};
    decltype(object.id()) Id{};
    decltype(object.maxSpeedInMhz()) MaxSpeedInMhz{};
    decltype(object.characteristics()) Characteristics{};
    decltype(object.coreCount()) CoreCount{};
    decltype(object.threadCount()) ThreadCount{};
    decltype(object.step()) Step{};
    decltype(object.microcode()) Microcode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Socket, Family, EffectiveFamily, EffectiveModel, Id, MaxSpeedInMhz, Characteristics, CoreCount, ThreadCount, Step, Microcode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Socket", Socket));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Family", Family));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EffectiveFamily", EffectiveFamily));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EffectiveModel", EffectiveModel));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Id", Id));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxSpeedInMhz", MaxSpeedInMhz));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Characteristics", Characteristics));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CoreCount", CoreCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ThreadCount", ThreadCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Step", Step));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Microcode", Microcode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.socket(Socket, skipSignals);
    object.family(Family, skipSignals);
    object.effectiveFamily(EffectiveFamily, skipSignals);
    object.effectiveModel(EffectiveModel, skipSignals);
    object.id(Id, skipSignals);
    object.maxSpeedInMhz(MaxSpeedInMhz, skipSignals);
    object.characteristics(Characteristics, skipSignals);
    object.coreCount(CoreCount, skipSignals);
    object.threadCount(ThreadCount, skipSignals);
    object.step(Step, skipSignals);
    object.microcode(Microcode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Microcode", object.microcode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::CpuCore& object,
          const std::uint32_t version)
{
    decltype(object.microcode()) Microcode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Microcode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Microcode", Microcode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.microcode(Microcode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MemoryDataWidth", object.memoryDataWidth()));
        a(cereal::make_nvp("MemorySizeInKB", object.memorySizeInKB()));
        a(cereal::make_nvp("MemoryDeviceLocator", object.memoryDeviceLocator()));
        a(cereal::make_nvp("MemoryType", object.memoryType()));
        a(cereal::make_nvp("MemoryTypeDetail", object.memoryTypeDetail()));
        a(cereal::make_nvp("MaxMemorySpeedInMhz", object.maxMemorySpeedInMhz()));
        a(cereal::make_nvp("MemoryAttributes", object.memoryAttributes()));
        a(cereal::make_nvp("MemoryConfiguredSpeedInMhz", object.memoryConfiguredSpeedInMhz()));
        a(cereal::make_nvp("ECC", object.ecc()));
        a(cereal::make_nvp("CASLatencies", object.casLatencies()));
        a(cereal::make_nvp("RevisionCode", object.revisionCode()));
        a(cereal::make_nvp("FormFactor", object.formFactor()));
        a(cereal::make_nvp("MemoryTotalWidth", object.memoryTotalWidth()));
        a(cereal::make_nvp("AllowedSpeedsMT", object.allowedSpeedsMT()));
        a(cereal::make_nvp("MemoryMedia", object.memoryMedia()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm& object,
          const std::uint32_t version)
{
    decltype(object.memoryDataWidth()) MemoryDataWidth{};
    decltype(object.memorySizeInKB()) MemorySizeInKB{};
    decltype(object.memoryDeviceLocator()) MemoryDeviceLocator{};
    decltype(object.memoryType()) MemoryType{};
    decltype(object.memoryTypeDetail()) MemoryTypeDetail{};
    decltype(object.maxMemorySpeedInMhz()) MaxMemorySpeedInMhz{};
    decltype(object.memoryAttributes()) MemoryAttributes{};
    decltype(object.memoryConfiguredSpeedInMhz()) MemoryConfiguredSpeedInMhz{};
    decltype(object.ecc()) ECC{};
    decltype(object.casLatencies()) CASLatencies{};
    decltype(object.revisionCode()) RevisionCode{};
    decltype(object.formFactor()) FormFactor{};
    decltype(object.memoryTotalWidth()) MemoryTotalWidth{};
    decltype(object.allowedSpeedsMT()) AllowedSpeedsMT{};
    decltype(object.memoryMedia()) MemoryMedia{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MemoryDataWidth, MemorySizeInKB, MemoryDeviceLocator, MemoryType, MemoryTypeDetail, MaxMemorySpeedInMhz, MemoryAttributes, MemoryConfiguredSpeedInMhz, ECC, CASLatencies, RevisionCode, FormFactor, MemoryTotalWidth, AllowedSpeedsMT, MemoryMedia);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MemoryDataWidth", MemoryDataWidth));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemorySizeInKB", MemorySizeInKB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryDeviceLocator", MemoryDeviceLocator));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryType", MemoryType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryTypeDetail", MemoryTypeDetail));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxMemorySpeedInMhz", MaxMemorySpeedInMhz));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryAttributes", MemoryAttributes));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryConfiguredSpeedInMhz", MemoryConfiguredSpeedInMhz));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ECC", ECC));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CASLatencies", CASLatencies));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RevisionCode", RevisionCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("FormFactor", FormFactor));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryTotalWidth", MemoryTotalWidth));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AllowedSpeedsMT", AllowedSpeedsMT));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryMedia", MemoryMedia));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.memoryDataWidth(MemoryDataWidth, skipSignals);
    object.memorySizeInKB(MemorySizeInKB, skipSignals);
    object.memoryDeviceLocator(MemoryDeviceLocator, skipSignals);
    object.memoryType(MemoryType, skipSignals);
    object.memoryTypeDetail(MemoryTypeDetail, skipSignals);
    object.maxMemorySpeedInMhz(MaxMemorySpeedInMhz, skipSignals);
    object.memoryAttributes(MemoryAttributes, skipSignals);
    object.memoryConfiguredSpeedInMhz(MemoryConfiguredSpeedInMhz, skipSignals);
    object.ecc(ECC, skipSignals);
    object.casLatencies(CASLatencies, skipSignals);
    object.revisionCode(RevisionCode, skipSignals);
    object.formFactor(FormFactor, skipSignals);
    object.memoryTotalWidth(MemoryTotalWidth, skipSignals);
    object.allowedSpeedsMT(AllowedSpeedsMT, skipSignals);
    object.memoryMedia(MemoryMedia, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::DiskBackplane& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Capacity", object.capacity()));
        a(cereal::make_nvp("Protocol", object.protocol()));
        a(cereal::make_nvp("Type", object.type()));
        a(cereal::make_nvp("EncryptionStatus", object.encryptionStatus()));
        a(cereal::make_nvp("LockedStatus", object.lockedStatus()));
        a(cereal::make_nvp("PredictedMediaLifeLeftPercent", object.predictedMediaLifeLeftPercent()));
        a(cereal::make_nvp("Resettable", object.resettable()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive& object,
          const std::uint32_t version)
{
    decltype(object.capacity()) Capacity{};
    decltype(object.protocol()) Protocol{};
    decltype(object.type()) Type{};
    decltype(object.encryptionStatus()) EncryptionStatus{};
    decltype(object.lockedStatus()) LockedStatus{};
    decltype(object.predictedMediaLifeLeftPercent()) PredictedMediaLifeLeftPercent{};
    decltype(object.resettable()) Resettable{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Capacity, Protocol, Type, EncryptionStatus, LockedStatus, PredictedMediaLifeLeftPercent, Resettable);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Capacity", Capacity));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Protocol", Protocol));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Type", Type));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EncryptionStatus", EncryptionStatus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LockedStatus", LockedStatus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PredictedMediaLifeLeftPercent", PredictedMediaLifeLeftPercent));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Resettable", Resettable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.capacity(Capacity, skipSignals);
    object.protocol(Protocol, skipSignals);
    object.type(Type, skipSignals);
    object.encryptionStatus(EncryptionStatus, skipSignals);
    object.lockedStatus(LockedStatus, skipSignals);
    object.predictedMediaLifeLeftPercent(PredictedMediaLifeLeftPercent, skipSignals);
    object.resettable(Resettable, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Ethernet& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::FabricAdapter& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Fan& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Global& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::MemoryBuffer& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MACAddress", object.macAddress()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::NetworkInterface& object,
          const std::uint32_t version)
{
    decltype(object.macAddress()) MACAddress{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MACAddress);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MACAddress", MACAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.macAddress(MACAddress, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("DeviceType", object.deviceType()));
        a(cereal::make_nvp("GenerationInUse", object.generationInUse()));
        a(cereal::make_nvp("GenerationSupported", object.generationSupported()));
        a(cereal::make_nvp("Function0ClassCode", object.function0ClassCode()));
        a(cereal::make_nvp("Function0DeviceClass", object.function0DeviceClass()));
        a(cereal::make_nvp("Function0DeviceId", object.function0DeviceId()));
        a(cereal::make_nvp("Function0FunctionType", object.function0FunctionType()));
        a(cereal::make_nvp("Function0RevisionId", object.function0RevisionId()));
        a(cereal::make_nvp("Function0SubsystemId", object.function0SubsystemId()));
        a(cereal::make_nvp("Function0SubsystemVendorId", object.function0SubsystemVendorId()));
        a(cereal::make_nvp("Function0VendorId", object.function0VendorId()));
        a(cereal::make_nvp("Function1ClassCode", object.function1ClassCode()));
        a(cereal::make_nvp("Function1DeviceClass", object.function1DeviceClass()));
        a(cereal::make_nvp("Function1DeviceId", object.function1DeviceId()));
        a(cereal::make_nvp("Function1FunctionType", object.function1FunctionType()));
        a(cereal::make_nvp("Function1RevisionId", object.function1RevisionId()));
        a(cereal::make_nvp("Function1SubsystemId", object.function1SubsystemId()));
        a(cereal::make_nvp("Function1SubsystemVendorId", object.function1SubsystemVendorId()));
        a(cereal::make_nvp("Function1VendorId", object.function1VendorId()));
        a(cereal::make_nvp("Function2ClassCode", object.function2ClassCode()));
        a(cereal::make_nvp("Function2DeviceClass", object.function2DeviceClass()));
        a(cereal::make_nvp("Function2DeviceId", object.function2DeviceId()));
        a(cereal::make_nvp("Function2FunctionType", object.function2FunctionType()));
        a(cereal::make_nvp("Function2RevisionId", object.function2RevisionId()));
        a(cereal::make_nvp("Function2SubsystemId", object.function2SubsystemId()));
        a(cereal::make_nvp("Function2SubsystemVendorId", object.function2SubsystemVendorId()));
        a(cereal::make_nvp("Function2VendorId", object.function2VendorId()));
        a(cereal::make_nvp("Function3ClassCode", object.function3ClassCode()));
        a(cereal::make_nvp("Function3DeviceClass", object.function3DeviceClass()));
        a(cereal::make_nvp("Function3DeviceId", object.function3DeviceId()));
        a(cereal::make_nvp("Function3FunctionType", object.function3FunctionType()));
        a(cereal::make_nvp("Function3RevisionId", object.function3RevisionId()));
        a(cereal::make_nvp("Function3SubsystemId", object.function3SubsystemId()));
        a(cereal::make_nvp("Function3SubsystemVendorId", object.function3SubsystemVendorId()));
        a(cereal::make_nvp("Function3VendorId", object.function3VendorId()));
        a(cereal::make_nvp("Function4ClassCode", object.function4ClassCode()));
        a(cereal::make_nvp("Function4DeviceClass", object.function4DeviceClass()));
        a(cereal::make_nvp("Function4DeviceId", object.function4DeviceId()));
        a(cereal::make_nvp("Function4FunctionType", object.function4FunctionType()));
        a(cereal::make_nvp("Function4RevisionId", object.function4RevisionId()));
        a(cereal::make_nvp("Function4SubsystemId", object.function4SubsystemId()));
        a(cereal::make_nvp("Function4SubsystemVendorId", object.function4SubsystemVendorId()));
        a(cereal::make_nvp("Function4VendorId", object.function4VendorId()));
        a(cereal::make_nvp("Function5ClassCode", object.function5ClassCode()));
        a(cereal::make_nvp("Function5DeviceClass", object.function5DeviceClass()));
        a(cereal::make_nvp("Function5DeviceId", object.function5DeviceId()));
        a(cereal::make_nvp("Function5FunctionType", object.function5FunctionType()));
        a(cereal::make_nvp("Function5RevisionId", object.function5RevisionId()));
        a(cereal::make_nvp("Function5SubsystemId", object.function5SubsystemId()));
        a(cereal::make_nvp("Function5SubsystemVendorId", object.function5SubsystemVendorId()));
        a(cereal::make_nvp("Function5VendorId", object.function5VendorId()));
        a(cereal::make_nvp("Function6ClassCode", object.function6ClassCode()));
        a(cereal::make_nvp("Function6DeviceClass", object.function6DeviceClass()));
        a(cereal::make_nvp("Function6DeviceId", object.function6DeviceId()));
        a(cereal::make_nvp("Function6FunctionType", object.function6FunctionType()));
        a(cereal::make_nvp("Function6RevisionId", object.function6RevisionId()));
        a(cereal::make_nvp("Function6SubsystemId", object.function6SubsystemId()));
        a(cereal::make_nvp("Function6SubsystemVendorId", object.function6SubsystemVendorId()));
        a(cereal::make_nvp("Function6VendorId", object.function6VendorId()));
        a(cereal::make_nvp("Function7ClassCode", object.function7ClassCode()));
        a(cereal::make_nvp("Function7DeviceClass", object.function7DeviceClass()));
        a(cereal::make_nvp("Function7DeviceId", object.function7DeviceId()));
        a(cereal::make_nvp("Function7FunctionType", object.function7FunctionType()));
        a(cereal::make_nvp("Function7RevisionId", object.function7RevisionId()));
        a(cereal::make_nvp("Function7SubsystemId", object.function7SubsystemId()));
        a(cereal::make_nvp("Function7SubsystemVendorId", object.function7SubsystemVendorId()));
        a(cereal::make_nvp("Function7VendorId", object.function7VendorId()));
        a(cereal::make_nvp("Manufacturer", object.manufacturer()));
        a(cereal::make_nvp("MaxLanes", object.maxLanes()));
        a(cereal::make_nvp("LanesInUse", object.lanesInUse()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeDevice& object,
          const std::uint32_t version)
{
    decltype(object.deviceType()) DeviceType{};
    decltype(object.generationInUse()) GenerationInUse{};
    decltype(object.generationSupported()) GenerationSupported{};
    decltype(object.function0ClassCode()) Function0ClassCode{};
    decltype(object.function0DeviceClass()) Function0DeviceClass{};
    decltype(object.function0DeviceId()) Function0DeviceId{};
    decltype(object.function0FunctionType()) Function0FunctionType{};
    decltype(object.function0RevisionId()) Function0RevisionId{};
    decltype(object.function0SubsystemId()) Function0SubsystemId{};
    decltype(object.function0SubsystemVendorId()) Function0SubsystemVendorId{};
    decltype(object.function0VendorId()) Function0VendorId{};
    decltype(object.function1ClassCode()) Function1ClassCode{};
    decltype(object.function1DeviceClass()) Function1DeviceClass{};
    decltype(object.function1DeviceId()) Function1DeviceId{};
    decltype(object.function1FunctionType()) Function1FunctionType{};
    decltype(object.function1RevisionId()) Function1RevisionId{};
    decltype(object.function1SubsystemId()) Function1SubsystemId{};
    decltype(object.function1SubsystemVendorId()) Function1SubsystemVendorId{};
    decltype(object.function1VendorId()) Function1VendorId{};
    decltype(object.function2ClassCode()) Function2ClassCode{};
    decltype(object.function2DeviceClass()) Function2DeviceClass{};
    decltype(object.function2DeviceId()) Function2DeviceId{};
    decltype(object.function2FunctionType()) Function2FunctionType{};
    decltype(object.function2RevisionId()) Function2RevisionId{};
    decltype(object.function2SubsystemId()) Function2SubsystemId{};
    decltype(object.function2SubsystemVendorId()) Function2SubsystemVendorId{};
    decltype(object.function2VendorId()) Function2VendorId{};
    decltype(object.function3ClassCode()) Function3ClassCode{};
    decltype(object.function3DeviceClass()) Function3DeviceClass{};
    decltype(object.function3DeviceId()) Function3DeviceId{};
    decltype(object.function3FunctionType()) Function3FunctionType{};
    decltype(object.function3RevisionId()) Function3RevisionId{};
    decltype(object.function3SubsystemId()) Function3SubsystemId{};
    decltype(object.function3SubsystemVendorId()) Function3SubsystemVendorId{};
    decltype(object.function3VendorId()) Function3VendorId{};
    decltype(object.function4ClassCode()) Function4ClassCode{};
    decltype(object.function4DeviceClass()) Function4DeviceClass{};
    decltype(object.function4DeviceId()) Function4DeviceId{};
    decltype(object.function4FunctionType()) Function4FunctionType{};
    decltype(object.function4RevisionId()) Function4RevisionId{};
    decltype(object.function4SubsystemId()) Function4SubsystemId{};
    decltype(object.function4SubsystemVendorId()) Function4SubsystemVendorId{};
    decltype(object.function4VendorId()) Function4VendorId{};
    decltype(object.function5ClassCode()) Function5ClassCode{};
    decltype(object.function5DeviceClass()) Function5DeviceClass{};
    decltype(object.function5DeviceId()) Function5DeviceId{};
    decltype(object.function5FunctionType()) Function5FunctionType{};
    decltype(object.function5RevisionId()) Function5RevisionId{};
    decltype(object.function5SubsystemId()) Function5SubsystemId{};
    decltype(object.function5SubsystemVendorId()) Function5SubsystemVendorId{};
    decltype(object.function5VendorId()) Function5VendorId{};
    decltype(object.function6ClassCode()) Function6ClassCode{};
    decltype(object.function6DeviceClass()) Function6DeviceClass{};
    decltype(object.function6DeviceId()) Function6DeviceId{};
    decltype(object.function6FunctionType()) Function6FunctionType{};
    decltype(object.function6RevisionId()) Function6RevisionId{};
    decltype(object.function6SubsystemId()) Function6SubsystemId{};
    decltype(object.function6SubsystemVendorId()) Function6SubsystemVendorId{};
    decltype(object.function6VendorId()) Function6VendorId{};
    decltype(object.function7ClassCode()) Function7ClassCode{};
    decltype(object.function7DeviceClass()) Function7DeviceClass{};
    decltype(object.function7DeviceId()) Function7DeviceId{};
    decltype(object.function7FunctionType()) Function7FunctionType{};
    decltype(object.function7RevisionId()) Function7RevisionId{};
    decltype(object.function7SubsystemId()) Function7SubsystemId{};
    decltype(object.function7SubsystemVendorId()) Function7SubsystemVendorId{};
    decltype(object.function7VendorId()) Function7VendorId{};
    decltype(object.manufacturer()) Manufacturer{};
    decltype(object.maxLanes()) MaxLanes{};
    decltype(object.lanesInUse()) LanesInUse{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(DeviceType, GenerationInUse, GenerationSupported, Function0ClassCode, Function0DeviceClass, Function0DeviceId, Function0FunctionType, Function0RevisionId, Function0SubsystemId, Function0SubsystemVendorId, Function0VendorId, Function1ClassCode, Function1DeviceClass, Function1DeviceId, Function1FunctionType, Function1RevisionId, Function1SubsystemId, Function1SubsystemVendorId, Function1VendorId, Function2ClassCode, Function2DeviceClass, Function2DeviceId, Function2FunctionType, Function2RevisionId, Function2SubsystemId, Function2SubsystemVendorId, Function2VendorId, Function3ClassCode, Function3DeviceClass, Function3DeviceId, Function3FunctionType, Function3RevisionId, Function3SubsystemId, Function3SubsystemVendorId, Function3VendorId, Function4ClassCode, Function4DeviceClass, Function4DeviceId, Function4FunctionType, Function4RevisionId, Function4SubsystemId, Function4SubsystemVendorId, Function4VendorId, Function5ClassCode, Function5DeviceClass, Function5DeviceId, Function5FunctionType, Function5RevisionId, Function5SubsystemId, Function5SubsystemVendorId, Function5VendorId, Function6ClassCode, Function6DeviceClass, Function6DeviceId, Function6FunctionType, Function6RevisionId, Function6SubsystemId, Function6SubsystemVendorId, Function6VendorId, Function7ClassCode, Function7DeviceClass, Function7DeviceId, Function7FunctionType, Function7RevisionId, Function7SubsystemId, Function7SubsystemVendorId, Function7VendorId, Manufacturer, MaxLanes, LanesInUse);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("DeviceType", DeviceType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("GenerationInUse", GenerationInUse));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("GenerationSupported", GenerationSupported));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0ClassCode", Function0ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0DeviceClass", Function0DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0DeviceId", Function0DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0FunctionType", Function0FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0RevisionId", Function0RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0SubsystemId", Function0SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0SubsystemVendorId", Function0SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function0VendorId", Function0VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1ClassCode", Function1ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1DeviceClass", Function1DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1DeviceId", Function1DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1FunctionType", Function1FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1RevisionId", Function1RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1SubsystemId", Function1SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1SubsystemVendorId", Function1SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function1VendorId", Function1VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2ClassCode", Function2ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2DeviceClass", Function2DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2DeviceId", Function2DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2FunctionType", Function2FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2RevisionId", Function2RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2SubsystemId", Function2SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2SubsystemVendorId", Function2SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function2VendorId", Function2VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3ClassCode", Function3ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3DeviceClass", Function3DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3DeviceId", Function3DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3FunctionType", Function3FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3RevisionId", Function3RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3SubsystemId", Function3SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3SubsystemVendorId", Function3SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function3VendorId", Function3VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4ClassCode", Function4ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4DeviceClass", Function4DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4DeviceId", Function4DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4FunctionType", Function4FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4RevisionId", Function4RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4SubsystemId", Function4SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4SubsystemVendorId", Function4SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function4VendorId", Function4VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5ClassCode", Function5ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5DeviceClass", Function5DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5DeviceId", Function5DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5FunctionType", Function5FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5RevisionId", Function5RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5SubsystemId", Function5SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5SubsystemVendorId", Function5SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function5VendorId", Function5VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6ClassCode", Function6ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6DeviceClass", Function6DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6DeviceId", Function6DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6FunctionType", Function6FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6RevisionId", Function6RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6SubsystemId", Function6SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6SubsystemVendorId", Function6SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function6VendorId", Function6VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7ClassCode", Function7ClassCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7DeviceClass", Function7DeviceClass));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7DeviceId", Function7DeviceId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7FunctionType", Function7FunctionType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7RevisionId", Function7RevisionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7SubsystemId", Function7SubsystemId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7SubsystemVendorId", Function7SubsystemVendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Function7VendorId", Function7VendorId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Manufacturer", Manufacturer));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxLanes", MaxLanes));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LanesInUse", LanesInUse));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.deviceType(DeviceType, skipSignals);
    object.generationInUse(GenerationInUse, skipSignals);
    object.generationSupported(GenerationSupported, skipSignals);
    object.function0ClassCode(Function0ClassCode, skipSignals);
    object.function0DeviceClass(Function0DeviceClass, skipSignals);
    object.function0DeviceId(Function0DeviceId, skipSignals);
    object.function0FunctionType(Function0FunctionType, skipSignals);
    object.function0RevisionId(Function0RevisionId, skipSignals);
    object.function0SubsystemId(Function0SubsystemId, skipSignals);
    object.function0SubsystemVendorId(Function0SubsystemVendorId, skipSignals);
    object.function0VendorId(Function0VendorId, skipSignals);
    object.function1ClassCode(Function1ClassCode, skipSignals);
    object.function1DeviceClass(Function1DeviceClass, skipSignals);
    object.function1DeviceId(Function1DeviceId, skipSignals);
    object.function1FunctionType(Function1FunctionType, skipSignals);
    object.function1RevisionId(Function1RevisionId, skipSignals);
    object.function1SubsystemId(Function1SubsystemId, skipSignals);
    object.function1SubsystemVendorId(Function1SubsystemVendorId, skipSignals);
    object.function1VendorId(Function1VendorId, skipSignals);
    object.function2ClassCode(Function2ClassCode, skipSignals);
    object.function2DeviceClass(Function2DeviceClass, skipSignals);
    object.function2DeviceId(Function2DeviceId, skipSignals);
    object.function2FunctionType(Function2FunctionType, skipSignals);
    object.function2RevisionId(Function2RevisionId, skipSignals);
    object.function2SubsystemId(Function2SubsystemId, skipSignals);
    object.function2SubsystemVendorId(Function2SubsystemVendorId, skipSignals);
    object.function2VendorId(Function2VendorId, skipSignals);
    object.function3ClassCode(Function3ClassCode, skipSignals);
    object.function3DeviceClass(Function3DeviceClass, skipSignals);
    object.function3DeviceId(Function3DeviceId, skipSignals);
    object.function3FunctionType(Function3FunctionType, skipSignals);
    object.function3RevisionId(Function3RevisionId, skipSignals);
    object.function3SubsystemId(Function3SubsystemId, skipSignals);
    object.function3SubsystemVendorId(Function3SubsystemVendorId, skipSignals);
    object.function3VendorId(Function3VendorId, skipSignals);
    object.function4ClassCode(Function4ClassCode, skipSignals);
    object.function4DeviceClass(Function4DeviceClass, skipSignals);
    object.function4DeviceId(Function4DeviceId, skipSignals);
    object.function4FunctionType(Function4FunctionType, skipSignals);
    object.function4RevisionId(Function4RevisionId, skipSignals);
    object.function4SubsystemId(Function4SubsystemId, skipSignals);
    object.function4SubsystemVendorId(Function4SubsystemVendorId, skipSignals);
    object.function4VendorId(Function4VendorId, skipSignals);
    object.function5ClassCode(Function5ClassCode, skipSignals);
    object.function5DeviceClass(Function5DeviceClass, skipSignals);
    object.function5DeviceId(Function5DeviceId, skipSignals);
    object.function5FunctionType(Function5FunctionType, skipSignals);
    object.function5RevisionId(Function5RevisionId, skipSignals);
    object.function5SubsystemId(Function5SubsystemId, skipSignals);
    object.function5SubsystemVendorId(Function5SubsystemVendorId, skipSignals);
    object.function5VendorId(Function5VendorId, skipSignals);
    object.function6ClassCode(Function6ClassCode, skipSignals);
    object.function6DeviceClass(Function6DeviceClass, skipSignals);
    object.function6DeviceId(Function6DeviceId, skipSignals);
    object.function6FunctionType(Function6FunctionType, skipSignals);
    object.function6RevisionId(Function6RevisionId, skipSignals);
    object.function6SubsystemId(Function6SubsystemId, skipSignals);
    object.function6SubsystemVendorId(Function6SubsystemVendorId, skipSignals);
    object.function6VendorId(Function6VendorId, skipSignals);
    object.function7ClassCode(Function7ClassCode, skipSignals);
    object.function7DeviceClass(Function7DeviceClass, skipSignals);
    object.function7DeviceId(Function7DeviceId, skipSignals);
    object.function7FunctionType(Function7FunctionType, skipSignals);
    object.function7RevisionId(Function7RevisionId, skipSignals);
    object.function7SubsystemId(Function7SubsystemId, skipSignals);
    object.function7SubsystemVendorId(Function7SubsystemVendorId, skipSignals);
    object.function7VendorId(Function7VendorId, skipSignals);
    object.manufacturer(Manufacturer, skipSignals);
    object.maxLanes(MaxLanes, skipSignals);
    object.lanesInUse(LanesInUse, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Generation", object.generation()));
        a(cereal::make_nvp("Lanes", object.lanes()));
        a(cereal::make_nvp("SlotType", object.slotType()));
        a(cereal::make_nvp("HotPluggable", object.hotPluggable()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot& object,
          const std::uint32_t version)
{
    decltype(object.generation()) Generation{};
    decltype(object.lanes()) Lanes{};
    decltype(object.slotType()) SlotType{};
    decltype(object.hotPluggable()) HotPluggable{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Generation, Lanes, SlotType, HotPluggable);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Generation", Generation));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Lanes", Lanes));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SlotType", SlotType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("HotPluggable", HotPluggable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.generation(Generation, skipSignals);
    object.lanes(Lanes, skipSignals);
    object.slotType(SlotType, skipSignals);
    object.hotPluggable(HotPluggable, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Panel& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ModuleManufacturerID", object.moduleManufacturerID()));
        a(cereal::make_nvp("ModuleProductID", object.moduleProductID()));
        a(cereal::make_nvp("SubsystemVendorID", object.subsystemVendorID()));
        a(cereal::make_nvp("SubsystemDeviceID", object.subsystemDeviceID()));
        a(cereal::make_nvp("VolatileRegionSizeLimitInKiB", object.volatileRegionSizeLimitInKiB()));
        a(cereal::make_nvp("PmRegionSizeLimitInKiB", object.pmRegionSizeLimitInKiB()));
        a(cereal::make_nvp("VolatileSizeInKiB", object.volatileSizeInKiB()));
        a(cereal::make_nvp("PmSizeInKiB", object.pmSizeInKiB()));
        a(cereal::make_nvp("CacheSizeInKiB", object.cacheSizeInKiB()));
        a(cereal::make_nvp("VolatileRegionMaxSizeInKiB", object.volatileRegionMaxSizeInKiB()));
        a(cereal::make_nvp("PmRegionMaxSizeInKiB", object.pmRegionMaxSizeInKiB()));
        a(cereal::make_nvp("AllocationIncrementInKiB", object.allocationIncrementInKiB()));
        a(cereal::make_nvp("AllocationAlignmentInKiB", object.allocationAlignmentInKiB()));
        a(cereal::make_nvp("VolatileRegionNumberLimit", object.volatileRegionNumberLimit()));
        a(cereal::make_nvp("PmRegionNumberLimit", object.pmRegionNumberLimit()));
        a(cereal::make_nvp("SpareDeviceCount", object.spareDeviceCount()));
        a(cereal::make_nvp("IsSpareDeviceInUse", object.isSpareDeviceInUse()));
        a(cereal::make_nvp("IsRankSpareEnabled", object.isRankSpareEnabled()));
        a(cereal::make_nvp("MaxAveragePowerLimitmW", object.maxAveragePowerLimitmW()));
        a(cereal::make_nvp("CurrentSecurityState", object.currentSecurityState()));
        a(cereal::make_nvp("ConfigurationLocked", object.configurationLocked()));
        a(cereal::make_nvp("AllowedMemoryModes", object.allowedMemoryModes()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory& object,
          const std::uint32_t version)
{
    decltype(object.moduleManufacturerID()) ModuleManufacturerID{};
    decltype(object.moduleProductID()) ModuleProductID{};
    decltype(object.subsystemVendorID()) SubsystemVendorID{};
    decltype(object.subsystemDeviceID()) SubsystemDeviceID{};
    decltype(object.volatileRegionSizeLimitInKiB()) VolatileRegionSizeLimitInKiB{};
    decltype(object.pmRegionSizeLimitInKiB()) PmRegionSizeLimitInKiB{};
    decltype(object.volatileSizeInKiB()) VolatileSizeInKiB{};
    decltype(object.pmSizeInKiB()) PmSizeInKiB{};
    decltype(object.cacheSizeInKiB()) CacheSizeInKiB{};
    decltype(object.volatileRegionMaxSizeInKiB()) VolatileRegionMaxSizeInKiB{};
    decltype(object.pmRegionMaxSizeInKiB()) PmRegionMaxSizeInKiB{};
    decltype(object.allocationIncrementInKiB()) AllocationIncrementInKiB{};
    decltype(object.allocationAlignmentInKiB()) AllocationAlignmentInKiB{};
    decltype(object.volatileRegionNumberLimit()) VolatileRegionNumberLimit{};
    decltype(object.pmRegionNumberLimit()) PmRegionNumberLimit{};
    decltype(object.spareDeviceCount()) SpareDeviceCount{};
    decltype(object.isSpareDeviceInUse()) IsSpareDeviceInUse{};
    decltype(object.isRankSpareEnabled()) IsRankSpareEnabled{};
    decltype(object.maxAveragePowerLimitmW()) MaxAveragePowerLimitmW{};
    decltype(object.currentSecurityState()) CurrentSecurityState{};
    decltype(object.configurationLocked()) ConfigurationLocked{};
    decltype(object.allowedMemoryModes()) AllowedMemoryModes{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ModuleManufacturerID, ModuleProductID, SubsystemVendorID, SubsystemDeviceID, VolatileRegionSizeLimitInKiB, PmRegionSizeLimitInKiB, VolatileSizeInKiB, PmSizeInKiB, CacheSizeInKiB, VolatileRegionMaxSizeInKiB, PmRegionMaxSizeInKiB, AllocationIncrementInKiB, AllocationAlignmentInKiB, VolatileRegionNumberLimit, PmRegionNumberLimit, SpareDeviceCount, IsSpareDeviceInUse, IsRankSpareEnabled, MaxAveragePowerLimitmW, CurrentSecurityState, ConfigurationLocked, AllowedMemoryModes);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ModuleManufacturerID", ModuleManufacturerID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ModuleProductID", ModuleProductID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SubsystemVendorID", SubsystemVendorID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SubsystemDeviceID", SubsystemDeviceID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("VolatileRegionSizeLimitInKiB", VolatileRegionSizeLimitInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PmRegionSizeLimitInKiB", PmRegionSizeLimitInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("VolatileSizeInKiB", VolatileSizeInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PmSizeInKiB", PmSizeInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CacheSizeInKiB", CacheSizeInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("VolatileRegionMaxSizeInKiB", VolatileRegionMaxSizeInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PmRegionMaxSizeInKiB", PmRegionMaxSizeInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AllocationIncrementInKiB", AllocationIncrementInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AllocationAlignmentInKiB", AllocationAlignmentInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("VolatileRegionNumberLimit", VolatileRegionNumberLimit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PmRegionNumberLimit", PmRegionNumberLimit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SpareDeviceCount", SpareDeviceCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("IsSpareDeviceInUse", IsSpareDeviceInUse));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("IsRankSpareEnabled", IsRankSpareEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxAveragePowerLimitmW", MaxAveragePowerLimitmW));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentSecurityState", CurrentSecurityState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ConfigurationLocked", ConfigurationLocked));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AllowedMemoryModes", AllowedMemoryModes));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.moduleManufacturerID(ModuleManufacturerID, skipSignals);
    object.moduleProductID(ModuleProductID, skipSignals);
    object.subsystemVendorID(SubsystemVendorID, skipSignals);
    object.subsystemDeviceID(SubsystemDeviceID, skipSignals);
    object.volatileRegionSizeLimitInKiB(VolatileRegionSizeLimitInKiB, skipSignals);
    object.pmRegionSizeLimitInKiB(PmRegionSizeLimitInKiB, skipSignals);
    object.volatileSizeInKiB(VolatileSizeInKiB, skipSignals);
    object.pmSizeInKiB(PmSizeInKiB, skipSignals);
    object.cacheSizeInKiB(CacheSizeInKiB, skipSignals);
    object.volatileRegionMaxSizeInKiB(VolatileRegionMaxSizeInKiB, skipSignals);
    object.pmRegionMaxSizeInKiB(PmRegionMaxSizeInKiB, skipSignals);
    object.allocationIncrementInKiB(AllocationIncrementInKiB, skipSignals);
    object.allocationAlignmentInKiB(AllocationAlignmentInKiB, skipSignals);
    object.volatileRegionNumberLimit(VolatileRegionNumberLimit, skipSignals);
    object.pmRegionNumberLimit(PmRegionNumberLimit, skipSignals);
    object.spareDeviceCount(SpareDeviceCount, skipSignals);
    object.isSpareDeviceInUse(IsSpareDeviceInUse, skipSignals);
    object.isRankSpareEnabled(IsRankSpareEnabled, skipSignals);
    object.maxAveragePowerLimitmW(MaxAveragePowerLimitmW, skipSignals);
    object.currentSecurityState(CurrentSecurityState, skipSignals);
    object.configurationLocked(ConfigurationLocked, skipSignals);
    object.allowedMemoryModes(AllowedMemoryModes, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::PowerSupply& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("NominalVoltage", object.nominalVoltage()));
        a(cereal::make_nvp("NominalCurrent", object.nominalCurrent()));
        a(cereal::make_nvp("NominalRPM", object.nominalRPM()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Rotor& object,
          const std::uint32_t version)
{
    decltype(object.nominalVoltage()) NominalVoltage{};
    decltype(object.nominalCurrent()) NominalCurrent{};
    decltype(object.nominalRPM()) NominalRPM{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(NominalVoltage, NominalCurrent, NominalRPM);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("NominalVoltage", NominalVoltage));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("NominalCurrent", NominalCurrent));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("NominalRPM", NominalRPM));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.nominalVoltage(NominalVoltage, skipSignals);
    object.nominalCurrent(NominalCurrent, skipSignals);
    object.nominalRPM(NominalRPM, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Storage& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::StorageController& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::System& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::System& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Tpm& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::server::Vrm& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::IOBoard& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::Board::server::Motherboard& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("BaseSpeed", object.baseSpeed()));
        a(cereal::make_nvp("BaseSpeedPrioritySettings", object.baseSpeedPrioritySettings()));
        a(cereal::make_nvp("MaxJunctionTemperature", object.maxJunctionTemperature()));
        a(cereal::make_nvp("MaxSpeed", object.maxSpeed()));
        a(cereal::make_nvp("PowerLimit", object.powerLimit()));
        a(cereal::make_nvp("AvailableCoreCount", object.availableCoreCount()));
        a(cereal::make_nvp("TurboProfile", object.turboProfile()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::Cpu::server::OperatingConfig& object,
          const std::uint32_t version)
{
    decltype(object.baseSpeed()) BaseSpeed{};
    decltype(object.baseSpeedPrioritySettings()) BaseSpeedPrioritySettings{};
    decltype(object.maxJunctionTemperature()) MaxJunctionTemperature{};
    decltype(object.maxSpeed()) MaxSpeed{};
    decltype(object.powerLimit()) PowerLimit{};
    decltype(object.availableCoreCount()) AvailableCoreCount{};
    decltype(object.turboProfile()) TurboProfile{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(BaseSpeed, BaseSpeedPrioritySettings, MaxJunctionTemperature, MaxSpeed, PowerLimit, AvailableCoreCount, TurboProfile);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("BaseSpeed", BaseSpeed));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BaseSpeedPrioritySettings", BaseSpeedPrioritySettings));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxJunctionTemperature", MaxJunctionTemperature));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxSpeed", MaxSpeed));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PowerLimit", PowerLimit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AvailableCoreCount", AvailableCoreCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("TurboProfile", TurboProfile));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.baseSpeed(BaseSpeed, skipSignals);
    object.baseSpeedPrioritySettings(BaseSpeedPrioritySettings, skipSignals);
    object.maxJunctionTemperature(MaxJunctionTemperature, skipSignals);
    object.maxSpeed(MaxSpeed, skipSignals);
    object.powerLimit(PowerLimit, skipSignals);
    object.availableCoreCount(AvailableCoreCount, skipSignals);
    object.turboProfile(TurboProfile, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Socket", object.socket()));
        a(cereal::make_nvp("MemoryController", object.memoryController()));
        a(cereal::make_nvp("Channel", object.channel()));
        a(cereal::make_nvp("Slot", object.slot()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::Dimm::server::MemoryLocation& object,
          const std::uint32_t version)
{
    decltype(object.socket()) Socket{};
    decltype(object.memoryController()) MemoryController{};
    decltype(object.channel()) Channel{};
    decltype(object.slot()) Slot{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Socket, MemoryController, Channel, Slot);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Socket", Socket));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryController", MemoryController));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Channel", Channel));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Slot", Slot));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.socket(Socket, skipSignals);
    object.memoryController(MemoryController, skipSignals);
    object.channel(Channel, skipSignals);
    object.slot(Slot, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PartitionId", object.partitionId()));
        a(cereal::make_nvp("SizeInKiB", object.sizeInKiB()));
        a(cereal::make_nvp("MemoryClassification", object.memoryClassification()));
        a(cereal::make_nvp("OffsetInKiB", object.offsetInKiB()));
        a(cereal::make_nvp("PassphraseState", object.passphraseState()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::Partition& object,
          const std::uint32_t version)
{
    decltype(object.partitionId()) PartitionId{};
    decltype(object.sizeInKiB()) SizeInKiB{};
    decltype(object.memoryClassification()) MemoryClassification{};
    decltype(object.offsetInKiB()) OffsetInKiB{};
    decltype(object.passphraseState()) PassphraseState{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PartitionId, SizeInKiB, MemoryClassification, OffsetInKiB, PassphraseState);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PartitionId", PartitionId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SizeInKiB", SizeInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MemoryClassification", MemoryClassification));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("OffsetInKiB", OffsetInKiB));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PassphraseState", PassphraseState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.partitionId(PartitionId, skipSignals);
    object.sizeInKiB(SizeInKiB, skipSignals);
    object.memoryClassification(MemoryClassification, skipSignals);
    object.offsetInKiB(OffsetInKiB, skipSignals);
    object.passphraseState(PassphraseState, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PolicyEnabled", object.policyEnabled()));
        a(cereal::make_nvp("MaxTDPmW", object.maxTDPmW()));
        a(cereal::make_nvp("PeakPowerBudgetmW", object.peakPowerBudgetmW()));
        a(cereal::make_nvp("AveragePowerBudgetmW", object.averagePowerBudgetmW()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::PowerManagementPolicy& object,
          const std::uint32_t version)
{
    decltype(object.policyEnabled()) PolicyEnabled{};
    decltype(object.maxTDPmW()) MaxTDPmW{};
    decltype(object.peakPowerBudgetmW()) PeakPowerBudgetmW{};
    decltype(object.averagePowerBudgetmW()) AveragePowerBudgetmW{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PolicyEnabled, MaxTDPmW, PeakPowerBudgetmW, AveragePowerBudgetmW);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PolicyEnabled", PolicyEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxTDPmW", MaxTDPmW));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PeakPowerBudgetmW", PeakPowerBudgetmW));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AveragePowerBudgetmW", AveragePowerBudgetmW));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.policyEnabled(PolicyEnabled, skipSignals);
    object.maxTDPmW(MaxTDPmW, skipSignals);
    object.peakPowerBudgetmW(PeakPowerBudgetmW, skipSignals);
    object.averagePowerBudgetmW(AveragePowerBudgetmW, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MaxPassphraseCount", object.maxPassphraseCount()));
        a(cereal::make_nvp("PassphraseCapable", object.passphraseCapable()));
        a(cereal::make_nvp("ConfigurationLockCapable", object.configurationLockCapable()));
        a(cereal::make_nvp("DataLockCapable", object.dataLockCapable()));
        a(cereal::make_nvp("PassphraseLockLimit", object.passphraseLockLimit()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Item::PersistentMemory::server::SecurityCapabilities& object,
          const std::uint32_t version)
{
    decltype(object.maxPassphraseCount()) MaxPassphraseCount{};
    decltype(object.passphraseCapable()) PassphraseCapable{};
    decltype(object.configurationLockCapable()) ConfigurationLockCapable{};
    decltype(object.dataLockCapable()) DataLockCapable{};
    decltype(object.passphraseLockLimit()) PassphraseLockLimit{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MaxPassphraseCount, PassphraseCapable, ConfigurationLockCapable, DataLockCapable, PassphraseLockLimit);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MaxPassphraseCount", MaxPassphraseCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PassphraseCapable", PassphraseCapable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ConfigurationLockCapable", ConfigurationLockCapable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DataLockCapable", DataLockCapable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PassphraseLockLimit", PassphraseLockLimit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.maxPassphraseCount(MaxPassphraseCount, skipSignals);
    object.passphraseCapable(PassphraseCapable, skipSignals);
    object.configurationLockCapable(ConfigurationLockCapable, skipSignals);
    object.dataLockCapable(DataLockCapable, skipSignals);
    object.passphraseLockLimit(PassphraseLockLimit, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("EntityType", object.entityType()));
        a(cereal::make_nvp("EntityInstanceNumber", object.entityInstanceNumber()));
        a(cereal::make_nvp("ContainerID", object.containerID()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::Entity& object,
          const std::uint32_t version)
{
    decltype(object.entityType()) EntityType{};
    decltype(object.entityInstanceNumber()) EntityInstanceNumber{};
    decltype(object.containerID()) ContainerID{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(EntityType, EntityInstanceNumber, ContainerID);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("EntityType", EntityType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EntityInstanceNumber", EntityInstanceNumber));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ContainerID", ContainerID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.entityType(EntityType, skipSignals);
    object.entityInstanceNumber(EntityInstanceNumber, skipSignals);
    object.containerID(ContainerID, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ChassisType", object.chassisType()));
        a(cereal::make_nvp("Model", object.model()));
        a(cereal::make_nvp("PN", object.pn()));
        a(cereal::make_nvp("SN", object.sn()));
        a(cereal::make_nvp("Manufacturer", object.manufacturer()));
        a(cereal::make_nvp("ManufacturerDate", object.manufacturerDate()));
        a(cereal::make_nvp("Vendor", object.vendor()));
        a(cereal::make_nvp("Name", object.name()));
        a(cereal::make_nvp("SKU", object.sku()));
        a(cereal::make_nvp("Version", object.version()));
        a(cereal::make_nvp("AssetTag", object.assetTag()));
        a(cereal::make_nvp("Description", object.description()));
        a(cereal::make_nvp("ECLevel", object.ecLevel()));
        a(cereal::make_nvp("Other", object.other()));
        a(cereal::make_nvp("IANA", object.iana()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Inventory::Source::PLDM::server::FRU& object,
          const std::uint32_t version)
{
    decltype(object.chassisType()) ChassisType{};
    decltype(object.model()) Model{};
    decltype(object.pn()) PN{};
    decltype(object.sn()) SN{};
    decltype(object.manufacturer()) Manufacturer{};
    decltype(object.manufacturerDate()) ManufacturerDate{};
    decltype(object.vendor()) Vendor{};
    decltype(object.name()) Name{};
    decltype(object.sku()) SKU{};
    decltype(object.version()) Version{};
    decltype(object.assetTag()) AssetTag{};
    decltype(object.description()) Description{};
    decltype(object.ecLevel()) ECLevel{};
    decltype(object.other()) Other{};
    decltype(object.iana()) IANA{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ChassisType, Model, PN, SN, Manufacturer, ManufacturerDate, Vendor, Name, SKU, Version, AssetTag, Description, ECLevel, Other, IANA);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ChassisType", ChassisType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Model", Model));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PN", PN));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SN", SN));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Manufacturer", Manufacturer));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ManufacturerDate", ManufacturerDate));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Vendor", Vendor));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Name", Name));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SKU", SKU));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Version", Version));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AssetTag", AssetTag));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Description", Description));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ECLevel", ECLevel));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Other", Other));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("IANA", IANA));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.chassisType(ChassisType, skipSignals);
    object.model(Model, skipSignals);
    object.pn(PN, skipSignals);
    object.sn(SN, skipSignals);
    object.manufacturer(Manufacturer, skipSignals);
    object.manufacturerDate(ManufacturerDate, skipSignals);
    object.vendor(Vendor, skipSignals);
    object.name(Name, skipSignals);
    object.sku(SKU, skipSignals);
    object.version(Version, skipSignals);
    object.assetTag(AssetTag, skipSignals);
    object.description(Description, skipSignals);
    object.ecLevel(ECLevel, skipSignals);
    object.other(Other, skipSignals);
    object.iana(IANA, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Ipmi::server::SOL& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Progress", object.progress()));
        a(cereal::make_nvp("Enable", object.enable()));
        a(cereal::make_nvp("ForceEncryption", object.forceEncryption()));
        a(cereal::make_nvp("ForceAuthentication", object.forceAuthentication()));
        a(cereal::make_nvp("Privilege", object.privilege()));
        a(cereal::make_nvp("AccumulateIntervalMS", object.accumulateIntervalMS()));
        a(cereal::make_nvp("Threshold", object.threshold()));
        a(cereal::make_nvp("RetryCount", object.retryCount()));
        a(cereal::make_nvp("RetryIntervalMS", object.retryIntervalMS()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Ipmi::server::SOL& object,
          const std::uint32_t version)
{
    decltype(object.progress()) Progress{};
    decltype(object.enable()) Enable{};
    decltype(object.forceEncryption()) ForceEncryption{};
    decltype(object.forceAuthentication()) ForceAuthentication{};
    decltype(object.privilege()) Privilege{};
    decltype(object.accumulateIntervalMS()) AccumulateIntervalMS{};
    decltype(object.threshold()) Threshold{};
    decltype(object.retryCount()) RetryCount{};
    decltype(object.retryIntervalMS()) RetryIntervalMS{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Progress, Enable, ForceEncryption, ForceAuthentication, Privilege, AccumulateIntervalMS, Threshold, RetryCount, RetryIntervalMS);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Progress", Progress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Enable", Enable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ForceEncryption", ForceEncryption));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ForceAuthentication", ForceAuthentication));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Privilege", Privilege));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AccumulateIntervalMS", AccumulateIntervalMS));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Threshold", Threshold));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RetryCount", RetryCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RetryIntervalMS", RetryIntervalMS));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.progress(Progress, skipSignals);
    object.enable(Enable, skipSignals);
    object.forceEncryption(ForceEncryption, skipSignals);
    object.forceAuthentication(ForceAuthentication, skipSignals);
    object.privilege(Privilege, skipSignals);
    object.accumulateIntervalMS(AccumulateIntervalMS, skipSignals);
    object.threshold(Threshold, skipSignals);
    object.retryCount(RetryCount, skipSignals);
    object.retryIntervalMS(RetryIntervalMS, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SessionHandle", object.sessionHandle()));
        a(cereal::make_nvp("ChannelNum", object.channelNum()));
        a(cereal::make_nvp("CurrentPrivilege", object.currentPrivilege()));
        a(cereal::make_nvp("RemoteIPAddr", object.remoteIPAddr()));
        a(cereal::make_nvp("RemotePort", object.remotePort()));
        a(cereal::make_nvp("RemoteMACAddress", object.remoteMACAddress()));
        a(cereal::make_nvp("UserID", object.userID()));
        a(cereal::make_nvp("State", object.state()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Ipmi::server::SessionInfo& object,
          const std::uint32_t version)
{
    decltype(object.sessionHandle()) SessionHandle{};
    decltype(object.channelNum()) ChannelNum{};
    decltype(object.currentPrivilege()) CurrentPrivilege{};
    decltype(object.remoteIPAddr()) RemoteIPAddr{};
    decltype(object.remotePort()) RemotePort{};
    decltype(object.remoteMACAddress()) RemoteMACAddress{};
    decltype(object.userID()) UserID{};
    decltype(object.state()) State{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SessionHandle, ChannelNum, CurrentPrivilege, RemoteIPAddr, RemotePort, RemoteMACAddress, UserID, State);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SessionHandle", SessionHandle));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ChannelNum", ChannelNum));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentPrivilege", CurrentPrivilege));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RemoteIPAddr", RemoteIPAddr));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RemotePort", RemotePort));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RemoteMACAddress", RemoteMACAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UserID", UserID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("State", State));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.sessionHandle(SessionHandle, skipSignals);
    object.channelNum(ChannelNum, skipSignals);
    object.currentPrivilege(CurrentPrivilege, skipSignals);
    object.remoteIPAddr(RemoteIPAddr, skipSignals);
    object.remotePort(RemotePort, skipSignals);
    object.remoteMACAddress(RemoteMACAddress, skipSignals);
    object.userID(UserID, skipSignals);
    object.state(State, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Led::server::Group& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Asserted", object.asserted()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Led::server::Group& object,
          const std::uint32_t version)
{
    decltype(object.asserted()) Asserted{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Asserted);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Asserted", Asserted));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.asserted(Asserted, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Led::server::Physical& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("State", object.state()));
        a(cereal::make_nvp("DutyOn", object.dutyOn()));
        a(cereal::make_nvp("Color", object.color()));
        a(cereal::make_nvp("Period", object.period()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Led::server::Physical& object,
          const std::uint32_t version)
{
    decltype(object.state()) State{};
    decltype(object.dutyOn()) DutyOn{};
    decltype(object.color()) Color{};
    decltype(object.period()) Period{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(State, DutyOn, Color, Period);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("State", State));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DutyOn", DutyOn));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Color", Color));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Period", Period));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.state(State, skipSignals);
    object.dutyOn(DutyOn, skipSignals);
    object.color(Color, skipSignals);
    object.period(Period, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Logging::server::ErrorBlocksTransition& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Logging::server::Event& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Timestamp", object.timestamp()));
        a(cereal::make_nvp("Message", object.message()));
        a(cereal::make_nvp("AdditionalData", object.additionalData()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Logging::server::Event& object,
          const std::uint32_t version)
{
    decltype(object.timestamp()) Timestamp{};
    decltype(object.message()) Message{};
    decltype(object.additionalData()) AdditionalData{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Timestamp, Message, AdditionalData);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Timestamp", Timestamp));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Message", Message));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AdditionalData", AdditionalData));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.timestamp(Timestamp, skipSignals);
    object.message(Message, skipSignals);
    object.additionalData(AdditionalData, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Logging::server::Settings& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("QuiesceOnHwError", object.quiesceOnHwError()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Logging::server::Settings& object,
          const std::uint32_t version)
{
    decltype(object.quiesceOnHwError()) QuiesceOnHwError{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(QuiesceOnHwError);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("QuiesceOnHwError", QuiesceOnHwError));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.quiesceOnHwError(QuiesceOnHwError, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("From", object.from()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::server::Mail& object,
          const std::uint32_t version)
{
    decltype(object.from()) From{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(From);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("From", From));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.from(From, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Mailto", object.mailto()));
        a(cereal::make_nvp("Level", object.level()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Logging::Syslog::Destination::Mail::server::Entry& object,
          const std::uint32_t version)
{
    decltype(object.mailto()) Mailto{};
    decltype(object.level()) Level{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Mailto, Level);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Mailto", Mailto));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Level", Level));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.mailto(Mailto, skipSignals);
    object.level(Level, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("NetworkId", object.networkId()));
        a(cereal::make_nvp("EID", object.eid()));
        a(cereal::make_nvp("SupportedMessageTypes", object.supportedMessageTypes()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::MCTP::server::Endpoint& object,
          const std::uint32_t version)
{
    decltype(object.networkId()) NetworkId{};
    decltype(object.eid()) EID{};
    decltype(object.supportedMessageTypes()) SupportedMessageTypes{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(NetworkId, EID, SupportedMessageTypes);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("NetworkId", NetworkId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EID", EID));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SupportedMessageTypes", SupportedMessageTypes));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.networkId(NetworkId, skipSignals);
    object.eid(EID, skipSignals);
    object.supportedMessageTypes(SupportedMessageTypes, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("IsLoggingLimitReached", object.isLoggingLimitReached()));
        a(cereal::make_nvp("CeCount", object.ceCount()));
        a(cereal::make_nvp("UeCount", object.ueCount()));
        a(cereal::make_nvp("State", object.state()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC& object,
          const std::uint32_t version)
{
    decltype(object.isLoggingLimitReached()) IsLoggingLimitReached{};
    decltype(object.ceCount()) CeCount{};
    decltype(object.ueCount()) UeCount{};
    decltype(object.state()) State{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(IsLoggingLimitReached, CeCount, UeCount, State);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("IsLoggingLimitReached", IsLoggingLimitReached));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CeCount", CeCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UeCount", UeCount));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("State", State));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.isLoggingLimitReached(IsLoggingLimitReached, skipSignals);
    object.ceCount(CeCount, skipSignals);
    object.ueCount(UeCount, skipSignals);
    object.state(State, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::Client& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Address", object.address()));
        a(cereal::make_nvp("Port", object.port()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::Client& object,
          const std::uint32_t version)
{
    decltype(object.address()) Address{};
    decltype(object.port()) Port{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Address, Port);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Address", Address));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Port", Port));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.address(Address, skipSignals);
    object.port(Port, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("DNSEnabled", object.dnsEnabled()));
        a(cereal::make_nvp("NTPEnabled", object.ntpEnabled()));
        a(cereal::make_nvp("HostNameEnabled", object.hostNameEnabled()));
        a(cereal::make_nvp("DomainEnabled", object.domainEnabled()));
        a(cereal::make_nvp("SendHostNameEnabled", object.sendHostNameEnabled()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::DHCPConfiguration& object,
          const std::uint32_t version)
{
    decltype(object.dnsEnabled()) DNSEnabled{};
    decltype(object.ntpEnabled()) NTPEnabled{};
    decltype(object.hostNameEnabled()) HostNameEnabled{};
    decltype(object.domainEnabled()) DomainEnabled{};
    decltype(object.sendHostNameEnabled()) SendHostNameEnabled{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(DNSEnabled, NTPEnabled, HostNameEnabled, DomainEnabled, SendHostNameEnabled);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("DNSEnabled", DNSEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("NTPEnabled", NTPEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("HostNameEnabled", HostNameEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DomainEnabled", DomainEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SendHostNameEnabled", SendHostNameEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.dnsEnabled(DNSEnabled, skipSignals);
    object.ntpEnabled(NTPEnabled, skipSignals);
    object.hostNameEnabled(HostNameEnabled, skipSignals);
    object.domainEnabled(DomainEnabled, skipSignals);
    object.sendHostNameEnabled(SendHostNameEnabled, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("InterfaceName", object.interfaceName()));
        a(cereal::make_nvp("Speed", object.speed()));
        a(cereal::make_nvp("AutoNeg", object.autoNeg()));
        a(cereal::make_nvp("MTU", object.mtu()));
        a(cereal::make_nvp("DomainName", object.domainName()));
        a(cereal::make_nvp("DHCPEnabled", object.dhcpEnabled()));
        a(cereal::make_nvp("DHCP4", object.dhcp4()));
        a(cereal::make_nvp("DHCP6", object.dhcp6()));
        a(cereal::make_nvp("Nameservers", object.nameservers()));
        a(cereal::make_nvp("StaticNameServers", object.staticNameServers()));
        a(cereal::make_nvp("NTPServers", object.ntpServers()));
        a(cereal::make_nvp("StaticNTPServers", object.staticNTPServers()));
        a(cereal::make_nvp("LinkLocalAutoConf", object.linkLocalAutoConf()));
        a(cereal::make_nvp("IPv6AcceptRA", object.ipv6AcceptRA()));
        a(cereal::make_nvp("NICEnabled", object.nicEnabled()));
        a(cereal::make_nvp("LinkUp", object.linkUp()));
        a(cereal::make_nvp("DefaultGateway", object.defaultGateway()));
        a(cereal::make_nvp("DefaultGateway6", object.defaultGateway6()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface& object,
          const std::uint32_t version)
{
    decltype(object.interfaceName()) InterfaceName{};
    decltype(object.speed()) Speed{};
    decltype(object.autoNeg()) AutoNeg{};
    decltype(object.mtu()) MTU{};
    decltype(object.domainName()) DomainName{};
    decltype(object.dhcpEnabled()) DHCPEnabled{};
    decltype(object.dhcp4()) DHCP4{};
    decltype(object.dhcp6()) DHCP6{};
    decltype(object.nameservers()) Nameservers{};
    decltype(object.staticNameServers()) StaticNameServers{};
    decltype(object.ntpServers()) NTPServers{};
    decltype(object.staticNTPServers()) StaticNTPServers{};
    decltype(object.linkLocalAutoConf()) LinkLocalAutoConf{};
    decltype(object.ipv6AcceptRA()) IPv6AcceptRA{};
    decltype(object.nicEnabled()) NICEnabled{};
    decltype(object.linkUp()) LinkUp{};
    decltype(object.defaultGateway()) DefaultGateway{};
    decltype(object.defaultGateway6()) DefaultGateway6{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(InterfaceName, Speed, AutoNeg, MTU, DomainName, DHCPEnabled, DHCP4, DHCP6, Nameservers, StaticNameServers, NTPServers, StaticNTPServers, LinkLocalAutoConf, IPv6AcceptRA, NICEnabled, LinkUp, DefaultGateway, DefaultGateway6);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("InterfaceName", InterfaceName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Speed", Speed));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AutoNeg", AutoNeg));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MTU", MTU));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DomainName", DomainName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DHCPEnabled", DHCPEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DHCP4", DHCP4));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DHCP6", DHCP6));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Nameservers", Nameservers));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("StaticNameServers", StaticNameServers));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("NTPServers", NTPServers));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("StaticNTPServers", StaticNTPServers));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LinkLocalAutoConf", LinkLocalAutoConf));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("IPv6AcceptRA", IPv6AcceptRA));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("NICEnabled", NICEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LinkUp", LinkUp));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DefaultGateway", DefaultGateway));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DefaultGateway6", DefaultGateway6));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.interfaceName(InterfaceName, skipSignals);
    object.speed(Speed, skipSignals);
    object.autoNeg(AutoNeg, skipSignals);
    object.mtu(MTU, skipSignals);
    object.domainName(DomainName, skipSignals);
    object.dhcpEnabled(DHCPEnabled, skipSignals);
    object.dhcp4(DHCP4, skipSignals);
    object.dhcp6(DHCP6, skipSignals);
    object.nameservers(Nameservers, skipSignals);
    object.staticNameServers(StaticNameServers, skipSignals);
    object.ntpServers(NTPServers, skipSignals);
    object.staticNTPServers(StaticNTPServers, skipSignals);
    object.linkLocalAutoConf(LinkLocalAutoConf, skipSignals);
    object.ipv6AcceptRA(IPv6AcceptRA, skipSignals);
    object.nicEnabled(NICEnabled, skipSignals);
    object.linkUp(LinkUp, skipSignals);
    object.defaultGateway(DefaultGateway, skipSignals);
    object.defaultGateway6(DefaultGateway6, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::IP& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Address", object.address()));
        a(cereal::make_nvp("PrefixLength", object.prefixLength()));
        a(cereal::make_nvp("Origin", object.origin()));
        a(cereal::make_nvp("Gateway", object.gateway()));
        a(cereal::make_nvp("Type", object.type()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::IP& object,
          const std::uint32_t version)
{
    decltype(object.address()) Address{};
    decltype(object.prefixLength()) PrefixLength{};
    decltype(object.origin()) Origin{};
    decltype(object.gateway()) Gateway{};
    decltype(object.type()) Type{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Address, PrefixLength, Origin, Gateway, Type);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Address", Address));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PrefixLength", PrefixLength));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Origin", Origin));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Gateway", Gateway));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Type", Type));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.address(Address, skipSignals);
    object.prefixLength(PrefixLength, skipSignals);
    object.origin(Origin, skipSignals);
    object.gateway(Gateway, skipSignals);
    object.type(Type, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::MACAddress& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MACAddress", object.macAddress()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::MACAddress& object,
          const std::uint32_t version)
{
    decltype(object.macAddress()) MACAddress{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MACAddress);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MACAddress", MACAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.macAddress(MACAddress, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::Neighbor& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("IPAddress", object.ipAddress()));
        a(cereal::make_nvp("MACAddress", object.macAddress()));
        a(cereal::make_nvp("State", object.state()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::Neighbor& object,
          const std::uint32_t version)
{
    decltype(object.ipAddress()) IPAddress{};
    decltype(object.macAddress()) MACAddress{};
    decltype(object.state()) State{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(IPAddress, MACAddress, State);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("IPAddress", IPAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MACAddress", MACAddress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("State", State));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.ipAddress(IPAddress, skipSignals);
    object.macAddress(MACAddress, skipSignals);
    object.state(State, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("HostName", object.hostName()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::SystemConfiguration& object,
          const std::uint32_t version)
{
    decltype(object.hostName()) HostName{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(HostName);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("HostName", HostName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.hostName(HostName, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::server::VLAN& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("InterfaceName", object.interfaceName()));
        a(cereal::make_nvp("Id", object.id()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::server::VLAN& object,
          const std::uint32_t version)
{
    decltype(object.interfaceName()) InterfaceName{};
    decltype(object.id()) Id{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(InterfaceName, Id);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("InterfaceName", InterfaceName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Id", Id));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.interfaceName(InterfaceName, skipSignals);
    object.id(Id, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("InterfaceName", object.interfaceName()));
        a(cereal::make_nvp("BondedInterfaces", object.bondedInterfaces()));
        a(cereal::make_nvp("Mode", object.mode()));
        a(cereal::make_nvp("TransmitHashPolicy", object.transmitHashPolicy()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond& object,
          const std::uint32_t version)
{
    decltype(object.interfaceName()) InterfaceName{};
    decltype(object.bondedInterfaces()) BondedInterfaces{};
    decltype(object.mode()) Mode{};
    decltype(object.transmitHashPolicy()) TransmitHashPolicy{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(InterfaceName, BondedInterfaces, Mode, TransmitHashPolicy);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("InterfaceName", InterfaceName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BondedInterfaces", BondedInterfaces));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Mode", Mode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("TransmitHashPolicy", TransmitHashPolicy));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.interfaceName(InterfaceName, skipSignals);
    object.bondedInterfaces(BondedInterfaces, skipSignals);
    object.mode(Mode, skipSignals);
    object.transmitHashPolicy(TransmitHashPolicy, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("InterfaceName", object.interfaceName()));
        a(cereal::make_nvp("Local", object.local()));
        a(cereal::make_nvp("Remote", object.remote()));
        a(cereal::make_nvp("TOS", object.tos()));
        a(cereal::make_nvp("TTL", object.ttl()));
        a(cereal::make_nvp("DiscoverPathMTU", object.discoverPathMTU()));
        a(cereal::make_nvp("IPv6FlowLabel", object.ipv6FlowLabel()));
        a(cereal::make_nvp("CopyDSCP", object.copyDSCP()));
        a(cereal::make_nvp("EncapsulationLimit", object.encapsulationLimit()));
        a(cereal::make_nvp("Key", object.key()));
        a(cereal::make_nvp("InputKey", object.inputKey()));
        a(cereal::make_nvp("OutputKey", object.outputKey()));
        a(cereal::make_nvp("Mode", object.mode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel& object,
          const std::uint32_t version)
{
    decltype(object.interfaceName()) InterfaceName{};
    decltype(object.local()) Local{};
    decltype(object.remote()) Remote{};
    decltype(object.tos()) TOS{};
    decltype(object.ttl()) TTL{};
    decltype(object.discoverPathMTU()) DiscoverPathMTU{};
    decltype(object.ipv6FlowLabel()) IPv6FlowLabel{};
    decltype(object.copyDSCP()) CopyDSCP{};
    decltype(object.encapsulationLimit()) EncapsulationLimit{};
    decltype(object.key()) Key{};
    decltype(object.inputKey()) InputKey{};
    decltype(object.outputKey()) OutputKey{};
    decltype(object.mode()) Mode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(InterfaceName, Local, Remote, TOS, TTL, DiscoverPathMTU, IPv6FlowLabel, CopyDSCP, EncapsulationLimit, Key, InputKey, OutputKey, Mode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("InterfaceName", InterfaceName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Local", Local));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Remote", Remote));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("TOS", TOS));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("TTL", TTL));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DiscoverPathMTU", DiscoverPathMTU));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("IPv6FlowLabel", IPv6FlowLabel));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CopyDSCP", CopyDSCP));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("EncapsulationLimit", EncapsulationLimit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Key", Key));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("InputKey", InputKey));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("OutputKey", OutputKey));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Mode", Mode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.interfaceName(InterfaceName, skipSignals);
    object.local(Local, skipSignals);
    object.remote(Remote, skipSignals);
    object.tos(TOS, skipSignals);
    object.ttl(TTL, skipSignals);
    object.discoverPathMTU(DiscoverPathMTU, skipSignals);
    object.ipv6FlowLabel(IPv6FlowLabel, skipSignals);
    object.copyDSCP(CopyDSCP, skipSignals);
    object.encapsulationLimit(EncapsulationLimit, skipSignals);
    object.key(Key, skipSignals);
    object.inputKey(InputKey, skipSignals);
    object.outputKey(OutputKey, skipSignals);
    object.mode(Mode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Nvme::server::Status& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SmartWarnings", object.smartWarnings()));
        a(cereal::make_nvp("StatusFlags", object.statusFlags()));
        a(cereal::make_nvp("DriveLifeUsed", object.driveLifeUsed()));
        a(cereal::make_nvp("CapacityFault", object.capacityFault()));
        a(cereal::make_nvp("TemperatureFault", object.temperatureFault()));
        a(cereal::make_nvp("DegradesFault", object.degradesFault()));
        a(cereal::make_nvp("MediaFault", object.mediaFault()));
        a(cereal::make_nvp("BackupDeviceFault", object.backupDeviceFault()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Nvme::server::Status& object,
          const std::uint32_t version)
{
    decltype(object.smartWarnings()) SmartWarnings{};
    decltype(object.statusFlags()) StatusFlags{};
    decltype(object.driveLifeUsed()) DriveLifeUsed{};
    decltype(object.capacityFault()) CapacityFault{};
    decltype(object.temperatureFault()) TemperatureFault{};
    decltype(object.degradesFault()) DegradesFault{};
    decltype(object.mediaFault()) MediaFault{};
    decltype(object.backupDeviceFault()) BackupDeviceFault{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SmartWarnings, StatusFlags, DriveLifeUsed, CapacityFault, TemperatureFault, DegradesFault, MediaFault, BackupDeviceFault);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SmartWarnings", SmartWarnings));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("StatusFlags", StatusFlags));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DriveLifeUsed", DriveLifeUsed));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CapacityFault", CapacityFault));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("TemperatureFault", TemperatureFault));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("DegradesFault", DegradesFault));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MediaFault", MediaFault));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BackupDeviceFault", BackupDeviceFault));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.smartWarnings(SmartWarnings, skipSignals);
    object.statusFlags(StatusFlags, skipSignals);
    object.driveLifeUsed(DriveLifeUsed, skipSignals);
    object.capacityFault(CapacityFault, skipSignals);
    object.temperatureFault(TemperatureFault, skipSignals);
    object.degradesFault(DegradesFault, skipSignals);
    object.mediaFault(MediaFault, skipSignals);
    object.backupDeviceFault(BackupDeviceFault, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Object::server::Enable& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Enabled", object.enabled()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Object::server::Enable& object,
          const std::uint32_t version)
{
    decltype(object.enabled()) Enabled{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Enabled);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Enabled", Enabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.enabled(Enabled, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::PFR::server::Attributes& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Provisioned", object.provisioned()));
        a(cereal::make_nvp("Locked", object.locked()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::PFR::server::Attributes& object,
          const std::uint32_t version)
{
    decltype(object.provisioned()) Provisioned{};
    decltype(object.locked()) Locked{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Provisioned, Locked);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Provisioned", Provisioned));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Locked", Locked));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.provisioned(Provisioned, skipSignals);
    object.locked(Locked, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::PLDM::server::Event& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::PLDM::server::Event& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CSR", object.csr()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::PLDM::Provider::Certs::Authority::server::CSR& object,
          const std::uint32_t version)
{
    decltype(object.csr()) CSR{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CSR);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CSR", CSR));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.csr(CSR, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Accuracy", object.accuracy()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::server::Accuracy& object,
          const std::uint32_t version)
{
    decltype(object.accuracy()) Accuracy{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Accuracy);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Accuracy", Accuracy));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.accuracy(Accuracy, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::server::Value& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Value", object.value()));
        a(cereal::make_nvp("MaxValue", object.maxValue()));
        a(cereal::make_nvp("MinValue", object.minValue()));
        a(cereal::make_nvp("Unit", object.unit()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::server::Value& object,
          const std::uint32_t version)
{
    decltype(object.value()) Value{};
    decltype(object.maxValue()) MaxValue{};
    decltype(object.minValue()) MinValue{};
    decltype(object.unit()) Unit{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Value, MaxValue, MinValue, Unit);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Value", Value));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MaxValue", MaxValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MinValue", MinValue));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Unit", Unit));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.value(Value, skipSignals);
    object.maxValue(MaxValue, skipSignals);
    object.minValue(MinValue, skipSignals);
    object.unit(Unit, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Mutable", object.mutable_()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::server::ValueMutability& object,
          const std::uint32_t version)
{
    decltype(object.mutable_()) Mutable{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Mutable);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Mutable", Mutable));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.mutable_(Mutable, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("CriticalHigh", object.criticalHigh()));
        a(cereal::make_nvp("CriticalLow", object.criticalLow()));
        a(cereal::make_nvp("CriticalAlarmHigh", object.criticalAlarmHigh()));
        a(cereal::make_nvp("CriticalAlarmLow", object.criticalAlarmLow()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Critical& object,
          const std::uint32_t version)
{
    decltype(object.criticalHigh()) CriticalHigh{};
    decltype(object.criticalLow()) CriticalLow{};
    decltype(object.criticalAlarmHigh()) CriticalAlarmHigh{};
    decltype(object.criticalAlarmLow()) CriticalAlarmLow{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(CriticalHigh, CriticalLow, CriticalAlarmHigh, CriticalAlarmLow);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("CriticalHigh", CriticalHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CriticalLow", CriticalLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CriticalAlarmHigh", CriticalAlarmHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CriticalAlarmLow", CriticalAlarmLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.criticalHigh(CriticalHigh, skipSignals);
    object.criticalLow(CriticalLow, skipSignals);
    object.criticalAlarmHigh(CriticalAlarmHigh, skipSignals);
    object.criticalAlarmLow(CriticalAlarmLow, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("HardShutdownHigh", object.hardShutdownHigh()));
        a(cereal::make_nvp("HardShutdownLow", object.hardShutdownLow()));
        a(cereal::make_nvp("HardShutdownAlarmHigh", object.hardShutdownAlarmHigh()));
        a(cereal::make_nvp("HardShutdownAlarmLow", object.hardShutdownAlarmLow()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::HardShutdown& object,
          const std::uint32_t version)
{
    decltype(object.hardShutdownHigh()) HardShutdownHigh{};
    decltype(object.hardShutdownLow()) HardShutdownLow{};
    decltype(object.hardShutdownAlarmHigh()) HardShutdownAlarmHigh{};
    decltype(object.hardShutdownAlarmLow()) HardShutdownAlarmLow{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(HardShutdownHigh, HardShutdownLow, HardShutdownAlarmHigh, HardShutdownAlarmLow);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("HardShutdownHigh", HardShutdownHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("HardShutdownLow", HardShutdownLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("HardShutdownAlarmHigh", HardShutdownAlarmHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("HardShutdownAlarmLow", HardShutdownAlarmLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.hardShutdownHigh(HardShutdownHigh, skipSignals);
    object.hardShutdownLow(HardShutdownLow, skipSignals);
    object.hardShutdownAlarmHigh(HardShutdownAlarmHigh, skipSignals);
    object.hardShutdownAlarmLow(HardShutdownAlarmLow, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PerformanceLossHigh", object.performanceLossHigh()));
        a(cereal::make_nvp("PerformanceLossLow", object.performanceLossLow()));
        a(cereal::make_nvp("PerformanceLossAlarmHigh", object.performanceLossAlarmHigh()));
        a(cereal::make_nvp("PerformanceLossAlarmLow", object.performanceLossAlarmLow()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::PerformanceLoss& object,
          const std::uint32_t version)
{
    decltype(object.performanceLossHigh()) PerformanceLossHigh{};
    decltype(object.performanceLossLow()) PerformanceLossLow{};
    decltype(object.performanceLossAlarmHigh()) PerformanceLossAlarmHigh{};
    decltype(object.performanceLossAlarmLow()) PerformanceLossAlarmLow{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PerformanceLossHigh, PerformanceLossLow, PerformanceLossAlarmHigh, PerformanceLossAlarmLow);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PerformanceLossHigh", PerformanceLossHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PerformanceLossLow", PerformanceLossLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PerformanceLossAlarmHigh", PerformanceLossAlarmHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("PerformanceLossAlarmLow", PerformanceLossAlarmLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.performanceLossHigh(PerformanceLossHigh, skipSignals);
    object.performanceLossLow(PerformanceLossLow, skipSignals);
    object.performanceLossAlarmHigh(PerformanceLossAlarmHigh, skipSignals);
    object.performanceLossAlarmLow(PerformanceLossAlarmLow, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("SoftShutdownHigh", object.softShutdownHigh()));
        a(cereal::make_nvp("SoftShutdownLow", object.softShutdownLow()));
        a(cereal::make_nvp("SoftShutdownAlarmHigh", object.softShutdownAlarmHigh()));
        a(cereal::make_nvp("SoftShutdownAlarmLow", object.softShutdownAlarmLow()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::SoftShutdown& object,
          const std::uint32_t version)
{
    decltype(object.softShutdownHigh()) SoftShutdownHigh{};
    decltype(object.softShutdownLow()) SoftShutdownLow{};
    decltype(object.softShutdownAlarmHigh()) SoftShutdownAlarmHigh{};
    decltype(object.softShutdownAlarmLow()) SoftShutdownAlarmLow{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(SoftShutdownHigh, SoftShutdownLow, SoftShutdownAlarmHigh, SoftShutdownAlarmLow);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("SoftShutdownHigh", SoftShutdownHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SoftShutdownLow", SoftShutdownLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SoftShutdownAlarmHigh", SoftShutdownAlarmHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("SoftShutdownAlarmLow", SoftShutdownAlarmLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.softShutdownHigh(SoftShutdownHigh, skipSignals);
    object.softShutdownLow(SoftShutdownLow, skipSignals);
    object.softShutdownAlarmHigh(SoftShutdownAlarmHigh, skipSignals);
    object.softShutdownAlarmLow(SoftShutdownAlarmLow, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("WarningHigh", object.warningHigh()));
        a(cereal::make_nvp("WarningLow", object.warningLow()));
        a(cereal::make_nvp("WarningAlarmHigh", object.warningAlarmHigh()));
        a(cereal::make_nvp("WarningAlarmLow", object.warningAlarmLow()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Sensor::Threshold::server::Warning& object,
          const std::uint32_t version)
{
    decltype(object.warningHigh()) WarningHigh{};
    decltype(object.warningLow()) WarningLow{};
    decltype(object.warningAlarmHigh()) WarningAlarmHigh{};
    decltype(object.warningAlarmLow()) WarningAlarmLow{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(WarningHigh, WarningLow, WarningAlarmHigh, WarningAlarmLow);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("WarningHigh", WarningHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("WarningLow", WarningLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("WarningAlarmHigh", WarningAlarmHigh));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("WarningAlarmLow", WarningAlarmLow));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.warningHigh(WarningHigh, skipSignals);
    object.warningLow(WarningLow, skipSignals);
    object.warningAlarmHigh(WarningAlarmHigh, skipSignals);
    object.warningAlarmLow(WarningAlarmLow, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::Activation& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Activation", object.activation()));
        a(cereal::make_nvp("RequestedActivation", object.requestedActivation()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::Activation& object,
          const std::uint32_t version)
{
    decltype(object.activation()) Activation{};
    decltype(object.requestedActivation()) RequestedActivation{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Activation, RequestedActivation);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Activation", Activation));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RequestedActivation", RequestedActivation));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.activation(Activation, skipSignals);
    object.requestedActivation(RequestedActivation, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition& object,
          const std::uint32_t /* version */)
{
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::ActivationBlocksTransition& object,
          const std::uint32_t version)
{
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a();
    }
    else
    {
    }
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Progress", object.progress()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::ActivationProgress& object,
          const std::uint32_t version)
{
    decltype(object.progress()) Progress{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Progress);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Progress", Progress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.progress(Progress, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ClearConfig", object.clearConfig()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::ApplyOptions& object,
          const std::uint32_t version)
{
    decltype(object.clearConfig()) ClearConfig{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ClearConfig);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ClearConfig", ClearConfig));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.clearConfig(ClearConfig, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::ApplyTime& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("RequestedApplyTime", object.requestedApplyTime()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::ApplyTime& object,
          const std::uint32_t version)
{
    decltype(object.requestedApplyTime()) RequestedApplyTime{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(RequestedApplyTime);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("RequestedApplyTime", RequestedApplyTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.requestedApplyTime(RequestedApplyTime, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ExtendedVersion", object.extendedVersion()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::ExtendedVersion& object,
          const std::uint32_t version)
{
    decltype(object.extendedVersion()) ExtendedVersion{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ExtendedVersion);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ExtendedVersion", ExtendedVersion));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.extendedVersion(ExtendedVersion, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Priority", object.priority()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::RedundancyPriority& object,
          const std::uint32_t version)
{
    decltype(object.priority()) Priority{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Priority);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Priority", Priority));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.priority(Priority, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Priority", object.priority()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::RequestedRedundancyPriority& object,
          const std::uint32_t version)
{
    decltype(object.priority()) Priority{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Priority);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Priority", Priority));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.priority(Priority, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::Settings& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("WriteProtected", object.writeProtected()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::Settings& object,
          const std::uint32_t version)
{
    decltype(object.writeProtected()) WriteProtected{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(WriteProtected);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("WriteProtected", WriteProtected));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.writeProtected(WriteProtected, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Software::server::Version& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Version", object.version()));
        a(cereal::make_nvp("Purpose", object.purpose()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Software::server::Version& object,
          const std::uint32_t version)
{
    decltype(object.version()) Version{};
    decltype(object.purpose()) Purpose{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Version, Purpose);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Version", Version));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Purpose", Purpose));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.version(Version, skipSignals);
    object.purpose(Purpose, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::server::BMC& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("RequestedBMCTransition", object.requestedBMCTransition()));
        a(cereal::make_nvp("CurrentBMCState", object.currentBMCState()));
        a(cereal::make_nvp("LastRebootTime", object.lastRebootTime()));
        a(cereal::make_nvp("LastRebootCause", object.lastRebootCause()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::server::BMC& object,
          const std::uint32_t version)
{
    decltype(object.requestedBMCTransition()) RequestedBMCTransition{};
    decltype(object.currentBMCState()) CurrentBMCState{};
    decltype(object.lastRebootTime()) LastRebootTime{};
    decltype(object.lastRebootCause()) LastRebootCause{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(RequestedBMCTransition, CurrentBMCState, LastRebootTime, LastRebootCause);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("RequestedBMCTransition", RequestedBMCTransition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentBMCState", CurrentBMCState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LastRebootTime", LastRebootTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LastRebootCause", LastRebootCause));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.requestedBMCTransition(RequestedBMCTransition, skipSignals);
    object.currentBMCState(CurrentBMCState, skipSignals);
    object.lastRebootTime(LastRebootTime, skipSignals);
    object.lastRebootCause(LastRebootCause, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::server::Chassis& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("RequestedPowerTransition", object.requestedPowerTransition()));
        a(cereal::make_nvp("CurrentPowerState", object.currentPowerState()));
        a(cereal::make_nvp("CurrentPowerStatus", object.currentPowerStatus()));
        a(cereal::make_nvp("LastStateChangeTime", object.lastStateChangeTime()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::server::Chassis& object,
          const std::uint32_t version)
{
    decltype(object.requestedPowerTransition()) RequestedPowerTransition{};
    decltype(object.currentPowerState()) CurrentPowerState{};
    decltype(object.currentPowerStatus()) CurrentPowerStatus{};
    decltype(object.lastStateChangeTime()) LastStateChangeTime{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(RequestedPowerTransition, CurrentPowerState, CurrentPowerStatus, LastStateChangeTime);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("RequestedPowerTransition", RequestedPowerTransition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentPowerState", CurrentPowerState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentPowerStatus", CurrentPowerStatus));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LastStateChangeTime", LastStateChangeTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.requestedPowerTransition(RequestedPowerTransition, skipSignals);
    object.currentPowerState(CurrentPowerState, skipSignals);
    object.currentPowerStatus(CurrentPowerStatus, skipSignals);
    object.lastStateChangeTime(LastStateChangeTime, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::server::Drive& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Rebuilding", object.rebuilding()));
        a(cereal::make_nvp("RequestedDriveTransition", object.requestedDriveTransition()));
        a(cereal::make_nvp("CurrentDriveState", object.currentDriveState()));
        a(cereal::make_nvp("LastRebootTime", object.lastRebootTime()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::server::Drive& object,
          const std::uint32_t version)
{
    decltype(object.rebuilding()) Rebuilding{};
    decltype(object.requestedDriveTransition()) RequestedDriveTransition{};
    decltype(object.currentDriveState()) CurrentDriveState{};
    decltype(object.lastRebootTime()) LastRebootTime{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Rebuilding, RequestedDriveTransition, CurrentDriveState, LastRebootTime);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Rebuilding", Rebuilding));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RequestedDriveTransition", RequestedDriveTransition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentDriveState", CurrentDriveState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LastRebootTime", LastRebootTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.rebuilding(Rebuilding, skipSignals);
    object.requestedDriveTransition(RequestedDriveTransition, skipSignals);
    object.currentDriveState(CurrentDriveState, skipSignals);
    object.lastRebootTime(LastRebootTime, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::server::Host& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("RequestedHostTransition", object.requestedHostTransition()));
        a(cereal::make_nvp("CurrentHostState", object.currentHostState()));
        a(cereal::make_nvp("RestartCause", object.restartCause()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::server::Host& object,
          const std::uint32_t version)
{
    decltype(object.requestedHostTransition()) RequestedHostTransition{};
    decltype(object.currentHostState()) CurrentHostState{};
    decltype(object.restartCause()) RestartCause{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(RequestedHostTransition, CurrentHostState, RestartCause);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("RequestedHostTransition", RequestedHostTransition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("CurrentHostState", CurrentHostState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RestartCause", RestartCause));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.requestedHostTransition(RequestedHostTransition, skipSignals);
    object.currentHostState(CurrentHostState, skipSignals);
    object.restartCause(RestartCause, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::server::PowerOnHours& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("POHCounter", object.pohCounter()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::server::PowerOnHours& object,
          const std::uint32_t version)
{
    decltype(object.pohCounter()) POHCounter{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(POHCounter);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("POHCounter", POHCounter));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.pohCounter(POHCounter, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ScheduledTime", object.scheduledTime()));
        a(cereal::make_nvp("ScheduledTransition", object.scheduledTransition()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::server::ScheduledHostTransition& object,
          const std::uint32_t version)
{
    decltype(object.scheduledTime()) ScheduledTime{};
    decltype(object.scheduledTransition()) ScheduledTransition{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ScheduledTime, ScheduledTransition);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ScheduledTime", ScheduledTime));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ScheduledTransition", ScheduledTransition));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.scheduledTime(ScheduledTime, skipSignals);
    object.scheduledTransition(ScheduledTransition, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::Boot::server::Progress& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("BootProgress", object.bootProgress()));
        a(cereal::make_nvp("BootProgressLastUpdate", object.bootProgressLastUpdate()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::Boot::server::Progress& object,
          const std::uint32_t version)
{
    decltype(object.bootProgress()) BootProgress{};
    decltype(object.bootProgressLastUpdate()) BootProgressLastUpdate{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(BootProgress, BootProgressLastUpdate);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("BootProgress", BootProgress));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BootProgressLastUpdate", BootProgressLastUpdate));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.bootProgress(BootProgress, skipSignals);
    object.bootProgressLastUpdate(BootProgressLastUpdate, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::Boot::server::Raw& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Value", object.value()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::Boot::server::Raw& object,
          const std::uint32_t version)
{
    decltype(object.value()) Value{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Value);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Value", Value));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.value(Value, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Available", object.available()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::Decorator::server::Availability& object,
          const std::uint32_t version)
{
    decltype(object.available()) Available{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Available);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Available", Available));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.available(Available, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Functional", object.functional()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::Decorator::server::OperationalStatus& object,
          const std::uint32_t version)
{
    decltype(object.functional()) Functional{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Functional);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Functional", Functional));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.functional(Functional, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("PowerState", object.powerState()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState& object,
          const std::uint32_t version)
{
    decltype(object.powerState()) PowerState{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(PowerState);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("PowerState", PowerState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.powerState(PowerState, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Status", object.status()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs& object,
          const std::uint32_t version)
{
    decltype(object.status()) Status{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Status);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Status", Status));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.status(Status, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("OperatingSystemState", object.operatingSystemState()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status& object,
          const std::uint32_t version)
{
    decltype(object.operatingSystemState()) OperatingSystemState{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(OperatingSystemState);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("OperatingSystemState", OperatingSystemState));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.operatingSystemState(OperatingSystemState, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Time::server::EpochTime& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Elapsed", object.elapsed()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Time::server::EpochTime& object,
          const std::uint32_t version)
{
    decltype(object.elapsed()) Elapsed{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Elapsed);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Elapsed", Elapsed));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.elapsed(Elapsed, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::Time::server::Synchronization& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("TimeSyncMethod", object.timeSyncMethod()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::Time::server::Synchronization& object,
          const std::uint32_t version)
{
    decltype(object.timeSyncMethod()) TimeSyncMethod{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(TimeSyncMethod);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("TimeSyncMethod", TimeSyncMethod));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.timeSyncMethod(TimeSyncMethod, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::User::server::AccountPolicy& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("MaxLoginAttemptBeforeLockout", object.maxLoginAttemptBeforeLockout()));
        a(cereal::make_nvp("AccountUnlockTimeout", object.accountUnlockTimeout()));
        a(cereal::make_nvp("MinPasswordLength", object.minPasswordLength()));
        a(cereal::make_nvp("RememberOldPasswordTimes", object.rememberOldPasswordTimes()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::User::server::AccountPolicy& object,
          const std::uint32_t version)
{
    decltype(object.maxLoginAttemptBeforeLockout()) MaxLoginAttemptBeforeLockout{};
    decltype(object.accountUnlockTimeout()) AccountUnlockTimeout{};
    decltype(object.minPasswordLength()) MinPasswordLength{};
    decltype(object.rememberOldPasswordTimes()) RememberOldPasswordTimes{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(MaxLoginAttemptBeforeLockout, AccountUnlockTimeout, MinPasswordLength, RememberOldPasswordTimes);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("MaxLoginAttemptBeforeLockout", MaxLoginAttemptBeforeLockout));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("AccountUnlockTimeout", AccountUnlockTimeout));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("MinPasswordLength", MinPasswordLength));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RememberOldPasswordTimes", RememberOldPasswordTimes));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.maxLoginAttemptBeforeLockout(MaxLoginAttemptBeforeLockout, skipSignals);
    object.accountUnlockTimeout(AccountUnlockTimeout, skipSignals);
    object.minPasswordLength(MinPasswordLength, skipSignals);
    object.rememberOldPasswordTimes(RememberOldPasswordTimes, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::User::server::Attributes& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("UserGroups", object.userGroups()));
        a(cereal::make_nvp("UserPrivilege", object.userPrivilege()));
        a(cereal::make_nvp("UserEnabled", object.userEnabled()));
        a(cereal::make_nvp("UserLockedForFailedAttempt", object.userLockedForFailedAttempt()));
        a(cereal::make_nvp("RemoteUser", object.remoteUser()));
        a(cereal::make_nvp("UserPasswordExpired", object.userPasswordExpired()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::User::server::Attributes& object,
          const std::uint32_t version)
{
    decltype(object.userGroups()) UserGroups{};
    decltype(object.userPrivilege()) UserPrivilege{};
    decltype(object.userEnabled()) UserEnabled{};
    decltype(object.userLockedForFailedAttempt()) UserLockedForFailedAttempt{};
    decltype(object.remoteUser()) RemoteUser{};
    decltype(object.userPasswordExpired()) UserPasswordExpired{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(UserGroups, UserPrivilege, UserEnabled, UserLockedForFailedAttempt, RemoteUser, UserPasswordExpired);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("UserGroups", UserGroups));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UserPrivilege", UserPrivilege));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UserEnabled", UserEnabled));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UserLockedForFailedAttempt", UserLockedForFailedAttempt));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RemoteUser", RemoteUser));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UserPasswordExpired", UserPasswordExpired));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.userGroups(UserGroups, skipSignals);
    object.userPrivilege(UserPrivilege, skipSignals);
    object.userEnabled(UserEnabled, skipSignals);
    object.userLockedForFailedAttempt(UserLockedForFailedAttempt, skipSignals);
    object.remoteUser(RemoteUser, skipSignals);
    object.userPasswordExpired(UserPasswordExpired, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("GroupName", object.groupName()));
        a(cereal::make_nvp("Privilege", object.privilege()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::User::server::PrivilegeMapperEntry& object,
          const std::uint32_t version)
{
    decltype(object.groupName()) GroupName{};
    decltype(object.privilege()) Privilege{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(GroupName, Privilege);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("GroupName", GroupName));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Privilege", Privilege));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.groupName(GroupName, skipSignals);
    object.privilege(Privilege, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::User::Ldap::server::Config& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("LDAPServerURI", object.ldapServerURI()));
        a(cereal::make_nvp("LDAPBindDN", object.ldapBindDN()));
        a(cereal::make_nvp("LDAPBindDNPassword", object.ldapBindDNPassword()));
        a(cereal::make_nvp("LDAPBaseDN", object.ldapBaseDN()));
        a(cereal::make_nvp("LDAPSearchScope", object.ldapSearchScope()));
        a(cereal::make_nvp("LDAPType", object.ldapType()));
        a(cereal::make_nvp("GroupNameAttribute", object.groupNameAttribute()));
        a(cereal::make_nvp("UserNameAttribute", object.userNameAttribute()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::User::Ldap::server::Config& object,
          const std::uint32_t version)
{
    decltype(object.ldapServerURI()) LDAPServerURI{};
    decltype(object.ldapBindDN()) LDAPBindDN{};
    decltype(object.ldapBindDNPassword()) LDAPBindDNPassword{};
    decltype(object.ldapBaseDN()) LDAPBaseDN{};
    decltype(object.ldapSearchScope()) LDAPSearchScope{};
    decltype(object.ldapType()) LDAPType{};
    decltype(object.groupNameAttribute()) GroupNameAttribute{};
    decltype(object.userNameAttribute()) UserNameAttribute{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(LDAPServerURI, LDAPBindDN, LDAPBindDNPassword, LDAPBaseDN, LDAPSearchScope, LDAPType, GroupNameAttribute, UserNameAttribute);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("LDAPServerURI", LDAPServerURI));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LDAPBindDN", LDAPBindDN));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LDAPBindDNPassword", LDAPBindDNPassword));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LDAPBaseDN", LDAPBaseDN));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LDAPSearchScope", LDAPSearchScope));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("LDAPType", LDAPType));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("GroupNameAttribute", GroupNameAttribute));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("UserNameAttribute", UserNameAttribute));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.ldapServerURI(LDAPServerURI, skipSignals);
    object.ldapBindDN(LDAPBindDN, skipSignals);
    object.ldapBindDNPassword(LDAPBindDNPassword, skipSignals);
    object.ldapBaseDN(LDAPBaseDN, skipSignals);
    object.ldapSearchScope(LDAPSearchScope, skipSignals);
    object.ldapType(LDAPType, skipSignals);
    object.groupNameAttribute(GroupNameAttribute, skipSignals);
    object.userNameAttribute(UserNameAttribute, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("EndPointId", object.endPointId()));
        a(cereal::make_nvp("Mode", object.mode()));
        a(cereal::make_nvp("Device", object.device()));
        a(cereal::make_nvp("Socket", object.socket()));
        a(cereal::make_nvp("Timeout", object.timeout()));
        a(cereal::make_nvp("BlockSize", object.blockSize()));
        a(cereal::make_nvp("RemainingInactivityTimeout", object.remainingInactivityTimeout()));
        a(cereal::make_nvp("ImageURL", object.imageURL()));
        a(cereal::make_nvp("WriteProtected", object.writeProtected()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::VirtualMedia::server::MountPoint& object,
          const std::uint32_t version)
{
    decltype(object.endPointId()) EndPointId{};
    decltype(object.mode()) Mode{};
    decltype(object.device()) Device{};
    decltype(object.socket()) Socket{};
    decltype(object.timeout()) Timeout{};
    decltype(object.blockSize()) BlockSize{};
    decltype(object.remainingInactivityTimeout()) RemainingInactivityTimeout{};
    decltype(object.imageURL()) ImageURL{};
    decltype(object.writeProtected()) WriteProtected{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(EndPointId, Mode, Device, Socket, Timeout, BlockSize, RemainingInactivityTimeout, ImageURL, WriteProtected);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("EndPointId", EndPointId));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Mode", Mode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Device", Device));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Socket", Socket));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("Timeout", Timeout));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("BlockSize", BlockSize));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("RemainingInactivityTimeout", RemainingInactivityTimeout));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ImageURL", ImageURL));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("WriteProtected", WriteProtected));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.endPointId(EndPointId, skipSignals);
    object.mode(Mode, skipSignals);
    object.device(Device, skipSignals);
    object.socket(Socket, skipSignals);
    object.timeout(Timeout, skipSignals);
    object.blockSize(BlockSize, skipSignals);
    object.remainingInactivityTimeout(RemainingInactivityTimeout, skipSignals);
    object.imageURL(ImageURL, skipSignals);
    object.writeProtected(WriteProtected, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("Active", object.active()));
        a(cereal::make_nvp("ExitCode", object.exitCode()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::VirtualMedia::server::Process& object,
          const std::uint32_t version)
{
    decltype(object.active()) Active{};
    decltype(object.exitCode()) ExitCode{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(Active, ExitCode);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("Active", Active));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("ExitCode", ExitCode));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.active(Active, skipSignals);
    object.exitCode(ExitCode, skipSignals);
}

template<class Archive>
void save(Archive& a,
          const sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats& object,
          const std::uint32_t /* version */)
{
        a(cereal::make_nvp("ReadIO", object.readIO()));
        a(cereal::make_nvp("WriteIO", object.writeIO()));
}

template<class Archive>
void load(Archive& a,
          sdbusplus::xyz::openbmc_project::VirtualMedia::server::Stats& object,
          const std::uint32_t version)
{
    decltype(object.readIO()) ReadIO{};
    decltype(object.writeIO()) WriteIO{};
    if (version < CLASS_VERSION_WITH_NVP)
    {
        a(ReadIO, WriteIO);
    }
    else
    {
        try
        {
            a(cereal::make_nvp("ReadIO", ReadIO));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
        try
        {
            a(cereal::make_nvp("WriteIO", WriteIO));
        }
        catch (const Exception &e)
        {
            // Ignore any exceptions, property value stays default
        }
    }
    object.readIO(ReadIO, skipSignals);
    object.writeIO(WriteIO, skipSignals);
}

} // namespace cereal
