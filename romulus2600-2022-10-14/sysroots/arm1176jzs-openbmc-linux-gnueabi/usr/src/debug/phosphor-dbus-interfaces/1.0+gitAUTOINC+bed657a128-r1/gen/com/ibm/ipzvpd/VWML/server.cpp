#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VWML/server.hpp>









namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VWML::VWML(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VWML_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VWML::VWML(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VWML(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VWML::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VWML::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VWML::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VWML::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VWML::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int VWML::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto VWML::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto VWML::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int VWML::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::oc() const ->
        std::vector<uint8_t>
{
    return _oc;
}

int VWML::_callback_get_OC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->oc();
                    }
                ));
    }
}

auto VWML::oc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_oc != value)
    {
        _oc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("OC");
        }
    }

    return _oc;
}

auto VWML::oc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return oc(val, false);
}

int VWML::_callback_set_OC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->oc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_OC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::pdi() const ->
        std::vector<uint8_t>
{
    return _pdi;
}

int VWML::_callback_get_PD_I(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdi();
                    }
                ));
    }
}

auto VWML::pdi(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdi != value)
    {
        _pdi = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("PD_I");
        }
    }

    return _pdi;
}

auto VWML::pdi(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdi(val, false);
}

int VWML::_callback_set_PD_I(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdi(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_PD_I =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int VWML::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto VWML::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto VWML::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int VWML::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::d5() const ->
        std::vector<uint8_t>
{
    return _d5;
}

int VWML::_callback_get_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d5();
                    }
                ));
    }
}

auto VWML::d5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d5 != value)
    {
        _d5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("D5");
        }
    }

    return _d5;
}

auto VWML::d5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d5(val, false);
}

int VWML::_callback_set_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_D5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::d8() const ->
        std::vector<uint8_t>
{
    return _d8;
}

int VWML::_callback_get_D8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d8();
                    }
                ));
    }
}

auto VWML::d8(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d8 != value)
    {
        _d8 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("D8");
        }
    }

    return _d8;
}

auto VWML::d8(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d8(val, false);
}

int VWML::_callback_set_D8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d8(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_D8 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VWML::f5() const ->
        std::vector<uint8_t>
{
    return _f5;
}

int VWML::_callback_get_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f5();
                    }
                ));
    }
}

auto VWML::f5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f5 != value)
    {
        _f5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VWML_interface.property_changed("F5");
        }
    }

    return _f5;
}

auto VWML::f5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f5(val, false);
}

int VWML::_callback_set_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VWML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VWML
{
static const auto _property_F5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VWML::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "OC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        oc(v, skipSignal);
        return;
    }
    if (_name == "PD_I")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdi(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
    if (_name == "D5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d5(v, skipSignal);
        return;
    }
    if (_name == "D8")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d8(v, skipSignal);
        return;
    }
    if (_name == "F5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f5(v, skipSignal);
        return;
    }
}

auto VWML::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "OC")
    {
        return oc();
    }
    if (_name == "PD_I")
    {
        return pdi();
    }
    if (_name == "D4")
    {
        return d4();
    }
    if (_name == "D5")
    {
        return d5();
    }
    if (_name == "D8")
    {
        return d8();
    }
    if (_name == "F5")
    {
        return f5();
    }

    return PropertiesVariant();
}


const vtable_t VWML::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VWML::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::VWML::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("OC",
                     details::VWML::_property_OC
                        .data(),
                     _callback_get_OC,
                     _callback_set_OC,
                     vtable::property_::emits_change),
    vtable::property("PD_I",
                     details::VWML::_property_PD_I
                        .data(),
                     _callback_get_PD_I,
                     _callback_set_PD_I,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::VWML::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::property("D5",
                     details::VWML::_property_D5
                        .data(),
                     _callback_get_D5,
                     _callback_set_D5,
                     vtable::property_::emits_change),
    vtable::property("D8",
                     details::VWML::_property_D8
                        .data(),
                     _callback_get_D8,
                     _callback_set_D8,
                     vtable::property_::emits_change),
    vtable::property("F5",
                     details::VWML::_property_F5
                        .data(),
                     _callback_get_F5,
                     _callback_set_F5,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

