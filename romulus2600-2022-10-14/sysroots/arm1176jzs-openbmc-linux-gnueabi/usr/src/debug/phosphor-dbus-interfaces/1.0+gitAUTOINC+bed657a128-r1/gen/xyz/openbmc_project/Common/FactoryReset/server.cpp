#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/FactoryReset/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

FactoryReset::FactoryReset(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_FactoryReset_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int FactoryReset::_callback_Reset(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<FactoryReset*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->reset(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace FactoryReset
{
static const auto _param_Reset =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Reset =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t FactoryReset::_vtable[] = {
    vtable::start(),

    vtable::method("Reset",
                   details::FactoryReset::_param_Reset
                        .data(),
                   details::FactoryReset::_return_Reset
                        .data(),
                   _callback_Reset),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

