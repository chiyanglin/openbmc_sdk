#include <xyz/openbmc_project/Control/Host/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Host
{
namespace Error
{
const char* CommandNotSupported::name() const noexcept
{
    return errName;
}
const char* CommandNotSupported::description() const noexcept
{
    return errDesc;
}
const char* CommandNotSupported::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Host
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

