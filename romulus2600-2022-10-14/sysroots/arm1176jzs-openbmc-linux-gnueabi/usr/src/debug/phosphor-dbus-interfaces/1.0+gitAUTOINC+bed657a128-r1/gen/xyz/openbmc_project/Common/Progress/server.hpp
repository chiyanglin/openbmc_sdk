#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

class Progress
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Progress() = delete;
        Progress(const Progress&) = delete;
        Progress& operator=(const Progress&) = delete;
        Progress(Progress&&) = delete;
        Progress& operator=(Progress&&) = delete;
        virtual ~Progress() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Progress(bus_t& bus, const char* path);

        enum class OperationStatus
        {
            InProgress,
            Completed,
            Failed,
            Aborted,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                OperationStatus,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Progress(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Status */
        virtual OperationStatus status() const;
        /** Set value of Status with option to skip sending signal */
        virtual OperationStatus status(OperationStatus value,
               bool skipSignal);
        /** Set value of Status */
        virtual OperationStatus status(OperationStatus value);
        /** Get value of StartTime */
        virtual uint64_t startTime() const;
        /** Set value of StartTime with option to skip sending signal */
        virtual uint64_t startTime(uint64_t value,
               bool skipSignal);
        /** Set value of StartTime */
        virtual uint64_t startTime(uint64_t value);
        /** Get value of CompletedTime */
        virtual uint64_t completedTime() const;
        /** Set value of CompletedTime with option to skip sending signal */
        virtual uint64_t completedTime(uint64_t value,
               bool skipSignal);
        /** Set value of CompletedTime */
        virtual uint64_t completedTime(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Common.Progress.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static OperationStatus convertOperationStatusFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Common.Progress.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<OperationStatus> convertStringToOperationStatus(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Common.Progress.<value name>"
         */
        static std::string convertOperationStatusToString(OperationStatus e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Common_Progress_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Common_Progress_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Common.Progress";

    private:

        /** @brief sd-bus callback for get-property 'Status' */
        static int _callback_get_Status(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Status' */
        static int _callback_set_Status(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'StartTime' */
        static int _callback_get_StartTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'StartTime' */
        static int _callback_set_StartTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CompletedTime' */
        static int _callback_get_CompletedTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CompletedTime' */
        static int _callback_set_CompletedTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Common_Progress_interface;
        sdbusplus::SdBusInterface *_intf;

        OperationStatus _status = OperationStatus::InProgress;
        uint64_t _startTime{};
        uint64_t _completedTime{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Progress::OperationStatus.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Progress::OperationStatus e)
{
    return Progress::convertOperationStatusToString(e);
}

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Common::server::Progress::OperationStatus>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Common::server::Progress::convertStringToOperationStatus(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Common::server::Progress::OperationStatus>
{
    static std::string op(xyz::openbmc_project::Common::server::Progress::OperationStatus value)
    {
        return xyz::openbmc_project::Common::server::Progress::convertOperationStatusToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

