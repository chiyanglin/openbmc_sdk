#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VSYS/server.hpp>
















namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VSYS::VSYS(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VSYS_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VSYS::VSYS(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VSYS(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VSYS::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VSYS::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VSYS::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VSYS::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VSYS::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VSYS::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VSYS::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VSYS::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VSYS::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::br() const ->
        std::vector<uint8_t>
{
    return _br;
}

int VSYS::_callback_get_BR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->br();
                    }
                ));
    }
}

auto VSYS::br(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_br != value)
    {
        _br = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("BR");
        }
    }

    return _br;
}

auto VSYS::br(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return br(val, false);
}

int VSYS::_callback_set_BR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->br(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_BR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::se() const ->
        std::vector<uint8_t>
{
    return _se;
}

int VSYS::_callback_get_SE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->se();
                    }
                ));
    }
}

auto VSYS::se(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_se != value)
    {
        _se = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("SE");
        }
    }

    return _se;
}

auto VSYS::se(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return se(val, false);
}

int VSYS::_callback_set_SE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->se(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_SE =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::sg() const ->
        std::vector<uint8_t>
{
    return _sg;
}

int VSYS::_callback_get_SG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sg();
                    }
                ));
    }
}

auto VSYS::sg(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_sg != value)
    {
        _sg = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("SG");
        }
    }

    return _sg;
}

auto VSYS::sg(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return sg(val, false);
}

int VSYS::_callback_set_SG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->sg(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_SG =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::tm() const ->
        std::vector<uint8_t>
{
    return _tm;
}

int VSYS::_callback_get_TM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tm();
                    }
                ));
    }
}

auto VSYS::tm(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tm != value)
    {
        _tm = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("TM");
        }
    }

    return _tm;
}

auto VSYS::tm(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tm(val, false);
}

int VSYS::_callback_set_TM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tm(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_TM =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::tn() const ->
        std::vector<uint8_t>
{
    return _tn;
}

int VSYS::_callback_get_TN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tn();
                    }
                ));
    }
}

auto VSYS::tn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tn != value)
    {
        _tn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("TN");
        }
    }

    return _tn;
}

auto VSYS::tn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tn(val, false);
}

int VSYS::_callback_set_TN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_TN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::mn() const ->
        std::vector<uint8_t>
{
    return _mn;
}

int VSYS::_callback_get_MN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mn();
                    }
                ));
    }
}

auto VSYS::mn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_mn != value)
    {
        _mn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("MN");
        }
    }

    return _mn;
}

auto VSYS::mn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return mn(val, false);
}

int VSYS::_callback_set_MN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->mn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_MN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::id() const ->
        std::vector<uint8_t>
{
    return _id;
}

int VSYS::_callback_get_ID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->id();
                    }
                ));
    }
}

auto VSYS::id(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_id != value)
    {
        _id = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("ID");
        }
    }

    return _id;
}

auto VSYS::id(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return id(val, false);
}

int VSYS::_callback_set_ID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->id(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_ID =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::su() const ->
        std::vector<uint8_t>
{
    return _su;
}

int VSYS::_callback_get_SU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->su();
                    }
                ));
    }
}

auto VSYS::su(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_su != value)
    {
        _su = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("SU");
        }
    }

    return _su;
}

auto VSYS::su(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return su(val, false);
}

int VSYS::_callback_set_SU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->su(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_SU =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::nn() const ->
        std::vector<uint8_t>
{
    return _nn;
}

int VSYS::_callback_get_NN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nn();
                    }
                ));
    }
}

auto VSYS::nn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_nn != value)
    {
        _nn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("NN");
        }
    }

    return _nn;
}

auto VSYS::nn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return nn(val, false);
}

int VSYS::_callback_set_NN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->nn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_NN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::rg() const ->
        std::vector<uint8_t>
{
    return _rg;
}

int VSYS::_callback_get_RG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rg();
                    }
                ));
    }
}

auto VSYS::rg(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rg != value)
    {
        _rg = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("RG");
        }
    }

    return _rg;
}

auto VSYS::rg(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rg(val, false);
}

int VSYS::_callback_set_RG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rg(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_RG =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::rb() const ->
        std::vector<uint8_t>
{
    return _rb;
}

int VSYS::_callback_get_RB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rb();
                    }
                ));
    }
}

auto VSYS::rb(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rb != value)
    {
        _rb = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("RB");
        }
    }

    return _rb;
}

auto VSYS::rb(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rb(val, false);
}

int VSYS::_callback_set_RB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rb(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_RB =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::wn() const ->
        std::vector<uint8_t>
{
    return _wn;
}

int VSYS::_callback_get_WN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->wn();
                    }
                ));
    }
}

auto VSYS::wn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_wn != value)
    {
        _wn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("WN");
        }
    }

    return _wn;
}

auto VSYS::wn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return wn(val, false);
}

int VSYS::_callback_set_WN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->wn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_WN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSYS::fv() const ->
        std::vector<uint8_t>
{
    return _fv;
}

int VSYS::_callback_get_FV(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fv();
                    }
                ));
    }
}

auto VSYS::fv(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_fv != value)
    {
        _fv = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSYS_interface.property_changed("FV");
        }
    }

    return _fv;
}

auto VSYS::fv(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return fv(val, false);
}

int VSYS::_callback_set_FV(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSYS*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->fv(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSYS
{
static const auto _property_FV =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VSYS::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "BR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        br(v, skipSignal);
        return;
    }
    if (_name == "SE")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        se(v, skipSignal);
        return;
    }
    if (_name == "SG")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        sg(v, skipSignal);
        return;
    }
    if (_name == "TM")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tm(v, skipSignal);
        return;
    }
    if (_name == "TN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tn(v, skipSignal);
        return;
    }
    if (_name == "MN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        mn(v, skipSignal);
        return;
    }
    if (_name == "ID")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        id(v, skipSignal);
        return;
    }
    if (_name == "SU")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        su(v, skipSignal);
        return;
    }
    if (_name == "NN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        nn(v, skipSignal);
        return;
    }
    if (_name == "RG")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rg(v, skipSignal);
        return;
    }
    if (_name == "RB")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rb(v, skipSignal);
        return;
    }
    if (_name == "WN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        wn(v, skipSignal);
        return;
    }
    if (_name == "FV")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        fv(v, skipSignal);
        return;
    }
}

auto VSYS::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "BR")
    {
        return br();
    }
    if (_name == "SE")
    {
        return se();
    }
    if (_name == "SG")
    {
        return sg();
    }
    if (_name == "TM")
    {
        return tm();
    }
    if (_name == "TN")
    {
        return tn();
    }
    if (_name == "MN")
    {
        return mn();
    }
    if (_name == "ID")
    {
        return id();
    }
    if (_name == "SU")
    {
        return su();
    }
    if (_name == "NN")
    {
        return nn();
    }
    if (_name == "RG")
    {
        return rg();
    }
    if (_name == "RB")
    {
        return rb();
    }
    if (_name == "WN")
    {
        return wn();
    }
    if (_name == "FV")
    {
        return fv();
    }

    return PropertiesVariant();
}


const vtable_t VSYS::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VSYS::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VSYS::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("BR",
                     details::VSYS::_property_BR
                        .data(),
                     _callback_get_BR,
                     _callback_set_BR,
                     vtable::property_::emits_change),
    vtable::property("SE",
                     details::VSYS::_property_SE
                        .data(),
                     _callback_get_SE,
                     _callback_set_SE,
                     vtable::property_::emits_change),
    vtable::property("SG",
                     details::VSYS::_property_SG
                        .data(),
                     _callback_get_SG,
                     _callback_set_SG,
                     vtable::property_::emits_change),
    vtable::property("TM",
                     details::VSYS::_property_TM
                        .data(),
                     _callback_get_TM,
                     _callback_set_TM,
                     vtable::property_::emits_change),
    vtable::property("TN",
                     details::VSYS::_property_TN
                        .data(),
                     _callback_get_TN,
                     _callback_set_TN,
                     vtable::property_::emits_change),
    vtable::property("MN",
                     details::VSYS::_property_MN
                        .data(),
                     _callback_get_MN,
                     _callback_set_MN,
                     vtable::property_::emits_change),
    vtable::property("ID",
                     details::VSYS::_property_ID
                        .data(),
                     _callback_get_ID,
                     _callback_set_ID,
                     vtable::property_::emits_change),
    vtable::property("SU",
                     details::VSYS::_property_SU
                        .data(),
                     _callback_get_SU,
                     _callback_set_SU,
                     vtable::property_::emits_change),
    vtable::property("NN",
                     details::VSYS::_property_NN
                        .data(),
                     _callback_get_NN,
                     _callback_set_NN,
                     vtable::property_::emits_change),
    vtable::property("RG",
                     details::VSYS::_property_RG
                        .data(),
                     _callback_get_RG,
                     _callback_set_RG,
                     vtable::property_::emits_change),
    vtable::property("RB",
                     details::VSYS::_property_RB
                        .data(),
                     _callback_get_RB,
                     _callback_set_RB,
                     vtable::property_::emits_change),
    vtable::property("WN",
                     details::VSYS::_property_WN
                        .data(),
                     _callback_get_WN,
                     _callback_set_WN,
                     vtable::property_::emits_change),
    vtable::property("FV",
                     details::VSYS::_property_FV
                        .data(),
                     _callback_get_FV,
                     _callback_set_FV,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

