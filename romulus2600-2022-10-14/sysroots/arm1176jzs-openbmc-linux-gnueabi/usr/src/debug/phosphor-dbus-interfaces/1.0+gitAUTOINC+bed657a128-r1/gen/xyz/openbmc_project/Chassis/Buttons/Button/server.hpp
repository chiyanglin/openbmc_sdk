#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>








#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace server
{

class Button
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Button() = delete;
        Button(const Button&) = delete;
        Button& operator=(const Button&) = delete;
        Button(Button&&) = delete;
        Button& operator=(Button&&) = delete;
        virtual ~Button() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Button(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Button(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for simPress
         *  Emulate button press.
         */
        virtual void simPress(
            ) = 0;

        /** @brief Implementation for simRelease
         *  Emulate button release.
         */
        virtual void simRelease(
            ) = 0;

        /** @brief Implementation for simLongPress
         *  Emulate long button press.
         */
        virtual void simLongPress(
            ) = 0;


        /** @brief Send signal 'Released'
         *
         *  Button released signal
         */
        void released(
            );

        /** @brief Send signal 'Pressed'
         *
         *  Button pressed signal
         */
        void pressed(
            );

        /** @brief Send signal 'PressedLong'
         *
         *  Button long pressed signal
         */
        void pressedLong(
            );

        /** Get value of Enabled */
        virtual bool enabled() const;
        /** Set value of Enabled with option to skip sending signal */
        virtual bool enabled(bool value,
               bool skipSignal);
        /** Set value of Enabled */
        virtual bool enabled(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Chassis_Buttons_Button_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Chassis_Buttons_Button_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Chassis.Buttons.Button";

    private:

        /** @brief sd-bus callback for simPress
         */
        static int _callback_SimPress(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for simRelease
         */
        static int _callback_SimRelease(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for simLongPress
         */
        static int _callback_SimLongPress(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Enabled' */
        static int _callback_get_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Enabled' */
        static int _callback_set_Enabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Chassis_Buttons_Button_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _enabled = true;

};


} // namespace server
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

