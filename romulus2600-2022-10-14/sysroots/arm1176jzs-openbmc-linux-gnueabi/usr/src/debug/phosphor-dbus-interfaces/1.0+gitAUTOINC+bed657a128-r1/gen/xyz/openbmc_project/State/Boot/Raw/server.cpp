#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Boot/Raw/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace server
{

Raw::Raw(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Boot_Raw_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Raw::Raw(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Raw(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Raw::value() const ->
        std::tuple<uint64_t, std::vector<uint8_t>>
{
    return _value;
}

int Raw::_callback_get_Value(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Raw*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->value();
                    }
                ));
    }
}

auto Raw::value(std::tuple<uint64_t, std::vector<uint8_t>> value,
                                         bool skipSignal) ->
        std::tuple<uint64_t, std::vector<uint8_t>>
{
    if (_value != value)
    {
        _value = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Boot_Raw_interface.property_changed("Value");
        }
    }

    return _value;
}

auto Raw::value(std::tuple<uint64_t, std::vector<uint8_t>> val) ->
        std::tuple<uint64_t, std::vector<uint8_t>>
{
    return value(val, false);
}

int Raw::_callback_set_Value(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Raw*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::tuple<uint64_t, std::vector<uint8_t>>&& arg)
                    {
                        o->value(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Raw
{
static const auto _property_Value =
    utility::tuple_to_array(message::types::type_id<
            std::tuple<uint64_t, std::vector<uint8_t>>>());
}
}

void Raw::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Value")
    {
        auto& v = std::get<std::tuple<uint64_t, std::vector<uint8_t>>>(val);
        value(v, skipSignal);
        return;
    }
}

auto Raw::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Value")
    {
        return value();
    }

    return PropertiesVariant();
}


const vtable_t Raw::_vtable[] = {
    vtable::start(),
    vtable::property("Value",
                     details::Raw::_property_Value
                        .data(),
                     _callback_get_Value,
                     _callback_set_Value,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

