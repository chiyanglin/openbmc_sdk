#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Time/Synchronization/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Time
{
namespace server
{

Synchronization::Synchronization(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Time_Synchronization_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Synchronization::Synchronization(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Synchronization(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Synchronization::timeSyncMethod() const ->
        Method
{
    return _timeSyncMethod;
}

int Synchronization::_callback_get_TimeSyncMethod(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Synchronization*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timeSyncMethod();
                    }
                ));
    }
}

auto Synchronization::timeSyncMethod(Method value,
                                         bool skipSignal) ->
        Method
{
    if (_timeSyncMethod != value)
    {
        _timeSyncMethod = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Time_Synchronization_interface.property_changed("TimeSyncMethod");
        }
    }

    return _timeSyncMethod;
}

auto Synchronization::timeSyncMethod(Method val) ->
        Method
{
    return timeSyncMethod(val, false);
}

int Synchronization::_callback_set_TimeSyncMethod(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Synchronization*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Method&& arg)
                    {
                        o->timeSyncMethod(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Synchronization
{
static const auto _property_TimeSyncMethod =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Time::server::Synchronization::Method>());
}
}

void Synchronization::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "TimeSyncMethod")
    {
        auto& v = std::get<Method>(val);
        timeSyncMethod(v, skipSignal);
        return;
    }
}

auto Synchronization::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "TimeSyncMethod")
    {
        return timeSyncMethod();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Synchronization::Method */
static const std::tuple<const char*, Synchronization::Method> mappingSynchronizationMethod[] =
        {
            std::make_tuple( "xyz.openbmc_project.Time.Synchronization.Method.NTP",                 Synchronization::Method::NTP ),
            std::make_tuple( "xyz.openbmc_project.Time.Synchronization.Method.Manual",                 Synchronization::Method::Manual ),
        };

} // anonymous namespace

auto Synchronization::convertStringToMethod(const std::string& s) noexcept ->
        std::optional<Method>
{
    auto i = std::find_if(
            std::begin(mappingSynchronizationMethod),
            std::end(mappingSynchronizationMethod),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingSynchronizationMethod) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Synchronization::convertMethodFromString(const std::string& s) ->
        Method
{
    auto r = convertStringToMethod(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Synchronization::convertMethodToString(Synchronization::Method v)
{
    auto i = std::find_if(
            std::begin(mappingSynchronizationMethod),
            std::end(mappingSynchronizationMethod),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingSynchronizationMethod))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Synchronization::_vtable[] = {
    vtable::start(),
    vtable::property("TimeSyncMethod",
                     details::Synchronization::_property_TimeSyncMethod
                        .data(),
                     _callback_get_TimeSyncMethod,
                     _callback_set_TimeSyncMethod,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Time
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

