#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Smbios
{
namespace MDR_V2
{
namespace Error
{
const char* InvalidParameter::name() const noexcept
{
    return errName;
}
const char* InvalidParameter::description() const noexcept
{
    return errDesc;
}
const char* InvalidParameter::what() const noexcept
{
    return errWhat;
}
const char* UpdateInProgress::name() const noexcept
{
    return errName;
}
const char* UpdateInProgress::description() const noexcept
{
    return errDesc;
}
const char* UpdateInProgress::what() const noexcept
{
    return errWhat;
}
const char* InvalidId::name() const noexcept
{
    return errName;
}
const char* InvalidId::description() const noexcept
{
    return errDesc;
}
const char* InvalidId::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace MDR_V2
} // namespace Smbios
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

