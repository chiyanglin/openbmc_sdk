#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Cable
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Cable() = delete;
        Cable(const Cable&) = delete;
        Cable& operator=(const Cable&) = delete;
        Cable(Cable&&) = delete;
        Cable& operator=(Cable&&) = delete;
        virtual ~Cable() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Cable(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                double,
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Cable(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Length */
        virtual double length() const;
        /** Set value of Length with option to skip sending signal */
        virtual double length(double value,
               bool skipSignal);
        /** Set value of Length */
        virtual double length(double value);
        /** Get value of CableTypeDescription */
        virtual std::string cableTypeDescription() const;
        /** Set value of CableTypeDescription with option to skip sending signal */
        virtual std::string cableTypeDescription(std::string value,
               bool skipSignal);
        /** Set value of CableTypeDescription */
        virtual std::string cableTypeDescription(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Cable_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Cable_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Cable";

    private:

        /** @brief sd-bus callback for get-property 'Length' */
        static int _callback_get_Length(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Length' */
        static int _callback_set_Length(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CableTypeDescription' */
        static int _callback_get_CableTypeDescription(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CableTypeDescription' */
        static int _callback_set_CableTypeDescription(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Cable_interface;
        sdbusplus::SdBusInterface *_intf;

        double _length = std::numeric_limits<double>::quiet_NaN();
        std::string _cableTypeDescription = "";

};


} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

