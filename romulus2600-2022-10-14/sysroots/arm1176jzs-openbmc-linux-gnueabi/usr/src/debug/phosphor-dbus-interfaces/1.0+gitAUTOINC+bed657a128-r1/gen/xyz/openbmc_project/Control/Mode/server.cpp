#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Mode/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

Mode::Mode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Mode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Mode::Mode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Mode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Mode::manual() const ->
        bool
{
    return _manual;
}

int Mode::_callback_get_Manual(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->manual();
                    }
                ));
    }
}

auto Mode::manual(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_manual != value)
    {
        _manual = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Mode_interface.property_changed("Manual");
        }
    }

    return _manual;
}

auto Mode::manual(bool val) ->
        bool
{
    return manual(val, false);
}

int Mode::_callback_set_Manual(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->manual(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Mode
{
static const auto _property_Manual =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Mode::failSafe() const ->
        bool
{
    return _failSafe;
}

int Mode::_callback_get_FailSafe(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->failSafe();
                    }
                ));
    }
}

auto Mode::failSafe(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_failSafe != value)
    {
        _failSafe = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Mode_interface.property_changed("FailSafe");
        }
    }

    return _failSafe;
}

auto Mode::failSafe(bool val) ->
        bool
{
    return failSafe(val, false);
}

int Mode::_callback_set_FailSafe(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->failSafe(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Mode
{
static const auto _property_FailSafe =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Mode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Manual")
    {
        auto& v = std::get<bool>(val);
        manual(v, skipSignal);
        return;
    }
    if (_name == "FailSafe")
    {
        auto& v = std::get<bool>(val);
        failSafe(v, skipSignal);
        return;
    }
}

auto Mode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Manual")
    {
        return manual();
    }
    if (_name == "FailSafe")
    {
        return failSafe();
    }

    return PropertiesVariant();
}


const vtable_t Mode::_vtable[] = {
    vtable::start(),
    vtable::property("Manual",
                     details::Mode::_property_Manual
                        .data(),
                     _callback_get_Manual,
                     _callback_set_Manual,
                     vtable::property_::emits_change),
    vtable::property("FailSafe",
                     details::Mode::_property_FailSafe
                        .data(),
                     _callback_get_FailSafe,
                     _callback_set_FailSafe,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

