#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

class Authority
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Authority() = delete;
        Authority(const Authority&) = delete;
        Authority& operator=(const Authority&) = delete;
        Authority(Authority&&) = delete;
        Authority& operator=(Authority&&) = delete;
        virtual ~Authority() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Authority(bus_t& bus, const char* path);



        /** @brief Implementation for SignCSR
         *  This method provides signing authority functionality.
         *
         *  @param[in] csr - Should be a valid PEM encoded Certificate signing request string.
         *
         *  @return path[sdbusplus::message::object_path] - The object path of an object that implements, at a minimum, xyz.openbmc_project.Certs.Entry and xyz.openbmc_project.Object.Delete
         */
        virtual sdbusplus::message::object_path signCSR(
            std::string csr) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Certs_Authority_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Certs_Authority_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Certs.Authority";

    private:

        /** @brief sd-bus callback for SignCSR
         */
        static int _callback_SignCSR(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Certs_Authority_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

