#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/VirtualMedia/Stats/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

Stats::Stats(bus_t& bus, const char* path)
        : _xyz_openbmc_project_VirtualMedia_Stats_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Stats::Stats(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Stats(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Stats::readIO() const ->
        uint64_t
{
    return _readIO;
}

int Stats::_callback_get_ReadIO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Stats*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->readIO();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Stats::readIO(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_readIO != value)
    {
        _readIO = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_Stats_interface.property_changed("ReadIO");
        }
    }

    return _readIO;
}

auto Stats::readIO(uint64_t val) ->
        uint64_t
{
    return readIO(val, false);
}


namespace details
{
namespace Stats
{
static const auto _property_ReadIO =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Stats::writeIO() const ->
        uint64_t
{
    return _writeIO;
}

int Stats::_callback_get_WriteIO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Stats*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->writeIO();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Stats::writeIO(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_writeIO != value)
    {
        _writeIO = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_Stats_interface.property_changed("WriteIO");
        }
    }

    return _writeIO;
}

auto Stats::writeIO(uint64_t val) ->
        uint64_t
{
    return writeIO(val, false);
}


namespace details
{
namespace Stats
{
static const auto _property_WriteIO =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Stats::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ReadIO")
    {
        auto& v = std::get<uint64_t>(val);
        readIO(v, skipSignal);
        return;
    }
    if (_name == "WriteIO")
    {
        auto& v = std::get<uint64_t>(val);
        writeIO(v, skipSignal);
        return;
    }
}

auto Stats::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ReadIO")
    {
        return readIO();
    }
    if (_name == "WriteIO")
    {
        return writeIO();
    }

    return PropertiesVariant();
}


const vtable_t Stats::_vtable[] = {
    vtable::start(),
    vtable::property("ReadIO",
                     details::Stats::_property_ReadIO
                        .data(),
                     _callback_get_ReadIO,
                     vtable::property_::const_),
    vtable::property("WriteIO",
                     details::Stats::_property_WriteIO
                        .data(),
                     _callback_get_WriteIO,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

