#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/Logging/PEL/Entry/server.hpp>




namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Logging
{
namespace PEL
{
namespace server
{

Entry::Entry(bus_t& bus, const char* path)
        : _org_open_power_Logging_PEL_Entry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entry::Entry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Entry::hidden() const ->
        bool
{
    return _hidden;
}

int Entry::_callback_get_Hidden(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hidden();
                    }
                ));
    }
}

auto Entry::hidden(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hidden != value)
    {
        _hidden = value;
        if (!skipSignal)
        {
            _org_open_power_Logging_PEL_Entry_interface.property_changed("Hidden");
        }
    }

    return _hidden;
}

auto Entry::hidden(bool val) ->
        bool
{
    return hidden(val, false);
}

int Entry::_callback_set_Hidden(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hidden(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Hidden =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Entry::subsystem() const ->
        std::string
{
    return _subsystem;
}

int Entry::_callback_get_Subsystem(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->subsystem();
                    }
                ));
    }
}

auto Entry::subsystem(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_subsystem != value)
    {
        _subsystem = value;
        if (!skipSignal)
        {
            _org_open_power_Logging_PEL_Entry_interface.property_changed("Subsystem");
        }
    }

    return _subsystem;
}

auto Entry::subsystem(std::string val) ->
        std::string
{
    return subsystem(val, false);
}

int Entry::_callback_set_Subsystem(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->subsystem(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Subsystem =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Entry::managementSystemAck() const ->
        bool
{
    return _managementSystemAck;
}

int Entry::_callback_get_ManagementSystemAck(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->managementSystemAck();
                    }
                ));
    }
}

auto Entry::managementSystemAck(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_managementSystemAck != value)
    {
        _managementSystemAck = value;
        if (!skipSignal)
        {
            _org_open_power_Logging_PEL_Entry_interface.property_changed("ManagementSystemAck");
        }
    }

    return _managementSystemAck;
}

auto Entry::managementSystemAck(bool val) ->
        bool
{
    return managementSystemAck(val, false);
}

int Entry::_callback_set_ManagementSystemAck(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->managementSystemAck(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_ManagementSystemAck =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Entry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Hidden")
    {
        auto& v = std::get<bool>(val);
        hidden(v, skipSignal);
        return;
    }
    if (_name == "Subsystem")
    {
        auto& v = std::get<std::string>(val);
        subsystem(v, skipSignal);
        return;
    }
    if (_name == "ManagementSystemAck")
    {
        auto& v = std::get<bool>(val);
        managementSystemAck(v, skipSignal);
        return;
    }
}

auto Entry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Hidden")
    {
        return hidden();
    }
    if (_name == "Subsystem")
    {
        return subsystem();
    }
    if (_name == "ManagementSystemAck")
    {
        return managementSystemAck();
    }

    return PropertiesVariant();
}


const vtable_t Entry::_vtable[] = {
    vtable::start(),
    vtable::property("Hidden",
                     details::Entry::_property_Hidden
                        .data(),
                     _callback_get_Hidden,
                     _callback_set_Hidden,
                     vtable::property_::emits_change),
    vtable::property("Subsystem",
                     details::Entry::_property_Subsystem
                        .data(),
                     _callback_get_Subsystem,
                     _callback_set_Subsystem,
                     vtable::property_::emits_change),
    vtable::property("ManagementSystemAck",
                     details::Entry::_property_ManagementSystemAck
                        .data(),
                     _callback_get_ManagementSystemAck,
                     _callback_set_ManagementSystemAck,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PEL
} // namespace Logging
} // namespace open_power
} // namespace org
} // namespace sdbusplus

