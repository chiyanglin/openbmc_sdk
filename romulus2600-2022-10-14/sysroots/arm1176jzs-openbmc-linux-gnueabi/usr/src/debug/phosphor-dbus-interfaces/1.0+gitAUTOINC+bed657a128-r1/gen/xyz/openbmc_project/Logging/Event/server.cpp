#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Event/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

Event::Event(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Event_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Event::Event(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Event(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Event::timestamp() const ->
        uint64_t
{
    return _timestamp;
}

int Event::_callback_get_Timestamp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Event*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timestamp();
                    }
                ));
    }
}

auto Event::timestamp(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_timestamp != value)
    {
        _timestamp = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Event_interface.property_changed("Timestamp");
        }
    }

    return _timestamp;
}

auto Event::timestamp(uint64_t val) ->
        uint64_t
{
    return timestamp(val, false);
}

int Event::_callback_set_Timestamp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Event*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->timestamp(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Event
{
static const auto _property_Timestamp =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Event::message() const ->
        std::string
{
    return _message;
}

int Event::_callback_get_Message(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Event*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->message();
                    }
                ));
    }
}

auto Event::message(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_message != value)
    {
        _message = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Event_interface.property_changed("Message");
        }
    }

    return _message;
}

auto Event::message(std::string val) ->
        std::string
{
    return message(val, false);
}

int Event::_callback_set_Message(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Event*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->message(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Event
{
static const auto _property_Message =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Event::additionalData() const ->
        std::vector<std::string>
{
    return _additionalData;
}

int Event::_callback_get_AdditionalData(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Event*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->additionalData();
                    }
                ));
    }
}

auto Event::additionalData(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_additionalData != value)
    {
        _additionalData = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Event_interface.property_changed("AdditionalData");
        }
    }

    return _additionalData;
}

auto Event::additionalData(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return additionalData(val, false);
}

int Event::_callback_set_AdditionalData(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Event*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->additionalData(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Event
{
static const auto _property_AdditionalData =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

void Event::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Timestamp")
    {
        auto& v = std::get<uint64_t>(val);
        timestamp(v, skipSignal);
        return;
    }
    if (_name == "Message")
    {
        auto& v = std::get<std::string>(val);
        message(v, skipSignal);
        return;
    }
    if (_name == "AdditionalData")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        additionalData(v, skipSignal);
        return;
    }
}

auto Event::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Timestamp")
    {
        return timestamp();
    }
    if (_name == "Message")
    {
        return message();
    }
    if (_name == "AdditionalData")
    {
        return additionalData();
    }

    return PropertiesVariant();
}


const vtable_t Event::_vtable[] = {
    vtable::start(),
    vtable::property("Timestamp",
                     details::Event::_property_Timestamp
                        .data(),
                     _callback_get_Timestamp,
                     _callback_set_Timestamp,
                     vtable::property_::emits_change),
    vtable::property("Message",
                     details::Event::_property_Message
                        .data(),
                     _callback_get_Message,
                     _callback_set_Message,
                     vtable::property_::emits_change),
    vtable::property("AdditionalData",
                     details::Event::_property_AdditionalData
                        .data(),
                     _callback_get_AdditionalData,
                     _callback_set_AdditionalData,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

