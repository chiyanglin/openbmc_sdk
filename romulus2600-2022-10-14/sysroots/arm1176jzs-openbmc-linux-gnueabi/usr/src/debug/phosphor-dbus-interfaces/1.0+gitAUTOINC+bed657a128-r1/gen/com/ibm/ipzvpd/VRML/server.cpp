#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VRML/server.hpp>








namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VRML::VRML(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VRML_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VRML::VRML(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VRML(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VRML::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VRML::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VRML::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VRML::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VRML::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRML::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int VRML::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto VRML::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto VRML::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int VRML::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRML::pn() const ->
        std::vector<uint8_t>
{
    return _pn;
}

int VRML::_callback_get_PN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pn();
                    }
                ));
    }
}

auto VRML::pn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pn != value)
    {
        _pn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("PN");
        }
    }

    return _pn;
}

auto VRML::pn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pn(val, false);
}

int VRML::_callback_set_PN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_PN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRML::sn() const ->
        std::vector<uint8_t>
{
    return _sn;
}

int VRML::_callback_get_SN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sn();
                    }
                ));
    }
}

auto VRML::sn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_sn != value)
    {
        _sn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("SN");
        }
    }

    return _sn;
}

auto VRML::sn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return sn(val, false);
}

int VRML::_callback_set_SN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->sn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_SN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRML::tv() const ->
        std::vector<uint8_t>
{
    return _tv;
}

int VRML::_callback_get_TV(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tv();
                    }
                ));
    }
}

auto VRML::tv(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tv != value)
    {
        _tv = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("TV");
        }
    }

    return _tv;
}

auto VRML::tv(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tv(val, false);
}

int VRML::_callback_set_TV(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tv(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_TV =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRML::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int VRML::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto VRML::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto VRML::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int VRML::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRML::f5() const ->
        std::vector<uint8_t>
{
    return _f5;
}

int VRML::_callback_get_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f5();
                    }
                ));
    }
}

auto VRML::f5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f5 != value)
    {
        _f5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRML_interface.property_changed("F5");
        }
    }

    return _f5;
}

auto VRML::f5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f5(val, false);
}

int VRML::_callback_set_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRML*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRML
{
static const auto _property_F5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VRML::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "PN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pn(v, skipSignal);
        return;
    }
    if (_name == "SN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        sn(v, skipSignal);
        return;
    }
    if (_name == "TV")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tv(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
    if (_name == "F5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f5(v, skipSignal);
        return;
    }
}

auto VRML::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "PN")
    {
        return pn();
    }
    if (_name == "SN")
    {
        return sn();
    }
    if (_name == "TV")
    {
        return tv();
    }
    if (_name == "D4")
    {
        return d4();
    }
    if (_name == "F5")
    {
        return f5();
    }

    return PropertiesVariant();
}


const vtable_t VRML::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VRML::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::VRML::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("PN",
                     details::VRML::_property_PN
                        .data(),
                     _callback_get_PN,
                     _callback_set_PN,
                     vtable::property_::emits_change),
    vtable::property("SN",
                     details::VRML::_property_SN
                        .data(),
                     _callback_get_SN,
                     _callback_set_SN,
                     vtable::property_::emits_change),
    vtable::property("TV",
                     details::VRML::_property_TV
                        .data(),
                     _callback_get_TV,
                     _callback_set_TV,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::VRML::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::property("F5",
                     details::VRML::_property_F5
                        .data(),
                     _callback_get_F5,
                     _callback_set_F5,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

