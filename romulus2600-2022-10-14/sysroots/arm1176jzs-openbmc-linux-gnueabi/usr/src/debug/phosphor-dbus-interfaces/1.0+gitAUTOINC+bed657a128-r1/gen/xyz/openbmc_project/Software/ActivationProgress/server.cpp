#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/ActivationProgress/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

ActivationProgress::ActivationProgress(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_ActivationProgress_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ActivationProgress::ActivationProgress(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ActivationProgress(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ActivationProgress::progress() const ->
        uint8_t
{
    return _progress;
}

int ActivationProgress::_callback_get_Progress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ActivationProgress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->progress();
                    }
                ));
    }
}

auto ActivationProgress::progress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_progress != value)
    {
        _progress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_ActivationProgress_interface.property_changed("Progress");
        }
    }

    return _progress;
}

auto ActivationProgress::progress(uint8_t val) ->
        uint8_t
{
    return progress(val, false);
}

int ActivationProgress::_callback_set_Progress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ActivationProgress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->progress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ActivationProgress
{
static const auto _property_Progress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void ActivationProgress::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Progress")
    {
        auto& v = std::get<uint8_t>(val);
        progress(v, skipSignal);
        return;
    }
}

auto ActivationProgress::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Progress")
    {
        return progress();
    }

    return PropertiesVariant();
}


const vtable_t ActivationProgress::_vtable[] = {
    vtable::start(),
    vtable::property("Progress",
                     details::ActivationProgress::_property_Progress
                        .data(),
                     _callback_get_Progress,
                     _callback_set_Progress,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

