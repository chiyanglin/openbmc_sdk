#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Processor/CurrentOperatingConfig/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/Device/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/Device/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Processor
{
namespace server
{

CurrentOperatingConfig::CurrentOperatingConfig(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Processor_CurrentOperatingConfig_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CurrentOperatingConfig::CurrentOperatingConfig(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CurrentOperatingConfig(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CurrentOperatingConfig::appliedConfig() const ->
        sdbusplus::message::object_path
{
    return _appliedConfig;
}

int CurrentOperatingConfig::_callback_get_AppliedConfig(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CurrentOperatingConfig*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->appliedConfig();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Device::Error::WriteFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto CurrentOperatingConfig::appliedConfig(sdbusplus::message::object_path value,
                                         bool skipSignal) ->
        sdbusplus::message::object_path
{
    if (_appliedConfig != value)
    {
        _appliedConfig = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Processor_CurrentOperatingConfig_interface.property_changed("AppliedConfig");
        }
    }

    return _appliedConfig;
}

auto CurrentOperatingConfig::appliedConfig(sdbusplus::message::object_path val) ->
        sdbusplus::message::object_path
{
    return appliedConfig(val, false);
}

int CurrentOperatingConfig::_callback_set_AppliedConfig(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CurrentOperatingConfig*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& arg)
                    {
                        o->appliedConfig(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Device::Error::WriteFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace CurrentOperatingConfig
{
static const auto _property_AppliedConfig =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::message::object_path>());
}
}

auto CurrentOperatingConfig::baseSpeedPriorityEnabled() const ->
        bool
{
    return _baseSpeedPriorityEnabled;
}

int CurrentOperatingConfig::_callback_get_BaseSpeedPriorityEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CurrentOperatingConfig*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->baseSpeedPriorityEnabled();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Device::Error::WriteFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto CurrentOperatingConfig::baseSpeedPriorityEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_baseSpeedPriorityEnabled != value)
    {
        _baseSpeedPriorityEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Processor_CurrentOperatingConfig_interface.property_changed("BaseSpeedPriorityEnabled");
        }
    }

    return _baseSpeedPriorityEnabled;
}

auto CurrentOperatingConfig::baseSpeedPriorityEnabled(bool val) ->
        bool
{
    return baseSpeedPriorityEnabled(val, false);
}

int CurrentOperatingConfig::_callback_set_BaseSpeedPriorityEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CurrentOperatingConfig*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->baseSpeedPriorityEnabled(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Device::Error::WriteFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace CurrentOperatingConfig
{
static const auto _property_BaseSpeedPriorityEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void CurrentOperatingConfig::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AppliedConfig")
    {
        auto& v = std::get<sdbusplus::message::object_path>(val);
        appliedConfig(v, skipSignal);
        return;
    }
    if (_name == "BaseSpeedPriorityEnabled")
    {
        auto& v = std::get<bool>(val);
        baseSpeedPriorityEnabled(v, skipSignal);
        return;
    }
}

auto CurrentOperatingConfig::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AppliedConfig")
    {
        return appliedConfig();
    }
    if (_name == "BaseSpeedPriorityEnabled")
    {
        return baseSpeedPriorityEnabled();
    }

    return PropertiesVariant();
}


const vtable_t CurrentOperatingConfig::_vtable[] = {
    vtable::start(),
    vtable::property("AppliedConfig",
                     details::CurrentOperatingConfig::_property_AppliedConfig
                        .data(),
                     _callback_get_AppliedConfig,
                     _callback_set_AppliedConfig,
                     vtable::property_::emits_change),
    vtable::property("BaseSpeedPriorityEnabled",
                     details::CurrentOperatingConfig::_property_BaseSpeedPriorityEnabled
                        .data(),
                     _callback_get_BaseSpeedPriorityEnabled,
                     _callback_set_BaseSpeedPriorityEnabled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Processor
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

