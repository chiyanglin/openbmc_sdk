#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>












#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Cpu
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Cpu() = delete;
        Cpu(const Cpu&) = delete;
        Cpu& operator=(const Cpu&) = delete;
        Cpu(Cpu&&) = delete;
        Cpu& operator=(Cpu&&) = delete;
        virtual ~Cpu() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Cpu(bus_t& bus, const char* path);

        enum class Capability
        {
            Capable64bit,
            MultiCore,
            HardwareThread,
            ExecuteProtection,
            EnhancedVirtualization,
            PowerPerformanceControl,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::string,
                std::vector<Capability>,
                uint16_t,
                uint32_t,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Cpu(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Socket */
        virtual std::string socket() const;
        /** Set value of Socket with option to skip sending signal */
        virtual std::string socket(std::string value,
               bool skipSignal);
        /** Set value of Socket */
        virtual std::string socket(std::string value);
        /** Get value of Family */
        virtual std::string family() const;
        /** Set value of Family with option to skip sending signal */
        virtual std::string family(std::string value,
               bool skipSignal);
        /** Set value of Family */
        virtual std::string family(std::string value);
        /** Get value of EffectiveFamily */
        virtual uint16_t effectiveFamily() const;
        /** Set value of EffectiveFamily with option to skip sending signal */
        virtual uint16_t effectiveFamily(uint16_t value,
               bool skipSignal);
        /** Set value of EffectiveFamily */
        virtual uint16_t effectiveFamily(uint16_t value);
        /** Get value of EffectiveModel */
        virtual uint16_t effectiveModel() const;
        /** Set value of EffectiveModel with option to skip sending signal */
        virtual uint16_t effectiveModel(uint16_t value,
               bool skipSignal);
        /** Set value of EffectiveModel */
        virtual uint16_t effectiveModel(uint16_t value);
        /** Get value of Id */
        virtual uint64_t id() const;
        /** Set value of Id with option to skip sending signal */
        virtual uint64_t id(uint64_t value,
               bool skipSignal);
        /** Set value of Id */
        virtual uint64_t id(uint64_t value);
        /** Get value of MaxSpeedInMhz */
        virtual uint32_t maxSpeedInMhz() const;
        /** Set value of MaxSpeedInMhz with option to skip sending signal */
        virtual uint32_t maxSpeedInMhz(uint32_t value,
               bool skipSignal);
        /** Set value of MaxSpeedInMhz */
        virtual uint32_t maxSpeedInMhz(uint32_t value);
        /** Get value of Characteristics */
        virtual std::vector<Capability> characteristics() const;
        /** Set value of Characteristics with option to skip sending signal */
        virtual std::vector<Capability> characteristics(std::vector<Capability> value,
               bool skipSignal);
        /** Set value of Characteristics */
        virtual std::vector<Capability> characteristics(std::vector<Capability> value);
        /** Get value of CoreCount */
        virtual uint16_t coreCount() const;
        /** Set value of CoreCount with option to skip sending signal */
        virtual uint16_t coreCount(uint16_t value,
               bool skipSignal);
        /** Set value of CoreCount */
        virtual uint16_t coreCount(uint16_t value);
        /** Get value of ThreadCount */
        virtual uint16_t threadCount() const;
        /** Set value of ThreadCount with option to skip sending signal */
        virtual uint16_t threadCount(uint16_t value,
               bool skipSignal);
        /** Set value of ThreadCount */
        virtual uint16_t threadCount(uint16_t value);
        /** Get value of Step */
        virtual uint16_t step() const;
        /** Set value of Step with option to skip sending signal */
        virtual uint16_t step(uint16_t value,
               bool skipSignal);
        /** Set value of Step */
        virtual uint16_t step(uint16_t value);
        /** Get value of Microcode */
        virtual uint32_t microcode() const;
        /** Set value of Microcode with option to skip sending signal */
        virtual uint32_t microcode(uint32_t value,
               bool skipSignal);
        /** Set value of Microcode */
        virtual uint32_t microcode(uint32_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Cpu.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Capability convertCapabilityFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Cpu.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Capability> convertStringToCapability(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Cpu.<value name>"
         */
        static std::string convertCapabilityToString(Capability e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Cpu";

    private:

        /** @brief sd-bus callback for get-property 'Socket' */
        static int _callback_get_Socket(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Socket' */
        static int _callback_set_Socket(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Family' */
        static int _callback_get_Family(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Family' */
        static int _callback_set_Family(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EffectiveFamily' */
        static int _callback_get_EffectiveFamily(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EffectiveFamily' */
        static int _callback_set_EffectiveFamily(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EffectiveModel' */
        static int _callback_get_EffectiveModel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EffectiveModel' */
        static int _callback_set_EffectiveModel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Id' */
        static int _callback_get_Id(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Id' */
        static int _callback_set_Id(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxSpeedInMhz' */
        static int _callback_get_MaxSpeedInMhz(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxSpeedInMhz' */
        static int _callback_set_MaxSpeedInMhz(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Characteristics' */
        static int _callback_get_Characteristics(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Characteristics' */
        static int _callback_set_Characteristics(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CoreCount' */
        static int _callback_get_CoreCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CoreCount' */
        static int _callback_set_CoreCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ThreadCount' */
        static int _callback_get_ThreadCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ThreadCount' */
        static int _callback_set_ThreadCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Step' */
        static int _callback_get_Step(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Step' */
        static int _callback_set_Step(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Microcode' */
        static int _callback_get_Microcode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Microcode' */
        static int _callback_set_Microcode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Cpu_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _socket = "";
        std::string _family = "";
        uint16_t _effectiveFamily = 2;
        uint16_t _effectiveModel = 0;
        uint64_t _id = 0;
        uint32_t _maxSpeedInMhz{};
        std::vector<Capability> _characteristics{};
        uint16_t _coreCount{};
        uint16_t _threadCount{};
        uint16_t _step = 0;
        uint32_t _microcode = 0;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Cpu::Capability.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Cpu::Capability e)
{
    return Cpu::convertCapabilityToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Cpu::Capability>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Cpu::convertStringToCapability(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Cpu::Capability>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Cpu::Capability value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Cpu::convertCapabilityToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

