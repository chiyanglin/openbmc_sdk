#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Led
{
namespace server
{

class Physical
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Physical() = delete;
        Physical(const Physical&) = delete;
        Physical& operator=(const Physical&) = delete;
        Physical(Physical&&) = delete;
        Physical& operator=(Physical&&) = delete;
        virtual ~Physical() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Physical(bus_t& bus, const char* path);

        enum class Action
        {
            Off,
            On,
            Blink,
        };
        enum class Palette
        {
            Unknown,
            Red,
            Green,
            Blue,
            Yellow,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Action,
                Palette,
                uint16_t,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Physical(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of State */
        virtual Action state() const;
        /** Set value of State with option to skip sending signal */
        virtual Action state(Action value,
               bool skipSignal);
        /** Set value of State */
        virtual Action state(Action value);
        /** Get value of DutyOn */
        virtual uint8_t dutyOn() const;
        /** Set value of DutyOn with option to skip sending signal */
        virtual uint8_t dutyOn(uint8_t value,
               bool skipSignal);
        /** Set value of DutyOn */
        virtual uint8_t dutyOn(uint8_t value);
        /** Get value of Color */
        virtual Palette color() const;
        /** Set value of Color with option to skip sending signal */
        virtual Palette color(Palette value,
               bool skipSignal);
        /** Set value of Color */
        virtual Palette color(Palette value);
        /** Get value of Period */
        virtual uint16_t period() const;
        /** Set value of Period with option to skip sending signal */
        virtual uint16_t period(uint16_t value,
               bool skipSignal);
        /** Set value of Period */
        virtual uint16_t period(uint16_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Led.Physical.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Action convertActionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Led.Physical.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Action> convertStringToAction(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Led.Physical.<value name>"
         */
        static std::string convertActionToString(Action e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Led.Physical.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Palette convertPaletteFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Led.Physical.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Palette> convertStringToPalette(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Led.Physical.<value name>"
         */
        static std::string convertPaletteToString(Palette e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Led_Physical_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Led_Physical_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Led.Physical";

    private:

        /** @brief sd-bus callback for get-property 'State' */
        static int _callback_get_State(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'State' */
        static int _callback_set_State(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DutyOn' */
        static int _callback_get_DutyOn(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DutyOn' */
        static int _callback_set_DutyOn(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Color' */
        static int _callback_get_Color(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Color' */
        static int _callback_set_Color(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Period' */
        static int _callback_get_Period(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Period' */
        static int _callback_set_Period(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Led_Physical_interface;
        sdbusplus::SdBusInterface *_intf;

        Action _state = Action::Off;
        uint8_t _dutyOn = 50;
        Palette _color = Palette::Unknown;
        uint16_t _period = 1000;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Physical::Action.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Physical::Action e)
{
    return Physical::convertActionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Physical::Palette.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Physical::Palette e)
{
    return Physical::convertPaletteToString(e);
}

} // namespace server
} // namespace Led
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Led::server::Physical::Action>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Led::server::Physical::convertStringToAction(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Led::server::Physical::Action>
{
    static std::string op(xyz::openbmc_project::Led::server::Physical::Action value)
    {
        return xyz::openbmc_project::Led::server::Physical::convertActionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Led::server::Physical::Palette>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Led::server::Physical::convertStringToPalette(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Led::server::Physical::Palette>
{
    static std::string op(xyz::openbmc_project::Led::server::Physical::Palette value)
    {
        return xyz::openbmc_project::Led::server::Physical::convertPaletteToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

