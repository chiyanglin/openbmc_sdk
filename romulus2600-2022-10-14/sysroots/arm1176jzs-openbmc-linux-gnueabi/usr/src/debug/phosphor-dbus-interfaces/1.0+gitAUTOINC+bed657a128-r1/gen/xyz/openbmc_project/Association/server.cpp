#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Association/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace server
{

Association::Association(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Association_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Association::Association(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Association(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Association::endpoints() const ->
        std::vector<sdbusplus::message::object_path>
{
    return _endpoints;
}

int Association::_callback_get_Endpoints(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Association*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->endpoints();
                    }
                ));
    }
}

auto Association::endpoints(std::vector<sdbusplus::message::object_path> value,
                                         bool skipSignal) ->
        std::vector<sdbusplus::message::object_path>
{
    if (_endpoints != value)
    {
        _endpoints = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Association_interface.property_changed("Endpoints");
        }
    }

    return _endpoints;
}

auto Association::endpoints(std::vector<sdbusplus::message::object_path> val) ->
        std::vector<sdbusplus::message::object_path>
{
    return endpoints(val, false);
}

int Association::_callback_set_Endpoints(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Association*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<sdbusplus::message::object_path>&& arg)
                    {
                        o->endpoints(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Association
{
static const auto _property_Endpoints =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::message::object_path>>());
}
}

void Association::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Endpoints")
    {
        auto& v = std::get<std::vector<sdbusplus::message::object_path>>(val);
        endpoints(v, skipSignal);
        return;
    }
}

auto Association::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Endpoints")
    {
        return endpoints();
    }

    return PropertiesVariant();
}


const vtable_t Association::_vtable[] = {
    vtable::start(),
    vtable::property("Endpoints",
                     details::Association::_property_Endpoints
                        .data(),
                     _callback_get_Endpoints,
                     _callback_set_Endpoints,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

