#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Boot/Type/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace server
{

Type::Type(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Boot_Type_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Type::Type(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Type(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Type::bootType() const ->
        Types
{
    return _bootType;
}

int Type::_callback_get_BootType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Type*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bootType();
                    }
                ));
    }
}

auto Type::bootType(Types value,
                                         bool skipSignal) ->
        Types
{
    if (_bootType != value)
    {
        _bootType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Boot_Type_interface.property_changed("BootType");
        }
    }

    return _bootType;
}

auto Type::bootType(Types val) ->
        Types
{
    return bootType(val, false);
}

int Type::_callback_set_BootType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Type*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Types&& arg)
                    {
                        o->bootType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Type
{
static const auto _property_BootType =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Boot::server::Type::Types>());
}
}

void Type::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BootType")
    {
        auto& v = std::get<Types>(val);
        bootType(v, skipSignal);
        return;
    }
}

auto Type::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BootType")
    {
        return bootType();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Type::Types */
static const std::tuple<const char*, Type::Types> mappingTypeTypes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Type.Types.Legacy",                 Type::Types::Legacy ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Type.Types.EFI",                 Type::Types::EFI ),
        };

} // anonymous namespace

auto Type::convertStringToTypes(const std::string& s) noexcept ->
        std::optional<Types>
{
    auto i = std::find_if(
            std::begin(mappingTypeTypes),
            std::end(mappingTypeTypes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingTypeTypes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Type::convertTypesFromString(const std::string& s) ->
        Types
{
    auto r = convertStringToTypes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Type::convertTypesToString(Type::Types v)
{
    auto i = std::find_if(
            std::begin(mappingTypeTypes),
            std::end(mappingTypeTypes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingTypeTypes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Type::_vtable[] = {
    vtable::start(),
    vtable::property("BootType",
                     details::Type::_property_BootType
                        .data(),
                     _callback_get_BootType,
                     _callback_set_BootType,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

