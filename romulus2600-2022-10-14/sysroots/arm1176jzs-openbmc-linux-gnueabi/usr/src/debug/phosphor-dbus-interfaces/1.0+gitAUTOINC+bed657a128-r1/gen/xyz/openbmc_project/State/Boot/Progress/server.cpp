#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Boot/Progress/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace server
{

Progress::Progress(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Boot_Progress_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Progress::Progress(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Progress(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Progress::bootProgress() const ->
        ProgressStages
{
    return _bootProgress;
}

int Progress::_callback_get_BootProgress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bootProgress();
                    }
                ));
    }
}

auto Progress::bootProgress(ProgressStages value,
                                         bool skipSignal) ->
        ProgressStages
{
    if (_bootProgress != value)
    {
        _bootProgress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Boot_Progress_interface.property_changed("BootProgress");
        }
    }

    return _bootProgress;
}

auto Progress::bootProgress(ProgressStages val) ->
        ProgressStages
{
    return bootProgress(val, false);
}

int Progress::_callback_set_BootProgress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ProgressStages&& arg)
                    {
                        o->bootProgress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Progress
{
static const auto _property_BootProgress =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::Boot::server::Progress::ProgressStages>());
}
}

auto Progress::bootProgressLastUpdate() const ->
        uint64_t
{
    return _bootProgressLastUpdate;
}

int Progress::_callback_get_BootProgressLastUpdate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bootProgressLastUpdate();
                    }
                ));
    }
}

auto Progress::bootProgressLastUpdate(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_bootProgressLastUpdate != value)
    {
        _bootProgressLastUpdate = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Boot_Progress_interface.property_changed("BootProgressLastUpdate");
        }
    }

    return _bootProgressLastUpdate;
}

auto Progress::bootProgressLastUpdate(uint64_t val) ->
        uint64_t
{
    return bootProgressLastUpdate(val, false);
}

int Progress::_callback_set_BootProgressLastUpdate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->bootProgressLastUpdate(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Progress
{
static const auto _property_BootProgressLastUpdate =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Progress::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BootProgress")
    {
        auto& v = std::get<ProgressStages>(val);
        bootProgress(v, skipSignal);
        return;
    }
    if (_name == "BootProgressLastUpdate")
    {
        auto& v = std::get<uint64_t>(val);
        bootProgressLastUpdate(v, skipSignal);
        return;
    }
}

auto Progress::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BootProgress")
    {
        return bootProgress();
    }
    if (_name == "BootProgressLastUpdate")
    {
        return bootProgressLastUpdate();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Progress::ProgressStages */
static const std::tuple<const char*, Progress::ProgressStages> mappingProgressProgressStages[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.Unspecified",                 Progress::ProgressStages::Unspecified ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.PrimaryProcInit",                 Progress::ProgressStages::PrimaryProcInit ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.BusInit",                 Progress::ProgressStages::BusInit ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.MemoryInit",                 Progress::ProgressStages::MemoryInit ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.SecondaryProcInit",                 Progress::ProgressStages::SecondaryProcInit ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.PCIInit",                 Progress::ProgressStages::PCIInit ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.SystemInitComplete",                 Progress::ProgressStages::SystemInitComplete ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.OSStart",                 Progress::ProgressStages::OSStart ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.OSRunning",                 Progress::ProgressStages::OSRunning ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.SystemSetup",                 Progress::ProgressStages::SystemSetup ),
            std::make_tuple( "xyz.openbmc_project.State.Boot.Progress.ProgressStages.MotherboardInit",                 Progress::ProgressStages::MotherboardInit ),
        };

} // anonymous namespace

auto Progress::convertStringToProgressStages(const std::string& s) noexcept ->
        std::optional<ProgressStages>
{
    auto i = std::find_if(
            std::begin(mappingProgressProgressStages),
            std::end(mappingProgressProgressStages),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingProgressProgressStages) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Progress::convertProgressStagesFromString(const std::string& s) ->
        ProgressStages
{
    auto r = convertStringToProgressStages(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Progress::convertProgressStagesToString(Progress::ProgressStages v)
{
    auto i = std::find_if(
            std::begin(mappingProgressProgressStages),
            std::end(mappingProgressProgressStages),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingProgressProgressStages))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Progress::_vtable[] = {
    vtable::start(),
    vtable::property("BootProgress",
                     details::Progress::_property_BootProgress
                        .data(),
                     _callback_get_BootProgress,
                     _callback_set_BootProgress,
                     vtable::property_::emits_change),
    vtable::property("BootProgressLastUpdate",
                     details::Progress::_property_BootProgressLastUpdate
                        .data(),
                     _callback_get_BootProgressLastUpdate,
                     _callback_set_BootProgressLastUpdate,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

