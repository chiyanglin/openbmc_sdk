#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/VirtualMedia/MountPoint/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

MountPoint::MountPoint(bus_t& bus, const char* path)
        : _xyz_openbmc_project_VirtualMedia_MountPoint_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MountPoint::MountPoint(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MountPoint(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MountPoint::endPointId() const ->
        std::string
{
    return _endPointId;
}

int MountPoint::_callback_get_EndPointId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->endPointId();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::endPointId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_endPointId != value)
    {
        _endPointId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("EndPointId");
        }
    }

    return _endPointId;
}

auto MountPoint::endPointId(std::string val) ->
        std::string
{
    return endPointId(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_EndPointId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto MountPoint::mode() const ->
        uint8_t
{
    return _mode;
}

int MountPoint::_callback_get_Mode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mode();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::mode(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_mode != value)
    {
        _mode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("Mode");
        }
    }

    return _mode;
}

auto MountPoint::mode(uint8_t val) ->
        uint8_t
{
    return mode(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_Mode =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto MountPoint::device() const ->
        std::string
{
    return _device;
}

int MountPoint::_callback_get_Device(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->device();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::device(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_device != value)
    {
        _device = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("Device");
        }
    }

    return _device;
}

auto MountPoint::device(std::string val) ->
        std::string
{
    return device(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_Device =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto MountPoint::socket() const ->
        std::string
{
    return _socket;
}

int MountPoint::_callback_get_Socket(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->socket();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::socket(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_socket != value)
    {
        _socket = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("Socket");
        }
    }

    return _socket;
}

auto MountPoint::socket(std::string val) ->
        std::string
{
    return socket(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_Socket =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto MountPoint::timeout() const ->
        uint16_t
{
    return _timeout;
}

int MountPoint::_callback_get_Timeout(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timeout();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::timeout(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_timeout != value)
    {
        _timeout = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("Timeout");
        }
    }

    return _timeout;
}

auto MountPoint::timeout(uint16_t val) ->
        uint16_t
{
    return timeout(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_Timeout =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto MountPoint::blockSize() const ->
        uint16_t
{
    return _blockSize;
}

int MountPoint::_callback_get_BlockSize(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->blockSize();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::blockSize(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_blockSize != value)
    {
        _blockSize = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("BlockSize");
        }
    }

    return _blockSize;
}

auto MountPoint::blockSize(uint16_t val) ->
        uint16_t
{
    return blockSize(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_BlockSize =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto MountPoint::remainingInactivityTimeout() const ->
        uint16_t
{
    return _remainingInactivityTimeout;
}

int MountPoint::_callback_get_RemainingInactivityTimeout(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->remainingInactivityTimeout();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::remainingInactivityTimeout(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_remainingInactivityTimeout != value)
    {
        _remainingInactivityTimeout = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("RemainingInactivityTimeout");
        }
    }

    return _remainingInactivityTimeout;
}

auto MountPoint::remainingInactivityTimeout(uint16_t val) ->
        uint16_t
{
    return remainingInactivityTimeout(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_RemainingInactivityTimeout =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto MountPoint::imageURL() const ->
        std::string
{
    return _imageURL;
}

int MountPoint::_callback_get_ImageURL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->imageURL();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::imageURL(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_imageURL != value)
    {
        _imageURL = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("ImageURL");
        }
    }

    return _imageURL;
}

auto MountPoint::imageURL(std::string val) ->
        std::string
{
    return imageURL(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_ImageURL =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto MountPoint::writeProtected() const ->
        bool
{
    return _writeProtected;
}

int MountPoint::_callback_get_WriteProtected(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MountPoint*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->writeProtected();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MountPoint::writeProtected(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_writeProtected != value)
    {
        _writeProtected = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_MountPoint_interface.property_changed("WriteProtected");
        }
    }

    return _writeProtected;
}

auto MountPoint::writeProtected(bool val) ->
        bool
{
    return writeProtected(val, false);
}


namespace details
{
namespace MountPoint
{
static const auto _property_WriteProtected =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void MountPoint::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "EndPointId")
    {
        auto& v = std::get<std::string>(val);
        endPointId(v, skipSignal);
        return;
    }
    if (_name == "Mode")
    {
        auto& v = std::get<uint8_t>(val);
        mode(v, skipSignal);
        return;
    }
    if (_name == "Device")
    {
        auto& v = std::get<std::string>(val);
        device(v, skipSignal);
        return;
    }
    if (_name == "Socket")
    {
        auto& v = std::get<std::string>(val);
        socket(v, skipSignal);
        return;
    }
    if (_name == "Timeout")
    {
        auto& v = std::get<uint16_t>(val);
        timeout(v, skipSignal);
        return;
    }
    if (_name == "BlockSize")
    {
        auto& v = std::get<uint16_t>(val);
        blockSize(v, skipSignal);
        return;
    }
    if (_name == "RemainingInactivityTimeout")
    {
        auto& v = std::get<uint16_t>(val);
        remainingInactivityTimeout(v, skipSignal);
        return;
    }
    if (_name == "ImageURL")
    {
        auto& v = std::get<std::string>(val);
        imageURL(v, skipSignal);
        return;
    }
    if (_name == "WriteProtected")
    {
        auto& v = std::get<bool>(val);
        writeProtected(v, skipSignal);
        return;
    }
}

auto MountPoint::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "EndPointId")
    {
        return endPointId();
    }
    if (_name == "Mode")
    {
        return mode();
    }
    if (_name == "Device")
    {
        return device();
    }
    if (_name == "Socket")
    {
        return socket();
    }
    if (_name == "Timeout")
    {
        return timeout();
    }
    if (_name == "BlockSize")
    {
        return blockSize();
    }
    if (_name == "RemainingInactivityTimeout")
    {
        return remainingInactivityTimeout();
    }
    if (_name == "ImageURL")
    {
        return imageURL();
    }
    if (_name == "WriteProtected")
    {
        return writeProtected();
    }

    return PropertiesVariant();
}


const vtable_t MountPoint::_vtable[] = {
    vtable::start(),
    vtable::property("EndPointId",
                     details::MountPoint::_property_EndPointId
                        .data(),
                     _callback_get_EndPointId,
                     vtable::property_::const_),
    vtable::property("Mode",
                     details::MountPoint::_property_Mode
                        .data(),
                     _callback_get_Mode,
                     vtable::property_::const_),
    vtable::property("Device",
                     details::MountPoint::_property_Device
                        .data(),
                     _callback_get_Device,
                     vtable::property_::const_),
    vtable::property("Socket",
                     details::MountPoint::_property_Socket
                        .data(),
                     _callback_get_Socket,
                     vtable::property_::const_),
    vtable::property("Timeout",
                     details::MountPoint::_property_Timeout
                        .data(),
                     _callback_get_Timeout,
                     vtable::property_::const_),
    vtable::property("BlockSize",
                     details::MountPoint::_property_BlockSize
                        .data(),
                     _callback_get_BlockSize,
                     vtable::property_::const_),
    vtable::property("RemainingInactivityTimeout",
                     details::MountPoint::_property_RemainingInactivityTimeout
                        .data(),
                     _callback_get_RemainingInactivityTimeout,
                     vtable::property_::const_),
    vtable::property("ImageURL",
                     details::MountPoint::_property_ImageURL
                        .data(),
                     _callback_get_ImageURL,
                     vtable::property_::const_),
    vtable::property("WriteProtected",
                     details::MountPoint::_property_WriteProtected
                        .data(),
                     _callback_get_WriteProtected,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

