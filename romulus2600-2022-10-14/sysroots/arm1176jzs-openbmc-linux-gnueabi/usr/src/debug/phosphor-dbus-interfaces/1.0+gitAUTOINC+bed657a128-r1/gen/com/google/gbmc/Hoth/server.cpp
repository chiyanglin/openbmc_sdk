#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/google/gbmc/Hoth/server.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>


#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>


#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>


#include <com/google/gbmc/Hoth/error.hpp>


#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>

#include <com/google/gbmc/Hoth/error.hpp>
#include <com/google/gbmc/Hoth/error.hpp>


namespace sdbusplus
{
namespace com
{
namespace google
{
namespace gbmc
{
namespace server
{

Hoth::Hoth(bus_t& bus, const char* path)
        : _com_google_gbmc_Hoth_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Hoth::_callback_SendHostCommand(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& command)
                    {
                        return o->sendHostCommand(
                                command);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Timeout& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_SendHostCommand =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
static const auto _return_SendHostCommand =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int Hoth::_callback_SendTrustedHostCommand(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& command)
                    {
                        return o->sendTrustedHostCommand(
                                command);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Timeout& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_SendTrustedHostCommand =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
static const auto _return_SendTrustedHostCommand =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int Hoth::_callback_SendHostCommandAsync(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& command)
                    {
                        return o->sendHostCommandAsync(
                                command);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_SendHostCommandAsync =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
static const auto _return_SendHostCommandAsync =
        utility::tuple_to_array(message::types::type_id<
                uint64_t>());
}
}

int Hoth::_callback_GetHostCommandResponse(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint64_t&& callToken)
                    {
                        return o->getHostCommandResponse(
                                callToken);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetHostCommandResponse =
        utility::tuple_to_array(message::types::type_id<
                uint64_t>());
static const auto _return_GetHostCommandResponse =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int Hoth::_callback_UpdateFirmware(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& firmwareData)
                    {
                        return o->updateFirmware(
                                firmwareData);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::FirmwareFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_UpdateFirmware =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
static const auto _return_UpdateFirmware =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_GetFirmwareUpdateStatus(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getFirmwareUpdateStatus(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetFirmwareUpdateStatus =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetFirmwareUpdateStatus =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::com::google::gbmc::server::Hoth::FirmwareUpdateStatus>());
}
}

int Hoth::_callback_InitiatePayload(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->initiatePayload(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_InitiatePayload =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_InitiatePayload =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_GetInitiatePayloadStatus(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getInitiatePayloadStatus(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetInitiatePayloadStatus =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetInitiatePayloadStatus =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::com::google::gbmc::server::Hoth::FirmwareUpdateStatus>());
}
}

int Hoth::_callback_ErasePayload(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& offset, uint32_t&& size)
                    {
                        return o->erasePayload(
                                offset, size);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_ErasePayload =
        utility::tuple_to_array(message::types::type_id<
                uint32_t, uint32_t>());
static const auto _return_ErasePayload =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_SendPayload(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& imagePath)
                    {
                        return o->sendPayload(
                                imagePath);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::FirmwareFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_SendPayload =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_SendPayload =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_GetSendPayloadStatus(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getSendPayloadStatus(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetSendPayloadStatus =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetSendPayloadStatus =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::com::google::gbmc::server::Hoth::FirmwareUpdateStatus>());
}
}

int Hoth::_callback_VerifyPayload(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->verifyPayload(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_VerifyPayload =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_VerifyPayload =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_GetVerifyPayloadStatus(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getVerifyPayloadStatus(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetVerifyPayloadStatus =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetVerifyPayloadStatus =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::com::google::gbmc::server::Hoth::FirmwareUpdateStatus>());
}
}

int Hoth::_callback_ActivatePayload(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](bool&& makePersistent)
                    {
                        return o->activatePayload(
                                makePersistent);
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_ActivatePayload =
        utility::tuple_to_array(message::types::type_id<
                bool>());
static const auto _return_ActivatePayload =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_GetPayloadSize(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getPayloadSize(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetPayloadSize =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetPayloadSize =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int Hoth::_callback_Confirm(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->confirm(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::CommandFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::InterfaceError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_Confirm =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Confirm =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Hoth::_callback_GetTotalBootTime(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getTotalBootTime(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ExpectedInfoNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetTotalBootTime =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetTotalBootTime =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int Hoth::_callback_GetFirmwareUpdateTime(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getFirmwareUpdateTime(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ExpectedInfoNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetFirmwareUpdateTime =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetFirmwareUpdateTime =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int Hoth::_callback_GetFirmwareMirroringTime(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getFirmwareMirroringTime(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ExpectedInfoNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetFirmwareMirroringTime =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetFirmwareMirroringTime =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int Hoth::_callback_GetPayloadValidationTime(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Hoth*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getPayloadValidationTime(
                                );
                    }
                ));
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ResponseFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::com::google::gbmc::Hoth::Error::ExpectedInfoNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Hoth
{
static const auto _param_GetPayloadValidationTime =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetPayloadValidationTime =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}


void Hoth::hostCommandResponseReady(
            uint64_t callToken)
{
    auto& i = _com_google_gbmc_Hoth_interface;
    auto m = i.new_signal("HostCommandResponseReady");

    m.append(callToken);
    m.signal_send();
}

namespace details
{
namespace Hoth
{
static const auto _signal_HostCommandResponseReady =
        utility::tuple_to_array(message::types::type_id<
                uint64_t>());
}
}



namespace
{
/** String to enum mapping for Hoth::FirmwareUpdateStatus */
static const std::tuple<const char*, Hoth::FirmwareUpdateStatus> mappingHothFirmwareUpdateStatus[] =
        {
            std::make_tuple( "com.google.gbmc.Hoth.FirmwareUpdateStatus.None",                 Hoth::FirmwareUpdateStatus::None ),
            std::make_tuple( "com.google.gbmc.Hoth.FirmwareUpdateStatus.InProgress",                 Hoth::FirmwareUpdateStatus::InProgress ),
            std::make_tuple( "com.google.gbmc.Hoth.FirmwareUpdateStatus.Error",                 Hoth::FirmwareUpdateStatus::Error ),
            std::make_tuple( "com.google.gbmc.Hoth.FirmwareUpdateStatus.Done",                 Hoth::FirmwareUpdateStatus::Done ),
        };

} // anonymous namespace

auto Hoth::convertStringToFirmwareUpdateStatus(const std::string& s) noexcept ->
        std::optional<FirmwareUpdateStatus>
{
    auto i = std::find_if(
            std::begin(mappingHothFirmwareUpdateStatus),
            std::end(mappingHothFirmwareUpdateStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHothFirmwareUpdateStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Hoth::convertFirmwareUpdateStatusFromString(const std::string& s) ->
        FirmwareUpdateStatus
{
    auto r = convertStringToFirmwareUpdateStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Hoth::convertFirmwareUpdateStatusToString(Hoth::FirmwareUpdateStatus v)
{
    auto i = std::find_if(
            std::begin(mappingHothFirmwareUpdateStatus),
            std::end(mappingHothFirmwareUpdateStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHothFirmwareUpdateStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Hoth::_vtable[] = {
    vtable::start(),

    vtable::method("SendHostCommand",
                   details::Hoth::_param_SendHostCommand
                        .data(),
                   details::Hoth::_return_SendHostCommand
                        .data(),
                   _callback_SendHostCommand),

    vtable::method("SendTrustedHostCommand",
                   details::Hoth::_param_SendTrustedHostCommand
                        .data(),
                   details::Hoth::_return_SendTrustedHostCommand
                        .data(),
                   _callback_SendTrustedHostCommand),

    vtable::method("SendHostCommandAsync",
                   details::Hoth::_param_SendHostCommandAsync
                        .data(),
                   details::Hoth::_return_SendHostCommandAsync
                        .data(),
                   _callback_SendHostCommandAsync),

    vtable::method("GetHostCommandResponse",
                   details::Hoth::_param_GetHostCommandResponse
                        .data(),
                   details::Hoth::_return_GetHostCommandResponse
                        .data(),
                   _callback_GetHostCommandResponse),

    vtable::method("UpdateFirmware",
                   details::Hoth::_param_UpdateFirmware
                        .data(),
                   details::Hoth::_return_UpdateFirmware
                        .data(),
                   _callback_UpdateFirmware),

    vtable::method("GetFirmwareUpdateStatus",
                   details::Hoth::_param_GetFirmwareUpdateStatus
                        .data(),
                   details::Hoth::_return_GetFirmwareUpdateStatus
                        .data(),
                   _callback_GetFirmwareUpdateStatus),

    vtable::method("InitiatePayload",
                   details::Hoth::_param_InitiatePayload
                        .data(),
                   details::Hoth::_return_InitiatePayload
                        .data(),
                   _callback_InitiatePayload),

    vtable::method("GetInitiatePayloadStatus",
                   details::Hoth::_param_GetInitiatePayloadStatus
                        .data(),
                   details::Hoth::_return_GetInitiatePayloadStatus
                        .data(),
                   _callback_GetInitiatePayloadStatus),

    vtable::method("ErasePayload",
                   details::Hoth::_param_ErasePayload
                        .data(),
                   details::Hoth::_return_ErasePayload
                        .data(),
                   _callback_ErasePayload),

    vtable::method("SendPayload",
                   details::Hoth::_param_SendPayload
                        .data(),
                   details::Hoth::_return_SendPayload
                        .data(),
                   _callback_SendPayload),

    vtable::method("GetSendPayloadStatus",
                   details::Hoth::_param_GetSendPayloadStatus
                        .data(),
                   details::Hoth::_return_GetSendPayloadStatus
                        .data(),
                   _callback_GetSendPayloadStatus),

    vtable::method("VerifyPayload",
                   details::Hoth::_param_VerifyPayload
                        .data(),
                   details::Hoth::_return_VerifyPayload
                        .data(),
                   _callback_VerifyPayload),

    vtable::method("GetVerifyPayloadStatus",
                   details::Hoth::_param_GetVerifyPayloadStatus
                        .data(),
                   details::Hoth::_return_GetVerifyPayloadStatus
                        .data(),
                   _callback_GetVerifyPayloadStatus),

    vtable::method("ActivatePayload",
                   details::Hoth::_param_ActivatePayload
                        .data(),
                   details::Hoth::_return_ActivatePayload
                        .data(),
                   _callback_ActivatePayload),

    vtable::method("GetPayloadSize",
                   details::Hoth::_param_GetPayloadSize
                        .data(),
                   details::Hoth::_return_GetPayloadSize
                        .data(),
                   _callback_GetPayloadSize),

    vtable::method("Confirm",
                   details::Hoth::_param_Confirm
                        .data(),
                   details::Hoth::_return_Confirm
                        .data(),
                   _callback_Confirm),

    vtable::method("GetTotalBootTime",
                   details::Hoth::_param_GetTotalBootTime
                        .data(),
                   details::Hoth::_return_GetTotalBootTime
                        .data(),
                   _callback_GetTotalBootTime),

    vtable::method("GetFirmwareUpdateTime",
                   details::Hoth::_param_GetFirmwareUpdateTime
                        .data(),
                   details::Hoth::_return_GetFirmwareUpdateTime
                        .data(),
                   _callback_GetFirmwareUpdateTime),

    vtable::method("GetFirmwareMirroringTime",
                   details::Hoth::_param_GetFirmwareMirroringTime
                        .data(),
                   details::Hoth::_return_GetFirmwareMirroringTime
                        .data(),
                   _callback_GetFirmwareMirroringTime),

    vtable::method("GetPayloadValidationTime",
                   details::Hoth::_param_GetPayloadValidationTime
                        .data(),
                   details::Hoth::_return_GetPayloadValidationTime
                        .data(),
                   _callback_GetPayloadValidationTime),

    vtable::signal("HostCommandResponseReady",
                   details::Hoth::_signal_HostCommandResponseReady
                        .data()),
    vtable::end()
};

} // namespace server
} // namespace gbmc
} // namespace google
} // namespace com
} // namespace sdbusplus

