#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace server
{

Item::Item(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Item::Item(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Item(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Item::prettyName() const ->
        std::string
{
    return _prettyName;
}

int Item::_callback_get_PrettyName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Item*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->prettyName();
                    }
                ));
    }
}

auto Item::prettyName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_prettyName != value)
    {
        _prettyName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_interface.property_changed("PrettyName");
        }
    }

    return _prettyName;
}

auto Item::prettyName(std::string val) ->
        std::string
{
    return prettyName(val, false);
}

int Item::_callback_set_PrettyName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Item*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->prettyName(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Item
{
static const auto _property_PrettyName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Item::present() const ->
        bool
{
    return _present;
}

int Item::_callback_get_Present(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Item*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->present();
                    }
                ));
    }
}

auto Item::present(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_present != value)
    {
        _present = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_interface.property_changed("Present");
        }
    }

    return _present;
}

auto Item::present(bool val) ->
        bool
{
    return present(val, false);
}

int Item::_callback_set_Present(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Item*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->present(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Item
{
static const auto _property_Present =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Item::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PrettyName")
    {
        auto& v = std::get<std::string>(val);
        prettyName(v, skipSignal);
        return;
    }
    if (_name == "Present")
    {
        auto& v = std::get<bool>(val);
        present(v, skipSignal);
        return;
    }
}

auto Item::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PrettyName")
    {
        return prettyName();
    }
    if (_name == "Present")
    {
        return present();
    }

    return PropertiesVariant();
}


const vtable_t Item::_vtable[] = {
    vtable::start(),
    vtable::property("PrettyName",
                     details::Item::_property_PrettyName
                        .data(),
                     _callback_get_PrettyName,
                     _callback_set_PrettyName,
                     vtable::property_::emits_change),
    vtable::property("Present",
                     details::Item::_property_Present
                        .data(),
                     _callback_get_Present,
                     _callback_set_Present,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

