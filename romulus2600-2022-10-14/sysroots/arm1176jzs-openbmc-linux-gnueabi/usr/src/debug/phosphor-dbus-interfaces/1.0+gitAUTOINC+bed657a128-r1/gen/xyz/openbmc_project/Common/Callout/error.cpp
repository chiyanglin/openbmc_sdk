#include <xyz/openbmc_project/Common/Callout/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace Callout
{
namespace Error
{
const char* Device::name() const noexcept
{
    return errName;
}
const char* Device::description() const noexcept
{
    return errDesc;
}
const char* Device::what() const noexcept
{
    return errWhat;
}
const char* GPIO::name() const noexcept
{
    return errName;
}
const char* GPIO::description() const noexcept
{
    return errDesc;
}
const char* GPIO::what() const noexcept
{
    return errWhat;
}
const char* IIC::name() const noexcept
{
    return errName;
}
const char* IIC::description() const noexcept
{
    return errDesc;
}
const char* IIC::what() const noexcept
{
    return errWhat;
}
const char* Inventory::name() const noexcept
{
    return errName;
}
const char* Inventory::description() const noexcept
{
    return errDesc;
}
const char* Inventory::what() const noexcept
{
    return errWhat;
}
const char* IPMISensor::name() const noexcept
{
    return errName;
}
const char* IPMISensor::description() const noexcept
{
    return errDesc;
}
const char* IPMISensor::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Callout
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

