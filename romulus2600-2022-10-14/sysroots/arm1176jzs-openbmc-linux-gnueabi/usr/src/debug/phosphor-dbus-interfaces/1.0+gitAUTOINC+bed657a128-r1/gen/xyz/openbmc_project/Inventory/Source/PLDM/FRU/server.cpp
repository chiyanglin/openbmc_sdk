#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Source/PLDM/FRU/server.hpp>
















namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Source
{
namespace PLDM
{
namespace server
{

FRU::FRU(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FRU::FRU(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FRU(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FRU::chassisType() const ->
        std::string
{
    return _chassisType;
}

int FRU::_callback_get_ChassisType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->chassisType();
                    }
                ));
    }
}

auto FRU::chassisType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_chassisType != value)
    {
        _chassisType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("ChassisType");
        }
    }

    return _chassisType;
}

auto FRU::chassisType(std::string val) ->
        std::string
{
    return chassisType(val, false);
}

int FRU::_callback_set_ChassisType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->chassisType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_ChassisType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::model() const ->
        std::string
{
    return _model;
}

int FRU::_callback_get_Model(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->model();
                    }
                ));
    }
}

auto FRU::model(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_model != value)
    {
        _model = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Model");
        }
    }

    return _model;
}

auto FRU::model(std::string val) ->
        std::string
{
    return model(val, false);
}

int FRU::_callback_set_Model(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->model(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Model =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::pn() const ->
        std::string
{
    return _pn;
}

int FRU::_callback_get_PN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pn();
                    }
                ));
    }
}

auto FRU::pn(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_pn != value)
    {
        _pn = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("PN");
        }
    }

    return _pn;
}

auto FRU::pn(std::string val) ->
        std::string
{
    return pn(val, false);
}

int FRU::_callback_set_PN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->pn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_PN =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::sn() const ->
        std::string
{
    return _sn;
}

int FRU::_callback_get_SN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sn();
                    }
                ));
    }
}

auto FRU::sn(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_sn != value)
    {
        _sn = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("SN");
        }
    }

    return _sn;
}

auto FRU::sn(std::string val) ->
        std::string
{
    return sn(val, false);
}

int FRU::_callback_set_SN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->sn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_SN =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::manufacturer() const ->
        std::string
{
    return _manufacturer;
}

int FRU::_callback_get_Manufacturer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->manufacturer();
                    }
                ));
    }
}

auto FRU::manufacturer(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_manufacturer != value)
    {
        _manufacturer = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Manufacturer");
        }
    }

    return _manufacturer;
}

auto FRU::manufacturer(std::string val) ->
        std::string
{
    return manufacturer(val, false);
}

int FRU::_callback_set_Manufacturer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->manufacturer(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Manufacturer =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::manufacturerDate() const ->
        std::string
{
    return _manufacturerDate;
}

int FRU::_callback_get_ManufacturerDate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->manufacturerDate();
                    }
                ));
    }
}

auto FRU::manufacturerDate(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_manufacturerDate != value)
    {
        _manufacturerDate = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("ManufacturerDate");
        }
    }

    return _manufacturerDate;
}

auto FRU::manufacturerDate(std::string val) ->
        std::string
{
    return manufacturerDate(val, false);
}

int FRU::_callback_set_ManufacturerDate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->manufacturerDate(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_ManufacturerDate =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::vendor() const ->
        std::string
{
    return _vendor;
}

int FRU::_callback_get_Vendor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vendor();
                    }
                ));
    }
}

auto FRU::vendor(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_vendor != value)
    {
        _vendor = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Vendor");
        }
    }

    return _vendor;
}

auto FRU::vendor(std::string val) ->
        std::string
{
    return vendor(val, false);
}

int FRU::_callback_set_Vendor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->vendor(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Vendor =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::name() const ->
        std::string
{
    return _name;
}

int FRU::_callback_get_Name(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->name();
                    }
                ));
    }
}

auto FRU::name(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_name != value)
    {
        _name = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Name");
        }
    }

    return _name;
}

auto FRU::name(std::string val) ->
        std::string
{
    return name(val, false);
}

int FRU::_callback_set_Name(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->name(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Name =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::sku() const ->
        std::string
{
    return _sku;
}

int FRU::_callback_get_SKU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sku();
                    }
                ));
    }
}

auto FRU::sku(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_sku != value)
    {
        _sku = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("SKU");
        }
    }

    return _sku;
}

auto FRU::sku(std::string val) ->
        std::string
{
    return sku(val, false);
}

int FRU::_callback_set_SKU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->sku(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_SKU =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::version() const ->
        std::string
{
    return _version;
}

int FRU::_callback_get_Version(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->version();
                    }
                ));
    }
}

auto FRU::version(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_version != value)
    {
        _version = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Version");
        }
    }

    return _version;
}

auto FRU::version(std::string val) ->
        std::string
{
    return version(val, false);
}

int FRU::_callback_set_Version(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->version(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Version =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::assetTag() const ->
        std::string
{
    return _assetTag;
}

int FRU::_callback_get_AssetTag(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->assetTag();
                    }
                ));
    }
}

auto FRU::assetTag(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_assetTag != value)
    {
        _assetTag = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("AssetTag");
        }
    }

    return _assetTag;
}

auto FRU::assetTag(std::string val) ->
        std::string
{
    return assetTag(val, false);
}

int FRU::_callback_set_AssetTag(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->assetTag(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_AssetTag =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::description() const ->
        std::string
{
    return _description;
}

int FRU::_callback_get_Description(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->description();
                    }
                ));
    }
}

auto FRU::description(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_description != value)
    {
        _description = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Description");
        }
    }

    return _description;
}

auto FRU::description(std::string val) ->
        std::string
{
    return description(val, false);
}

int FRU::_callback_set_Description(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->description(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Description =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::ecLevel() const ->
        std::string
{
    return _ecLevel;
}

int FRU::_callback_get_ECLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ecLevel();
                    }
                ));
    }
}

auto FRU::ecLevel(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ecLevel != value)
    {
        _ecLevel = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("ECLevel");
        }
    }

    return _ecLevel;
}

auto FRU::ecLevel(std::string val) ->
        std::string
{
    return ecLevel(val, false);
}

int FRU::_callback_set_ECLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ecLevel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_ECLevel =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::other() const ->
        std::string
{
    return _other;
}

int FRU::_callback_get_Other(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->other();
                    }
                ));
    }
}

auto FRU::other(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_other != value)
    {
        _other = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("Other");
        }
    }

    return _other;
}

auto FRU::other(std::string val) ->
        std::string
{
    return other(val, false);
}

int FRU::_callback_set_Other(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->other(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_Other =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FRU::iana() const ->
        uint32_t
{
    return _iana;
}

int FRU::_callback_get_IANA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->iana();
                    }
                ));
    }
}

auto FRU::iana(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_iana != value)
    {
        _iana = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.property_changed("IANA");
        }
    }

    return _iana;
}

auto FRU::iana(uint32_t val) ->
        uint32_t
{
    return iana(val, false);
}

int FRU::_callback_set_IANA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FRU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->iana(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FRU
{
static const auto _property_IANA =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void FRU::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ChassisType")
    {
        auto& v = std::get<std::string>(val);
        chassisType(v, skipSignal);
        return;
    }
    if (_name == "Model")
    {
        auto& v = std::get<std::string>(val);
        model(v, skipSignal);
        return;
    }
    if (_name == "PN")
    {
        auto& v = std::get<std::string>(val);
        pn(v, skipSignal);
        return;
    }
    if (_name == "SN")
    {
        auto& v = std::get<std::string>(val);
        sn(v, skipSignal);
        return;
    }
    if (_name == "Manufacturer")
    {
        auto& v = std::get<std::string>(val);
        manufacturer(v, skipSignal);
        return;
    }
    if (_name == "ManufacturerDate")
    {
        auto& v = std::get<std::string>(val);
        manufacturerDate(v, skipSignal);
        return;
    }
    if (_name == "Vendor")
    {
        auto& v = std::get<std::string>(val);
        vendor(v, skipSignal);
        return;
    }
    if (_name == "Name")
    {
        auto& v = std::get<std::string>(val);
        name(v, skipSignal);
        return;
    }
    if (_name == "SKU")
    {
        auto& v = std::get<std::string>(val);
        sku(v, skipSignal);
        return;
    }
    if (_name == "Version")
    {
        auto& v = std::get<std::string>(val);
        version(v, skipSignal);
        return;
    }
    if (_name == "AssetTag")
    {
        auto& v = std::get<std::string>(val);
        assetTag(v, skipSignal);
        return;
    }
    if (_name == "Description")
    {
        auto& v = std::get<std::string>(val);
        description(v, skipSignal);
        return;
    }
    if (_name == "ECLevel")
    {
        auto& v = std::get<std::string>(val);
        ecLevel(v, skipSignal);
        return;
    }
    if (_name == "Other")
    {
        auto& v = std::get<std::string>(val);
        other(v, skipSignal);
        return;
    }
    if (_name == "IANA")
    {
        auto& v = std::get<uint32_t>(val);
        iana(v, skipSignal);
        return;
    }
}

auto FRU::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ChassisType")
    {
        return chassisType();
    }
    if (_name == "Model")
    {
        return model();
    }
    if (_name == "PN")
    {
        return pn();
    }
    if (_name == "SN")
    {
        return sn();
    }
    if (_name == "Manufacturer")
    {
        return manufacturer();
    }
    if (_name == "ManufacturerDate")
    {
        return manufacturerDate();
    }
    if (_name == "Vendor")
    {
        return vendor();
    }
    if (_name == "Name")
    {
        return name();
    }
    if (_name == "SKU")
    {
        return sku();
    }
    if (_name == "Version")
    {
        return version();
    }
    if (_name == "AssetTag")
    {
        return assetTag();
    }
    if (_name == "Description")
    {
        return description();
    }
    if (_name == "ECLevel")
    {
        return ecLevel();
    }
    if (_name == "Other")
    {
        return other();
    }
    if (_name == "IANA")
    {
        return iana();
    }

    return PropertiesVariant();
}


const vtable_t FRU::_vtable[] = {
    vtable::start(),
    vtable::property("ChassisType",
                     details::FRU::_property_ChassisType
                        .data(),
                     _callback_get_ChassisType,
                     _callback_set_ChassisType,
                     vtable::property_::emits_change),
    vtable::property("Model",
                     details::FRU::_property_Model
                        .data(),
                     _callback_get_Model,
                     _callback_set_Model,
                     vtable::property_::emits_change),
    vtable::property("PN",
                     details::FRU::_property_PN
                        .data(),
                     _callback_get_PN,
                     _callback_set_PN,
                     vtable::property_::emits_change),
    vtable::property("SN",
                     details::FRU::_property_SN
                        .data(),
                     _callback_get_SN,
                     _callback_set_SN,
                     vtable::property_::emits_change),
    vtable::property("Manufacturer",
                     details::FRU::_property_Manufacturer
                        .data(),
                     _callback_get_Manufacturer,
                     _callback_set_Manufacturer,
                     vtable::property_::emits_change),
    vtable::property("ManufacturerDate",
                     details::FRU::_property_ManufacturerDate
                        .data(),
                     _callback_get_ManufacturerDate,
                     _callback_set_ManufacturerDate,
                     vtable::property_::emits_change),
    vtable::property("Vendor",
                     details::FRU::_property_Vendor
                        .data(),
                     _callback_get_Vendor,
                     _callback_set_Vendor,
                     vtable::property_::emits_change),
    vtable::property("Name",
                     details::FRU::_property_Name
                        .data(),
                     _callback_get_Name,
                     _callback_set_Name,
                     vtable::property_::emits_change),
    vtable::property("SKU",
                     details::FRU::_property_SKU
                        .data(),
                     _callback_get_SKU,
                     _callback_set_SKU,
                     vtable::property_::emits_change),
    vtable::property("Version",
                     details::FRU::_property_Version
                        .data(),
                     _callback_get_Version,
                     _callback_set_Version,
                     vtable::property_::emits_change),
    vtable::property("AssetTag",
                     details::FRU::_property_AssetTag
                        .data(),
                     _callback_get_AssetTag,
                     _callback_set_AssetTag,
                     vtable::property_::emits_change),
    vtable::property("Description",
                     details::FRU::_property_Description
                        .data(),
                     _callback_get_Description,
                     _callback_set_Description,
                     vtable::property_::emits_change),
    vtable::property("ECLevel",
                     details::FRU::_property_ECLevel
                        .data(),
                     _callback_get_ECLevel,
                     _callback_set_ECLevel,
                     vtable::property_::emits_change),
    vtable::property("Other",
                     details::FRU::_property_Other
                        .data(),
                     _callback_get_Other,
                     _callback_set_Other,
                     vtable::property_::emits_change),
    vtable::property("IANA",
                     details::FRU::_property_IANA
                        .data(),
                     _callback_get_IANA,
                     _callback_set_IANA,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PLDM
} // namespace Source
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

