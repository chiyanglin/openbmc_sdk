#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/TFTP/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

TFTP::TFTP(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_TFTP_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int TFTP::_callback_DownloadViaTFTP(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<TFTP*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& fileName, std::string&& serverAddress)
                    {
                        return o->downloadViaTFTP(
                                fileName, serverAddress);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace TFTP
{
static const auto _param_DownloadViaTFTP =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string>());
static const auto _return_DownloadViaTFTP =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t TFTP::_vtable[] = {
    vtable::start(),

    vtable::method("DownloadViaTFTP",
                   details::TFTP::_param_DownloadViaTFTP
                        .data(),
                   details::TFTP::_return_DownloadViaTFTP
                        .data(),
                   _callback_DownloadViaTFTP),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

