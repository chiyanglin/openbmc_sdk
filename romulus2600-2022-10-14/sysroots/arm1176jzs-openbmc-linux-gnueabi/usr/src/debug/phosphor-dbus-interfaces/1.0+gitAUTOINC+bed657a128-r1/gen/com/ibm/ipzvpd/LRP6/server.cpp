#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/LRP6/server.hpp>





namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

LRP6::LRP6(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_LRP6_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

LRP6::LRP6(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : LRP6(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto LRP6::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int LRP6::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto LRP6::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP6_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto LRP6::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int LRP6::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP6
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LRP6::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int LRP6::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto LRP6::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP6_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto LRP6::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int LRP6::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP6
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LRP6::tc() const ->
        std::vector<uint8_t>
{
    return _tc;
}

int LRP6::_callback_get_TC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tc();
                    }
                ));
    }
}

auto LRP6::tc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tc != value)
    {
        _tc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP6_interface.property_changed("TC");
        }
    }

    return _tc;
}

auto LRP6::tc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tc(val, false);
}

int LRP6::_callback_set_TC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP6
{
static const auto _property_TC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LRP6::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int LRP6::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto LRP6::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP6_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto LRP6::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int LRP6::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP6*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP6
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void LRP6::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "TC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tc(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
}

auto LRP6::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "TC")
    {
        return tc();
    }
    if (_name == "D4")
    {
        return d4();
    }

    return PropertiesVariant();
}


const vtable_t LRP6::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::LRP6::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::LRP6::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("TC",
                     details::LRP6::_property_TC
                        .data(),
                     _callback_get_TC,
                     _callback_set_TC,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::LRP6::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

