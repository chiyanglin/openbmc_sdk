#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

class Critical
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Critical() = delete;
        Critical(const Critical&) = delete;
        Critical& operator=(const Critical&) = delete;
        Critical(Critical&&) = delete;
        Critical& operator=(Critical&&) = delete;
        virtual ~Critical() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Critical(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Critical(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** @brief Send signal 'CriticalHighAlarmAsserted'
         *
         *  The high threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void criticalHighAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'CriticalHighAlarmDeasserted'
         *
         *  The high threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void criticalHighAlarmDeasserted(
            double sensorValue);

        /** @brief Send signal 'CriticalLowAlarmAsserted'
         *
         *  The low threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void criticalLowAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'CriticalLowAlarmDeasserted'
         *
         *  The low threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void criticalLowAlarmDeasserted(
            double sensorValue);

        /** Get value of CriticalHigh */
        virtual double criticalHigh() const;
        /** Set value of CriticalHigh with option to skip sending signal */
        virtual double criticalHigh(double value,
               bool skipSignal);
        /** Set value of CriticalHigh */
        virtual double criticalHigh(double value);
        /** Get value of CriticalLow */
        virtual double criticalLow() const;
        /** Set value of CriticalLow with option to skip sending signal */
        virtual double criticalLow(double value,
               bool skipSignal);
        /** Set value of CriticalLow */
        virtual double criticalLow(double value);
        /** Get value of CriticalAlarmHigh */
        virtual bool criticalAlarmHigh() const;
        /** Set value of CriticalAlarmHigh with option to skip sending signal */
        virtual bool criticalAlarmHigh(bool value,
               bool skipSignal);
        /** Set value of CriticalAlarmHigh */
        virtual bool criticalAlarmHigh(bool value);
        /** Get value of CriticalAlarmLow */
        virtual bool criticalAlarmLow() const;
        /** Set value of CriticalAlarmLow with option to skip sending signal */
        virtual bool criticalAlarmLow(bool value,
               bool skipSignal);
        /** Set value of CriticalAlarmLow */
        virtual bool criticalAlarmLow(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Sensor_Threshold_Critical_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Sensor_Threshold_Critical_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.Critical";

    private:

        /** @brief sd-bus callback for get-property 'CriticalHigh' */
        static int _callback_get_CriticalHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CriticalHigh' */
        static int _callback_set_CriticalHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CriticalLow' */
        static int _callback_get_CriticalLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CriticalLow' */
        static int _callback_set_CriticalLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CriticalAlarmHigh' */
        static int _callback_get_CriticalAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CriticalAlarmHigh' */
        static int _callback_set_CriticalAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CriticalAlarmLow' */
        static int _callback_get_CriticalAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CriticalAlarmLow' */
        static int _callback_set_CriticalAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Sensor_Threshold_Critical_interface;
        sdbusplus::SdBusInterface *_intf;

        double _criticalHigh = std::numeric_limits<double>::quiet_NaN();
        double _criticalLow = std::numeric_limits<double>::quiet_NaN();
        bool _criticalAlarmHigh{};
        bool _criticalAlarmLow{};

};


} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

