#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/LWP1/server.hpp>













namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

LWP1::LWP1(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_LWP1_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

LWP1::LWP1(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : LWP1(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto LWP1::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int LWP1::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto LWP1::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto LWP1::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int LWP1::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int LWP1::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto LWP1::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto LWP1::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int LWP1::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n20() const ->
        std::vector<uint8_t>
{
    return _n20;
}

int LWP1::_callback_get_N_20(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n20();
                    }
                ));
    }
}

auto LWP1::n20(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n20 != value)
    {
        _n20 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_20");
        }
    }

    return _n20;
}

auto LWP1::n20(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n20(val, false);
}

int LWP1::_callback_set_N_20(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n20(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_20 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n21() const ->
        std::vector<uint8_t>
{
    return _n21;
}

int LWP1::_callback_get_N_21(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n21();
                    }
                ));
    }
}

auto LWP1::n21(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n21 != value)
    {
        _n21 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_21");
        }
    }

    return _n21;
}

auto LWP1::n21(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n21(val, false);
}

int LWP1::_callback_set_N_21(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n21(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_21 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n22() const ->
        std::vector<uint8_t>
{
    return _n22;
}

int LWP1::_callback_get_N_22(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n22();
                    }
                ));
    }
}

auto LWP1::n22(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n22 != value)
    {
        _n22 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_22");
        }
    }

    return _n22;
}

auto LWP1::n22(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n22(val, false);
}

int LWP1::_callback_set_N_22(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n22(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_22 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n23() const ->
        std::vector<uint8_t>
{
    return _n23;
}

int LWP1::_callback_get_N_23(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n23();
                    }
                ));
    }
}

auto LWP1::n23(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n23 != value)
    {
        _n23 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_23");
        }
    }

    return _n23;
}

auto LWP1::n23(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n23(val, false);
}

int LWP1::_callback_set_N_23(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n23(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_23 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n30() const ->
        std::vector<uint8_t>
{
    return _n30;
}

int LWP1::_callback_get_N_30(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n30();
                    }
                ));
    }
}

auto LWP1::n30(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n30 != value)
    {
        _n30 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_30");
        }
    }

    return _n30;
}

auto LWP1::n30(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n30(val, false);
}

int LWP1::_callback_set_N_30(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n30(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_30 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n31() const ->
        std::vector<uint8_t>
{
    return _n31;
}

int LWP1::_callback_get_N_31(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n31();
                    }
                ));
    }
}

auto LWP1::n31(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n31 != value)
    {
        _n31 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_31");
        }
    }

    return _n31;
}

auto LWP1::n31(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n31(val, false);
}

int LWP1::_callback_set_N_31(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n31(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_31 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n32() const ->
        std::vector<uint8_t>
{
    return _n32;
}

int LWP1::_callback_get_N_32(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n32();
                    }
                ));
    }
}

auto LWP1::n32(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n32 != value)
    {
        _n32 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_32");
        }
    }

    return _n32;
}

auto LWP1::n32(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n32(val, false);
}

int LWP1::_callback_set_N_32(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n32(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_32 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::n33() const ->
        std::vector<uint8_t>
{
    return _n33;
}

int LWP1::_callback_get_N_33(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->n33();
                    }
                ));
    }
}

auto LWP1::n33(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_n33 != value)
    {
        _n33 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("N_33");
        }
    }

    return _n33;
}

auto LWP1::n33(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return n33(val, false);
}

int LWP1::_callback_set_N_33(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->n33(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_N_33 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int LWP1::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto LWP1::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto LWP1::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int LWP1::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LWP1::f5() const ->
        std::vector<uint8_t>
{
    return _f5;
}

int LWP1::_callback_get_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f5();
                    }
                ));
    }
}

auto LWP1::f5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f5 != value)
    {
        _f5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LWP1_interface.property_changed("F5");
        }
    }

    return _f5;
}

auto LWP1::f5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f5(val, false);
}

int LWP1::_callback_set_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LWP1*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LWP1
{
static const auto _property_F5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void LWP1::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "N_20")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n20(v, skipSignal);
        return;
    }
    if (_name == "N_21")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n21(v, skipSignal);
        return;
    }
    if (_name == "N_22")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n22(v, skipSignal);
        return;
    }
    if (_name == "N_23")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n23(v, skipSignal);
        return;
    }
    if (_name == "N_30")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n30(v, skipSignal);
        return;
    }
    if (_name == "N_31")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n31(v, skipSignal);
        return;
    }
    if (_name == "N_32")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n32(v, skipSignal);
        return;
    }
    if (_name == "N_33")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        n33(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
    if (_name == "F5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f5(v, skipSignal);
        return;
    }
}

auto LWP1::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "N_20")
    {
        return n20();
    }
    if (_name == "N_21")
    {
        return n21();
    }
    if (_name == "N_22")
    {
        return n22();
    }
    if (_name == "N_23")
    {
        return n23();
    }
    if (_name == "N_30")
    {
        return n30();
    }
    if (_name == "N_31")
    {
        return n31();
    }
    if (_name == "N_32")
    {
        return n32();
    }
    if (_name == "N_33")
    {
        return n33();
    }
    if (_name == "D4")
    {
        return d4();
    }
    if (_name == "F5")
    {
        return f5();
    }

    return PropertiesVariant();
}


const vtable_t LWP1::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::LWP1::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::LWP1::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("N_20",
                     details::LWP1::_property_N_20
                        .data(),
                     _callback_get_N_20,
                     _callback_set_N_20,
                     vtable::property_::emits_change),
    vtable::property("N_21",
                     details::LWP1::_property_N_21
                        .data(),
                     _callback_get_N_21,
                     _callback_set_N_21,
                     vtable::property_::emits_change),
    vtable::property("N_22",
                     details::LWP1::_property_N_22
                        .data(),
                     _callback_get_N_22,
                     _callback_set_N_22,
                     vtable::property_::emits_change),
    vtable::property("N_23",
                     details::LWP1::_property_N_23
                        .data(),
                     _callback_get_N_23,
                     _callback_set_N_23,
                     vtable::property_::emits_change),
    vtable::property("N_30",
                     details::LWP1::_property_N_30
                        .data(),
                     _callback_get_N_30,
                     _callback_set_N_30,
                     vtable::property_::emits_change),
    vtable::property("N_31",
                     details::LWP1::_property_N_31
                        .data(),
                     _callback_get_N_31,
                     _callback_set_N_31,
                     vtable::property_::emits_change),
    vtable::property("N_32",
                     details::LWP1::_property_N_32
                        .data(),
                     _callback_get_N_32,
                     _callback_set_N_32,
                     vtable::property_::emits_change),
    vtable::property("N_33",
                     details::LWP1::_property_N_33
                        .data(),
                     _callback_get_N_33,
                     _callback_set_N_33,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::LWP1::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::property("F5",
                     details::LWP1::_property_F5
                        .data(),
                     _callback_get_F5,
                     _callback_set_F5,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

