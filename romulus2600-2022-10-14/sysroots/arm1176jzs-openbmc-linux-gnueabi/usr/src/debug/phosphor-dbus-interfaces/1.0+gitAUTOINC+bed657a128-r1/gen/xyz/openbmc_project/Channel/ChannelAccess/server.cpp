#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Channel/ChannelAccess/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Channel
{
namespace server
{

ChannelAccess::ChannelAccess(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Channel_ChannelAccess_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ChannelAccess::ChannelAccess(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ChannelAccess(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ChannelAccess::maxPrivilege() const ->
        std::string
{
    return _maxPrivilege;
}

int ChannelAccess::_callback_get_MaxPrivilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChannelAccess*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxPrivilege();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto ChannelAccess::maxPrivilege(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_maxPrivilege != value)
    {
        _maxPrivilege = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Channel_ChannelAccess_interface.property_changed("MaxPrivilege");
        }
    }

    return _maxPrivilege;
}

auto ChannelAccess::maxPrivilege(std::string val) ->
        std::string
{
    return maxPrivilege(val, false);
}

int ChannelAccess::_callback_set_MaxPrivilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChannelAccess*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->maxPrivilege(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace ChannelAccess
{
static const auto _property_MaxPrivilege =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void ChannelAccess::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MaxPrivilege")
    {
        auto& v = std::get<std::string>(val);
        maxPrivilege(v, skipSignal);
        return;
    }
}

auto ChannelAccess::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MaxPrivilege")
    {
        return maxPrivilege();
    }

    return PropertiesVariant();
}


const vtable_t ChannelAccess::_vtable[] = {
    vtable::start(),
    vtable::property("MaxPrivilege",
                     details::ChannelAccess::_property_MaxPrivilege
                        .data(),
                     _callback_get_MaxPrivilege,
                     _callback_set_MaxPrivilege,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Channel
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

