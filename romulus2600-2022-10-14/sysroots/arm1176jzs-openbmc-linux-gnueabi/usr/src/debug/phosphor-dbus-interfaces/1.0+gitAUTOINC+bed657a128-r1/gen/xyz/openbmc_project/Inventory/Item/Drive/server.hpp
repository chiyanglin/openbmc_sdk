#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>








#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Drive
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Drive() = delete;
        Drive(const Drive&) = delete;
        Drive& operator=(const Drive&) = delete;
        Drive(Drive&&) = delete;
        Drive& operator=(Drive&&) = delete;
        virtual ~Drive() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Drive(bus_t& bus, const char* path);

        enum class DriveProtocol
        {
            SAS,
            SATA,
            NVMe,
            FC,
            Unknown,
        };
        enum class DriveType
        {
            HDD,
            SSD,
            Unknown,
        };
        enum class DriveEncryptionState
        {
            Encrypted,
            Unencrypted,
            Unknown,
        };
        enum class DriveLockState
        {
            Locked,
            Unlocked,
            Unknown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                DriveEncryptionState,
                DriveLockState,
                DriveProtocol,
                DriveType,
                bool,
                uint64_t,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Drive(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Capacity */
        virtual uint64_t capacity() const;
        /** Set value of Capacity with option to skip sending signal */
        virtual uint64_t capacity(uint64_t value,
               bool skipSignal);
        /** Set value of Capacity */
        virtual uint64_t capacity(uint64_t value);
        /** Get value of Protocol */
        virtual DriveProtocol protocol() const;
        /** Set value of Protocol with option to skip sending signal */
        virtual DriveProtocol protocol(DriveProtocol value,
               bool skipSignal);
        /** Set value of Protocol */
        virtual DriveProtocol protocol(DriveProtocol value);
        /** Get value of Type */
        virtual DriveType type() const;
        /** Set value of Type with option to skip sending signal */
        virtual DriveType type(DriveType value,
               bool skipSignal);
        /** Set value of Type */
        virtual DriveType type(DriveType value);
        /** Get value of EncryptionStatus */
        virtual DriveEncryptionState encryptionStatus() const;
        /** Set value of EncryptionStatus with option to skip sending signal */
        virtual DriveEncryptionState encryptionStatus(DriveEncryptionState value,
               bool skipSignal);
        /** Set value of EncryptionStatus */
        virtual DriveEncryptionState encryptionStatus(DriveEncryptionState value);
        /** Get value of LockedStatus */
        virtual DriveLockState lockedStatus() const;
        /** Set value of LockedStatus with option to skip sending signal */
        virtual DriveLockState lockedStatus(DriveLockState value,
               bool skipSignal);
        /** Set value of LockedStatus */
        virtual DriveLockState lockedStatus(DriveLockState value);
        /** Get value of PredictedMediaLifeLeftPercent */
        virtual uint8_t predictedMediaLifeLeftPercent() const;
        /** Set value of PredictedMediaLifeLeftPercent with option to skip sending signal */
        virtual uint8_t predictedMediaLifeLeftPercent(uint8_t value,
               bool skipSignal);
        /** Set value of PredictedMediaLifeLeftPercent */
        virtual uint8_t predictedMediaLifeLeftPercent(uint8_t value);
        /** Get value of Resettable */
        virtual bool resettable() const;
        /** Set value of Resettable with option to skip sending signal */
        virtual bool resettable(bool value,
               bool skipSignal);
        /** Set value of Resettable */
        virtual bool resettable(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DriveProtocol convertDriveProtocolFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DriveProtocol> convertStringToDriveProtocol(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         */
        static std::string convertDriveProtocolToString(DriveProtocol e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DriveType convertDriveTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DriveType> convertStringToDriveType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         */
        static std::string convertDriveTypeToString(DriveType e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DriveEncryptionState convertDriveEncryptionStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DriveEncryptionState> convertStringToDriveEncryptionState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         */
        static std::string convertDriveEncryptionStateToString(DriveEncryptionState e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DriveLockState convertDriveLockStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DriveLockState> convertStringToDriveLockState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Drive.<value name>"
         */
        static std::string convertDriveLockStateToString(DriveLockState e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Drive";

    private:

        /** @brief sd-bus callback for get-property 'Capacity' */
        static int _callback_get_Capacity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Capacity' */
        static int _callback_set_Capacity(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Protocol' */
        static int _callback_get_Protocol(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Protocol' */
        static int _callback_set_Protocol(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Type' */
        static int _callback_get_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Type' */
        static int _callback_set_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'EncryptionStatus' */
        static int _callback_get_EncryptionStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'EncryptionStatus' */
        static int _callback_set_EncryptionStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LockedStatus' */
        static int _callback_get_LockedStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LockedStatus' */
        static int _callback_set_LockedStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PredictedMediaLifeLeftPercent' */
        static int _callback_get_PredictedMediaLifeLeftPercent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PredictedMediaLifeLeftPercent' */
        static int _callback_set_PredictedMediaLifeLeftPercent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Resettable' */
        static int _callback_get_Resettable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Drive_interface;
        sdbusplus::SdBusInterface *_intf;

        uint64_t _capacity = 0;
        DriveProtocol _protocol = DriveProtocol::Unknown;
        DriveType _type = DriveType::Unknown;
        DriveEncryptionState _encryptionStatus = DriveEncryptionState::Unknown;
        DriveLockState _lockedStatus = DriveLockState::Unknown;
        uint8_t _predictedMediaLifeLeftPercent = std::numeric_limits<uint8_t>::max();
        bool _resettable = false;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Drive::DriveProtocol.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Drive::DriveProtocol e)
{
    return Drive::convertDriveProtocolToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Drive::DriveType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Drive::DriveType e)
{
    return Drive::convertDriveTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Drive::DriveEncryptionState.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Drive::DriveEncryptionState e)
{
    return Drive::convertDriveEncryptionStateToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Drive::DriveLockState.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Drive::DriveLockState e)
{
    return Drive::convertDriveLockStateToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveProtocol>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertStringToDriveProtocol(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveProtocol>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Drive::DriveProtocol value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertDriveProtocolToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertStringToDriveType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveType>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Drive::DriveType value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertDriveTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveEncryptionState>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertStringToDriveEncryptionState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveEncryptionState>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Drive::DriveEncryptionState value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertDriveEncryptionStateToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveLockState>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertStringToDriveLockState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Drive::DriveLockState>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Drive::DriveLockState value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Drive::convertDriveLockStateToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

