#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Drive/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

Drive::Drive(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Drive_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Drive::Drive(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Drive(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Drive::rebuilding() const ->
        bool
{
    return _rebuilding;
}

int Drive::_callback_get_Rebuilding(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rebuilding();
                    }
                ));
    }
}

auto Drive::rebuilding(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_rebuilding != value)
    {
        _rebuilding = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Drive_interface.property_changed("Rebuilding");
        }
    }

    return _rebuilding;
}

auto Drive::rebuilding(bool val) ->
        bool
{
    return rebuilding(val, false);
}

int Drive::_callback_set_Rebuilding(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->rebuilding(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_Rebuilding =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Drive::requestedDriveTransition() const ->
        Transition
{
    return _requestedDriveTransition;
}

int Drive::_callback_get_RequestedDriveTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->requestedDriveTransition();
                    }
                ));
    }
}

auto Drive::requestedDriveTransition(Transition value,
                                         bool skipSignal) ->
        Transition
{
    if (_requestedDriveTransition != value)
    {
        _requestedDriveTransition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Drive_interface.property_changed("RequestedDriveTransition");
        }
    }

    return _requestedDriveTransition;
}

auto Drive::requestedDriveTransition(Transition val) ->
        Transition
{
    return requestedDriveTransition(val, false);
}

int Drive::_callback_set_RequestedDriveTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Transition&& arg)
                    {
                        o->requestedDriveTransition(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_RequestedDriveTransition =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Drive::Transition>());
}
}

auto Drive::currentDriveState() const ->
        DriveState
{
    return _currentDriveState;
}

int Drive::_callback_get_CurrentDriveState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentDriveState();
                    }
                ));
    }
}

auto Drive::currentDriveState(DriveState value,
                                         bool skipSignal) ->
        DriveState
{
    if (_currentDriveState != value)
    {
        _currentDriveState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Drive_interface.property_changed("CurrentDriveState");
        }
    }

    return _currentDriveState;
}

auto Drive::currentDriveState(DriveState val) ->
        DriveState
{
    return currentDriveState(val, false);
}


namespace details
{
namespace Drive
{
static const auto _property_CurrentDriveState =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Drive::DriveState>());
}
}

auto Drive::lastRebootTime() const ->
        uint64_t
{
    return _lastRebootTime;
}

int Drive::_callback_get_LastRebootTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lastRebootTime();
                    }
                ));
    }
}

auto Drive::lastRebootTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_lastRebootTime != value)
    {
        _lastRebootTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Drive_interface.property_changed("LastRebootTime");
        }
    }

    return _lastRebootTime;
}

auto Drive::lastRebootTime(uint64_t val) ->
        uint64_t
{
    return lastRebootTime(val, false);
}


namespace details
{
namespace Drive
{
static const auto _property_LastRebootTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Drive::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Rebuilding")
    {
        auto& v = std::get<bool>(val);
        rebuilding(v, skipSignal);
        return;
    }
    if (_name == "RequestedDriveTransition")
    {
        auto& v = std::get<Transition>(val);
        requestedDriveTransition(v, skipSignal);
        return;
    }
    if (_name == "CurrentDriveState")
    {
        auto& v = std::get<DriveState>(val);
        currentDriveState(v, skipSignal);
        return;
    }
    if (_name == "LastRebootTime")
    {
        auto& v = std::get<uint64_t>(val);
        lastRebootTime(v, skipSignal);
        return;
    }
}

auto Drive::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Rebuilding")
    {
        return rebuilding();
    }
    if (_name == "RequestedDriveTransition")
    {
        return requestedDriveTransition();
    }
    if (_name == "CurrentDriveState")
    {
        return currentDriveState();
    }
    if (_name == "LastRebootTime")
    {
        return lastRebootTime();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Drive::Transition */
static const std::tuple<const char*, Drive::Transition> mappingDriveTransition[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Drive.Transition.Reboot",                 Drive::Transition::Reboot ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.Transition.HardReboot",                 Drive::Transition::HardReboot ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.Transition.Powercycle",                 Drive::Transition::Powercycle ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.Transition.None",                 Drive::Transition::None ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.Transition.NotSupported",                 Drive::Transition::NotSupported ),
        };

} // anonymous namespace

auto Drive::convertStringToTransition(const std::string& s) noexcept ->
        std::optional<Transition>
{
    auto i = std::find_if(
            std::begin(mappingDriveTransition),
            std::end(mappingDriveTransition),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDriveTransition) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Drive::convertTransitionFromString(const std::string& s) ->
        Transition
{
    auto r = convertStringToTransition(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Drive::convertTransitionToString(Drive::Transition v)
{
    auto i = std::find_if(
            std::begin(mappingDriveTransition),
            std::end(mappingDriveTransition),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDriveTransition))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Drive::DriveState */
static const std::tuple<const char*, Drive::DriveState> mappingDriveDriveState[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Drive.DriveState.Unknown",                 Drive::DriveState::Unknown ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.DriveState.Ready",                 Drive::DriveState::Ready ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.DriveState.NotReady",                 Drive::DriveState::NotReady ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.DriveState.Offline",                 Drive::DriveState::Offline ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.DriveState.Debug",                 Drive::DriveState::Debug ),
            std::make_tuple( "xyz.openbmc_project.State.Drive.DriveState.UpdateInProgress",                 Drive::DriveState::UpdateInProgress ),
        };

} // anonymous namespace

auto Drive::convertStringToDriveState(const std::string& s) noexcept ->
        std::optional<DriveState>
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveState),
            std::end(mappingDriveDriveState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDriveDriveState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Drive::convertDriveStateFromString(const std::string& s) ->
        DriveState
{
    auto r = convertStringToDriveState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Drive::convertDriveStateToString(Drive::DriveState v)
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveState),
            std::end(mappingDriveDriveState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDriveDriveState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Drive::_vtable[] = {
    vtable::start(),
    vtable::property("Rebuilding",
                     details::Drive::_property_Rebuilding
                        .data(),
                     _callback_get_Rebuilding,
                     _callback_set_Rebuilding,
                     vtable::property_::emits_change),
    vtable::property("RequestedDriveTransition",
                     details::Drive::_property_RequestedDriveTransition
                        .data(),
                     _callback_get_RequestedDriveTransition,
                     _callback_set_RequestedDriveTransition,
                     vtable::property_::emits_change),
    vtable::property("CurrentDriveState",
                     details::Drive::_property_CurrentDriveState
                        .data(),
                     _callback_get_CurrentDriveState,
                     vtable::property_::emits_change),
    vtable::property("LastRebootTime",
                     details::Drive::_property_LastRebootTime
                        .data(),
                     _callback_get_LastRebootTime,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

