#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VPRI/server.hpp>





namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VPRI::VPRI(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VPRI_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VPRI::VPRI(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VPRI(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VPRI::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VPRI::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VPRI::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VPRI_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VPRI::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VPRI::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VPRI
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VPRI::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int VPRI::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto VPRI::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VPRI_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto VPRI::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int VPRI::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VPRI
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VPRI::cr() const ->
        std::vector<uint8_t>
{
    return _cr;
}

int VPRI::_callback_get_CR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->cr();
                    }
                ));
    }
}

auto VPRI::cr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_cr != value)
    {
        _cr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VPRI_interface.property_changed("CR");
        }
    }

    return _cr;
}

auto VPRI::cr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return cr(val, false);
}

int VPRI::_callback_set_CR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->cr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VPRI
{
static const auto _property_CR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VPRI::pdd() const ->
        std::vector<uint8_t>
{
    return _pdd;
}

int VPRI::_callback_get_PD_D(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdd();
                    }
                ));
    }
}

auto VPRI::pdd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdd != value)
    {
        _pdd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VPRI_interface.property_changed("PD_D");
        }
    }

    return _pdd;
}

auto VPRI::pdd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdd(val, false);
}

int VPRI::_callback_set_PD_D(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VPRI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VPRI
{
static const auto _property_PD_D =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VPRI::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "CR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        cr(v, skipSignal);
        return;
    }
    if (_name == "PD_D")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdd(v, skipSignal);
        return;
    }
}

auto VPRI::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "CR")
    {
        return cr();
    }
    if (_name == "PD_D")
    {
        return pdd();
    }

    return PropertiesVariant();
}


const vtable_t VPRI::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VPRI::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::VPRI::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("CR",
                     details::VPRI::_property_CR
                        .data(),
                     _callback_get_CR,
                     _callback_set_CR,
                     vtable::property_::emits_change),
    vtable::property("PD_D",
                     details::VPRI::_property_PD_D
                        .data(),
                     _callback_get_PD_D,
                     _callback_set_PD_D,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

