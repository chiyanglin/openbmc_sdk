#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>







#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Volume
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Volume() = delete;
        Volume(const Volume&) = delete;
        Volume& operator=(const Volume&) = delete;
        Volume(Volume&&) = delete;
        Volume& operator=(Volume&&) = delete;
        virtual ~Volume() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Volume(bus_t& bus, const char* path);

        enum class EraseMethod
        {
            CryptoErase,
            VerifyGeometry,
            LogicalOverWrite,
            LogicalVerify,
            VendorSanitize,
            ZeroOverWrite,
            ZeroVerify,
            SecuredLocked,
        };
        enum class FilesystemType
        {
            ext2,
            ext3,
            ext4,
            vfat,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Volume(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for FormatLuks
         *  Format a LUKS encrypted device and create a filesystem.
         *
         *  @param[in] password - Array of bytes to use as the LUKS password.
         *  @param[in] type - Type of filesystem, e.g. ext2, ext3, ext4, vfat.
         */
        virtual void formatLuks(
            std::vector<uint8_t> password,
            FilesystemType type) = 0;

        /** @brief Implementation for Erase
         *  Erase the contents of the volume.
         *
         *  @param[in] eraseType - Describes what type of erase is done.
         */
        virtual void erase(
            EraseMethod eraseType) = 0;

        /** @brief Implementation for Lock
         *  Unmount the filesystem, lock the volume, and remove sensitive data (e.g. volume key) from memory.
         */
        virtual void lock(
            ) = 0;

        /** @brief Implementation for Unlock
         *  Activate the volume and mount the filesystem.
         *
         *  @param[in] password - Array of bytes to use as the LUKS password.
         */
        virtual void unlock(
            std::vector<uint8_t> password) = 0;

        /** @brief Implementation for ChangePassword
         *  Change the LUKS password that unlocks the storage volume.
         *
         *  @param[in] oldPassword - Array of bytes for the old LUKS password.
         *  @param[in] newPassword - Array of bytes to use as the LUKS password.
         */
        virtual void changePassword(
            std::vector<uint8_t> oldPassword,
            std::vector<uint8_t> newPassword) = 0;


        /** Get value of Locked */
        virtual bool locked() const;
        /** Set value of Locked with option to skip sending signal */
        virtual bool locked(bool value,
               bool skipSignal);
        /** Set value of Locked */
        virtual bool locked(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Volume.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static EraseMethod convertEraseMethodFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Volume.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<EraseMethod> convertStringToEraseMethod(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Volume.<value name>"
         */
        static std::string convertEraseMethodToString(EraseMethod e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Volume.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static FilesystemType convertFilesystemTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Volume.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<FilesystemType> convertStringToFilesystemType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Volume.<value name>"
         */
        static std::string convertFilesystemTypeToString(FilesystemType e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Volume_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Volume_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Volume";

    private:

        /** @brief sd-bus callback for FormatLuks
         */
        static int _callback_FormatLuks(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for Erase
         */
        static int _callback_Erase(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for Lock
         */
        static int _callback_Lock(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for Unlock
         */
        static int _callback_Unlock(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for ChangePassword
         */
        static int _callback_ChangePassword(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Locked' */
        static int _callback_get_Locked(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Locked' */
        static int _callback_set_Locked(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Volume_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _locked = false;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Volume::EraseMethod.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Volume::EraseMethod e)
{
    return Volume::convertEraseMethodToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Volume::FilesystemType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Volume::FilesystemType e)
{
    return Volume::convertFilesystemTypeToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Volume::EraseMethod>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Volume::convertStringToEraseMethod(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Volume::EraseMethod>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Volume::EraseMethod value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Volume::convertEraseMethodToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Volume::FilesystemType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Volume::convertStringToFilesystemType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Volume::FilesystemType>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Volume::FilesystemType value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Volume::convertFilesystemTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

