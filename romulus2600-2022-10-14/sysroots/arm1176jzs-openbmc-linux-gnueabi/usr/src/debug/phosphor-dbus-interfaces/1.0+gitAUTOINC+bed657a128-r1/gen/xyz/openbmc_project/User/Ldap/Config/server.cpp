#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/Ldap/Config/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>


#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Ldap
{
namespace server
{

Config::Config(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_Ldap_Config_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Config::Config(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Config(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Config::ldapServerURI() const ->
        std::string
{
    return _ldapServerURI;
}

int Config::_callback_get_LDAPServerURI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ldapServerURI();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NoCACertificate& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Config::ldapServerURI(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ldapServerURI != value)
    {
        _ldapServerURI = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("LDAPServerURI");
        }
    }

    return _ldapServerURI;
}

auto Config::ldapServerURI(std::string val) ->
        std::string
{
    return ldapServerURI(val, false);
}

int Config::_callback_set_LDAPServerURI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ldapServerURI(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NoCACertificate& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_LDAPServerURI =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Config::ldapBindDN() const ->
        std::string
{
    return _ldapBindDN;
}

int Config::_callback_get_LDAPBindDN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ldapBindDN();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Config::ldapBindDN(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ldapBindDN != value)
    {
        _ldapBindDN = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("LDAPBindDN");
        }
    }

    return _ldapBindDN;
}

auto Config::ldapBindDN(std::string val) ->
        std::string
{
    return ldapBindDN(val, false);
}

int Config::_callback_set_LDAPBindDN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ldapBindDN(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_LDAPBindDN =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Config::ldapBindDNPassword() const ->
        std::string
{
    return _ldapBindDNPassword;
}

int Config::_callback_get_LDAPBindDNPassword(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ldapBindDNPassword();
                    }
                ));
    }
}

auto Config::ldapBindDNPassword(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ldapBindDNPassword != value)
    {
        _ldapBindDNPassword = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("LDAPBindDNPassword");
        }
    }

    return _ldapBindDNPassword;
}

auto Config::ldapBindDNPassword(std::string val) ->
        std::string
{
    return ldapBindDNPassword(val, false);
}

int Config::_callback_set_LDAPBindDNPassword(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ldapBindDNPassword(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_LDAPBindDNPassword =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Config::ldapBaseDN() const ->
        std::string
{
    return _ldapBaseDN;
}

int Config::_callback_get_LDAPBaseDN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ldapBaseDN();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Config::ldapBaseDN(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ldapBaseDN != value)
    {
        _ldapBaseDN = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("LDAPBaseDN");
        }
    }

    return _ldapBaseDN;
}

auto Config::ldapBaseDN(std::string val) ->
        std::string
{
    return ldapBaseDN(val, false);
}

int Config::_callback_set_LDAPBaseDN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ldapBaseDN(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_LDAPBaseDN =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Config::ldapSearchScope() const ->
        SearchScope
{
    return _ldapSearchScope;
}

int Config::_callback_get_LDAPSearchScope(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ldapSearchScope();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Config::ldapSearchScope(SearchScope value,
                                         bool skipSignal) ->
        SearchScope
{
    if (_ldapSearchScope != value)
    {
        _ldapSearchScope = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("LDAPSearchScope");
        }
    }

    return _ldapSearchScope;
}

auto Config::ldapSearchScope(SearchScope val) ->
        SearchScope
{
    return ldapSearchScope(val, false);
}

int Config::_callback_set_LDAPSearchScope(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](SearchScope&& arg)
                    {
                        o->ldapSearchScope(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_LDAPSearchScope =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::User::Ldap::server::Config::SearchScope>());
}
}

auto Config::ldapType() const ->
        Type
{
    return _ldapType;
}

int Config::_callback_get_LDAPType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ldapType();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Config::ldapType(Type value,
                                         bool skipSignal) ->
        Type
{
    if (_ldapType != value)
    {
        _ldapType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("LDAPType");
        }
    }

    return _ldapType;
}

auto Config::ldapType(Type val) ->
        Type
{
    return ldapType(val, false);
}

int Config::_callback_set_LDAPType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Type&& arg)
                    {
                        o->ldapType(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_LDAPType =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::User::Ldap::server::Config::Type>());
}
}

auto Config::groupNameAttribute() const ->
        std::string
{
    return _groupNameAttribute;
}

int Config::_callback_get_GroupNameAttribute(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->groupNameAttribute();
                    }
                ));
    }
}

auto Config::groupNameAttribute(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_groupNameAttribute != value)
    {
        _groupNameAttribute = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("GroupNameAttribute");
        }
    }

    return _groupNameAttribute;
}

auto Config::groupNameAttribute(std::string val) ->
        std::string
{
    return groupNameAttribute(val, false);
}

int Config::_callback_set_GroupNameAttribute(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->groupNameAttribute(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_GroupNameAttribute =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Config::userNameAttribute() const ->
        std::string
{
    return _userNameAttribute;
}

int Config::_callback_get_UserNameAttribute(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userNameAttribute();
                    }
                ));
    }
}

auto Config::userNameAttribute(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_userNameAttribute != value)
    {
        _userNameAttribute = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.property_changed("UserNameAttribute");
        }
    }

    return _userNameAttribute;
}

auto Config::userNameAttribute(std::string val) ->
        std::string
{
    return userNameAttribute(val, false);
}

int Config::_callback_set_UserNameAttribute(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Config*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->userNameAttribute(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Config
{
static const auto _property_UserNameAttribute =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Config::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "LDAPServerURI")
    {
        auto& v = std::get<std::string>(val);
        ldapServerURI(v, skipSignal);
        return;
    }
    if (_name == "LDAPBindDN")
    {
        auto& v = std::get<std::string>(val);
        ldapBindDN(v, skipSignal);
        return;
    }
    if (_name == "LDAPBindDNPassword")
    {
        auto& v = std::get<std::string>(val);
        ldapBindDNPassword(v, skipSignal);
        return;
    }
    if (_name == "LDAPBaseDN")
    {
        auto& v = std::get<std::string>(val);
        ldapBaseDN(v, skipSignal);
        return;
    }
    if (_name == "LDAPSearchScope")
    {
        auto& v = std::get<SearchScope>(val);
        ldapSearchScope(v, skipSignal);
        return;
    }
    if (_name == "LDAPType")
    {
        auto& v = std::get<Type>(val);
        ldapType(v, skipSignal);
        return;
    }
    if (_name == "GroupNameAttribute")
    {
        auto& v = std::get<std::string>(val);
        groupNameAttribute(v, skipSignal);
        return;
    }
    if (_name == "UserNameAttribute")
    {
        auto& v = std::get<std::string>(val);
        userNameAttribute(v, skipSignal);
        return;
    }
}

auto Config::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "LDAPServerURI")
    {
        return ldapServerURI();
    }
    if (_name == "LDAPBindDN")
    {
        return ldapBindDN();
    }
    if (_name == "LDAPBindDNPassword")
    {
        return ldapBindDNPassword();
    }
    if (_name == "LDAPBaseDN")
    {
        return ldapBaseDN();
    }
    if (_name == "LDAPSearchScope")
    {
        return ldapSearchScope();
    }
    if (_name == "LDAPType")
    {
        return ldapType();
    }
    if (_name == "GroupNameAttribute")
    {
        return groupNameAttribute();
    }
    if (_name == "UserNameAttribute")
    {
        return userNameAttribute();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Config::SearchScope */
static const std::tuple<const char*, Config::SearchScope> mappingConfigSearchScope[] =
        {
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Config.SearchScope.sub",                 Config::SearchScope::sub ),
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Config.SearchScope.one",                 Config::SearchScope::one ),
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Config.SearchScope.base",                 Config::SearchScope::base ),
        };

} // anonymous namespace

auto Config::convertStringToSearchScope(const std::string& s) noexcept ->
        std::optional<SearchScope>
{
    auto i = std::find_if(
            std::begin(mappingConfigSearchScope),
            std::end(mappingConfigSearchScope),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingConfigSearchScope) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Config::convertSearchScopeFromString(const std::string& s) ->
        SearchScope
{
    auto r = convertStringToSearchScope(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Config::convertSearchScopeToString(Config::SearchScope v)
{
    auto i = std::find_if(
            std::begin(mappingConfigSearchScope),
            std::end(mappingConfigSearchScope),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingConfigSearchScope))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Config::Type */
static const std::tuple<const char*, Config::Type> mappingConfigType[] =
        {
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Config.Type.ActiveDirectory",                 Config::Type::ActiveDirectory ),
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Config.Type.OpenLdap",                 Config::Type::OpenLdap ),
        };

} // anonymous namespace

auto Config::convertStringToType(const std::string& s) noexcept ->
        std::optional<Type>
{
    auto i = std::find_if(
            std::begin(mappingConfigType),
            std::end(mappingConfigType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingConfigType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Config::convertTypeFromString(const std::string& s) ->
        Type
{
    auto r = convertStringToType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Config::convertTypeToString(Config::Type v)
{
    auto i = std::find_if(
            std::begin(mappingConfigType),
            std::end(mappingConfigType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingConfigType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Config::_vtable[] = {
    vtable::start(),
    vtable::property("LDAPServerURI",
                     details::Config::_property_LDAPServerURI
                        .data(),
                     _callback_get_LDAPServerURI,
                     _callback_set_LDAPServerURI,
                     vtable::property_::emits_change),
    vtable::property("LDAPBindDN",
                     details::Config::_property_LDAPBindDN
                        .data(),
                     _callback_get_LDAPBindDN,
                     _callback_set_LDAPBindDN,
                     vtable::property_::emits_change),
    vtable::property("LDAPBindDNPassword",
                     details::Config::_property_LDAPBindDNPassword
                        .data(),
                     _callback_get_LDAPBindDNPassword,
                     _callback_set_LDAPBindDNPassword,
                     vtable::property_::emits_change),
    vtable::property("LDAPBaseDN",
                     details::Config::_property_LDAPBaseDN
                        .data(),
                     _callback_get_LDAPBaseDN,
                     _callback_set_LDAPBaseDN,
                     vtable::property_::emits_change),
    vtable::property("LDAPSearchScope",
                     details::Config::_property_LDAPSearchScope
                        .data(),
                     _callback_get_LDAPSearchScope,
                     _callback_set_LDAPSearchScope,
                     vtable::property_::emits_change),
    vtable::property("LDAPType",
                     details::Config::_property_LDAPType
                        .data(),
                     _callback_get_LDAPType,
                     _callback_set_LDAPType,
                     vtable::property_::emits_change),
    vtable::property("GroupNameAttribute",
                     details::Config::_property_GroupNameAttribute
                        .data(),
                     _callback_get_GroupNameAttribute,
                     _callback_set_GroupNameAttribute,
                     vtable::property_::emits_change),
    vtable::property("UserNameAttribute",
                     details::Config::_property_UserNameAttribute
                        .data(),
                     _callback_get_UserNameAttribute,
                     _callback_set_UserNameAttribute,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Ldap
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

