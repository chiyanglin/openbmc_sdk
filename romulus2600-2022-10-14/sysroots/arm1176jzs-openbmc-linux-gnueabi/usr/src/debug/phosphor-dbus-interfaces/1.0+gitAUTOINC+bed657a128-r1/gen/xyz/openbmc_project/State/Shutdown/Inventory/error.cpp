#include <xyz/openbmc_project/State/Shutdown/Inventory/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Shutdown
{
namespace Inventory
{
namespace Error
{
const char* Fan::name() const noexcept
{
    return errName;
}
const char* Fan::description() const noexcept
{
    return errDesc;
}
const char* Fan::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Inventory
} // namespace Shutdown
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

