#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/Manager/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

Manager::Manager(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_Manager_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Manager::Manager(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Manager(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Manager::_callback_CreateUser(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& userName, std::vector<std::string>&& groupNames, std::string&& privilege, bool&& enabled)
                    {
                        return o->createUser(
                                userName, groupNames, privilege, enabled);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InsufficientPermission& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNameExists& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNameGroupFail& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNamePrivFail& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::NoResource& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_CreateUser =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::vector<std::string>, std::string, bool>());
static const auto _return_CreateUser =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Manager::_callback_RenameUser(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& userName, std::string&& newUserName)
                    {
                        return o->renameUser(
                                userName, newUserName);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InsufficientPermission& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNameDoesNotExist& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNameExists& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNameGroupFail& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNamePrivFail& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::NoResource& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_RenameUser =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string>());
static const auto _return_RenameUser =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Manager::_callback_GetUserInfo(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& userName)
                    {
                        return o->getUserInfo(
                                userName);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InsufficientPermission& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::UserNameDoesNotExist& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_GetUserInfo =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_GetUserInfo =
        utility::tuple_to_array(message::types::type_id<
                std::map<std::string, std::variant<std::string, std::vector<std::string>, bool>>>());
}
}


void Manager::userRenamed(
            std::string userName,
            std::string newUserName)
{
    auto& i = _xyz_openbmc_project_User_Manager_interface;
    auto m = i.new_signal("UserRenamed");

    m.append(userName, newUserName);
    m.signal_send();
}

namespace details
{
namespace Manager
{
static const auto _signal_UserRenamed =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string>());
}
}


auto Manager::allPrivileges() const ->
        std::vector<std::string>
{
    return _allPrivileges;
}

int Manager::_callback_get_AllPrivileges(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allPrivileges();
                    }
                ));
    }
}

auto Manager::allPrivileges(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_allPrivileges != value)
    {
        _allPrivileges = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Manager_interface.property_changed("AllPrivileges");
        }
    }

    return _allPrivileges;
}

auto Manager::allPrivileges(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return allPrivileges(val, false);
}


namespace details
{
namespace Manager
{
static const auto _property_AllPrivileges =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto Manager::allGroups() const ->
        std::vector<std::string>
{
    return _allGroups;
}

int Manager::_callback_get_AllGroups(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allGroups();
                    }
                ));
    }
}

auto Manager::allGroups(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_allGroups != value)
    {
        _allGroups = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Manager_interface.property_changed("AllGroups");
        }
    }

    return _allGroups;
}

auto Manager::allGroups(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return allGroups(val, false);
}


namespace details
{
namespace Manager
{
static const auto _property_AllGroups =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

void Manager::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AllPrivileges")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        allPrivileges(v, skipSignal);
        return;
    }
    if (_name == "AllGroups")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        allGroups(v, skipSignal);
        return;
    }
}

auto Manager::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AllPrivileges")
    {
        return allPrivileges();
    }
    if (_name == "AllGroups")
    {
        return allGroups();
    }

    return PropertiesVariant();
}


const vtable_t Manager::_vtable[] = {
    vtable::start(),

    vtable::method("CreateUser",
                   details::Manager::_param_CreateUser
                        .data(),
                   details::Manager::_return_CreateUser
                        .data(),
                   _callback_CreateUser),

    vtable::method("RenameUser",
                   details::Manager::_param_RenameUser
                        .data(),
                   details::Manager::_return_RenameUser
                        .data(),
                   _callback_RenameUser),

    vtable::method("GetUserInfo",
                   details::Manager::_param_GetUserInfo
                        .data(),
                   details::Manager::_return_GetUserInfo
                        .data(),
                   _callback_GetUserInfo),

    vtable::signal("UserRenamed",
                   details::Manager::_signal_UserRenamed
                        .data()),
    vtable::property("AllPrivileges",
                     details::Manager::_property_AllPrivileges
                        .data(),
                     _callback_get_AllPrivileges,
                     vtable::property_::const_),
    vtable::property("AllGroups",
                     details::Manager::_property_AllGroups
                        .data(),
                     _callback_get_AllGroups,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

