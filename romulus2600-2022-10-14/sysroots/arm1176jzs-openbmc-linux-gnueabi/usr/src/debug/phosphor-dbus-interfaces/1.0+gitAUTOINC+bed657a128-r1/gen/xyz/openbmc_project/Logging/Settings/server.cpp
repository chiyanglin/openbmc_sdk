#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Settings/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

Settings::Settings(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Settings_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Settings::Settings(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Settings(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Settings::quiesceOnHwError() const ->
        bool
{
    return _quiesceOnHwError;
}

int Settings::_callback_get_QuiesceOnHwError(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Settings*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->quiesceOnHwError();
                    }
                ));
    }
}

auto Settings::quiesceOnHwError(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_quiesceOnHwError != value)
    {
        _quiesceOnHwError = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Settings_interface.property_changed("QuiesceOnHwError");
        }
    }

    return _quiesceOnHwError;
}

auto Settings::quiesceOnHwError(bool val) ->
        bool
{
    return quiesceOnHwError(val, false);
}

int Settings::_callback_set_QuiesceOnHwError(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Settings*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->quiesceOnHwError(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Settings
{
static const auto _property_QuiesceOnHwError =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Settings::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "QuiesceOnHwError")
    {
        auto& v = std::get<bool>(val);
        quiesceOnHwError(v, skipSignal);
        return;
    }
}

auto Settings::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "QuiesceOnHwError")
    {
        return quiesceOnHwError();
    }

    return PropertiesVariant();
}


const vtable_t Settings::_vtable[] = {
    vtable::start(),
    vtable::property("QuiesceOnHwError",
                     details::Settings::_property_QuiesceOnHwError
                        .data(),
                     _callback_get_QuiesceOnHwError,
                     _callback_set_QuiesceOnHwError,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

