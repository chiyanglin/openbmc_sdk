#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/BMC/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

BMC::BMC(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_BMC_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

BMC::BMC(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : BMC(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto BMC::requestedBMCTransition() const ->
        Transition
{
    return _requestedBMCTransition;
}

int BMC::_callback_get_RequestedBMCTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->requestedBMCTransition();
                    }
                ));
    }
}

auto BMC::requestedBMCTransition(Transition value,
                                         bool skipSignal) ->
        Transition
{
    if (_requestedBMCTransition != value)
    {
        _requestedBMCTransition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_BMC_interface.property_changed("RequestedBMCTransition");
        }
    }

    return _requestedBMCTransition;
}

auto BMC::requestedBMCTransition(Transition val) ->
        Transition
{
    return requestedBMCTransition(val, false);
}

int BMC::_callback_set_RequestedBMCTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Transition&& arg)
                    {
                        o->requestedBMCTransition(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace BMC
{
static const auto _property_RequestedBMCTransition =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::BMC::Transition>());
}
}

auto BMC::currentBMCState() const ->
        BMCState
{
    return _currentBMCState;
}

int BMC::_callback_get_CurrentBMCState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentBMCState();
                    }
                ));
    }
}

auto BMC::currentBMCState(BMCState value,
                                         bool skipSignal) ->
        BMCState
{
    if (_currentBMCState != value)
    {
        _currentBMCState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_BMC_interface.property_changed("CurrentBMCState");
        }
    }

    return _currentBMCState;
}

auto BMC::currentBMCState(BMCState val) ->
        BMCState
{
    return currentBMCState(val, false);
}

int BMC::_callback_set_CurrentBMCState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](BMCState&& arg)
                    {
                        o->currentBMCState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace BMC
{
static const auto _property_CurrentBMCState =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::BMC::BMCState>());
}
}

auto BMC::lastRebootTime() const ->
        uint64_t
{
    return _lastRebootTime;
}

int BMC::_callback_get_LastRebootTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lastRebootTime();
                    }
                ));
    }
}

auto BMC::lastRebootTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_lastRebootTime != value)
    {
        _lastRebootTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_BMC_interface.property_changed("LastRebootTime");
        }
    }

    return _lastRebootTime;
}

auto BMC::lastRebootTime(uint64_t val) ->
        uint64_t
{
    return lastRebootTime(val, false);
}

int BMC::_callback_set_LastRebootTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->lastRebootTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace BMC
{
static const auto _property_LastRebootTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto BMC::lastRebootCause() const ->
        RebootCause
{
    return _lastRebootCause;
}

int BMC::_callback_get_LastRebootCause(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lastRebootCause();
                    }
                ));
    }
}

auto BMC::lastRebootCause(RebootCause value,
                                         bool skipSignal) ->
        RebootCause
{
    if (_lastRebootCause != value)
    {
        _lastRebootCause = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_BMC_interface.property_changed("LastRebootCause");
        }
    }

    return _lastRebootCause;
}

auto BMC::lastRebootCause(RebootCause val) ->
        RebootCause
{
    return lastRebootCause(val, false);
}

int BMC::_callback_set_LastRebootCause(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](RebootCause&& arg)
                    {
                        o->lastRebootCause(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace BMC
{
static const auto _property_LastRebootCause =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::BMC::RebootCause>());
}
}

void BMC::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RequestedBMCTransition")
    {
        auto& v = std::get<Transition>(val);
        requestedBMCTransition(v, skipSignal);
        return;
    }
    if (_name == "CurrentBMCState")
    {
        auto& v = std::get<BMCState>(val);
        currentBMCState(v, skipSignal);
        return;
    }
    if (_name == "LastRebootTime")
    {
        auto& v = std::get<uint64_t>(val);
        lastRebootTime(v, skipSignal);
        return;
    }
    if (_name == "LastRebootCause")
    {
        auto& v = std::get<RebootCause>(val);
        lastRebootCause(v, skipSignal);
        return;
    }
}

auto BMC::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RequestedBMCTransition")
    {
        return requestedBMCTransition();
    }
    if (_name == "CurrentBMCState")
    {
        return currentBMCState();
    }
    if (_name == "LastRebootTime")
    {
        return lastRebootTime();
    }
    if (_name == "LastRebootCause")
    {
        return lastRebootCause();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for BMC::Transition */
static const std::tuple<const char*, BMC::Transition> mappingBMCTransition[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.BMC.Transition.Reboot",                 BMC::Transition::Reboot ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.Transition.HardReboot",                 BMC::Transition::HardReboot ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.Transition.None",                 BMC::Transition::None ),
        };

} // anonymous namespace

auto BMC::convertStringToTransition(const std::string& s) noexcept ->
        std::optional<Transition>
{
    auto i = std::find_if(
            std::begin(mappingBMCTransition),
            std::end(mappingBMCTransition),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingBMCTransition) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto BMC::convertTransitionFromString(const std::string& s) ->
        Transition
{
    auto r = convertStringToTransition(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string BMC::convertTransitionToString(BMC::Transition v)
{
    auto i = std::find_if(
            std::begin(mappingBMCTransition),
            std::end(mappingBMCTransition),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingBMCTransition))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for BMC::BMCState */
static const std::tuple<const char*, BMC::BMCState> mappingBMCBMCState[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.BMC.BMCState.Ready",                 BMC::BMCState::Ready ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.BMCState.NotReady",                 BMC::BMCState::NotReady ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.BMCState.UpdateInProgress",                 BMC::BMCState::UpdateInProgress ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.BMCState.Quiesced",                 BMC::BMCState::Quiesced ),
        };

} // anonymous namespace

auto BMC::convertStringToBMCState(const std::string& s) noexcept ->
        std::optional<BMCState>
{
    auto i = std::find_if(
            std::begin(mappingBMCBMCState),
            std::end(mappingBMCBMCState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingBMCBMCState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto BMC::convertBMCStateFromString(const std::string& s) ->
        BMCState
{
    auto r = convertStringToBMCState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string BMC::convertBMCStateToString(BMC::BMCState v)
{
    auto i = std::find_if(
            std::begin(mappingBMCBMCState),
            std::end(mappingBMCBMCState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingBMCBMCState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for BMC::RebootCause */
static const std::tuple<const char*, BMC::RebootCause> mappingBMCRebootCause[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.BMC.RebootCause.POR",                 BMC::RebootCause::POR ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.RebootCause.PinholeReset",                 BMC::RebootCause::PinholeReset ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.RebootCause.Watchdog",                 BMC::RebootCause::Watchdog ),
            std::make_tuple( "xyz.openbmc_project.State.BMC.RebootCause.Unknown",                 BMC::RebootCause::Unknown ),
        };

} // anonymous namespace

auto BMC::convertStringToRebootCause(const std::string& s) noexcept ->
        std::optional<RebootCause>
{
    auto i = std::find_if(
            std::begin(mappingBMCRebootCause),
            std::end(mappingBMCRebootCause),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingBMCRebootCause) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto BMC::convertRebootCauseFromString(const std::string& s) ->
        RebootCause
{
    auto r = convertStringToRebootCause(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string BMC::convertRebootCauseToString(BMC::RebootCause v)
{
    auto i = std::find_if(
            std::begin(mappingBMCRebootCause),
            std::end(mappingBMCRebootCause),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingBMCRebootCause))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t BMC::_vtable[] = {
    vtable::start(),
    vtable::property("RequestedBMCTransition",
                     details::BMC::_property_RequestedBMCTransition
                        .data(),
                     _callback_get_RequestedBMCTransition,
                     _callback_set_RequestedBMCTransition,
                     vtable::property_::emits_change),
    vtable::property("CurrentBMCState",
                     details::BMC::_property_CurrentBMCState
                        .data(),
                     _callback_get_CurrentBMCState,
                     _callback_set_CurrentBMCState,
                     vtable::property_::emits_change),
    vtable::property("LastRebootTime",
                     details::BMC::_property_LastRebootTime
                        .data(),
                     _callback_get_LastRebootTime,
                     _callback_set_LastRebootTime,
                     vtable::property_::emits_change),
    vtable::property("LastRebootCause",
                     details::BMC::_property_LastRebootCause
                        .data(),
                     _callback_get_LastRebootCause,
                     _callback_set_LastRebootCause,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

