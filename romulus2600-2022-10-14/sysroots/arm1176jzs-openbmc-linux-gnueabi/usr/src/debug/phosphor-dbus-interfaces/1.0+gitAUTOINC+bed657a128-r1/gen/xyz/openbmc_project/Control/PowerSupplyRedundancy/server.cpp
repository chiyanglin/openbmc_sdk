#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/PowerSupplyRedundancy/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

PowerSupplyRedundancy::PowerSupplyRedundancy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PowerSupplyRedundancy::PowerSupplyRedundancy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PowerSupplyRedundancy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PowerSupplyRedundancy::powerSupplyRedundancyEnabled() const ->
        bool
{
    return _powerSupplyRedundancyEnabled;
}

int PowerSupplyRedundancy::_callback_get_PowerSupplyRedundancyEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerSupplyRedundancyEnabled();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::powerSupplyRedundancyEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_powerSupplyRedundancyEnabled != value)
    {
        _powerSupplyRedundancyEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("PowerSupplyRedundancyEnabled");
        }
    }

    return _powerSupplyRedundancyEnabled;
}

auto PowerSupplyRedundancy::powerSupplyRedundancyEnabled(bool val) ->
        bool
{
    return powerSupplyRedundancyEnabled(val, false);
}


namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_PowerSupplyRedundancyEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PowerSupplyRedundancy::rotationEnabled() const ->
        bool
{
    return _rotationEnabled;
}

int PowerSupplyRedundancy::_callback_get_RotationEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rotationEnabled();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::rotationEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_rotationEnabled != value)
    {
        _rotationEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("RotationEnabled");
        }
    }

    return _rotationEnabled;
}

auto PowerSupplyRedundancy::rotationEnabled(bool val) ->
        bool
{
    return rotationEnabled(val, false);
}

int PowerSupplyRedundancy::_callback_set_RotationEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->rotationEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_RotationEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PowerSupplyRedundancy::rotationAlgorithm() const ->
        Algo
{
    return _rotationAlgorithm;
}

int PowerSupplyRedundancy::_callback_get_RotationAlgorithm(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rotationAlgorithm();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::rotationAlgorithm(Algo value,
                                         bool skipSignal) ->
        Algo
{
    if (_rotationAlgorithm != value)
    {
        _rotationAlgorithm = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("RotationAlgorithm");
        }
    }

    return _rotationAlgorithm;
}

auto PowerSupplyRedundancy::rotationAlgorithm(Algo val) ->
        Algo
{
    return rotationAlgorithm(val, false);
}

int PowerSupplyRedundancy::_callback_set_RotationAlgorithm(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Algo&& arg)
                    {
                        o->rotationAlgorithm(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_RotationAlgorithm =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Algo>());
}
}

auto PowerSupplyRedundancy::rotationRankOrder() const ->
        std::vector<uint8_t>
{
    return _rotationRankOrder;
}

int PowerSupplyRedundancy::_callback_get_RotationRankOrder(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rotationRankOrder();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::rotationRankOrder(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rotationRankOrder != value)
    {
        _rotationRankOrder = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("RotationRankOrder");
        }
    }

    return _rotationRankOrder;
}

auto PowerSupplyRedundancy::rotationRankOrder(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rotationRankOrder(val, false);
}

int PowerSupplyRedundancy::_callback_set_RotationRankOrder(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rotationRankOrder(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_RotationRankOrder =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto PowerSupplyRedundancy::periodOfRotation() const ->
        uint32_t
{
    return _periodOfRotation;
}

int PowerSupplyRedundancy::_callback_get_PeriodOfRotation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->periodOfRotation();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::periodOfRotation(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_periodOfRotation != value)
    {
        _periodOfRotation = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("PeriodOfRotation");
        }
    }

    return _periodOfRotation;
}

auto PowerSupplyRedundancy::periodOfRotation(uint32_t val) ->
        uint32_t
{
    return periodOfRotation(val, false);
}

int PowerSupplyRedundancy::_callback_set_PeriodOfRotation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->periodOfRotation(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_PeriodOfRotation =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto PowerSupplyRedundancy::coldRedundancyStatus() const ->
        Status
{
    return _coldRedundancyStatus;
}

int PowerSupplyRedundancy::_callback_get_ColdRedundancyStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->coldRedundancyStatus();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::coldRedundancyStatus(Status value,
                                         bool skipSignal) ->
        Status
{
    if (_coldRedundancyStatus != value)
    {
        _coldRedundancyStatus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("ColdRedundancyStatus");
        }
    }

    return _coldRedundancyStatus;
}

auto PowerSupplyRedundancy::coldRedundancyStatus(Status val) ->
        Status
{
    return coldRedundancyStatus(val, false);
}

int PowerSupplyRedundancy::_callback_set_ColdRedundancyStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Status&& arg)
                    {
                        o->coldRedundancyStatus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_ColdRedundancyStatus =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Status>());
}
}

auto PowerSupplyRedundancy::psuNumber() const ->
        uint8_t
{
    return _psuNumber;
}

int PowerSupplyRedundancy::_callback_get_PSUNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->psuNumber();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::psuNumber(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_psuNumber != value)
    {
        _psuNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("PSUNumber");
        }
    }

    return _psuNumber;
}

auto PowerSupplyRedundancy::psuNumber(uint8_t val) ->
        uint8_t
{
    return psuNumber(val, false);
}

int PowerSupplyRedundancy::_callback_set_PSUNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->psuNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_PSUNumber =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto PowerSupplyRedundancy::redundantCount() const ->
        uint8_t
{
    return _redundantCount;
}

int PowerSupplyRedundancy::_callback_get_RedundantCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->redundantCount();
                    }
                ));
    }
}

auto PowerSupplyRedundancy::redundantCount(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_redundantCount != value)
    {
        _redundantCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.property_changed("RedundantCount");
        }
    }

    return _redundantCount;
}

auto PowerSupplyRedundancy::redundantCount(uint8_t val) ->
        uint8_t
{
    return redundantCount(val, false);
}

int PowerSupplyRedundancy::_callback_set_RedundantCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->redundantCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSupplyRedundancy
{
static const auto _property_RedundantCount =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void PowerSupplyRedundancy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PowerSupplyRedundancyEnabled")
    {
        auto& v = std::get<bool>(val);
        powerSupplyRedundancyEnabled(v, skipSignal);
        return;
    }
    if (_name == "RotationEnabled")
    {
        auto& v = std::get<bool>(val);
        rotationEnabled(v, skipSignal);
        return;
    }
    if (_name == "RotationAlgorithm")
    {
        auto& v = std::get<Algo>(val);
        rotationAlgorithm(v, skipSignal);
        return;
    }
    if (_name == "RotationRankOrder")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rotationRankOrder(v, skipSignal);
        return;
    }
    if (_name == "PeriodOfRotation")
    {
        auto& v = std::get<uint32_t>(val);
        periodOfRotation(v, skipSignal);
        return;
    }
    if (_name == "ColdRedundancyStatus")
    {
        auto& v = std::get<Status>(val);
        coldRedundancyStatus(v, skipSignal);
        return;
    }
    if (_name == "PSUNumber")
    {
        auto& v = std::get<uint8_t>(val);
        psuNumber(v, skipSignal);
        return;
    }
    if (_name == "RedundantCount")
    {
        auto& v = std::get<uint8_t>(val);
        redundantCount(v, skipSignal);
        return;
    }
}

auto PowerSupplyRedundancy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PowerSupplyRedundancyEnabled")
    {
        return powerSupplyRedundancyEnabled();
    }
    if (_name == "RotationEnabled")
    {
        return rotationEnabled();
    }
    if (_name == "RotationAlgorithm")
    {
        return rotationAlgorithm();
    }
    if (_name == "RotationRankOrder")
    {
        return rotationRankOrder();
    }
    if (_name == "PeriodOfRotation")
    {
        return periodOfRotation();
    }
    if (_name == "ColdRedundancyStatus")
    {
        return coldRedundancyStatus();
    }
    if (_name == "PSUNumber")
    {
        return psuNumber();
    }
    if (_name == "RedundantCount")
    {
        return redundantCount();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for PowerSupplyRedundancy::Algo */
static const std::tuple<const char*, PowerSupplyRedundancy::Algo> mappingPowerSupplyRedundancyAlgo[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.PowerSupplyRedundancy.Algo.bmcSpecific",                 PowerSupplyRedundancy::Algo::bmcSpecific ),
            std::make_tuple( "xyz.openbmc_project.Control.PowerSupplyRedundancy.Algo.userSpecific",                 PowerSupplyRedundancy::Algo::userSpecific ),
        };

} // anonymous namespace

auto PowerSupplyRedundancy::convertStringToAlgo(const std::string& s) noexcept ->
        std::optional<Algo>
{
    auto i = std::find_if(
            std::begin(mappingPowerSupplyRedundancyAlgo),
            std::end(mappingPowerSupplyRedundancyAlgo),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPowerSupplyRedundancyAlgo) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PowerSupplyRedundancy::convertAlgoFromString(const std::string& s) ->
        Algo
{
    auto r = convertStringToAlgo(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PowerSupplyRedundancy::convertAlgoToString(PowerSupplyRedundancy::Algo v)
{
    auto i = std::find_if(
            std::begin(mappingPowerSupplyRedundancyAlgo),
            std::end(mappingPowerSupplyRedundancyAlgo),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPowerSupplyRedundancyAlgo))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for PowerSupplyRedundancy::Status */
static const std::tuple<const char*, PowerSupplyRedundancy::Status> mappingPowerSupplyRedundancyStatus[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.PowerSupplyRedundancy.Status.inProgress",                 PowerSupplyRedundancy::Status::inProgress ),
            std::make_tuple( "xyz.openbmc_project.Control.PowerSupplyRedundancy.Status.completed",                 PowerSupplyRedundancy::Status::completed ),
        };

} // anonymous namespace

auto PowerSupplyRedundancy::convertStringToStatus(const std::string& s) noexcept ->
        std::optional<Status>
{
    auto i = std::find_if(
            std::begin(mappingPowerSupplyRedundancyStatus),
            std::end(mappingPowerSupplyRedundancyStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPowerSupplyRedundancyStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PowerSupplyRedundancy::convertStatusFromString(const std::string& s) ->
        Status
{
    auto r = convertStringToStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PowerSupplyRedundancy::convertStatusToString(PowerSupplyRedundancy::Status v)
{
    auto i = std::find_if(
            std::begin(mappingPowerSupplyRedundancyStatus),
            std::end(mappingPowerSupplyRedundancyStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPowerSupplyRedundancyStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t PowerSupplyRedundancy::_vtable[] = {
    vtable::start(),
    vtable::property("PowerSupplyRedundancyEnabled",
                     details::PowerSupplyRedundancy::_property_PowerSupplyRedundancyEnabled
                        .data(),
                     _callback_get_PowerSupplyRedundancyEnabled,
                     vtable::property_::const_),
    vtable::property("RotationEnabled",
                     details::PowerSupplyRedundancy::_property_RotationEnabled
                        .data(),
                     _callback_get_RotationEnabled,
                     _callback_set_RotationEnabled,
                     vtable::property_::emits_change),
    vtable::property("RotationAlgorithm",
                     details::PowerSupplyRedundancy::_property_RotationAlgorithm
                        .data(),
                     _callback_get_RotationAlgorithm,
                     _callback_set_RotationAlgorithm,
                     vtable::property_::emits_change),
    vtable::property("RotationRankOrder",
                     details::PowerSupplyRedundancy::_property_RotationRankOrder
                        .data(),
                     _callback_get_RotationRankOrder,
                     _callback_set_RotationRankOrder,
                     vtable::property_::emits_change),
    vtable::property("PeriodOfRotation",
                     details::PowerSupplyRedundancy::_property_PeriodOfRotation
                        .data(),
                     _callback_get_PeriodOfRotation,
                     _callback_set_PeriodOfRotation,
                     vtable::property_::emits_change),
    vtable::property("ColdRedundancyStatus",
                     details::PowerSupplyRedundancy::_property_ColdRedundancyStatus
                        .data(),
                     _callback_get_ColdRedundancyStatus,
                     _callback_set_ColdRedundancyStatus,
                     vtable::property_::emits_change),
    vtable::property("PSUNumber",
                     details::PowerSupplyRedundancy::_property_PSUNumber
                        .data(),
                     _callback_get_PSUNumber,
                     _callback_set_PSUNumber,
                     vtable::property_::emits_change),
    vtable::property("RedundantCount",
                     details::PowerSupplyRedundancy::_property_RedundantCount
                        .data(),
                     _callback_get_RedundantCount,
                     _callback_set_RedundantCount,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

