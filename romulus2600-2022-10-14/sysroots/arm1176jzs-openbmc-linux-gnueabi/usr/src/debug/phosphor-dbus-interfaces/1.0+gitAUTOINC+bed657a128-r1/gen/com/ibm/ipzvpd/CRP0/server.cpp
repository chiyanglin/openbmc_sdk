#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/CRP0/server.hpp>


















namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

CRP0::CRP0(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_CRP0_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CRP0::CRP0(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CRP0(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CRP0::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int CRP0::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto CRP0::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto CRP0::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int CRP0::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int CRP0::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto CRP0::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto CRP0::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int CRP0::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::ed() const ->
        std::vector<uint8_t>
{
    return _ed;
}

int CRP0::_callback_get_ED(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ed();
                    }
                ));
    }
}

auto CRP0::ed(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ed != value)
    {
        _ed = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("ED");
        }
    }

    return _ed;
}

auto CRP0::ed(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ed(val, false);
}

int CRP0::_callback_set_ED(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ed(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_ED =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::te() const ->
        std::vector<uint8_t>
{
    return _te;
}

int CRP0::_callback_get_TE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->te();
                    }
                ));
    }
}

auto CRP0::te(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_te != value)
    {
        _te = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("TE");
        }
    }

    return _te;
}

auto CRP0::te(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return te(val, false);
}

int CRP0::_callback_set_TE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->te(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_TE =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::dd() const ->
        std::vector<uint8_t>
{
    return _dd;
}

int CRP0::_callback_get_DD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dd();
                    }
                ));
    }
}

auto CRP0::dd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dd != value)
    {
        _dd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("DD");
        }
    }

    return _dd;
}

auto CRP0::dd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dd(val, false);
}

int CRP0::_callback_set_DD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_DD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::dn() const ->
        std::vector<uint8_t>
{
    return _dn;
}

int CRP0::_callback_get_DN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dn();
                    }
                ));
    }
}

auto CRP0::dn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dn != value)
    {
        _dn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("DN");
        }
    }

    return _dn;
}

auto CRP0::dn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dn(val, false);
}

int CRP0::_callback_set_DN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_DN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::iq() const ->
        std::vector<uint8_t>
{
    return _iq;
}

int CRP0::_callback_get_IQ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->iq();
                    }
                ));
    }
}

auto CRP0::iq(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_iq != value)
    {
        _iq = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("IQ");
        }
    }

    return _iq;
}

auto CRP0::iq(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return iq(val, false);
}

int CRP0::_callback_set_IQ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->iq(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_IQ =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::tc() const ->
        std::vector<uint8_t>
{
    return _tc;
}

int CRP0::_callback_get_TC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tc();
                    }
                ));
    }
}

auto CRP0::tc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tc != value)
    {
        _tc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("TC");
        }
    }

    return _tc;
}

auto CRP0::tc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tc(val, false);
}

int CRP0::_callback_set_TC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_TC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::ta() const ->
        std::vector<uint8_t>
{
    return _ta;
}

int CRP0::_callback_get_TA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ta();
                    }
                ));
    }
}

auto CRP0::ta(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ta != value)
    {
        _ta = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("TA");
        }
    }

    return _ta;
}

auto CRP0::ta(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ta(val, false);
}

int CRP0::_callback_set_TA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ta(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_TA =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::do_() const ->
        std::vector<uint8_t>
{
    return _do_;
}

int CRP0::_callback_get_DO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->do_();
                    }
                ));
    }
}

auto CRP0::do_(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_do_ != value)
    {
        _do_ = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("DO");
        }
    }

    return _do_;
}

auto CRP0::do_(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return do_(val, false);
}

int CRP0::_callback_set_DO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->do_(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_DO =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::pdw() const ->
        std::vector<uint8_t>
{
    return _pdw;
}

int CRP0::_callback_get_PD_W(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdw();
                    }
                ));
    }
}

auto CRP0::pdw(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdw != value)
    {
        _pdw = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("PD_W");
        }
    }

    return _pdw;
}

auto CRP0::pdw(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdw(val, false);
}

int CRP0::_callback_set_PD_W(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdw(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_PD_W =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::pdv() const ->
        std::vector<uint8_t>
{
    return _pdv;
}

int CRP0::_callback_get_PD_V(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdv();
                    }
                ));
    }
}

auto CRP0::pdv(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdv != value)
    {
        _pdv = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("PD_V");
        }
    }

    return _pdv;
}

auto CRP0::pdv(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdv(val, false);
}

int CRP0::_callback_set_PD_V(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdv(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_PD_V =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int CRP0::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto CRP0::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto CRP0::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int CRP0::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::d5() const ->
        std::vector<uint8_t>
{
    return _d5;
}

int CRP0::_callback_get_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d5();
                    }
                ));
    }
}

auto CRP0::d5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d5 != value)
    {
        _d5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("D5");
        }
    }

    return _d5;
}

auto CRP0::d5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d5(val, false);
}

int CRP0::_callback_set_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_D5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::d8() const ->
        std::vector<uint8_t>
{
    return _d8;
}

int CRP0::_callback_get_D8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d8();
                    }
                ));
    }
}

auto CRP0::d8(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d8 != value)
    {
        _d8 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("D8");
        }
    }

    return _d8;
}

auto CRP0::d8(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d8(val, false);
}

int CRP0::_callback_set_D8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d8(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_D8 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::f5() const ->
        std::vector<uint8_t>
{
    return _f5;
}

int CRP0::_callback_get_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f5();
                    }
                ));
    }
}

auto CRP0::f5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f5 != value)
    {
        _f5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("F5");
        }
    }

    return _f5;
}

auto CRP0::f5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f5(val, false);
}

int CRP0::_callback_set_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_F5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CRP0::ci() const ->
        std::vector<uint8_t>
{
    return _ci;
}

int CRP0::_callback_get_CI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ci();
                    }
                ));
    }
}

auto CRP0::ci(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ci != value)
    {
        _ci = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CRP0_interface.property_changed("CI");
        }
    }

    return _ci;
}

auto CRP0::ci(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ci(val, false);
}

int CRP0::_callback_set_CI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CRP0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ci(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CRP0
{
static const auto _property_CI =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void CRP0::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "ED")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ed(v, skipSignal);
        return;
    }
    if (_name == "TE")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        te(v, skipSignal);
        return;
    }
    if (_name == "DD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dd(v, skipSignal);
        return;
    }
    if (_name == "DN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dn(v, skipSignal);
        return;
    }
    if (_name == "IQ")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        iq(v, skipSignal);
        return;
    }
    if (_name == "TC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tc(v, skipSignal);
        return;
    }
    if (_name == "TA")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ta(v, skipSignal);
        return;
    }
    if (_name == "DO")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        do_(v, skipSignal);
        return;
    }
    if (_name == "PD_W")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdw(v, skipSignal);
        return;
    }
    if (_name == "PD_V")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdv(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
    if (_name == "D5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d5(v, skipSignal);
        return;
    }
    if (_name == "D8")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d8(v, skipSignal);
        return;
    }
    if (_name == "F5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f5(v, skipSignal);
        return;
    }
    if (_name == "CI")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ci(v, skipSignal);
        return;
    }
}

auto CRP0::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "ED")
    {
        return ed();
    }
    if (_name == "TE")
    {
        return te();
    }
    if (_name == "DD")
    {
        return dd();
    }
    if (_name == "DN")
    {
        return dn();
    }
    if (_name == "IQ")
    {
        return iq();
    }
    if (_name == "TC")
    {
        return tc();
    }
    if (_name == "TA")
    {
        return ta();
    }
    if (_name == "DO")
    {
        return do_();
    }
    if (_name == "PD_W")
    {
        return pdw();
    }
    if (_name == "PD_V")
    {
        return pdv();
    }
    if (_name == "D4")
    {
        return d4();
    }
    if (_name == "D5")
    {
        return d5();
    }
    if (_name == "D8")
    {
        return d8();
    }
    if (_name == "F5")
    {
        return f5();
    }
    if (_name == "CI")
    {
        return ci();
    }

    return PropertiesVariant();
}


const vtable_t CRP0::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::CRP0::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::CRP0::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("ED",
                     details::CRP0::_property_ED
                        .data(),
                     _callback_get_ED,
                     _callback_set_ED,
                     vtable::property_::emits_change),
    vtable::property("TE",
                     details::CRP0::_property_TE
                        .data(),
                     _callback_get_TE,
                     _callback_set_TE,
                     vtable::property_::emits_change),
    vtable::property("DD",
                     details::CRP0::_property_DD
                        .data(),
                     _callback_get_DD,
                     _callback_set_DD,
                     vtable::property_::emits_change),
    vtable::property("DN",
                     details::CRP0::_property_DN
                        .data(),
                     _callback_get_DN,
                     _callback_set_DN,
                     vtable::property_::emits_change),
    vtable::property("IQ",
                     details::CRP0::_property_IQ
                        .data(),
                     _callback_get_IQ,
                     _callback_set_IQ,
                     vtable::property_::emits_change),
    vtable::property("TC",
                     details::CRP0::_property_TC
                        .data(),
                     _callback_get_TC,
                     _callback_set_TC,
                     vtable::property_::emits_change),
    vtable::property("TA",
                     details::CRP0::_property_TA
                        .data(),
                     _callback_get_TA,
                     _callback_set_TA,
                     vtable::property_::emits_change),
    vtable::property("DO",
                     details::CRP0::_property_DO
                        .data(),
                     _callback_get_DO,
                     _callback_set_DO,
                     vtable::property_::emits_change),
    vtable::property("PD_W",
                     details::CRP0::_property_PD_W
                        .data(),
                     _callback_get_PD_W,
                     _callback_set_PD_W,
                     vtable::property_::emits_change),
    vtable::property("PD_V",
                     details::CRP0::_property_PD_V
                        .data(),
                     _callback_get_PD_V,
                     _callback_set_PD_V,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::CRP0::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::property("D5",
                     details::CRP0::_property_D5
                        .data(),
                     _callback_get_D5,
                     _callback_set_D5,
                     vtable::property_::emits_change),
    vtable::property("D8",
                     details::CRP0::_property_D8
                        .data(),
                     _callback_get_D8,
                     _callback_set_D8,
                     vtable::property_::emits_change),
    vtable::property("F5",
                     details::CRP0::_property_F5
                        .data(),
                     _callback_get_F5,
                     _callback_set_F5,
                     vtable::property_::emits_change),
    vtable::property("CI",
                     details::CRP0::_property_CI
                        .data(),
                     _callback_get_CI,
                     _callback_set_CI,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

