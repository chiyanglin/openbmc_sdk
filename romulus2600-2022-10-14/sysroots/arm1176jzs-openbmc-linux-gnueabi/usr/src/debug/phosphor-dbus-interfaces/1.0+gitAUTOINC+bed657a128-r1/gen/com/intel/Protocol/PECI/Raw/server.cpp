#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/intel/Protocol/PECI/Raw/server.hpp>


namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Protocol
{
namespace PECI
{
namespace server
{

Raw::Raw(bus_t& bus, const char* path)
        : _com_intel_Protocol_PECI_Raw_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Raw::_callback_Send(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Raw*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& device, std::vector<std::vector<uint8_t>>&& commands)
                    {
                        return o->send(
                                device, commands);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Raw
{
static const auto _param_Send =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::vector<std::vector<uint8_t>>>());
static const auto _return_Send =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::vector<uint8_t>>>());
}
}




const vtable_t Raw::_vtable[] = {
    vtable::start(),

    vtable::method("Send",
                   details::Raw::_param_Send
                        .data(),
                   details::Raw::_return_Send
                        .data(),
                   _callback_Send),
    vtable::end()
};

} // namespace server
} // namespace PECI
} // namespace Protocol
} // namespace intel
} // namespace com
} // namespace sdbusplus

