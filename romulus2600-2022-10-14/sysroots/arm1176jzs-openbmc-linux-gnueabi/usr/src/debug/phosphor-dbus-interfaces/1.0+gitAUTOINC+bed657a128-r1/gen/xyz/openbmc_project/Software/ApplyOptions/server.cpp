#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/ApplyOptions/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

ApplyOptions::ApplyOptions(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_ApplyOptions_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ApplyOptions::ApplyOptions(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ApplyOptions(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ApplyOptions::clearConfig() const ->
        bool
{
    return _clearConfig;
}

int ApplyOptions::_callback_get_ClearConfig(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ApplyOptions*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->clearConfig();
                    }
                ));
    }
}

auto ApplyOptions::clearConfig(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_clearConfig != value)
    {
        _clearConfig = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_ApplyOptions_interface.property_changed("ClearConfig");
        }
    }

    return _clearConfig;
}

auto ApplyOptions::clearConfig(bool val) ->
        bool
{
    return clearConfig(val, false);
}

int ApplyOptions::_callback_set_ClearConfig(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ApplyOptions*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->clearConfig(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ApplyOptions
{
static const auto _property_ClearConfig =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void ApplyOptions::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ClearConfig")
    {
        auto& v = std::get<bool>(val);
        clearConfig(v, skipSignal);
        return;
    }
}

auto ApplyOptions::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ClearConfig")
    {
        return clearConfig();
    }

    return PropertiesVariant();
}


const vtable_t ApplyOptions::_vtable[] = {
    vtable::start(),
    vtable::property("ClearConfig",
                     details::ApplyOptions::_property_ClearConfig
                        .data(),
                     _callback_get_ClearConfig,
                     _callback_set_ClearConfig,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

