#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/VendorInformation/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

VendorInformation::VendorInformation(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_VendorInformation_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VendorInformation::VendorInformation(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VendorInformation(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VendorInformation::customField1() const ->
        std::string
{
    return _customField1;
}

int VendorInformation::_callback_get_CustomField1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VendorInformation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->customField1();
                    }
                ));
    }
}

auto VendorInformation::customField1(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_customField1 != value)
    {
        _customField1 = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_VendorInformation_interface.property_changed("CustomField1");
        }
    }

    return _customField1;
}

auto VendorInformation::customField1(std::string val) ->
        std::string
{
    return customField1(val, false);
}

int VendorInformation::_callback_set_CustomField1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VendorInformation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->customField1(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VendorInformation
{
static const auto _property_CustomField1 =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto VendorInformation::customField2() const ->
        std::string
{
    return _customField2;
}

int VendorInformation::_callback_get_CustomField2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VendorInformation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->customField2();
                    }
                ));
    }
}

auto VendorInformation::customField2(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_customField2 != value)
    {
        _customField2 = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_VendorInformation_interface.property_changed("CustomField2");
        }
    }

    return _customField2;
}

auto VendorInformation::customField2(std::string val) ->
        std::string
{
    return customField2(val, false);
}

int VendorInformation::_callback_set_CustomField2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VendorInformation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->customField2(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VendorInformation
{
static const auto _property_CustomField2 =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void VendorInformation::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CustomField1")
    {
        auto& v = std::get<std::string>(val);
        customField1(v, skipSignal);
        return;
    }
    if (_name == "CustomField2")
    {
        auto& v = std::get<std::string>(val);
        customField2(v, skipSignal);
        return;
    }
}

auto VendorInformation::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CustomField1")
    {
        return customField1();
    }
    if (_name == "CustomField2")
    {
        return customField2();
    }

    return PropertiesVariant();
}


const vtable_t VendorInformation::_vtable[] = {
    vtable::start(),
    vtable::property("CustomField1",
                     details::VendorInformation::_property_CustomField1
                        .data(),
                     _callback_get_CustomField1,
                     _callback_set_CustomField1,
                     vtable::property_::emits_change),
    vtable::property("CustomField2",
                     details::VendorInformation::_property_CustomField2
                        .data(),
                     _callback_get_CustomField2,
                     _callback_set_CustomField2,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

