#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/intel/Control/NMISource/server.hpp>



namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Control
{
namespace server
{

NMISource::NMISource(bus_t& bus, const char* path)
        : _com_intel_Control_NMISource_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

NMISource::NMISource(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : NMISource(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto NMISource::bmcSource() const ->
        BMCSourceSignal
{
    return _bmcSource;
}

int NMISource::_callback_get_BMCSource(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bmcSource();
                    }
                ));
    }
}

auto NMISource::bmcSource(BMCSourceSignal value,
                                         bool skipSignal) ->
        BMCSourceSignal
{
    if (_bmcSource != value)
    {
        _bmcSource = value;
        if (!skipSignal)
        {
            _com_intel_Control_NMISource_interface.property_changed("BMCSource");
        }
    }

    return _bmcSource;
}

auto NMISource::bmcSource(BMCSourceSignal val) ->
        BMCSourceSignal
{
    return bmcSource(val, false);
}

int NMISource::_callback_set_BMCSource(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](BMCSourceSignal&& arg)
                    {
                        o->bmcSource(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace NMISource
{
static const auto _property_BMCSource =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::com::intel::Control::server::NMISource::BMCSourceSignal>());
}
}

auto NMISource::enabled() const ->
        bool
{
    return _enabled;
}

int NMISource::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
}

auto NMISource::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _com_intel_Control_NMISource_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto NMISource::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int NMISource::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace NMISource
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void NMISource::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BMCSource")
    {
        auto& v = std::get<BMCSourceSignal>(val);
        bmcSource(v, skipSignal);
        return;
    }
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
}

auto NMISource::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BMCSource")
    {
        return bmcSource();
    }
    if (_name == "Enabled")
    {
        return enabled();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for NMISource::BMCSourceSignal */
static const std::tuple<const char*, NMISource::BMCSourceSignal> mappingNMISourceBMCSourceSignal[] =
        {
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.None",                 NMISource::BMCSourceSignal::None ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.FpBtn",                 NMISource::BMCSourceSignal::FpBtn ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.WdPreTimeout",                 NMISource::BMCSourceSignal::WdPreTimeout ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.PefMatch",                 NMISource::BMCSourceSignal::PefMatch ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.ChassisCmd",                 NMISource::BMCSourceSignal::ChassisCmd ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.MemoryError",                 NMISource::BMCSourceSignal::MemoryError ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.PciSerrPerr",                 NMISource::BMCSourceSignal::PciSerrPerr ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.SouthbridgeNmi",                 NMISource::BMCSourceSignal::SouthbridgeNmi ),
            std::make_tuple( "com.intel.Control.NMISource.BMCSourceSignal.ChipsetNmi",                 NMISource::BMCSourceSignal::ChipsetNmi ),
        };

} // anonymous namespace

auto NMISource::convertStringToBMCSourceSignal(const std::string& s) noexcept ->
        std::optional<BMCSourceSignal>
{
    auto i = std::find_if(
            std::begin(mappingNMISourceBMCSourceSignal),
            std::end(mappingNMISourceBMCSourceSignal),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingNMISourceBMCSourceSignal) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto NMISource::convertBMCSourceSignalFromString(const std::string& s) ->
        BMCSourceSignal
{
    auto r = convertStringToBMCSourceSignal(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string NMISource::convertBMCSourceSignalToString(NMISource::BMCSourceSignal v)
{
    auto i = std::find_if(
            std::begin(mappingNMISourceBMCSourceSignal),
            std::end(mappingNMISourceBMCSourceSignal),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingNMISourceBMCSourceSignal))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t NMISource::_vtable[] = {
    vtable::start(),
    vtable::property("BMCSource",
                     details::NMISource::_property_BMCSource
                        .data(),
                     _callback_get_BMCSource,
                     _callback_set_BMCSource,
                     vtable::property_::emits_change),
    vtable::property("Enabled",
                     details::NMISource::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace intel
} // namespace com
} // namespace sdbusplus

