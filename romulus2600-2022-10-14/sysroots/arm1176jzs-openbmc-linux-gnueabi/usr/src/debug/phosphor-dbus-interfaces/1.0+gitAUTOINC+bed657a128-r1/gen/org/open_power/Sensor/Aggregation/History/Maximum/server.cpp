#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/Sensor/Aggregation/History/Maximum/server.hpp>




namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Sensor
{
namespace Aggregation
{
namespace History
{
namespace server
{

Maximum::Maximum(bus_t& bus, const char* path)
        : _org_open_power_Sensor_Aggregation_History_Maximum_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Maximum::Maximum(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Maximum(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Maximum::scale() const ->
        int64_t
{
    return _scale;
}

int Maximum::_callback_get_Scale(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Maximum*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->scale();
                    }
                ));
    }
}

auto Maximum::scale(int64_t value,
                                         bool skipSignal) ->
        int64_t
{
    if (_scale != value)
    {
        _scale = value;
        if (!skipSignal)
        {
            _org_open_power_Sensor_Aggregation_History_Maximum_interface.property_changed("Scale");
        }
    }

    return _scale;
}

auto Maximum::scale(int64_t val) ->
        int64_t
{
    return scale(val, false);
}

int Maximum::_callback_set_Scale(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Maximum*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](int64_t&& arg)
                    {
                        o->scale(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Maximum
{
static const auto _property_Scale =
    utility::tuple_to_array(message::types::type_id<
            int64_t>());
}
}

auto Maximum::unit() const ->
        Unit
{
    return _unit;
}

int Maximum::_callback_get_Unit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Maximum*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->unit();
                    }
                ));
    }
}

auto Maximum::unit(Unit value,
                                         bool skipSignal) ->
        Unit
{
    if (_unit != value)
    {
        _unit = value;
        if (!skipSignal)
        {
            _org_open_power_Sensor_Aggregation_History_Maximum_interface.property_changed("Unit");
        }
    }

    return _unit;
}

auto Maximum::unit(Unit val) ->
        Unit
{
    return unit(val, false);
}

int Maximum::_callback_set_Unit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Maximum*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Unit&& arg)
                    {
                        o->unit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Maximum
{
static const auto _property_Unit =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::org::open_power::Sensor::Aggregation::History::server::Maximum::Unit>());
}
}

auto Maximum::values() const ->
        std::vector<std::tuple<uint64_t, int64_t>>
{
    return _values;
}

int Maximum::_callback_get_Values(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Maximum*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->values();
                    }
                ));
    }
}

auto Maximum::values(std::vector<std::tuple<uint64_t, int64_t>> value,
                                         bool skipSignal) ->
        std::vector<std::tuple<uint64_t, int64_t>>
{
    if (_values != value)
    {
        _values = value;
        if (!skipSignal)
        {
            _org_open_power_Sensor_Aggregation_History_Maximum_interface.property_changed("Values");
        }
    }

    return _values;
}

auto Maximum::values(std::vector<std::tuple<uint64_t, int64_t>> val) ->
        std::vector<std::tuple<uint64_t, int64_t>>
{
    return values(val, false);
}

int Maximum::_callback_set_Values(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Maximum*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::tuple<uint64_t, int64_t>>&& arg)
                    {
                        o->values(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Maximum
{
static const auto _property_Values =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::tuple<uint64_t, int64_t>>>());
}
}

void Maximum::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Scale")
    {
        auto& v = std::get<int64_t>(val);
        scale(v, skipSignal);
        return;
    }
    if (_name == "Unit")
    {
        auto& v = std::get<Unit>(val);
        unit(v, skipSignal);
        return;
    }
    if (_name == "Values")
    {
        auto& v = std::get<std::vector<std::tuple<uint64_t, int64_t>>>(val);
        values(v, skipSignal);
        return;
    }
}

auto Maximum::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Scale")
    {
        return scale();
    }
    if (_name == "Unit")
    {
        return unit();
    }
    if (_name == "Values")
    {
        return values();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Maximum::Unit */
static const std::tuple<const char*, Maximum::Unit> mappingMaximumUnit[] =
        {
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.DegreesC",                 Maximum::Unit::DegreesC ),
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.RPMS",                 Maximum::Unit::RPMS ),
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.Volts",                 Maximum::Unit::Volts ),
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.Meters",                 Maximum::Unit::Meters ),
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.Amperes",                 Maximum::Unit::Amperes ),
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.Watts",                 Maximum::Unit::Watts ),
            std::make_tuple( "org.open_power.Sensor.Aggregation.History.Maximum.Unit.Joules",                 Maximum::Unit::Joules ),
        };

} // anonymous namespace

auto Maximum::convertStringToUnit(const std::string& s) noexcept ->
        std::optional<Unit>
{
    auto i = std::find_if(
            std::begin(mappingMaximumUnit),
            std::end(mappingMaximumUnit),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingMaximumUnit) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Maximum::convertUnitFromString(const std::string& s) ->
        Unit
{
    auto r = convertStringToUnit(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Maximum::convertUnitToString(Maximum::Unit v)
{
    auto i = std::find_if(
            std::begin(mappingMaximumUnit),
            std::end(mappingMaximumUnit),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingMaximumUnit))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Maximum::_vtable[] = {
    vtable::start(),
    vtable::property("Scale",
                     details::Maximum::_property_Scale
                        .data(),
                     _callback_get_Scale,
                     _callback_set_Scale,
                     vtable::property_::emits_change),
    vtable::property("Unit",
                     details::Maximum::_property_Unit
                        .data(),
                     _callback_get_Unit,
                     _callback_set_Unit,
                     vtable::property_::emits_change),
    vtable::property("Values",
                     details::Maximum::_property_Values
                        .data(),
                     _callback_get_Values,
                     _callback_set_Values,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace History
} // namespace Aggregation
} // namespace Sensor
} // namespace open_power
} // namespace org
} // namespace sdbusplus

