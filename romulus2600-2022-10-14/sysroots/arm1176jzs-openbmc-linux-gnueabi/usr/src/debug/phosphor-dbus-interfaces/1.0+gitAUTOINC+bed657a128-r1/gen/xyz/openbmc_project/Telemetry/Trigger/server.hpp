#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>








#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

class Trigger
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Trigger() = delete;
        Trigger(const Trigger&) = delete;
        Trigger& operator=(const Trigger&) = delete;
        Trigger(Trigger&&) = delete;
        Trigger& operator=(Trigger&&) = delete;
        virtual ~Trigger() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Trigger(bus_t& bus, const char* path);

        enum class TriggerAction
        {
            LogToJournal,
            LogToRedfishEventLog,
            UpdateReport,
        };
        enum class Type
        {
            LowerCritical,
            LowerWarning,
            UpperWarning,
            UpperCritical,
        };
        enum class Direction
        {
            Either,
            Decreasing,
            Increasing,
        };
        enum class Severity
        {
            OK,
            Warning,
            Critical,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string,
                std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>>,
                std::vector<TriggerAction>,
                std::vector<sdbusplus::message::object_path>,
                std::vector<std::map<sdbusplus::message::object_path, std::string>>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Trigger(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Discrete */
        virtual bool discrete() const;
        /** Set value of Discrete with option to skip sending signal */
        virtual bool discrete(bool value,
               bool skipSignal);
        /** Set value of Discrete */
        virtual bool discrete(bool value);
        /** Get value of TriggerActions */
        virtual std::vector<TriggerAction> triggerActions() const;
        /** Set value of TriggerActions with option to skip sending signal */
        virtual std::vector<TriggerAction> triggerActions(std::vector<TriggerAction> value,
               bool skipSignal);
        /** Set value of TriggerActions */
        virtual std::vector<TriggerAction> triggerActions(std::vector<TriggerAction> value);
        /** Get value of Persistent */
        virtual bool persistent() const;
        /** Set value of Persistent with option to skip sending signal */
        virtual bool persistent(bool value,
               bool skipSignal);
        /** Set value of Persistent */
        virtual bool persistent(bool value);
        /** Get value of Reports */
        virtual std::vector<sdbusplus::message::object_path> reports() const;
        /** Set value of Reports with option to skip sending signal */
        virtual std::vector<sdbusplus::message::object_path> reports(std::vector<sdbusplus::message::object_path> value,
               bool skipSignal);
        /** Set value of Reports */
        virtual std::vector<sdbusplus::message::object_path> reports(std::vector<sdbusplus::message::object_path> value);
        /** Get value of Sensors */
        virtual std::vector<std::map<sdbusplus::message::object_path, std::string>> sensors() const;
        /** Set value of Sensors with option to skip sending signal */
        virtual std::vector<std::map<sdbusplus::message::object_path, std::string>> sensors(std::vector<std::map<sdbusplus::message::object_path, std::string>> value,
               bool skipSignal);
        /** Set value of Sensors */
        virtual std::vector<std::map<sdbusplus::message::object_path, std::string>> sensors(std::vector<std::map<sdbusplus::message::object_path, std::string>> value);
        /** Get value of Thresholds */
        virtual std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> thresholds() const;
        /** Set value of Thresholds with option to skip sending signal */
        virtual std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> thresholds(std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> value,
               bool skipSignal);
        /** Set value of Thresholds */
        virtual std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> thresholds(std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> value);
        /** Get value of Name */
        virtual std::string name() const;
        /** Set value of Name with option to skip sending signal */
        virtual std::string name(std::string value,
               bool skipSignal);
        /** Set value of Name */
        virtual std::string name(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static TriggerAction convertTriggerActionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<TriggerAction> convertStringToTriggerAction(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         */
        static std::string convertTriggerActionToString(TriggerAction e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Type convertTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Type> convertStringToType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         */
        static std::string convertTypeToString(Type e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Direction convertDirectionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Direction> convertStringToDirection(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         */
        static std::string convertDirectionToString(Direction e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Severity convertSeverityFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Severity> convertStringToSeverity(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Telemetry.Trigger.<value name>"
         */
        static std::string convertSeverityToString(Severity e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Telemetry.Trigger";

    private:

        /** @brief sd-bus callback for get-property 'Discrete' */
        static int _callback_get_Discrete(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TriggerActions' */
        static int _callback_get_TriggerActions(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Persistent' */
        static int _callback_get_Persistent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Persistent' */
        static int _callback_set_Persistent(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Reports' */
        static int _callback_get_Reports(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Reports' */
        static int _callback_set_Reports(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Sensors' */
        static int _callback_get_Sensors(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Sensors' */
        static int _callback_set_Sensors(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Thresholds' */
        static int _callback_get_Thresholds(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Thresholds' */
        static int _callback_set_Thresholds(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Name' */
        static int _callback_get_Name(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Name' */
        static int _callback_set_Name(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Telemetry_Trigger_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _discrete{};
        std::vector<TriggerAction> _triggerActions{};
        bool _persistent{};
        std::vector<sdbusplus::message::object_path> _reports{};
        std::vector<std::map<sdbusplus::message::object_path, std::string>> _sensors{};
        std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> _thresholds{};
        std::string _name{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Trigger::TriggerAction.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Trigger::TriggerAction e)
{
    return Trigger::convertTriggerActionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Trigger::Type.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Trigger::Type e)
{
    return Trigger::convertTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Trigger::Direction.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Trigger::Direction e)
{
    return Trigger::convertDirectionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Trigger::Severity.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Trigger::Severity e)
{
    return Trigger::convertSeverityToString(e);
}

} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertStringToTriggerAction(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction value)
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertTriggerActionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Trigger::Type>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertStringToType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Trigger::Type>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Trigger::Type value)
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Trigger::Direction>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertStringToDirection(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Trigger::Direction>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Trigger::Direction value)
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertDirectionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Telemetry::server::Trigger::Severity>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertStringToSeverity(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Telemetry::server::Trigger::Severity>
{
    static std::string op(xyz::openbmc_project::Telemetry::server::Trigger::Severity value)
    {
        return xyz::openbmc_project::Telemetry::server::Trigger::convertSeverityToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

