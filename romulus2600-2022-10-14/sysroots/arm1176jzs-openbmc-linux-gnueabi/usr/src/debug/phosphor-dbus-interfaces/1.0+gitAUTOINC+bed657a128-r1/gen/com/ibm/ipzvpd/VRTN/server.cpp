#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VRTN/server.hpp>





namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VRTN::VRTN(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VRTN_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VRTN::VRTN(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VRTN(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VRTN::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VRTN::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VRTN::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRTN_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VRTN::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VRTN::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRTN
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRTN::so() const ->
        std::vector<uint8_t>
{
    return _so;
}

int VRTN::_callback_get_SO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->so();
                    }
                ));
    }
}

auto VRTN::so(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_so != value)
    {
        _so = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRTN_interface.property_changed("SO");
        }
    }

    return _so;
}

auto VRTN::so(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return so(val, false);
}

int VRTN::_callback_set_SO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->so(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRTN
{
static const auto _property_SO =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRTN::in() const ->
        std::vector<uint8_t>
{
    return _in;
}

int VRTN::_callback_get_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->in();
                    }
                ));
    }
}

auto VRTN::in(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_in != value)
    {
        _in = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRTN_interface.property_changed("IN");
        }
    }

    return _in;
}

auto VRTN::in(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return in(val, false);
}

int VRTN::_callback_set_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->in(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRTN
{
static const auto _property_IN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VRTN::i2() const ->
        std::vector<uint8_t>
{
    return _i2;
}

int VRTN::_callback_get_I2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->i2();
                    }
                ));
    }
}

auto VRTN::i2(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_i2 != value)
    {
        _i2 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VRTN_interface.property_changed("I2");
        }
    }

    return _i2;
}

auto VRTN::i2(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return i2(val, false);
}

int VRTN::_callback_set_I2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VRTN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->i2(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VRTN
{
static const auto _property_I2 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VRTN::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "SO")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        so(v, skipSignal);
        return;
    }
    if (_name == "IN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        in(v, skipSignal);
        return;
    }
    if (_name == "I2")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        i2(v, skipSignal);
        return;
    }
}

auto VRTN::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "SO")
    {
        return so();
    }
    if (_name == "IN")
    {
        return in();
    }
    if (_name == "I2")
    {
        return i2();
    }

    return PropertiesVariant();
}


const vtable_t VRTN::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VRTN::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("SO",
                     details::VRTN::_property_SO
                        .data(),
                     _callback_get_SO,
                     _callback_set_SO,
                     vtable::property_::emits_change),
    vtable::property("IN",
                     details::VRTN::_property_IN
                        .data(),
                     _callback_get_IN,
                     _callback_set_IN,
                     vtable::property_::emits_change),
    vtable::property("I2",
                     details::VRTN::_property_I2
                        .data(),
                     _callback_get_I2,
                     _callback_set_I2,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

