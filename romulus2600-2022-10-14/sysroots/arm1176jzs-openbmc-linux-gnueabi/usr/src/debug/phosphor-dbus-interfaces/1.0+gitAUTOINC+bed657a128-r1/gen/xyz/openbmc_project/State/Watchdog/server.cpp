#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Watchdog/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

Watchdog::Watchdog(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Watchdog_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Watchdog::Watchdog(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Watchdog(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Watchdog::_callback_ResetTimeRemaining(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](bool&& enableWatchdog)
                    {
                        return o->resetTimeRemaining(
                                enableWatchdog);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Watchdog
{
static const auto _param_ResetTimeRemaining =
        utility::tuple_to_array(message::types::type_id<
                bool>());
static const auto _return_ResetTimeRemaining =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


void Watchdog::timeout(
            Action action)
{
    auto& i = _xyz_openbmc_project_State_Watchdog_interface;
    auto m = i.new_signal("Timeout");

    m.append(action);
    m.signal_send();
}

namespace details
{
namespace Watchdog
{
static const auto _signal_Timeout =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::xyz::openbmc_project::State::server::Watchdog::Action>());
}
}


auto Watchdog::initialized() const ->
        bool
{
    return _initialized;
}

int Watchdog::_callback_get_Initialized(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->initialized();
                    }
                ));
    }
}

auto Watchdog::initialized(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_initialized != value)
    {
        _initialized = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("Initialized");
        }
    }

    return _initialized;
}

auto Watchdog::initialized(bool val) ->
        bool
{
    return initialized(val, false);
}

int Watchdog::_callback_set_Initialized(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->initialized(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_Initialized =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Watchdog::enabled() const ->
        bool
{
    return _enabled;
}

int Watchdog::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
}

auto Watchdog::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto Watchdog::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int Watchdog::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Watchdog::expireAction() const ->
        Action
{
    return _expireAction;
}

int Watchdog::_callback_get_ExpireAction(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->expireAction();
                    }
                ));
    }
}

auto Watchdog::expireAction(Action value,
                                         bool skipSignal) ->
        Action
{
    if (_expireAction != value)
    {
        _expireAction = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("ExpireAction");
        }
    }

    return _expireAction;
}

auto Watchdog::expireAction(Action val) ->
        Action
{
    return expireAction(val, false);
}

int Watchdog::_callback_set_ExpireAction(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Action&& arg)
                    {
                        o->expireAction(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_ExpireAction =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Watchdog::Action>());
}
}

auto Watchdog::interval() const ->
        uint64_t
{
    return _interval;
}

int Watchdog::_callback_get_Interval(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->interval();
                    }
                ));
    }
}

auto Watchdog::interval(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_interval != value)
    {
        _interval = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("Interval");
        }
    }

    return _interval;
}

auto Watchdog::interval(uint64_t val) ->
        uint64_t
{
    return interval(val, false);
}

int Watchdog::_callback_set_Interval(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->interval(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_Interval =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Watchdog::timeRemaining() const ->
        uint64_t
{
    return _timeRemaining;
}

int Watchdog::_callback_get_TimeRemaining(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timeRemaining();
                    }
                ));
    }
}

auto Watchdog::timeRemaining(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_timeRemaining != value)
    {
        _timeRemaining = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("TimeRemaining");
        }
    }

    return _timeRemaining;
}

auto Watchdog::timeRemaining(uint64_t val) ->
        uint64_t
{
    return timeRemaining(val, false);
}

int Watchdog::_callback_set_TimeRemaining(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->timeRemaining(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_TimeRemaining =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Watchdog::currentTimerUse() const ->
        TimerUse
{
    return _currentTimerUse;
}

int Watchdog::_callback_get_CurrentTimerUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentTimerUse();
                    }
                ));
    }
}

auto Watchdog::currentTimerUse(TimerUse value,
                                         bool skipSignal) ->
        TimerUse
{
    if (_currentTimerUse != value)
    {
        _currentTimerUse = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("CurrentTimerUse");
        }
    }

    return _currentTimerUse;
}

auto Watchdog::currentTimerUse(TimerUse val) ->
        TimerUse
{
    return currentTimerUse(val, false);
}

int Watchdog::_callback_set_CurrentTimerUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](TimerUse&& arg)
                    {
                        o->currentTimerUse(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_CurrentTimerUse =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Watchdog::TimerUse>());
}
}

auto Watchdog::expiredTimerUse() const ->
        TimerUse
{
    return _expiredTimerUse;
}

int Watchdog::_callback_get_ExpiredTimerUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->expiredTimerUse();
                    }
                ));
    }
}

auto Watchdog::expiredTimerUse(TimerUse value,
                                         bool skipSignal) ->
        TimerUse
{
    if (_expiredTimerUse != value)
    {
        _expiredTimerUse = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Watchdog_interface.property_changed("ExpiredTimerUse");
        }
    }

    return _expiredTimerUse;
}

auto Watchdog::expiredTimerUse(TimerUse val) ->
        TimerUse
{
    return expiredTimerUse(val, false);
}

int Watchdog::_callback_set_ExpiredTimerUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Watchdog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](TimerUse&& arg)
                    {
                        o->expiredTimerUse(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Watchdog
{
static const auto _property_ExpiredTimerUse =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Watchdog::TimerUse>());
}
}

void Watchdog::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Initialized")
    {
        auto& v = std::get<bool>(val);
        initialized(v, skipSignal);
        return;
    }
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
    if (_name == "ExpireAction")
    {
        auto& v = std::get<Action>(val);
        expireAction(v, skipSignal);
        return;
    }
    if (_name == "Interval")
    {
        auto& v = std::get<uint64_t>(val);
        interval(v, skipSignal);
        return;
    }
    if (_name == "TimeRemaining")
    {
        auto& v = std::get<uint64_t>(val);
        timeRemaining(v, skipSignal);
        return;
    }
    if (_name == "CurrentTimerUse")
    {
        auto& v = std::get<TimerUse>(val);
        currentTimerUse(v, skipSignal);
        return;
    }
    if (_name == "ExpiredTimerUse")
    {
        auto& v = std::get<TimerUse>(val);
        expiredTimerUse(v, skipSignal);
        return;
    }
}

auto Watchdog::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Initialized")
    {
        return initialized();
    }
    if (_name == "Enabled")
    {
        return enabled();
    }
    if (_name == "ExpireAction")
    {
        return expireAction();
    }
    if (_name == "Interval")
    {
        return interval();
    }
    if (_name == "TimeRemaining")
    {
        return timeRemaining();
    }
    if (_name == "CurrentTimerUse")
    {
        return currentTimerUse();
    }
    if (_name == "ExpiredTimerUse")
    {
        return expiredTimerUse();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Watchdog::Action */
static const std::tuple<const char*, Watchdog::Action> mappingWatchdogAction[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.Action.None",                 Watchdog::Action::None ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.Action.HardReset",                 Watchdog::Action::HardReset ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.Action.PowerOff",                 Watchdog::Action::PowerOff ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.Action.PowerCycle",                 Watchdog::Action::PowerCycle ),
        };

} // anonymous namespace

auto Watchdog::convertStringToAction(const std::string& s) noexcept ->
        std::optional<Action>
{
    auto i = std::find_if(
            std::begin(mappingWatchdogAction),
            std::end(mappingWatchdogAction),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingWatchdogAction) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Watchdog::convertActionFromString(const std::string& s) ->
        Action
{
    auto r = convertStringToAction(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Watchdog::convertActionToString(Watchdog::Action v)
{
    auto i = std::find_if(
            std::begin(mappingWatchdogAction),
            std::end(mappingWatchdogAction),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingWatchdogAction))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Watchdog::TimerUse */
static const std::tuple<const char*, Watchdog::TimerUse> mappingWatchdogTimerUse[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.TimerUse.Reserved",                 Watchdog::TimerUse::Reserved ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.TimerUse.BIOSFRB2",                 Watchdog::TimerUse::BIOSFRB2 ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.TimerUse.BIOSPOST",                 Watchdog::TimerUse::BIOSPOST ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.TimerUse.OSLoad",                 Watchdog::TimerUse::OSLoad ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.TimerUse.SMSOS",                 Watchdog::TimerUse::SMSOS ),
            std::make_tuple( "xyz.openbmc_project.State.Watchdog.TimerUse.OEM",                 Watchdog::TimerUse::OEM ),
        };

} // anonymous namespace

auto Watchdog::convertStringToTimerUse(const std::string& s) noexcept ->
        std::optional<TimerUse>
{
    auto i = std::find_if(
            std::begin(mappingWatchdogTimerUse),
            std::end(mappingWatchdogTimerUse),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingWatchdogTimerUse) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Watchdog::convertTimerUseFromString(const std::string& s) ->
        TimerUse
{
    auto r = convertStringToTimerUse(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Watchdog::convertTimerUseToString(Watchdog::TimerUse v)
{
    auto i = std::find_if(
            std::begin(mappingWatchdogTimerUse),
            std::end(mappingWatchdogTimerUse),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingWatchdogTimerUse))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Watchdog::_vtable[] = {
    vtable::start(),

    vtable::method("ResetTimeRemaining",
                   details::Watchdog::_param_ResetTimeRemaining
                        .data(),
                   details::Watchdog::_return_ResetTimeRemaining
                        .data(),
                   _callback_ResetTimeRemaining),

    vtable::signal("Timeout",
                   details::Watchdog::_signal_Timeout
                        .data()),
    vtable::property("Initialized",
                     details::Watchdog::_property_Initialized
                        .data(),
                     _callback_get_Initialized,
                     _callback_set_Initialized,
                     vtable::property_::emits_change),
    vtable::property("Enabled",
                     details::Watchdog::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::property("ExpireAction",
                     details::Watchdog::_property_ExpireAction
                        .data(),
                     _callback_get_ExpireAction,
                     _callback_set_ExpireAction,
                     vtable::property_::emits_change),
    vtable::property("Interval",
                     details::Watchdog::_property_Interval
                        .data(),
                     _callback_get_Interval,
                     _callback_set_Interval,
                     vtable::property_::emits_change),
    vtable::property("TimeRemaining",
                     details::Watchdog::_property_TimeRemaining
                        .data(),
                     _callback_get_TimeRemaining,
                     _callback_set_TimeRemaining,
                     vtable::property_::emits_change),
    vtable::property("CurrentTimerUse",
                     details::Watchdog::_property_CurrentTimerUse
                        .data(),
                     _callback_get_CurrentTimerUse,
                     _callback_set_CurrentTimerUse,
                     vtable::property_::emits_change),
    vtable::property("ExpiredTimerUse",
                     details::Watchdog::_property_ExpiredTimerUse
                        .data(),
                     _callback_get_ExpiredTimerUse,
                     _callback_set_ExpiredTimerUse,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

