#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace server
{

class Requester
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Requester() = delete;
        Requester(const Requester&) = delete;
        Requester& operator=(const Requester&) = delete;
        Requester(Requester&&) = delete;
        Requester& operator=(Requester&&) = delete;
        virtual ~Requester() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Requester(bus_t& bus, const char* path);



        /** @brief Implementation for GetInstanceId
         *  Obtain a new PLDM instance id, for the input MCTP EID, to be used in a PLDM request message. Instance ids help distinguish PLDM response messages when a PLDM requester sends out multiple request messages, without waiting for a response message. Refer the PLDM specification DSP0240 version 1.0.0. Also refer https://github.com/openbmc/docs/blob/master/designs/pldm-stack.md#Requester.
The instance id starts at 0 and can go upto 31 (5 bits), for each MCTP EID that a PLDM requester is communicating with. An implementation of this API should track instance ids in terms of whether they're in use (request message with that instance id has been sent out, and the corresponding response message hasn't been received yet) or not, and grant unused ids via this method. An implementation may also optionally implement the instance id expiry, as per the spec DSP0240 v1.0.0. The spec recommends implementing the same.
If there's a call to this method when all 32 ids have already been granted to the input eid, then the xyz.openbmc_project.Common.Error.TooManyResources exception will be thrown. The recommendation for the caller upon receiving this exception is to retry calling this method, at least once, after a time period equal to the maximum instance id expiration interval, which is 6 seconds as per DSP0240 v1.0.0. If the exception persists post this interval, the way of handling the same (further retries, report an error, etc) is left to the user.
         *
         *  @param[in] eid - The MCTP endpoint, specified by 'eid' (endpoint id), for which the new PLDM instance id needs to be generated.
         *
         *  @return instanceid[uint8_t] - PLDM instance id.
         */
        virtual uint8_t getInstanceId(
            uint8_t eid) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_PLDM_Requester_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_PLDM_Requester_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.PLDM.Requester";

    private:

        /** @brief sd-bus callback for GetInstanceId
         */
        static int _callback_GetInstanceId(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_PLDM_Requester_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

