#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/PFR/Attributes/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PFR
{
namespace server
{

Attributes::Attributes(bus_t& bus, const char* path)
        : _xyz_openbmc_project_PFR_Attributes_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Attributes::Attributes(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Attributes(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Attributes::provisioned() const ->
        bool
{
    return _provisioned;
}

int Attributes::_callback_get_Provisioned(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->provisioned();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::provisioned(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_provisioned != value)
    {
        _provisioned = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_PFR_Attributes_interface.property_changed("Provisioned");
        }
    }

    return _provisioned;
}

auto Attributes::provisioned(bool val) ->
        bool
{
    return provisioned(val, false);
}

int Attributes::_callback_set_Provisioned(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->provisioned(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_Provisioned =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Attributes::locked() const ->
        bool
{
    return _locked;
}

int Attributes::_callback_get_Locked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->locked();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::locked(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_locked != value)
    {
        _locked = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_PFR_Attributes_interface.property_changed("Locked");
        }
    }

    return _locked;
}

auto Attributes::locked(bool val) ->
        bool
{
    return locked(val, false);
}

int Attributes::_callback_set_Locked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->locked(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_Locked =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Attributes::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Provisioned")
    {
        auto& v = std::get<bool>(val);
        provisioned(v, skipSignal);
        return;
    }
    if (_name == "Locked")
    {
        auto& v = std::get<bool>(val);
        locked(v, skipSignal);
        return;
    }
}

auto Attributes::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Provisioned")
    {
        return provisioned();
    }
    if (_name == "Locked")
    {
        return locked();
    }

    return PropertiesVariant();
}


const vtable_t Attributes::_vtable[] = {
    vtable::start(),
    vtable::property("Provisioned",
                     details::Attributes::_property_Provisioned
                        .data(),
                     _callback_get_Provisioned,
                     _callback_set_Provisioned,
                     vtable::property_::emits_change),
    vtable::property("Locked",
                     details::Attributes::_property_Locked
                        .data(),
                     _callback_get_Locked,
                     _callback_set_Locked,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PFR
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

