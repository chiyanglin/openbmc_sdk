#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/PCIeSlot/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

PCIeSlot::PCIeSlot(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PCIeSlot::PCIeSlot(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PCIeSlot(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PCIeSlot::generation() const ->
        Generations
{
    return _generation;
}

int PCIeSlot::_callback_get_Generation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->generation();
                    }
                ));
    }
}

auto PCIeSlot::generation(Generations value,
                                         bool skipSignal) ->
        Generations
{
    if (_generation != value)
    {
        _generation = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface.property_changed("Generation");
        }
    }

    return _generation;
}

auto PCIeSlot::generation(Generations val) ->
        Generations
{
    return generation(val, false);
}

int PCIeSlot::_callback_set_Generation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Generations&& arg)
                    {
                        o->generation(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeSlot
{
static const auto _property_Generation =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>());
}
}

auto PCIeSlot::lanes() const ->
        size_t
{
    return _lanes;
}

int PCIeSlot::_callback_get_Lanes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lanes();
                    }
                ));
    }
}

auto PCIeSlot::lanes(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_lanes != value)
    {
        _lanes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface.property_changed("Lanes");
        }
    }

    return _lanes;
}

auto PCIeSlot::lanes(size_t val) ->
        size_t
{
    return lanes(val, false);
}

int PCIeSlot::_callback_set_Lanes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->lanes(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeSlot
{
static const auto _property_Lanes =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto PCIeSlot::slotType() const ->
        SlotTypes
{
    return _slotType;
}

int PCIeSlot::_callback_get_SlotType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->slotType();
                    }
                ));
    }
}

auto PCIeSlot::slotType(SlotTypes value,
                                         bool skipSignal) ->
        SlotTypes
{
    if (_slotType != value)
    {
        _slotType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface.property_changed("SlotType");
        }
    }

    return _slotType;
}

auto PCIeSlot::slotType(SlotTypes val) ->
        SlotTypes
{
    return slotType(val, false);
}

int PCIeSlot::_callback_set_SlotType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](SlotTypes&& arg)
                    {
                        o->slotType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeSlot
{
static const auto _property_SlotType =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::PCIeSlot::SlotTypes>());
}
}

auto PCIeSlot::hotPluggable() const ->
        bool
{
    return _hotPluggable;
}

int PCIeSlot::_callback_get_HotPluggable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hotPluggable();
                    }
                ));
    }
}

auto PCIeSlot::hotPluggable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hotPluggable != value)
    {
        _hotPluggable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeSlot_interface.property_changed("HotPluggable");
        }
    }

    return _hotPluggable;
}

auto PCIeSlot::hotPluggable(bool val) ->
        bool
{
    return hotPluggable(val, false);
}

int PCIeSlot::_callback_set_HotPluggable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeSlot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hotPluggable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeSlot
{
static const auto _property_HotPluggable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void PCIeSlot::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Generation")
    {
        auto& v = std::get<Generations>(val);
        generation(v, skipSignal);
        return;
    }
    if (_name == "Lanes")
    {
        auto& v = std::get<size_t>(val);
        lanes(v, skipSignal);
        return;
    }
    if (_name == "SlotType")
    {
        auto& v = std::get<SlotTypes>(val);
        slotType(v, skipSignal);
        return;
    }
    if (_name == "HotPluggable")
    {
        auto& v = std::get<bool>(val);
        hotPluggable(v, skipSignal);
        return;
    }
}

auto PCIeSlot::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Generation")
    {
        return generation();
    }
    if (_name == "Lanes")
    {
        return lanes();
    }
    if (_name == "SlotType")
    {
        return slotType();
    }
    if (_name == "HotPluggable")
    {
        return hotPluggable();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for PCIeSlot::Generations */
static const std::tuple<const char*, PCIeSlot::Generations> mappingPCIeSlotGenerations[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.Generations.Gen1",                 PCIeSlot::Generations::Gen1 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.Generations.Gen2",                 PCIeSlot::Generations::Gen2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.Generations.Gen3",                 PCIeSlot::Generations::Gen3 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.Generations.Gen4",                 PCIeSlot::Generations::Gen4 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.Generations.Gen5",                 PCIeSlot::Generations::Gen5 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.Generations.Unknown",                 PCIeSlot::Generations::Unknown ),
        };

} // anonymous namespace

auto PCIeSlot::convertStringToGenerations(const std::string& s) noexcept ->
        std::optional<Generations>
{
    auto i = std::find_if(
            std::begin(mappingPCIeSlotGenerations),
            std::end(mappingPCIeSlotGenerations),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPCIeSlotGenerations) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PCIeSlot::convertGenerationsFromString(const std::string& s) ->
        Generations
{
    auto r = convertStringToGenerations(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PCIeSlot::convertGenerationsToString(PCIeSlot::Generations v)
{
    auto i = std::find_if(
            std::begin(mappingPCIeSlotGenerations),
            std::end(mappingPCIeSlotGenerations),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPCIeSlotGenerations))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for PCIeSlot::SlotTypes */
static const std::tuple<const char*, PCIeSlot::SlotTypes> mappingPCIeSlotSlotTypes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.FullLength",                 PCIeSlot::SlotTypes::FullLength ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.HalfLength",                 PCIeSlot::SlotTypes::HalfLength ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.LowProfile",                 PCIeSlot::SlotTypes::LowProfile ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.Mini",                 PCIeSlot::SlotTypes::Mini ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.M_2",                 PCIeSlot::SlotTypes::M_2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.OEM",                 PCIeSlot::SlotTypes::OEM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.OCP3Small",                 PCIeSlot::SlotTypes::OCP3Small ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.OCP3Large",                 PCIeSlot::SlotTypes::OCP3Large ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.U_2",                 PCIeSlot::SlotTypes::U_2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PCIeSlot.SlotTypes.Unknown",                 PCIeSlot::SlotTypes::Unknown ),
        };

} // anonymous namespace

auto PCIeSlot::convertStringToSlotTypes(const std::string& s) noexcept ->
        std::optional<SlotTypes>
{
    auto i = std::find_if(
            std::begin(mappingPCIeSlotSlotTypes),
            std::end(mappingPCIeSlotSlotTypes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPCIeSlotSlotTypes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PCIeSlot::convertSlotTypesFromString(const std::string& s) ->
        SlotTypes
{
    auto r = convertStringToSlotTypes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PCIeSlot::convertSlotTypesToString(PCIeSlot::SlotTypes v)
{
    auto i = std::find_if(
            std::begin(mappingPCIeSlotSlotTypes),
            std::end(mappingPCIeSlotSlotTypes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPCIeSlotSlotTypes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t PCIeSlot::_vtable[] = {
    vtable::start(),
    vtable::property("Generation",
                     details::PCIeSlot::_property_Generation
                        .data(),
                     _callback_get_Generation,
                     _callback_set_Generation,
                     vtable::property_::emits_change),
    vtable::property("Lanes",
                     details::PCIeSlot::_property_Lanes
                        .data(),
                     _callback_get_Lanes,
                     _callback_set_Lanes,
                     vtable::property_::emits_change),
    vtable::property("SlotType",
                     details::PCIeSlot::_property_SlotType
                        .data(),
                     _callback_get_SlotType,
                     _callback_set_SlotType,
                     vtable::property_::emits_change),
    vtable::property("HotPluggable",
                     details::PCIeSlot::_property_HotPluggable
                        .data(),
                     _callback_get_HotPluggable,
                     _callback_set_HotPluggable,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

