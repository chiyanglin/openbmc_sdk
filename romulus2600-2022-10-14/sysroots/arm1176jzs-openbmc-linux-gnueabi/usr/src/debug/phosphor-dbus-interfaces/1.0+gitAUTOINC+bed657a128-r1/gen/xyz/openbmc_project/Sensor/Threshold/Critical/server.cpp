#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Threshold/Critical/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

Critical::Critical(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Threshold_Critical_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Critical::Critical(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Critical(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}



void Critical::criticalHighAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Critical_interface;
    auto m = i.new_signal("CriticalHighAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Critical
{
static const auto _signal_CriticalHighAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void Critical::criticalHighAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Critical_interface;
    auto m = i.new_signal("CriticalHighAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Critical
{
static const auto _signal_CriticalHighAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void Critical::criticalLowAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Critical_interface;
    auto m = i.new_signal("CriticalLowAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Critical
{
static const auto _signal_CriticalLowAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void Critical::criticalLowAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Critical_interface;
    auto m = i.new_signal("CriticalLowAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Critical
{
static const auto _signal_CriticalLowAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}


auto Critical::criticalHigh() const ->
        double
{
    return _criticalHigh;
}

int Critical::_callback_get_CriticalHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->criticalHigh();
                    }
                ));
    }
}

auto Critical::criticalHigh(double value,
                                         bool skipSignal) ->
        double
{
    if (_criticalHigh != value)
    {
        _criticalHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Critical_interface.property_changed("CriticalHigh");
        }
    }

    return _criticalHigh;
}

auto Critical::criticalHigh(double val) ->
        double
{
    return criticalHigh(val, false);
}

int Critical::_callback_set_CriticalHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->criticalHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Critical
{
static const auto _property_CriticalHigh =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Critical::criticalLow() const ->
        double
{
    return _criticalLow;
}

int Critical::_callback_get_CriticalLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->criticalLow();
                    }
                ));
    }
}

auto Critical::criticalLow(double value,
                                         bool skipSignal) ->
        double
{
    if (_criticalLow != value)
    {
        _criticalLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Critical_interface.property_changed("CriticalLow");
        }
    }

    return _criticalLow;
}

auto Critical::criticalLow(double val) ->
        double
{
    return criticalLow(val, false);
}

int Critical::_callback_set_CriticalLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->criticalLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Critical
{
static const auto _property_CriticalLow =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Critical::criticalAlarmHigh() const ->
        bool
{
    return _criticalAlarmHigh;
}

int Critical::_callback_get_CriticalAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->criticalAlarmHigh();
                    }
                ));
    }
}

auto Critical::criticalAlarmHigh(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_criticalAlarmHigh != value)
    {
        _criticalAlarmHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Critical_interface.property_changed("CriticalAlarmHigh");
        }
    }

    return _criticalAlarmHigh;
}

auto Critical::criticalAlarmHigh(bool val) ->
        bool
{
    return criticalAlarmHigh(val, false);
}

int Critical::_callback_set_CriticalAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->criticalAlarmHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Critical
{
static const auto _property_CriticalAlarmHigh =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Critical::criticalAlarmLow() const ->
        bool
{
    return _criticalAlarmLow;
}

int Critical::_callback_get_CriticalAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->criticalAlarmLow();
                    }
                ));
    }
}

auto Critical::criticalAlarmLow(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_criticalAlarmLow != value)
    {
        _criticalAlarmLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Critical_interface.property_changed("CriticalAlarmLow");
        }
    }

    return _criticalAlarmLow;
}

auto Critical::criticalAlarmLow(bool val) ->
        bool
{
    return criticalAlarmLow(val, false);
}

int Critical::_callback_set_CriticalAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Critical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->criticalAlarmLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Critical
{
static const auto _property_CriticalAlarmLow =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Critical::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CriticalHigh")
    {
        auto& v = std::get<double>(val);
        criticalHigh(v, skipSignal);
        return;
    }
    if (_name == "CriticalLow")
    {
        auto& v = std::get<double>(val);
        criticalLow(v, skipSignal);
        return;
    }
    if (_name == "CriticalAlarmHigh")
    {
        auto& v = std::get<bool>(val);
        criticalAlarmHigh(v, skipSignal);
        return;
    }
    if (_name == "CriticalAlarmLow")
    {
        auto& v = std::get<bool>(val);
        criticalAlarmLow(v, skipSignal);
        return;
    }
}

auto Critical::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CriticalHigh")
    {
        return criticalHigh();
    }
    if (_name == "CriticalLow")
    {
        return criticalLow();
    }
    if (_name == "CriticalAlarmHigh")
    {
        return criticalAlarmHigh();
    }
    if (_name == "CriticalAlarmLow")
    {
        return criticalAlarmLow();
    }

    return PropertiesVariant();
}


const vtable_t Critical::_vtable[] = {
    vtable::start(),

    vtable::signal("CriticalHighAlarmAsserted",
                   details::Critical::_signal_CriticalHighAlarmAsserted
                        .data()),

    vtable::signal("CriticalHighAlarmDeasserted",
                   details::Critical::_signal_CriticalHighAlarmDeasserted
                        .data()),

    vtable::signal("CriticalLowAlarmAsserted",
                   details::Critical::_signal_CriticalLowAlarmAsserted
                        .data()),

    vtable::signal("CriticalLowAlarmDeasserted",
                   details::Critical::_signal_CriticalLowAlarmDeasserted
                        .data()),
    vtable::property("CriticalHigh",
                     details::Critical::_property_CriticalHigh
                        .data(),
                     _callback_get_CriticalHigh,
                     _callback_set_CriticalHigh,
                     vtable::property_::emits_change),
    vtable::property("CriticalLow",
                     details::Critical::_property_CriticalLow
                        .data(),
                     _callback_get_CriticalLow,
                     _callback_set_CriticalLow,
                     vtable::property_::emits_change),
    vtable::property("CriticalAlarmHigh",
                     details::Critical::_property_CriticalAlarmHigh
                        .data(),
                     _callback_get_CriticalAlarmHigh,
                     _callback_set_CriticalAlarmHigh,
                     vtable::property_::emits_change),
    vtable::property("CriticalAlarmLow",
                     details::Critical::_property_CriticalAlarmLow
                        .data(),
                     _callback_get_CriticalAlarmLow,
                     _callback_set_CriticalAlarmLow,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

