#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Dimension/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Dimension::Dimension(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Dimension_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Dimension::Dimension(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Dimension(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Dimension::height() const ->
        double
{
    return _height;
}

int Dimension::_callback_get_Height(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimension*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->height();
                    }
                ));
    }
}

auto Dimension::height(double value,
                                         bool skipSignal) ->
        double
{
    if (_height != value)
    {
        _height = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Dimension_interface.property_changed("Height");
        }
    }

    return _height;
}

auto Dimension::height(double val) ->
        double
{
    return height(val, false);
}

int Dimension::_callback_set_Height(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimension*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->height(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimension
{
static const auto _property_Height =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Dimension::width() const ->
        double
{
    return _width;
}

int Dimension::_callback_get_Width(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimension*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->width();
                    }
                ));
    }
}

auto Dimension::width(double value,
                                         bool skipSignal) ->
        double
{
    if (_width != value)
    {
        _width = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Dimension_interface.property_changed("Width");
        }
    }

    return _width;
}

auto Dimension::width(double val) ->
        double
{
    return width(val, false);
}

int Dimension::_callback_set_Width(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimension*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->width(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimension
{
static const auto _property_Width =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Dimension::depth() const ->
        double
{
    return _depth;
}

int Dimension::_callback_get_Depth(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimension*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->depth();
                    }
                ));
    }
}

auto Dimension::depth(double value,
                                         bool skipSignal) ->
        double
{
    if (_depth != value)
    {
        _depth = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Dimension_interface.property_changed("Depth");
        }
    }

    return _depth;
}

auto Dimension::depth(double val) ->
        double
{
    return depth(val, false);
}

int Dimension::_callback_set_Depth(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimension*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->depth(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimension
{
static const auto _property_Depth =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

void Dimension::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Height")
    {
        auto& v = std::get<double>(val);
        height(v, skipSignal);
        return;
    }
    if (_name == "Width")
    {
        auto& v = std::get<double>(val);
        width(v, skipSignal);
        return;
    }
    if (_name == "Depth")
    {
        auto& v = std::get<double>(val);
        depth(v, skipSignal);
        return;
    }
}

auto Dimension::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Height")
    {
        return height();
    }
    if (_name == "Width")
    {
        return width();
    }
    if (_name == "Depth")
    {
        return depth();
    }

    return PropertiesVariant();
}


const vtable_t Dimension::_vtable[] = {
    vtable::start(),
    vtable::property("Height",
                     details::Dimension::_property_Height
                        .data(),
                     _callback_get_Height,
                     _callback_set_Height,
                     vtable::property_::emits_change),
    vtable::property("Width",
                     details::Dimension::_property_Width
                        .data(),
                     _callback_get_Width,
                     _callback_set_Width,
                     vtable::property_::emits_change),
    vtable::property("Depth",
                     details::Dimension::_property_Depth
                        .data(),
                     _callback_get_Depth,
                     _callback_set_Depth,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

