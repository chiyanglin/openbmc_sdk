#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Power/IdlePowerSaver/server.hpp>







namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

IdlePowerSaver::IdlePowerSaver(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

IdlePowerSaver::IdlePowerSaver(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : IdlePowerSaver(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto IdlePowerSaver::enabled() const ->
        bool
{
    return _enabled;
}

int IdlePowerSaver::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
}

auto IdlePowerSaver::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto IdlePowerSaver::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int IdlePowerSaver::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace IdlePowerSaver
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto IdlePowerSaver::enterUtilizationPercent() const ->
        uint8_t
{
    return _enterUtilizationPercent;
}

int IdlePowerSaver::_callback_get_EnterUtilizationPercent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enterUtilizationPercent();
                    }
                ));
    }
}

auto IdlePowerSaver::enterUtilizationPercent(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_enterUtilizationPercent != value)
    {
        _enterUtilizationPercent = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.property_changed("EnterUtilizationPercent");
        }
    }

    return _enterUtilizationPercent;
}

auto IdlePowerSaver::enterUtilizationPercent(uint8_t val) ->
        uint8_t
{
    return enterUtilizationPercent(val, false);
}

int IdlePowerSaver::_callback_set_EnterUtilizationPercent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->enterUtilizationPercent(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace IdlePowerSaver
{
static const auto _property_EnterUtilizationPercent =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto IdlePowerSaver::enterDwellTime() const ->
        uint64_t
{
    return _enterDwellTime;
}

int IdlePowerSaver::_callback_get_EnterDwellTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enterDwellTime();
                    }
                ));
    }
}

auto IdlePowerSaver::enterDwellTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_enterDwellTime != value)
    {
        _enterDwellTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.property_changed("EnterDwellTime");
        }
    }

    return _enterDwellTime;
}

auto IdlePowerSaver::enterDwellTime(uint64_t val) ->
        uint64_t
{
    return enterDwellTime(val, false);
}

int IdlePowerSaver::_callback_set_EnterDwellTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->enterDwellTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace IdlePowerSaver
{
static const auto _property_EnterDwellTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto IdlePowerSaver::exitUtilizationPercent() const ->
        uint8_t
{
    return _exitUtilizationPercent;
}

int IdlePowerSaver::_callback_get_ExitUtilizationPercent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->exitUtilizationPercent();
                    }
                ));
    }
}

auto IdlePowerSaver::exitUtilizationPercent(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_exitUtilizationPercent != value)
    {
        _exitUtilizationPercent = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.property_changed("ExitUtilizationPercent");
        }
    }

    return _exitUtilizationPercent;
}

auto IdlePowerSaver::exitUtilizationPercent(uint8_t val) ->
        uint8_t
{
    return exitUtilizationPercent(val, false);
}

int IdlePowerSaver::_callback_set_ExitUtilizationPercent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->exitUtilizationPercent(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace IdlePowerSaver
{
static const auto _property_ExitUtilizationPercent =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto IdlePowerSaver::exitDwellTime() const ->
        uint64_t
{
    return _exitDwellTime;
}

int IdlePowerSaver::_callback_get_ExitDwellTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->exitDwellTime();
                    }
                ));
    }
}

auto IdlePowerSaver::exitDwellTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_exitDwellTime != value)
    {
        _exitDwellTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.property_changed("ExitDwellTime");
        }
    }

    return _exitDwellTime;
}

auto IdlePowerSaver::exitDwellTime(uint64_t val) ->
        uint64_t
{
    return exitDwellTime(val, false);
}

int IdlePowerSaver::_callback_set_ExitDwellTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->exitDwellTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace IdlePowerSaver
{
static const auto _property_ExitDwellTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto IdlePowerSaver::active() const ->
        bool
{
    return _active;
}

int IdlePowerSaver::_callback_get_Active(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IdlePowerSaver*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->active();
                    }
                ));
    }
}

auto IdlePowerSaver::active(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_active != value)
    {
        _active = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_IdlePowerSaver_interface.property_changed("Active");
        }
    }

    return _active;
}

auto IdlePowerSaver::active(bool val) ->
        bool
{
    return active(val, false);
}


namespace details
{
namespace IdlePowerSaver
{
static const auto _property_Active =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void IdlePowerSaver::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
    if (_name == "EnterUtilizationPercent")
    {
        auto& v = std::get<uint8_t>(val);
        enterUtilizationPercent(v, skipSignal);
        return;
    }
    if (_name == "EnterDwellTime")
    {
        auto& v = std::get<uint64_t>(val);
        enterDwellTime(v, skipSignal);
        return;
    }
    if (_name == "ExitUtilizationPercent")
    {
        auto& v = std::get<uint8_t>(val);
        exitUtilizationPercent(v, skipSignal);
        return;
    }
    if (_name == "ExitDwellTime")
    {
        auto& v = std::get<uint64_t>(val);
        exitDwellTime(v, skipSignal);
        return;
    }
    if (_name == "Active")
    {
        auto& v = std::get<bool>(val);
        active(v, skipSignal);
        return;
    }
}

auto IdlePowerSaver::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Enabled")
    {
        return enabled();
    }
    if (_name == "EnterUtilizationPercent")
    {
        return enterUtilizationPercent();
    }
    if (_name == "EnterDwellTime")
    {
        return enterDwellTime();
    }
    if (_name == "ExitUtilizationPercent")
    {
        return exitUtilizationPercent();
    }
    if (_name == "ExitDwellTime")
    {
        return exitDwellTime();
    }
    if (_name == "Active")
    {
        return active();
    }

    return PropertiesVariant();
}


const vtable_t IdlePowerSaver::_vtable[] = {
    vtable::start(),
    vtable::property("Enabled",
                     details::IdlePowerSaver::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::property("EnterUtilizationPercent",
                     details::IdlePowerSaver::_property_EnterUtilizationPercent
                        .data(),
                     _callback_get_EnterUtilizationPercent,
                     _callback_set_EnterUtilizationPercent,
                     vtable::property_::emits_change),
    vtable::property("EnterDwellTime",
                     details::IdlePowerSaver::_property_EnterDwellTime
                        .data(),
                     _callback_get_EnterDwellTime,
                     _callback_set_EnterDwellTime,
                     vtable::property_::emits_change),
    vtable::property("ExitUtilizationPercent",
                     details::IdlePowerSaver::_property_ExitUtilizationPercent
                        .data(),
                     _callback_get_ExitUtilizationPercent,
                     _callback_set_ExitUtilizationPercent,
                     vtable::property_::emits_change),
    vtable::property("ExitDwellTime",
                     details::IdlePowerSaver::_property_ExitDwellTime
                        .data(),
                     _callback_get_ExitDwellTime,
                     _callback_set_ExitDwellTime,
                     vtable::property_::emits_change),
    vtable::property("Active",
                     details::IdlePowerSaver::_property_Active
                        .data(),
                     _callback_get_Active,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

