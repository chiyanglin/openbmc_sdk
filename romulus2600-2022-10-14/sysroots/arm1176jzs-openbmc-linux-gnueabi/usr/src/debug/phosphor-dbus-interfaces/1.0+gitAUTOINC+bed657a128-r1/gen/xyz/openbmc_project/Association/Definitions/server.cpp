#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Association/Definitions/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Association
{
namespace server
{

Definitions::Definitions(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Association_Definitions_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Definitions::Definitions(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Definitions(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Definitions::associations() const ->
        std::vector<std::tuple<std::string, std::string, std::string>>
{
    return _associations;
}

int Definitions::_callback_get_Associations(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Definitions*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->associations();
                    }
                ));
    }
}

auto Definitions::associations(std::vector<std::tuple<std::string, std::string, std::string>> value,
                                         bool skipSignal) ->
        std::vector<std::tuple<std::string, std::string, std::string>>
{
    if (_associations != value)
    {
        _associations = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Association_Definitions_interface.property_changed("Associations");
        }
    }

    return _associations;
}

auto Definitions::associations(std::vector<std::tuple<std::string, std::string, std::string>> val) ->
        std::vector<std::tuple<std::string, std::string, std::string>>
{
    return associations(val, false);
}

int Definitions::_callback_set_Associations(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Definitions*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::tuple<std::string, std::string, std::string>>&& arg)
                    {
                        o->associations(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Definitions
{
static const auto _property_Associations =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::tuple<std::string, std::string, std::string>>>());
}
}

void Definitions::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Associations")
    {
        auto& v = std::get<std::vector<std::tuple<std::string, std::string, std::string>>>(val);
        associations(v, skipSignal);
        return;
    }
}

auto Definitions::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Associations")
    {
        return associations();
    }

    return PropertiesVariant();
}


const vtable_t Definitions::_vtable[] = {
    vtable::start(),
    vtable::property("Associations",
                     details::Definitions::_property_Associations
                        .data(),
                     _callback_get_Associations,
                     _callback_set_Associations,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Association
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

