#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/ValueMutability/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace server
{

ValueMutability::ValueMutability(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_ValueMutability_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ValueMutability::ValueMutability(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ValueMutability(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ValueMutability::mutable_() const ->
        bool
{
    return _mutable_;
}

int ValueMutability::_callback_get_Mutable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ValueMutability*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mutable_();
                    }
                ));
    }
}

auto ValueMutability::mutable_(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_mutable_ != value)
    {
        _mutable_ = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_ValueMutability_interface.property_changed("Mutable");
        }
    }

    return _mutable_;
}

auto ValueMutability::mutable_(bool val) ->
        bool
{
    return mutable_(val, false);
}

int ValueMutability::_callback_set_Mutable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ValueMutability*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->mutable_(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ValueMutability
{
static const auto _property_Mutable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void ValueMutability::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Mutable")
    {
        auto& v = std::get<bool>(val);
        mutable_(v, skipSignal);
        return;
    }
}

auto ValueMutability::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Mutable")
    {
        return mutable_();
    }

    return PropertiesVariant();
}


const vtable_t ValueMutability::_vtable[] = {
    vtable::start(),
    vtable::property("Mutable",
                     details::ValueMutability::_property_Mutable
                        .data(),
                     _callback_get_Mutable,
                     _callback_set_Mutable,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

