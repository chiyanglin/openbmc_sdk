#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

class FactoryReset
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        FactoryReset() = delete;
        FactoryReset(const FactoryReset&) = delete;
        FactoryReset& operator=(const FactoryReset&) = delete;
        FactoryReset(FactoryReset&&) = delete;
        FactoryReset& operator=(FactoryReset&&) = delete;
        virtual ~FactoryReset() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        FactoryReset(bus_t& bus, const char* path);



        /** @brief Implementation for Reset
         *  Generic method to start factory reset. Implemented by each service.
         */
        virtual void reset(
            ) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Common_FactoryReset_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Common_FactoryReset_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Common.FactoryReset";

    private:

        /** @brief sd-bus callback for Reset
         */
        static int _callback_Reset(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Common_FactoryReset_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

