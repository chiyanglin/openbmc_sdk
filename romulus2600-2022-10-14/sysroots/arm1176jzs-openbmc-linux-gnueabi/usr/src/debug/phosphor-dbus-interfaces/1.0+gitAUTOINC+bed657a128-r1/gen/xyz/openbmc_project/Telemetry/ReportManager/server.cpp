#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Telemetry/ReportManager/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

ReportManager::ReportManager(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Telemetry_ReportManager_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ReportManager::ReportManager(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ReportManager(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int ReportManager::_callback_AddReport(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<ReportManager*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<std::map<std::string, std::variant<bool, uint32_t, uint64_t, std::string, xyz::openbmc_project::Telemetry::server::Report::ReportingType, xyz::openbmc_project::Telemetry::server::Report::ReportUpdates, std::vector<xyz::openbmc_project::Telemetry::server::Report::ReportActions>, std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, xyz::openbmc_project::Telemetry::server::Report::OperationType, std::string, xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope, uint64_t>>>>>&& properties)
                    {
                        return o->addReport(
                                properties);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace ReportManager
{
static const auto _param_AddReport =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::map<std::string, std::variant<bool, uint32_t, uint64_t, std::string, xyz::openbmc_project::Telemetry::server::Report::ReportingType, xyz::openbmc_project::Telemetry::server::Report::ReportUpdates, std::vector<xyz::openbmc_project::Telemetry::server::Report::ReportActions>, std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, xyz::openbmc_project::Telemetry::server::Report::OperationType, std::string, xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope, uint64_t>>>>>>());
static const auto _return_AddReport =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}



auto ReportManager::maxReports() const ->
        size_t
{
    return _maxReports;
}

int ReportManager::_callback_get_MaxReports(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ReportManager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxReports();
                    }
                ));
    }
}

auto ReportManager::maxReports(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_maxReports != value)
    {
        _maxReports = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_ReportManager_interface.property_changed("MaxReports");
        }
    }

    return _maxReports;
}

auto ReportManager::maxReports(size_t val) ->
        size_t
{
    return maxReports(val, false);
}


namespace details
{
namespace ReportManager
{
static const auto _property_MaxReports =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto ReportManager::minInterval() const ->
        uint64_t
{
    return _minInterval;
}

int ReportManager::_callback_get_MinInterval(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ReportManager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minInterval();
                    }
                ));
    }
}

auto ReportManager::minInterval(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_minInterval != value)
    {
        _minInterval = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_ReportManager_interface.property_changed("MinInterval");
        }
    }

    return _minInterval;
}

auto ReportManager::minInterval(uint64_t val) ->
        uint64_t
{
    return minInterval(val, false);
}


namespace details
{
namespace ReportManager
{
static const auto _property_MinInterval =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto ReportManager::supportedOperationTypes() const ->
        std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType>
{
    return _supportedOperationTypes;
}

int ReportManager::_callback_get_SupportedOperationTypes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ReportManager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->supportedOperationTypes();
                    }
                ));
    }
}

auto ReportManager::supportedOperationTypes(std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> value,
                                         bool skipSignal) ->
        std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType>
{
    if (_supportedOperationTypes != value)
    {
        _supportedOperationTypes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_ReportManager_interface.property_changed("SupportedOperationTypes");
        }
    }

    return _supportedOperationTypes;
}

auto ReportManager::supportedOperationTypes(std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType> val) ->
        std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType>
{
    return supportedOperationTypes(val, false);
}


namespace details
{
namespace ReportManager
{
static const auto _property_SupportedOperationTypes =
    utility::tuple_to_array(message::types::type_id<
            std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType>>());
}
}

void ReportManager::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MaxReports")
    {
        auto& v = std::get<size_t>(val);
        maxReports(v, skipSignal);
        return;
    }
    if (_name == "MinInterval")
    {
        auto& v = std::get<uint64_t>(val);
        minInterval(v, skipSignal);
        return;
    }
    if (_name == "SupportedOperationTypes")
    {
        auto& v = std::get<std::vector<xyz::openbmc_project::Telemetry::server::Report::OperationType>>(val);
        supportedOperationTypes(v, skipSignal);
        return;
    }
}

auto ReportManager::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MaxReports")
    {
        return maxReports();
    }
    if (_name == "MinInterval")
    {
        return minInterval();
    }
    if (_name == "SupportedOperationTypes")
    {
        return supportedOperationTypes();
    }

    return PropertiesVariant();
}


const vtable_t ReportManager::_vtable[] = {
    vtable::start(),

    vtable::method("AddReport",
                   details::ReportManager::_param_AddReport
                        .data(),
                   details::ReportManager::_return_AddReport
                        .data(),
                   _callback_AddReport),
    vtable::property("MaxReports",
                     details::ReportManager::_property_MaxReports
                        .data(),
                     _callback_get_MaxReports,
                     vtable::property_::const_),
    vtable::property("MinInterval",
                     details::ReportManager::_property_MinInterval
                        .data(),
                     _callback_get_MinInterval,
                     vtable::property_::const_),
    vtable::property("SupportedOperationTypes",
                     details::ReportManager::_property_SupportedOperationTypes
                        .data(),
                     _callback_get_SupportedOperationTypes,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

