#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/ScheduledHostTransition/server.hpp>

#include <xyz/openbmc_project/ScheduledTime/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

ScheduledHostTransition::ScheduledHostTransition(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_ScheduledHostTransition_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ScheduledHostTransition::ScheduledHostTransition(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ScheduledHostTransition(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ScheduledHostTransition::scheduledTime() const ->
        uint64_t
{
    return _scheduledTime;
}

int ScheduledHostTransition::_callback_get_ScheduledTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ScheduledHostTransition*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->scheduledTime();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::ScheduledTime::Error::InvalidTime& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto ScheduledHostTransition::scheduledTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_scheduledTime != value)
    {
        _scheduledTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_ScheduledHostTransition_interface.property_changed("ScheduledTime");
        }
    }

    return _scheduledTime;
}

auto ScheduledHostTransition::scheduledTime(uint64_t val) ->
        uint64_t
{
    return scheduledTime(val, false);
}

int ScheduledHostTransition::_callback_set_ScheduledTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ScheduledHostTransition*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->scheduledTime(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::ScheduledTime::Error::InvalidTime& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace ScheduledHostTransition
{
static const auto _property_ScheduledTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto ScheduledHostTransition::scheduledTransition() const ->
        xyz::openbmc_project::State::server::Host::Transition
{
    return _scheduledTransition;
}

int ScheduledHostTransition::_callback_get_ScheduledTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ScheduledHostTransition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->scheduledTransition();
                    }
                ));
    }
}

auto ScheduledHostTransition::scheduledTransition(xyz::openbmc_project::State::server::Host::Transition value,
                                         bool skipSignal) ->
        xyz::openbmc_project::State::server::Host::Transition
{
    if (_scheduledTransition != value)
    {
        _scheduledTransition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_ScheduledHostTransition_interface.property_changed("ScheduledTransition");
        }
    }

    return _scheduledTransition;
}

auto ScheduledHostTransition::scheduledTransition(xyz::openbmc_project::State::server::Host::Transition val) ->
        xyz::openbmc_project::State::server::Host::Transition
{
    return scheduledTransition(val, false);
}

int ScheduledHostTransition::_callback_set_ScheduledTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ScheduledHostTransition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](xyz::openbmc_project::State::server::Host::Transition&& arg)
                    {
                        o->scheduledTransition(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ScheduledHostTransition
{
static const auto _property_ScheduledTransition =
    utility::tuple_to_array(message::types::type_id<
            xyz::openbmc_project::State::server::Host::Transition>());
}
}

void ScheduledHostTransition::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ScheduledTime")
    {
        auto& v = std::get<uint64_t>(val);
        scheduledTime(v, skipSignal);
        return;
    }
    if (_name == "ScheduledTransition")
    {
        auto& v = std::get<xyz::openbmc_project::State::server::Host::Transition>(val);
        scheduledTransition(v, skipSignal);
        return;
    }
}

auto ScheduledHostTransition::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ScheduledTime")
    {
        return scheduledTime();
    }
    if (_name == "ScheduledTransition")
    {
        return scheduledTransition();
    }

    return PropertiesVariant();
}


const vtable_t ScheduledHostTransition::_vtable[] = {
    vtable::start(),
    vtable::property("ScheduledTime",
                     details::ScheduledHostTransition::_property_ScheduledTime
                        .data(),
                     _callback_get_ScheduledTime,
                     _callback_set_ScheduledTime,
                     vtable::property_::emits_change),
    vtable::property("ScheduledTransition",
                     details::ScheduledHostTransition::_property_ScheduledTransition
                        .data(),
                     _callback_get_ScheduledTransition,
                     _callback_set_ScheduledTransition,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

