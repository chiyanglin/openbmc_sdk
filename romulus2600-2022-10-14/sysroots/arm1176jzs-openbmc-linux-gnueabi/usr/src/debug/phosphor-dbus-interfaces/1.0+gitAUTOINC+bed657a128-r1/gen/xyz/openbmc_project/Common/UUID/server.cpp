#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/UUID/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

UUID::UUID(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_UUID_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

UUID::UUID(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : UUID(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto UUID::uuid() const ->
        std::string
{
    return _uuid;
}

int UUID::_callback_get_UUID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UUID*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->uuid();
                    }
                ));
    }
}

auto UUID::uuid(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_uuid != value)
    {
        _uuid = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_UUID_interface.property_changed("UUID");
        }
    }

    return _uuid;
}

auto UUID::uuid(std::string val) ->
        std::string
{
    return uuid(val, false);
}

int UUID::_callback_set_UUID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UUID*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->uuid(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UUID
{
static const auto _property_UUID =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void UUID::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "UUID")
    {
        auto& v = std::get<std::string>(val);
        uuid(v, skipSignal);
        return;
    }
}

auto UUID::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "UUID")
    {
        return uuid();
    }

    return PropertiesVariant();
}


const vtable_t UUID::_vtable[] = {
    vtable::start(),
    vtable::property("UUID",
                     details::UUID::_property_UUID
                        .data(),
                     _callback_get_UUID,
                     _callback_set_UUID,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

