#include <xyz/openbmc_project/State/Host/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Host
{
namespace Error
{
const char* SoftOffTimeout::name() const noexcept
{
    return errName;
}
const char* SoftOffTimeout::description() const noexcept
{
    return errDesc;
}
const char* SoftOffTimeout::what() const noexcept
{
    return errWhat;
}
const char* HostStartFailure::name() const noexcept
{
    return errName;
}
const char* HostStartFailure::description() const noexcept
{
    return errDesc;
}
const char* HostStartFailure::what() const noexcept
{
    return errWhat;
}
const char* HostMinStartFailure::name() const noexcept
{
    return errName;
}
const char* HostMinStartFailure::description() const noexcept
{
    return errDesc;
}
const char* HostMinStartFailure::what() const noexcept
{
    return errWhat;
}
const char* HostShutdownFailure::name() const noexcept
{
    return errName;
}
const char* HostShutdownFailure::description() const noexcept
{
    return errDesc;
}
const char* HostShutdownFailure::what() const noexcept
{
    return errWhat;
}
const char* HostStopFailure::name() const noexcept
{
    return errName;
}
const char* HostStopFailure::description() const noexcept
{
    return errDesc;
}
const char* HostStopFailure::what() const noexcept
{
    return errWhat;
}
const char* HostRebootFailure::name() const noexcept
{
    return errName;
}
const char* HostRebootFailure::description() const noexcept
{
    return errDesc;
}
const char* HostRebootFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Host
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

