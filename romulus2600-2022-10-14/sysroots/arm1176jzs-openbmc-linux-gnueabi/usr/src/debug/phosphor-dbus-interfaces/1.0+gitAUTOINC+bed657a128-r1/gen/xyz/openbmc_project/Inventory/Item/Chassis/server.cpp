#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Chassis/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Chassis::Chassis(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Chassis_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Chassis::Chassis(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Chassis(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Chassis::type() const ->
        ChassisType
{
    return _type;
}

int Chassis::_callback_get_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->type();
                    }
                ));
    }
}

auto Chassis::type(ChassisType value,
                                         bool skipSignal) ->
        ChassisType
{
    if (_type != value)
    {
        _type = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Chassis_interface.property_changed("Type");
        }
    }

    return _type;
}

auto Chassis::type(ChassisType val) ->
        ChassisType
{
    return type(val, false);
}

int Chassis::_callback_set_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ChassisType&& arg)
                    {
                        o->type(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Chassis
{
static const auto _property_Type =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Chassis::ChassisType>());
}
}

void Chassis::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Type")
    {
        auto& v = std::get<ChassisType>(val);
        type(v, skipSignal);
        return;
    }
}

auto Chassis::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Type")
    {
        return type();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Chassis::ChassisType */
static const std::tuple<const char*, Chassis::ChassisType> mappingChassisChassisType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.Component",                 Chassis::ChassisType::Component ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.Enclosure",                 Chassis::ChassisType::Enclosure ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.Module",                 Chassis::ChassisType::Module ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.RackMount",                 Chassis::ChassisType::RackMount ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.StandAlone",                 Chassis::ChassisType::StandAlone ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.StorageEnclosure",                 Chassis::ChassisType::StorageEnclosure ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.Unknown",                 Chassis::ChassisType::Unknown ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Chassis.ChassisType.Zone",                 Chassis::ChassisType::Zone ),
        };

} // anonymous namespace

auto Chassis::convertStringToChassisType(const std::string& s) noexcept ->
        std::optional<ChassisType>
{
    auto i = std::find_if(
            std::begin(mappingChassisChassisType),
            std::end(mappingChassisChassisType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingChassisChassisType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Chassis::convertChassisTypeFromString(const std::string& s) ->
        ChassisType
{
    auto r = convertStringToChassisType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Chassis::convertChassisTypeToString(Chassis::ChassisType v)
{
    auto i = std::find_if(
            std::begin(mappingChassisChassisType),
            std::end(mappingChassisChassisType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingChassisChassisType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Chassis::_vtable[] = {
    vtable::start(),
    vtable::property("Type",
                     details::Chassis::_property_Type
                        .data(),
                     _callback_get_Type,
                     _callback_set_Type,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

