#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VCFG/server.hpp>





namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VCFG::VCFG(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VCFG_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VCFG::VCFG(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VCFG(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VCFG::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VCFG::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VCFG::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCFG_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VCFG::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VCFG::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCFG
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCFG::vz() const ->
        std::vector<uint8_t>
{
    return _vz;
}

int VCFG::_callback_get_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vz();
                    }
                ));
    }
}

auto VCFG::vz(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vz != value)
    {
        _vz = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCFG_interface.property_changed("VZ");
        }
    }

    return _vz;
}

auto VCFG::vz(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vz(val, false);
}

int VCFG::_callback_set_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCFG
{
static const auto _property_VZ =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCFG::z0() const ->
        std::vector<uint8_t>
{
    return _z0;
}

int VCFG::_callback_get_Z0(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->z0();
                    }
                ));
    }
}

auto VCFG::z0(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_z0 != value)
    {
        _z0 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCFG_interface.property_changed("Z0");
        }
    }

    return _z0;
}

auto VCFG::z0(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return z0(val, false);
}

int VCFG::_callback_set_Z0(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->z0(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCFG
{
static const auto _property_Z0 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCFG::z1() const ->
        std::vector<uint8_t>
{
    return _z1;
}

int VCFG::_callback_get_Z1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->z1();
                    }
                ));
    }
}

auto VCFG::z1(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_z1 != value)
    {
        _z1 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCFG_interface.property_changed("Z1");
        }
    }

    return _z1;
}

auto VCFG::z1(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return z1(val, false);
}

int VCFG::_callback_set_Z1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCFG*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->z1(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCFG
{
static const auto _property_Z1 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VCFG::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VZ")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vz(v, skipSignal);
        return;
    }
    if (_name == "Z0")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        z0(v, skipSignal);
        return;
    }
    if (_name == "Z1")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        z1(v, skipSignal);
        return;
    }
}

auto VCFG::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VZ")
    {
        return vz();
    }
    if (_name == "Z0")
    {
        return z0();
    }
    if (_name == "Z1")
    {
        return z1();
    }

    return PropertiesVariant();
}


const vtable_t VCFG::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VCFG::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VZ",
                     details::VCFG::_property_VZ
                        .data(),
                     _callback_get_VZ,
                     _callback_set_VZ,
                     vtable::property_::emits_change),
    vtable::property("Z0",
                     details::VCFG::_property_Z0
                        .data(),
                     _callback_get_Z0,
                     _callback_set_Z0,
                     vtable::property_::emits_change),
    vtable::property("Z1",
                     details::VCFG::_property_Z1
                        .data(),
                     _callback_get_Z1,
                     _callback_set_Z1,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

