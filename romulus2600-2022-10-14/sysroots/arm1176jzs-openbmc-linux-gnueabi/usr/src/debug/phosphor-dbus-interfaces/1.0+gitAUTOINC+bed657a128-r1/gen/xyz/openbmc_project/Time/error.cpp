#include <xyz/openbmc_project/Time/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Time
{
namespace Error
{
const char* NotAllowed::name() const noexcept
{
    return errName;
}
const char* NotAllowed::description() const noexcept
{
    return errDesc;
}
const char* NotAllowed::what() const noexcept
{
    return errWhat;
}
const char* Failed::name() const noexcept
{
    return errName;
}
const char* Failed::description() const noexcept
{
    return errDesc;
}
const char* Failed::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Time
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

