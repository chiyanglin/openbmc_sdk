#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

class VoltageRegulatorMode
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        VoltageRegulatorMode() = delete;
        VoltageRegulatorMode(const VoltageRegulatorMode&) = delete;
        VoltageRegulatorMode& operator=(const VoltageRegulatorMode&) = delete;
        VoltageRegulatorMode(VoltageRegulatorMode&&) = delete;
        VoltageRegulatorMode& operator=(VoltageRegulatorMode&&) = delete;
        virtual ~VoltageRegulatorMode() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        VoltageRegulatorMode(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::string,
                std::vector<std::string>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        VoltageRegulatorMode(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Supported */
        virtual std::vector<std::string> supported() const;
        /** Set value of Supported with option to skip sending signal */
        virtual std::vector<std::string> supported(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of Supported */
        virtual std::vector<std::string> supported(std::vector<std::string> value);
        /** Get value of Selected */
        virtual std::string selected() const;
        /** Set value of Selected with option to skip sending signal */
        virtual std::string selected(std::string value,
               bool skipSignal);
        /** Set value of Selected */
        virtual std::string selected(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_VoltageRegulatorMode_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_VoltageRegulatorMode_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.VoltageRegulatorMode";

    private:

        /** @brief sd-bus callback for get-property 'Supported' */
        static int _callback_get_Supported(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Selected' */
        static int _callback_get_Selected(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Selected' */
        static int _callback_set_Selected(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_VoltageRegulatorMode_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<std::string> _supported{};
        std::string _selected = "Default";

};


} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

