#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

class Host
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Host() = delete;
        Host(const Host&) = delete;
        Host& operator=(const Host&) = delete;
        Host(Host&&) = delete;
        Host& operator=(Host&&) = delete;
        virtual ~Host() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Host(bus_t& bus, const char* path);

        enum class Transition
        {
            Off,
            On,
            Reboot,
            GracefulWarmReboot,
            ForceWarmReboot,
        };
        enum class HostState
        {
            Off,
            TransitioningToOff,
            Standby,
            Running,
            TransitioningToRunning,
            Quiesced,
            DiagnosticMode,
        };
        enum class RestartCause
        {
            Unknown,
            RemoteCommand,
            ResetButton,
            PowerButton,
            WatchdogTimer,
            PowerPolicyAlwaysOn,
            PowerPolicyPreviousState,
            SoftReset,
            ScheduledPowerOn,
            HostCrash,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                HostState,
                RestartCause,
                Transition>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Host(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RequestedHostTransition */
        virtual Transition requestedHostTransition() const;
        /** Set value of RequestedHostTransition with option to skip sending signal */
        virtual Transition requestedHostTransition(Transition value,
               bool skipSignal);
        /** Set value of RequestedHostTransition */
        virtual Transition requestedHostTransition(Transition value);
        /** Get value of CurrentHostState */
        virtual HostState currentHostState() const;
        /** Set value of CurrentHostState with option to skip sending signal */
        virtual HostState currentHostState(HostState value,
               bool skipSignal);
        /** Set value of CurrentHostState */
        virtual HostState currentHostState(HostState value);
        /** Get value of RestartCause */
        virtual RestartCause restartCause() const;
        /** Set value of RestartCause with option to skip sending signal */
        virtual RestartCause restartCause(RestartCause value,
               bool skipSignal);
        /** Set value of RestartCause */
        virtual RestartCause restartCause(RestartCause value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Host.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Transition convertTransitionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Host.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Transition> convertStringToTransition(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Host.<value name>"
         */
        static std::string convertTransitionToString(Transition e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Host.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static HostState convertHostStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Host.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<HostState> convertStringToHostState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Host.<value name>"
         */
        static std::string convertHostStateToString(HostState e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Host.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static RestartCause convertRestartCauseFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Host.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<RestartCause> convertStringToRestartCause(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Host.<value name>"
         */
        static std::string convertRestartCauseToString(RestartCause e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_Host_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_Host_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.Host";

    private:

        /** @brief sd-bus callback for get-property 'RequestedHostTransition' */
        static int _callback_get_RequestedHostTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RequestedHostTransition' */
        static int _callback_set_RequestedHostTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentHostState' */
        static int _callback_get_CurrentHostState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CurrentHostState' */
        static int _callback_set_CurrentHostState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RestartCause' */
        static int _callback_get_RestartCause(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RestartCause' */
        static int _callback_set_RestartCause(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_Host_interface;
        sdbusplus::SdBusInterface *_intf;

        Transition _requestedHostTransition = Transition::Off;
        HostState _currentHostState{};
        RestartCause _restartCause = RestartCause::Unknown;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Host::Transition.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Host::Transition e)
{
    return Host::convertTransitionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Host::HostState.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Host::HostState e)
{
    return Host::convertHostStateToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Host::RestartCause.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Host::RestartCause e)
{
    return Host::convertRestartCauseToString(e);
}

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Host::Transition>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Host::convertStringToTransition(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Host::Transition>
{
    static std::string op(xyz::openbmc_project::State::server::Host::Transition value)
    {
        return xyz::openbmc_project::State::server::Host::convertTransitionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Host::HostState>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Host::convertStringToHostState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Host::HostState>
{
    static std::string op(xyz::openbmc_project::State::server::Host::HostState value)
    {
        return xyz::openbmc_project::State::server::Host::convertHostStateToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::Host::RestartCause>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::Host::convertStringToRestartCause(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::Host::RestartCause>
{
    static std::string op(xyz::openbmc_project::State::server::Host::RestartCause value)
    {
        return xyz::openbmc_project::State::server::Host::convertRestartCauseToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

