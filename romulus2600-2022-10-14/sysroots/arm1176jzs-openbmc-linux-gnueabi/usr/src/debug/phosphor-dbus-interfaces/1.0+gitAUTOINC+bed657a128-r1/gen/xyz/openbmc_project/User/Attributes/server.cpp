#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/Attributes/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

Attributes::Attributes(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_Attributes_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Attributes::Attributes(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Attributes(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Attributes::userGroups() const ->
        std::vector<std::string>
{
    return _userGroups;
}

int Attributes::_callback_get_UserGroups(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userGroups();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::userGroups(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_userGroups != value)
    {
        _userGroups = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Attributes_interface.property_changed("UserGroups");
        }
    }

    return _userGroups;
}

auto Attributes::userGroups(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return userGroups(val, false);
}

int Attributes::_callback_set_UserGroups(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->userGroups(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_UserGroups =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto Attributes::userPrivilege() const ->
        std::string
{
    return _userPrivilege;
}

int Attributes::_callback_get_UserPrivilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userPrivilege();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::userPrivilege(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_userPrivilege != value)
    {
        _userPrivilege = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Attributes_interface.property_changed("UserPrivilege");
        }
    }

    return _userPrivilege;
}

auto Attributes::userPrivilege(std::string val) ->
        std::string
{
    return userPrivilege(val, false);
}

int Attributes::_callback_set_UserPrivilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->userPrivilege(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_UserPrivilege =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Attributes::userEnabled() const ->
        bool
{
    return _userEnabled;
}

int Attributes::_callback_get_UserEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userEnabled();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::userEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_userEnabled != value)
    {
        _userEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Attributes_interface.property_changed("UserEnabled");
        }
    }

    return _userEnabled;
}

auto Attributes::userEnabled(bool val) ->
        bool
{
    return userEnabled(val, false);
}

int Attributes::_callback_set_UserEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->userEnabled(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_UserEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Attributes::userLockedForFailedAttempt() const ->
        bool
{
    return _userLockedForFailedAttempt;
}

int Attributes::_callback_get_UserLockedForFailedAttempt(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userLockedForFailedAttempt();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::userLockedForFailedAttempt(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_userLockedForFailedAttempt != value)
    {
        _userLockedForFailedAttempt = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Attributes_interface.property_changed("UserLockedForFailedAttempt");
        }
    }

    return _userLockedForFailedAttempt;
}

auto Attributes::userLockedForFailedAttempt(bool val) ->
        bool
{
    return userLockedForFailedAttempt(val, false);
}

int Attributes::_callback_set_UserLockedForFailedAttempt(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->userLockedForFailedAttempt(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_UserLockedForFailedAttempt =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Attributes::remoteUser() const ->
        bool
{
    return _remoteUser;
}

int Attributes::_callback_get_RemoteUser(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->remoteUser();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::remoteUser(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_remoteUser != value)
    {
        _remoteUser = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Attributes_interface.property_changed("RemoteUser");
        }
    }

    return _remoteUser;
}

auto Attributes::remoteUser(bool val) ->
        bool
{
    return remoteUser(val, false);
}


namespace details
{
namespace Attributes
{
static const auto _property_RemoteUser =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Attributes::userPasswordExpired() const ->
        bool
{
    return _userPasswordExpired;
}

int Attributes::_callback_get_UserPasswordExpired(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userPasswordExpired();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Attributes::userPasswordExpired(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_userPasswordExpired != value)
    {
        _userPasswordExpired = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_Attributes_interface.property_changed("UserPasswordExpired");
        }
    }

    return _userPasswordExpired;
}

auto Attributes::userPasswordExpired(bool val) ->
        bool
{
    return userPasswordExpired(val, false);
}


namespace details
{
namespace Attributes
{
static const auto _property_UserPasswordExpired =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Attributes::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "UserGroups")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        userGroups(v, skipSignal);
        return;
    }
    if (_name == "UserPrivilege")
    {
        auto& v = std::get<std::string>(val);
        userPrivilege(v, skipSignal);
        return;
    }
    if (_name == "UserEnabled")
    {
        auto& v = std::get<bool>(val);
        userEnabled(v, skipSignal);
        return;
    }
    if (_name == "UserLockedForFailedAttempt")
    {
        auto& v = std::get<bool>(val);
        userLockedForFailedAttempt(v, skipSignal);
        return;
    }
    if (_name == "RemoteUser")
    {
        auto& v = std::get<bool>(val);
        remoteUser(v, skipSignal);
        return;
    }
    if (_name == "UserPasswordExpired")
    {
        auto& v = std::get<bool>(val);
        userPasswordExpired(v, skipSignal);
        return;
    }
}

auto Attributes::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "UserGroups")
    {
        return userGroups();
    }
    if (_name == "UserPrivilege")
    {
        return userPrivilege();
    }
    if (_name == "UserEnabled")
    {
        return userEnabled();
    }
    if (_name == "UserLockedForFailedAttempt")
    {
        return userLockedForFailedAttempt();
    }
    if (_name == "RemoteUser")
    {
        return remoteUser();
    }
    if (_name == "UserPasswordExpired")
    {
        return userPasswordExpired();
    }

    return PropertiesVariant();
}


const vtable_t Attributes::_vtable[] = {
    vtable::start(),
    vtable::property("UserGroups",
                     details::Attributes::_property_UserGroups
                        .data(),
                     _callback_get_UserGroups,
                     _callback_set_UserGroups,
                     vtable::property_::emits_change),
    vtable::property("UserPrivilege",
                     details::Attributes::_property_UserPrivilege
                        .data(),
                     _callback_get_UserPrivilege,
                     _callback_set_UserPrivilege,
                     vtable::property_::emits_change),
    vtable::property("UserEnabled",
                     details::Attributes::_property_UserEnabled
                        .data(),
                     _callback_get_UserEnabled,
                     _callback_set_UserEnabled,
                     vtable::property_::emits_change),
    vtable::property("UserLockedForFailedAttempt",
                     details::Attributes::_property_UserLockedForFailedAttempt
                        .data(),
                     _callback_get_UserLockedForFailedAttempt,
                     _callback_set_UserLockedForFailedAttempt,
                     vtable::property_::emits_change),
    vtable::property("RemoteUser",
                     details::Attributes::_property_RemoteUser
                        .data(),
                     _callback_get_RemoteUser,
                     vtable::property_::const_),
    vtable::property("UserPasswordExpired",
                     details::Attributes::_property_UserPasswordExpired
                        .data(),
                     _callback_get_UserPasswordExpired,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

