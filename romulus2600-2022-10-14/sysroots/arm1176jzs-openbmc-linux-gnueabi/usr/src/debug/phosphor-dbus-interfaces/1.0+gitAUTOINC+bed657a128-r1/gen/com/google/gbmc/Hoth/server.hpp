#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace google
{
namespace gbmc
{
namespace server
{

class Hoth
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Hoth() = delete;
        Hoth(const Hoth&) = delete;
        Hoth& operator=(const Hoth&) = delete;
        Hoth(Hoth&&) = delete;
        Hoth& operator=(Hoth&&) = delete;
        virtual ~Hoth() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Hoth(bus_t& bus, const char* path);

        enum class FirmwareUpdateStatus
        {
            None,
            InProgress,
            Error,
            Done,
        };


        /** @brief Implementation for SendHostCommand
         *  Send a host command to Hoth and return the response. This will block on Hoth completing its internal operations. Critical host commands like LoadTokens are banned. This method should be called from the IPMI HostCommand passthrough.
         *
         *  @param[in] command - Data to write to Hoth SPI host command offset.
         *
         *  @return response[std::vector<uint8_t>] - Data read from Hoth SPI host command offset.
         */
        virtual std::vector<uint8_t> sendHostCommand(
            std::vector<uint8_t> command) = 0;

        /** @brief Implementation for SendTrustedHostCommand
         *  Send a host command to Hoth and return the response. This will block on Hoth completing its internal operations. Critical host commands like LoadTokens are allowed. This method should be called from the BMC.
         *
         *  @param[in] command - Data to write to Hoth SPI host command offset.
         *
         *  @return response[std::vector<uint8_t>] - Data read from Hoth SPI host command offset.
         */
        virtual std::vector<uint8_t> sendTrustedHostCommand(
            std::vector<uint8_t> command) = 0;

        /** @brief Implementation for SendHostCommandAsync
         *  Send a host command to Hoth and immediately return without waiting for response. Caller can either poll with calls to GetHostCommandResponse until retrieving the response, or wait for a ResponseReady signal.
         *
         *  @param[in] command - Data to write to Hoth SPI host command offset.
         *
         *  @return callToken[uint64_t] - The representation for the call made
         */
        virtual uint64_t sendHostCommandAsync(
            std::vector<uint8_t> command) = 0;

        /** @brief Implementation for GetHostCommandResponse
         *  Read the response from Hoth mailbox.
         *
         *  @param[in] callToken - The token returned from SendHostCommandAsync()
         *
         *  @return response[std::vector<uint8_t>] - Data read from Hoth SPI host command offset
         */
        virtual std::vector<uint8_t> getHostCommandResponse(
            uint64_t callToken) = 0;

        /** @brief Implementation for UpdateFirmware
         *  Write given firmware data to the Hoth firmware partition in EEPROM.
         *
         *  @param[in] firmwareData - Hoth firmware image
         */
        virtual void updateFirmware(
            std::vector<uint8_t> firmwareData) = 0;

        /** @brief Implementation for GetFirmwareUpdateStatus
         *  Get the status of the firmware update process.
         *
         *  @return status[FirmwareUpdateStatus] - Status of the firmware update
         */
        virtual FirmwareUpdateStatus getFirmwareUpdateStatus(
            ) = 0;

        /** @brief Implementation for InitiatePayload
         *  Initiates erasure of the EEPROM staging area. Note that this will lock up access to Hoth for an extended time and may go over the kernel's SPI write timeout. Calling multiple small ErasePayload is recommended.
         */
        virtual void initiatePayload(
            ) = 0;

        /** @brief Implementation for GetInitiatePayloadStatus
         *  Get the status of the payload initiation process.
         *
         *  @return status[FirmwareUpdateStatus] - Status of the payload initiation
         */
        virtual FirmwareUpdateStatus getInitiatePayloadStatus(
            ) = 0;

        /** @brief Implementation for ErasePayload
         *  Erases the given size starting at the specified offset of the staging partition.
         *
         *  @param[in] offset - Offset of the staging partition to start erasing from.
         *  @param[in] size - Size of the staging partition to erase.
         */
        virtual void erasePayload(
            uint32_t offset,
            uint32_t size) = 0;

        /** @brief Implementation for SendPayload
         *  Chunk and send the binary specified in the image path
         *
         *  @param[in] imagePath - Firmware image path
         */
        virtual void sendPayload(
            std::string imagePath) = 0;

        /** @brief Implementation for GetSendPayloadStatus
         *  Get the status of the send payload process.
         *
         *  @return status[FirmwareUpdateStatus] - Status of the send payload process.
         */
        virtual FirmwareUpdateStatus getSendPayloadStatus(
            ) = 0;

        /** @brief Implementation for VerifyPayload
         *  Initiates the verification process without activating the staging area
         */
        virtual void verifyPayload(
            ) = 0;

        /** @brief Implementation for GetVerifyPayloadStatus
         *  Get the status of the payload verification process.
         *
         *  @return status[FirmwareUpdateStatus] - Status of the payload verification
         */
        virtual FirmwareUpdateStatus getVerifyPayloadStatus(
            ) = 0;

        /** @brief Implementation for ActivatePayload
         *  Activates the staging area as persistent or non-persistent for next boot if verification was successful.
         *
         *  @param[in] makePersistent - Flag to determine whether to activate the staged image as persistent or non-persistent for next boot.
         */
        virtual void activatePayload(
            bool makePersistent) = 0;

        /** @brief Implementation for GetPayloadSize
         *  Determines the max size of the payload region.
         *
         *  @return size[uint32_t] - The size of the payload region
         */
        virtual uint32_t getPayloadSize(
            ) = 0;

        /** @brief Implementation for Confirm
         *  Prevents hoth from rolling back and using the previous image. When an image can be comfirmed to be working well, this command is given, which disarms the hoth watchdog.
         */
        virtual void confirm(
            ) = 0;

        /** @brief Implementation for GetTotalBootTime
         *  Get total time spending from reset to HVNGOOD.
         *
         *  @return time[uint32_t] - Time in microseconds.
         */
        virtual uint32_t getTotalBootTime(
            ) = 0;

        /** @brief Implementation for GetFirmwareUpdateTime
         *  Get time spending in the self update routine. Since a proper self update involves a reset, this time is always expected to be low.
         *
         *  @return time[uint32_t] - Time in microseconds.
         */
        virtual uint32_t getFirmwareUpdateTime(
            ) = 0;

        /** @brief Implementation for GetFirmwareMirroringTime
         *  Get time spending in mirroing the self-update. This time is a reasonable proxy for the total self update time.
         *
         *  @return time[uint32_t] - Time in microseconds.
         */
        virtual uint32_t getFirmwareMirroringTime(
            ) = 0;

        /** @brief Implementation for GetPayloadValidationTime
         *  Get time spending in validating the payload, copying mutable regions and/or dealing with failsafe fallback.
         *
         *  @return time[uint32_t] - Time in microseconds.
         */
        virtual uint32_t getPayloadValidationTime(
            ) = 0;


        /** @brief Send signal 'HostCommandResponseReady'
         *
         *  This signal is broadcast when a response is ready for the call that returned CallToken.
         *
         *  @param[in] callToken - The token returned from SendHostCommandAsync()
         */
        void hostCommandResponseReady(
            uint64_t callToken);


        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.google.gbmc.Hoth.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static FirmwareUpdateStatus convertFirmwareUpdateStatusFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.google.gbmc.Hoth.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<FirmwareUpdateStatus> convertStringToFirmwareUpdateStatus(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "com.google.gbmc.Hoth.<value name>"
         */
        static std::string convertFirmwareUpdateStatusToString(FirmwareUpdateStatus e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _com_google_gbmc_Hoth_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_google_gbmc_Hoth_interface.emit_removed();
        }

        static constexpr auto interface = "com.google.gbmc.Hoth";

    private:

        /** @brief sd-bus callback for SendHostCommand
         */
        static int _callback_SendHostCommand(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SendTrustedHostCommand
         */
        static int _callback_SendTrustedHostCommand(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SendHostCommandAsync
         */
        static int _callback_SendHostCommandAsync(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetHostCommandResponse
         */
        static int _callback_GetHostCommandResponse(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for UpdateFirmware
         */
        static int _callback_UpdateFirmware(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetFirmwareUpdateStatus
         */
        static int _callback_GetFirmwareUpdateStatus(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for InitiatePayload
         */
        static int _callback_InitiatePayload(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetInitiatePayloadStatus
         */
        static int _callback_GetInitiatePayloadStatus(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for ErasePayload
         */
        static int _callback_ErasePayload(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SendPayload
         */
        static int _callback_SendPayload(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetSendPayloadStatus
         */
        static int _callback_GetSendPayloadStatus(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for VerifyPayload
         */
        static int _callback_VerifyPayload(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetVerifyPayloadStatus
         */
        static int _callback_GetVerifyPayloadStatus(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for ActivatePayload
         */
        static int _callback_ActivatePayload(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetPayloadSize
         */
        static int _callback_GetPayloadSize(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for Confirm
         */
        static int _callback_Confirm(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetTotalBootTime
         */
        static int _callback_GetTotalBootTime(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetFirmwareUpdateTime
         */
        static int _callback_GetFirmwareUpdateTime(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetFirmwareMirroringTime
         */
        static int _callback_GetFirmwareMirroringTime(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetPayloadValidationTime
         */
        static int _callback_GetPayloadValidationTime(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_google_gbmc_Hoth_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Hoth::FirmwareUpdateStatus.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Hoth::FirmwareUpdateStatus e)
{
    return Hoth::convertFirmwareUpdateStatusToString(e);
}

} // namespace server
} // namespace gbmc
} // namespace google
} // namespace com

namespace message::details
{
template <>
struct convert_from_string<com::google::gbmc::server::Hoth::FirmwareUpdateStatus>
{
    static auto op(const std::string& value) noexcept
    {
        return com::google::gbmc::server::Hoth::convertStringToFirmwareUpdateStatus(value);
    }
};

template <>
struct convert_to_string<com::google::gbmc::server::Hoth::FirmwareUpdateStatus>
{
    static std::string op(com::google::gbmc::server::Hoth::FirmwareUpdateStatus value)
    {
        return com::google::gbmc::server::Hoth::convertFirmwareUpdateStatusToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

