#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Power/Cap/server.hpp>






namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

Cap::Cap(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Power_Cap_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Cap::Cap(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Cap(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Cap::powerCap() const ->
        uint32_t
{
    return _powerCap;
}

int Cap::_callback_get_PowerCap(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerCap();
                    }
                ));
    }
}

auto Cap::powerCap(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_powerCap != value)
    {
        _powerCap = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.property_changed("PowerCap");
        }
    }

    return _powerCap;
}

auto Cap::powerCap(uint32_t val) ->
        uint32_t
{
    return powerCap(val, false);
}

int Cap::_callback_set_PowerCap(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->powerCap(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cap
{
static const auto _property_PowerCap =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Cap::powerCapEnable() const ->
        bool
{
    return _powerCapEnable;
}

int Cap::_callback_get_PowerCapEnable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerCapEnable();
                    }
                ));
    }
}

auto Cap::powerCapEnable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_powerCapEnable != value)
    {
        _powerCapEnable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.property_changed("PowerCapEnable");
        }
    }

    return _powerCapEnable;
}

auto Cap::powerCapEnable(bool val) ->
        bool
{
    return powerCapEnable(val, false);
}

int Cap::_callback_set_PowerCapEnable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->powerCapEnable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cap
{
static const auto _property_PowerCapEnable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Cap::minPowerCapValue() const ->
        uint32_t
{
    return _minPowerCapValue;
}

int Cap::_callback_get_MinPowerCapValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minPowerCapValue();
                    }
                ));
    }
}

auto Cap::minPowerCapValue(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_minPowerCapValue != value)
    {
        _minPowerCapValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.property_changed("MinPowerCapValue");
        }
    }

    return _minPowerCapValue;
}

auto Cap::minPowerCapValue(uint32_t val) ->
        uint32_t
{
    return minPowerCapValue(val, false);
}

int Cap::_callback_set_MinPowerCapValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->minPowerCapValue(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cap
{
static const auto _property_MinPowerCapValue =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Cap::maxPowerCapValue() const ->
        uint32_t
{
    return _maxPowerCapValue;
}

int Cap::_callback_get_MaxPowerCapValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxPowerCapValue();
                    }
                ));
    }
}

auto Cap::maxPowerCapValue(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_maxPowerCapValue != value)
    {
        _maxPowerCapValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.property_changed("MaxPowerCapValue");
        }
    }

    return _maxPowerCapValue;
}

auto Cap::maxPowerCapValue(uint32_t val) ->
        uint32_t
{
    return maxPowerCapValue(val, false);
}

int Cap::_callback_set_MaxPowerCapValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->maxPowerCapValue(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cap
{
static const auto _property_MaxPowerCapValue =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Cap::minSoftPowerCapValue() const ->
        uint32_t
{
    return _minSoftPowerCapValue;
}

int Cap::_callback_get_MinSoftPowerCapValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minSoftPowerCapValue();
                    }
                ));
    }
}

auto Cap::minSoftPowerCapValue(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_minSoftPowerCapValue != value)
    {
        _minSoftPowerCapValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.property_changed("MinSoftPowerCapValue");
        }
    }

    return _minSoftPowerCapValue;
}

auto Cap::minSoftPowerCapValue(uint32_t val) ->
        uint32_t
{
    return minSoftPowerCapValue(val, false);
}

int Cap::_callback_set_MinSoftPowerCapValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cap*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->minSoftPowerCapValue(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cap
{
static const auto _property_MinSoftPowerCapValue =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void Cap::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PowerCap")
    {
        auto& v = std::get<uint32_t>(val);
        powerCap(v, skipSignal);
        return;
    }
    if (_name == "PowerCapEnable")
    {
        auto& v = std::get<bool>(val);
        powerCapEnable(v, skipSignal);
        return;
    }
    if (_name == "MinPowerCapValue")
    {
        auto& v = std::get<uint32_t>(val);
        minPowerCapValue(v, skipSignal);
        return;
    }
    if (_name == "MaxPowerCapValue")
    {
        auto& v = std::get<uint32_t>(val);
        maxPowerCapValue(v, skipSignal);
        return;
    }
    if (_name == "MinSoftPowerCapValue")
    {
        auto& v = std::get<uint32_t>(val);
        minSoftPowerCapValue(v, skipSignal);
        return;
    }
}

auto Cap::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PowerCap")
    {
        return powerCap();
    }
    if (_name == "PowerCapEnable")
    {
        return powerCapEnable();
    }
    if (_name == "MinPowerCapValue")
    {
        return minPowerCapValue();
    }
    if (_name == "MaxPowerCapValue")
    {
        return maxPowerCapValue();
    }
    if (_name == "MinSoftPowerCapValue")
    {
        return minSoftPowerCapValue();
    }

    return PropertiesVariant();
}


const vtable_t Cap::_vtable[] = {
    vtable::start(),
    vtable::property("PowerCap",
                     details::Cap::_property_PowerCap
                        .data(),
                     _callback_get_PowerCap,
                     _callback_set_PowerCap,
                     vtable::property_::emits_change),
    vtable::property("PowerCapEnable",
                     details::Cap::_property_PowerCapEnable
                        .data(),
                     _callback_get_PowerCapEnable,
                     _callback_set_PowerCapEnable,
                     vtable::property_::emits_change),
    vtable::property("MinPowerCapValue",
                     details::Cap::_property_MinPowerCapValue
                        .data(),
                     _callback_get_MinPowerCapValue,
                     _callback_set_MinPowerCapValue,
                     vtable::property_::emits_change),
    vtable::property("MaxPowerCapValue",
                     details::Cap::_property_MaxPowerCapValue
                        .data(),
                     _callback_get_MaxPowerCapValue,
                     _callback_set_MaxPowerCapValue,
                     vtable::property_::emits_change),
    vtable::property("MinSoftPowerCapValue",
                     details::Cap::_property_MinSoftPowerCapValue
                        .data(),
                     _callback_get_MinSoftPowerCapValue,
                     _callback_set_MinSoftPowerCapValue,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

