#include <com/ibm/VPD/error.hpp>

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace VPD
{
namespace Error
{
const char* LocationNotFound::name() const noexcept
{
    return errName;
}
const char* LocationNotFound::description() const noexcept
{
    return errDesc;
}
const char* LocationNotFound::what() const noexcept
{
    return errWhat;
}
const char* NodeNotFound::name() const noexcept
{
    return errName;
}
const char* NodeNotFound::description() const noexcept
{
    return errDesc;
}
const char* NodeNotFound::what() const noexcept
{
    return errWhat;
}
const char* PathNotFound::name() const noexcept
{
    return errName;
}
const char* PathNotFound::description() const noexcept
{
    return errDesc;
}
const char* PathNotFound::what() const noexcept
{
    return errWhat;
}
const char* RecordNotFound::name() const noexcept
{
    return errName;
}
const char* RecordNotFound::description() const noexcept
{
    return errDesc;
}
const char* RecordNotFound::what() const noexcept
{
    return errWhat;
}
const char* KeywordNotFound::name() const noexcept
{
    return errName;
}
const char* KeywordNotFound::description() const noexcept
{
    return errDesc;
}
const char* KeywordNotFound::what() const noexcept
{
    return errWhat;
}
const char* BlankSystemVPD::name() const noexcept
{
    return errName;
}
const char* BlankSystemVPD::description() const noexcept
{
    return errDesc;
}
const char* BlankSystemVPD::what() const noexcept
{
    return errWhat;
}
const char* InvalidEepromPath::name() const noexcept
{
    return errName;
}
const char* InvalidEepromPath::description() const noexcept
{
    return errDesc;
}
const char* InvalidEepromPath::what() const noexcept
{
    return errWhat;
}
const char* InvalidVPD::name() const noexcept
{
    return errName;
}
const char* InvalidVPD::description() const noexcept
{
    return errDesc;
}
const char* InvalidVPD::what() const noexcept
{
    return errWhat;
}
const char* EccCheckFailed::name() const noexcept
{
    return errName;
}
const char* EccCheckFailed::description() const noexcept
{
    return errDesc;
}
const char* EccCheckFailed::what() const noexcept
{
    return errWhat;
}
const char* InvalidJson::name() const noexcept
{
    return errName;
}
const char* InvalidJson::description() const noexcept
{
    return errDesc;
}
const char* InvalidJson::what() const noexcept
{
    return errWhat;
}
const char* DbusFailure::name() const noexcept
{
    return errName;
}
const char* DbusFailure::description() const noexcept
{
    return errDesc;
}
const char* DbusFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace VPD
} // namespace ibm
} // namespace com
} // namespace sdbusplus

