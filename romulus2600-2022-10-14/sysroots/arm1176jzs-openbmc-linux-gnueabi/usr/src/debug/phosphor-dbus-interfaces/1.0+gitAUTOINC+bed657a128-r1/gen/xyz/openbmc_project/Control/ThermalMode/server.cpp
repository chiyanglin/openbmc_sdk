#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/ThermalMode/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

ThermalMode::ThermalMode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_ThermalMode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ThermalMode::ThermalMode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ThermalMode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ThermalMode::supported() const ->
        std::vector<std::string>
{
    return _supported;
}

int ThermalMode::_callback_get_Supported(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ThermalMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->supported();
                    }
                ));
    }
}

auto ThermalMode::supported(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_supported != value)
    {
        _supported = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ThermalMode_interface.property_changed("Supported");
        }
    }

    return _supported;
}

auto ThermalMode::supported(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return supported(val, false);
}


namespace details
{
namespace ThermalMode
{
static const auto _property_Supported =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto ThermalMode::current() const ->
        std::string
{
    return _current;
}

int ThermalMode::_callback_get_Current(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ThermalMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->current();
                    }
                ));
    }
}

auto ThermalMode::current(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_current != value)
    {
        _current = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ThermalMode_interface.property_changed("Current");
        }
    }

    return _current;
}

auto ThermalMode::current(std::string val) ->
        std::string
{
    return current(val, false);
}

int ThermalMode::_callback_set_Current(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ThermalMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->current(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ThermalMode
{
static const auto _property_Current =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void ThermalMode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Supported")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        supported(v, skipSignal);
        return;
    }
    if (_name == "Current")
    {
        auto& v = std::get<std::string>(val);
        current(v, skipSignal);
        return;
    }
}

auto ThermalMode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Supported")
    {
        return supported();
    }
    if (_name == "Current")
    {
        return current();
    }

    return PropertiesVariant();
}


const vtable_t ThermalMode::_vtable[] = {
    vtable::start(),
    vtable::property("Supported",
                     details::ThermalMode::_property_Supported
                        .data(),
                     _callback_get_Supported,
                     vtable::property_::emits_change),
    vtable::property("Current",
                     details::ThermalMode::_property_Current
                        .data(),
                     _callback_get_Current,
                     _callback_set_Current,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

