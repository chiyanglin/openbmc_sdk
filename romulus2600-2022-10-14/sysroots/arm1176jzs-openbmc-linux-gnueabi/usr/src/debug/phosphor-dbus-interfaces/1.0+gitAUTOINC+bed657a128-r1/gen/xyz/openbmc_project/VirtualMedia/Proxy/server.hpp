#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

class Proxy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Proxy() = delete;
        Proxy(const Proxy&) = delete;
        Proxy& operator=(const Proxy&) = delete;
        Proxy(Proxy&&) = delete;
        Proxy& operator=(Proxy&&) = delete;
        virtual ~Proxy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Proxy(bus_t& bus, const char* path);



        /** @brief Implementation for Mount
         *  Perform an asynchronous operation of mounting to HOST on given object.
         *
         *  @return status[bool] - mounting status. True on success.
         */
        virtual bool mount(
            ) = 0;

        /** @brief Implementation for Unmount
         *  Perform an asynchronous operation of unmount from HOST on given object.
         *
         *  @return status[bool] - the unmount status. True on success.
         */
        virtual bool unmount(
            ) = 0;


        /** @brief Send signal 'Completion'
         *
         *  Signal indicating completion of mount or unmount action.
         *
         *  @param[in] result - Returns 0 for success or errno on failure after background operation completes.
         */
        void completion(
            int32_t result);



        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_VirtualMedia_Proxy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_VirtualMedia_Proxy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Proxy";

    private:

        /** @brief sd-bus callback for Mount
         */
        static int _callback_Mount(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for Unmount
         */
        static int _callback_Unmount(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_VirtualMedia_Proxy_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

