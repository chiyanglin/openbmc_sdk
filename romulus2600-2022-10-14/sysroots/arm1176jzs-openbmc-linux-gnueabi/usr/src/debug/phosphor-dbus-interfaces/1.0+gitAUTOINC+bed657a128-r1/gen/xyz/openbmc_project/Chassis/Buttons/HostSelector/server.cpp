#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Buttons/HostSelector/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace server
{

HostSelector::HostSelector(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Buttons_HostSelector_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

HostSelector::HostSelector(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : HostSelector(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto HostSelector::position() const ->
        size_t
{
    return _position;
}

int HostSelector::_callback_get_Position(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HostSelector*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->position();
                    }
                ));
    }
}

auto HostSelector::position(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_position != value)
    {
        _position = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Buttons_HostSelector_interface.property_changed("Position");
        }
    }

    return _position;
}

auto HostSelector::position(size_t val) ->
        size_t
{
    return position(val, false);
}

int HostSelector::_callback_set_Position(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HostSelector*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->position(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace HostSelector
{
static const auto _property_Position =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto HostSelector::maxPosition() const ->
        size_t
{
    return _maxPosition;
}

int HostSelector::_callback_get_MaxPosition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HostSelector*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxPosition();
                    }
                ));
    }
}

auto HostSelector::maxPosition(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_maxPosition != value)
    {
        _maxPosition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Buttons_HostSelector_interface.property_changed("MaxPosition");
        }
    }

    return _maxPosition;
}

auto HostSelector::maxPosition(size_t val) ->
        size_t
{
    return maxPosition(val, false);
}


namespace details
{
namespace HostSelector
{
static const auto _property_MaxPosition =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

void HostSelector::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Position")
    {
        auto& v = std::get<size_t>(val);
        position(v, skipSignal);
        return;
    }
    if (_name == "MaxPosition")
    {
        auto& v = std::get<size_t>(val);
        maxPosition(v, skipSignal);
        return;
    }
}

auto HostSelector::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Position")
    {
        return position();
    }
    if (_name == "MaxPosition")
    {
        return maxPosition();
    }

    return PropertiesVariant();
}


const vtable_t HostSelector::_vtable[] = {
    vtable::start(),
    vtable::property("Position",
                     details::HostSelector::_property_Position
                        .data(),
                     _callback_get_Position,
                     _callback_set_Position,
                     vtable::property_::emits_change),
    vtable::property("MaxPosition",
                     details::HostSelector::_property_MaxPosition
                        .data(),
                     _callback_get_MaxPosition,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

