#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/ObjectMapper/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace server
{

ObjectMapper::ObjectMapper(bus_t& bus, const char* path)
        : _xyz_openbmc_project_ObjectMapper_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int ObjectMapper::_callback_GetObject(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<ObjectMapper*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& path, std::vector<std::string>&& interfaces)
                    {
                        return o->getObject(
                                path, interfaces);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace ObjectMapper
{
static const auto _param_GetObject =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path, std::vector<std::string>>());
static const auto _return_GetObject =
        utility::tuple_to_array(message::types::type_id<
                std::map<std::string, std::vector<std::string>>>());
}
}

int ObjectMapper::_callback_GetAncestors(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<ObjectMapper*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& path, std::vector<std::string>&& interfaces)
                    {
                        return o->getAncestors(
                                path, interfaces);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace ObjectMapper
{
static const auto _param_GetAncestors =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path, std::vector<std::string>>());
static const auto _return_GetAncestors =
        utility::tuple_to_array(message::types::type_id<
                std::map<sdbusplus::message::object_path, std::map<std::string, std::vector<std::string>>>>());
}
}

int ObjectMapper::_callback_GetSubTree(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<ObjectMapper*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& subtree, int32_t&& depth, std::vector<std::string>&& interfaces)
                    {
                        return o->getSubTree(
                                subtree, depth, interfaces);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace ObjectMapper
{
static const auto _param_GetSubTree =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path, int32_t, std::vector<std::string>>());
static const auto _return_GetSubTree =
        utility::tuple_to_array(message::types::type_id<
                std::map<sdbusplus::message::object_path, std::map<std::string, std::vector<std::string>>>>());
}
}

int ObjectMapper::_callback_GetSubTreePaths(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<ObjectMapper*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& subtree, int32_t&& depth, std::vector<std::string>&& interfaces)
                    {
                        return o->getSubTreePaths(
                                subtree, depth, interfaces);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace ObjectMapper
{
static const auto _param_GetSubTreePaths =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path, int32_t, std::vector<std::string>>());
static const auto _return_GetSubTreePaths =
        utility::tuple_to_array(message::types::type_id<
                std::vector<sdbusplus::message::object_path>>());
}
}




const vtable_t ObjectMapper::_vtable[] = {
    vtable::start(),

    vtable::method("GetObject",
                   details::ObjectMapper::_param_GetObject
                        .data(),
                   details::ObjectMapper::_return_GetObject
                        .data(),
                   _callback_GetObject),

    vtable::method("GetAncestors",
                   details::ObjectMapper::_param_GetAncestors
                        .data(),
                   details::ObjectMapper::_return_GetAncestors
                        .data(),
                   _callback_GetAncestors),

    vtable::method("GetSubTree",
                   details::ObjectMapper::_param_GetSubTree
                        .data(),
                   details::ObjectMapper::_return_GetSubTree
                        .data(),
                   _callback_GetSubTree),

    vtable::method("GetSubTreePaths",
                   details::ObjectMapper::_param_GetSubTreePaths
                        .data(),
                   details::ObjectMapper::_return_GetSubTreePaths
                        .data(),
                   _callback_GetSubTreePaths),
    vtable::end()
};

} // namespace server
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

