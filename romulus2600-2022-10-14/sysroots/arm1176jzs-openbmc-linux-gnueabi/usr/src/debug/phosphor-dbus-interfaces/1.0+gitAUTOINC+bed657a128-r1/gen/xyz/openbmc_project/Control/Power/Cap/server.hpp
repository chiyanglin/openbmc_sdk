#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

class Cap
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Cap() = delete;
        Cap(const Cap&) = delete;
        Cap& operator=(const Cap&) = delete;
        Cap(Cap&&) = delete;
        Cap& operator=(Cap&&) = delete;
        virtual ~Cap() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Cap(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Cap(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PowerCap */
        virtual uint32_t powerCap() const;
        /** Set value of PowerCap with option to skip sending signal */
        virtual uint32_t powerCap(uint32_t value,
               bool skipSignal);
        /** Set value of PowerCap */
        virtual uint32_t powerCap(uint32_t value);
        /** Get value of PowerCapEnable */
        virtual bool powerCapEnable() const;
        /** Set value of PowerCapEnable with option to skip sending signal */
        virtual bool powerCapEnable(bool value,
               bool skipSignal);
        /** Set value of PowerCapEnable */
        virtual bool powerCapEnable(bool value);
        /** Get value of MinPowerCapValue */
        virtual uint32_t minPowerCapValue() const;
        /** Set value of MinPowerCapValue with option to skip sending signal */
        virtual uint32_t minPowerCapValue(uint32_t value,
               bool skipSignal);
        /** Set value of MinPowerCapValue */
        virtual uint32_t minPowerCapValue(uint32_t value);
        /** Get value of MaxPowerCapValue */
        virtual uint32_t maxPowerCapValue() const;
        /** Set value of MaxPowerCapValue with option to skip sending signal */
        virtual uint32_t maxPowerCapValue(uint32_t value,
               bool skipSignal);
        /** Set value of MaxPowerCapValue */
        virtual uint32_t maxPowerCapValue(uint32_t value);
        /** Get value of MinSoftPowerCapValue */
        virtual uint32_t minSoftPowerCapValue() const;
        /** Set value of MinSoftPowerCapValue with option to skip sending signal */
        virtual uint32_t minSoftPowerCapValue(uint32_t value,
               bool skipSignal);
        /** Set value of MinSoftPowerCapValue */
        virtual uint32_t minSoftPowerCapValue(uint32_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Power_Cap_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Power.Cap";

    private:

        /** @brief sd-bus callback for get-property 'PowerCap' */
        static int _callback_get_PowerCap(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerCap' */
        static int _callback_set_PowerCap(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PowerCapEnable' */
        static int _callback_get_PowerCapEnable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerCapEnable' */
        static int _callback_set_PowerCapEnable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MinPowerCapValue' */
        static int _callback_get_MinPowerCapValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MinPowerCapValue' */
        static int _callback_set_MinPowerCapValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxPowerCapValue' */
        static int _callback_get_MaxPowerCapValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxPowerCapValue' */
        static int _callback_set_MaxPowerCapValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MinSoftPowerCapValue' */
        static int _callback_get_MinSoftPowerCapValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MinSoftPowerCapValue' */
        static int _callback_set_MinSoftPowerCapValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Power_Cap_interface;
        sdbusplus::SdBusInterface *_intf;

        uint32_t _powerCap{};
        bool _powerCapEnable{};
        uint32_t _minPowerCapValue = 0;
        uint32_t _maxPowerCapValue = std::numeric_limits<uint32_t>::max();
        uint32_t _minSoftPowerCapValue = 0;

};


} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

