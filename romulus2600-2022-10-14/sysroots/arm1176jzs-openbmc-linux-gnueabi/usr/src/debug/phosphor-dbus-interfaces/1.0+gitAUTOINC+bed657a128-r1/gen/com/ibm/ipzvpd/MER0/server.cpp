#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/MER0/server.hpp>



namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

MER0::MER0(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_MER0_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MER0::MER0(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MER0(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MER0::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int MER0::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MER0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto MER0::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_MER0_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto MER0::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int MER0::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MER0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MER0
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto MER0::pdi() const ->
        std::vector<uint8_t>
{
    return _pdi;
}

int MER0::_callback_get_PD_I(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MER0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdi();
                    }
                ));
    }
}

auto MER0::pdi(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdi != value)
    {
        _pdi = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_MER0_interface.property_changed("PD_I");
        }
    }

    return _pdi;
}

auto MER0::pdi(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdi(val, false);
}

int MER0::_callback_set_PD_I(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MER0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdi(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MER0
{
static const auto _property_PD_I =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void MER0::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "PD_I")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdi(v, skipSignal);
        return;
    }
}

auto MER0::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "PD_I")
    {
        return pdi();
    }

    return PropertiesVariant();
}


const vtable_t MER0::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::MER0::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("PD_I",
                     details::MER0::_property_PD_I
                        .data(),
                     _callback_get_PD_I,
                     _callback_set_PD_I,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

