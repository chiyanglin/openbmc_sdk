#include <xyz/openbmc_project/Nvme/Status/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Nvme
{
namespace Status
{
namespace Error
{
const char* CapacityFault::name() const noexcept
{
    return errName;
}
const char* CapacityFault::description() const noexcept
{
    return errDesc;
}
const char* CapacityFault::what() const noexcept
{
    return errWhat;
}
const char* TemperatureFault::name() const noexcept
{
    return errName;
}
const char* TemperatureFault::description() const noexcept
{
    return errDesc;
}
const char* TemperatureFault::what() const noexcept
{
    return errWhat;
}
const char* DegradesFault::name() const noexcept
{
    return errName;
}
const char* DegradesFault::description() const noexcept
{
    return errDesc;
}
const char* DegradesFault::what() const noexcept
{
    return errWhat;
}
const char* MediaFault::name() const noexcept
{
    return errName;
}
const char* MediaFault::description() const noexcept
{
    return errDesc;
}
const char* MediaFault::what() const noexcept
{
    return errWhat;
}
const char* BackupDeviceFault::name() const noexcept
{
    return errName;
}
const char* BackupDeviceFault::description() const noexcept
{
    return errDesc;
}
const char* BackupDeviceFault::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Status
} // namespace Nvme
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

