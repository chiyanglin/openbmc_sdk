#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace Error
{

struct Timeout final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.Timeout";
    static constexpr auto errDesc =
            "Operation timed out.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.Timeout: Operation timed out.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InternalFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.InternalFailure";
    static constexpr auto errDesc =
            "The operation failed internally.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.InternalFailure: The operation failed internally.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InvalidArgument final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.InvalidArgument";
    static constexpr auto errDesc =
            "Invalid argument was given.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.InvalidArgument: Invalid argument was given.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InsufficientPermission final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.InsufficientPermission";
    static constexpr auto errDesc =
            "Insufficient permission to perform operation";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.InsufficientPermission: Insufficient permission to perform operation";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct NotAllowed final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.NotAllowed";
    static constexpr auto errDesc =
            "The operation is not allowed";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.NotAllowed: The operation is not allowed";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct NoCACertificate final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.NoCACertificate";
    static constexpr auto errDesc =
            "Server's CA certificate has not been provided.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.NoCACertificate: Server's CA certificate has not been provided.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct TooManyResources final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.TooManyResources";
    static constexpr auto errDesc =
            "Too many resources have already been granted.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.TooManyResources: Too many resources have already been granted.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct ResourceNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.ResourceNotFound";
    static constexpr auto errDesc =
            "The resource is not found.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.ResourceNotFound: The resource is not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct Unavailable final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.Unavailable";
    static constexpr auto errDesc =
            "The service is temporarily unavailable.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.Unavailable: The service is temporarily unavailable.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct UnsupportedRequest final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Error.UnsupportedRequest";
    static constexpr auto errDesc =
            "The request is unsupported.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Error.UnsupportedRequest: The request is unsupported.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

