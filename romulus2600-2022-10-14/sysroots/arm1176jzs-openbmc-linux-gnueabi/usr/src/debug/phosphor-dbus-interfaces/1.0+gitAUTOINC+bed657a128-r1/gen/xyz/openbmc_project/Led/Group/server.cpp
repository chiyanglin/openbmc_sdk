#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Led/Group/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Led
{
namespace server
{

Group::Group(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Led_Group_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Group::Group(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Group(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Group::asserted() const ->
        bool
{
    return _asserted;
}

int Group::_callback_get_Asserted(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Group*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->asserted();
                    }
                ));
    }
}

auto Group::asserted(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_asserted != value)
    {
        _asserted = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Led_Group_interface.property_changed("Asserted");
        }
    }

    return _asserted;
}

auto Group::asserted(bool val) ->
        bool
{
    return asserted(val, false);
}

int Group::_callback_set_Asserted(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Group*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->asserted(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Group
{
static const auto _property_Asserted =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Group::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Asserted")
    {
        auto& v = std::get<bool>(val);
        asserted(v, skipSignal);
        return;
    }
}

auto Group::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Asserted")
    {
        return asserted();
    }

    return PropertiesVariant();
}


const vtable_t Group::_vtable[] = {
    vtable::start(),
    vtable::property("Asserted",
                     details::Group::_property_Asserted
                        .data(),
                     _callback_get_Asserted,
                     _callback_set_Asserted,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Led
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

