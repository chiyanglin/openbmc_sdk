#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/AccountPolicy/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

AccountPolicy::AccountPolicy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_AccountPolicy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

AccountPolicy::AccountPolicy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : AccountPolicy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto AccountPolicy::maxLoginAttemptBeforeLockout() const ->
        uint16_t
{
    return _maxLoginAttemptBeforeLockout;
}

int AccountPolicy::_callback_get_MaxLoginAttemptBeforeLockout(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxLoginAttemptBeforeLockout();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto AccountPolicy::maxLoginAttemptBeforeLockout(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_maxLoginAttemptBeforeLockout != value)
    {
        _maxLoginAttemptBeforeLockout = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_AccountPolicy_interface.property_changed("MaxLoginAttemptBeforeLockout");
        }
    }

    return _maxLoginAttemptBeforeLockout;
}

auto AccountPolicy::maxLoginAttemptBeforeLockout(uint16_t val) ->
        uint16_t
{
    return maxLoginAttemptBeforeLockout(val, false);
}

int AccountPolicy::_callback_set_MaxLoginAttemptBeforeLockout(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->maxLoginAttemptBeforeLockout(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace AccountPolicy
{
static const auto _property_MaxLoginAttemptBeforeLockout =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto AccountPolicy::accountUnlockTimeout() const ->
        uint32_t
{
    return _accountUnlockTimeout;
}

int AccountPolicy::_callback_get_AccountUnlockTimeout(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->accountUnlockTimeout();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto AccountPolicy::accountUnlockTimeout(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_accountUnlockTimeout != value)
    {
        _accountUnlockTimeout = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_AccountPolicy_interface.property_changed("AccountUnlockTimeout");
        }
    }

    return _accountUnlockTimeout;
}

auto AccountPolicy::accountUnlockTimeout(uint32_t val) ->
        uint32_t
{
    return accountUnlockTimeout(val, false);
}

int AccountPolicy::_callback_set_AccountUnlockTimeout(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->accountUnlockTimeout(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace AccountPolicy
{
static const auto _property_AccountUnlockTimeout =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto AccountPolicy::minPasswordLength() const ->
        uint8_t
{
    return _minPasswordLength;
}

int AccountPolicy::_callback_get_MinPasswordLength(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minPasswordLength();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto AccountPolicy::minPasswordLength(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_minPasswordLength != value)
    {
        _minPasswordLength = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_AccountPolicy_interface.property_changed("MinPasswordLength");
        }
    }

    return _minPasswordLength;
}

auto AccountPolicy::minPasswordLength(uint8_t val) ->
        uint8_t
{
    return minPasswordLength(val, false);
}

int AccountPolicy::_callback_set_MinPasswordLength(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->minPasswordLength(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace AccountPolicy
{
static const auto _property_MinPasswordLength =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto AccountPolicy::rememberOldPasswordTimes() const ->
        uint8_t
{
    return _rememberOldPasswordTimes;
}

int AccountPolicy::_callback_get_RememberOldPasswordTimes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rememberOldPasswordTimes();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto AccountPolicy::rememberOldPasswordTimes(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_rememberOldPasswordTimes != value)
    {
        _rememberOldPasswordTimes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_AccountPolicy_interface.property_changed("RememberOldPasswordTimes");
        }
    }

    return _rememberOldPasswordTimes;
}

auto AccountPolicy::rememberOldPasswordTimes(uint8_t val) ->
        uint8_t
{
    return rememberOldPasswordTimes(val, false);
}

int AccountPolicy::_callback_set_RememberOldPasswordTimes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AccountPolicy*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->rememberOldPasswordTimes(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace AccountPolicy
{
static const auto _property_RememberOldPasswordTimes =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void AccountPolicy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MaxLoginAttemptBeforeLockout")
    {
        auto& v = std::get<uint16_t>(val);
        maxLoginAttemptBeforeLockout(v, skipSignal);
        return;
    }
    if (_name == "AccountUnlockTimeout")
    {
        auto& v = std::get<uint32_t>(val);
        accountUnlockTimeout(v, skipSignal);
        return;
    }
    if (_name == "MinPasswordLength")
    {
        auto& v = std::get<uint8_t>(val);
        minPasswordLength(v, skipSignal);
        return;
    }
    if (_name == "RememberOldPasswordTimes")
    {
        auto& v = std::get<uint8_t>(val);
        rememberOldPasswordTimes(v, skipSignal);
        return;
    }
}

auto AccountPolicy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MaxLoginAttemptBeforeLockout")
    {
        return maxLoginAttemptBeforeLockout();
    }
    if (_name == "AccountUnlockTimeout")
    {
        return accountUnlockTimeout();
    }
    if (_name == "MinPasswordLength")
    {
        return minPasswordLength();
    }
    if (_name == "RememberOldPasswordTimes")
    {
        return rememberOldPasswordTimes();
    }

    return PropertiesVariant();
}


const vtable_t AccountPolicy::_vtable[] = {
    vtable::start(),
    vtable::property("MaxLoginAttemptBeforeLockout",
                     details::AccountPolicy::_property_MaxLoginAttemptBeforeLockout
                        .data(),
                     _callback_get_MaxLoginAttemptBeforeLockout,
                     _callback_set_MaxLoginAttemptBeforeLockout,
                     vtable::property_::emits_change),
    vtable::property("AccountUnlockTimeout",
                     details::AccountPolicy::_property_AccountUnlockTimeout
                        .data(),
                     _callback_get_AccountUnlockTimeout,
                     _callback_set_AccountUnlockTimeout,
                     vtable::property_::emits_change),
    vtable::property("MinPasswordLength",
                     details::AccountPolicy::_property_MinPasswordLength
                        .data(),
                     _callback_get_MinPasswordLength,
                     _callback_set_MinPasswordLength,
                     vtable::property_::emits_change),
    vtable::property("RememberOldPasswordTimes",
                     details::AccountPolicy::_property_RememberOldPasswordTimes
                        .data(),
                     _callback_get_RememberOldPasswordTimes,
                     _callback_set_RememberOldPasswordTimes,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

