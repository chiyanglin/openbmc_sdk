#include <xyz/openbmc_project/User/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Common
{
namespace Error
{
const char* UserNameExists::name() const noexcept
{
    return errName;
}
const char* UserNameExists::description() const noexcept
{
    return errDesc;
}
const char* UserNameExists::what() const noexcept
{
    return errWhat;
}
const char* UserNameDoesNotExist::name() const noexcept
{
    return errName;
}
const char* UserNameDoesNotExist::description() const noexcept
{
    return errDesc;
}
const char* UserNameDoesNotExist::what() const noexcept
{
    return errWhat;
}
const char* UserNameGroupFail::name() const noexcept
{
    return errName;
}
const char* UserNameGroupFail::description() const noexcept
{
    return errDesc;
}
const char* UserNameGroupFail::what() const noexcept
{
    return errWhat;
}
const char* UserNamePrivFail::name() const noexcept
{
    return errName;
}
const char* UserNamePrivFail::description() const noexcept
{
    return errDesc;
}
const char* UserNamePrivFail::what() const noexcept
{
    return errWhat;
}
const char* NoResource::name() const noexcept
{
    return errName;
}
const char* NoResource::description() const noexcept
{
    return errDesc;
}
const char* NoResource::what() const noexcept
{
    return errWhat;
}
const char* PrivilegeMappingExists::name() const noexcept
{
    return errName;
}
const char* PrivilegeMappingExists::description() const noexcept
{
    return errDesc;
}
const char* PrivilegeMappingExists::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Common
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

