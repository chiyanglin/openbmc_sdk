#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace Error
{

struct IsolatedAlready final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.HardwareIsolation.Error.IsolatedAlready";
    static constexpr auto errDesc =
            "The requested hardware is already isolated.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.HardwareIsolation.Error.IsolatedAlready: The requested hardware is already isolated.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

