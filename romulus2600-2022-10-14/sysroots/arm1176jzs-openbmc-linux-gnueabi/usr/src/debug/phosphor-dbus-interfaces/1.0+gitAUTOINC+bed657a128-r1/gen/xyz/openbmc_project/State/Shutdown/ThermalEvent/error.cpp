#include <xyz/openbmc_project/State/Shutdown/ThermalEvent/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Shutdown
{
namespace ThermalEvent
{
namespace Error
{
const char* Processor::name() const noexcept
{
    return errName;
}
const char* Processor::description() const noexcept
{
    return errDesc;
}
const char* Processor::what() const noexcept
{
    return errWhat;
}
const char* GPU::name() const noexcept
{
    return errName;
}
const char* GPU::description() const noexcept
{
    return errDesc;
}
const char* GPU::what() const noexcept
{
    return errWhat;
}
const char* Ambient::name() const noexcept
{
    return errName;
}
const char* Ambient::description() const noexcept
{
    return errDesc;
}
const char* Ambient::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace ThermalEvent
} // namespace Shutdown
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

