#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/CpuCore/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

CpuCore::CpuCore(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_CpuCore_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CpuCore::CpuCore(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CpuCore(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CpuCore::microcode() const ->
        uint32_t
{
    return _microcode;
}

int CpuCore::_callback_get_Microcode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CpuCore*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->microcode();
                    }
                ));
    }
}

auto CpuCore::microcode(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_microcode != value)
    {
        _microcode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_CpuCore_interface.property_changed("Microcode");
        }
    }

    return _microcode;
}

auto CpuCore::microcode(uint32_t val) ->
        uint32_t
{
    return microcode(val, false);
}

int CpuCore::_callback_set_Microcode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CpuCore*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->microcode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CpuCore
{
static const auto _property_Microcode =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void CpuCore::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Microcode")
    {
        auto& v = std::get<uint32_t>(val);
        microcode(v, skipSignal);
        return;
    }
}

auto CpuCore::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Microcode")
    {
        return microcode();
    }

    return PropertiesVariant();
}


const vtable_t CpuCore::_vtable[] = {
    vtable::start(),
    vtable::property("Microcode",
                     details::CpuCore::_property_Microcode
                        .data(),
                     _callback_get_Microcode,
                     _callback_set_Microcode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

