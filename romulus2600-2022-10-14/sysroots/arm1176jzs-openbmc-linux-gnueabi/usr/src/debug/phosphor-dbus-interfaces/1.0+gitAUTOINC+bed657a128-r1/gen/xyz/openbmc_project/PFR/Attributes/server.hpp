#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PFR
{
namespace server
{

class Attributes
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Attributes() = delete;
        Attributes(const Attributes&) = delete;
        Attributes& operator=(const Attributes&) = delete;
        Attributes(Attributes&&) = delete;
        Attributes& operator=(Attributes&&) = delete;
        virtual ~Attributes() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Attributes(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Attributes(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Provisioned */
        virtual bool provisioned() const;
        /** Set value of Provisioned with option to skip sending signal */
        virtual bool provisioned(bool value,
               bool skipSignal);
        /** Set value of Provisioned */
        virtual bool provisioned(bool value);
        /** Get value of Locked */
        virtual bool locked() const;
        /** Set value of Locked with option to skip sending signal */
        virtual bool locked(bool value,
               bool skipSignal);
        /** Set value of Locked */
        virtual bool locked(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_PFR_Attributes_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_PFR_Attributes_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.PFR.Attributes";

    private:

        /** @brief sd-bus callback for get-property 'Provisioned' */
        static int _callback_get_Provisioned(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Provisioned' */
        static int _callback_set_Provisioned(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Locked' */
        static int _callback_get_Locked(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Locked' */
        static int _callback_set_Locked(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_PFR_Attributes_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _provisioned{};
        bool _locked{};

};


} // namespace server
} // namespace PFR
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

