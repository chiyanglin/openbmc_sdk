#include <xyz/openbmc_project/Software/Version/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace Version
{
namespace Error
{
const char* Incompatible::name() const noexcept
{
    return errName;
}
const char* Incompatible::description() const noexcept
{
    return errDesc;
}
const char* Incompatible::what() const noexcept
{
    return errWhat;
}
const char* AlreadyExists::name() const noexcept
{
    return errName;
}
const char* AlreadyExists::description() const noexcept
{
    return errDesc;
}
const char* AlreadyExists::what() const noexcept
{
    return errWhat;
}
const char* InvalidSignature::name() const noexcept
{
    return errName;
}
const char* InvalidSignature::description() const noexcept
{
    return errDesc;
}
const char* InvalidSignature::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Version
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

