#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/CoolingType/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

CoolingType::CoolingType(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_CoolingType_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CoolingType::CoolingType(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CoolingType(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CoolingType::airCooled() const ->
        bool
{
    return _airCooled;
}

int CoolingType::_callback_get_AirCooled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CoolingType*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->airCooled();
                    }
                ));
    }
}

auto CoolingType::airCooled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_airCooled != value)
    {
        _airCooled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_CoolingType_interface.property_changed("AirCooled");
        }
    }

    return _airCooled;
}

auto CoolingType::airCooled(bool val) ->
        bool
{
    return airCooled(val, false);
}

int CoolingType::_callback_set_AirCooled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CoolingType*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->airCooled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CoolingType
{
static const auto _property_AirCooled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto CoolingType::waterCooled() const ->
        bool
{
    return _waterCooled;
}

int CoolingType::_callback_get_WaterCooled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CoolingType*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->waterCooled();
                    }
                ));
    }
}

auto CoolingType::waterCooled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_waterCooled != value)
    {
        _waterCooled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_CoolingType_interface.property_changed("WaterCooled");
        }
    }

    return _waterCooled;
}

auto CoolingType::waterCooled(bool val) ->
        bool
{
    return waterCooled(val, false);
}

int CoolingType::_callback_set_WaterCooled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CoolingType*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->waterCooled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CoolingType
{
static const auto _property_WaterCooled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void CoolingType::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AirCooled")
    {
        auto& v = std::get<bool>(val);
        airCooled(v, skipSignal);
        return;
    }
    if (_name == "WaterCooled")
    {
        auto& v = std::get<bool>(val);
        waterCooled(v, skipSignal);
        return;
    }
}

auto CoolingType::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AirCooled")
    {
        return airCooled();
    }
    if (_name == "WaterCooled")
    {
        return waterCooled();
    }

    return PropertiesVariant();
}


const vtable_t CoolingType::_vtable[] = {
    vtable::start(),
    vtable::property("AirCooled",
                     details::CoolingType::_property_AirCooled
                        .data(),
                     _callback_get_AirCooled,
                     _callback_set_AirCooled,
                     vtable::property_::emits_change),
    vtable::property("WaterCooled",
                     details::CoolingType::_property_WaterCooled
                        .data(),
                     _callback_get_WaterCooled,
                     _callback_set_WaterCooled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

