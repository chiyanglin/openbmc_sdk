#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

class BMC
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        BMC() = delete;
        BMC(const BMC&) = delete;
        BMC& operator=(const BMC&) = delete;
        BMC(BMC&&) = delete;
        BMC& operator=(BMC&&) = delete;
        virtual ~BMC() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        BMC(bus_t& bus, const char* path);

        enum class Transition
        {
            Reboot,
            HardReboot,
            None,
        };
        enum class BMCState
        {
            Ready,
            NotReady,
            UpdateInProgress,
            Quiesced,
        };
        enum class RebootCause
        {
            POR,
            PinholeReset,
            Watchdog,
            Unknown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                BMCState,
                RebootCause,
                Transition,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        BMC(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RequestedBMCTransition */
        virtual Transition requestedBMCTransition() const;
        /** Set value of RequestedBMCTransition with option to skip sending signal */
        virtual Transition requestedBMCTransition(Transition value,
               bool skipSignal);
        /** Set value of RequestedBMCTransition */
        virtual Transition requestedBMCTransition(Transition value);
        /** Get value of CurrentBMCState */
        virtual BMCState currentBMCState() const;
        /** Set value of CurrentBMCState with option to skip sending signal */
        virtual BMCState currentBMCState(BMCState value,
               bool skipSignal);
        /** Set value of CurrentBMCState */
        virtual BMCState currentBMCState(BMCState value);
        /** Get value of LastRebootTime */
        virtual uint64_t lastRebootTime() const;
        /** Set value of LastRebootTime with option to skip sending signal */
        virtual uint64_t lastRebootTime(uint64_t value,
               bool skipSignal);
        /** Set value of LastRebootTime */
        virtual uint64_t lastRebootTime(uint64_t value);
        /** Get value of LastRebootCause */
        virtual RebootCause lastRebootCause() const;
        /** Set value of LastRebootCause with option to skip sending signal */
        virtual RebootCause lastRebootCause(RebootCause value,
               bool skipSignal);
        /** Set value of LastRebootCause */
        virtual RebootCause lastRebootCause(RebootCause value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.BMC.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Transition convertTransitionFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.BMC.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Transition> convertStringToTransition(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.BMC.<value name>"
         */
        static std::string convertTransitionToString(Transition e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.BMC.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static BMCState convertBMCStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.BMC.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<BMCState> convertStringToBMCState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.BMC.<value name>"
         */
        static std::string convertBMCStateToString(BMCState e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.BMC.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static RebootCause convertRebootCauseFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.BMC.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<RebootCause> convertStringToRebootCause(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.BMC.<value name>"
         */
        static std::string convertRebootCauseToString(RebootCause e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_BMC_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_BMC_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.BMC";

    private:

        /** @brief sd-bus callback for get-property 'RequestedBMCTransition' */
        static int _callback_get_RequestedBMCTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RequestedBMCTransition' */
        static int _callback_set_RequestedBMCTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentBMCState' */
        static int _callback_get_CurrentBMCState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CurrentBMCState' */
        static int _callback_set_CurrentBMCState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LastRebootTime' */
        static int _callback_get_LastRebootTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LastRebootTime' */
        static int _callback_set_LastRebootTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LastRebootCause' */
        static int _callback_get_LastRebootCause(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LastRebootCause' */
        static int _callback_set_LastRebootCause(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_BMC_interface;
        sdbusplus::SdBusInterface *_intf;

        Transition _requestedBMCTransition = Transition::None;
        BMCState _currentBMCState{};
        uint64_t _lastRebootTime{};
        RebootCause _lastRebootCause = RebootCause::Unknown;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type BMC::Transition.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(BMC::Transition e)
{
    return BMC::convertTransitionToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type BMC::BMCState.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(BMC::BMCState e)
{
    return BMC::convertBMCStateToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type BMC::RebootCause.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(BMC::RebootCause e)
{
    return BMC::convertRebootCauseToString(e);
}

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::State::server::BMC::Transition>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::BMC::convertStringToTransition(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::BMC::Transition>
{
    static std::string op(xyz::openbmc_project::State::server::BMC::Transition value)
    {
        return xyz::openbmc_project::State::server::BMC::convertTransitionToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::BMC::BMCState>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::BMC::convertStringToBMCState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::BMC::BMCState>
{
    static std::string op(xyz::openbmc_project::State::server::BMC::BMCState value)
    {
        return xyz::openbmc_project::State::server::BMC::convertBMCStateToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::State::server::BMC::RebootCause>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::server::BMC::convertStringToRebootCause(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::server::BMC::RebootCause>
{
    static std::string op(xyz::openbmc_project::State::server::BMC::RebootCause value)
    {
        return xyz::openbmc_project::State::server::BMC::convertRebootCauseToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

