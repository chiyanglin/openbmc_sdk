#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Decorator/PowerSystemInputs/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Decorator
{
namespace server
{

PowerSystemInputs::PowerSystemInputs(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Decorator_PowerSystemInputs_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PowerSystemInputs::PowerSystemInputs(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PowerSystemInputs(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PowerSystemInputs::status() const ->
        Status
{
    return _status;
}

int PowerSystemInputs::_callback_get_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSystemInputs*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->status();
                    }
                ));
    }
}

auto PowerSystemInputs::status(Status value,
                                         bool skipSignal) ->
        Status
{
    if (_status != value)
    {
        _status = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Decorator_PowerSystemInputs_interface.property_changed("Status");
        }
    }

    return _status;
}

auto PowerSystemInputs::status(Status val) ->
        Status
{
    return status(val, false);
}

int PowerSystemInputs::_callback_set_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSystemInputs*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Status&& arg)
                    {
                        o->status(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerSystemInputs
{
static const auto _property_Status =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerSystemInputs::Status>());
}
}

void PowerSystemInputs::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Status")
    {
        auto& v = std::get<Status>(val);
        status(v, skipSignal);
        return;
    }
}

auto PowerSystemInputs::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Status")
    {
        return status();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for PowerSystemInputs::Status */
static const std::tuple<const char*, PowerSystemInputs::Status> mappingPowerSystemInputsStatus[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerSystemInputs.Status.Fault",                 PowerSystemInputs::Status::Fault ),
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerSystemInputs.Status.Good",                 PowerSystemInputs::Status::Good ),
        };

} // anonymous namespace

auto PowerSystemInputs::convertStringToStatus(const std::string& s) noexcept ->
        std::optional<Status>
{
    auto i = std::find_if(
            std::begin(mappingPowerSystemInputsStatus),
            std::end(mappingPowerSystemInputsStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPowerSystemInputsStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PowerSystemInputs::convertStatusFromString(const std::string& s) ->
        Status
{
    auto r = convertStringToStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PowerSystemInputs::convertStatusToString(PowerSystemInputs::Status v)
{
    auto i = std::find_if(
            std::begin(mappingPowerSystemInputsStatus),
            std::end(mappingPowerSystemInputsStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPowerSystemInputsStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t PowerSystemInputs::_vtable[] = {
    vtable::start(),
    vtable::property("Status",
                     details::PowerSystemInputs::_property_Status
                        .data(),
                     _callback_get_Status,
                     _callback_set_Status,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

