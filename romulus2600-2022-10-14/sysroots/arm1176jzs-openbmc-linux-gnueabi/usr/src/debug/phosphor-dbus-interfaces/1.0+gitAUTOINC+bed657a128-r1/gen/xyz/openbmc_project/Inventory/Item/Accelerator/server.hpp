#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class Accelerator
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Accelerator() = delete;
        Accelerator(const Accelerator&) = delete;
        Accelerator& operator=(const Accelerator&) = delete;
        Accelerator(Accelerator&&) = delete;
        Accelerator& operator=(Accelerator&&) = delete;
        virtual ~Accelerator() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Accelerator(bus_t& bus, const char* path);

        enum class AcceleratorType
        {
            ASIC,
            FPGA,
            GPU,
            Unknown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                AcceleratorType>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Accelerator(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Type */
        virtual AcceleratorType type() const;
        /** Set value of Type with option to skip sending signal */
        virtual AcceleratorType type(AcceleratorType value,
               bool skipSignal);
        /** Set value of Type */
        virtual AcceleratorType type(AcceleratorType value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Accelerator.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static AcceleratorType convertAcceleratorTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.Accelerator.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<AcceleratorType> convertStringToAcceleratorType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.Accelerator.<value name>"
         */
        static std::string convertAcceleratorTypeToString(AcceleratorType e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Accelerator_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Accelerator_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Accelerator";

    private:

        /** @brief sd-bus callback for get-property 'Type' */
        static int _callback_get_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Type' */
        static int _callback_set_Type(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Accelerator_interface;
        sdbusplus::SdBusInterface *_intf;

        AcceleratorType _type = AcceleratorType::Unknown;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Accelerator::AcceleratorType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Accelerator::AcceleratorType e)
{
    return Accelerator::convertAcceleratorTypeToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::Accelerator::AcceleratorType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::Accelerator::convertStringToAcceleratorType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::Accelerator::AcceleratorType>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::Accelerator::AcceleratorType value)
    {
        return xyz::openbmc_project::Inventory::Item::server::Accelerator::convertAcceleratorTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

