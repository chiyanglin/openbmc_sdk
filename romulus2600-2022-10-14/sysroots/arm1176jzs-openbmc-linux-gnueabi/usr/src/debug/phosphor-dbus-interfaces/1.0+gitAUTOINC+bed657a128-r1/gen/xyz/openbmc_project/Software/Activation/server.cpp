#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/Activation/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

Activation::Activation(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_Activation_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Activation::Activation(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Activation(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Activation::activation() const ->
        Activations
{
    return _activation;
}

int Activation::_callback_get_Activation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Activation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->activation();
                    }
                ));
    }
}

auto Activation::activation(Activations value,
                                         bool skipSignal) ->
        Activations
{
    if (_activation != value)
    {
        _activation = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_Activation_interface.property_changed("Activation");
        }
    }

    return _activation;
}

auto Activation::activation(Activations val) ->
        Activations
{
    return activation(val, false);
}

int Activation::_callback_set_Activation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Activation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Activations&& arg)
                    {
                        o->activation(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Activation
{
static const auto _property_Activation =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Software::server::Activation::Activations>());
}
}

auto Activation::requestedActivation() const ->
        RequestedActivations
{
    return _requestedActivation;
}

int Activation::_callback_get_RequestedActivation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Activation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->requestedActivation();
                    }
                ));
    }
}

auto Activation::requestedActivation(RequestedActivations value,
                                         bool skipSignal) ->
        RequestedActivations
{
    if (_requestedActivation != value)
    {
        _requestedActivation = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_Activation_interface.property_changed("RequestedActivation");
        }
    }

    return _requestedActivation;
}

auto Activation::requestedActivation(RequestedActivations val) ->
        RequestedActivations
{
    return requestedActivation(val, false);
}

int Activation::_callback_set_RequestedActivation(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Activation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](RequestedActivations&& arg)
                    {
                        o->requestedActivation(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Activation
{
static const auto _property_RequestedActivation =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Software::server::Activation::RequestedActivations>());
}
}

void Activation::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Activation")
    {
        auto& v = std::get<Activations>(val);
        activation(v, skipSignal);
        return;
    }
    if (_name == "RequestedActivation")
    {
        auto& v = std::get<RequestedActivations>(val);
        requestedActivation(v, skipSignal);
        return;
    }
}

auto Activation::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Activation")
    {
        return activation();
    }
    if (_name == "RequestedActivation")
    {
        return requestedActivation();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Activation::Activations */
static const std::tuple<const char*, Activation::Activations> mappingActivationActivations[] =
        {
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.NotReady",                 Activation::Activations::NotReady ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Invalid",                 Activation::Activations::Invalid ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Ready",                 Activation::Activations::Ready ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Activating",                 Activation::Activations::Activating ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Active",                 Activation::Activations::Active ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Failed",                 Activation::Activations::Failed ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Staged",                 Activation::Activations::Staged ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.Activations.Staging",                 Activation::Activations::Staging ),
        };

} // anonymous namespace

auto Activation::convertStringToActivations(const std::string& s) noexcept ->
        std::optional<Activations>
{
    auto i = std::find_if(
            std::begin(mappingActivationActivations),
            std::end(mappingActivationActivations),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingActivationActivations) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Activation::convertActivationsFromString(const std::string& s) ->
        Activations
{
    auto r = convertStringToActivations(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Activation::convertActivationsToString(Activation::Activations v)
{
    auto i = std::find_if(
            std::begin(mappingActivationActivations),
            std::end(mappingActivationActivations),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingActivationActivations))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Activation::RequestedActivations */
static const std::tuple<const char*, Activation::RequestedActivations> mappingActivationRequestedActivations[] =
        {
            std::make_tuple( "xyz.openbmc_project.Software.Activation.RequestedActivations.None",                 Activation::RequestedActivations::None ),
            std::make_tuple( "xyz.openbmc_project.Software.Activation.RequestedActivations.Active",                 Activation::RequestedActivations::Active ),
        };

} // anonymous namespace

auto Activation::convertStringToRequestedActivations(const std::string& s) noexcept ->
        std::optional<RequestedActivations>
{
    auto i = std::find_if(
            std::begin(mappingActivationRequestedActivations),
            std::end(mappingActivationRequestedActivations),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingActivationRequestedActivations) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Activation::convertRequestedActivationsFromString(const std::string& s) ->
        RequestedActivations
{
    auto r = convertStringToRequestedActivations(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Activation::convertRequestedActivationsToString(Activation::RequestedActivations v)
{
    auto i = std::find_if(
            std::begin(mappingActivationRequestedActivations),
            std::end(mappingActivationRequestedActivations),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingActivationRequestedActivations))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Activation::_vtable[] = {
    vtable::start(),
    vtable::property("Activation",
                     details::Activation::_property_Activation
                        .data(),
                     _callback_get_Activation,
                     _callback_set_Activation,
                     vtable::property_::emits_change),
    vtable::property("RequestedActivation",
                     details::Activation::_property_RequestedActivation
                        .data(),
                     _callback_get_RequestedActivation,
                     _callback_set_RequestedActivation,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

