#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Dump/Create/server.hpp>

#include <xyz/openbmc_project/Common/File/error.hpp>
#include <xyz/openbmc_project/Common/File/error.hpp>
#include <xyz/openbmc_project/Dump/Create/error.hpp>
#include <xyz/openbmc_project/Dump/Create/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Dump_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_CreateDump(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::map<std::string, std::variant<std::string, uint64_t>>&& additionalData)
                    {
                        return o->createDump(
                                additionalData);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::File::Error::Open& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::File::Error::Write& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Dump::Create::Error::Disabled& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Dump::Create::Error::QuotaExceeded& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_CreateDump =
        utility::tuple_to_array(message::types::type_id<
                std::map<std::string, std::variant<std::string, uint64_t>>>());
static const auto _return_CreateDump =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




namespace
{
/** String to enum mapping for Create::CreateParameters */
static const std::tuple<const char*, Create::CreateParameters> mappingCreateCreateParameters[] =
        {
            std::make_tuple( "xyz.openbmc_project.Dump.Create.CreateParameters.OriginatorId",                 Create::CreateParameters::OriginatorId ),
            std::make_tuple( "xyz.openbmc_project.Dump.Create.CreateParameters.OriginatorType",                 Create::CreateParameters::OriginatorType ),
        };

} // anonymous namespace

auto Create::convertStringToCreateParameters(const std::string& s) noexcept ->
        std::optional<CreateParameters>
{
    auto i = std::find_if(
            std::begin(mappingCreateCreateParameters),
            std::end(mappingCreateCreateParameters),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCreateCreateParameters) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Create::convertCreateParametersFromString(const std::string& s) ->
        CreateParameters
{
    auto r = convertStringToCreateParameters(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Create::convertCreateParametersToString(Create::CreateParameters v)
{
    auto i = std::find_if(
            std::begin(mappingCreateCreateParameters),
            std::end(mappingCreateCreateParameters),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCreateCreateParameters))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("CreateDump",
                   details::Create::_param_CreateDump
                        .data(),
                   details::Create::_return_CreateDump
                        .data(),
                   _callback_CreateDump),
    vtable::end()
};

} // namespace server
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

