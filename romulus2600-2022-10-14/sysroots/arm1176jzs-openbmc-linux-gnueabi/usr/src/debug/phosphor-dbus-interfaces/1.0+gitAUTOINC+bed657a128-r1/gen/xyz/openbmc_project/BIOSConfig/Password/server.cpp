#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/BIOSConfig/Password/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace server
{

Password::Password(bus_t& bus, const char* path)
        : _xyz_openbmc_project_BIOSConfig_Password_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Password::Password(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Password(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Password::_callback_ChangePassword(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Password*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& userName, std::string&& currentPassword, std::string&& newPassword)
                    {
                        return o->changePassword(
                                userName, currentPassword, newPassword);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::PasswordNotSettable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::InvalidCurrentPassword& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Password
{
static const auto _param_ChangePassword =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string, std::string>());
static const auto _return_ChangePassword =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}



auto Password::passwordInitialized() const ->
        bool
{
    return _passwordInitialized;
}

int Password::_callback_get_PasswordInitialized(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Password*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->passwordInitialized();
                    }
                ));
    }
}

auto Password::passwordInitialized(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_passwordInitialized != value)
    {
        _passwordInitialized = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_BIOSConfig_Password_interface.property_changed("PasswordInitialized");
        }
    }

    return _passwordInitialized;
}

auto Password::passwordInitialized(bool val) ->
        bool
{
    return passwordInitialized(val, false);
}

int Password::_callback_set_PasswordInitialized(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Password*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->passwordInitialized(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Password
{
static const auto _property_PasswordInitialized =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Password::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PasswordInitialized")
    {
        auto& v = std::get<bool>(val);
        passwordInitialized(v, skipSignal);
        return;
    }
}

auto Password::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PasswordInitialized")
    {
        return passwordInitialized();
    }

    return PropertiesVariant();
}


const vtable_t Password::_vtable[] = {
    vtable::start(),

    vtable::method("ChangePassword",
                   details::Password::_param_ChangePassword
                        .data(),
                   details::Password::_return_ChangePassword
                        .data(),
                   _callback_ChangePassword),
    vtable::property("PasswordInitialized",
                     details::Password::_property_PasswordInitialized
                        .data(),
                     _callback_get_PasswordInitialized,
                     _callback_set_PasswordInitialized,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

