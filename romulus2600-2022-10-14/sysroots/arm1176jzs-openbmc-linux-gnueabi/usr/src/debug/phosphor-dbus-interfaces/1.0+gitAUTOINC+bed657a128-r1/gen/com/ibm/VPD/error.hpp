#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace VPD
{
namespace Error
{

struct LocationNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.LocationNotFound";
    static constexpr auto errDesc =
            "Location is not found.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.LocationNotFound: Location is not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct NodeNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.NodeNotFound";
    static constexpr auto errDesc =
            "Node number is not found.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.NodeNotFound: Node number is not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct PathNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.PathNotFound";
    static constexpr auto errDesc =
            "Inventory path is not found.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.PathNotFound: Inventory path is not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct RecordNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.RecordNotFound";
    static constexpr auto errDesc =
            "Record not found.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.RecordNotFound: Record not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct KeywordNotFound final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.KeywordNotFound";
    static constexpr auto errDesc =
            "Keyword is not found.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.KeywordNotFound: Keyword is not found.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct BlankSystemVPD final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.BlankSystemVPD";
    static constexpr auto errDesc =
            "System VPD is blank on both hardware and cache. On IBM systems, certain VPD data must be available for the system to boot. This error is used to indicate that no valid data was found by the BMC.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.BlankSystemVPD: System VPD is blank on both hardware and cache. On IBM systems, certain VPD data must be available for the system to boot. This error is used to indicate that no valid data was found by the BMC.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InvalidEepromPath final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.InvalidEepromPath";
    static constexpr auto errDesc =
            "EEPROM path is invalid. Parser failed to access the path.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.InvalidEepromPath: EEPROM path is invalid. Parser failed to access the path.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InvalidVPD final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.InvalidVPD";
    static constexpr auto errDesc =
            "VPD file is not valid. Mandatory records are missing in VPD file.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.InvalidVPD: VPD file is not valid. Mandatory records are missing in VPD file.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct EccCheckFailed final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.EccCheckFailed";
    static constexpr auto errDesc =
            "";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.EccCheckFailed: ";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InvalidJson final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.InvalidJson";
    static constexpr auto errDesc =
            "Invalid Json file.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.InvalidJson: Invalid Json file.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct DbusFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "com.ibm.VPD.Error.DbusFailure";
    static constexpr auto errDesc =
            "DBus error occurred.";
    static constexpr auto errWhat =
            "com.ibm.VPD.Error.DbusFailure: DBus error occurred.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace VPD
} // namespace ibm
} // namespace com
} // namespace sdbusplus

