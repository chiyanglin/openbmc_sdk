#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/Dump/Entry/Hostboot/server.hpp>

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace Entry
{
namespace server
{

Hostboot::Hostboot(bus_t& bus, const char* path)
        : _com_ibm_Dump_Entry_Hostboot_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}





const vtable_t Hostboot::_vtable[] = {
    vtable::start(),
    vtable::end()
};

} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace ibm
} // namespace com
} // namespace sdbusplus

