#include <org/open_power/Host/error.hpp>

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Host
{
namespace Error
{
const char* Event::name() const noexcept
{
    return errName;
}
const char* Event::description() const noexcept
{
    return errDesc;
}
const char* Event::what() const noexcept
{
    return errWhat;
}
const char* MaintenanceProcedure::name() const noexcept
{
    return errName;
}
const char* MaintenanceProcedure::description() const noexcept
{
    return errDesc;
}
const char* MaintenanceProcedure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Host
} // namespace open_power
} // namespace org
} // namespace sdbusplus

