#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Cpu/server.hpp>












namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Cpu::Cpu(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Cpu_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Cpu::Cpu(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Cpu(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Cpu::socket() const ->
        std::string
{
    return _socket;
}

int Cpu::_callback_get_Socket(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->socket();
                    }
                ));
    }
}

auto Cpu::socket(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_socket != value)
    {
        _socket = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("Socket");
        }
    }

    return _socket;
}

auto Cpu::socket(std::string val) ->
        std::string
{
    return socket(val, false);
}

int Cpu::_callback_set_Socket(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->socket(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_Socket =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Cpu::family() const ->
        std::string
{
    return _family;
}

int Cpu::_callback_get_Family(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->family();
                    }
                ));
    }
}

auto Cpu::family(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_family != value)
    {
        _family = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("Family");
        }
    }

    return _family;
}

auto Cpu::family(std::string val) ->
        std::string
{
    return family(val, false);
}

int Cpu::_callback_set_Family(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->family(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_Family =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Cpu::effectiveFamily() const ->
        uint16_t
{
    return _effectiveFamily;
}

int Cpu::_callback_get_EffectiveFamily(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->effectiveFamily();
                    }
                ));
    }
}

auto Cpu::effectiveFamily(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_effectiveFamily != value)
    {
        _effectiveFamily = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("EffectiveFamily");
        }
    }

    return _effectiveFamily;
}

auto Cpu::effectiveFamily(uint16_t val) ->
        uint16_t
{
    return effectiveFamily(val, false);
}

int Cpu::_callback_set_EffectiveFamily(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->effectiveFamily(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_EffectiveFamily =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Cpu::effectiveModel() const ->
        uint16_t
{
    return _effectiveModel;
}

int Cpu::_callback_get_EffectiveModel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->effectiveModel();
                    }
                ));
    }
}

auto Cpu::effectiveModel(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_effectiveModel != value)
    {
        _effectiveModel = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("EffectiveModel");
        }
    }

    return _effectiveModel;
}

auto Cpu::effectiveModel(uint16_t val) ->
        uint16_t
{
    return effectiveModel(val, false);
}

int Cpu::_callback_set_EffectiveModel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->effectiveModel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_EffectiveModel =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Cpu::id() const ->
        uint64_t
{
    return _id;
}

int Cpu::_callback_get_Id(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->id();
                    }
                ));
    }
}

auto Cpu::id(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_id != value)
    {
        _id = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("Id");
        }
    }

    return _id;
}

auto Cpu::id(uint64_t val) ->
        uint64_t
{
    return id(val, false);
}

int Cpu::_callback_set_Id(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->id(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_Id =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Cpu::maxSpeedInMhz() const ->
        uint32_t
{
    return _maxSpeedInMhz;
}

int Cpu::_callback_get_MaxSpeedInMhz(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxSpeedInMhz();
                    }
                ));
    }
}

auto Cpu::maxSpeedInMhz(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_maxSpeedInMhz != value)
    {
        _maxSpeedInMhz = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("MaxSpeedInMhz");
        }
    }

    return _maxSpeedInMhz;
}

auto Cpu::maxSpeedInMhz(uint32_t val) ->
        uint32_t
{
    return maxSpeedInMhz(val, false);
}

int Cpu::_callback_set_MaxSpeedInMhz(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->maxSpeedInMhz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_MaxSpeedInMhz =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Cpu::characteristics() const ->
        std::vector<Capability>
{
    return _characteristics;
}

int Cpu::_callback_get_Characteristics(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->characteristics();
                    }
                ));
    }
}

auto Cpu::characteristics(std::vector<Capability> value,
                                         bool skipSignal) ->
        std::vector<Capability>
{
    if (_characteristics != value)
    {
        _characteristics = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("Characteristics");
        }
    }

    return _characteristics;
}

auto Cpu::characteristics(std::vector<Capability> val) ->
        std::vector<Capability>
{
    return characteristics(val, false);
}

int Cpu::_callback_set_Characteristics(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<Capability>&& arg)
                    {
                        o->characteristics(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_Characteristics =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::xyz::openbmc_project::Inventory::Item::server::Cpu::Capability>>());
}
}

auto Cpu::coreCount() const ->
        uint16_t
{
    return _coreCount;
}

int Cpu::_callback_get_CoreCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->coreCount();
                    }
                ));
    }
}

auto Cpu::coreCount(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_coreCount != value)
    {
        _coreCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("CoreCount");
        }
    }

    return _coreCount;
}

auto Cpu::coreCount(uint16_t val) ->
        uint16_t
{
    return coreCount(val, false);
}

int Cpu::_callback_set_CoreCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->coreCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_CoreCount =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Cpu::threadCount() const ->
        uint16_t
{
    return _threadCount;
}

int Cpu::_callback_get_ThreadCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->threadCount();
                    }
                ));
    }
}

auto Cpu::threadCount(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_threadCount != value)
    {
        _threadCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("ThreadCount");
        }
    }

    return _threadCount;
}

auto Cpu::threadCount(uint16_t val) ->
        uint16_t
{
    return threadCount(val, false);
}

int Cpu::_callback_set_ThreadCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->threadCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_ThreadCount =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Cpu::step() const ->
        uint16_t
{
    return _step;
}

int Cpu::_callback_get_Step(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->step();
                    }
                ));
    }
}

auto Cpu::step(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_step != value)
    {
        _step = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("Step");
        }
    }

    return _step;
}

auto Cpu::step(uint16_t val) ->
        uint16_t
{
    return step(val, false);
}

int Cpu::_callback_set_Step(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->step(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_Step =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Cpu::microcode() const ->
        uint32_t
{
    return _microcode;
}

int Cpu::_callback_get_Microcode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->microcode();
                    }
                ));
    }
}

auto Cpu::microcode(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_microcode != value)
    {
        _microcode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_interface.property_changed("Microcode");
        }
    }

    return _microcode;
}

auto Cpu::microcode(uint32_t val) ->
        uint32_t
{
    return microcode(val, false);
}

int Cpu::_callback_set_Microcode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cpu*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->microcode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cpu
{
static const auto _property_Microcode =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void Cpu::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Socket")
    {
        auto& v = std::get<std::string>(val);
        socket(v, skipSignal);
        return;
    }
    if (_name == "Family")
    {
        auto& v = std::get<std::string>(val);
        family(v, skipSignal);
        return;
    }
    if (_name == "EffectiveFamily")
    {
        auto& v = std::get<uint16_t>(val);
        effectiveFamily(v, skipSignal);
        return;
    }
    if (_name == "EffectiveModel")
    {
        auto& v = std::get<uint16_t>(val);
        effectiveModel(v, skipSignal);
        return;
    }
    if (_name == "Id")
    {
        auto& v = std::get<uint64_t>(val);
        id(v, skipSignal);
        return;
    }
    if (_name == "MaxSpeedInMhz")
    {
        auto& v = std::get<uint32_t>(val);
        maxSpeedInMhz(v, skipSignal);
        return;
    }
    if (_name == "Characteristics")
    {
        auto& v = std::get<std::vector<Capability>>(val);
        characteristics(v, skipSignal);
        return;
    }
    if (_name == "CoreCount")
    {
        auto& v = std::get<uint16_t>(val);
        coreCount(v, skipSignal);
        return;
    }
    if (_name == "ThreadCount")
    {
        auto& v = std::get<uint16_t>(val);
        threadCount(v, skipSignal);
        return;
    }
    if (_name == "Step")
    {
        auto& v = std::get<uint16_t>(val);
        step(v, skipSignal);
        return;
    }
    if (_name == "Microcode")
    {
        auto& v = std::get<uint32_t>(val);
        microcode(v, skipSignal);
        return;
    }
}

auto Cpu::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Socket")
    {
        return socket();
    }
    if (_name == "Family")
    {
        return family();
    }
    if (_name == "EffectiveFamily")
    {
        return effectiveFamily();
    }
    if (_name == "EffectiveModel")
    {
        return effectiveModel();
    }
    if (_name == "Id")
    {
        return id();
    }
    if (_name == "MaxSpeedInMhz")
    {
        return maxSpeedInMhz();
    }
    if (_name == "Characteristics")
    {
        return characteristics();
    }
    if (_name == "CoreCount")
    {
        return coreCount();
    }
    if (_name == "ThreadCount")
    {
        return threadCount();
    }
    if (_name == "Step")
    {
        return step();
    }
    if (_name == "Microcode")
    {
        return microcode();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Cpu::Capability */
static const std::tuple<const char*, Cpu::Capability> mappingCpuCapability[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Cpu.Capability.Capable64bit",                 Cpu::Capability::Capable64bit ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Cpu.Capability.MultiCore",                 Cpu::Capability::MultiCore ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Cpu.Capability.HardwareThread",                 Cpu::Capability::HardwareThread ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Cpu.Capability.ExecuteProtection",                 Cpu::Capability::ExecuteProtection ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Cpu.Capability.EnhancedVirtualization",                 Cpu::Capability::EnhancedVirtualization ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Cpu.Capability.PowerPerformanceControl",                 Cpu::Capability::PowerPerformanceControl ),
        };

} // anonymous namespace

auto Cpu::convertStringToCapability(const std::string& s) noexcept ->
        std::optional<Capability>
{
    auto i = std::find_if(
            std::begin(mappingCpuCapability),
            std::end(mappingCpuCapability),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCpuCapability) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Cpu::convertCapabilityFromString(const std::string& s) ->
        Capability
{
    auto r = convertStringToCapability(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Cpu::convertCapabilityToString(Cpu::Capability v)
{
    auto i = std::find_if(
            std::begin(mappingCpuCapability),
            std::end(mappingCpuCapability),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCpuCapability))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Cpu::_vtable[] = {
    vtable::start(),
    vtable::property("Socket",
                     details::Cpu::_property_Socket
                        .data(),
                     _callback_get_Socket,
                     _callback_set_Socket,
                     vtable::property_::emits_change),
    vtable::property("Family",
                     details::Cpu::_property_Family
                        .data(),
                     _callback_get_Family,
                     _callback_set_Family,
                     vtable::property_::emits_change),
    vtable::property("EffectiveFamily",
                     details::Cpu::_property_EffectiveFamily
                        .data(),
                     _callback_get_EffectiveFamily,
                     _callback_set_EffectiveFamily,
                     vtable::property_::emits_change),
    vtable::property("EffectiveModel",
                     details::Cpu::_property_EffectiveModel
                        .data(),
                     _callback_get_EffectiveModel,
                     _callback_set_EffectiveModel,
                     vtable::property_::emits_change),
    vtable::property("Id",
                     details::Cpu::_property_Id
                        .data(),
                     _callback_get_Id,
                     _callback_set_Id,
                     vtable::property_::emits_change),
    vtable::property("MaxSpeedInMhz",
                     details::Cpu::_property_MaxSpeedInMhz
                        .data(),
                     _callback_get_MaxSpeedInMhz,
                     _callback_set_MaxSpeedInMhz,
                     vtable::property_::emits_change),
    vtable::property("Characteristics",
                     details::Cpu::_property_Characteristics
                        .data(),
                     _callback_get_Characteristics,
                     _callback_set_Characteristics,
                     vtable::property_::emits_change),
    vtable::property("CoreCount",
                     details::Cpu::_property_CoreCount
                        .data(),
                     _callback_get_CoreCount,
                     _callback_set_CoreCount,
                     vtable::property_::emits_change),
    vtable::property("ThreadCount",
                     details::Cpu::_property_ThreadCount
                        .data(),
                     _callback_get_ThreadCount,
                     _callback_set_ThreadCount,
                     vtable::property_::emits_change),
    vtable::property("Step",
                     details::Cpu::_property_Step
                        .data(),
                     _callback_get_Step,
                     _callback_set_Step,
                     vtable::property_::emits_change),
    vtable::property("Microcode",
                     details::Cpu::_property_Microcode
                        .data(),
                     _callback_get_Microcode,
                     _callback_set_Microcode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

