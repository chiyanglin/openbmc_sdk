#include <xyz/openbmc_project/State/Shutdown/Power/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Shutdown
{
namespace Power
{
namespace Error
{
const char* Fault::name() const noexcept
{
    return errName;
}
const char* Fault::description() const noexcept
{
    return errDesc;
}
const char* Fault::what() const noexcept
{
    return errWhat;
}
const char* Blackout::name() const noexcept
{
    return errName;
}
const char* Blackout::description() const noexcept
{
    return errDesc;
}
const char* Blackout::what() const noexcept
{
    return errWhat;
}
const char* Regulator::name() const noexcept
{
    return errName;
}
const char* Regulator::description() const noexcept
{
    return errDesc;
}
const char* Regulator::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Power
} // namespace Shutdown
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

