#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/Authority/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

Authority::Authority(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_Authority_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Authority::_callback_SignCSR(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Authority*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& csr)
                    {
                        return o->signCSR(
                                csr);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Authority
{
static const auto _param_SignCSR =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_SignCSR =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t Authority::_vtable[] = {
    vtable::start(),

    vtable::method("SignCSR",
                   details::Authority::_param_SignCSR
                        .data(),
                   details::Authority::_return_SignCSR
                        .data(),
                   _callback_SignCSR),
    vtable::end()
};

} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

