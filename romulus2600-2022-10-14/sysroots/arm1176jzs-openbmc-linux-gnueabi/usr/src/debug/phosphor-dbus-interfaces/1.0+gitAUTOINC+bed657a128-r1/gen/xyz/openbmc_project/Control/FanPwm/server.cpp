#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/FanPwm/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

FanPwm::FanPwm(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_FanPwm_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FanPwm::FanPwm(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FanPwm(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FanPwm::target() const ->
        uint64_t
{
    return _target;
}

int FanPwm::_callback_get_Target(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanPwm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->target();
                    }
                ));
    }
}

auto FanPwm::target(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_target != value)
    {
        _target = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_FanPwm_interface.property_changed("Target");
        }
    }

    return _target;
}

auto FanPwm::target(uint64_t val) ->
        uint64_t
{
    return target(val, false);
}

int FanPwm::_callback_set_Target(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanPwm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->target(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FanPwm
{
static const auto _property_Target =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void FanPwm::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Target")
    {
        auto& v = std::get<uint64_t>(val);
        target(v, skipSignal);
        return;
    }
}

auto FanPwm::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Target")
    {
        return target();
    }

    return PropertiesVariant();
}


const vtable_t FanPwm::_vtable[] = {
    vtable::start(),
    vtable::property("Target",
                     details::FanPwm::_property_Target
                        .data(),
                     _callback_get_Target,
                     _callback_set_Target,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

