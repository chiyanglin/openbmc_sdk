#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

class PowerSupplyRedundancy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PowerSupplyRedundancy() = delete;
        PowerSupplyRedundancy(const PowerSupplyRedundancy&) = delete;
        PowerSupplyRedundancy& operator=(const PowerSupplyRedundancy&) = delete;
        PowerSupplyRedundancy(PowerSupplyRedundancy&&) = delete;
        PowerSupplyRedundancy& operator=(PowerSupplyRedundancy&&) = delete;
        virtual ~PowerSupplyRedundancy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PowerSupplyRedundancy(bus_t& bus, const char* path);

        enum class Algo
        {
            bmcSpecific,
            userSpecific,
        };
        enum class Status
        {
            inProgress,
            completed,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Algo,
                Status,
                bool,
                std::vector<uint8_t>,
                uint32_t,
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PowerSupplyRedundancy(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PowerSupplyRedundancyEnabled */
        virtual bool powerSupplyRedundancyEnabled() const;
        /** Set value of PowerSupplyRedundancyEnabled with option to skip sending signal */
        virtual bool powerSupplyRedundancyEnabled(bool value,
               bool skipSignal);
        /** Set value of PowerSupplyRedundancyEnabled */
        virtual bool powerSupplyRedundancyEnabled(bool value);
        /** Get value of RotationEnabled */
        virtual bool rotationEnabled() const;
        /** Set value of RotationEnabled with option to skip sending signal */
        virtual bool rotationEnabled(bool value,
               bool skipSignal);
        /** Set value of RotationEnabled */
        virtual bool rotationEnabled(bool value);
        /** Get value of RotationAlgorithm */
        virtual Algo rotationAlgorithm() const;
        /** Set value of RotationAlgorithm with option to skip sending signal */
        virtual Algo rotationAlgorithm(Algo value,
               bool skipSignal);
        /** Set value of RotationAlgorithm */
        virtual Algo rotationAlgorithm(Algo value);
        /** Get value of RotationRankOrder */
        virtual std::vector<uint8_t> rotationRankOrder() const;
        /** Set value of RotationRankOrder with option to skip sending signal */
        virtual std::vector<uint8_t> rotationRankOrder(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RotationRankOrder */
        virtual std::vector<uint8_t> rotationRankOrder(std::vector<uint8_t> value);
        /** Get value of PeriodOfRotation */
        virtual uint32_t periodOfRotation() const;
        /** Set value of PeriodOfRotation with option to skip sending signal */
        virtual uint32_t periodOfRotation(uint32_t value,
               bool skipSignal);
        /** Set value of PeriodOfRotation */
        virtual uint32_t periodOfRotation(uint32_t value);
        /** Get value of ColdRedundancyStatus */
        virtual Status coldRedundancyStatus() const;
        /** Set value of ColdRedundancyStatus with option to skip sending signal */
        virtual Status coldRedundancyStatus(Status value,
               bool skipSignal);
        /** Set value of ColdRedundancyStatus */
        virtual Status coldRedundancyStatus(Status value);
        /** Get value of PSUNumber */
        virtual uint8_t psuNumber() const;
        /** Set value of PSUNumber with option to skip sending signal */
        virtual uint8_t psuNumber(uint8_t value,
               bool skipSignal);
        /** Set value of PSUNumber */
        virtual uint8_t psuNumber(uint8_t value);
        /** Get value of RedundantCount */
        virtual uint8_t redundantCount() const;
        /** Set value of RedundantCount with option to skip sending signal */
        virtual uint8_t redundantCount(uint8_t value,
               bool skipSignal);
        /** Set value of RedundantCount */
        virtual uint8_t redundantCount(uint8_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.PowerSupplyRedundancy.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Algo convertAlgoFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.PowerSupplyRedundancy.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Algo> convertStringToAlgo(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.PowerSupplyRedundancy.<value name>"
         */
        static std::string convertAlgoToString(Algo e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.PowerSupplyRedundancy.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Status convertStatusFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.PowerSupplyRedundancy.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Status> convertStringToStatus(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.PowerSupplyRedundancy.<value name>"
         */
        static std::string convertStatusToString(Status e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.PowerSupplyRedundancy";

    private:

        /** @brief sd-bus callback for get-property 'PowerSupplyRedundancyEnabled' */
        static int _callback_get_PowerSupplyRedundancyEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RotationEnabled' */
        static int _callback_get_RotationEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RotationEnabled' */
        static int _callback_set_RotationEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RotationAlgorithm' */
        static int _callback_get_RotationAlgorithm(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RotationAlgorithm' */
        static int _callback_set_RotationAlgorithm(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RotationRankOrder' */
        static int _callback_get_RotationRankOrder(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RotationRankOrder' */
        static int _callback_set_RotationRankOrder(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PeriodOfRotation' */
        static int _callback_get_PeriodOfRotation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PeriodOfRotation' */
        static int _callback_set_PeriodOfRotation(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ColdRedundancyStatus' */
        static int _callback_get_ColdRedundancyStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ColdRedundancyStatus' */
        static int _callback_set_ColdRedundancyStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PSUNumber' */
        static int _callback_get_PSUNumber(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PSUNumber' */
        static int _callback_set_PSUNumber(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RedundantCount' */
        static int _callback_get_RedundantCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RedundantCount' */
        static int _callback_set_RedundantCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_PowerSupplyRedundancy_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _powerSupplyRedundancyEnabled{};
        bool _rotationEnabled{};
        Algo _rotationAlgorithm{};
        std::vector<uint8_t> _rotationRankOrder{};
        uint32_t _periodOfRotation{};
        Status _coldRedundancyStatus{};
        uint8_t _psuNumber{};
        uint8_t _redundantCount = 2;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PowerSupplyRedundancy::Algo.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PowerSupplyRedundancy::Algo e)
{
    return PowerSupplyRedundancy::convertAlgoToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PowerSupplyRedundancy::Status.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PowerSupplyRedundancy::Status e)
{
    return PowerSupplyRedundancy::convertStatusToString(e);
}

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Algo>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::server::PowerSupplyRedundancy::convertStringToAlgo(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Algo>
{
    static std::string op(xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Algo value)
    {
        return xyz::openbmc_project::Control::server::PowerSupplyRedundancy::convertAlgoToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Status>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::server::PowerSupplyRedundancy::convertStringToStatus(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Status>
{
    static std::string op(xyz::openbmc_project::Control::server::PowerSupplyRedundancy::Status value)
    {
        return xyz::openbmc_project::Control::server::PowerSupplyRedundancy::convertStatusToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

