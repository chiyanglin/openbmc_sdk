#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Host/TurboAllowed/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Host
{
namespace server
{

TurboAllowed::TurboAllowed(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Host_TurboAllowed_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

TurboAllowed::TurboAllowed(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : TurboAllowed(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto TurboAllowed::turboAllowed() const ->
        bool
{
    return _turboAllowed;
}

int TurboAllowed::_callback_get_TurboAllowed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<TurboAllowed*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->turboAllowed();
                    }
                ));
    }
}

auto TurboAllowed::turboAllowed(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_turboAllowed != value)
    {
        _turboAllowed = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Host_TurboAllowed_interface.property_changed("TurboAllowed");
        }
    }

    return _turboAllowed;
}

auto TurboAllowed::turboAllowed(bool val) ->
        bool
{
    return turboAllowed(val, false);
}

int TurboAllowed::_callback_set_TurboAllowed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<TurboAllowed*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->turboAllowed(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace TurboAllowed
{
static const auto _property_TurboAllowed =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void TurboAllowed::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "TurboAllowed")
    {
        auto& v = std::get<bool>(val);
        turboAllowed(v, skipSignal);
        return;
    }
}

auto TurboAllowed::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "TurboAllowed")
    {
        return turboAllowed();
    }

    return PropertiesVariant();
}


const vtable_t TurboAllowed::_vtable[] = {
    vtable::start(),
    vtable::property("TurboAllowed",
                     details::TurboAllowed::_property_TurboAllowed
                        .data(),
                     _callback_get_TurboAllowed,
                     _callback_set_TurboAllowed,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Host
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

