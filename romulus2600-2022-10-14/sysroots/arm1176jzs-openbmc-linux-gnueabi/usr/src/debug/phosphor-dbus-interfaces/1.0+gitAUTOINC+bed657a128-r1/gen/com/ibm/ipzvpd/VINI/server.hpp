#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class VINI
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        VINI() = delete;
        VINI(const VINI&) = delete;
        VINI& operator=(const VINI&) = delete;
        VINI(VINI&&) = delete;
        VINI& operator=(VINI&&) = delete;
        virtual ~VINI() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        VINI(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        VINI(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of DR */
        virtual std::vector<uint8_t> dr() const;
        /** Set value of DR with option to skip sending signal */
        virtual std::vector<uint8_t> dr(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DR */
        virtual std::vector<uint8_t> dr(std::vector<uint8_t> value);
        /** Get value of CE */
        virtual std::vector<uint8_t> ce() const;
        /** Set value of CE with option to skip sending signal */
        virtual std::vector<uint8_t> ce(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of CE */
        virtual std::vector<uint8_t> ce(std::vector<uint8_t> value);
        /** Get value of VZ */
        virtual std::vector<uint8_t> vz() const;
        /** Set value of VZ with option to skip sending signal */
        virtual std::vector<uint8_t> vz(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of VZ */
        virtual std::vector<uint8_t> vz(std::vector<uint8_t> value);
        /** Get value of FN */
        virtual std::vector<uint8_t> fn() const;
        /** Set value of FN with option to skip sending signal */
        virtual std::vector<uint8_t> fn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of FN */
        virtual std::vector<uint8_t> fn(std::vector<uint8_t> value);
        /** Get value of PN */
        virtual std::vector<uint8_t> pn() const;
        /** Set value of PN with option to skip sending signal */
        virtual std::vector<uint8_t> pn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PN */
        virtual std::vector<uint8_t> pn(std::vector<uint8_t> value);
        /** Get value of SN */
        virtual std::vector<uint8_t> sn() const;
        /** Set value of SN with option to skip sending signal */
        virtual std::vector<uint8_t> sn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of SN */
        virtual std::vector<uint8_t> sn(std::vector<uint8_t> value);
        /** Get value of CC */
        virtual std::vector<uint8_t> cc() const;
        /** Set value of CC with option to skip sending signal */
        virtual std::vector<uint8_t> cc(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of CC */
        virtual std::vector<uint8_t> cc(std::vector<uint8_t> value);
        /** Get value of HE */
        virtual std::vector<uint8_t> he() const;
        /** Set value of HE with option to skip sending signal */
        virtual std::vector<uint8_t> he(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of HE */
        virtual std::vector<uint8_t> he(std::vector<uint8_t> value);
        /** Get value of CT */
        virtual std::vector<uint8_t> ct() const;
        /** Set value of CT with option to skip sending signal */
        virtual std::vector<uint8_t> ct(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of CT */
        virtual std::vector<uint8_t> ct(std::vector<uint8_t> value);
        /** Get value of HW */
        virtual std::vector<uint8_t> hw() const;
        /** Set value of HW with option to skip sending signal */
        virtual std::vector<uint8_t> hw(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of HW */
        virtual std::vector<uint8_t> hw(std::vector<uint8_t> value);
        /** Get value of B3 */
        virtual std::vector<uint8_t> b3() const;
        /** Set value of B3 with option to skip sending signal */
        virtual std::vector<uint8_t> b3(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of B3 */
        virtual std::vector<uint8_t> b3(std::vector<uint8_t> value);
        /** Get value of B4 */
        virtual std::vector<uint8_t> b4() const;
        /** Set value of B4 with option to skip sending signal */
        virtual std::vector<uint8_t> b4(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of B4 */
        virtual std::vector<uint8_t> b4(std::vector<uint8_t> value);
        /** Get value of B7 */
        virtual std::vector<uint8_t> b7() const;
        /** Set value of B7 with option to skip sending signal */
        virtual std::vector<uint8_t> b7(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of B7 */
        virtual std::vector<uint8_t> b7(std::vector<uint8_t> value);
        /** Get value of HX */
        virtual std::vector<uint8_t> hx() const;
        /** Set value of HX with option to skip sending signal */
        virtual std::vector<uint8_t> hx(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of HX */
        virtual std::vector<uint8_t> hx(std::vector<uint8_t> value);
        /** Get value of FG */
        virtual std::vector<uint8_t> fg() const;
        /** Set value of FG with option to skip sending signal */
        virtual std::vector<uint8_t> fg(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of FG */
        virtual std::vector<uint8_t> fg(std::vector<uint8_t> value);
        /** Get value of TS */
        virtual std::vector<uint8_t> ts() const;
        /** Set value of TS with option to skip sending signal */
        virtual std::vector<uint8_t> ts(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of TS */
        virtual std::vector<uint8_t> ts(std::vector<uint8_t> value);
        /** Get value of PR */
        virtual std::vector<uint8_t> pr() const;
        /** Set value of PR with option to skip sending signal */
        virtual std::vector<uint8_t> pr(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PR */
        virtual std::vector<uint8_t> pr(std::vector<uint8_t> value);
        /** Get value of VN */
        virtual std::vector<uint8_t> vn() const;
        /** Set value of VN with option to skip sending signal */
        virtual std::vector<uint8_t> vn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of VN */
        virtual std::vector<uint8_t> vn(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_VINI_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_VINI_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.VINI";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DR' */
        static int _callback_get_DR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DR' */
        static int _callback_set_DR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CE' */
        static int _callback_get_CE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CE' */
        static int _callback_set_CE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VZ' */
        static int _callback_get_VZ(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VZ' */
        static int _callback_set_VZ(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'FN' */
        static int _callback_get_FN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'FN' */
        static int _callback_set_FN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PN' */
        static int _callback_get_PN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PN' */
        static int _callback_set_PN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SN' */
        static int _callback_get_SN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SN' */
        static int _callback_set_SN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CC' */
        static int _callback_get_CC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CC' */
        static int _callback_set_CC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HE' */
        static int _callback_get_HE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HE' */
        static int _callback_set_HE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CT' */
        static int _callback_get_CT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CT' */
        static int _callback_set_CT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HW' */
        static int _callback_get_HW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HW' */
        static int _callback_set_HW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'B3' */
        static int _callback_get_B3(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'B3' */
        static int _callback_set_B3(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'B4' */
        static int _callback_get_B4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'B4' */
        static int _callback_set_B4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'B7' */
        static int _callback_get_B7(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'B7' */
        static int _callback_set_B7(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'HX' */
        static int _callback_get_HX(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'HX' */
        static int _callback_set_HX(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'FG' */
        static int _callback_get_FG(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'FG' */
        static int _callback_set_FG(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TS' */
        static int _callback_get_TS(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TS' */
        static int _callback_set_TS(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PR' */
        static int _callback_get_PR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PR' */
        static int _callback_set_PR(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VN' */
        static int _callback_get_VN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VN' */
        static int _callback_set_VN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_VINI_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _dr{};
        std::vector<uint8_t> _ce{};
        std::vector<uint8_t> _vz{};
        std::vector<uint8_t> _fn{};
        std::vector<uint8_t> _pn{};
        std::vector<uint8_t> _sn{};
        std::vector<uint8_t> _cc{};
        std::vector<uint8_t> _he{};
        std::vector<uint8_t> _ct{};
        std::vector<uint8_t> _hw{};
        std::vector<uint8_t> _b3{};
        std::vector<uint8_t> _b4{};
        std::vector<uint8_t> _b7{};
        std::vector<uint8_t> _hx{};
        std::vector<uint8_t> _fg{};
        std::vector<uint8_t> _ts{};
        std::vector<uint8_t> _pr{};
        std::vector<uint8_t> _vn{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

