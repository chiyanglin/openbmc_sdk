#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VW10/server.hpp>




namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VW10::VW10(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VW10_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VW10::VW10(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VW10(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VW10::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VW10::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VW10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VW10::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VW10_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VW10::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VW10::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VW10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VW10
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VW10::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VW10::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VW10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VW10::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VW10_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VW10::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VW10::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VW10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VW10
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VW10::gd() const ->
        std::vector<uint8_t>
{
    return _gd;
}

int VW10::_callback_get_GD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VW10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->gd();
                    }
                ));
    }
}

auto VW10::gd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_gd != value)
    {
        _gd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VW10_interface.property_changed("GD");
        }
    }

    return _gd;
}

auto VW10::gd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return gd(val, false);
}

int VW10::_callback_set_GD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VW10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->gd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VW10
{
static const auto _property_GD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VW10::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "GD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        gd(v, skipSignal);
        return;
    }
}

auto VW10::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "GD")
    {
        return gd();
    }

    return PropertiesVariant();
}


const vtable_t VW10::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VW10::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VW10::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("GD",
                     details::VW10::_property_GD
                        .data(),
                     _callback_get_GD,
                     _callback_set_GD,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

