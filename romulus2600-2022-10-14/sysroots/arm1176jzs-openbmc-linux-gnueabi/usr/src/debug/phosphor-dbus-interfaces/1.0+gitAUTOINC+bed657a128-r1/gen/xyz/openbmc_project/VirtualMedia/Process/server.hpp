#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

class Process
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Process() = delete;
        Process(const Process&) = delete;
        Process& operator=(const Process&) = delete;
        Process(Process&&) = delete;
        Process& operator=(Process&&) = delete;
        virtual ~Process() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Process(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                int32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Process(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Active */
        virtual bool active() const;
        /** Set value of Active with option to skip sending signal */
        virtual bool active(bool value,
               bool skipSignal);
        /** Set value of Active */
        virtual bool active(bool value);
        /** Get value of ExitCode */
        virtual int32_t exitCode() const;
        /** Set value of ExitCode with option to skip sending signal */
        virtual int32_t exitCode(int32_t value,
               bool skipSignal);
        /** Set value of ExitCode */
        virtual int32_t exitCode(int32_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_VirtualMedia_Process_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_VirtualMedia_Process_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Process";

    private:

        /** @brief sd-bus callback for get-property 'Active' */
        static int _callback_get_Active(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ExitCode' */
        static int _callback_get_ExitCode(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_VirtualMedia_Process_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _active{};
        int32_t _exitCode{};

};


} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

