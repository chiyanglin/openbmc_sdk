#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace BMC
{
namespace Error
{

struct MultiUserTargetFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.BMC.Error.MultiUserTargetFailure";
    static constexpr auto errDesc =
            "The systemd multi-user.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.BMC.Error.MultiUserTargetFailure: The systemd multi-user.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace BMC
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

