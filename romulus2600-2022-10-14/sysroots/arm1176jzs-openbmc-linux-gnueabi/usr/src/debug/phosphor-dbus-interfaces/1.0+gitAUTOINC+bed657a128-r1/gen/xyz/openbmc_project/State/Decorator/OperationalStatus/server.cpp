#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Decorator/OperationalStatus/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Decorator
{
namespace server
{

OperationalStatus::OperationalStatus(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Decorator_OperationalStatus_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

OperationalStatus::OperationalStatus(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : OperationalStatus(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto OperationalStatus::functional() const ->
        bool
{
    return _functional;
}

int OperationalStatus::_callback_get_Functional(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperationalStatus*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->functional();
                    }
                ));
    }
}

auto OperationalStatus::functional(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_functional != value)
    {
        _functional = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Decorator_OperationalStatus_interface.property_changed("Functional");
        }
    }

    return _functional;
}

auto OperationalStatus::functional(bool val) ->
        bool
{
    return functional(val, false);
}

int OperationalStatus::_callback_set_Functional(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperationalStatus*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->functional(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace OperationalStatus
{
static const auto _property_Functional =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void OperationalStatus::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Functional")
    {
        auto& v = std::get<bool>(val);
        functional(v, skipSignal);
        return;
    }
}

auto OperationalStatus::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Functional")
    {
        return functional();
    }

    return PropertiesVariant();
}


const vtable_t OperationalStatus::_vtable[] = {
    vtable::start(),
    vtable::property("Functional",
                     details::OperationalStatus::_property_Functional
                        .data(),
                     _callback_get_Functional,
                     _callback_set_Functional,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

