#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/ApplyTime/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

ApplyTime::ApplyTime(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_ApplyTime_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ApplyTime::ApplyTime(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ApplyTime(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ApplyTime::requestedApplyTime() const ->
        RequestedApplyTimes
{
    return _requestedApplyTime;
}

int ApplyTime::_callback_get_RequestedApplyTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ApplyTime*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->requestedApplyTime();
                    }
                ));
    }
}

auto ApplyTime::requestedApplyTime(RequestedApplyTimes value,
                                         bool skipSignal) ->
        RequestedApplyTimes
{
    if (_requestedApplyTime != value)
    {
        _requestedApplyTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_ApplyTime_interface.property_changed("RequestedApplyTime");
        }
    }

    return _requestedApplyTime;
}

auto ApplyTime::requestedApplyTime(RequestedApplyTimes val) ->
        RequestedApplyTimes
{
    return requestedApplyTime(val, false);
}

int ApplyTime::_callback_set_RequestedApplyTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ApplyTime*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](RequestedApplyTimes&& arg)
                    {
                        o->requestedApplyTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ApplyTime
{
static const auto _property_RequestedApplyTime =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Software::server::ApplyTime::RequestedApplyTimes>());
}
}

void ApplyTime::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RequestedApplyTime")
    {
        auto& v = std::get<RequestedApplyTimes>(val);
        requestedApplyTime(v, skipSignal);
        return;
    }
}

auto ApplyTime::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RequestedApplyTime")
    {
        return requestedApplyTime();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for ApplyTime::RequestedApplyTimes */
static const std::tuple<const char*, ApplyTime::RequestedApplyTimes> mappingApplyTimeRequestedApplyTimes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Software.ApplyTime.RequestedApplyTimes.Immediate",                 ApplyTime::RequestedApplyTimes::Immediate ),
            std::make_tuple( "xyz.openbmc_project.Software.ApplyTime.RequestedApplyTimes.OnReset",                 ApplyTime::RequestedApplyTimes::OnReset ),
        };

} // anonymous namespace

auto ApplyTime::convertStringToRequestedApplyTimes(const std::string& s) noexcept ->
        std::optional<RequestedApplyTimes>
{
    auto i = std::find_if(
            std::begin(mappingApplyTimeRequestedApplyTimes),
            std::end(mappingApplyTimeRequestedApplyTimes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingApplyTimeRequestedApplyTimes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto ApplyTime::convertRequestedApplyTimesFromString(const std::string& s) ->
        RequestedApplyTimes
{
    auto r = convertStringToRequestedApplyTimes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string ApplyTime::convertRequestedApplyTimesToString(ApplyTime::RequestedApplyTimes v)
{
    auto i = std::find_if(
            std::begin(mappingApplyTimeRequestedApplyTimes),
            std::end(mappingApplyTimeRequestedApplyTimes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingApplyTimeRequestedApplyTimes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t ApplyTime::_vtable[] = {
    vtable::start(),
    vtable::property("RequestedApplyTime",
                     details::ApplyTime::_property_RequestedApplyTime
                        .data(),
                     _callback_get_RequestedApplyTime,
                     _callback_set_RequestedApplyTime,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

