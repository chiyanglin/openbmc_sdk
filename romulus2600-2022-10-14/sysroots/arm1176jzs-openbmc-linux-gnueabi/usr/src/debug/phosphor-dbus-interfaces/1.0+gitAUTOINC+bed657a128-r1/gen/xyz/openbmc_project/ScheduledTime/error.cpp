#include <xyz/openbmc_project/ScheduledTime/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace ScheduledTime
{
namespace Error
{
const char* InvalidTime::name() const noexcept
{
    return errName;
}
const char* InvalidTime::description() const noexcept
{
    return errDesc;
}
const char* InvalidTime::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace ScheduledTime
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

