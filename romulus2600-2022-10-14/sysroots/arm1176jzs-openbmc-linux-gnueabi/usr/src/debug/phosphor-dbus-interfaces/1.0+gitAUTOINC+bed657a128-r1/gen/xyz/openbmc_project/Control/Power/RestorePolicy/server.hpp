#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

class RestorePolicy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        RestorePolicy() = delete;
        RestorePolicy(const RestorePolicy&) = delete;
        RestorePolicy& operator=(const RestorePolicy&) = delete;
        RestorePolicy(RestorePolicy&&) = delete;
        RestorePolicy& operator=(RestorePolicy&&) = delete;
        virtual ~RestorePolicy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        RestorePolicy(bus_t& bus, const char* path);

        enum class Policy
        {
            None,
            AlwaysOn,
            AlwaysOff,
            Restore,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Policy,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        RestorePolicy(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PowerRestorePolicy */
        virtual Policy powerRestorePolicy() const;
        /** Set value of PowerRestorePolicy with option to skip sending signal */
        virtual Policy powerRestorePolicy(Policy value,
               bool skipSignal);
        /** Set value of PowerRestorePolicy */
        virtual Policy powerRestorePolicy(Policy value);
        /** Get value of PowerRestoreDelay */
        virtual uint64_t powerRestoreDelay() const;
        /** Set value of PowerRestoreDelay with option to skip sending signal */
        virtual uint64_t powerRestoreDelay(uint64_t value,
               bool skipSignal);
        /** Set value of PowerRestoreDelay */
        virtual uint64_t powerRestoreDelay(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Power.RestorePolicy.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Policy convertPolicyFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Power.RestorePolicy.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Policy> convertStringToPolicy(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.Power.RestorePolicy.<value name>"
         */
        static std::string convertPolicyToString(Policy e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Power_RestorePolicy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Power_RestorePolicy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Power.RestorePolicy";

    private:

        /** @brief sd-bus callback for get-property 'PowerRestorePolicy' */
        static int _callback_get_PowerRestorePolicy(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerRestorePolicy' */
        static int _callback_set_PowerRestorePolicy(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PowerRestoreDelay' */
        static int _callback_get_PowerRestoreDelay(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerRestoreDelay' */
        static int _callback_set_PowerRestoreDelay(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Power_RestorePolicy_interface;
        sdbusplus::SdBusInterface *_intf;

        Policy _powerRestorePolicy = Policy::Restore;
        uint64_t _powerRestoreDelay = 0;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type RestorePolicy::Policy.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(RestorePolicy::Policy e)
{
    return RestorePolicy::convertPolicyToString(e);
}

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Control::Power::server::RestorePolicy::Policy>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::Power::server::RestorePolicy::convertStringToPolicy(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::Power::server::RestorePolicy::Policy>
{
    static std::string op(xyz::openbmc_project::Control::Power::server::RestorePolicy::Policy value)
    {
        return xyz::openbmc_project::Control::Power::server::RestorePolicy::convertPolicyToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

