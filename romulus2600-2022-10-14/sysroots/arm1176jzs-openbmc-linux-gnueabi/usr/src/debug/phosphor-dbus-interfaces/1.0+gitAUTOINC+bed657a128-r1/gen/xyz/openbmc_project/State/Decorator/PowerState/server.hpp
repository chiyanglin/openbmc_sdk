#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Decorator
{
namespace server
{

class PowerState
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PowerState() = delete;
        PowerState(const PowerState&) = delete;
        PowerState& operator=(const PowerState&) = delete;
        PowerState(PowerState&&) = delete;
        PowerState& operator=(PowerState&&) = delete;
        virtual ~PowerState() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PowerState(bus_t& bus, const char* path);

        enum class State
        {
            On,
            Off,
            PoweringOn,
            PoweringOff,
            Unknown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                State>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PowerState(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PowerState */
        virtual State powerState() const;
        /** Set value of PowerState with option to skip sending signal */
        virtual State powerState(State value,
               bool skipSignal);
        /** Set value of PowerState */
        virtual State powerState(State value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Decorator.PowerState.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static State convertStateFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.State.Decorator.PowerState.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<State> convertStringToState(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.State.Decorator.PowerState.<value name>"
         */
        static std::string convertStateToString(State e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_Decorator_PowerState_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_Decorator_PowerState_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.Decorator.PowerState";

    private:

        /** @brief sd-bus callback for get-property 'PowerState' */
        static int _callback_get_PowerState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PowerState' */
        static int _callback_set_PowerState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_Decorator_PowerState_interface;
        sdbusplus::SdBusInterface *_intf;

        State _powerState = State::Unknown;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PowerState::State.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PowerState::State e)
{
    return PowerState::convertStateToString(e);
}

} // namespace server
} // namespace Decorator
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::State::Decorator::server::PowerState::State>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::State::Decorator::server::PowerState::convertStringToState(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::State::Decorator::server::PowerState::State>
{
    static std::string op(xyz::openbmc_project::State::Decorator::server::PowerState::State value)
    {
        return xyz::openbmc_project::State::Decorator::server::PowerState::convertStateToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

