#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

class Version
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Version() = delete;
        Version(const Version&) = delete;
        Version& operator=(const Version&) = delete;
        Version(Version&&) = delete;
        Version& operator=(Version&&) = delete;
        virtual ~Version() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Version(bus_t& bus, const char* path);

        enum class VersionPurpose
        {
            Unknown,
            Other,
            System,
            BMC,
            Host,
            PSU,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                VersionPurpose,
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Version(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Version */
        virtual std::string version() const;
        /** Set value of Version with option to skip sending signal */
        virtual std::string version(std::string value,
               bool skipSignal);
        /** Set value of Version */
        virtual std::string version(std::string value);
        /** Get value of Purpose */
        virtual VersionPurpose purpose() const;
        /** Set value of Purpose with option to skip sending signal */
        virtual VersionPurpose purpose(VersionPurpose value,
               bool skipSignal);
        /** Set value of Purpose */
        virtual VersionPurpose purpose(VersionPurpose value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.Version.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static VersionPurpose convertVersionPurposeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.Version.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<VersionPurpose> convertStringToVersionPurpose(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Software.Version.<value name>"
         */
        static std::string convertVersionPurposeToString(VersionPurpose e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Software_Version_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Software_Version_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Software.Version";

    private:

        /** @brief sd-bus callback for get-property 'Version' */
        static int _callback_get_Version(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Version' */
        static int _callback_set_Version(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Purpose' */
        static int _callback_get_Purpose(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Purpose' */
        static int _callback_set_Purpose(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Software_Version_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _version{};
        VersionPurpose _purpose{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Version::VersionPurpose.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Version::VersionPurpose e)
{
    return Version::convertVersionPurposeToString(e);
}

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Software::server::Version::VersionPurpose>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Software::server::Version::convertStringToVersionPurpose(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Software::server::Version::VersionPurpose>
{
    static std::string op(xyz::openbmc_project::Software::server::Version::VersionPurpose value)
    {
        return xyz::openbmc_project::Software::server::Version::convertVersionPurposeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

