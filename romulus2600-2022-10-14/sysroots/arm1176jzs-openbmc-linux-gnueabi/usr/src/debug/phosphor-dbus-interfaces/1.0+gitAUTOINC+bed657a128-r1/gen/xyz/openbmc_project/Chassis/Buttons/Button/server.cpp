#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Buttons/Button/server.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace server
{

Button::Button(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Buttons_Button_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Button::Button(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Button(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Button::_callback_SimPress(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Button*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->simPress(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Button
{
static const auto _param_SimPress =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_SimPress =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Button::_callback_SimRelease(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Button*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->simRelease(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Button
{
static const auto _param_SimRelease =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_SimRelease =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Button::_callback_SimLongPress(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Button*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->simLongPress(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Button
{
static const auto _param_SimLongPress =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_SimLongPress =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


void Button::released(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_Button_interface;
    auto m = i.new_signal("Released");

    m.append();
    m.signal_send();
}

namespace details
{
namespace Button
{
static const auto _signal_Released =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

void Button::pressed(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_Button_interface;
    auto m = i.new_signal("Pressed");

    m.append();
    m.signal_send();
}

namespace details
{
namespace Button
{
static const auto _signal_Pressed =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

void Button::pressedLong(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_Button_interface;
    auto m = i.new_signal("PressedLong");

    m.append();
    m.signal_send();
}

namespace details
{
namespace Button
{
static const auto _signal_PressedLong =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


auto Button::enabled() const ->
        bool
{
    return _enabled;
}

int Button::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Button*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Button::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Buttons_Button_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto Button::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int Button::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Button*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Button
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Button::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
}

auto Button::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Enabled")
    {
        return enabled();
    }

    return PropertiesVariant();
}


const vtable_t Button::_vtable[] = {
    vtable::start(),

    vtable::method("simPress",
                   details::Button::_param_SimPress
                        .data(),
                   details::Button::_return_SimPress
                        .data(),
                   _callback_SimPress),

    vtable::method("simRelease",
                   details::Button::_param_SimRelease
                        .data(),
                   details::Button::_return_SimRelease
                        .data(),
                   _callback_SimRelease),

    vtable::method("simLongPress",
                   details::Button::_param_SimLongPress
                        .data(),
                   details::Button::_return_SimLongPress
                        .data(),
                   _callback_SimLongPress),

    vtable::signal("Released",
                   details::Button::_signal_Released
                        .data()),

    vtable::signal("Pressed",
                   details::Button::_signal_Pressed
                        .data()),

    vtable::signal("PressedLong",
                   details::Button::_signal_PressedLong
                        .data()),
    vtable::property("Enabled",
                     details::Button::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

