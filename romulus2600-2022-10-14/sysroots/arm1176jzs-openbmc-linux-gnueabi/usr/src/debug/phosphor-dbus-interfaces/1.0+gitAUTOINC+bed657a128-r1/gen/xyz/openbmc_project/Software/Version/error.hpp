#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace Version
{
namespace Error
{

struct Incompatible final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Version.Error.Incompatible";
    static constexpr auto errDesc =
            "A system component has a software version that is incompatible as determined by the implementation and needs to be updated. Some usage examples for this error include creating logging events and providing information on implementation reactions such as when the system is prevented from powering on if a minimum version level is not met.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Version.Error.Incompatible: A system component has a software version that is incompatible as determined by the implementation and needs to be updated. Some usage examples for this error include creating logging events and providing information on implementation reactions such as when the system is prevented from powering on if a minimum version level is not met.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct AlreadyExists final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Version.Error.AlreadyExists";
    static constexpr auto errDesc =
            "This image version already exists on the device";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Version.Error.AlreadyExists: This image version already exists on the device";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct InvalidSignature final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Software.Version.Error.InvalidSignature";
    static constexpr auto errDesc =
            "Signature Validation failed for image version.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Software.Version.Error.InvalidSignature: Signature Validation failed for image version.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Version
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

