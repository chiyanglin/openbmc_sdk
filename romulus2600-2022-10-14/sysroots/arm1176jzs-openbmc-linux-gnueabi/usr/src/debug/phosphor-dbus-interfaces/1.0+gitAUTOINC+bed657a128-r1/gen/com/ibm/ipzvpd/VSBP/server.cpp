#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VSBP/server.hpp>





namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VSBP::VSBP(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VSBP_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VSBP::VSBP(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VSBP(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VSBP::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VSBP::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VSBP::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSBP_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VSBP::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VSBP::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSBP
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSBP::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VSBP::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VSBP::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSBP_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VSBP::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VSBP::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSBP
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSBP::pa() const ->
        std::vector<uint8_t>
{
    return _pa;
}

int VSBP::_callback_get_PA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pa();
                    }
                ));
    }
}

auto VSBP::pa(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pa != value)
    {
        _pa = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSBP_interface.property_changed("PA");
        }
    }

    return _pa;
}

auto VSBP::pa(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pa(val, false);
}

int VSBP::_callback_set_PA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pa(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSBP
{
static const auto _property_PA =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSBP::im() const ->
        std::vector<uint8_t>
{
    return _im;
}

int VSBP::_callback_get_IM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->im();
                    }
                ));
    }
}

auto VSBP::im(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_im != value)
    {
        _im = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSBP_interface.property_changed("IM");
        }
    }

    return _im;
}

auto VSBP::im(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return im(val, false);
}

int VSBP::_callback_set_IM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSBP*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->im(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSBP
{
static const auto _property_IM =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VSBP::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "PA")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pa(v, skipSignal);
        return;
    }
    if (_name == "IM")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        im(v, skipSignal);
        return;
    }
}

auto VSBP::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "PA")
    {
        return pa();
    }
    if (_name == "IM")
    {
        return im();
    }

    return PropertiesVariant();
}


const vtable_t VSBP::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VSBP::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VSBP::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("PA",
                     details::VSBP::_property_PA
                        .data(),
                     _callback_get_PA,
                     _callback_set_PA,
                     vtable::property_::emits_change),
    vtable::property("IM",
                     details::VSBP::_property_IM
                        .data(),
                     _callback_get_IM,
                     _callback_set_IM,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

