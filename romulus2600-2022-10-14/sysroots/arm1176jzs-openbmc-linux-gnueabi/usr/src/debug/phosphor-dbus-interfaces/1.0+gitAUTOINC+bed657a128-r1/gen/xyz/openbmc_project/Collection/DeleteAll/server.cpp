#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Collection/DeleteAll/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Collection
{
namespace server
{

DeleteAll::DeleteAll(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Collection_DeleteAll_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int DeleteAll::_callback_DeleteAll(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<DeleteAll*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->deleteAll(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace DeleteAll
{
static const auto _param_DeleteAll =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_DeleteAll =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t DeleteAll::_vtable[] = {
    vtable::start(),

    vtable::method("DeleteAll",
                   details::DeleteAll::_param_DeleteAll
                        .data(),
                   details::DeleteAll::_return_DeleteAll
                        .data(),
                   _callback_DeleteAll),
    vtable::end()
};

} // namespace server
} // namespace Collection
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

