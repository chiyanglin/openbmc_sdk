#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/Location/server.hpp>


namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

Location::Location(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_Location_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Location::Location(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Location(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Location::locationCode() const ->
        std::string
{
    return _locationCode;
}

int Location::_callback_get_LocationCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Location*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->locationCode();
                    }
                ));
    }
}

auto Location::locationCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_locationCode != value)
    {
        _locationCode = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_Location_interface.property_changed("LocationCode");
        }
    }

    return _locationCode;
}

auto Location::locationCode(std::string val) ->
        std::string
{
    return locationCode(val, false);
}

int Location::_callback_set_LocationCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Location*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->locationCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Location
{
static const auto _property_LocationCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Location::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "LocationCode")
    {
        auto& v = std::get<std::string>(val);
        locationCode(v, skipSignal);
        return;
    }
}

auto Location::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "LocationCode")
    {
        return locationCode();
    }

    return PropertiesVariant();
}


const vtable_t Location::_vtable[] = {
    vtable::start(),
    vtable::property("LocationCode",
                     details::Location::_property_LocationCode
                        .data(),
                     _callback_get_LocationCode,
                     _callback_set_LocationCode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

