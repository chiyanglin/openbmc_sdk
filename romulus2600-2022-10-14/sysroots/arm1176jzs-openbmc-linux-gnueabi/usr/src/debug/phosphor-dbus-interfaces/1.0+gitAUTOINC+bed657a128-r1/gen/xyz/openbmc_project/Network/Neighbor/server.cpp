#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/Neighbor/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

Neighbor::Neighbor(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_Neighbor_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Neighbor::Neighbor(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Neighbor(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Neighbor::ipAddress() const ->
        std::string
{
    return _ipAddress;
}

int Neighbor::_callback_get_IPAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Neighbor*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ipAddress();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Neighbor::ipAddress(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ipAddress != value)
    {
        _ipAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Neighbor_interface.property_changed("IPAddress");
        }
    }

    return _ipAddress;
}

auto Neighbor::ipAddress(std::string val) ->
        std::string
{
    return ipAddress(val, false);
}

int Neighbor::_callback_set_IPAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Neighbor*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ipAddress(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Neighbor
{
static const auto _property_IPAddress =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Neighbor::macAddress() const ->
        std::string
{
    return _macAddress;
}

int Neighbor::_callback_get_MACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Neighbor*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->macAddress();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Neighbor::macAddress(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_macAddress != value)
    {
        _macAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Neighbor_interface.property_changed("MACAddress");
        }
    }

    return _macAddress;
}

auto Neighbor::macAddress(std::string val) ->
        std::string
{
    return macAddress(val, false);
}

int Neighbor::_callback_set_MACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Neighbor*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->macAddress(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Neighbor
{
static const auto _property_MACAddress =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Neighbor::state() const ->
        State
{
    return _state;
}

int Neighbor::_callback_get_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Neighbor*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->state();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Neighbor::state(State value,
                                         bool skipSignal) ->
        State
{
    if (_state != value)
    {
        _state = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Neighbor_interface.property_changed("State");
        }
    }

    return _state;
}

auto Neighbor::state(State val) ->
        State
{
    return state(val, false);
}

int Neighbor::_callback_set_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Neighbor*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](State&& arg)
                    {
                        o->state(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Neighbor
{
static const auto _property_State =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::server::Neighbor::State>());
}
}

void Neighbor::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "IPAddress")
    {
        auto& v = std::get<std::string>(val);
        ipAddress(v, skipSignal);
        return;
    }
    if (_name == "MACAddress")
    {
        auto& v = std::get<std::string>(val);
        macAddress(v, skipSignal);
        return;
    }
    if (_name == "State")
    {
        auto& v = std::get<State>(val);
        state(v, skipSignal);
        return;
    }
}

auto Neighbor::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "IPAddress")
    {
        return ipAddress();
    }
    if (_name == "MACAddress")
    {
        return macAddress();
    }
    if (_name == "State")
    {
        return state();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Neighbor::State */
static const std::tuple<const char*, Neighbor::State> mappingNeighborState[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.Neighbor.State.Incomplete",                 Neighbor::State::Incomplete ),
            std::make_tuple( "xyz.openbmc_project.Network.Neighbor.State.Reachable",                 Neighbor::State::Reachable ),
            std::make_tuple( "xyz.openbmc_project.Network.Neighbor.State.Stale",                 Neighbor::State::Stale ),
            std::make_tuple( "xyz.openbmc_project.Network.Neighbor.State.Invalid",                 Neighbor::State::Invalid ),
            std::make_tuple( "xyz.openbmc_project.Network.Neighbor.State.Permanent",                 Neighbor::State::Permanent ),
        };

} // anonymous namespace

auto Neighbor::convertStringToState(const std::string& s) noexcept ->
        std::optional<State>
{
    auto i = std::find_if(
            std::begin(mappingNeighborState),
            std::end(mappingNeighborState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingNeighborState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Neighbor::convertStateFromString(const std::string& s) ->
        State
{
    auto r = convertStringToState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Neighbor::convertStateToString(Neighbor::State v)
{
    auto i = std::find_if(
            std::begin(mappingNeighborState),
            std::end(mappingNeighborState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingNeighborState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Neighbor::_vtable[] = {
    vtable::start(),
    vtable::property("IPAddress",
                     details::Neighbor::_property_IPAddress
                        .data(),
                     _callback_get_IPAddress,
                     _callback_set_IPAddress,
                     vtable::property_::emits_change),
    vtable::property("MACAddress",
                     details::Neighbor::_property_MACAddress
                        .data(),
                     _callback_get_MACAddress,
                     _callback_set_MACAddress,
                     vtable::property_::emits_change),
    vtable::property("State",
                     details::Neighbor::_property_State
                        .data(),
                     _callback_get_State,
                     _callback_set_State,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

