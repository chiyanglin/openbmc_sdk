#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Power/Mode/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

Mode::Mode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Power_Mode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Mode::Mode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Mode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Mode::powerMode() const ->
        PowerMode
{
    return _powerMode;
}

int Mode::_callback_get_PowerMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerMode();
                    }
                ));
    }
}

auto Mode::powerMode(PowerMode value,
                                         bool skipSignal) ->
        PowerMode
{
    if (_powerMode != value)
    {
        _powerMode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Mode_interface.property_changed("PowerMode");
        }
    }

    return _powerMode;
}

auto Mode::powerMode(PowerMode val) ->
        PowerMode
{
    return powerMode(val, false);
}

int Mode::_callback_set_PowerMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](PowerMode&& arg)
                    {
                        o->powerMode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Mode
{
static const auto _property_PowerMode =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Power::server::Mode::PowerMode>());
}
}

auto Mode::safeMode() const ->
        bool
{
    return _safeMode;
}

int Mode::_callback_get_SafeMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->safeMode();
                    }
                ));
    }
}

auto Mode::safeMode(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_safeMode != value)
    {
        _safeMode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_Mode_interface.property_changed("SafeMode");
        }
    }

    return _safeMode;
}

auto Mode::safeMode(bool val) ->
        bool
{
    return safeMode(val, false);
}


namespace details
{
namespace Mode
{
static const auto _property_SafeMode =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Mode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PowerMode")
    {
        auto& v = std::get<PowerMode>(val);
        powerMode(v, skipSignal);
        return;
    }
    if (_name == "SafeMode")
    {
        auto& v = std::get<bool>(val);
        safeMode(v, skipSignal);
        return;
    }
}

auto Mode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PowerMode")
    {
        return powerMode();
    }
    if (_name == "SafeMode")
    {
        return safeMode();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Mode::PowerMode */
static const std::tuple<const char*, Mode::PowerMode> mappingModePowerMode[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Power.Mode.PowerMode.Static",                 Mode::PowerMode::Static ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.Mode.PowerMode.PowerSaving",                 Mode::PowerMode::PowerSaving ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.Mode.PowerMode.MaximumPerformance",                 Mode::PowerMode::MaximumPerformance ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.Mode.PowerMode.OEM",                 Mode::PowerMode::OEM ),
        };

} // anonymous namespace

auto Mode::convertStringToPowerMode(const std::string& s) noexcept ->
        std::optional<PowerMode>
{
    auto i = std::find_if(
            std::begin(mappingModePowerMode),
            std::end(mappingModePowerMode),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingModePowerMode) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Mode::convertPowerModeFromString(const std::string& s) ->
        PowerMode
{
    auto r = convertStringToPowerMode(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Mode::convertPowerModeToString(Mode::PowerMode v)
{
    auto i = std::find_if(
            std::begin(mappingModePowerMode),
            std::end(mappingModePowerMode),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingModePowerMode))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Mode::_vtable[] = {
    vtable::start(),
    vtable::property("PowerMode",
                     details::Mode::_property_PowerMode
                        .data(),
                     _callback_get_PowerMode,
                     _callback_set_PowerMode,
                     vtable::property_::emits_change),
    vtable::property("SafeMode",
                     details::Mode::_property_SafeMode
                        .data(),
                     _callback_get_SafeMode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

