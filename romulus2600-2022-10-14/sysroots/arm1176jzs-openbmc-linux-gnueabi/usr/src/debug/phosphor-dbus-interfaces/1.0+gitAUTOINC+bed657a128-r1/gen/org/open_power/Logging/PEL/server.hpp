#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#include <xyz/openbmc_project/Logging/Create/server.hpp>
#include <xyz/openbmc_project/Logging/Entry/server.hpp>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Logging
{
namespace server
{

class PEL
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PEL() = delete;
        PEL(const PEL&) = delete;
        PEL& operator=(const PEL&) = delete;
        PEL(PEL&&) = delete;
        PEL& operator=(PEL&&) = delete;
        virtual ~PEL() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PEL(bus_t& bus, const char* path);

        enum class RejectionReason
        {
            BadPEL,
            HostFull,
        };


        /** @brief Implementation for GetPEL
         *  Returns a file desciptor to a PEL.
         *
         *  @param[in] pelID - The PEL log ID of the PEL to retrieve.
         *
         *  @return data[sdbusplus::message::unix_fd] - A file descriptor for a file that contains the PEL.
         */
        virtual sdbusplus::message::unix_fd getPEL(
            uint32_t pelID) = 0;

        /** @brief Implementation for GetPELFromOBMCID
         *  Returns PEL data based on the OpenBMC event log ID.
         *
         *  @param[in] obmcLogID - The OpenBMC event log ID of the PEL to retrieve.
         *
         *  @return data[std::vector<uint8_t>] - The PEL data
         */
        virtual std::vector<uint8_t> getPELFromOBMCID(
            uint32_t obmcLogID) = 0;

        /** @brief Implementation for HostAck
         *  Notifies the PEL handler that the host (usually the OS) acked a PEL.
         *
         *  @param[in] pelID - The PEL log ID
         */
        virtual void hostAck(
            uint32_t pelID) = 0;

        /** @brief Implementation for HostReject
         *  Notifies the PEL handler that the host could not process a PEL.
         *
         *  @param[in] pelID - The PEL log ID
         *  @param[in] reason - The reason the PEL was rejected
         */
        virtual void hostReject(
            uint32_t pelID,
            RejectionReason reason) = 0;

        /** @brief Implementation for CreatePELWithFFDCFiles
         *  Creates an OpenBMC event log and a corresponding PEL. This method returns the IDs of the created PEL and OpenBMC event log, unlike the 'xyz.openbmc_project.Logging.Create.CreateWithFFDCFiles' method which doesn't return anything.
         *
         *  @param[in] message - The Message property of the OpenBMC event log entry. This is also the key into the PEL message registry.
         *  @param[in] severity - The Severity property of the event entry.
         *  @param[in] additionalData - The AdditionalData property of the event entry. e.g.:
  {
    "key1": "value1",
    "key2": "value2"
  }
ends up in AdditionaData like:
  ["KEY1=value1", "KEY2=value2"]
         *  @param[in] ffdc - File descriptors for any files containing FFDC, along with metadata about the contents:

  FFDCFormat- The format type of the contained data.
  subType - The format subtype, used for the 'Custom' type.
  version - The version of the data format, used for the 'Custom'
            type.
  unixfd - The file descriptor to the data file.

e.g.: [
  {"xyz.openbmc_project.Logging.Create.FFDCFormat.JSON", 0, 0, 5},
  {"xyz.openbmc_project.Logging.Create.FFDCFormat.Custom", 1, 2, 6}
]
         *
         *  @return iDs[std::tuple<uint32_t, uint32_t>] - The IDs of the 2 created logs:
  - OpenBMC event log ID
  - PEL log ID: The unique ID of the PEL
Note that the PEL's platform log ID (PLID) field is always equal to the PEL log ID for BMC created PELs.
         */
        virtual std::tuple<uint32_t, uint32_t> createPELWithFFDCFiles(
            std::string message,
            xyz::openbmc_project::Logging::server::Entry::Level severity,
            std::map<std::string, std::string> additionalData,
            std::vector<std::tuple<xyz::openbmc_project::Logging::server::Create::FFDCFormat, uint8_t, uint8_t, sdbusplus::message::unix_fd>> ffdc) = 0;

        /** @brief Implementation for GetPELIdFromBMCLogId
         *  Returns the PEL Id (aka Entry ID (EID)) based on the given BMC event log id.
         *
         *  @param[in] bmcLogId - The BMC event log id of the PEL to retrieve the PEL id.
         *
         *  @return pelId[uint32_t] - The Id of the PEL.
         */
        virtual uint32_t getPELIdFromBMCLogId(
            uint32_t bmcLogId) = 0;

        /** @brief Implementation for GetBMCLogIdFromPELId
         *  Returns the BMC event log id based on the given PEL id (aka Entry ID (EID)).
         *
         *  @param[in] pelId - The PEL id to retrieve the BMC event log id.
         *
         *  @return bmcLogId[uint32_t] - The BMC event log id of the PEL.
         */
        virtual uint32_t getBMCLogIdFromPELId(
            uint32_t pelId) = 0;

        /** @brief Implementation for GetPELJSON
         *  Returns a string containing the JSON representation of the PEL.
         *
         *  @param[in] bmcLogId - The BMC event log id of the PEL to retrieve JSON for
         *
         *  @return json[std::string] - The PEL in JSON format
         */
        virtual std::string getPELJSON(
            uint32_t bmcLogId) = 0;



        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "org.open_power.Logging.PEL.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static RejectionReason convertRejectionReasonFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "org.open_power.Logging.PEL.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<RejectionReason> convertStringToRejectionReason(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "org.open_power.Logging.PEL.<value name>"
         */
        static std::string convertRejectionReasonToString(RejectionReason e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _org_open_power_Logging_PEL_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _org_open_power_Logging_PEL_interface.emit_removed();
        }

        static constexpr auto interface = "org.open_power.Logging.PEL";

    private:

        /** @brief sd-bus callback for GetPEL
         */
        static int _callback_GetPEL(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetPELFromOBMCID
         */
        static int _callback_GetPELFromOBMCID(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for HostAck
         */
        static int _callback_HostAck(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for HostReject
         */
        static int _callback_HostReject(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for CreatePELWithFFDCFiles
         */
        static int _callback_CreatePELWithFFDCFiles(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetPELIdFromBMCLogId
         */
        static int _callback_GetPELIdFromBMCLogId(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetBMCLogIdFromPELId
         */
        static int _callback_GetBMCLogIdFromPELId(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetPELJSON
         */
        static int _callback_GetPELJSON(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _org_open_power_Logging_PEL_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PEL::RejectionReason.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PEL::RejectionReason e)
{
    return PEL::convertRejectionReasonToString(e);
}

} // namespace server
} // namespace Logging
} // namespace open_power
} // namespace org

namespace message::details
{
template <>
struct convert_from_string<org::open_power::Logging::server::PEL::RejectionReason>
{
    static auto op(const std::string& value) noexcept
    {
        return org::open_power::Logging::server::PEL::convertStringToRejectionReason(value);
    }
};

template <>
struct convert_to_string<org::open_power::Logging::server::PEL::RejectionReason>
{
    static std::string op(org::open_power::Logging::server::PEL::RejectionReason value)
    {
        return org::open_power::Logging::server::PEL::convertRejectionReasonToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

