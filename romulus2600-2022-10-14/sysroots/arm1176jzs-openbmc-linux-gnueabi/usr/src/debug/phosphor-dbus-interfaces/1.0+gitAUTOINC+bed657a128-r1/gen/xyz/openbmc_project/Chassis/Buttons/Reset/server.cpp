#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Buttons/Reset/server.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace server
{

Reset::Reset(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Buttons_Reset_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Reset::Reset(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Reset(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Reset::_callback_SimPress(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Reset*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->simPress(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Reset
{
static const auto _param_SimPress =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_SimPress =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


void Reset::released(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_Reset_interface;
    auto m = i.new_signal("Released");

    m.append();
    m.signal_send();
}

namespace details
{
namespace Reset
{
static const auto _signal_Released =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

void Reset::pressed(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_Reset_interface;
    auto m = i.new_signal("Pressed");

    m.append();
    m.signal_send();
}

namespace details
{
namespace Reset
{
static const auto _signal_Pressed =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


auto Reset::enabled() const ->
        bool
{
    return _enabled;
}

int Reset::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Reset*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Reset::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Buttons_Reset_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto Reset::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int Reset::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Reset*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Reset
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Reset::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
}

auto Reset::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Enabled")
    {
        return enabled();
    }

    return PropertiesVariant();
}


const vtable_t Reset::_vtable[] = {
    vtable::start(),

    vtable::method("simPress",
                   details::Reset::_param_SimPress
                        .data(),
                   details::Reset::_return_SimPress
                        .data(),
                   _callback_SimPress),

    vtable::signal("Released",
                   details::Reset::_signal_Released
                        .data()),

    vtable::signal("Pressed",
                   details::Reset::_signal_Pressed
                        .data()),
    vtable::property("Enabled",
                     details::Reset::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

