#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/intel/Control/OCOTShutdownPolicy/server.hpp>


namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Control
{
namespace server
{

OCOTShutdownPolicy::OCOTShutdownPolicy(bus_t& bus, const char* path)
        : _com_intel_Control_OCOTShutdownPolicy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

OCOTShutdownPolicy::OCOTShutdownPolicy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : OCOTShutdownPolicy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto OCOTShutdownPolicy::ocotPolicy() const ->
        Policy
{
    return _ocotPolicy;
}

int OCOTShutdownPolicy::_callback_get_OCOTPolicy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OCOTShutdownPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ocotPolicy();
                    }
                ));
    }
}

auto OCOTShutdownPolicy::ocotPolicy(Policy value,
                                         bool skipSignal) ->
        Policy
{
    if (_ocotPolicy != value)
    {
        _ocotPolicy = value;
        if (!skipSignal)
        {
            _com_intel_Control_OCOTShutdownPolicy_interface.property_changed("OCOTPolicy");
        }
    }

    return _ocotPolicy;
}

auto OCOTShutdownPolicy::ocotPolicy(Policy val) ->
        Policy
{
    return ocotPolicy(val, false);
}

int OCOTShutdownPolicy::_callback_set_OCOTPolicy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OCOTShutdownPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Policy&& arg)
                    {
                        o->ocotPolicy(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace OCOTShutdownPolicy
{
static const auto _property_OCOTPolicy =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::com::intel::Control::server::OCOTShutdownPolicy::Policy>());
}
}

void OCOTShutdownPolicy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "OCOTPolicy")
    {
        auto& v = std::get<Policy>(val);
        ocotPolicy(v, skipSignal);
        return;
    }
}

auto OCOTShutdownPolicy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "OCOTPolicy")
    {
        return ocotPolicy();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for OCOTShutdownPolicy::Policy */
static const std::tuple<const char*, OCOTShutdownPolicy::Policy> mappingOCOTShutdownPolicyPolicy[] =
        {
            std::make_tuple( "com.intel.Control.OCOTShutdownPolicy.Policy.NoShutdownOnOCOT",                 OCOTShutdownPolicy::Policy::NoShutdownOnOCOT ),
            std::make_tuple( "com.intel.Control.OCOTShutdownPolicy.Policy.ShutdownOnOCOT",                 OCOTShutdownPolicy::Policy::ShutdownOnOCOT ),
        };

} // anonymous namespace

auto OCOTShutdownPolicy::convertStringToPolicy(const std::string& s) noexcept ->
        std::optional<Policy>
{
    auto i = std::find_if(
            std::begin(mappingOCOTShutdownPolicyPolicy),
            std::end(mappingOCOTShutdownPolicyPolicy),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingOCOTShutdownPolicyPolicy) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto OCOTShutdownPolicy::convertPolicyFromString(const std::string& s) ->
        Policy
{
    auto r = convertStringToPolicy(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string OCOTShutdownPolicy::convertPolicyToString(OCOTShutdownPolicy::Policy v)
{
    auto i = std::find_if(
            std::begin(mappingOCOTShutdownPolicyPolicy),
            std::end(mappingOCOTShutdownPolicyPolicy),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingOCOTShutdownPolicyPolicy))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t OCOTShutdownPolicy::_vtable[] = {
    vtable::start(),
    vtable::property("OCOTPolicy",
                     details::OCOTShutdownPolicy::_property_OCOTPolicy
                        .data(),
                     _callback_get_OCOTPolicy,
                     _callback_set_OCOTPolicy,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace intel
} // namespace com
} // namespace sdbusplus

