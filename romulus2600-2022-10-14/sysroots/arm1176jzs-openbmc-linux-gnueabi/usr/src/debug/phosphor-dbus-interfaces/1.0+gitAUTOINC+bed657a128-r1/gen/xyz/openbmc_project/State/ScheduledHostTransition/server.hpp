#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#include <xyz/openbmc_project/State/Host/server.hpp>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

class ScheduledHostTransition
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        ScheduledHostTransition() = delete;
        ScheduledHostTransition(const ScheduledHostTransition&) = delete;
        ScheduledHostTransition& operator=(const ScheduledHostTransition&) = delete;
        ScheduledHostTransition(ScheduledHostTransition&&) = delete;
        ScheduledHostTransition& operator=(ScheduledHostTransition&&) = delete;
        virtual ~ScheduledHostTransition() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        ScheduledHostTransition(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint64_t,
                xyz::openbmc_project::State::server::Host::Transition>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        ScheduledHostTransition(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ScheduledTime */
        virtual uint64_t scheduledTime() const;
        /** Set value of ScheduledTime with option to skip sending signal */
        virtual uint64_t scheduledTime(uint64_t value,
               bool skipSignal);
        /** Set value of ScheduledTime */
        virtual uint64_t scheduledTime(uint64_t value);
        /** Get value of ScheduledTransition */
        virtual xyz::openbmc_project::State::server::Host::Transition scheduledTransition() const;
        /** Set value of ScheduledTransition with option to skip sending signal */
        virtual xyz::openbmc_project::State::server::Host::Transition scheduledTransition(xyz::openbmc_project::State::server::Host::Transition value,
               bool skipSignal);
        /** Set value of ScheduledTransition */
        virtual xyz::openbmc_project::State::server::Host::Transition scheduledTransition(xyz::openbmc_project::State::server::Host::Transition value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_State_ScheduledHostTransition_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_State_ScheduledHostTransition_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.State.ScheduledHostTransition";

    private:

        /** @brief sd-bus callback for get-property 'ScheduledTime' */
        static int _callback_get_ScheduledTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ScheduledTime' */
        static int _callback_set_ScheduledTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ScheduledTransition' */
        static int _callback_get_ScheduledTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ScheduledTransition' */
        static int _callback_set_ScheduledTransition(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_State_ScheduledHostTransition_interface;
        sdbusplus::SdBusInterface *_intf;

        uint64_t _scheduledTime = 0;
        xyz::openbmc_project::State::server::Host::Transition _scheduledTransition = xyz::openbmc_project::State::server::Host::Transition::On;

};


} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

