#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>













#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class CP00
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        CP00() = delete;
        CP00(const CP00&) = delete;
        CP00& operator=(const CP00&) = delete;
        CP00(CP00&&) = delete;
        CP00& operator=(CP00&&) = delete;
        virtual ~CP00() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        CP00(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        CP00(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of VD */
        virtual std::vector<uint8_t> vd() const;
        /** Set value of VD with option to skip sending signal */
        virtual std::vector<uint8_t> vd(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of VD */
        virtual std::vector<uint8_t> vd(std::vector<uint8_t> value);
        /** Get value of PG */
        virtual std::vector<uint8_t> pg() const;
        /** Set value of PG with option to skip sending signal */
        virtual std::vector<uint8_t> pg(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PG */
        virtual std::vector<uint8_t> pg(std::vector<uint8_t> value);
        /** Get value of MK */
        virtual std::vector<uint8_t> mk() const;
        /** Set value of MK with option to skip sending signal */
        virtual std::vector<uint8_t> mk(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of MK */
        virtual std::vector<uint8_t> mk(std::vector<uint8_t> value);
        /** Get value of PD_G */
        virtual std::vector<uint8_t> pdg() const;
        /** Set value of PD_G with option to skip sending signal */
        virtual std::vector<uint8_t> pdg(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PD_G */
        virtual std::vector<uint8_t> pdg(std::vector<uint8_t> value);
        /** Get value of PD_R */
        virtual std::vector<uint8_t> pdr() const;
        /** Set value of PD_R with option to skip sending signal */
        virtual std::vector<uint8_t> pdr(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PD_R */
        virtual std::vector<uint8_t> pdr(std::vector<uint8_t> value);
        /** Get value of SB */
        virtual std::vector<uint8_t> sb() const;
        /** Set value of SB with option to skip sending signal */
        virtual std::vector<uint8_t> sb(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of SB */
        virtual std::vector<uint8_t> sb(std::vector<uint8_t> value);
        /** Get value of PZ */
        virtual std::vector<uint8_t> pz() const;
        /** Set value of PZ with option to skip sending signal */
        virtual std::vector<uint8_t> pz(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PZ */
        virtual std::vector<uint8_t> pz(std::vector<uint8_t> value);
        /** Get value of AW */
        virtual std::vector<uint8_t> aw() const;
        /** Set value of AW with option to skip sending signal */
        virtual std::vector<uint8_t> aw(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of AW */
        virtual std::vector<uint8_t> aw(std::vector<uint8_t> value);
        /** Get value of PD_P */
        virtual std::vector<uint8_t> pdp() const;
        /** Set value of PD_P with option to skip sending signal */
        virtual std::vector<uint8_t> pdp(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PD_P */
        virtual std::vector<uint8_t> pdp(std::vector<uint8_t> value);
        /** Get value of D4 */
        virtual std::vector<uint8_t> d4() const;
        /** Set value of D4 with option to skip sending signal */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D4 */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value);
        /** Get value of D5 */
        virtual std::vector<uint8_t> d5() const;
        /** Set value of D5 with option to skip sending signal */
        virtual std::vector<uint8_t> d5(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D5 */
        virtual std::vector<uint8_t> d5(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_CP00_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_CP00_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.CP00";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VD' */
        static int _callback_get_VD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VD' */
        static int _callback_set_VD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PG' */
        static int _callback_get_PG(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PG' */
        static int _callback_set_PG(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MK' */
        static int _callback_get_MK(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MK' */
        static int _callback_set_MK(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PD_G' */
        static int _callback_get_PD_G(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PD_G' */
        static int _callback_set_PD_G(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PD_R' */
        static int _callback_get_PD_R(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PD_R' */
        static int _callback_set_PD_R(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SB' */
        static int _callback_get_SB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SB' */
        static int _callback_set_SB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PZ' */
        static int _callback_get_PZ(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PZ' */
        static int _callback_set_PZ(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AW' */
        static int _callback_get_AW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AW' */
        static int _callback_set_AW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PD_P' */
        static int _callback_get_PD_P(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PD_P' */
        static int _callback_set_PD_P(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D4' */
        static int _callback_get_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D4' */
        static int _callback_set_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D5' */
        static int _callback_get_D5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D5' */
        static int _callback_set_D5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_CP00_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _vd{};
        std::vector<uint8_t> _pg{};
        std::vector<uint8_t> _mk{};
        std::vector<uint8_t> _pdg{};
        std::vector<uint8_t> _pdr{};
        std::vector<uint8_t> _sb{};
        std::vector<uint8_t> _pz{};
        std::vector<uint8_t> _aw{};
        std::vector<uint8_t> _pdp{};
        std::vector<uint8_t> _d4{};
        std::vector<uint8_t> _d5{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

