#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VMPU/server.hpp>






namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VMPU::VMPU(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VMPU_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VMPU::VMPU(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VMPU(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VMPU::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VMPU::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VMPU::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMPU_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VMPU::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VMPU::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMPU
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VMPU::vz() const ->
        std::vector<uint8_t>
{
    return _vz;
}

int VMPU::_callback_get_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vz();
                    }
                ));
    }
}

auto VMPU::vz(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vz != value)
    {
        _vz = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMPU_interface.property_changed("VZ");
        }
    }

    return _vz;
}

auto VMPU::vz(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vz(val, false);
}

int VMPU::_callback_set_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMPU
{
static const auto _property_VZ =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VMPU::so() const ->
        std::vector<uint8_t>
{
    return _so;
}

int VMPU::_callback_get_SO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->so();
                    }
                ));
    }
}

auto VMPU::so(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_so != value)
    {
        _so = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMPU_interface.property_changed("SO");
        }
    }

    return _so;
}

auto VMPU::so(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return so(val, false);
}

int VMPU::_callback_set_SO(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->so(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMPU
{
static const auto _property_SO =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VMPU::di() const ->
        std::vector<uint8_t>
{
    return _di;
}

int VMPU::_callback_get_DI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->di();
                    }
                ));
    }
}

auto VMPU::di(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_di != value)
    {
        _di = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMPU_interface.property_changed("DI");
        }
    }

    return _di;
}

auto VMPU::di(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return di(val, false);
}

int VMPU::_callback_set_DI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->di(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMPU
{
static const auto _property_DI =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VMPU::in() const ->
        std::vector<uint8_t>
{
    return _in;
}

int VMPU::_callback_get_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->in();
                    }
                ));
    }
}

auto VMPU::in(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_in != value)
    {
        _in = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMPU_interface.property_changed("IN");
        }
    }

    return _in;
}

auto VMPU::in(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return in(val, false);
}

int VMPU::_callback_set_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMPU*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->in(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMPU
{
static const auto _property_IN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VMPU::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VZ")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vz(v, skipSignal);
        return;
    }
    if (_name == "SO")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        so(v, skipSignal);
        return;
    }
    if (_name == "DI")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        di(v, skipSignal);
        return;
    }
    if (_name == "IN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        in(v, skipSignal);
        return;
    }
}

auto VMPU::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VZ")
    {
        return vz();
    }
    if (_name == "SO")
    {
        return so();
    }
    if (_name == "DI")
    {
        return di();
    }
    if (_name == "IN")
    {
        return in();
    }

    return PropertiesVariant();
}


const vtable_t VMPU::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VMPU::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VZ",
                     details::VMPU::_property_VZ
                        .data(),
                     _callback_get_VZ,
                     _callback_set_VZ,
                     vtable::property_::emits_change),
    vtable::property("SO",
                     details::VMPU::_property_SO
                        .data(),
                     _callback_get_SO,
                     _callback_set_SO,
                     vtable::property_::emits_change),
    vtable::property("DI",
                     details::VMPU::_property_DI
                        .data(),
                     _callback_get_DI,
                     _callback_set_DI,
                     vtable::property_::emits_change),
    vtable::property("IN",
                     details::VMPU::_property_IN
                        .data(),
                     _callback_get_IN,
                     _callback_set_IN,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

