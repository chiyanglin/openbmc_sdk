#include <org/open_power/Host/Access/error.hpp>

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Host
{
namespace Access
{
namespace Error
{
const char* WriteCFAM::name() const noexcept
{
    return errName;
}
const char* WriteCFAM::description() const noexcept
{
    return errDesc;
}
const char* WriteCFAM::what() const noexcept
{
    return errWhat;
}
const char* ReadCFAM::name() const noexcept
{
    return errName;
}
const char* ReadCFAM::description() const noexcept
{
    return errDesc;
}
const char* ReadCFAM::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Access
} // namespace Host
} // namespace open_power
} // namespace org
} // namespace sdbusplus

