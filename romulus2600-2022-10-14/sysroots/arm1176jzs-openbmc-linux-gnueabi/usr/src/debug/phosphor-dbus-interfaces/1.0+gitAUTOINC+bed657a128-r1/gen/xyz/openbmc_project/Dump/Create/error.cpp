#include <xyz/openbmc_project/Dump/Create/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Create
{
namespace Error
{
const char* Disabled::name() const noexcept
{
    return errName;
}
const char* Disabled::description() const noexcept
{
    return errDesc;
}
const char* Disabled::what() const noexcept
{
    return errWhat;
}
const char* QuotaExceeded::name() const noexcept
{
    return errName;
}
const char* QuotaExceeded::description() const noexcept
{
    return errDesc;
}
const char* QuotaExceeded::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Create
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

