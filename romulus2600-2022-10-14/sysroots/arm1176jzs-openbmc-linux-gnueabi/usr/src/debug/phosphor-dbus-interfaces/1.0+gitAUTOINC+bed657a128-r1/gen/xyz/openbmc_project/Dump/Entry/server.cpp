#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Dump/Entry/server.hpp>

#include <xyz/openbmc_project/Common/File/error.hpp>
#include <xyz/openbmc_project/Common/File/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace server
{

Entry::Entry(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Dump_Entry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entry::Entry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Entry::_callback_InitiateOffload(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& offloadUri)
                    {
                        return o->initiateOffload(
                                offloadUri);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::File::Error::Open& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::File::Error::Write& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Entry
{
static const auto _param_InitiateOffload =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_InitiateOffload =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}



auto Entry::size() const ->
        uint64_t
{
    return _size;
}

int Entry::_callback_get_Size(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->size();
                    }
                ));
    }
}

auto Entry::size(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_size != value)
    {
        _size = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_interface.property_changed("Size");
        }
    }

    return _size;
}

auto Entry::size(uint64_t val) ->
        uint64_t
{
    return size(val, false);
}

int Entry::_callback_set_Size(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->size(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Size =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Entry::offloaded() const ->
        bool
{
    return _offloaded;
}

int Entry::_callback_get_Offloaded(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->offloaded();
                    }
                ));
    }
}

auto Entry::offloaded(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_offloaded != value)
    {
        _offloaded = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_interface.property_changed("Offloaded");
        }
    }

    return _offloaded;
}

auto Entry::offloaded(bool val) ->
        bool
{
    return offloaded(val, false);
}

int Entry::_callback_set_Offloaded(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->offloaded(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Offloaded =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Entry::offloadUri() const ->
        std::string
{
    return _offloadUri;
}

int Entry::_callback_get_OffloadUri(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->offloadUri();
                    }
                ));
    }
}

auto Entry::offloadUri(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_offloadUri != value)
    {
        _offloadUri = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_interface.property_changed("OffloadUri");
        }
    }

    return _offloadUri;
}

auto Entry::offloadUri(std::string val) ->
        std::string
{
    return offloadUri(val, false);
}

int Entry::_callback_set_OffloadUri(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->offloadUri(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_OffloadUri =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Entry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Size")
    {
        auto& v = std::get<uint64_t>(val);
        size(v, skipSignal);
        return;
    }
    if (_name == "Offloaded")
    {
        auto& v = std::get<bool>(val);
        offloaded(v, skipSignal);
        return;
    }
    if (_name == "OffloadUri")
    {
        auto& v = std::get<std::string>(val);
        offloadUri(v, skipSignal);
        return;
    }
}

auto Entry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Size")
    {
        return size();
    }
    if (_name == "Offloaded")
    {
        return offloaded();
    }
    if (_name == "OffloadUri")
    {
        return offloadUri();
    }

    return PropertiesVariant();
}


const vtable_t Entry::_vtable[] = {
    vtable::start(),

    vtable::method("InitiateOffload",
                   details::Entry::_param_InitiateOffload
                        .data(),
                   details::Entry::_return_InitiateOffload
                        .data(),
                   _callback_InitiateOffload),
    vtable::property("Size",
                     details::Entry::_property_Size
                        .data(),
                     _callback_get_Size,
                     _callback_set_Size,
                     vtable::property_::emits_change),
    vtable::property("Offloaded",
                     details::Entry::_property_Offloaded
                        .data(),
                     _callback_get_Offloaded,
                     _callback_set_Offloaded,
                     vtable::property_::emits_change),
    vtable::property("OffloadUri",
                     details::Entry::_property_OffloadUri
                        .data(),
                     _callback_get_OffloadUri,
                     _callback_set_OffloadUri,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

