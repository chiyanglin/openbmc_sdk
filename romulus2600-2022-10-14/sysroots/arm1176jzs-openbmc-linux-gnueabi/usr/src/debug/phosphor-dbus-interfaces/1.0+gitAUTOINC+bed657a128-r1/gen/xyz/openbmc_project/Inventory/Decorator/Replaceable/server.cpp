#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Replaceable/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Replaceable::Replaceable(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Replaceable_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Replaceable::Replaceable(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Replaceable(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Replaceable::fieldReplaceable() const ->
        bool
{
    return _fieldReplaceable;
}

int Replaceable::_callback_get_FieldReplaceable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Replaceable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fieldReplaceable();
                    }
                ));
    }
}

auto Replaceable::fieldReplaceable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_fieldReplaceable != value)
    {
        _fieldReplaceable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Replaceable_interface.property_changed("FieldReplaceable");
        }
    }

    return _fieldReplaceable;
}

auto Replaceable::fieldReplaceable(bool val) ->
        bool
{
    return fieldReplaceable(val, false);
}

int Replaceable::_callback_set_FieldReplaceable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Replaceable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->fieldReplaceable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Replaceable
{
static const auto _property_FieldReplaceable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Replaceable::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "FieldReplaceable")
    {
        auto& v = std::get<bool>(val);
        fieldReplaceable(v, skipSignal);
        return;
    }
}

auto Replaceable::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "FieldReplaceable")
    {
        return fieldReplaceable();
    }

    return PropertiesVariant();
}


const vtable_t Replaceable::_vtable[] = {
    vtable::start(),
    vtable::property("FieldReplaceable",
                     details::Replaceable::_property_FieldReplaceable
                        .data(),
                     _callback_get_FieldReplaceable,
                     _callback_set_FieldReplaceable,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

