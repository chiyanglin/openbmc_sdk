#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/Progress/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

Progress::Progress(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_Progress_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Progress::Progress(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Progress(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Progress::status() const ->
        OperationStatus
{
    return _status;
}

int Progress::_callback_get_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->status();
                    }
                ));
    }
}

auto Progress::status(OperationStatus value,
                                         bool skipSignal) ->
        OperationStatus
{
    if (_status != value)
    {
        _status = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_Progress_interface.property_changed("Status");
        }
    }

    return _status;
}

auto Progress::status(OperationStatus val) ->
        OperationStatus
{
    return status(val, false);
}

int Progress::_callback_set_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](OperationStatus&& arg)
                    {
                        o->status(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Progress
{
static const auto _property_Status =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Common::server::Progress::OperationStatus>());
}
}

auto Progress::startTime() const ->
        uint64_t
{
    return _startTime;
}

int Progress::_callback_get_StartTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->startTime();
                    }
                ));
    }
}

auto Progress::startTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_startTime != value)
    {
        _startTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_Progress_interface.property_changed("StartTime");
        }
    }

    return _startTime;
}

auto Progress::startTime(uint64_t val) ->
        uint64_t
{
    return startTime(val, false);
}

int Progress::_callback_set_StartTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->startTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Progress
{
static const auto _property_StartTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Progress::completedTime() const ->
        uint64_t
{
    return _completedTime;
}

int Progress::_callback_get_CompletedTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->completedTime();
                    }
                ));
    }
}

auto Progress::completedTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_completedTime != value)
    {
        _completedTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_Progress_interface.property_changed("CompletedTime");
        }
    }

    return _completedTime;
}

auto Progress::completedTime(uint64_t val) ->
        uint64_t
{
    return completedTime(val, false);
}

int Progress::_callback_set_CompletedTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Progress*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->completedTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Progress
{
static const auto _property_CompletedTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Progress::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Status")
    {
        auto& v = std::get<OperationStatus>(val);
        status(v, skipSignal);
        return;
    }
    if (_name == "StartTime")
    {
        auto& v = std::get<uint64_t>(val);
        startTime(v, skipSignal);
        return;
    }
    if (_name == "CompletedTime")
    {
        auto& v = std::get<uint64_t>(val);
        completedTime(v, skipSignal);
        return;
    }
}

auto Progress::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Status")
    {
        return status();
    }
    if (_name == "StartTime")
    {
        return startTime();
    }
    if (_name == "CompletedTime")
    {
        return completedTime();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Progress::OperationStatus */
static const std::tuple<const char*, Progress::OperationStatus> mappingProgressOperationStatus[] =
        {
            std::make_tuple( "xyz.openbmc_project.Common.Progress.OperationStatus.InProgress",                 Progress::OperationStatus::InProgress ),
            std::make_tuple( "xyz.openbmc_project.Common.Progress.OperationStatus.Completed",                 Progress::OperationStatus::Completed ),
            std::make_tuple( "xyz.openbmc_project.Common.Progress.OperationStatus.Failed",                 Progress::OperationStatus::Failed ),
            std::make_tuple( "xyz.openbmc_project.Common.Progress.OperationStatus.Aborted",                 Progress::OperationStatus::Aborted ),
        };

} // anonymous namespace

auto Progress::convertStringToOperationStatus(const std::string& s) noexcept ->
        std::optional<OperationStatus>
{
    auto i = std::find_if(
            std::begin(mappingProgressOperationStatus),
            std::end(mappingProgressOperationStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingProgressOperationStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Progress::convertOperationStatusFromString(const std::string& s) ->
        OperationStatus
{
    auto r = convertStringToOperationStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Progress::convertOperationStatusToString(Progress::OperationStatus v)
{
    auto i = std::find_if(
            std::begin(mappingProgressOperationStatus),
            std::end(mappingProgressOperationStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingProgressOperationStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Progress::_vtable[] = {
    vtable::start(),
    vtable::property("Status",
                     details::Progress::_property_Status
                        .data(),
                     _callback_get_Status,
                     _callback_set_Status,
                     vtable::property_::emits_change),
    vtable::property("StartTime",
                     details::Progress::_property_StartTime
                        .data(),
                     _callback_get_StartTime,
                     _callback_set_StartTime,
                     vtable::property_::emits_change),
    vtable::property("CompletedTime",
                     details::Progress::_property_CompletedTime
                        .data(),
                     _callback_get_CompletedTime,
                     _callback_set_CompletedTime,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

