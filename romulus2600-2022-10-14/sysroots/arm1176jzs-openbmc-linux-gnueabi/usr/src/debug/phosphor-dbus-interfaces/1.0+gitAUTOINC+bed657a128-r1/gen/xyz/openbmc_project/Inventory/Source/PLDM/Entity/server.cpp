#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Source/PLDM/Entity/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Source
{
namespace PLDM
{
namespace server
{

Entity::Entity(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entity::Entity(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entity(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Entity::entityType() const ->
        uint16_t
{
    return _entityType;
}

int Entity::_callback_get_EntityType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entity*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->entityType();
                    }
                ));
    }
}

auto Entity::entityType(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_entityType != value)
    {
        _entityType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface.property_changed("EntityType");
        }
    }

    return _entityType;
}

auto Entity::entityType(uint16_t val) ->
        uint16_t
{
    return entityType(val, false);
}

int Entity::_callback_set_EntityType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entity*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->entityType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entity
{
static const auto _property_EntityType =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Entity::entityInstanceNumber() const ->
        uint16_t
{
    return _entityInstanceNumber;
}

int Entity::_callback_get_EntityInstanceNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entity*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->entityInstanceNumber();
                    }
                ));
    }
}

auto Entity::entityInstanceNumber(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_entityInstanceNumber != value)
    {
        _entityInstanceNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface.property_changed("EntityInstanceNumber");
        }
    }

    return _entityInstanceNumber;
}

auto Entity::entityInstanceNumber(uint16_t val) ->
        uint16_t
{
    return entityInstanceNumber(val, false);
}

int Entity::_callback_set_EntityInstanceNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entity*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->entityInstanceNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entity
{
static const auto _property_EntityInstanceNumber =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Entity::containerID() const ->
        uint16_t
{
    return _containerID;
}

int Entity::_callback_get_ContainerID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entity*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->containerID();
                    }
                ));
    }
}

auto Entity::containerID(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_containerID != value)
    {
        _containerID = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_Entity_interface.property_changed("ContainerID");
        }
    }

    return _containerID;
}

auto Entity::containerID(uint16_t val) ->
        uint16_t
{
    return containerID(val, false);
}

int Entity::_callback_set_ContainerID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entity*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->containerID(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entity
{
static const auto _property_ContainerID =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

void Entity::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "EntityType")
    {
        auto& v = std::get<uint16_t>(val);
        entityType(v, skipSignal);
        return;
    }
    if (_name == "EntityInstanceNumber")
    {
        auto& v = std::get<uint16_t>(val);
        entityInstanceNumber(v, skipSignal);
        return;
    }
    if (_name == "ContainerID")
    {
        auto& v = std::get<uint16_t>(val);
        containerID(v, skipSignal);
        return;
    }
}

auto Entity::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "EntityType")
    {
        return entityType();
    }
    if (_name == "EntityInstanceNumber")
    {
        return entityInstanceNumber();
    }
    if (_name == "ContainerID")
    {
        return containerID();
    }

    return PropertiesVariant();
}


const vtable_t Entity::_vtable[] = {
    vtable::start(),
    vtable::property("EntityType",
                     details::Entity::_property_EntityType
                        .data(),
                     _callback_get_EntityType,
                     _callback_set_EntityType,
                     vtable::property_::emits_change),
    vtable::property("EntityInstanceNumber",
                     details::Entity::_property_EntityInstanceNumber
                        .data(),
                     _callback_get_EntityInstanceNumber,
                     _callback_set_EntityInstanceNumber,
                     vtable::property_::emits_change),
    vtable::property("ContainerID",
                     details::Entity::_property_ContainerID
                        .data(),
                     _callback_get_ContainerID,
                     _callback_set_ContainerID,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PLDM
} // namespace Source
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

