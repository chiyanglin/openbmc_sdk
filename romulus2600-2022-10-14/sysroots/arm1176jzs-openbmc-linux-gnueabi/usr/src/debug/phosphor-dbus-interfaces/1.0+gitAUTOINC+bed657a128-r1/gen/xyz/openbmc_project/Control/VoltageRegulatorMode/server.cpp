#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/VoltageRegulatorMode/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

VoltageRegulatorMode::VoltageRegulatorMode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_VoltageRegulatorMode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VoltageRegulatorMode::VoltageRegulatorMode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VoltageRegulatorMode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VoltageRegulatorMode::supported() const ->
        std::vector<std::string>
{
    return _supported;
}

int VoltageRegulatorMode::_callback_get_Supported(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->supported();
                    }
                ));
    }
}

auto VoltageRegulatorMode::supported(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_supported != value)
    {
        _supported = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_VoltageRegulatorMode_interface.property_changed("Supported");
        }
    }

    return _supported;
}

auto VoltageRegulatorMode::supported(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return supported(val, false);
}


namespace details
{
namespace VoltageRegulatorMode
{
static const auto _property_Supported =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto VoltageRegulatorMode::selected() const ->
        std::string
{
    return _selected;
}

int VoltageRegulatorMode::_callback_get_Selected(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->selected();
                    }
                ));
    }
}

auto VoltageRegulatorMode::selected(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_selected != value)
    {
        _selected = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_VoltageRegulatorMode_interface.property_changed("Selected");
        }
    }

    return _selected;
}

auto VoltageRegulatorMode::selected(std::string val) ->
        std::string
{
    return selected(val, false);
}

int VoltageRegulatorMode::_callback_set_Selected(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->selected(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VoltageRegulatorMode
{
static const auto _property_Selected =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void VoltageRegulatorMode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Supported")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        supported(v, skipSignal);
        return;
    }
    if (_name == "Selected")
    {
        auto& v = std::get<std::string>(val);
        selected(v, skipSignal);
        return;
    }
}

auto VoltageRegulatorMode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Supported")
    {
        return supported();
    }
    if (_name == "Selected")
    {
        return selected();
    }

    return PropertiesVariant();
}


const vtable_t VoltageRegulatorMode::_vtable[] = {
    vtable::start(),
    vtable::property("Supported",
                     details::VoltageRegulatorMode::_property_Supported
                        .data(),
                     _callback_get_Supported,
                     vtable::property_::emits_change),
    vtable::property("Selected",
                     details::VoltageRegulatorMode::_property_Selected
                        .data(),
                     _callback_get_Selected,
                     _callback_set_Selected,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

