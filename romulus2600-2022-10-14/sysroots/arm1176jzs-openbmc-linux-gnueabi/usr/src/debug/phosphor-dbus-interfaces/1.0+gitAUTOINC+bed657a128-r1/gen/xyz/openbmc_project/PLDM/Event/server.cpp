#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/PLDM/Event/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace server
{

Event::Event(bus_t& bus, const char* path)
        : _xyz_openbmc_project_PLDM_Event_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}



void Event::stateSensorEvent(
            uint8_t tid,
            uint16_t sensorID,
            uint8_t sensorOffset,
            uint8_t eventState,
            uint8_t previousEventState)
{
    auto& i = _xyz_openbmc_project_PLDM_Event_interface;
    auto m = i.new_signal("StateSensorEvent");

    m.append(tid, sensorID, sensorOffset, eventState, previousEventState);
    m.signal_send();
}

namespace details
{
namespace Event
{
static const auto _signal_StateSensorEvent =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint16_t, uint8_t, uint8_t, uint8_t>());
}
}



const vtable_t Event::_vtable[] = {
    vtable::start(),

    vtable::signal("StateSensorEvent",
                   details::Event::_signal_StateSensorEvent
                        .data()),
    vtable::end()
};

} // namespace server
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

