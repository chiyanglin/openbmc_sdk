#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/Control/TPM/SecurityKeys/server.hpp>


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Control
{
namespace TPM
{
namespace server
{

SecurityKeys::SecurityKeys(bus_t& bus, const char* path)
        : _org_open_power_Control_TPM_SecurityKeys_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SecurityKeys::SecurityKeys(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SecurityKeys(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SecurityKeys::clearHostSecurityKeys() const ->
        uint8_t
{
    return _clearHostSecurityKeys;
}

int SecurityKeys::_callback_get_ClearHostSecurityKeys(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityKeys*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->clearHostSecurityKeys();
                    }
                ));
    }
}

auto SecurityKeys::clearHostSecurityKeys(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_clearHostSecurityKeys != value)
    {
        _clearHostSecurityKeys = value;
        if (!skipSignal)
        {
            _org_open_power_Control_TPM_SecurityKeys_interface.property_changed("ClearHostSecurityKeys");
        }
    }

    return _clearHostSecurityKeys;
}

auto SecurityKeys::clearHostSecurityKeys(uint8_t val) ->
        uint8_t
{
    return clearHostSecurityKeys(val, false);
}

int SecurityKeys::_callback_set_ClearHostSecurityKeys(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityKeys*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->clearHostSecurityKeys(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SecurityKeys
{
static const auto _property_ClearHostSecurityKeys =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void SecurityKeys::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ClearHostSecurityKeys")
    {
        auto& v = std::get<uint8_t>(val);
        clearHostSecurityKeys(v, skipSignal);
        return;
    }
}

auto SecurityKeys::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ClearHostSecurityKeys")
    {
        return clearHostSecurityKeys();
    }

    return PropertiesVariant();
}


const vtable_t SecurityKeys::_vtable[] = {
    vtable::start(),
    vtable::property("ClearHostSecurityKeys",
                     details::SecurityKeys::_property_ClearHostSecurityKeys
                        .data(),
                     _callback_get_ClearHostSecurityKeys,
                     _callback_set_ClearHostSecurityKeys,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace TPM
} // namespace Control
} // namespace open_power
} // namespace org
} // namespace sdbusplus

