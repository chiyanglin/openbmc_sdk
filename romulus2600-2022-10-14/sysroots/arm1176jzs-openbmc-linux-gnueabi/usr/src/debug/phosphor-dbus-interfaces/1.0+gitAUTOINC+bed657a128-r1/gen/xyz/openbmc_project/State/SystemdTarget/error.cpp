#include <xyz/openbmc_project/State/SystemdTarget/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace SystemdTarget
{
namespace Error
{
const char* Failure::name() const noexcept
{
    return errName;
}
const char* Failure::description() const noexcept
{
    return errDesc;
}
const char* Failure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace SystemdTarget
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

