#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/I2CDevice/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

I2CDevice::I2CDevice(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_I2CDevice_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

I2CDevice::I2CDevice(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : I2CDevice(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto I2CDevice::bus() const ->
        size_t
{
    return _bus;
}

int I2CDevice::_callback_get_Bus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<I2CDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bus();
                    }
                ));
    }
}

auto I2CDevice::bus(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_bus != value)
    {
        _bus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_I2CDevice_interface.property_changed("Bus");
        }
    }

    return _bus;
}

auto I2CDevice::bus(size_t val) ->
        size_t
{
    return bus(val, false);
}

int I2CDevice::_callback_set_Bus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<I2CDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->bus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace I2CDevice
{
static const auto _property_Bus =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto I2CDevice::address() const ->
        size_t
{
    return _address;
}

int I2CDevice::_callback_get_Address(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<I2CDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->address();
                    }
                ));
    }
}

auto I2CDevice::address(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_address != value)
    {
        _address = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_I2CDevice_interface.property_changed("Address");
        }
    }

    return _address;
}

auto I2CDevice::address(size_t val) ->
        size_t
{
    return address(val, false);
}

int I2CDevice::_callback_set_Address(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<I2CDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->address(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace I2CDevice
{
static const auto _property_Address =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

void I2CDevice::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Bus")
    {
        auto& v = std::get<size_t>(val);
        bus(v, skipSignal);
        return;
    }
    if (_name == "Address")
    {
        auto& v = std::get<size_t>(val);
        address(v, skipSignal);
        return;
    }
}

auto I2CDevice::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Bus")
    {
        return bus();
    }
    if (_name == "Address")
    {
        return address();
    }

    return PropertiesVariant();
}


const vtable_t I2CDevice::_vtable[] = {
    vtable::start(),
    vtable::property("Bus",
                     details::I2CDevice::_property_Bus
                        .data(),
                     _callback_get_Bus,
                     _callback_set_Bus,
                     vtable::property_::emits_change),
    vtable::property("Address",
                     details::I2CDevice::_property_Address
                        .data(),
                     _callback_get_Address,
                     _callback_set_Address,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

