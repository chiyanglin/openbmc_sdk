#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Chassis
{
namespace Error
{

struct PowerOnFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Chassis.Error.PowerOnFailure";
    static constexpr auto errDesc =
            "The systemd obmc-chassis-poweron.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Chassis.Error.PowerOnFailure: The systemd obmc-chassis-poweron.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct PowerOffFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Chassis.Error.PowerOffFailure";
    static constexpr auto errDesc =
            "The systemd obmc-chassis-poweroff.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Chassis.Error.PowerOffFailure: The systemd obmc-chassis-poweroff.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Chassis
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

