#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/FilePath/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

FilePath::FilePath(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_FilePath_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FilePath::FilePath(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FilePath(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FilePath::path() const ->
        std::string
{
    return _path;
}

int FilePath::_callback_get_Path(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FilePath*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->path();
                    }
                ));
    }
}

auto FilePath::path(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_path != value)
    {
        _path = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_FilePath_interface.property_changed("Path");
        }
    }

    return _path;
}

auto FilePath::path(std::string val) ->
        std::string
{
    return path(val, false);
}

int FilePath::_callback_set_Path(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FilePath*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->path(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FilePath
{
static const auto _property_Path =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void FilePath::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Path")
    {
        auto& v = std::get<std::string>(val);
        path(v, skipSignal);
        return;
    }
}

auto FilePath::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Path")
    {
        return path();
    }

    return PropertiesVariant();
}


const vtable_t FilePath::_vtable[] = {
    vtable::start(),
    vtable::property("Path",
                     details::FilePath::_property_Path
                        .data(),
                     _callback_get_Path,
                     _callback_set_Path,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

