#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/LocationCode/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

LocationCode::LocationCode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_LocationCode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

LocationCode::LocationCode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : LocationCode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto LocationCode::locationCode() const ->
        std::string
{
    return _locationCode;
}

int LocationCode::_callback_get_LocationCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LocationCode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->locationCode();
                    }
                ));
    }
}

auto LocationCode::locationCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_locationCode != value)
    {
        _locationCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_LocationCode_interface.property_changed("LocationCode");
        }
    }

    return _locationCode;
}

auto LocationCode::locationCode(std::string val) ->
        std::string
{
    return locationCode(val, false);
}

int LocationCode::_callback_set_LocationCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LocationCode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->locationCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LocationCode
{
static const auto _property_LocationCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void LocationCode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "LocationCode")
    {
        auto& v = std::get<std::string>(val);
        locationCode(v, skipSignal);
        return;
    }
}

auto LocationCode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "LocationCode")
    {
        return locationCode();
    }

    return PropertiesVariant();
}


const vtable_t LocationCode::_vtable[] = {
    vtable::start(),
    vtable::property("LocationCode",
                     details::LocationCode::_property_LocationCode
                        .data(),
                     _callback_get_LocationCode,
                     _callback_set_LocationCode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

