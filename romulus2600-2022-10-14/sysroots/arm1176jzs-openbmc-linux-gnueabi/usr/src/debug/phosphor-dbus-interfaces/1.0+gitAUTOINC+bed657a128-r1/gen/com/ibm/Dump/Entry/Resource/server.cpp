#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/Dump/Entry/Resource/server.hpp>




namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace Entry
{
namespace server
{

Resource::Resource(bus_t& bus, const char* path)
        : _com_ibm_Dump_Entry_Resource_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Resource::Resource(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Resource(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Resource::sourceDumpId() const ->
        uint32_t
{
    return _sourceDumpId;
}

int Resource::_callback_get_SourceDumpId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Resource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sourceDumpId();
                    }
                ));
    }
}

auto Resource::sourceDumpId(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_sourceDumpId != value)
    {
        _sourceDumpId = value;
        if (!skipSignal)
        {
            _com_ibm_Dump_Entry_Resource_interface.property_changed("SourceDumpId");
        }
    }

    return _sourceDumpId;
}

auto Resource::sourceDumpId(uint32_t val) ->
        uint32_t
{
    return sourceDumpId(val, false);
}

int Resource::_callback_set_SourceDumpId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Resource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->sourceDumpId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Resource
{
static const auto _property_SourceDumpId =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Resource::vspString() const ->
        std::string
{
    return _vspString;
}

int Resource::_callback_get_VSPString(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Resource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vspString();
                    }
                ));
    }
}

auto Resource::vspString(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_vspString != value)
    {
        _vspString = value;
        if (!skipSignal)
        {
            _com_ibm_Dump_Entry_Resource_interface.property_changed("VSPString");
        }
    }

    return _vspString;
}

auto Resource::vspString(std::string val) ->
        std::string
{
    return vspString(val, false);
}

int Resource::_callback_set_VSPString(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Resource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->vspString(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Resource
{
static const auto _property_VSPString =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Resource::password() const ->
        std::string
{
    return _password;
}

int Resource::_callback_get_Password(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Resource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->password();
                    }
                ));
    }
}

auto Resource::password(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_password != value)
    {
        _password = value;
        if (!skipSignal)
        {
            _com_ibm_Dump_Entry_Resource_interface.property_changed("Password");
        }
    }

    return _password;
}

auto Resource::password(std::string val) ->
        std::string
{
    return password(val, false);
}

int Resource::_callback_set_Password(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Resource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->password(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Resource
{
static const auto _property_Password =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Resource::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SourceDumpId")
    {
        auto& v = std::get<uint32_t>(val);
        sourceDumpId(v, skipSignal);
        return;
    }
    if (_name == "VSPString")
    {
        auto& v = std::get<std::string>(val);
        vspString(v, skipSignal);
        return;
    }
    if (_name == "Password")
    {
        auto& v = std::get<std::string>(val);
        password(v, skipSignal);
        return;
    }
}

auto Resource::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SourceDumpId")
    {
        return sourceDumpId();
    }
    if (_name == "VSPString")
    {
        return vspString();
    }
    if (_name == "Password")
    {
        return password();
    }

    return PropertiesVariant();
}


const vtable_t Resource::_vtable[] = {
    vtable::start(),
    vtable::property("SourceDumpId",
                     details::Resource::_property_SourceDumpId
                        .data(),
                     _callback_get_SourceDumpId,
                     _callback_set_SourceDumpId,
                     vtable::property_::emits_change),
    vtable::property("VSPString",
                     details::Resource::_property_VSPString
                        .data(),
                     _callback_get_VSPString,
                     _callback_set_VSPString,
                     vtable::property_::emits_change),
    vtable::property("Password",
                     details::Resource::_property_Password
                        .data(),
                     _callback_get_Password,
                     _callback_set_Password,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace ibm
} // namespace com
} // namespace sdbusplus

