#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Boot/RebootAttempts/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace server
{

RebootAttempts::RebootAttempts(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Boot_RebootAttempts_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

RebootAttempts::RebootAttempts(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : RebootAttempts(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto RebootAttempts::attemptsLeft() const ->
        uint32_t
{
    return _attemptsLeft;
}

int RebootAttempts::_callback_get_AttemptsLeft(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RebootAttempts*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->attemptsLeft();
                    }
                ));
    }
}

auto RebootAttempts::attemptsLeft(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_attemptsLeft != value)
    {
        _attemptsLeft = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Boot_RebootAttempts_interface.property_changed("AttemptsLeft");
        }
    }

    return _attemptsLeft;
}

auto RebootAttempts::attemptsLeft(uint32_t val) ->
        uint32_t
{
    return attemptsLeft(val, false);
}

int RebootAttempts::_callback_set_AttemptsLeft(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RebootAttempts*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->attemptsLeft(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RebootAttempts
{
static const auto _property_AttemptsLeft =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto RebootAttempts::retryAttempts() const ->
        uint32_t
{
    return _retryAttempts;
}

int RebootAttempts::_callback_get_RetryAttempts(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RebootAttempts*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->retryAttempts();
                    }
                ));
    }
}

auto RebootAttempts::retryAttempts(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_retryAttempts != value)
    {
        _retryAttempts = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Boot_RebootAttempts_interface.property_changed("RetryAttempts");
        }
    }

    return _retryAttempts;
}

auto RebootAttempts::retryAttempts(uint32_t val) ->
        uint32_t
{
    return retryAttempts(val, false);
}

int RebootAttempts::_callback_set_RetryAttempts(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RebootAttempts*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->retryAttempts(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RebootAttempts
{
static const auto _property_RetryAttempts =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void RebootAttempts::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AttemptsLeft")
    {
        auto& v = std::get<uint32_t>(val);
        attemptsLeft(v, skipSignal);
        return;
    }
    if (_name == "RetryAttempts")
    {
        auto& v = std::get<uint32_t>(val);
        retryAttempts(v, skipSignal);
        return;
    }
}

auto RebootAttempts::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AttemptsLeft")
    {
        return attemptsLeft();
    }
    if (_name == "RetryAttempts")
    {
        return retryAttempts();
    }

    return PropertiesVariant();
}


const vtable_t RebootAttempts::_vtable[] = {
    vtable::start(),
    vtable::property("AttemptsLeft",
                     details::RebootAttempts::_property_AttemptsLeft
                        .data(),
                     _callback_get_AttemptsLeft,
                     _callback_set_AttemptsLeft,
                     vtable::property_::emits_change),
    vtable::property("RetryAttempts",
                     details::RebootAttempts::_property_RetryAttempts
                        .data(),
                     _callback_get_RetryAttempts,
                     _callback_set_RetryAttempts,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

