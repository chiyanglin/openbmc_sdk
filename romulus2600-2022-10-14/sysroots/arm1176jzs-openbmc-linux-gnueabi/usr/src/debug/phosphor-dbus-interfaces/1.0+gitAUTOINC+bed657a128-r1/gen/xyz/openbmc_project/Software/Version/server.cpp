#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/Version/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

Version::Version(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_Version_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Version::Version(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Version(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Version::version() const ->
        std::string
{
    return _version;
}

int Version::_callback_get_Version(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Version*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->version();
                    }
                ));
    }
}

auto Version::version(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_version != value)
    {
        _version = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_Version_interface.property_changed("Version");
        }
    }

    return _version;
}

auto Version::version(std::string val) ->
        std::string
{
    return version(val, false);
}

int Version::_callback_set_Version(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Version*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->version(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Version
{
static const auto _property_Version =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Version::purpose() const ->
        VersionPurpose
{
    return _purpose;
}

int Version::_callback_get_Purpose(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Version*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->purpose();
                    }
                ));
    }
}

auto Version::purpose(VersionPurpose value,
                                         bool skipSignal) ->
        VersionPurpose
{
    if (_purpose != value)
    {
        _purpose = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_Version_interface.property_changed("Purpose");
        }
    }

    return _purpose;
}

auto Version::purpose(VersionPurpose val) ->
        VersionPurpose
{
    return purpose(val, false);
}

int Version::_callback_set_Purpose(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Version*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](VersionPurpose&& arg)
                    {
                        o->purpose(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Version
{
static const auto _property_Purpose =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Software::server::Version::VersionPurpose>());
}
}

void Version::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Version")
    {
        auto& v = std::get<std::string>(val);
        version(v, skipSignal);
        return;
    }
    if (_name == "Purpose")
    {
        auto& v = std::get<VersionPurpose>(val);
        purpose(v, skipSignal);
        return;
    }
}

auto Version::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Version")
    {
        return version();
    }
    if (_name == "Purpose")
    {
        return purpose();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Version::VersionPurpose */
static const std::tuple<const char*, Version::VersionPurpose> mappingVersionVersionPurpose[] =
        {
            std::make_tuple( "xyz.openbmc_project.Software.Version.VersionPurpose.Unknown",                 Version::VersionPurpose::Unknown ),
            std::make_tuple( "xyz.openbmc_project.Software.Version.VersionPurpose.Other",                 Version::VersionPurpose::Other ),
            std::make_tuple( "xyz.openbmc_project.Software.Version.VersionPurpose.System",                 Version::VersionPurpose::System ),
            std::make_tuple( "xyz.openbmc_project.Software.Version.VersionPurpose.BMC",                 Version::VersionPurpose::BMC ),
            std::make_tuple( "xyz.openbmc_project.Software.Version.VersionPurpose.Host",                 Version::VersionPurpose::Host ),
            std::make_tuple( "xyz.openbmc_project.Software.Version.VersionPurpose.PSU",                 Version::VersionPurpose::PSU ),
        };

} // anonymous namespace

auto Version::convertStringToVersionPurpose(const std::string& s) noexcept ->
        std::optional<VersionPurpose>
{
    auto i = std::find_if(
            std::begin(mappingVersionVersionPurpose),
            std::end(mappingVersionVersionPurpose),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingVersionVersionPurpose) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Version::convertVersionPurposeFromString(const std::string& s) ->
        VersionPurpose
{
    auto r = convertStringToVersionPurpose(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Version::convertVersionPurposeToString(Version::VersionPurpose v)
{
    auto i = std::find_if(
            std::begin(mappingVersionVersionPurpose),
            std::end(mappingVersionVersionPurpose),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingVersionVersionPurpose))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Version::_vtable[] = {
    vtable::start(),
    vtable::property("Version",
                     details::Version::_property_Version
                        .data(),
                     _callback_get_Version,
                     _callback_set_Version,
                     vtable::property_::emits_change),
    vtable::property("Purpose",
                     details::Version::_property_Purpose
                        .data(),
                     _callback_get_Purpose,
                     _callback_set_Purpose,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

