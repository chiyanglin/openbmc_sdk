#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/OCC/Status/server.hpp>





namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace OCC
{
namespace server
{

Status::Status(bus_t& bus, const char* path)
        : _org_open_power_OCC_Status_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Status::Status(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Status(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Status::occActive() const ->
        bool
{
    return _occActive;
}

int Status::_callback_get_OccActive(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->occActive();
                    }
                ));
    }
}

auto Status::occActive(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_occActive != value)
    {
        _occActive = value;
        if (!skipSignal)
        {
            _org_open_power_OCC_Status_interface.property_changed("OccActive");
        }
    }

    return _occActive;
}

auto Status::occActive(bool val) ->
        bool
{
    return occActive(val, false);
}

int Status::_callback_set_OccActive(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->occActive(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_OccActive =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::throttleProcTemp() const ->
        bool
{
    return _throttleProcTemp;
}

int Status::_callback_get_ThrottleProcTemp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->throttleProcTemp();
                    }
                ));
    }
}

auto Status::throttleProcTemp(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_throttleProcTemp != value)
    {
        _throttleProcTemp = value;
        if (!skipSignal)
        {
            _org_open_power_OCC_Status_interface.property_changed("ThrottleProcTemp");
        }
    }

    return _throttleProcTemp;
}

auto Status::throttleProcTemp(bool val) ->
        bool
{
    return throttleProcTemp(val, false);
}

int Status::_callback_set_ThrottleProcTemp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->throttleProcTemp(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_ThrottleProcTemp =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::throttleProcPower() const ->
        bool
{
    return _throttleProcPower;
}

int Status::_callback_get_ThrottleProcPower(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->throttleProcPower();
                    }
                ));
    }
}

auto Status::throttleProcPower(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_throttleProcPower != value)
    {
        _throttleProcPower = value;
        if (!skipSignal)
        {
            _org_open_power_OCC_Status_interface.property_changed("ThrottleProcPower");
        }
    }

    return _throttleProcPower;
}

auto Status::throttleProcPower(bool val) ->
        bool
{
    return throttleProcPower(val, false);
}

int Status::_callback_set_ThrottleProcPower(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->throttleProcPower(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_ThrottleProcPower =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::throttleMemTemp() const ->
        bool
{
    return _throttleMemTemp;
}

int Status::_callback_get_ThrottleMemTemp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->throttleMemTemp();
                    }
                ));
    }
}

auto Status::throttleMemTemp(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_throttleMemTemp != value)
    {
        _throttleMemTemp = value;
        if (!skipSignal)
        {
            _org_open_power_OCC_Status_interface.property_changed("ThrottleMemTemp");
        }
    }

    return _throttleMemTemp;
}

auto Status::throttleMemTemp(bool val) ->
        bool
{
    return throttleMemTemp(val, false);
}

int Status::_callback_set_ThrottleMemTemp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->throttleMemTemp(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_ThrottleMemTemp =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Status::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "OccActive")
    {
        auto& v = std::get<bool>(val);
        occActive(v, skipSignal);
        return;
    }
    if (_name == "ThrottleProcTemp")
    {
        auto& v = std::get<bool>(val);
        throttleProcTemp(v, skipSignal);
        return;
    }
    if (_name == "ThrottleProcPower")
    {
        auto& v = std::get<bool>(val);
        throttleProcPower(v, skipSignal);
        return;
    }
    if (_name == "ThrottleMemTemp")
    {
        auto& v = std::get<bool>(val);
        throttleMemTemp(v, skipSignal);
        return;
    }
}

auto Status::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "OccActive")
    {
        return occActive();
    }
    if (_name == "ThrottleProcTemp")
    {
        return throttleProcTemp();
    }
    if (_name == "ThrottleProcPower")
    {
        return throttleProcPower();
    }
    if (_name == "ThrottleMemTemp")
    {
        return throttleMemTemp();
    }

    return PropertiesVariant();
}


const vtable_t Status::_vtable[] = {
    vtable::start(),
    vtable::property("OccActive",
                     details::Status::_property_OccActive
                        .data(),
                     _callback_get_OccActive,
                     _callback_set_OccActive,
                     vtable::property_::emits_change),
    vtable::property("ThrottleProcTemp",
                     details::Status::_property_ThrottleProcTemp
                        .data(),
                     _callback_get_ThrottleProcTemp,
                     _callback_set_ThrottleProcTemp,
                     vtable::property_::emits_change),
    vtable::property("ThrottleProcPower",
                     details::Status::_property_ThrottleProcPower
                        .data(),
                     _callback_get_ThrottleProcPower,
                     _callback_set_ThrottleProcPower,
                     vtable::property_::emits_change),
    vtable::property("ThrottleMemTemp",
                     details::Status::_property_ThrottleMemTemp
                        .data(),
                     _callback_get_ThrottleMemTemp,
                     _callback_set_ThrottleMemTemp,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace OCC
} // namespace open_power
} // namespace org
} // namespace sdbusplus

