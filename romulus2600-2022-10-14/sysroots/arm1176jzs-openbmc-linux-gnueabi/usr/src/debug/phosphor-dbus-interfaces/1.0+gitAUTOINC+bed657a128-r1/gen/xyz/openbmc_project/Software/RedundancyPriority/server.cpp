#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/RedundancyPriority/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

RedundancyPriority::RedundancyPriority(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_RedundancyPriority_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

RedundancyPriority::RedundancyPriority(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : RedundancyPriority(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto RedundancyPriority::priority() const ->
        uint8_t
{
    return _priority;
}

int RedundancyPriority::_callback_get_Priority(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RedundancyPriority*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->priority();
                    }
                ));
    }
}

auto RedundancyPriority::priority(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_priority != value)
    {
        _priority = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_RedundancyPriority_interface.property_changed("Priority");
        }
    }

    return _priority;
}

auto RedundancyPriority::priority(uint8_t val) ->
        uint8_t
{
    return priority(val, false);
}

int RedundancyPriority::_callback_set_Priority(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RedundancyPriority*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->priority(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RedundancyPriority
{
static const auto _property_Priority =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void RedundancyPriority::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Priority")
    {
        auto& v = std::get<uint8_t>(val);
        priority(v, skipSignal);
        return;
    }
}

auto RedundancyPriority::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Priority")
    {
        return priority();
    }

    return PropertiesVariant();
}


const vtable_t RedundancyPriority::_vtable[] = {
    vtable::start(),
    vtable::property("Priority",
                     details::RedundancyPriority::_property_Priority
                        .data(),
                     _callback_get_Priority,
                     _callback_set_Priority,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

