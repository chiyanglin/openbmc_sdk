#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Control
{
namespace server
{

class OCOTShutdownPolicy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        OCOTShutdownPolicy() = delete;
        OCOTShutdownPolicy(const OCOTShutdownPolicy&) = delete;
        OCOTShutdownPolicy& operator=(const OCOTShutdownPolicy&) = delete;
        OCOTShutdownPolicy(OCOTShutdownPolicy&&) = delete;
        OCOTShutdownPolicy& operator=(OCOTShutdownPolicy&&) = delete;
        virtual ~OCOTShutdownPolicy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        OCOTShutdownPolicy(bus_t& bus, const char* path);

        enum class Policy
        {
            NoShutdownOnOCOT,
            ShutdownOnOCOT,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                Policy>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        OCOTShutdownPolicy(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of OCOTPolicy */
        virtual Policy ocotPolicy() const;
        /** Set value of OCOTPolicy with option to skip sending signal */
        virtual Policy ocotPolicy(Policy value,
               bool skipSignal);
        /** Set value of OCOTPolicy */
        virtual Policy ocotPolicy(Policy value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.intel.Control.OCOTShutdownPolicy.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Policy convertPolicyFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "com.intel.Control.OCOTShutdownPolicy.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Policy> convertStringToPolicy(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "com.intel.Control.OCOTShutdownPolicy.<value name>"
         */
        static std::string convertPolicyToString(Policy e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _com_intel_Control_OCOTShutdownPolicy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_intel_Control_OCOTShutdownPolicy_interface.emit_removed();
        }

        static constexpr auto interface = "com.intel.Control.OCOTShutdownPolicy";

    private:

        /** @brief sd-bus callback for get-property 'OCOTPolicy' */
        static int _callback_get_OCOTPolicy(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'OCOTPolicy' */
        static int _callback_set_OCOTPolicy(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_intel_Control_OCOTShutdownPolicy_interface;
        sdbusplus::SdBusInterface *_intf;

        Policy _ocotPolicy = Policy::NoShutdownOnOCOT;

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type OCOTShutdownPolicy::Policy.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(OCOTShutdownPolicy::Policy e)
{
    return OCOTShutdownPolicy::convertPolicyToString(e);
}

} // namespace server
} // namespace Control
} // namespace intel
} // namespace com

namespace message::details
{
template <>
struct convert_from_string<com::intel::Control::server::OCOTShutdownPolicy::Policy>
{
    static auto op(const std::string& value) noexcept
    {
        return com::intel::Control::server::OCOTShutdownPolicy::convertStringToPolicy(value);
    }
};

template <>
struct convert_to_string<com::intel::Control::server::OCOTShutdownPolicy::Policy>
{
    static std::string op(com::intel::Control::server::OCOTShutdownPolicy::Policy value)
    {
        return com::intel::Control::server::OCOTShutdownPolicy::convertPolicyToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

