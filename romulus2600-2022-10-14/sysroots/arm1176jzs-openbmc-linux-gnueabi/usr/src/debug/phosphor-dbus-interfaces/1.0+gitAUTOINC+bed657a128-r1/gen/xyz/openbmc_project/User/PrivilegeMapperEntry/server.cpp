#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/PrivilegeMapperEntry/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

PrivilegeMapperEntry::PrivilegeMapperEntry(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_PrivilegeMapperEntry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PrivilegeMapperEntry::PrivilegeMapperEntry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PrivilegeMapperEntry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PrivilegeMapperEntry::groupName() const ->
        std::string
{
    return _groupName;
}

int PrivilegeMapperEntry::_callback_get_GroupName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PrivilegeMapperEntry*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->groupName();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::PrivilegeMappingExists& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto PrivilegeMapperEntry::groupName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_groupName != value)
    {
        _groupName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_PrivilegeMapperEntry_interface.property_changed("GroupName");
        }
    }

    return _groupName;
}

auto PrivilegeMapperEntry::groupName(std::string val) ->
        std::string
{
    return groupName(val, false);
}

int PrivilegeMapperEntry::_callback_set_GroupName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PrivilegeMapperEntry*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->groupName(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::PrivilegeMappingExists& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace PrivilegeMapperEntry
{
static const auto _property_GroupName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PrivilegeMapperEntry::privilege() const ->
        std::string
{
    return _privilege;
}

int PrivilegeMapperEntry::_callback_get_Privilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PrivilegeMapperEntry*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->privilege();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto PrivilegeMapperEntry::privilege(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_privilege != value)
    {
        _privilege = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_User_PrivilegeMapperEntry_interface.property_changed("Privilege");
        }
    }

    return _privilege;
}

auto PrivilegeMapperEntry::privilege(std::string val) ->
        std::string
{
    return privilege(val, false);
}

int PrivilegeMapperEntry::_callback_set_Privilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PrivilegeMapperEntry*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->privilege(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace PrivilegeMapperEntry
{
static const auto _property_Privilege =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void PrivilegeMapperEntry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "GroupName")
    {
        auto& v = std::get<std::string>(val);
        groupName(v, skipSignal);
        return;
    }
    if (_name == "Privilege")
    {
        auto& v = std::get<std::string>(val);
        privilege(v, skipSignal);
        return;
    }
}

auto PrivilegeMapperEntry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "GroupName")
    {
        return groupName();
    }
    if (_name == "Privilege")
    {
        return privilege();
    }

    return PropertiesVariant();
}


const vtable_t PrivilegeMapperEntry::_vtable[] = {
    vtable::start(),
    vtable::property("GroupName",
                     details::PrivilegeMapperEntry::_property_GroupName
                        .data(),
                     _callback_get_GroupName,
                     _callback_set_GroupName,
                     vtable::property_::emits_change),
    vtable::property("Privilege",
                     details::PrivilegeMapperEntry::_property_Privilege
                        .data(),
                     _callback_get_Privilege,
                     _callback_set_Privilege,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

