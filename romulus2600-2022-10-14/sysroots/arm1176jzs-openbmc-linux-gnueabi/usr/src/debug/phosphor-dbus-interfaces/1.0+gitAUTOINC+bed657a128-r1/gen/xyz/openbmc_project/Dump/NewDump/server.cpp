#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Dump/NewDump/server.hpp>

#include <xyz/openbmc_project/Dump/Create/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace server
{

NewDump::NewDump(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Dump_NewDump_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int NewDump::_callback_Notify(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<NewDump*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& sourceDumpId, uint64_t&& size)
                    {
                        return o->notify(
                                sourceDumpId, size);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Dump::Create::Error::Disabled& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace NewDump
{
static const auto _param_Notify =
        utility::tuple_to_array(message::types::type_id<
                uint32_t, uint64_t>());
static const auto _return_Notify =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t NewDump::_vtable[] = {
    vtable::start(),

    vtable::method("Notify",
                   details::NewDump::_param_Notify
                        .data(),
                   details::NewDump::_return_Notify
                        .data(),
                   _callback_Notify),
    vtable::end()
};

} // namespace server
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

