#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/MeetsMinimumShipLevel/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

MeetsMinimumShipLevel::MeetsMinimumShipLevel(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_MeetsMinimumShipLevel_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MeetsMinimumShipLevel::MeetsMinimumShipLevel(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MeetsMinimumShipLevel(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MeetsMinimumShipLevel::meetsMinimumShipLevel() const ->
        bool
{
    return _meetsMinimumShipLevel;
}

int MeetsMinimumShipLevel::_callback_get_MeetsMinimumShipLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MeetsMinimumShipLevel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->meetsMinimumShipLevel();
                    }
                ));
    }
}

auto MeetsMinimumShipLevel::meetsMinimumShipLevel(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_meetsMinimumShipLevel != value)
    {
        _meetsMinimumShipLevel = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_MeetsMinimumShipLevel_interface.property_changed("MeetsMinimumShipLevel");
        }
    }

    return _meetsMinimumShipLevel;
}

auto MeetsMinimumShipLevel::meetsMinimumShipLevel(bool val) ->
        bool
{
    return meetsMinimumShipLevel(val, false);
}

int MeetsMinimumShipLevel::_callback_set_MeetsMinimumShipLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MeetsMinimumShipLevel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->meetsMinimumShipLevel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MeetsMinimumShipLevel
{
static const auto _property_MeetsMinimumShipLevel =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void MeetsMinimumShipLevel::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MeetsMinimumShipLevel")
    {
        auto& v = std::get<bool>(val);
        meetsMinimumShipLevel(v, skipSignal);
        return;
    }
}

auto MeetsMinimumShipLevel::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MeetsMinimumShipLevel")
    {
        return meetsMinimumShipLevel();
    }

    return PropertiesVariant();
}


const vtable_t MeetsMinimumShipLevel::_vtable[] = {
    vtable::start(),
    vtable::property("MeetsMinimumShipLevel",
                     details::MeetsMinimumShipLevel::_property_MeetsMinimumShipLevel
                        .data(),
                     _callback_get_MeetsMinimumShipLevel,
                     _callback_set_MeetsMinimumShipLevel,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

