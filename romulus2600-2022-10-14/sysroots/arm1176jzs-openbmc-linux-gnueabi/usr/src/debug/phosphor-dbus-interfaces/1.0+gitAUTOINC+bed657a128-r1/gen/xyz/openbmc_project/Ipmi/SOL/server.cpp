#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Ipmi/SOL/server.hpp>










namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Ipmi
{
namespace server
{

SOL::SOL(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Ipmi_SOL_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SOL::SOL(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SOL(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SOL::progress() const ->
        uint8_t
{
    return _progress;
}

int SOL::_callback_get_Progress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->progress();
                    }
                ));
    }
}

auto SOL::progress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_progress != value)
    {
        _progress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("Progress");
        }
    }

    return _progress;
}

auto SOL::progress(uint8_t val) ->
        uint8_t
{
    return progress(val, false);
}

int SOL::_callback_set_Progress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->progress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_Progress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SOL::enable() const ->
        bool
{
    return _enable;
}

int SOL::_callback_get_Enable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enable();
                    }
                ));
    }
}

auto SOL::enable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enable != value)
    {
        _enable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("Enable");
        }
    }

    return _enable;
}

auto SOL::enable(bool val) ->
        bool
{
    return enable(val, false);
}

int SOL::_callback_set_Enable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_Enable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SOL::forceEncryption() const ->
        bool
{
    return _forceEncryption;
}

int SOL::_callback_get_ForceEncryption(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->forceEncryption();
                    }
                ));
    }
}

auto SOL::forceEncryption(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_forceEncryption != value)
    {
        _forceEncryption = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("ForceEncryption");
        }
    }

    return _forceEncryption;
}

auto SOL::forceEncryption(bool val) ->
        bool
{
    return forceEncryption(val, false);
}

int SOL::_callback_set_ForceEncryption(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->forceEncryption(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_ForceEncryption =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SOL::forceAuthentication() const ->
        bool
{
    return _forceAuthentication;
}

int SOL::_callback_get_ForceAuthentication(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->forceAuthentication();
                    }
                ));
    }
}

auto SOL::forceAuthentication(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_forceAuthentication != value)
    {
        _forceAuthentication = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("ForceAuthentication");
        }
    }

    return _forceAuthentication;
}

auto SOL::forceAuthentication(bool val) ->
        bool
{
    return forceAuthentication(val, false);
}

int SOL::_callback_set_ForceAuthentication(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->forceAuthentication(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_ForceAuthentication =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SOL::privilege() const ->
        uint8_t
{
    return _privilege;
}

int SOL::_callback_get_Privilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->privilege();
                    }
                ));
    }
}

auto SOL::privilege(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_privilege != value)
    {
        _privilege = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("Privilege");
        }
    }

    return _privilege;
}

auto SOL::privilege(uint8_t val) ->
        uint8_t
{
    return privilege(val, false);
}

int SOL::_callback_set_Privilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->privilege(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_Privilege =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SOL::accumulateIntervalMS() const ->
        uint8_t
{
    return _accumulateIntervalMS;
}

int SOL::_callback_get_AccumulateIntervalMS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->accumulateIntervalMS();
                    }
                ));
    }
}

auto SOL::accumulateIntervalMS(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_accumulateIntervalMS != value)
    {
        _accumulateIntervalMS = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("AccumulateIntervalMS");
        }
    }

    return _accumulateIntervalMS;
}

auto SOL::accumulateIntervalMS(uint8_t val) ->
        uint8_t
{
    return accumulateIntervalMS(val, false);
}

int SOL::_callback_set_AccumulateIntervalMS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->accumulateIntervalMS(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_AccumulateIntervalMS =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SOL::threshold() const ->
        uint8_t
{
    return _threshold;
}

int SOL::_callback_get_Threshold(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->threshold();
                    }
                ));
    }
}

auto SOL::threshold(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_threshold != value)
    {
        _threshold = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("Threshold");
        }
    }

    return _threshold;
}

auto SOL::threshold(uint8_t val) ->
        uint8_t
{
    return threshold(val, false);
}

int SOL::_callback_set_Threshold(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->threshold(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_Threshold =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SOL::retryCount() const ->
        uint8_t
{
    return _retryCount;
}

int SOL::_callback_get_RetryCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->retryCount();
                    }
                ));
    }
}

auto SOL::retryCount(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_retryCount != value)
    {
        _retryCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("RetryCount");
        }
    }

    return _retryCount;
}

auto SOL::retryCount(uint8_t val) ->
        uint8_t
{
    return retryCount(val, false);
}

int SOL::_callback_set_RetryCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->retryCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_RetryCount =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SOL::retryIntervalMS() const ->
        uint8_t
{
    return _retryIntervalMS;
}

int SOL::_callback_get_RetryIntervalMS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->retryIntervalMS();
                    }
                ));
    }
}

auto SOL::retryIntervalMS(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_retryIntervalMS != value)
    {
        _retryIntervalMS = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SOL_interface.property_changed("RetryIntervalMS");
        }
    }

    return _retryIntervalMS;
}

auto SOL::retryIntervalMS(uint8_t val) ->
        uint8_t
{
    return retryIntervalMS(val, false);
}

int SOL::_callback_set_RetryIntervalMS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SOL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->retryIntervalMS(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SOL
{
static const auto _property_RetryIntervalMS =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void SOL::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Progress")
    {
        auto& v = std::get<uint8_t>(val);
        progress(v, skipSignal);
        return;
    }
    if (_name == "Enable")
    {
        auto& v = std::get<bool>(val);
        enable(v, skipSignal);
        return;
    }
    if (_name == "ForceEncryption")
    {
        auto& v = std::get<bool>(val);
        forceEncryption(v, skipSignal);
        return;
    }
    if (_name == "ForceAuthentication")
    {
        auto& v = std::get<bool>(val);
        forceAuthentication(v, skipSignal);
        return;
    }
    if (_name == "Privilege")
    {
        auto& v = std::get<uint8_t>(val);
        privilege(v, skipSignal);
        return;
    }
    if (_name == "AccumulateIntervalMS")
    {
        auto& v = std::get<uint8_t>(val);
        accumulateIntervalMS(v, skipSignal);
        return;
    }
    if (_name == "Threshold")
    {
        auto& v = std::get<uint8_t>(val);
        threshold(v, skipSignal);
        return;
    }
    if (_name == "RetryCount")
    {
        auto& v = std::get<uint8_t>(val);
        retryCount(v, skipSignal);
        return;
    }
    if (_name == "RetryIntervalMS")
    {
        auto& v = std::get<uint8_t>(val);
        retryIntervalMS(v, skipSignal);
        return;
    }
}

auto SOL::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Progress")
    {
        return progress();
    }
    if (_name == "Enable")
    {
        return enable();
    }
    if (_name == "ForceEncryption")
    {
        return forceEncryption();
    }
    if (_name == "ForceAuthentication")
    {
        return forceAuthentication();
    }
    if (_name == "Privilege")
    {
        return privilege();
    }
    if (_name == "AccumulateIntervalMS")
    {
        return accumulateIntervalMS();
    }
    if (_name == "Threshold")
    {
        return threshold();
    }
    if (_name == "RetryCount")
    {
        return retryCount();
    }
    if (_name == "RetryIntervalMS")
    {
        return retryIntervalMS();
    }

    return PropertiesVariant();
}


const vtable_t SOL::_vtable[] = {
    vtable::start(),
    vtable::property("Progress",
                     details::SOL::_property_Progress
                        .data(),
                     _callback_get_Progress,
                     _callback_set_Progress,
                     vtable::property_::emits_change),
    vtable::property("Enable",
                     details::SOL::_property_Enable
                        .data(),
                     _callback_get_Enable,
                     _callback_set_Enable,
                     vtable::property_::emits_change),
    vtable::property("ForceEncryption",
                     details::SOL::_property_ForceEncryption
                        .data(),
                     _callback_get_ForceEncryption,
                     _callback_set_ForceEncryption,
                     vtable::property_::emits_change),
    vtable::property("ForceAuthentication",
                     details::SOL::_property_ForceAuthentication
                        .data(),
                     _callback_get_ForceAuthentication,
                     _callback_set_ForceAuthentication,
                     vtable::property_::emits_change),
    vtable::property("Privilege",
                     details::SOL::_property_Privilege
                        .data(),
                     _callback_get_Privilege,
                     _callback_set_Privilege,
                     vtable::property_::emits_change),
    vtable::property("AccumulateIntervalMS",
                     details::SOL::_property_AccumulateIntervalMS
                        .data(),
                     _callback_get_AccumulateIntervalMS,
                     _callback_set_AccumulateIntervalMS,
                     vtable::property_::emits_change),
    vtable::property("Threshold",
                     details::SOL::_property_Threshold
                        .data(),
                     _callback_get_Threshold,
                     _callback_set_Threshold,
                     vtable::property_::emits_change),
    vtable::property("RetryCount",
                     details::SOL::_property_RetryCount
                        .data(),
                     _callback_get_RetryCount,
                     _callback_set_RetryCount,
                     vtable::property_::emits_change),
    vtable::property("RetryIntervalMS",
                     details::SOL::_property_RetryIntervalMS
                        .data(),
                     _callback_get_RetryIntervalMS,
                     _callback_set_RetryIntervalMS,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Ipmi
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

