#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VINI/server.hpp>




















namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VINI::VINI(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VINI_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VINI::VINI(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VINI(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VINI::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VINI::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VINI::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VINI::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VINI::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VINI::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VINI::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VINI::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VINI::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::ce() const ->
        std::vector<uint8_t>
{
    return _ce;
}

int VINI::_callback_get_CE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ce();
                    }
                ));
    }
}

auto VINI::ce(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ce != value)
    {
        _ce = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("CE");
        }
    }

    return _ce;
}

auto VINI::ce(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ce(val, false);
}

int VINI::_callback_set_CE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ce(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_CE =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::vz() const ->
        std::vector<uint8_t>
{
    return _vz;
}

int VINI::_callback_get_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vz();
                    }
                ));
    }
}

auto VINI::vz(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vz != value)
    {
        _vz = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("VZ");
        }
    }

    return _vz;
}

auto VINI::vz(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vz(val, false);
}

int VINI::_callback_set_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_VZ =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::fn() const ->
        std::vector<uint8_t>
{
    return _fn;
}

int VINI::_callback_get_FN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fn();
                    }
                ));
    }
}

auto VINI::fn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_fn != value)
    {
        _fn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("FN");
        }
    }

    return _fn;
}

auto VINI::fn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return fn(val, false);
}

int VINI::_callback_set_FN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->fn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_FN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::pn() const ->
        std::vector<uint8_t>
{
    return _pn;
}

int VINI::_callback_get_PN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pn();
                    }
                ));
    }
}

auto VINI::pn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pn != value)
    {
        _pn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("PN");
        }
    }

    return _pn;
}

auto VINI::pn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pn(val, false);
}

int VINI::_callback_set_PN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_PN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::sn() const ->
        std::vector<uint8_t>
{
    return _sn;
}

int VINI::_callback_get_SN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sn();
                    }
                ));
    }
}

auto VINI::sn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_sn != value)
    {
        _sn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("SN");
        }
    }

    return _sn;
}

auto VINI::sn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return sn(val, false);
}

int VINI::_callback_set_SN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->sn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_SN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::cc() const ->
        std::vector<uint8_t>
{
    return _cc;
}

int VINI::_callback_get_CC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->cc();
                    }
                ));
    }
}

auto VINI::cc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_cc != value)
    {
        _cc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("CC");
        }
    }

    return _cc;
}

auto VINI::cc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return cc(val, false);
}

int VINI::_callback_set_CC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->cc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_CC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::he() const ->
        std::vector<uint8_t>
{
    return _he;
}

int VINI::_callback_get_HE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->he();
                    }
                ));
    }
}

auto VINI::he(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_he != value)
    {
        _he = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("HE");
        }
    }

    return _he;
}

auto VINI::he(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return he(val, false);
}

int VINI::_callback_set_HE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->he(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_HE =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::ct() const ->
        std::vector<uint8_t>
{
    return _ct;
}

int VINI::_callback_get_CT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ct();
                    }
                ));
    }
}

auto VINI::ct(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ct != value)
    {
        _ct = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("CT");
        }
    }

    return _ct;
}

auto VINI::ct(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ct(val, false);
}

int VINI::_callback_set_CT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ct(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_CT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::hw() const ->
        std::vector<uint8_t>
{
    return _hw;
}

int VINI::_callback_get_HW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hw();
                    }
                ));
    }
}

auto VINI::hw(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_hw != value)
    {
        _hw = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("HW");
        }
    }

    return _hw;
}

auto VINI::hw(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return hw(val, false);
}

int VINI::_callback_set_HW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->hw(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_HW =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::b3() const ->
        std::vector<uint8_t>
{
    return _b3;
}

int VINI::_callback_get_B3(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->b3();
                    }
                ));
    }
}

auto VINI::b3(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_b3 != value)
    {
        _b3 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("B3");
        }
    }

    return _b3;
}

auto VINI::b3(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return b3(val, false);
}

int VINI::_callback_set_B3(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->b3(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_B3 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::b4() const ->
        std::vector<uint8_t>
{
    return _b4;
}

int VINI::_callback_get_B4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->b4();
                    }
                ));
    }
}

auto VINI::b4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_b4 != value)
    {
        _b4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("B4");
        }
    }

    return _b4;
}

auto VINI::b4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return b4(val, false);
}

int VINI::_callback_set_B4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->b4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_B4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::b7() const ->
        std::vector<uint8_t>
{
    return _b7;
}

int VINI::_callback_get_B7(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->b7();
                    }
                ));
    }
}

auto VINI::b7(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_b7 != value)
    {
        _b7 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("B7");
        }
    }

    return _b7;
}

auto VINI::b7(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return b7(val, false);
}

int VINI::_callback_set_B7(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->b7(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_B7 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::hx() const ->
        std::vector<uint8_t>
{
    return _hx;
}

int VINI::_callback_get_HX(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hx();
                    }
                ));
    }
}

auto VINI::hx(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_hx != value)
    {
        _hx = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("HX");
        }
    }

    return _hx;
}

auto VINI::hx(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return hx(val, false);
}

int VINI::_callback_set_HX(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->hx(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_HX =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::fg() const ->
        std::vector<uint8_t>
{
    return _fg;
}

int VINI::_callback_get_FG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fg();
                    }
                ));
    }
}

auto VINI::fg(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_fg != value)
    {
        _fg = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("FG");
        }
    }

    return _fg;
}

auto VINI::fg(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return fg(val, false);
}

int VINI::_callback_set_FG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->fg(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_FG =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::ts() const ->
        std::vector<uint8_t>
{
    return _ts;
}

int VINI::_callback_get_TS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ts();
                    }
                ));
    }
}

auto VINI::ts(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ts != value)
    {
        _ts = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("TS");
        }
    }

    return _ts;
}

auto VINI::ts(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ts(val, false);
}

int VINI::_callback_set_TS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ts(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_TS =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::pr() const ->
        std::vector<uint8_t>
{
    return _pr;
}

int VINI::_callback_get_PR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pr();
                    }
                ));
    }
}

auto VINI::pr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pr != value)
    {
        _pr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("PR");
        }
    }

    return _pr;
}

auto VINI::pr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pr(val, false);
}

int VINI::_callback_set_PR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_PR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VINI::vn() const ->
        std::vector<uint8_t>
{
    return _vn;
}

int VINI::_callback_get_VN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vn();
                    }
                ));
    }
}

auto VINI::vn(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vn != value)
    {
        _vn = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VINI_interface.property_changed("VN");
        }
    }

    return _vn;
}

auto VINI::vn(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vn(val, false);
}

int VINI::_callback_set_VN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VINI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VINI
{
static const auto _property_VN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VINI::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "CE")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ce(v, skipSignal);
        return;
    }
    if (_name == "VZ")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vz(v, skipSignal);
        return;
    }
    if (_name == "FN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        fn(v, skipSignal);
        return;
    }
    if (_name == "PN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pn(v, skipSignal);
        return;
    }
    if (_name == "SN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        sn(v, skipSignal);
        return;
    }
    if (_name == "CC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        cc(v, skipSignal);
        return;
    }
    if (_name == "HE")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        he(v, skipSignal);
        return;
    }
    if (_name == "CT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ct(v, skipSignal);
        return;
    }
    if (_name == "HW")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        hw(v, skipSignal);
        return;
    }
    if (_name == "B3")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        b3(v, skipSignal);
        return;
    }
    if (_name == "B4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        b4(v, skipSignal);
        return;
    }
    if (_name == "B7")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        b7(v, skipSignal);
        return;
    }
    if (_name == "HX")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        hx(v, skipSignal);
        return;
    }
    if (_name == "FG")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        fg(v, skipSignal);
        return;
    }
    if (_name == "TS")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ts(v, skipSignal);
        return;
    }
    if (_name == "PR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pr(v, skipSignal);
        return;
    }
    if (_name == "VN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vn(v, skipSignal);
        return;
    }
}

auto VINI::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "CE")
    {
        return ce();
    }
    if (_name == "VZ")
    {
        return vz();
    }
    if (_name == "FN")
    {
        return fn();
    }
    if (_name == "PN")
    {
        return pn();
    }
    if (_name == "SN")
    {
        return sn();
    }
    if (_name == "CC")
    {
        return cc();
    }
    if (_name == "HE")
    {
        return he();
    }
    if (_name == "CT")
    {
        return ct();
    }
    if (_name == "HW")
    {
        return hw();
    }
    if (_name == "B3")
    {
        return b3();
    }
    if (_name == "B4")
    {
        return b4();
    }
    if (_name == "B7")
    {
        return b7();
    }
    if (_name == "HX")
    {
        return hx();
    }
    if (_name == "FG")
    {
        return fg();
    }
    if (_name == "TS")
    {
        return ts();
    }
    if (_name == "PR")
    {
        return pr();
    }
    if (_name == "VN")
    {
        return vn();
    }

    return PropertiesVariant();
}


const vtable_t VINI::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VINI::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VINI::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("CE",
                     details::VINI::_property_CE
                        .data(),
                     _callback_get_CE,
                     _callback_set_CE,
                     vtable::property_::emits_change),
    vtable::property("VZ",
                     details::VINI::_property_VZ
                        .data(),
                     _callback_get_VZ,
                     _callback_set_VZ,
                     vtable::property_::emits_change),
    vtable::property("FN",
                     details::VINI::_property_FN
                        .data(),
                     _callback_get_FN,
                     _callback_set_FN,
                     vtable::property_::emits_change),
    vtable::property("PN",
                     details::VINI::_property_PN
                        .data(),
                     _callback_get_PN,
                     _callback_set_PN,
                     vtable::property_::emits_change),
    vtable::property("SN",
                     details::VINI::_property_SN
                        .data(),
                     _callback_get_SN,
                     _callback_set_SN,
                     vtable::property_::emits_change),
    vtable::property("CC",
                     details::VINI::_property_CC
                        .data(),
                     _callback_get_CC,
                     _callback_set_CC,
                     vtable::property_::emits_change),
    vtable::property("HE",
                     details::VINI::_property_HE
                        .data(),
                     _callback_get_HE,
                     _callback_set_HE,
                     vtable::property_::emits_change),
    vtable::property("CT",
                     details::VINI::_property_CT
                        .data(),
                     _callback_get_CT,
                     _callback_set_CT,
                     vtable::property_::emits_change),
    vtable::property("HW",
                     details::VINI::_property_HW
                        .data(),
                     _callback_get_HW,
                     _callback_set_HW,
                     vtable::property_::emits_change),
    vtable::property("B3",
                     details::VINI::_property_B3
                        .data(),
                     _callback_get_B3,
                     _callback_set_B3,
                     vtable::property_::emits_change),
    vtable::property("B4",
                     details::VINI::_property_B4
                        .data(),
                     _callback_get_B4,
                     _callback_set_B4,
                     vtable::property_::emits_change),
    vtable::property("B7",
                     details::VINI::_property_B7
                        .data(),
                     _callback_get_B7,
                     _callback_set_B7,
                     vtable::property_::emits_change),
    vtable::property("HX",
                     details::VINI::_property_HX
                        .data(),
                     _callback_get_HX,
                     _callback_set_HX,
                     vtable::property_::emits_change),
    vtable::property("FG",
                     details::VINI::_property_FG
                        .data(),
                     _callback_get_FG,
                     _callback_set_FG,
                     vtable::property_::emits_change),
    vtable::property("TS",
                     details::VINI::_property_TS
                        .data(),
                     _callback_get_TS,
                     _callback_set_TS,
                     vtable::property_::emits_change),
    vtable::property("PR",
                     details::VINI::_property_PR
                        .data(),
                     _callback_get_PR,
                     _callback_set_PR,
                     vtable::property_::emits_change),
    vtable::property("VN",
                     details::VINI::_property_VN
                        .data(),
                     _callback_get_VN,
                     _callback_set_VN,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

