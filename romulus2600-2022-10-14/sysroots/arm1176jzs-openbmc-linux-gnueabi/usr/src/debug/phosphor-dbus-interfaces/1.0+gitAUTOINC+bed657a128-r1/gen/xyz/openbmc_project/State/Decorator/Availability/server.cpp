#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Decorator/Availability/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Decorator
{
namespace server
{

Availability::Availability(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Decorator_Availability_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Availability::Availability(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Availability(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Availability::available() const ->
        bool
{
    return _available;
}

int Availability::_callback_get_Available(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Availability*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->available();
                    }
                ));
    }
}

auto Availability::available(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_available != value)
    {
        _available = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Decorator_Availability_interface.property_changed("Available");
        }
    }

    return _available;
}

auto Availability::available(bool val) ->
        bool
{
    return available(val, false);
}

int Availability::_callback_set_Available(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Availability*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->available(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Availability
{
static const auto _property_Available =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Availability::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Available")
    {
        auto& v = std::get<bool>(val);
        available(v, skipSignal);
        return;
    }
}

auto Availability::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Available")
    {
        return available();
    }

    return PropertiesVariant();
}


const vtable_t Availability::_vtable[] = {
    vtable::start(),
    vtable::property("Available",
                     details::Availability::_property_Available
                        .data(),
                     _callback_get_Available,
                     _callback_set_Available,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

