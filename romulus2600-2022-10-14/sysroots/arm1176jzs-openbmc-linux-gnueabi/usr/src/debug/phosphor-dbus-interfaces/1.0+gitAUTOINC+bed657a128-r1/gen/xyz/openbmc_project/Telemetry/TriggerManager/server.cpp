#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Telemetry/TriggerManager/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

TriggerManager::TriggerManager(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Telemetry_TriggerManager_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int TriggerManager::_callback_AddTrigger(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<TriggerManager*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& id, std::string&& name, std::vector<xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction>&& triggerActions, std::vector<std::map<sdbusplus::message::object_path, std::string>>&& sensors, std::vector<sdbusplus::message::object_path>&& reports, std::variant<std::vector<std::tuple<xyz::openbmc_project::Telemetry::server::Trigger::Type, uint64_t, xyz::openbmc_project::Telemetry::server::Trigger::Direction, double>>, std::vector<std::tuple<std::string, xyz::openbmc_project::Telemetry::server::Trigger::Severity, uint64_t, std::string>>>&& thresholds)
                    {
                        return o->addTrigger(
                                id, name, triggerActions, sensors, reports, thresholds);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace TriggerManager
{
static const auto _param_AddTrigger =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string, std::vector<xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction>, std::vector<std::map<sdbusplus::message::object_path, std::string>>, std::vector<sdbusplus::message::object_path>, std::variant<std::vector<std::tuple<xyz::openbmc_project::Telemetry::server::Trigger::Type, uint64_t, xyz::openbmc_project::Telemetry::server::Trigger::Direction, double>>, std::vector<std::tuple<std::string, xyz::openbmc_project::Telemetry::server::Trigger::Severity, uint64_t, std::string>>>>());
static const auto _return_AddTrigger =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t TriggerManager::_vtable[] = {
    vtable::start(),

    vtable::method("AddTrigger",
                   details::TriggerManager::_param_AddTrigger
                        .data(),
                   details::TriggerManager::_return_AddTrigger
                        .data(),
                   _callback_AddTrigger),
    vtable::end()
};

} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

