#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Security/RestrictionMode/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Security
{
namespace server
{

RestrictionMode::RestrictionMode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Security_RestrictionMode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

RestrictionMode::RestrictionMode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : RestrictionMode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto RestrictionMode::restrictionMode() const ->
        Modes
{
    return _restrictionMode;
}

int RestrictionMode::_callback_get_RestrictionMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RestrictionMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->restrictionMode();
                    }
                ));
    }
}

auto RestrictionMode::restrictionMode(Modes value,
                                         bool skipSignal) ->
        Modes
{
    if (_restrictionMode != value)
    {
        _restrictionMode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Security_RestrictionMode_interface.property_changed("RestrictionMode");
        }
    }

    return _restrictionMode;
}

auto RestrictionMode::restrictionMode(Modes val) ->
        Modes
{
    return restrictionMode(val, false);
}

int RestrictionMode::_callback_set_RestrictionMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RestrictionMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Modes&& arg)
                    {
                        o->restrictionMode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RestrictionMode
{
static const auto _property_RestrictionMode =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Security::server::RestrictionMode::Modes>());
}
}

void RestrictionMode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RestrictionMode")
    {
        auto& v = std::get<Modes>(val);
        restrictionMode(v, skipSignal);
        return;
    }
}

auto RestrictionMode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RestrictionMode")
    {
        return restrictionMode();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for RestrictionMode::Modes */
static const std::tuple<const char*, RestrictionMode::Modes> mappingRestrictionModeModes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Security.RestrictionMode.Modes.None",                 RestrictionMode::Modes::None ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.RestrictionMode.Modes.Whitelist",                 RestrictionMode::Modes::Whitelist ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.RestrictionMode.Modes.Blacklist",                 RestrictionMode::Modes::Blacklist ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.RestrictionMode.Modes.Provisioning",                 RestrictionMode::Modes::Provisioning ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.RestrictionMode.Modes.ProvisionedHostWhitelist",                 RestrictionMode::Modes::ProvisionedHostWhitelist ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.RestrictionMode.Modes.ProvisionedHostDisabled",                 RestrictionMode::Modes::ProvisionedHostDisabled ),
        };

} // anonymous namespace

auto RestrictionMode::convertStringToModes(const std::string& s) noexcept ->
        std::optional<Modes>
{
    auto i = std::find_if(
            std::begin(mappingRestrictionModeModes),
            std::end(mappingRestrictionModeModes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingRestrictionModeModes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto RestrictionMode::convertModesFromString(const std::string& s) ->
        Modes
{
    auto r = convertStringToModes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string RestrictionMode::convertModesToString(RestrictionMode::Modes v)
{
    auto i = std::find_if(
            std::begin(mappingRestrictionModeModes),
            std::end(mappingRestrictionModeModes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingRestrictionModeModes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t RestrictionMode::_vtable[] = {
    vtable::start(),
    vtable::property("RestrictionMode",
                     details::RestrictionMode::_property_RestrictionMode
                        .data(),
                     _callback_get_RestrictionMode,
                     _callback_set_RestrictionMode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Security
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

