#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/Inventory/Decorator/Asset/server.hpp>


namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Asset::Asset(bus_t& bus, const char* path)
        : _org_open_power_Inventory_Decorator_Asset_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Asset::Asset(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Asset(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Asset::ccin() const ->
        std::string
{
    return _ccin;
}

int Asset::_callback_get_CCIN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ccin();
                    }
                ));
    }
}

auto Asset::ccin(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_ccin != value)
    {
        _ccin = value;
        if (!skipSignal)
        {
            _org_open_power_Inventory_Decorator_Asset_interface.property_changed("CCIN");
        }
    }

    return _ccin;
}

auto Asset::ccin(std::string val) ->
        std::string
{
    return ccin(val, false);
}

int Asset::_callback_set_CCIN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->ccin(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_CCIN =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Asset::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CCIN")
    {
        auto& v = std::get<std::string>(val);
        ccin(v, skipSignal);
        return;
    }
}

auto Asset::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CCIN")
    {
        return ccin();
    }

    return PropertiesVariant();
}


const vtable_t Asset::_vtable[] = {
    vtable::start(),
    vtable::property("CCIN",
                     details::Asset::_property_CCIN
                        .data(),
                     _callback_get_CCIN,
                     _callback_set_CCIN,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace open_power
} // namespace org
} // namespace sdbusplus

