#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace OCC
{
namespace server
{

class PassThrough
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PassThrough() = delete;
        PassThrough(const PassThrough&) = delete;
        PassThrough& operator=(const PassThrough&) = delete;
        PassThrough(PassThrough&&) = delete;
        PassThrough& operator=(PassThrough&&) = delete;
        virtual ~PassThrough() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PassThrough(bus_t& bus, const char* path);



        /** @brief Implementation for Send
         *  Pass through a command to the OCC.
         *
         *  @param[in] command - An array of integers representing the command and payload. This should still be bytes worth of data (as though using array[byte]), so each entry in the array should pack as many bytes as possible.
         *
         *  @return response[std::vector<int32_t>] - An array of integers representing the response. This should still be bytes worth of data (as though using array[byte]), so each entry in the array should pack as many bytes as possible.
         */
        virtual std::vector<int32_t> send(
            std::vector<int32_t> command) = 0;

        /** @brief Implementation for SetMode
         *  Change the power mode of the system.
         *
         *  @param[in] mode - Desired power mode of the system.
         *  @param[in] frequencyPoint - Frequency point required by some power modes.
         *
         *  @return status[bool] - Returns true if the mode change was accepted.
         */
        virtual bool setMode(
            uint8_t mode,
            uint16_t frequencyPoint) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _org_open_power_OCC_PassThrough_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _org_open_power_OCC_PassThrough_interface.emit_removed();
        }

        static constexpr auto interface = "org.open_power.OCC.PassThrough";

    private:

        /** @brief sd-bus callback for Send
         */
        static int _callback_Send(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SetMode
         */
        static int _callback_SetMode(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _org_open_power_OCC_PassThrough_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace OCC
} // namespace open_power
} // namespace org

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

