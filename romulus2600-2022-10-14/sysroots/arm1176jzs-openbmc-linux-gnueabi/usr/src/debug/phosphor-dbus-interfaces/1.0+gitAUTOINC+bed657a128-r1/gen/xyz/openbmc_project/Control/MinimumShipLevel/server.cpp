#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/MinimumShipLevel/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

MinimumShipLevel::MinimumShipLevel(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_MinimumShipLevel_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MinimumShipLevel::MinimumShipLevel(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MinimumShipLevel(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MinimumShipLevel::minimumShipLevelRequired() const ->
        bool
{
    return _minimumShipLevelRequired;
}

int MinimumShipLevel::_callback_get_MinimumShipLevelRequired(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MinimumShipLevel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minimumShipLevelRequired();
                    }
                ));
    }
}

auto MinimumShipLevel::minimumShipLevelRequired(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_minimumShipLevelRequired != value)
    {
        _minimumShipLevelRequired = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_MinimumShipLevel_interface.property_changed("MinimumShipLevelRequired");
        }
    }

    return _minimumShipLevelRequired;
}

auto MinimumShipLevel::minimumShipLevelRequired(bool val) ->
        bool
{
    return minimumShipLevelRequired(val, false);
}

int MinimumShipLevel::_callback_set_MinimumShipLevelRequired(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MinimumShipLevel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->minimumShipLevelRequired(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MinimumShipLevel
{
static const auto _property_MinimumShipLevelRequired =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void MinimumShipLevel::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MinimumShipLevelRequired")
    {
        auto& v = std::get<bool>(val);
        minimumShipLevelRequired(v, skipSignal);
        return;
    }
}

auto MinimumShipLevel::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MinimumShipLevelRequired")
    {
        return minimumShipLevelRequired();
    }

    return PropertiesVariant();
}


const vtable_t MinimumShipLevel::_vtable[] = {
    vtable::start(),
    vtable::property("MinimumShipLevelRequired",
                     details::MinimumShipLevel::_property_MinimumShipLevelRequired
                        .data(),
                     _callback_get_MinimumShipLevelRequired,
                     _callback_set_MinimumShipLevelRequired,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

