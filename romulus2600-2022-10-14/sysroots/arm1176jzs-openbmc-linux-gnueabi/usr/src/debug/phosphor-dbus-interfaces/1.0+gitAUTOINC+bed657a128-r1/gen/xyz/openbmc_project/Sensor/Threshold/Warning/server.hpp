#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

class Warning
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Warning() = delete;
        Warning(const Warning&) = delete;
        Warning& operator=(const Warning&) = delete;
        Warning(Warning&&) = delete;
        Warning& operator=(Warning&&) = delete;
        virtual ~Warning() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Warning(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Warning(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** @brief Send signal 'WarningHighAlarmAsserted'
         *
         *  The high threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void warningHighAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'WarningHighAlarmDeasserted'
         *
         *  The high threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void warningHighAlarmDeasserted(
            double sensorValue);

        /** @brief Send signal 'WarningLowAlarmAsserted'
         *
         *  The low threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void warningLowAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'WarningLowAlarmDeasserted'
         *
         *  The low threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void warningLowAlarmDeasserted(
            double sensorValue);

        /** Get value of WarningHigh */
        virtual double warningHigh() const;
        /** Set value of WarningHigh with option to skip sending signal */
        virtual double warningHigh(double value,
               bool skipSignal);
        /** Set value of WarningHigh */
        virtual double warningHigh(double value);
        /** Get value of WarningLow */
        virtual double warningLow() const;
        /** Set value of WarningLow with option to skip sending signal */
        virtual double warningLow(double value,
               bool skipSignal);
        /** Set value of WarningLow */
        virtual double warningLow(double value);
        /** Get value of WarningAlarmHigh */
        virtual bool warningAlarmHigh() const;
        /** Set value of WarningAlarmHigh with option to skip sending signal */
        virtual bool warningAlarmHigh(bool value,
               bool skipSignal);
        /** Set value of WarningAlarmHigh */
        virtual bool warningAlarmHigh(bool value);
        /** Get value of WarningAlarmLow */
        virtual bool warningAlarmLow() const;
        /** Set value of WarningAlarmLow with option to skip sending signal */
        virtual bool warningAlarmLow(bool value,
               bool skipSignal);
        /** Set value of WarningAlarmLow */
        virtual bool warningAlarmLow(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Sensor_Threshold_Warning_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Sensor_Threshold_Warning_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.Warning";

    private:

        /** @brief sd-bus callback for get-property 'WarningHigh' */
        static int _callback_get_WarningHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'WarningHigh' */
        static int _callback_set_WarningHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WarningLow' */
        static int _callback_get_WarningLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'WarningLow' */
        static int _callback_set_WarningLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WarningAlarmHigh' */
        static int _callback_get_WarningAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'WarningAlarmHigh' */
        static int _callback_set_WarningAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WarningAlarmLow' */
        static int _callback_get_WarningAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'WarningAlarmLow' */
        static int _callback_set_WarningAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Sensor_Threshold_Warning_interface;
        sdbusplus::SdBusInterface *_intf;

        double _warningHigh = std::numeric_limits<double>::quiet_NaN();
        double _warningLow = std::numeric_limits<double>::quiet_NaN();
        bool _warningAlarmHigh{};
        bool _warningAlarmLow{};

};


} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

