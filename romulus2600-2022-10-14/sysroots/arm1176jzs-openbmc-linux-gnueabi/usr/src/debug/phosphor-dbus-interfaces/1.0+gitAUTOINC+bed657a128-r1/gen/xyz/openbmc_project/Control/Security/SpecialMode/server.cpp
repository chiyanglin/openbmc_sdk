#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Security/SpecialMode/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Security
{
namespace server
{

SpecialMode::SpecialMode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Security_SpecialMode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SpecialMode::SpecialMode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SpecialMode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SpecialMode::specialMode() const ->
        Modes
{
    return _specialMode;
}

int SpecialMode::_callback_get_SpecialMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SpecialMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->specialMode();
                    }
                ));
    }
}

auto SpecialMode::specialMode(Modes value,
                                         bool skipSignal) ->
        Modes
{
    if (_specialMode != value)
    {
        _specialMode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Security_SpecialMode_interface.property_changed("SpecialMode");
        }
    }

    return _specialMode;
}

auto SpecialMode::specialMode(Modes val) ->
        Modes
{
    return specialMode(val, false);
}

int SpecialMode::_callback_set_SpecialMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SpecialMode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Modes&& arg)
                    {
                        o->specialMode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SpecialMode
{
static const auto _property_SpecialMode =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Security::server::SpecialMode::Modes>());
}
}

void SpecialMode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SpecialMode")
    {
        auto& v = std::get<Modes>(val);
        specialMode(v, skipSignal);
        return;
    }
}

auto SpecialMode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SpecialMode")
    {
        return specialMode();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for SpecialMode::Modes */
static const std::tuple<const char*, SpecialMode::Modes> mappingSpecialModeModes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Security.SpecialMode.Modes.None",                 SpecialMode::Modes::None ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.SpecialMode.Modes.Manufacturing",                 SpecialMode::Modes::Manufacturing ),
            std::make_tuple( "xyz.openbmc_project.Control.Security.SpecialMode.Modes.ValidationUnsecure",                 SpecialMode::Modes::ValidationUnsecure ),
        };

} // anonymous namespace

auto SpecialMode::convertStringToModes(const std::string& s) noexcept ->
        std::optional<Modes>
{
    auto i = std::find_if(
            std::begin(mappingSpecialModeModes),
            std::end(mappingSpecialModeModes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingSpecialModeModes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto SpecialMode::convertModesFromString(const std::string& s) ->
        Modes
{
    auto r = convertStringToModes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string SpecialMode::convertModesToString(SpecialMode::Modes v)
{
    auto i = std::find_if(
            std::begin(mappingSpecialModeModes),
            std::end(mappingSpecialModeModes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingSpecialModeModes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t SpecialMode::_vtable[] = {
    vtable::start(),
    vtable::property("SpecialMode",
                     details::SpecialMode::_property_SpecialMode
                        .data(),
                     _callback_get_SpecialMode,
                     _callback_set_SpecialMode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Security
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

