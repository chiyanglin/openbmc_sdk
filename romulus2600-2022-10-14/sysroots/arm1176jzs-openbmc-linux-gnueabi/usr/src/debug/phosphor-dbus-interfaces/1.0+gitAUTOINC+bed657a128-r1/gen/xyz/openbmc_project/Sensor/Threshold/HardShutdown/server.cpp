#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Threshold/HardShutdown/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

HardShutdown::HardShutdown(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

HardShutdown::HardShutdown(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : HardShutdown(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}



void HardShutdown::hardShutdownHighAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface;
    auto m = i.new_signal("HardShutdownHighAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace HardShutdown
{
static const auto _signal_HardShutdownHighAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void HardShutdown::hardShutdownHighAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface;
    auto m = i.new_signal("HardShutdownHighAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace HardShutdown
{
static const auto _signal_HardShutdownHighAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void HardShutdown::hardShutdownLowAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface;
    auto m = i.new_signal("HardShutdownLowAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace HardShutdown
{
static const auto _signal_HardShutdownLowAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void HardShutdown::hardShutdownLowAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface;
    auto m = i.new_signal("HardShutdownLowAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace HardShutdown
{
static const auto _signal_HardShutdownLowAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}


auto HardShutdown::hardShutdownHigh() const ->
        double
{
    return _hardShutdownHigh;
}

int HardShutdown::_callback_get_HardShutdownHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hardShutdownHigh();
                    }
                ));
    }
}

auto HardShutdown::hardShutdownHigh(double value,
                                         bool skipSignal) ->
        double
{
    if (_hardShutdownHigh != value)
    {
        _hardShutdownHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface.property_changed("HardShutdownHigh");
        }
    }

    return _hardShutdownHigh;
}

auto HardShutdown::hardShutdownHigh(double val) ->
        double
{
    return hardShutdownHigh(val, false);
}

int HardShutdown::_callback_set_HardShutdownHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->hardShutdownHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace HardShutdown
{
static const auto _property_HardShutdownHigh =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto HardShutdown::hardShutdownLow() const ->
        double
{
    return _hardShutdownLow;
}

int HardShutdown::_callback_get_HardShutdownLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hardShutdownLow();
                    }
                ));
    }
}

auto HardShutdown::hardShutdownLow(double value,
                                         bool skipSignal) ->
        double
{
    if (_hardShutdownLow != value)
    {
        _hardShutdownLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface.property_changed("HardShutdownLow");
        }
    }

    return _hardShutdownLow;
}

auto HardShutdown::hardShutdownLow(double val) ->
        double
{
    return hardShutdownLow(val, false);
}

int HardShutdown::_callback_set_HardShutdownLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->hardShutdownLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace HardShutdown
{
static const auto _property_HardShutdownLow =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto HardShutdown::hardShutdownAlarmHigh() const ->
        bool
{
    return _hardShutdownAlarmHigh;
}

int HardShutdown::_callback_get_HardShutdownAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hardShutdownAlarmHigh();
                    }
                ));
    }
}

auto HardShutdown::hardShutdownAlarmHigh(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hardShutdownAlarmHigh != value)
    {
        _hardShutdownAlarmHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface.property_changed("HardShutdownAlarmHigh");
        }
    }

    return _hardShutdownAlarmHigh;
}

auto HardShutdown::hardShutdownAlarmHigh(bool val) ->
        bool
{
    return hardShutdownAlarmHigh(val, false);
}

int HardShutdown::_callback_set_HardShutdownAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hardShutdownAlarmHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace HardShutdown
{
static const auto _property_HardShutdownAlarmHigh =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto HardShutdown::hardShutdownAlarmLow() const ->
        bool
{
    return _hardShutdownAlarmLow;
}

int HardShutdown::_callback_get_HardShutdownAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hardShutdownAlarmLow();
                    }
                ));
    }
}

auto HardShutdown::hardShutdownAlarmLow(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hardShutdownAlarmLow != value)
    {
        _hardShutdownAlarmLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_HardShutdown_interface.property_changed("HardShutdownAlarmLow");
        }
    }

    return _hardShutdownAlarmLow;
}

auto HardShutdown::hardShutdownAlarmLow(bool val) ->
        bool
{
    return hardShutdownAlarmLow(val, false);
}

int HardShutdown::_callback_set_HardShutdownAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HardShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hardShutdownAlarmLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace HardShutdown
{
static const auto _property_HardShutdownAlarmLow =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void HardShutdown::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "HardShutdownHigh")
    {
        auto& v = std::get<double>(val);
        hardShutdownHigh(v, skipSignal);
        return;
    }
    if (_name == "HardShutdownLow")
    {
        auto& v = std::get<double>(val);
        hardShutdownLow(v, skipSignal);
        return;
    }
    if (_name == "HardShutdownAlarmHigh")
    {
        auto& v = std::get<bool>(val);
        hardShutdownAlarmHigh(v, skipSignal);
        return;
    }
    if (_name == "HardShutdownAlarmLow")
    {
        auto& v = std::get<bool>(val);
        hardShutdownAlarmLow(v, skipSignal);
        return;
    }
}

auto HardShutdown::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "HardShutdownHigh")
    {
        return hardShutdownHigh();
    }
    if (_name == "HardShutdownLow")
    {
        return hardShutdownLow();
    }
    if (_name == "HardShutdownAlarmHigh")
    {
        return hardShutdownAlarmHigh();
    }
    if (_name == "HardShutdownAlarmLow")
    {
        return hardShutdownAlarmLow();
    }

    return PropertiesVariant();
}


const vtable_t HardShutdown::_vtable[] = {
    vtable::start(),

    vtable::signal("HardShutdownHighAlarmAsserted",
                   details::HardShutdown::_signal_HardShutdownHighAlarmAsserted
                        .data()),

    vtable::signal("HardShutdownHighAlarmDeasserted",
                   details::HardShutdown::_signal_HardShutdownHighAlarmDeasserted
                        .data()),

    vtable::signal("HardShutdownLowAlarmAsserted",
                   details::HardShutdown::_signal_HardShutdownLowAlarmAsserted
                        .data()),

    vtable::signal("HardShutdownLowAlarmDeasserted",
                   details::HardShutdown::_signal_HardShutdownLowAlarmDeasserted
                        .data()),
    vtable::property("HardShutdownHigh",
                     details::HardShutdown::_property_HardShutdownHigh
                        .data(),
                     _callback_get_HardShutdownHigh,
                     _callback_set_HardShutdownHigh,
                     vtable::property_::emits_change),
    vtable::property("HardShutdownLow",
                     details::HardShutdown::_property_HardShutdownLow
                        .data(),
                     _callback_get_HardShutdownLow,
                     _callback_set_HardShutdownLow,
                     vtable::property_::emits_change),
    vtable::property("HardShutdownAlarmHigh",
                     details::HardShutdown::_property_HardShutdownAlarmHigh
                        .data(),
                     _callback_get_HardShutdownAlarmHigh,
                     _callback_set_HardShutdownAlarmHigh,
                     vtable::property_::emits_change),
    vtable::property("HardShutdownAlarmLow",
                     details::HardShutdown::_property_HardShutdownAlarmLow
                        .data(),
                     _callback_get_HardShutdownAlarmLow,
                     _callback_set_HardShutdownAlarmLow,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

