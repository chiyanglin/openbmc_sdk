#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Drive/server.hpp>








namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Drive::Drive(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Drive_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Drive::Drive(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Drive(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Drive::capacity() const ->
        uint64_t
{
    return _capacity;
}

int Drive::_callback_get_Capacity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->capacity();
                    }
                ));
    }
}

auto Drive::capacity(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_capacity != value)
    {
        _capacity = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("Capacity");
        }
    }

    return _capacity;
}

auto Drive::capacity(uint64_t val) ->
        uint64_t
{
    return capacity(val, false);
}

int Drive::_callback_set_Capacity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->capacity(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_Capacity =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Drive::protocol() const ->
        DriveProtocol
{
    return _protocol;
}

int Drive::_callback_get_Protocol(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->protocol();
                    }
                ));
    }
}

auto Drive::protocol(DriveProtocol value,
                                         bool skipSignal) ->
        DriveProtocol
{
    if (_protocol != value)
    {
        _protocol = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("Protocol");
        }
    }

    return _protocol;
}

auto Drive::protocol(DriveProtocol val) ->
        DriveProtocol
{
    return protocol(val, false);
}

int Drive::_callback_set_Protocol(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](DriveProtocol&& arg)
                    {
                        o->protocol(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_Protocol =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive::DriveProtocol>());
}
}

auto Drive::type() const ->
        DriveType
{
    return _type;
}

int Drive::_callback_get_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->type();
                    }
                ));
    }
}

auto Drive::type(DriveType value,
                                         bool skipSignal) ->
        DriveType
{
    if (_type != value)
    {
        _type = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("Type");
        }
    }

    return _type;
}

auto Drive::type(DriveType val) ->
        DriveType
{
    return type(val, false);
}

int Drive::_callback_set_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](DriveType&& arg)
                    {
                        o->type(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_Type =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive::DriveType>());
}
}

auto Drive::encryptionStatus() const ->
        DriveEncryptionState
{
    return _encryptionStatus;
}

int Drive::_callback_get_EncryptionStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->encryptionStatus();
                    }
                ));
    }
}

auto Drive::encryptionStatus(DriveEncryptionState value,
                                         bool skipSignal) ->
        DriveEncryptionState
{
    if (_encryptionStatus != value)
    {
        _encryptionStatus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("EncryptionStatus");
        }
    }

    return _encryptionStatus;
}

auto Drive::encryptionStatus(DriveEncryptionState val) ->
        DriveEncryptionState
{
    return encryptionStatus(val, false);
}

int Drive::_callback_set_EncryptionStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](DriveEncryptionState&& arg)
                    {
                        o->encryptionStatus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_EncryptionStatus =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive::DriveEncryptionState>());
}
}

auto Drive::lockedStatus() const ->
        DriveLockState
{
    return _lockedStatus;
}

int Drive::_callback_get_LockedStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lockedStatus();
                    }
                ));
    }
}

auto Drive::lockedStatus(DriveLockState value,
                                         bool skipSignal) ->
        DriveLockState
{
    if (_lockedStatus != value)
    {
        _lockedStatus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("LockedStatus");
        }
    }

    return _lockedStatus;
}

auto Drive::lockedStatus(DriveLockState val) ->
        DriveLockState
{
    return lockedStatus(val, false);
}

int Drive::_callback_set_LockedStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](DriveLockState&& arg)
                    {
                        o->lockedStatus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_LockedStatus =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Drive::DriveLockState>());
}
}

auto Drive::predictedMediaLifeLeftPercent() const ->
        uint8_t
{
    return _predictedMediaLifeLeftPercent;
}

int Drive::_callback_get_PredictedMediaLifeLeftPercent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->predictedMediaLifeLeftPercent();
                    }
                ));
    }
}

auto Drive::predictedMediaLifeLeftPercent(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_predictedMediaLifeLeftPercent != value)
    {
        _predictedMediaLifeLeftPercent = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("PredictedMediaLifeLeftPercent");
        }
    }

    return _predictedMediaLifeLeftPercent;
}

auto Drive::predictedMediaLifeLeftPercent(uint8_t val) ->
        uint8_t
{
    return predictedMediaLifeLeftPercent(val, false);
}

int Drive::_callback_set_PredictedMediaLifeLeftPercent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->predictedMediaLifeLeftPercent(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Drive
{
static const auto _property_PredictedMediaLifeLeftPercent =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto Drive::resettable() const ->
        bool
{
    return _resettable;
}

int Drive::_callback_get_Resettable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Drive*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->resettable();
                    }
                ));
    }
}

auto Drive::resettable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_resettable != value)
    {
        _resettable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Drive_interface.property_changed("Resettable");
        }
    }

    return _resettable;
}

auto Drive::resettable(bool val) ->
        bool
{
    return resettable(val, false);
}


namespace details
{
namespace Drive
{
static const auto _property_Resettable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Drive::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Capacity")
    {
        auto& v = std::get<uint64_t>(val);
        capacity(v, skipSignal);
        return;
    }
    if (_name == "Protocol")
    {
        auto& v = std::get<DriveProtocol>(val);
        protocol(v, skipSignal);
        return;
    }
    if (_name == "Type")
    {
        auto& v = std::get<DriveType>(val);
        type(v, skipSignal);
        return;
    }
    if (_name == "EncryptionStatus")
    {
        auto& v = std::get<DriveEncryptionState>(val);
        encryptionStatus(v, skipSignal);
        return;
    }
    if (_name == "LockedStatus")
    {
        auto& v = std::get<DriveLockState>(val);
        lockedStatus(v, skipSignal);
        return;
    }
    if (_name == "PredictedMediaLifeLeftPercent")
    {
        auto& v = std::get<uint8_t>(val);
        predictedMediaLifeLeftPercent(v, skipSignal);
        return;
    }
    if (_name == "Resettable")
    {
        auto& v = std::get<bool>(val);
        resettable(v, skipSignal);
        return;
    }
}

auto Drive::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Capacity")
    {
        return capacity();
    }
    if (_name == "Protocol")
    {
        return protocol();
    }
    if (_name == "Type")
    {
        return type();
    }
    if (_name == "EncryptionStatus")
    {
        return encryptionStatus();
    }
    if (_name == "LockedStatus")
    {
        return lockedStatus();
    }
    if (_name == "PredictedMediaLifeLeftPercent")
    {
        return predictedMediaLifeLeftPercent();
    }
    if (_name == "Resettable")
    {
        return resettable();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Drive::DriveProtocol */
static const std::tuple<const char*, Drive::DriveProtocol> mappingDriveDriveProtocol[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveProtocol.SAS",                 Drive::DriveProtocol::SAS ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveProtocol.SATA",                 Drive::DriveProtocol::SATA ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveProtocol.NVMe",                 Drive::DriveProtocol::NVMe ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveProtocol.FC",                 Drive::DriveProtocol::FC ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveProtocol.Unknown",                 Drive::DriveProtocol::Unknown ),
        };

} // anonymous namespace

auto Drive::convertStringToDriveProtocol(const std::string& s) noexcept ->
        std::optional<DriveProtocol>
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveProtocol),
            std::end(mappingDriveDriveProtocol),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDriveDriveProtocol) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Drive::convertDriveProtocolFromString(const std::string& s) ->
        DriveProtocol
{
    auto r = convertStringToDriveProtocol(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Drive::convertDriveProtocolToString(Drive::DriveProtocol v)
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveProtocol),
            std::end(mappingDriveDriveProtocol),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDriveDriveProtocol))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Drive::DriveType */
static const std::tuple<const char*, Drive::DriveType> mappingDriveDriveType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveType.HDD",                 Drive::DriveType::HDD ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveType.SSD",                 Drive::DriveType::SSD ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveType.Unknown",                 Drive::DriveType::Unknown ),
        };

} // anonymous namespace

auto Drive::convertStringToDriveType(const std::string& s) noexcept ->
        std::optional<DriveType>
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveType),
            std::end(mappingDriveDriveType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDriveDriveType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Drive::convertDriveTypeFromString(const std::string& s) ->
        DriveType
{
    auto r = convertStringToDriveType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Drive::convertDriveTypeToString(Drive::DriveType v)
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveType),
            std::end(mappingDriveDriveType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDriveDriveType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Drive::DriveEncryptionState */
static const std::tuple<const char*, Drive::DriveEncryptionState> mappingDriveDriveEncryptionState[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveEncryptionState.Encrypted",                 Drive::DriveEncryptionState::Encrypted ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveEncryptionState.Unencrypted",                 Drive::DriveEncryptionState::Unencrypted ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveEncryptionState.Unknown",                 Drive::DriveEncryptionState::Unknown ),
        };

} // anonymous namespace

auto Drive::convertStringToDriveEncryptionState(const std::string& s) noexcept ->
        std::optional<DriveEncryptionState>
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveEncryptionState),
            std::end(mappingDriveDriveEncryptionState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDriveDriveEncryptionState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Drive::convertDriveEncryptionStateFromString(const std::string& s) ->
        DriveEncryptionState
{
    auto r = convertStringToDriveEncryptionState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Drive::convertDriveEncryptionStateToString(Drive::DriveEncryptionState v)
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveEncryptionState),
            std::end(mappingDriveDriveEncryptionState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDriveDriveEncryptionState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Drive::DriveLockState */
static const std::tuple<const char*, Drive::DriveLockState> mappingDriveDriveLockState[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveLockState.Locked",                 Drive::DriveLockState::Locked ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveLockState.Unlocked",                 Drive::DriveLockState::Unlocked ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Drive.DriveLockState.Unknown",                 Drive::DriveLockState::Unknown ),
        };

} // anonymous namespace

auto Drive::convertStringToDriveLockState(const std::string& s) noexcept ->
        std::optional<DriveLockState>
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveLockState),
            std::end(mappingDriveDriveLockState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDriveDriveLockState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Drive::convertDriveLockStateFromString(const std::string& s) ->
        DriveLockState
{
    auto r = convertStringToDriveLockState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Drive::convertDriveLockStateToString(Drive::DriveLockState v)
{
    auto i = std::find_if(
            std::begin(mappingDriveDriveLockState),
            std::end(mappingDriveDriveLockState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDriveDriveLockState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Drive::_vtable[] = {
    vtable::start(),
    vtable::property("Capacity",
                     details::Drive::_property_Capacity
                        .data(),
                     _callback_get_Capacity,
                     _callback_set_Capacity,
                     vtable::property_::emits_change),
    vtable::property("Protocol",
                     details::Drive::_property_Protocol
                        .data(),
                     _callback_get_Protocol,
                     _callback_set_Protocol,
                     vtable::property_::emits_change),
    vtable::property("Type",
                     details::Drive::_property_Type
                        .data(),
                     _callback_get_Type,
                     _callback_set_Type,
                     vtable::property_::emits_change),
    vtable::property("EncryptionStatus",
                     details::Drive::_property_EncryptionStatus
                        .data(),
                     _callback_get_EncryptionStatus,
                     _callback_set_EncryptionStatus,
                     vtable::property_::emits_change),
    vtable::property("LockedStatus",
                     details::Drive::_property_LockedStatus
                        .data(),
                     _callback_get_LockedStatus,
                     _callback_set_LockedStatus,
                     vtable::property_::emits_change),
    vtable::property("PredictedMediaLifeLeftPercent",
                     details::Drive::_property_PredictedMediaLifeLeftPercent
                        .data(),
                     _callback_get_PredictedMediaLifeLeftPercent,
                     _callback_set_PredictedMediaLifeLeftPercent,
                     vtable::property_::emits_change),
    vtable::property("Resettable",
                     details::Drive::_property_Resettable
                        .data(),
                     _callback_get_Resettable,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

