#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Logging
{
namespace PEL
{
namespace server
{

class Entry
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Entry() = delete;
        Entry(const Entry&) = delete;
        Entry& operator=(const Entry&) = delete;
        Entry(Entry&&) = delete;
        Entry& operator=(Entry&&) = delete;
        virtual ~Entry() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Entry(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Entry(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Hidden */
        virtual bool hidden() const;
        /** Set value of Hidden with option to skip sending signal */
        virtual bool hidden(bool value,
               bool skipSignal);
        /** Set value of Hidden */
        virtual bool hidden(bool value);
        /** Get value of Subsystem */
        virtual std::string subsystem() const;
        /** Set value of Subsystem with option to skip sending signal */
        virtual std::string subsystem(std::string value,
               bool skipSignal);
        /** Set value of Subsystem */
        virtual std::string subsystem(std::string value);
        /** Get value of ManagementSystemAck */
        virtual bool managementSystemAck() const;
        /** Set value of ManagementSystemAck with option to skip sending signal */
        virtual bool managementSystemAck(bool value,
               bool skipSignal);
        /** Set value of ManagementSystemAck */
        virtual bool managementSystemAck(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _org_open_power_Logging_PEL_Entry_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _org_open_power_Logging_PEL_Entry_interface.emit_removed();
        }

        static constexpr auto interface = "org.open_power.Logging.PEL.Entry";

    private:

        /** @brief sd-bus callback for get-property 'Hidden' */
        static int _callback_get_Hidden(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Hidden' */
        static int _callback_set_Hidden(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Subsystem' */
        static int _callback_get_Subsystem(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Subsystem' */
        static int _callback_set_Subsystem(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ManagementSystemAck' */
        static int _callback_get_ManagementSystemAck(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ManagementSystemAck' */
        static int _callback_set_ManagementSystemAck(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _org_open_power_Logging_PEL_Entry_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _hidden = false;
        std::string _subsystem{};
        bool _managementSystemAck = false;

};


} // namespace server
} // namespace PEL
} // namespace Logging
} // namespace open_power
} // namespace org

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

