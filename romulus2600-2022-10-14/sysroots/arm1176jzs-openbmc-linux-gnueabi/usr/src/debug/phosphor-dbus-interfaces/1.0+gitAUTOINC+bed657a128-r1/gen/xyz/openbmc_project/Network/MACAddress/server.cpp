#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/MACAddress/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

MACAddress::MACAddress(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_MACAddress_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MACAddress::MACAddress(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MACAddress(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MACAddress::macAddress() const ->
        std::string
{
    return _macAddress;
}

int MACAddress::_callback_get_MACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MACAddress*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->macAddress();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto MACAddress::macAddress(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_macAddress != value)
    {
        _macAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_MACAddress_interface.property_changed("MACAddress");
        }
    }

    return _macAddress;
}

auto MACAddress::macAddress(std::string val) ->
        std::string
{
    return macAddress(val, false);
}

int MACAddress::_callback_set_MACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MACAddress*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->macAddress(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace MACAddress
{
static const auto _property_MACAddress =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void MACAddress::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MACAddress")
    {
        auto& v = std::get<std::string>(val);
        macAddress(v, skipSignal);
        return;
    }
}

auto MACAddress::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MACAddress")
    {
        return macAddress();
    }

    return PropertiesVariant();
}


const vtable_t MACAddress::_vtable[] = {
    vtable::start(),
    vtable::property("MACAddress",
                     details::MACAddress::_property_MACAddress
                        .data(),
                     _callback_get_MACAddress,
                     _callback_set_MACAddress,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

