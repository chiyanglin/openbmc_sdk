#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/Settings/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

Settings::Settings(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_Settings_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Settings::Settings(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Settings(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Settings::writeProtected() const ->
        bool
{
    return _writeProtected;
}

int Settings::_callback_get_WriteProtected(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Settings*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->writeProtected();
                    }
                ));
    }
}

auto Settings::writeProtected(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_writeProtected != value)
    {
        _writeProtected = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_Settings_interface.property_changed("WriteProtected");
        }
    }

    return _writeProtected;
}

auto Settings::writeProtected(bool val) ->
        bool
{
    return writeProtected(val, false);
}

int Settings::_callback_set_WriteProtected(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Settings*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->writeProtected(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Settings
{
static const auto _property_WriteProtected =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Settings::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "WriteProtected")
    {
        auto& v = std::get<bool>(val);
        writeProtected(v, skipSignal);
        return;
    }
}

auto Settings::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "WriteProtected")
    {
        return writeProtected();
    }

    return PropertiesVariant();
}


const vtable_t Settings::_vtable[] = {
    vtable::start(),
    vtable::property("WriteProtected",
                     details::Settings::_property_WriteProtected
                        .data(),
                     _callback_get_WriteProtected,
                     _callback_set_WriteProtected,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

