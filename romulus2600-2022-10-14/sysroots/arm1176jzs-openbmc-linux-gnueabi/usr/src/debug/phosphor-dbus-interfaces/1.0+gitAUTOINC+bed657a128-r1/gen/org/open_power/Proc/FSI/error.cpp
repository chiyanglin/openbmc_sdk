#include <org/open_power/Proc/FSI/error.hpp>

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Proc
{
namespace FSI
{
namespace Error
{
const char* MasterDetectionFailure::name() const noexcept
{
    return errName;
}
const char* MasterDetectionFailure::description() const noexcept
{
    return errDesc;
}
const char* MasterDetectionFailure::what() const noexcept
{
    return errWhat;
}
const char* SlaveDetectionFailure::name() const noexcept
{
    return errName;
}
const char* SlaveDetectionFailure::description() const noexcept
{
    return errDesc;
}
const char* SlaveDetectionFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace FSI
} // namespace Proc
} // namespace open_power
} // namespace org
} // namespace sdbusplus

