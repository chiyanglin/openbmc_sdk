#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Nvme/Status/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Nvme
{
namespace server
{

Status::Status(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Nvme_Status_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Status::Status(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Status(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Status::smartWarnings() const ->
        std::string
{
    return _smartWarnings;
}

int Status::_callback_get_SmartWarnings(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->smartWarnings();
                    }
                ));
    }
}

auto Status::smartWarnings(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_smartWarnings != value)
    {
        _smartWarnings = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("SmartWarnings");
        }
    }

    return _smartWarnings;
}

auto Status::smartWarnings(std::string val) ->
        std::string
{
    return smartWarnings(val, false);
}

int Status::_callback_set_SmartWarnings(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->smartWarnings(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_SmartWarnings =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Status::statusFlags() const ->
        std::string
{
    return _statusFlags;
}

int Status::_callback_get_StatusFlags(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->statusFlags();
                    }
                ));
    }
}

auto Status::statusFlags(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_statusFlags != value)
    {
        _statusFlags = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("StatusFlags");
        }
    }

    return _statusFlags;
}

auto Status::statusFlags(std::string val) ->
        std::string
{
    return statusFlags(val, false);
}

int Status::_callback_set_StatusFlags(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->statusFlags(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_StatusFlags =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Status::driveLifeUsed() const ->
        std::string
{
    return _driveLifeUsed;
}

int Status::_callback_get_DriveLifeUsed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->driveLifeUsed();
                    }
                ));
    }
}

auto Status::driveLifeUsed(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_driveLifeUsed != value)
    {
        _driveLifeUsed = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("DriveLifeUsed");
        }
    }

    return _driveLifeUsed;
}

auto Status::driveLifeUsed(std::string val) ->
        std::string
{
    return driveLifeUsed(val, false);
}

int Status::_callback_set_DriveLifeUsed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->driveLifeUsed(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_DriveLifeUsed =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Status::capacityFault() const ->
        bool
{
    return _capacityFault;
}

int Status::_callback_get_CapacityFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->capacityFault();
                    }
                ));
    }
}

auto Status::capacityFault(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_capacityFault != value)
    {
        _capacityFault = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("CapacityFault");
        }
    }

    return _capacityFault;
}

auto Status::capacityFault(bool val) ->
        bool
{
    return capacityFault(val, false);
}

int Status::_callback_set_CapacityFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->capacityFault(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_CapacityFault =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::temperatureFault() const ->
        bool
{
    return _temperatureFault;
}

int Status::_callback_get_TemperatureFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->temperatureFault();
                    }
                ));
    }
}

auto Status::temperatureFault(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_temperatureFault != value)
    {
        _temperatureFault = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("TemperatureFault");
        }
    }

    return _temperatureFault;
}

auto Status::temperatureFault(bool val) ->
        bool
{
    return temperatureFault(val, false);
}

int Status::_callback_set_TemperatureFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->temperatureFault(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_TemperatureFault =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::degradesFault() const ->
        bool
{
    return _degradesFault;
}

int Status::_callback_get_DegradesFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->degradesFault();
                    }
                ));
    }
}

auto Status::degradesFault(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_degradesFault != value)
    {
        _degradesFault = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("DegradesFault");
        }
    }

    return _degradesFault;
}

auto Status::degradesFault(bool val) ->
        bool
{
    return degradesFault(val, false);
}

int Status::_callback_set_DegradesFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->degradesFault(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_DegradesFault =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::mediaFault() const ->
        bool
{
    return _mediaFault;
}

int Status::_callback_get_MediaFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mediaFault();
                    }
                ));
    }
}

auto Status::mediaFault(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_mediaFault != value)
    {
        _mediaFault = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("MediaFault");
        }
    }

    return _mediaFault;
}

auto Status::mediaFault(bool val) ->
        bool
{
    return mediaFault(val, false);
}

int Status::_callback_set_MediaFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->mediaFault(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_MediaFault =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Status::backupDeviceFault() const ->
        bool
{
    return _backupDeviceFault;
}

int Status::_callback_get_BackupDeviceFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->backupDeviceFault();
                    }
                ));
    }
}

auto Status::backupDeviceFault(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_backupDeviceFault != value)
    {
        _backupDeviceFault = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Nvme_Status_interface.property_changed("BackupDeviceFault");
        }
    }

    return _backupDeviceFault;
}

auto Status::backupDeviceFault(bool val) ->
        bool
{
    return backupDeviceFault(val, false);
}

int Status::_callback_set_BackupDeviceFault(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->backupDeviceFault(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_BackupDeviceFault =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Status::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SmartWarnings")
    {
        auto& v = std::get<std::string>(val);
        smartWarnings(v, skipSignal);
        return;
    }
    if (_name == "StatusFlags")
    {
        auto& v = std::get<std::string>(val);
        statusFlags(v, skipSignal);
        return;
    }
    if (_name == "DriveLifeUsed")
    {
        auto& v = std::get<std::string>(val);
        driveLifeUsed(v, skipSignal);
        return;
    }
    if (_name == "CapacityFault")
    {
        auto& v = std::get<bool>(val);
        capacityFault(v, skipSignal);
        return;
    }
    if (_name == "TemperatureFault")
    {
        auto& v = std::get<bool>(val);
        temperatureFault(v, skipSignal);
        return;
    }
    if (_name == "DegradesFault")
    {
        auto& v = std::get<bool>(val);
        degradesFault(v, skipSignal);
        return;
    }
    if (_name == "MediaFault")
    {
        auto& v = std::get<bool>(val);
        mediaFault(v, skipSignal);
        return;
    }
    if (_name == "BackupDeviceFault")
    {
        auto& v = std::get<bool>(val);
        backupDeviceFault(v, skipSignal);
        return;
    }
}

auto Status::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SmartWarnings")
    {
        return smartWarnings();
    }
    if (_name == "StatusFlags")
    {
        return statusFlags();
    }
    if (_name == "DriveLifeUsed")
    {
        return driveLifeUsed();
    }
    if (_name == "CapacityFault")
    {
        return capacityFault();
    }
    if (_name == "TemperatureFault")
    {
        return temperatureFault();
    }
    if (_name == "DegradesFault")
    {
        return degradesFault();
    }
    if (_name == "MediaFault")
    {
        return mediaFault();
    }
    if (_name == "BackupDeviceFault")
    {
        return backupDeviceFault();
    }

    return PropertiesVariant();
}


const vtable_t Status::_vtable[] = {
    vtable::start(),
    vtable::property("SmartWarnings",
                     details::Status::_property_SmartWarnings
                        .data(),
                     _callback_get_SmartWarnings,
                     _callback_set_SmartWarnings,
                     vtable::property_::emits_change),
    vtable::property("StatusFlags",
                     details::Status::_property_StatusFlags
                        .data(),
                     _callback_get_StatusFlags,
                     _callback_set_StatusFlags,
                     vtable::property_::emits_change),
    vtable::property("DriveLifeUsed",
                     details::Status::_property_DriveLifeUsed
                        .data(),
                     _callback_get_DriveLifeUsed,
                     _callback_set_DriveLifeUsed,
                     vtable::property_::emits_change),
    vtable::property("CapacityFault",
                     details::Status::_property_CapacityFault
                        .data(),
                     _callback_get_CapacityFault,
                     _callback_set_CapacityFault,
                     vtable::property_::emits_change),
    vtable::property("TemperatureFault",
                     details::Status::_property_TemperatureFault
                        .data(),
                     _callback_get_TemperatureFault,
                     _callback_set_TemperatureFault,
                     vtable::property_::emits_change),
    vtable::property("DegradesFault",
                     details::Status::_property_DegradesFault
                        .data(),
                     _callback_get_DegradesFault,
                     _callback_set_DegradesFault,
                     vtable::property_::emits_change),
    vtable::property("MediaFault",
                     details::Status::_property_MediaFault
                        .data(),
                     _callback_get_MediaFault,
                     _callback_set_MediaFault,
                     vtable::property_::emits_change),
    vtable::property("BackupDeviceFault",
                     details::Status::_property_BackupDeviceFault
                        .data(),
                     _callback_get_BackupDeviceFault,
                     _callback_set_BackupDeviceFault,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Nvme
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

