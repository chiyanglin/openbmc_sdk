#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/BIOSConfig/Manager/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>



#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>
#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace server
{

Manager::Manager(bus_t& bus, const char* path)
        : _xyz_openbmc_project_BIOSConfig_Manager_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Manager::Manager(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Manager(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Manager::_callback_SetAttribute(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& attributeName, std::variant<int64_t, std::string>&& attributeValue)
                    {
                        return o->setAttribute(
                                attributeName, attributeValue);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeReadOnly& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_SetAttribute =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::variant<int64_t, std::string>>());
static const auto _return_SetAttribute =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Manager::_callback_GetAttribute(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback<true>(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& attributeName)
                    {
                        return o->getAttribute(
                                attributeName);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_GetAttribute =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_GetAttribute =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::xyz::openbmc_project::BIOSConfig::server::Manager::AttributeType, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>>());
}
}



auto Manager::resetBIOSSettings() const ->
        ResetFlag
{
    return _resetBIOSSettings;
}

int Manager::_callback_get_ResetBIOSSettings(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->resetBIOSSettings();
                    }
                ));
    }
}

auto Manager::resetBIOSSettings(ResetFlag value,
                                         bool skipSignal) ->
        ResetFlag
{
    if (_resetBIOSSettings != value)
    {
        _resetBIOSSettings = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_BIOSConfig_Manager_interface.property_changed("ResetBIOSSettings");
        }
    }

    return _resetBIOSSettings;
}

auto Manager::resetBIOSSettings(ResetFlag val) ->
        ResetFlag
{
    return resetBIOSSettings(val, false);
}

int Manager::_callback_set_ResetBIOSSettings(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ResetFlag&& arg)
                    {
                        o->resetBIOSSettings(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Manager
{
static const auto _property_ResetBIOSSettings =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::BIOSConfig::server::Manager::ResetFlag>());
}
}

auto Manager::baseBIOSTable() const ->
        std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>>
{
    return _baseBIOSTable;
}

int Manager::_callback_get_BaseBIOSTable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->baseBIOSTable();
                    }
                ));
    }
}

auto Manager::baseBIOSTable(std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> value,
                                         bool skipSignal) ->
        std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>>
{
    if (_baseBIOSTable != value)
    {
        _baseBIOSTable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_BIOSConfig_Manager_interface.property_changed("BaseBIOSTable");
        }
    }

    return _baseBIOSTable;
}

auto Manager::baseBIOSTable(std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> val) ->
        std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>>
{
    return baseBIOSTable(val, false);
}

int Manager::_callback_set_BaseBIOSTable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>>&& arg)
                    {
                        o->baseBIOSTable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Manager
{
static const auto _property_BaseBIOSTable =
    utility::tuple_to_array(message::types::type_id<
            std::map<std::string, std::tuple<sdbusplus::xyz::openbmc_project::BIOSConfig::server::Manager::AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<sdbusplus::xyz::openbmc_project::BIOSConfig::server::Manager::BoundType, std::variant<int64_t, std::string>>>>>>());
}
}

auto Manager::pendingAttributes() const ->
        std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>>
{
    return _pendingAttributes;
}

int Manager::_callback_get_PendingAttributes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pendingAttributes();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeReadOnly& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Manager::pendingAttributes(std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> value,
                                         bool skipSignal) ->
        std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>>
{
    if (_pendingAttributes != value)
    {
        _pendingAttributes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_BIOSConfig_Manager_interface.property_changed("PendingAttributes");
        }
    }

    return _pendingAttributes;
}

auto Manager::pendingAttributes(std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> val) ->
        std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>>
{
    return pendingAttributes(val, false);
}

int Manager::_callback_set_PendingAttributes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>>&& arg)
                    {
                        o->pendingAttributes(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::BIOSConfig::Common::Error::AttributeReadOnly& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Manager
{
static const auto _property_PendingAttributes =
    utility::tuple_to_array(message::types::type_id<
            std::map<std::string, std::tuple<sdbusplus::xyz::openbmc_project::BIOSConfig::server::Manager::AttributeType, std::variant<int64_t, std::string>>>>());
}
}

void Manager::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ResetBIOSSettings")
    {
        auto& v = std::get<ResetFlag>(val);
        resetBIOSSettings(v, skipSignal);
        return;
    }
    if (_name == "BaseBIOSTable")
    {
        auto& v = std::get<std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>>>(val);
        baseBIOSTable(v, skipSignal);
        return;
    }
    if (_name == "PendingAttributes")
    {
        auto& v = std::get<std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>>>(val);
        pendingAttributes(v, skipSignal);
        return;
    }
}

auto Manager::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ResetBIOSSettings")
    {
        return resetBIOSSettings();
    }
    if (_name == "BaseBIOSTable")
    {
        return baseBIOSTable();
    }
    if (_name == "PendingAttributes")
    {
        return pendingAttributes();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Manager::AttributeType */
static const std::tuple<const char*, Manager::AttributeType> mappingManagerAttributeType[] =
        {
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.AttributeType.Enumeration",                 Manager::AttributeType::Enumeration ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.AttributeType.String",                 Manager::AttributeType::String ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.AttributeType.Password",                 Manager::AttributeType::Password ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.AttributeType.Integer",                 Manager::AttributeType::Integer ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.AttributeType.Boolean",                 Manager::AttributeType::Boolean ),
        };

} // anonymous namespace

auto Manager::convertStringToAttributeType(const std::string& s) noexcept ->
        std::optional<AttributeType>
{
    auto i = std::find_if(
            std::begin(mappingManagerAttributeType),
            std::end(mappingManagerAttributeType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingManagerAttributeType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Manager::convertAttributeTypeFromString(const std::string& s) ->
        AttributeType
{
    auto r = convertStringToAttributeType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Manager::convertAttributeTypeToString(Manager::AttributeType v)
{
    auto i = std::find_if(
            std::begin(mappingManagerAttributeType),
            std::end(mappingManagerAttributeType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingManagerAttributeType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Manager::ResetFlag */
static const std::tuple<const char*, Manager::ResetFlag> mappingManagerResetFlag[] =
        {
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.ResetFlag.NoAction",                 Manager::ResetFlag::NoAction ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.ResetFlag.FactoryDefaults",                 Manager::ResetFlag::FactoryDefaults ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.ResetFlag.FailSafeDefaults",                 Manager::ResetFlag::FailSafeDefaults ),
        };

} // anonymous namespace

auto Manager::convertStringToResetFlag(const std::string& s) noexcept ->
        std::optional<ResetFlag>
{
    auto i = std::find_if(
            std::begin(mappingManagerResetFlag),
            std::end(mappingManagerResetFlag),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingManagerResetFlag) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Manager::convertResetFlagFromString(const std::string& s) ->
        ResetFlag
{
    auto r = convertStringToResetFlag(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Manager::convertResetFlagToString(Manager::ResetFlag v)
{
    auto i = std::find_if(
            std::begin(mappingManagerResetFlag),
            std::end(mappingManagerResetFlag),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingManagerResetFlag))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Manager::BoundType */
static const std::tuple<const char*, Manager::BoundType> mappingManagerBoundType[] =
        {
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.BoundType.LowerBound",                 Manager::BoundType::LowerBound ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.BoundType.UpperBound",                 Manager::BoundType::UpperBound ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.BoundType.ScalarIncrement",                 Manager::BoundType::ScalarIncrement ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.BoundType.MinStringLength",                 Manager::BoundType::MinStringLength ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.BoundType.MaxStringLength",                 Manager::BoundType::MaxStringLength ),
            std::make_tuple( "xyz.openbmc_project.BIOSConfig.Manager.BoundType.OneOf",                 Manager::BoundType::OneOf ),
        };

} // anonymous namespace

auto Manager::convertStringToBoundType(const std::string& s) noexcept ->
        std::optional<BoundType>
{
    auto i = std::find_if(
            std::begin(mappingManagerBoundType),
            std::end(mappingManagerBoundType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingManagerBoundType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Manager::convertBoundTypeFromString(const std::string& s) ->
        BoundType
{
    auto r = convertStringToBoundType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Manager::convertBoundTypeToString(Manager::BoundType v)
{
    auto i = std::find_if(
            std::begin(mappingManagerBoundType),
            std::end(mappingManagerBoundType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingManagerBoundType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Manager::_vtable[] = {
    vtable::start(),

    vtable::method("SetAttribute",
                   details::Manager::_param_SetAttribute
                        .data(),
                   details::Manager::_return_SetAttribute
                        .data(),
                   _callback_SetAttribute),

    vtable::method("GetAttribute",
                   details::Manager::_param_GetAttribute
                        .data(),
                   details::Manager::_return_GetAttribute
                        .data(),
                   _callback_GetAttribute),
    vtable::property("ResetBIOSSettings",
                     details::Manager::_property_ResetBIOSSettings
                        .data(),
                     _callback_get_ResetBIOSSettings,
                     _callback_set_ResetBIOSSettings,
                     vtable::property_::emits_change),
    vtable::property("BaseBIOSTable",
                     details::Manager::_property_BaseBIOSTable
                        .data(),
                     _callback_get_BaseBIOSTable,
                     _callback_set_BaseBIOSTable,
                     vtable::property_::emits_change),
    vtable::property("PendingAttributes",
                     details::Manager::_property_PendingAttributes
                        .data(),
                     _callback_get_PendingAttributes,
                     _callback_set_PendingAttributes,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

