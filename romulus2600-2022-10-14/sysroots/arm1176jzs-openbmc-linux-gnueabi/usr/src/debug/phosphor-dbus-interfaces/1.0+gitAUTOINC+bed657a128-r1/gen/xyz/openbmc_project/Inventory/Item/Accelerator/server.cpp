#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Accelerator/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Accelerator::Accelerator(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Accelerator_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Accelerator::Accelerator(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Accelerator(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Accelerator::type() const ->
        AcceleratorType
{
    return _type;
}

int Accelerator::_callback_get_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Accelerator*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->type();
                    }
                ));
    }
}

auto Accelerator::type(AcceleratorType value,
                                         bool skipSignal) ->
        AcceleratorType
{
    if (_type != value)
    {
        _type = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Accelerator_interface.property_changed("Type");
        }
    }

    return _type;
}

auto Accelerator::type(AcceleratorType val) ->
        AcceleratorType
{
    return type(val, false);
}

int Accelerator::_callback_set_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Accelerator*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](AcceleratorType&& arg)
                    {
                        o->type(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Accelerator
{
static const auto _property_Type =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Accelerator::AcceleratorType>());
}
}

void Accelerator::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Type")
    {
        auto& v = std::get<AcceleratorType>(val);
        type(v, skipSignal);
        return;
    }
}

auto Accelerator::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Type")
    {
        return type();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Accelerator::AcceleratorType */
static const std::tuple<const char*, Accelerator::AcceleratorType> mappingAcceleratorAcceleratorType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Accelerator.AcceleratorType.ASIC",                 Accelerator::AcceleratorType::ASIC ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Accelerator.AcceleratorType.FPGA",                 Accelerator::AcceleratorType::FPGA ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Accelerator.AcceleratorType.GPU",                 Accelerator::AcceleratorType::GPU ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Accelerator.AcceleratorType.Unknown",                 Accelerator::AcceleratorType::Unknown ),
        };

} // anonymous namespace

auto Accelerator::convertStringToAcceleratorType(const std::string& s) noexcept ->
        std::optional<AcceleratorType>
{
    auto i = std::find_if(
            std::begin(mappingAcceleratorAcceleratorType),
            std::end(mappingAcceleratorAcceleratorType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingAcceleratorAcceleratorType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Accelerator::convertAcceleratorTypeFromString(const std::string& s) ->
        AcceleratorType
{
    auto r = convertStringToAcceleratorType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Accelerator::convertAcceleratorTypeToString(Accelerator::AcceleratorType v)
{
    auto i = std::find_if(
            std::begin(mappingAcceleratorAcceleratorType),
            std::end(mappingAcceleratorAcceleratorType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingAcceleratorAcceleratorType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Accelerator::_vtable[] = {
    vtable::start(),
    vtable::property("Type",
                     details::Accelerator::_property_Type
                        .data(),
                     _callback_get_Type,
                     _callback_set_Type,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

