#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/OCC/PassThrough/server.hpp>



namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace OCC
{
namespace server
{

PassThrough::PassThrough(bus_t& bus, const char* path)
        : _org_open_power_OCC_PassThrough_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int PassThrough::_callback_Send(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PassThrough*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<int32_t>&& command)
                    {
                        return o->send(
                                command);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace PassThrough
{
static const auto _param_Send =
        utility::tuple_to_array(message::types::type_id<
                std::vector<int32_t>>());
static const auto _return_Send =
        utility::tuple_to_array(message::types::type_id<
                std::vector<int32_t>>());
}
}

int PassThrough::_callback_SetMode(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PassThrough*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& mode, uint16_t&& frequencyPoint)
                    {
                        return o->setMode(
                                mode, frequencyPoint);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace PassThrough
{
static const auto _param_SetMode =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint16_t>());
static const auto _return_SetMode =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}




const vtable_t PassThrough::_vtable[] = {
    vtable::start(),

    vtable::method("Send",
                   details::PassThrough::_param_Send
                        .data(),
                   details::PassThrough::_return_Send
                        .data(),
                   _callback_Send),

    vtable::method("SetMode",
                   details::PassThrough::_param_SetMode
                        .data(),
                   details::PassThrough::_return_SetMode
                        .data(),
                   _callback_SetMode),
    vtable::end()
};

} // namespace server
} // namespace OCC
} // namespace open_power
} // namespace org
} // namespace sdbusplus

