#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#include <xyz/openbmc_project/Logging/Entry/server.hpp>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Syslog
{
namespace Destination
{
namespace Mail
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);



        /** @brief Implementation for Create
         *  This method creates a new D-Bus object representing the destination for which e-mails would be sent for all syslog messages with matching or lower (meaning more important) level.
         *
         *  @param[in] mailto - Specifies the mailto:// URI (RFC6068) to send events to.
         *  @param[in] level - Specifies the maximum level for the log events to be sent.
         *
         *  @return path[sdbusplus::message::object_path] - The object path of the created xyz.openbmc_project.Logging.Syslog.Destination.Mail.Entry.
         */
        virtual sdbusplus::message::object_path create(
            std::string mailto,
            xyz::openbmc_project::Logging::server::Entry::Level level) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Logging.Syslog.Destination.Mail.Create";

    private:

        /** @brief sd-bus callback for Create
         */
        static int _callback_Create(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Mail
} // namespace Destination
} // namespace Syslog
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

