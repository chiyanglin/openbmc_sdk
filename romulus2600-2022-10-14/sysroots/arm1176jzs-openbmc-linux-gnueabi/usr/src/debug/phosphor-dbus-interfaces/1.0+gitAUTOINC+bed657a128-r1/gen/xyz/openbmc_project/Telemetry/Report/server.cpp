#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Telemetry/Report/server.hpp>














namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

Report::Report(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Telemetry_Report_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Report::Report(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Report(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Report::_callback_Update(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->update(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Report
{
static const auto _param_Update =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Update =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}



auto Report::persistency() const ->
        bool
{
    return _persistency;
}

int Report::_callback_get_Persistency(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->persistency();
                    }
                ));
    }
}

auto Report::persistency(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_persistency != value)
    {
        _persistency = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("Persistency");
        }
    }

    return _persistency;
}

auto Report::persistency(bool val) ->
        bool
{
    return persistency(val, false);
}

int Report::_callback_set_Persistency(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->persistency(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_Persistency =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Report::readingParameters() const ->
        std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>>
{
    return _readingParameters;
}

int Report::_callback_get_ReadingParameters(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->readingParameters();
                    }
                ));
    }
}

auto Report::readingParameters(std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> value,
                                         bool skipSignal) ->
        std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>>
{
    if (_readingParameters != value)
    {
        _readingParameters = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("ReadingParameters");
        }
    }

    return _readingParameters;
}

auto Report::readingParameters(std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>> val) ->
        std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>>
{
    return readingParameters(val, false);
}

int Report::_callback_set_ReadingParameters(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>>&& arg)
                    {
                        o->readingParameters(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_ReadingParameters =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, sdbusplus::xyz::openbmc_project::Telemetry::server::Report::OperationType, std::string, sdbusplus::xyz::openbmc_project::Telemetry::server::Report::CollectionTimescope, uint64_t>>>());
}
}

auto Report::readings() const ->
        std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>>
{
    return _readings;
}

int Report::_callback_get_Readings(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->readings();
                    }
                ));
    }
}

auto Report::readings(std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> value,
                                         bool skipSignal) ->
        std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>>
{
    if (_readings != value)
    {
        _readings = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("Readings");
        }
    }

    return _readings;
}

auto Report::readings(std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>> val) ->
        std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>>
{
    return readings(val, false);
}


namespace details
{
namespace Report
{
static const auto _property_Readings =
    utility::tuple_to_array(message::types::type_id<
            std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>>>());
}
}

auto Report::reportingType() const ->
        ReportingType
{
    return _reportingType;
}

int Report::_callback_get_ReportingType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->reportingType();
                    }
                ));
    }
}

auto Report::reportingType(ReportingType value,
                                         bool skipSignal) ->
        ReportingType
{
    if (_reportingType != value)
    {
        _reportingType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("ReportingType");
        }
    }

    return _reportingType;
}

auto Report::reportingType(ReportingType val) ->
        ReportingType
{
    return reportingType(val, false);
}

int Report::_callback_set_ReportingType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ReportingType&& arg)
                    {
                        o->reportingType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_ReportingType =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Telemetry::server::Report::ReportingType>());
}
}

auto Report::reportUpdates() const ->
        ReportUpdates
{
    return _reportUpdates;
}

int Report::_callback_get_ReportUpdates(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->reportUpdates();
                    }
                ));
    }
}

auto Report::reportUpdates(ReportUpdates value,
                                         bool skipSignal) ->
        ReportUpdates
{
    if (_reportUpdates != value)
    {
        _reportUpdates = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("ReportUpdates");
        }
    }

    return _reportUpdates;
}

auto Report::reportUpdates(ReportUpdates val) ->
        ReportUpdates
{
    return reportUpdates(val, false);
}

int Report::_callback_set_ReportUpdates(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ReportUpdates&& arg)
                    {
                        o->reportUpdates(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_ReportUpdates =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Telemetry::server::Report::ReportUpdates>());
}
}

auto Report::appendLimit() const ->
        size_t
{
    return _appendLimit;
}

int Report::_callback_get_AppendLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->appendLimit();
                    }
                ));
    }
}

auto Report::appendLimit(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_appendLimit != value)
    {
        _appendLimit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("AppendLimit");
        }
    }

    return _appendLimit;
}

auto Report::appendLimit(size_t val) ->
        size_t
{
    return appendLimit(val, false);
}


namespace details
{
namespace Report
{
static const auto _property_AppendLimit =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto Report::interval() const ->
        uint64_t
{
    return _interval;
}

int Report::_callback_get_Interval(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->interval();
                    }
                ));
    }
}

auto Report::interval(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_interval != value)
    {
        _interval = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("Interval");
        }
    }

    return _interval;
}

auto Report::interval(uint64_t val) ->
        uint64_t
{
    return interval(val, false);
}

int Report::_callback_set_Interval(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->interval(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_Interval =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Report::enabled() const ->
        bool
{
    return _enabled;
}

int Report::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
}

auto Report::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto Report::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int Report::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Report::errorMesages() const ->
        std::vector<std::tuple<ErrorType, std::string>>
{
    return _errorMesages;
}

int Report::_callback_get_ErrorMesages(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->errorMesages();
                    }
                ));
    }
}

auto Report::errorMesages(std::vector<std::tuple<ErrorType, std::string>> value,
                                         bool skipSignal) ->
        std::vector<std::tuple<ErrorType, std::string>>
{
    if (_errorMesages != value)
    {
        _errorMesages = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("ErrorMesages");
        }
    }

    return _errorMesages;
}

auto Report::errorMesages(std::vector<std::tuple<ErrorType, std::string>> val) ->
        std::vector<std::tuple<ErrorType, std::string>>
{
    return errorMesages(val, false);
}


namespace details
{
namespace Report
{
static const auto _property_ErrorMesages =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::tuple<sdbusplus::xyz::openbmc_project::Telemetry::server::Report::ErrorType, std::string>>>());
}
}

auto Report::name() const ->
        std::string
{
    return _name;
}

int Report::_callback_get_Name(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->name();
                    }
                ));
    }
}

auto Report::name(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_name != value)
    {
        _name = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("Name");
        }
    }

    return _name;
}

auto Report::name(std::string val) ->
        std::string
{
    return name(val, false);
}


namespace details
{
namespace Report
{
static const auto _property_Name =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Report::reportActions() const ->
        std::vector<ReportActions>
{
    return _reportActions;
}

int Report::_callback_get_ReportActions(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->reportActions();
                    }
                ));
    }
}

auto Report::reportActions(std::vector<ReportActions> value,
                                         bool skipSignal) ->
        std::vector<ReportActions>
{
    if (_reportActions != value)
    {
        _reportActions = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("ReportActions");
        }
    }

    return _reportActions;
}

auto Report::reportActions(std::vector<ReportActions> val) ->
        std::vector<ReportActions>
{
    return reportActions(val, false);
}

int Report::_callback_set_ReportActions(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<ReportActions>&& arg)
                    {
                        o->reportActions(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Report
{
static const auto _property_ReportActions =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::xyz::openbmc_project::Telemetry::server::Report::ReportActions>>());
}
}

auto Report::triggers() const ->
        std::vector<sdbusplus::message::object_path>
{
    return _triggers;
}

int Report::_callback_get_Triggers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Report*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->triggers();
                    }
                ));
    }
}

auto Report::triggers(std::vector<sdbusplus::message::object_path> value,
                                         bool skipSignal) ->
        std::vector<sdbusplus::message::object_path>
{
    if (_triggers != value)
    {
        _triggers = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Report_interface.property_changed("Triggers");
        }
    }

    return _triggers;
}

auto Report::triggers(std::vector<sdbusplus::message::object_path> val) ->
        std::vector<sdbusplus::message::object_path>
{
    return triggers(val, false);
}


namespace details
{
namespace Report
{
static const auto _property_Triggers =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::message::object_path>>());
}
}

void Report::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Persistency")
    {
        auto& v = std::get<bool>(val);
        persistency(v, skipSignal);
        return;
    }
    if (_name == "ReadingParameters")
    {
        auto& v = std::get<std::vector<std::tuple<std::vector<std::tuple<sdbusplus::message::object_path, std::string>>, OperationType, std::string, CollectionTimescope, uint64_t>>>(val);
        readingParameters(v, skipSignal);
        return;
    }
    if (_name == "Readings")
    {
        auto& v = std::get<std::tuple<uint64_t, std::vector<std::tuple<std::string, std::string, double, uint64_t>>>>(val);
        readings(v, skipSignal);
        return;
    }
    if (_name == "ReportingType")
    {
        auto& v = std::get<ReportingType>(val);
        reportingType(v, skipSignal);
        return;
    }
    if (_name == "ReportUpdates")
    {
        auto& v = std::get<ReportUpdates>(val);
        reportUpdates(v, skipSignal);
        return;
    }
    if (_name == "AppendLimit")
    {
        auto& v = std::get<size_t>(val);
        appendLimit(v, skipSignal);
        return;
    }
    if (_name == "Interval")
    {
        auto& v = std::get<uint64_t>(val);
        interval(v, skipSignal);
        return;
    }
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
    if (_name == "ErrorMesages")
    {
        auto& v = std::get<std::vector<std::tuple<ErrorType, std::string>>>(val);
        errorMesages(v, skipSignal);
        return;
    }
    if (_name == "Name")
    {
        auto& v = std::get<std::string>(val);
        name(v, skipSignal);
        return;
    }
    if (_name == "ReportActions")
    {
        auto& v = std::get<std::vector<ReportActions>>(val);
        reportActions(v, skipSignal);
        return;
    }
    if (_name == "Triggers")
    {
        auto& v = std::get<std::vector<sdbusplus::message::object_path>>(val);
        triggers(v, skipSignal);
        return;
    }
}

auto Report::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Persistency")
    {
        return persistency();
    }
    if (_name == "ReadingParameters")
    {
        return readingParameters();
    }
    if (_name == "Readings")
    {
        return readings();
    }
    if (_name == "ReportingType")
    {
        return reportingType();
    }
    if (_name == "ReportUpdates")
    {
        return reportUpdates();
    }
    if (_name == "AppendLimit")
    {
        return appendLimit();
    }
    if (_name == "Interval")
    {
        return interval();
    }
    if (_name == "Enabled")
    {
        return enabled();
    }
    if (_name == "ErrorMesages")
    {
        return errorMesages();
    }
    if (_name == "Name")
    {
        return name();
    }
    if (_name == "ReportActions")
    {
        return reportActions();
    }
    if (_name == "Triggers")
    {
        return triggers();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Report::OperationType */
static const std::tuple<const char*, Report::OperationType> mappingReportOperationType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.OperationType.Maximum",                 Report::OperationType::Maximum ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.OperationType.Minimum",                 Report::OperationType::Minimum ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.OperationType.Average",                 Report::OperationType::Average ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.OperationType.Summation",                 Report::OperationType::Summation ),
        };

} // anonymous namespace

auto Report::convertStringToOperationType(const std::string& s) noexcept ->
        std::optional<OperationType>
{
    auto i = std::find_if(
            std::begin(mappingReportOperationType),
            std::end(mappingReportOperationType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingReportOperationType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Report::convertOperationTypeFromString(const std::string& s) ->
        OperationType
{
    auto r = convertStringToOperationType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Report::convertOperationTypeToString(Report::OperationType v)
{
    auto i = std::find_if(
            std::begin(mappingReportOperationType),
            std::end(mappingReportOperationType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingReportOperationType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Report::ReportingType */
static const std::tuple<const char*, Report::ReportingType> mappingReportReportingType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportingType.OnChange",                 Report::ReportingType::OnChange ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportingType.OnRequest",                 Report::ReportingType::OnRequest ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportingType.Periodic",                 Report::ReportingType::Periodic ),
        };

} // anonymous namespace

auto Report::convertStringToReportingType(const std::string& s) noexcept ->
        std::optional<ReportingType>
{
    auto i = std::find_if(
            std::begin(mappingReportReportingType),
            std::end(mappingReportReportingType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingReportReportingType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Report::convertReportingTypeFromString(const std::string& s) ->
        ReportingType
{
    auto r = convertStringToReportingType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Report::convertReportingTypeToString(Report::ReportingType v)
{
    auto i = std::find_if(
            std::begin(mappingReportReportingType),
            std::end(mappingReportReportingType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingReportReportingType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Report::ReportUpdates */
static const std::tuple<const char*, Report::ReportUpdates> mappingReportReportUpdates[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportUpdates.Overwrite",                 Report::ReportUpdates::Overwrite ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportUpdates.AppendWrapsWhenFull",                 Report::ReportUpdates::AppendWrapsWhenFull ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportUpdates.AppendStopsWhenFull",                 Report::ReportUpdates::AppendStopsWhenFull ),
        };

} // anonymous namespace

auto Report::convertStringToReportUpdates(const std::string& s) noexcept ->
        std::optional<ReportUpdates>
{
    auto i = std::find_if(
            std::begin(mappingReportReportUpdates),
            std::end(mappingReportReportUpdates),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingReportReportUpdates) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Report::convertReportUpdatesFromString(const std::string& s) ->
        ReportUpdates
{
    auto r = convertStringToReportUpdates(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Report::convertReportUpdatesToString(Report::ReportUpdates v)
{
    auto i = std::find_if(
            std::begin(mappingReportReportUpdates),
            std::end(mappingReportReportUpdates),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingReportReportUpdates))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Report::CollectionTimescope */
static const std::tuple<const char*, Report::CollectionTimescope> mappingReportCollectionTimescope[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.CollectionTimescope.Point",                 Report::CollectionTimescope::Point ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.CollectionTimescope.Interval",                 Report::CollectionTimescope::Interval ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.CollectionTimescope.StartupInterval",                 Report::CollectionTimescope::StartupInterval ),
        };

} // anonymous namespace

auto Report::convertStringToCollectionTimescope(const std::string& s) noexcept ->
        std::optional<CollectionTimescope>
{
    auto i = std::find_if(
            std::begin(mappingReportCollectionTimescope),
            std::end(mappingReportCollectionTimescope),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingReportCollectionTimescope) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Report::convertCollectionTimescopeFromString(const std::string& s) ->
        CollectionTimescope
{
    auto r = convertStringToCollectionTimescope(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Report::convertCollectionTimescopeToString(Report::CollectionTimescope v)
{
    auto i = std::find_if(
            std::begin(mappingReportCollectionTimescope),
            std::end(mappingReportCollectionTimescope),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingReportCollectionTimescope))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Report::ErrorType */
static const std::tuple<const char*, Report::ErrorType> mappingReportErrorType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ErrorType.PropertyConflict",                 Report::ErrorType::PropertyConflict ),
        };

} // anonymous namespace

auto Report::convertStringToErrorType(const std::string& s) noexcept ->
        std::optional<ErrorType>
{
    auto i = std::find_if(
            std::begin(mappingReportErrorType),
            std::end(mappingReportErrorType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingReportErrorType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Report::convertErrorTypeFromString(const std::string& s) ->
        ErrorType
{
    auto r = convertStringToErrorType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Report::convertErrorTypeToString(Report::ErrorType v)
{
    auto i = std::find_if(
            std::begin(mappingReportErrorType),
            std::end(mappingReportErrorType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingReportErrorType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Report::ReportActions */
static const std::tuple<const char*, Report::ReportActions> mappingReportReportActions[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportActions.EmitsReadingsUpdate",                 Report::ReportActions::EmitsReadingsUpdate ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Report.ReportActions.LogToMetricReportsCollection",                 Report::ReportActions::LogToMetricReportsCollection ),
        };

} // anonymous namespace

auto Report::convertStringToReportActions(const std::string& s) noexcept ->
        std::optional<ReportActions>
{
    auto i = std::find_if(
            std::begin(mappingReportReportActions),
            std::end(mappingReportReportActions),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingReportReportActions) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Report::convertReportActionsFromString(const std::string& s) ->
        ReportActions
{
    auto r = convertStringToReportActions(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Report::convertReportActionsToString(Report::ReportActions v)
{
    auto i = std::find_if(
            std::begin(mappingReportReportActions),
            std::end(mappingReportReportActions),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingReportReportActions))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Report::_vtable[] = {
    vtable::start(),

    vtable::method("Update",
                   details::Report::_param_Update
                        .data(),
                   details::Report::_return_Update
                        .data(),
                   _callback_Update),
    vtable::property("Persistency",
                     details::Report::_property_Persistency
                        .data(),
                     _callback_get_Persistency,
                     _callback_set_Persistency,
                     vtable::property_::emits_change),
    vtable::property("ReadingParameters",
                     details::Report::_property_ReadingParameters
                        .data(),
                     _callback_get_ReadingParameters,
                     _callback_set_ReadingParameters,
                     vtable::property_::emits_change),
    vtable::property("Readings",
                     details::Report::_property_Readings
                        .data(),
                     _callback_get_Readings,
                     vtable::property_::emits_change),
    vtable::property("ReportingType",
                     details::Report::_property_ReportingType
                        .data(),
                     _callback_get_ReportingType,
                     _callback_set_ReportingType,
                     vtable::property_::emits_change),
    vtable::property("ReportUpdates",
                     details::Report::_property_ReportUpdates
                        .data(),
                     _callback_get_ReportUpdates,
                     _callback_set_ReportUpdates,
                     vtable::property_::emits_change),
    vtable::property("AppendLimit",
                     details::Report::_property_AppendLimit
                        .data(),
                     _callback_get_AppendLimit,
                     vtable::property_::emits_change),
    vtable::property("Interval",
                     details::Report::_property_Interval
                        .data(),
                     _callback_get_Interval,
                     _callback_set_Interval,
                     vtable::property_::emits_change),
    vtable::property("Enabled",
                     details::Report::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::property("ErrorMesages",
                     details::Report::_property_ErrorMesages
                        .data(),
                     _callback_get_ErrorMesages,
                     vtable::property_::emits_change),
    vtable::property("Name",
                     details::Report::_property_Name
                        .data(),
                     _callback_get_Name,
                     vtable::property_::const_),
    vtable::property("ReportActions",
                     details::Report::_property_ReportActions
                        .data(),
                     _callback_get_ReportActions,
                     _callback_set_ReportActions,
                     vtable::property_::emits_change),
    vtable::property("Triggers",
                     details::Report::_property_Triggers
                        .data(),
                     _callback_get_Triggers,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

