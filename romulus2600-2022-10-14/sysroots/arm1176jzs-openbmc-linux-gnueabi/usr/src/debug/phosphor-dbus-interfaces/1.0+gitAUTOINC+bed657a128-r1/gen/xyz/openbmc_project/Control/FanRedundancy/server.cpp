#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/FanRedundancy/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

FanRedundancy::FanRedundancy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_FanRedundancy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FanRedundancy::FanRedundancy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FanRedundancy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FanRedundancy::allowedFailures() const ->
        uint8_t
{
    return _allowedFailures;
}

int FanRedundancy::_callback_get_AllowedFailures(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allowedFailures();
                    }
                ));
    }
}

auto FanRedundancy::allowedFailures(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_allowedFailures != value)
    {
        _allowedFailures = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_FanRedundancy_interface.property_changed("AllowedFailures");
        }
    }

    return _allowedFailures;
}

auto FanRedundancy::allowedFailures(uint8_t val) ->
        uint8_t
{
    return allowedFailures(val, false);
}


namespace details
{
namespace FanRedundancy
{
static const auto _property_AllowedFailures =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto FanRedundancy::collection() const ->
        std::vector<sdbusplus::message::object_path>
{
    return _collection;
}

int FanRedundancy::_callback_get_Collection(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->collection();
                    }
                ));
    }
}

auto FanRedundancy::collection(std::vector<sdbusplus::message::object_path> value,
                                         bool skipSignal) ->
        std::vector<sdbusplus::message::object_path>
{
    if (_collection != value)
    {
        _collection = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_FanRedundancy_interface.property_changed("Collection");
        }
    }

    return _collection;
}

auto FanRedundancy::collection(std::vector<sdbusplus::message::object_path> val) ->
        std::vector<sdbusplus::message::object_path>
{
    return collection(val, false);
}


namespace details
{
namespace FanRedundancy
{
static const auto _property_Collection =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::message::object_path>>());
}
}

auto FanRedundancy::status() const ->
        State
{
    return _status;
}

int FanRedundancy::_callback_get_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->status();
                    }
                ));
    }
}

auto FanRedundancy::status(State value,
                                         bool skipSignal) ->
        State
{
    if (_status != value)
    {
        _status = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_FanRedundancy_interface.property_changed("Status");
        }
    }

    return _status;
}

auto FanRedundancy::status(State val) ->
        State
{
    return status(val, false);
}


namespace details
{
namespace FanRedundancy
{
static const auto _property_Status =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::server::FanRedundancy::State>());
}
}

void FanRedundancy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AllowedFailures")
    {
        auto& v = std::get<uint8_t>(val);
        allowedFailures(v, skipSignal);
        return;
    }
    if (_name == "Collection")
    {
        auto& v = std::get<std::vector<sdbusplus::message::object_path>>(val);
        collection(v, skipSignal);
        return;
    }
    if (_name == "Status")
    {
        auto& v = std::get<State>(val);
        status(v, skipSignal);
        return;
    }
}

auto FanRedundancy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AllowedFailures")
    {
        return allowedFailures();
    }
    if (_name == "Collection")
    {
        return collection();
    }
    if (_name == "Status")
    {
        return status();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for FanRedundancy::State */
static const std::tuple<const char*, FanRedundancy::State> mappingFanRedundancyState[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.FanRedundancy.State.Full",                 FanRedundancy::State::Full ),
            std::make_tuple( "xyz.openbmc_project.Control.FanRedundancy.State.Degraded",                 FanRedundancy::State::Degraded ),
            std::make_tuple( "xyz.openbmc_project.Control.FanRedundancy.State.Failed",                 FanRedundancy::State::Failed ),
        };

} // anonymous namespace

auto FanRedundancy::convertStringToState(const std::string& s) noexcept ->
        std::optional<State>
{
    auto i = std::find_if(
            std::begin(mappingFanRedundancyState),
            std::end(mappingFanRedundancyState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingFanRedundancyState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto FanRedundancy::convertStateFromString(const std::string& s) ->
        State
{
    auto r = convertStringToState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string FanRedundancy::convertStateToString(FanRedundancy::State v)
{
    auto i = std::find_if(
            std::begin(mappingFanRedundancyState),
            std::end(mappingFanRedundancyState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingFanRedundancyState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t FanRedundancy::_vtable[] = {
    vtable::start(),
    vtable::property("AllowedFailures",
                     details::FanRedundancy::_property_AllowedFailures
                        .data(),
                     _callback_get_AllowedFailures,
                     vtable::property_::const_),
    vtable::property("Collection",
                     details::FanRedundancy::_property_Collection
                        .data(),
                     _callback_get_Collection,
                     vtable::property_::const_),
    vtable::property("Status",
                     details::FanRedundancy::_property_Status
                        .data(),
                     _callback_get_Status,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

