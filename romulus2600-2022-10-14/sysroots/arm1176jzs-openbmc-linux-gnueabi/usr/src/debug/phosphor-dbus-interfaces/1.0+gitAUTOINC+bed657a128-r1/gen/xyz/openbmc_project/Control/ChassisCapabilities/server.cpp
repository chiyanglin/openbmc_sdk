#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/ChassisCapabilities/server.hpp>











namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

ChassisCapabilities::ChassisCapabilities(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_ChassisCapabilities_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ChassisCapabilities::ChassisCapabilities(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ChassisCapabilities(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ChassisCapabilities::capabilitiesFlags() const ->
        uint8_t
{
    return _capabilitiesFlags;
}

int ChassisCapabilities::_callback_get_CapabilitiesFlags(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->capabilitiesFlags();
                    }
                ));
    }
}

auto ChassisCapabilities::capabilitiesFlags(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_capabilitiesFlags != value)
    {
        _capabilitiesFlags = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("CapabilitiesFlags");
        }
    }

    return _capabilitiesFlags;
}

auto ChassisCapabilities::capabilitiesFlags(uint8_t val) ->
        uint8_t
{
    return capabilitiesFlags(val, false);
}

int ChassisCapabilities::_callback_set_CapabilitiesFlags(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->capabilitiesFlags(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_CapabilitiesFlags =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto ChassisCapabilities::chassisIntrusionEnabled() const ->
        bool
{
    return _chassisIntrusionEnabled;
}

int ChassisCapabilities::_callback_get_ChassisIntrusionEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->chassisIntrusionEnabled();
                    }
                ));
    }
}

auto ChassisCapabilities::chassisIntrusionEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_chassisIntrusionEnabled != value)
    {
        _chassisIntrusionEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("ChassisIntrusionEnabled");
        }
    }

    return _chassisIntrusionEnabled;
}

auto ChassisCapabilities::chassisIntrusionEnabled(bool val) ->
        bool
{
    return chassisIntrusionEnabled(val, false);
}

int ChassisCapabilities::_callback_set_ChassisIntrusionEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->chassisIntrusionEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_ChassisIntrusionEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto ChassisCapabilities::chassisFrontPanelLockoutEnabled() const ->
        bool
{
    return _chassisFrontPanelLockoutEnabled;
}

int ChassisCapabilities::_callback_get_ChassisFrontPanelLockoutEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->chassisFrontPanelLockoutEnabled();
                    }
                ));
    }
}

auto ChassisCapabilities::chassisFrontPanelLockoutEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_chassisFrontPanelLockoutEnabled != value)
    {
        _chassisFrontPanelLockoutEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("ChassisFrontPanelLockoutEnabled");
        }
    }

    return _chassisFrontPanelLockoutEnabled;
}

auto ChassisCapabilities::chassisFrontPanelLockoutEnabled(bool val) ->
        bool
{
    return chassisFrontPanelLockoutEnabled(val, false);
}

int ChassisCapabilities::_callback_set_ChassisFrontPanelLockoutEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->chassisFrontPanelLockoutEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_ChassisFrontPanelLockoutEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto ChassisCapabilities::chassisNMIEnabled() const ->
        bool
{
    return _chassisNMIEnabled;
}

int ChassisCapabilities::_callback_get_ChassisNMIEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->chassisNMIEnabled();
                    }
                ));
    }
}

auto ChassisCapabilities::chassisNMIEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_chassisNMIEnabled != value)
    {
        _chassisNMIEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("ChassisNMIEnabled");
        }
    }

    return _chassisNMIEnabled;
}

auto ChassisCapabilities::chassisNMIEnabled(bool val) ->
        bool
{
    return chassisNMIEnabled(val, false);
}

int ChassisCapabilities::_callback_set_ChassisNMIEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->chassisNMIEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_ChassisNMIEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto ChassisCapabilities::chassisPowerInterlockEnabled() const ->
        bool
{
    return _chassisPowerInterlockEnabled;
}

int ChassisCapabilities::_callback_get_ChassisPowerInterlockEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->chassisPowerInterlockEnabled();
                    }
                ));
    }
}

auto ChassisCapabilities::chassisPowerInterlockEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_chassisPowerInterlockEnabled != value)
    {
        _chassisPowerInterlockEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("ChassisPowerInterlockEnabled");
        }
    }

    return _chassisPowerInterlockEnabled;
}

auto ChassisCapabilities::chassisPowerInterlockEnabled(bool val) ->
        bool
{
    return chassisPowerInterlockEnabled(val, false);
}

int ChassisCapabilities::_callback_set_ChassisPowerInterlockEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->chassisPowerInterlockEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_ChassisPowerInterlockEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto ChassisCapabilities::fruDeviceAddress() const ->
        uint8_t
{
    return _fruDeviceAddress;
}

int ChassisCapabilities::_callback_get_FRUDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fruDeviceAddress();
                    }
                ));
    }
}

auto ChassisCapabilities::fruDeviceAddress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_fruDeviceAddress != value)
    {
        _fruDeviceAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("FRUDeviceAddress");
        }
    }

    return _fruDeviceAddress;
}

auto ChassisCapabilities::fruDeviceAddress(uint8_t val) ->
        uint8_t
{
    return fruDeviceAddress(val, false);
}

int ChassisCapabilities::_callback_set_FRUDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->fruDeviceAddress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_FRUDeviceAddress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto ChassisCapabilities::sdrDeviceAddress() const ->
        uint8_t
{
    return _sdrDeviceAddress;
}

int ChassisCapabilities::_callback_get_SDRDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sdrDeviceAddress();
                    }
                ));
    }
}

auto ChassisCapabilities::sdrDeviceAddress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_sdrDeviceAddress != value)
    {
        _sdrDeviceAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("SDRDeviceAddress");
        }
    }

    return _sdrDeviceAddress;
}

auto ChassisCapabilities::sdrDeviceAddress(uint8_t val) ->
        uint8_t
{
    return sdrDeviceAddress(val, false);
}

int ChassisCapabilities::_callback_set_SDRDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->sdrDeviceAddress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_SDRDeviceAddress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto ChassisCapabilities::selDeviceAddress() const ->
        uint8_t
{
    return _selDeviceAddress;
}

int ChassisCapabilities::_callback_get_SELDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->selDeviceAddress();
                    }
                ));
    }
}

auto ChassisCapabilities::selDeviceAddress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_selDeviceAddress != value)
    {
        _selDeviceAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("SELDeviceAddress");
        }
    }

    return _selDeviceAddress;
}

auto ChassisCapabilities::selDeviceAddress(uint8_t val) ->
        uint8_t
{
    return selDeviceAddress(val, false);
}

int ChassisCapabilities::_callback_set_SELDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->selDeviceAddress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_SELDeviceAddress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto ChassisCapabilities::smDeviceAddress() const ->
        uint8_t
{
    return _smDeviceAddress;
}

int ChassisCapabilities::_callback_get_SMDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->smDeviceAddress();
                    }
                ));
    }
}

auto ChassisCapabilities::smDeviceAddress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_smDeviceAddress != value)
    {
        _smDeviceAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("SMDeviceAddress");
        }
    }

    return _smDeviceAddress;
}

auto ChassisCapabilities::smDeviceAddress(uint8_t val) ->
        uint8_t
{
    return smDeviceAddress(val, false);
}

int ChassisCapabilities::_callback_set_SMDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->smDeviceAddress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_SMDeviceAddress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto ChassisCapabilities::bridgeDeviceAddress() const ->
        uint8_t
{
    return _bridgeDeviceAddress;
}

int ChassisCapabilities::_callback_get_BridgeDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bridgeDeviceAddress();
                    }
                ));
    }
}

auto ChassisCapabilities::bridgeDeviceAddress(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_bridgeDeviceAddress != value)
    {
        _bridgeDeviceAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_ChassisCapabilities_interface.property_changed("BridgeDeviceAddress");
        }
    }

    return _bridgeDeviceAddress;
}

auto ChassisCapabilities::bridgeDeviceAddress(uint8_t val) ->
        uint8_t
{
    return bridgeDeviceAddress(val, false);
}

int ChassisCapabilities::_callback_set_BridgeDeviceAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ChassisCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->bridgeDeviceAddress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ChassisCapabilities
{
static const auto _property_BridgeDeviceAddress =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void ChassisCapabilities::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CapabilitiesFlags")
    {
        auto& v = std::get<uint8_t>(val);
        capabilitiesFlags(v, skipSignal);
        return;
    }
    if (_name == "ChassisIntrusionEnabled")
    {
        auto& v = std::get<bool>(val);
        chassisIntrusionEnabled(v, skipSignal);
        return;
    }
    if (_name == "ChassisFrontPanelLockoutEnabled")
    {
        auto& v = std::get<bool>(val);
        chassisFrontPanelLockoutEnabled(v, skipSignal);
        return;
    }
    if (_name == "ChassisNMIEnabled")
    {
        auto& v = std::get<bool>(val);
        chassisNMIEnabled(v, skipSignal);
        return;
    }
    if (_name == "ChassisPowerInterlockEnabled")
    {
        auto& v = std::get<bool>(val);
        chassisPowerInterlockEnabled(v, skipSignal);
        return;
    }
    if (_name == "FRUDeviceAddress")
    {
        auto& v = std::get<uint8_t>(val);
        fruDeviceAddress(v, skipSignal);
        return;
    }
    if (_name == "SDRDeviceAddress")
    {
        auto& v = std::get<uint8_t>(val);
        sdrDeviceAddress(v, skipSignal);
        return;
    }
    if (_name == "SELDeviceAddress")
    {
        auto& v = std::get<uint8_t>(val);
        selDeviceAddress(v, skipSignal);
        return;
    }
    if (_name == "SMDeviceAddress")
    {
        auto& v = std::get<uint8_t>(val);
        smDeviceAddress(v, skipSignal);
        return;
    }
    if (_name == "BridgeDeviceAddress")
    {
        auto& v = std::get<uint8_t>(val);
        bridgeDeviceAddress(v, skipSignal);
        return;
    }
}

auto ChassisCapabilities::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CapabilitiesFlags")
    {
        return capabilitiesFlags();
    }
    if (_name == "ChassisIntrusionEnabled")
    {
        return chassisIntrusionEnabled();
    }
    if (_name == "ChassisFrontPanelLockoutEnabled")
    {
        return chassisFrontPanelLockoutEnabled();
    }
    if (_name == "ChassisNMIEnabled")
    {
        return chassisNMIEnabled();
    }
    if (_name == "ChassisPowerInterlockEnabled")
    {
        return chassisPowerInterlockEnabled();
    }
    if (_name == "FRUDeviceAddress")
    {
        return fruDeviceAddress();
    }
    if (_name == "SDRDeviceAddress")
    {
        return sdrDeviceAddress();
    }
    if (_name == "SELDeviceAddress")
    {
        return selDeviceAddress();
    }
    if (_name == "SMDeviceAddress")
    {
        return smDeviceAddress();
    }
    if (_name == "BridgeDeviceAddress")
    {
        return bridgeDeviceAddress();
    }

    return PropertiesVariant();
}


const vtable_t ChassisCapabilities::_vtable[] = {
    vtable::start(),
    vtable::property("CapabilitiesFlags",
                     details::ChassisCapabilities::_property_CapabilitiesFlags
                        .data(),
                     _callback_get_CapabilitiesFlags,
                     _callback_set_CapabilitiesFlags,
                     vtable::property_::emits_change),
    vtable::property("ChassisIntrusionEnabled",
                     details::ChassisCapabilities::_property_ChassisIntrusionEnabled
                        .data(),
                     _callback_get_ChassisIntrusionEnabled,
                     _callback_set_ChassisIntrusionEnabled,
                     vtable::property_::emits_change),
    vtable::property("ChassisFrontPanelLockoutEnabled",
                     details::ChassisCapabilities::_property_ChassisFrontPanelLockoutEnabled
                        .data(),
                     _callback_get_ChassisFrontPanelLockoutEnabled,
                     _callback_set_ChassisFrontPanelLockoutEnabled,
                     vtable::property_::emits_change),
    vtable::property("ChassisNMIEnabled",
                     details::ChassisCapabilities::_property_ChassisNMIEnabled
                        .data(),
                     _callback_get_ChassisNMIEnabled,
                     _callback_set_ChassisNMIEnabled,
                     vtable::property_::emits_change),
    vtable::property("ChassisPowerInterlockEnabled",
                     details::ChassisCapabilities::_property_ChassisPowerInterlockEnabled
                        .data(),
                     _callback_get_ChassisPowerInterlockEnabled,
                     _callback_set_ChassisPowerInterlockEnabled,
                     vtable::property_::emits_change),
    vtable::property("FRUDeviceAddress",
                     details::ChassisCapabilities::_property_FRUDeviceAddress
                        .data(),
                     _callback_get_FRUDeviceAddress,
                     _callback_set_FRUDeviceAddress,
                     vtable::property_::emits_change),
    vtable::property("SDRDeviceAddress",
                     details::ChassisCapabilities::_property_SDRDeviceAddress
                        .data(),
                     _callback_get_SDRDeviceAddress,
                     _callback_set_SDRDeviceAddress,
                     vtable::property_::emits_change),
    vtable::property("SELDeviceAddress",
                     details::ChassisCapabilities::_property_SELDeviceAddress
                        .data(),
                     _callback_get_SELDeviceAddress,
                     _callback_set_SELDeviceAddress,
                     vtable::property_::emits_change),
    vtable::property("SMDeviceAddress",
                     details::ChassisCapabilities::_property_SMDeviceAddress
                        .data(),
                     _callback_get_SMDeviceAddress,
                     _callback_set_SMDeviceAddress,
                     vtable::property_::emits_change),
    vtable::property("BridgeDeviceAddress",
                     details::ChassisCapabilities::_property_BridgeDeviceAddress
                        .data(),
                     _callback_get_BridgeDeviceAddress,
                     _callback_set_BridgeDeviceAddress,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

