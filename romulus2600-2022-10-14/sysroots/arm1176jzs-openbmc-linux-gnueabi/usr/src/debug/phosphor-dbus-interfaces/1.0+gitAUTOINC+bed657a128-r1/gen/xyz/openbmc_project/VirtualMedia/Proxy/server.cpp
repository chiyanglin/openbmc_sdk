#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/VirtualMedia/Proxy/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

Proxy::Proxy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_VirtualMedia_Proxy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Proxy::_callback_Mount(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Proxy*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mount(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Proxy
{
static const auto _param_Mount =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Mount =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}

int Proxy::_callback_Unmount(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Proxy*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->unmount(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Proxy
{
static const auto _param_Unmount =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Unmount =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}


void Proxy::completion(
            int32_t result)
{
    auto& i = _xyz_openbmc_project_VirtualMedia_Proxy_interface;
    auto m = i.new_signal("Completion");

    m.append(result);
    m.signal_send();
}

namespace details
{
namespace Proxy
{
static const auto _signal_Completion =
        utility::tuple_to_array(message::types::type_id<
                int32_t>());
}
}



const vtable_t Proxy::_vtable[] = {
    vtable::start(),

    vtable::method("Mount",
                   details::Proxy::_param_Mount
                        .data(),
                   details::Proxy::_return_Mount
                        .data(),
                   _callback_Mount),

    vtable::method("Unmount",
                   details::Proxy::_param_Unmount
                        .data(),
                   details::Proxy::_return_Unmount
                        .data(),
                   _callback_Unmount),

    vtable::signal("Completion",
                   details::Proxy::_signal_Completion
                        .data()),
    vtable::end()
};

} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

