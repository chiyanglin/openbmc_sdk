#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace server
{

class PDR
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PDR() = delete;
        PDR(const PDR&) = delete;
        PDR& operator=(const PDR&) = delete;
        PDR(PDR&&) = delete;
        PDR& operator=(PDR&&) = delete;
        virtual ~PDR() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PDR(bus_t& bus, const char* path);



        /** @brief Implementation for FindStateEffecterPDR
         *  Obtain the state effecter PDR, for the input TID, entity ID and state set id. If multiple PLDM entity instances(in state effecter PDRs) match the EntityId and StateSetId below,all the corresponding state effecter PDRs are returned. More than one PDR can be returned for the same state set id.
When the PDR for the given input TID, entity id and state set id is not found, then the xyz.openbmc_project.Common.Error.ResourceNotFound exception will be thrown.
         *
         *  @param[in] tid - A terminus id.
         *  @param[in] entityID - A numeric value that represents an entity that can be associated to a PLDM state set.
More information is found at http://dmtf.org/sites/default/files/standards/documents/DSP0249_1.0.0.pdf section 7.
         *  @param[in] stateSetId - A numeric value that identifies the PLDM State Set that is used with this sensor.
More information is found at http://dmtf.org/sites/default/files/standards/documents/DSP0249_1.0.0.pdf section 6.
         *
         *  @return stateEffecterPDR[std::vector<std::vector<uint8_t>>] - Array of State Effecter PDRs, where a PDR is an array[byte]. Multiple PDRs of the format present in table 89 of DSP0248 version 1.2.0 are returned. Each of the PDRs returned has the common header along with all the other fields in table 89 and 90 of DSP0248 of version 1.2.0 .
         */
        virtual std::vector<std::vector<uint8_t>> findStateEffecterPDR(
            uint8_t tid,
            uint16_t entityID,
            uint16_t stateSetId) = 0;

        /** @brief Implementation for FindStateSensorPDR
         *  Obtain the state sensor PDR, for the input TID, entity ID and state set id. If multiple PLDM entity instances (in state sensor PDRs) match the EntityId and StateSetId below, all the corresponding state sensor PDRs are returned. More than one PDR can be returned for the same state set id.
When the PDR for the given input TID, entity id and state set id is not found, then the xyz.openbmc_project.Common.Error.ResourceNotFound exception will be thrown.
         *
         *  @param[in] tid - A terminus id.
         *  @param[in] entityID - A numeric value that represents an entity that can be associated to a PLDM state set. More information is found at http://dmtf.org/sites/default/files/standards/documents/DSP0249_1.0.0.pdf section 7.
         *  @param[in] stateSetId - A numeric value that identifies the PLDM State Set that is used with this sensor. More information is found at http://dmtf.org/sites/default/files/standards/documents/DSP0249_1.0.0.pdf section 6.
         *
         *  @return stateSensorPDR[std::vector<std::vector<uint8_t>>] - Array of State Sensor PDRs, where a PDR is an array[byte]. Multiple PDRs of the format present in table 80 of DSP0248 version 1.2.0 are returned. Each of the PDRs returned has the common header along with all the other fields in table 80 and 81 of DSP0248 of version 1.2.0 .
         */
        virtual std::vector<std::vector<uint8_t>> findStateSensorPDR(
            uint8_t tid,
            uint16_t entityID,
            uint16_t stateSetId) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_PLDM_PDR_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_PLDM_PDR_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.PLDM.PDR";

    private:

        /** @brief sd-bus callback for FindStateEffecterPDR
         */
        static int _callback_FindStateEffecterPDR(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for FindStateSensorPDR
         */
        static int _callback_FindStateSensorPDR(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_PLDM_PDR_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

