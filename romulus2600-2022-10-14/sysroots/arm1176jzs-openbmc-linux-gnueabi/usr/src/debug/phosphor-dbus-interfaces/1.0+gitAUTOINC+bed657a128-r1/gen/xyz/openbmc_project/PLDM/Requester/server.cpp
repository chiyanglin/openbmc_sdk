#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/PLDM/Requester/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace server
{

Requester::Requester(bus_t& bus, const char* path)
        : _xyz_openbmc_project_PLDM_Requester_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Requester::_callback_GetInstanceId(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Requester*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& eid)
                    {
                        return o->getInstanceId(
                                eid);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::TooManyResources& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Requester
{
static const auto _param_GetInstanceId =
        utility::tuple_to_array(message::types::type_id<
                uint8_t>());
static const auto _return_GetInstanceId =
        utility::tuple_to_array(message::types::type_id<
                uint8_t>());
}
}




const vtable_t Requester::_vtable[] = {
    vtable::start(),

    vtable::method("GetInstanceId",
                   details::Requester::_param_GetInstanceId
                        .data(),
                   details::Requester::_return_GetInstanceId
                        .data(),
                   _callback_GetInstanceId),
    vtable::end()
};

} // namespace server
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

