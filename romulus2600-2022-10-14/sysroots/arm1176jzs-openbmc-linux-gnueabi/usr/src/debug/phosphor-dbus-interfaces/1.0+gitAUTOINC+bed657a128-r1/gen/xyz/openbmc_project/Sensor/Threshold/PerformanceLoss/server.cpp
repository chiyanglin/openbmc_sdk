#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Threshold/PerformanceLoss/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

PerformanceLoss::PerformanceLoss(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PerformanceLoss::PerformanceLoss(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PerformanceLoss(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}



void PerformanceLoss::performanceLossHighAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface;
    auto m = i.new_signal("PerformanceLossHighAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace PerformanceLoss
{
static const auto _signal_PerformanceLossHighAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void PerformanceLoss::performanceLossHighAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface;
    auto m = i.new_signal("PerformanceLossHighAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace PerformanceLoss
{
static const auto _signal_PerformanceLossHighAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void PerformanceLoss::performanceLossLowAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface;
    auto m = i.new_signal("PerformanceLossLowAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace PerformanceLoss
{
static const auto _signal_PerformanceLossLowAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void PerformanceLoss::performanceLossLowAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface;
    auto m = i.new_signal("PerformanceLossLowAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace PerformanceLoss
{
static const auto _signal_PerformanceLossLowAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}


auto PerformanceLoss::performanceLossHigh() const ->
        double
{
    return _performanceLossHigh;
}

int PerformanceLoss::_callback_get_PerformanceLossHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->performanceLossHigh();
                    }
                ));
    }
}

auto PerformanceLoss::performanceLossHigh(double value,
                                         bool skipSignal) ->
        double
{
    if (_performanceLossHigh != value)
    {
        _performanceLossHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface.property_changed("PerformanceLossHigh");
        }
    }

    return _performanceLossHigh;
}

auto PerformanceLoss::performanceLossHigh(double val) ->
        double
{
    return performanceLossHigh(val, false);
}

int PerformanceLoss::_callback_set_PerformanceLossHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->performanceLossHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PerformanceLoss
{
static const auto _property_PerformanceLossHigh =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto PerformanceLoss::performanceLossLow() const ->
        double
{
    return _performanceLossLow;
}

int PerformanceLoss::_callback_get_PerformanceLossLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->performanceLossLow();
                    }
                ));
    }
}

auto PerformanceLoss::performanceLossLow(double value,
                                         bool skipSignal) ->
        double
{
    if (_performanceLossLow != value)
    {
        _performanceLossLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface.property_changed("PerformanceLossLow");
        }
    }

    return _performanceLossLow;
}

auto PerformanceLoss::performanceLossLow(double val) ->
        double
{
    return performanceLossLow(val, false);
}

int PerformanceLoss::_callback_set_PerformanceLossLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->performanceLossLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PerformanceLoss
{
static const auto _property_PerformanceLossLow =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto PerformanceLoss::performanceLossAlarmHigh() const ->
        bool
{
    return _performanceLossAlarmHigh;
}

int PerformanceLoss::_callback_get_PerformanceLossAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->performanceLossAlarmHigh();
                    }
                ));
    }
}

auto PerformanceLoss::performanceLossAlarmHigh(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_performanceLossAlarmHigh != value)
    {
        _performanceLossAlarmHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface.property_changed("PerformanceLossAlarmHigh");
        }
    }

    return _performanceLossAlarmHigh;
}

auto PerformanceLoss::performanceLossAlarmHigh(bool val) ->
        bool
{
    return performanceLossAlarmHigh(val, false);
}

int PerformanceLoss::_callback_set_PerformanceLossAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->performanceLossAlarmHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PerformanceLoss
{
static const auto _property_PerformanceLossAlarmHigh =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PerformanceLoss::performanceLossAlarmLow() const ->
        bool
{
    return _performanceLossAlarmLow;
}

int PerformanceLoss::_callback_get_PerformanceLossAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->performanceLossAlarmLow();
                    }
                ));
    }
}

auto PerformanceLoss::performanceLossAlarmLow(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_performanceLossAlarmLow != value)
    {
        _performanceLossAlarmLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_PerformanceLoss_interface.property_changed("PerformanceLossAlarmLow");
        }
    }

    return _performanceLossAlarmLow;
}

auto PerformanceLoss::performanceLossAlarmLow(bool val) ->
        bool
{
    return performanceLossAlarmLow(val, false);
}

int PerformanceLoss::_callback_set_PerformanceLossAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PerformanceLoss*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->performanceLossAlarmLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PerformanceLoss
{
static const auto _property_PerformanceLossAlarmLow =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void PerformanceLoss::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PerformanceLossHigh")
    {
        auto& v = std::get<double>(val);
        performanceLossHigh(v, skipSignal);
        return;
    }
    if (_name == "PerformanceLossLow")
    {
        auto& v = std::get<double>(val);
        performanceLossLow(v, skipSignal);
        return;
    }
    if (_name == "PerformanceLossAlarmHigh")
    {
        auto& v = std::get<bool>(val);
        performanceLossAlarmHigh(v, skipSignal);
        return;
    }
    if (_name == "PerformanceLossAlarmLow")
    {
        auto& v = std::get<bool>(val);
        performanceLossAlarmLow(v, skipSignal);
        return;
    }
}

auto PerformanceLoss::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PerformanceLossHigh")
    {
        return performanceLossHigh();
    }
    if (_name == "PerformanceLossLow")
    {
        return performanceLossLow();
    }
    if (_name == "PerformanceLossAlarmHigh")
    {
        return performanceLossAlarmHigh();
    }
    if (_name == "PerformanceLossAlarmLow")
    {
        return performanceLossAlarmLow();
    }

    return PropertiesVariant();
}


const vtable_t PerformanceLoss::_vtable[] = {
    vtable::start(),

    vtable::signal("PerformanceLossHighAlarmAsserted",
                   details::PerformanceLoss::_signal_PerformanceLossHighAlarmAsserted
                        .data()),

    vtable::signal("PerformanceLossHighAlarmDeasserted",
                   details::PerformanceLoss::_signal_PerformanceLossHighAlarmDeasserted
                        .data()),

    vtable::signal("PerformanceLossLowAlarmAsserted",
                   details::PerformanceLoss::_signal_PerformanceLossLowAlarmAsserted
                        .data()),

    vtable::signal("PerformanceLossLowAlarmDeasserted",
                   details::PerformanceLoss::_signal_PerformanceLossLowAlarmDeasserted
                        .data()),
    vtable::property("PerformanceLossHigh",
                     details::PerformanceLoss::_property_PerformanceLossHigh
                        .data(),
                     _callback_get_PerformanceLossHigh,
                     _callback_set_PerformanceLossHigh,
                     vtable::property_::emits_change),
    vtable::property("PerformanceLossLow",
                     details::PerformanceLoss::_property_PerformanceLossLow
                        .data(),
                     _callback_get_PerformanceLossLow,
                     _callback_set_PerformanceLossLow,
                     vtable::property_::emits_change),
    vtable::property("PerformanceLossAlarmHigh",
                     details::PerformanceLoss::_property_PerformanceLossAlarmHigh
                        .data(),
                     _callback_get_PerformanceLossAlarmHigh,
                     _callback_set_PerformanceLossAlarmHigh,
                     vtable::property_::emits_change),
    vtable::property("PerformanceLossAlarmLow",
                     details::PerformanceLoss::_property_PerformanceLossAlarmLow
                        .data(),
                     _callback_get_PerformanceLossAlarmLow,
                     _callback_set_PerformanceLossAlarmLow,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

