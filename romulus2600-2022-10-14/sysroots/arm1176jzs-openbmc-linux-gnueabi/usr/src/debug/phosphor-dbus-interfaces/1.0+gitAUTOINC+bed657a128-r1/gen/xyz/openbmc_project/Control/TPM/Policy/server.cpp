#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/TPM/Policy/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace TPM
{
namespace server
{

Policy::Policy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_TPM_Policy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Policy::Policy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Policy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Policy::tpmEnable() const ->
        bool
{
    return _tpmEnable;
}

int Policy::_callback_get_TPMEnable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Policy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tpmEnable();
                    }
                ));
    }
}

auto Policy::tpmEnable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_tpmEnable != value)
    {
        _tpmEnable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_TPM_Policy_interface.property_changed("TPMEnable");
        }
    }

    return _tpmEnable;
}

auto Policy::tpmEnable(bool val) ->
        bool
{
    return tpmEnable(val, false);
}

int Policy::_callback_set_TPMEnable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Policy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->tpmEnable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Policy
{
static const auto _property_TPMEnable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Policy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "TPMEnable")
    {
        auto& v = std::get<bool>(val);
        tpmEnable(v, skipSignal);
        return;
    }
}

auto Policy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "TPMEnable")
    {
        return tpmEnable();
    }

    return PropertiesVariant();
}


const vtable_t Policy::_vtable[] = {
    vtable::start(),
    vtable::property("TPMEnable",
                     details::Policy::_property_TPMEnable
                        .data(),
                     _callback_get_TPMEnable,
                     _callback_set_TPMEnable,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace TPM
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

