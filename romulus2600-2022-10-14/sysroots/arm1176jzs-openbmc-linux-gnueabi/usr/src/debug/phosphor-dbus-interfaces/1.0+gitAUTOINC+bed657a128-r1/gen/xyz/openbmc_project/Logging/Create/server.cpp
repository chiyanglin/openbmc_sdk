#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Create/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_Create(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& message, xyz::openbmc_project::Logging::server::Entry::Level&& severity, std::map<std::string, std::string>&& additionalData)
                    {
                        return o->create(
                                message, severity, additionalData);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_Create =
        utility::tuple_to_array(message::types::type_id<
                std::string, xyz::openbmc_project::Logging::server::Entry::Level, std::map<std::string, std::string>>());
static const auto _return_Create =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Create::_callback_CreateWithFFDCFiles(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& message, xyz::openbmc_project::Logging::server::Entry::Level&& severity, std::map<std::string, std::string>&& additionalData, std::vector<std::tuple<FFDCFormat, uint8_t, uint8_t, sdbusplus::message::unix_fd>>&& ffdc)
                    {
                        return o->createWithFFDCFiles(
                                message, severity, additionalData, ffdc);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_CreateWithFFDCFiles =
        utility::tuple_to_array(message::types::type_id<
                std::string, xyz::openbmc_project::Logging::server::Entry::Level, std::map<std::string, std::string>, std::vector<std::tuple<sdbusplus::xyz::openbmc_project::Logging::server::Create::FFDCFormat, uint8_t, uint8_t, sdbusplus::message::unix_fd>>>());
static const auto _return_CreateWithFFDCFiles =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




namespace
{
/** String to enum mapping for Create::FFDCFormat */
static const std::tuple<const char*, Create::FFDCFormat> mappingCreateFFDCFormat[] =
        {
            std::make_tuple( "xyz.openbmc_project.Logging.Create.FFDCFormat.JSON",                 Create::FFDCFormat::JSON ),
            std::make_tuple( "xyz.openbmc_project.Logging.Create.FFDCFormat.CBOR",                 Create::FFDCFormat::CBOR ),
            std::make_tuple( "xyz.openbmc_project.Logging.Create.FFDCFormat.Text",                 Create::FFDCFormat::Text ),
            std::make_tuple( "xyz.openbmc_project.Logging.Create.FFDCFormat.Custom",                 Create::FFDCFormat::Custom ),
        };

} // anonymous namespace

auto Create::convertStringToFFDCFormat(const std::string& s) noexcept ->
        std::optional<FFDCFormat>
{
    auto i = std::find_if(
            std::begin(mappingCreateFFDCFormat),
            std::end(mappingCreateFFDCFormat),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCreateFFDCFormat) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Create::convertFFDCFormatFromString(const std::string& s) ->
        FFDCFormat
{
    auto r = convertStringToFFDCFormat(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Create::convertFFDCFormatToString(Create::FFDCFormat v)
{
    auto i = std::find_if(
            std::begin(mappingCreateFFDCFormat),
            std::end(mappingCreateFFDCFormat),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCreateFFDCFormat))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("Create",
                   details::Create::_param_Create
                        .data(),
                   details::Create::_return_Create
                        .data(),
                   _callback_Create),

    vtable::method("CreateWithFFDCFiles",
                   details::Create::_param_CreateWithFFDCFiles
                        .data(),
                   details::Create::_return_CreateWithFFDCFiles
                        .data(),
                   _callback_CreateWithFFDCFiles),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

