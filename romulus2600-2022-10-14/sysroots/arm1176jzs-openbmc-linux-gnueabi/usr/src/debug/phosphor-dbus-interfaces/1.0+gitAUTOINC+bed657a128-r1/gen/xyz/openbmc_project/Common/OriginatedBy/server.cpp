#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/OriginatedBy/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

OriginatedBy::OriginatedBy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_OriginatedBy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

OriginatedBy::OriginatedBy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : OriginatedBy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto OriginatedBy::originatorId() const ->
        std::string
{
    return _originatorId;
}

int OriginatedBy::_callback_get_OriginatorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OriginatedBy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->originatorId();
                    }
                ));
    }
}

auto OriginatedBy::originatorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_originatorId != value)
    {
        _originatorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_OriginatedBy_interface.property_changed("OriginatorId");
        }
    }

    return _originatorId;
}

auto OriginatedBy::originatorId(std::string val) ->
        std::string
{
    return originatorId(val, false);
}

int OriginatedBy::_callback_set_OriginatorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OriginatedBy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->originatorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace OriginatedBy
{
static const auto _property_OriginatorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto OriginatedBy::originatorType() const ->
        OriginatorTypes
{
    return _originatorType;
}

int OriginatedBy::_callback_get_OriginatorType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OriginatedBy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->originatorType();
                    }
                ));
    }
}

auto OriginatedBy::originatorType(OriginatorTypes value,
                                         bool skipSignal) ->
        OriginatorTypes
{
    if (_originatorType != value)
    {
        _originatorType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_OriginatedBy_interface.property_changed("OriginatorType");
        }
    }

    return _originatorType;
}

auto OriginatedBy::originatorType(OriginatorTypes val) ->
        OriginatorTypes
{
    return originatorType(val, false);
}

int OriginatedBy::_callback_set_OriginatorType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OriginatedBy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](OriginatorTypes&& arg)
                    {
                        o->originatorType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace OriginatedBy
{
static const auto _property_OriginatorType =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Common::server::OriginatedBy::OriginatorTypes>());
}
}

void OriginatedBy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "OriginatorId")
    {
        auto& v = std::get<std::string>(val);
        originatorId(v, skipSignal);
        return;
    }
    if (_name == "OriginatorType")
    {
        auto& v = std::get<OriginatorTypes>(val);
        originatorType(v, skipSignal);
        return;
    }
}

auto OriginatedBy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "OriginatorId")
    {
        return originatorId();
    }
    if (_name == "OriginatorType")
    {
        return originatorType();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for OriginatedBy::OriginatorTypes */
static const std::tuple<const char*, OriginatedBy::OriginatorTypes> mappingOriginatedByOriginatorTypes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Common.OriginatedBy.OriginatorTypes.Client",                 OriginatedBy::OriginatorTypes::Client ),
            std::make_tuple( "xyz.openbmc_project.Common.OriginatedBy.OriginatorTypes.Internal",                 OriginatedBy::OriginatorTypes::Internal ),
            std::make_tuple( "xyz.openbmc_project.Common.OriginatedBy.OriginatorTypes.SupportingService",                 OriginatedBy::OriginatorTypes::SupportingService ),
        };

} // anonymous namespace

auto OriginatedBy::convertStringToOriginatorTypes(const std::string& s) noexcept ->
        std::optional<OriginatorTypes>
{
    auto i = std::find_if(
            std::begin(mappingOriginatedByOriginatorTypes),
            std::end(mappingOriginatedByOriginatorTypes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingOriginatedByOriginatorTypes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto OriginatedBy::convertOriginatorTypesFromString(const std::string& s) ->
        OriginatorTypes
{
    auto r = convertStringToOriginatorTypes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string OriginatedBy::convertOriginatorTypesToString(OriginatedBy::OriginatorTypes v)
{
    auto i = std::find_if(
            std::begin(mappingOriginatedByOriginatorTypes),
            std::end(mappingOriginatedByOriginatorTypes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingOriginatedByOriginatorTypes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t OriginatedBy::_vtable[] = {
    vtable::start(),
    vtable::property("OriginatorId",
                     details::OriginatedBy::_property_OriginatorId
                        .data(),
                     _callback_get_OriginatorId,
                     _callback_set_OriginatorId,
                     vtable::property_::emits_change),
    vtable::property("OriginatorType",
                     details::OriginatedBy::_property_OriginatorType
                        .data(),
                     _callback_get_OriginatorType,
                     _callback_set_OriginatorType,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

