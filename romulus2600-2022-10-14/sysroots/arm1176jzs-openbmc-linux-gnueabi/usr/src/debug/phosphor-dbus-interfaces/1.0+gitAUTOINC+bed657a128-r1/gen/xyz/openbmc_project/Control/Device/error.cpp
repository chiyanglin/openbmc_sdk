#include <xyz/openbmc_project/Control/Device/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Device
{
namespace Error
{
const char* WriteFailure::name() const noexcept
{
    return errName;
}
const char* WriteFailure::description() const noexcept
{
    return errDesc;
}
const char* WriteFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Device
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

