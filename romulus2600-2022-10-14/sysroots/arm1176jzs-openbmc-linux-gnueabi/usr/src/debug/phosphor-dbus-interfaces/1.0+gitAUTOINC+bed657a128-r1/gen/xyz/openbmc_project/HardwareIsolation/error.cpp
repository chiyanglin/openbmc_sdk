#include <xyz/openbmc_project/HardwareIsolation/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace Error
{
const char* IsolatedAlready::name() const noexcept
{
    return errName;
}
const char* IsolatedAlready::description() const noexcept
{
    return errDesc;
}
const char* IsolatedAlready::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

