#include <xyz/openbmc_project/Sensor/Device/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Device
{
namespace Error
{
const char* ReadFailure::name() const noexcept
{
    return errName;
}
const char* ReadFailure::description() const noexcept
{
    return errDesc;
}
const char* ReadFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Device
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

