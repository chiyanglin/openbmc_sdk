#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/ManufacturerExt/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

ManufacturerExt::ManufacturerExt(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_ManufacturerExt_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ManufacturerExt::ManufacturerExt(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ManufacturerExt(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ManufacturerExt::extendedMFGData() const ->
        std::map<std::string, std::string>
{
    return _extendedMFGData;
}

int ManufacturerExt::_callback_get_ExtendedMFGData(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ManufacturerExt*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->extendedMFGData();
                    }
                ));
    }
}

auto ManufacturerExt::extendedMFGData(std::map<std::string, std::string> value,
                                         bool skipSignal) ->
        std::map<std::string, std::string>
{
    if (_extendedMFGData != value)
    {
        _extendedMFGData = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_ManufacturerExt_interface.property_changed("ExtendedMFGData");
        }
    }

    return _extendedMFGData;
}

auto ManufacturerExt::extendedMFGData(std::map<std::string, std::string> val) ->
        std::map<std::string, std::string>
{
    return extendedMFGData(val, false);
}

int ManufacturerExt::_callback_set_ExtendedMFGData(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ManufacturerExt*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::map<std::string, std::string>&& arg)
                    {
                        o->extendedMFGData(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ManufacturerExt
{
static const auto _property_ExtendedMFGData =
    utility::tuple_to_array(message::types::type_id<
            std::map<std::string, std::string>>());
}
}

void ManufacturerExt::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ExtendedMFGData")
    {
        auto& v = std::get<std::map<std::string, std::string>>(val);
        extendedMFGData(v, skipSignal);
        return;
    }
}

auto ManufacturerExt::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ExtendedMFGData")
    {
        return extendedMFGData();
    }

    return PropertiesVariant();
}


const vtable_t ManufacturerExt::_vtable[] = {
    vtable::start(),
    vtable::property("ExtendedMFGData",
                     details::ManufacturerExt::_property_ExtendedMFGData
                        .data(),
                     _callback_get_ExtendedMFGData,
                     _callback_set_ExtendedMFGData,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

