#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/VirtualMedia/Process/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

Process::Process(bus_t& bus, const char* path)
        : _xyz_openbmc_project_VirtualMedia_Process_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Process::Process(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Process(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Process::active() const ->
        bool
{
    return _active;
}

int Process::_callback_get_Active(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Process*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->active();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Process::active(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_active != value)
    {
        _active = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_Process_interface.property_changed("Active");
        }
    }

    return _active;
}

auto Process::active(bool val) ->
        bool
{
    return active(val, false);
}


namespace details
{
namespace Process
{
static const auto _property_Active =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Process::exitCode() const ->
        int32_t
{
    return _exitCode;
}

int Process::_callback_get_ExitCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Process*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->exitCode();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Process::exitCode(int32_t value,
                                         bool skipSignal) ->
        int32_t
{
    if (_exitCode != value)
    {
        _exitCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_VirtualMedia_Process_interface.property_changed("ExitCode");
        }
    }

    return _exitCode;
}

auto Process::exitCode(int32_t val) ->
        int32_t
{
    return exitCode(val, false);
}


namespace details
{
namespace Process
{
static const auto _property_ExitCode =
    utility::tuple_to_array(message::types::type_id<
            int32_t>());
}
}

void Process::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Active")
    {
        auto& v = std::get<bool>(val);
        active(v, skipSignal);
        return;
    }
    if (_name == "ExitCode")
    {
        auto& v = std::get<int32_t>(val);
        exitCode(v, skipSignal);
        return;
    }
}

auto Process::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Active")
    {
        return active();
    }
    if (_name == "ExitCode")
    {
        return exitCode();
    }

    return PropertiesVariant();
}


const vtable_t Process::_vtable[] = {
    vtable::start(),
    vtable::property("Active",
                     details::Process::_property_Active
                        .data(),
                     _callback_get_Active,
                     vtable::property_::const_),
    vtable::property("ExitCode",
                     details::Process::_property_ExitCode
                        .data(),
                     _callback_get_ExitCode,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

