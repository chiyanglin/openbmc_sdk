#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Asset/server.hpp>








namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Asset::Asset(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Asset_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Asset::Asset(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Asset(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Asset::partNumber() const ->
        std::string
{
    return _partNumber;
}

int Asset::_callback_get_PartNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->partNumber();
                    }
                ));
    }
}

auto Asset::partNumber(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_partNumber != value)
    {
        _partNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("PartNumber");
        }
    }

    return _partNumber;
}

auto Asset::partNumber(std::string val) ->
        std::string
{
    return partNumber(val, false);
}

int Asset::_callback_set_PartNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->partNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_PartNumber =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Asset::serialNumber() const ->
        std::string
{
    return _serialNumber;
}

int Asset::_callback_get_SerialNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->serialNumber();
                    }
                ));
    }
}

auto Asset::serialNumber(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_serialNumber != value)
    {
        _serialNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("SerialNumber");
        }
    }

    return _serialNumber;
}

auto Asset::serialNumber(std::string val) ->
        std::string
{
    return serialNumber(val, false);
}

int Asset::_callback_set_SerialNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->serialNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_SerialNumber =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Asset::manufacturer() const ->
        std::string
{
    return _manufacturer;
}

int Asset::_callback_get_Manufacturer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->manufacturer();
                    }
                ));
    }
}

auto Asset::manufacturer(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_manufacturer != value)
    {
        _manufacturer = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("Manufacturer");
        }
    }

    return _manufacturer;
}

auto Asset::manufacturer(std::string val) ->
        std::string
{
    return manufacturer(val, false);
}

int Asset::_callback_set_Manufacturer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->manufacturer(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_Manufacturer =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Asset::buildDate() const ->
        std::string
{
    return _buildDate;
}

int Asset::_callback_get_BuildDate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->buildDate();
                    }
                ));
    }
}

auto Asset::buildDate(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_buildDate != value)
    {
        _buildDate = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("BuildDate");
        }
    }

    return _buildDate;
}

auto Asset::buildDate(std::string val) ->
        std::string
{
    return buildDate(val, false);
}

int Asset::_callback_set_BuildDate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->buildDate(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_BuildDate =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Asset::model() const ->
        std::string
{
    return _model;
}

int Asset::_callback_get_Model(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->model();
                    }
                ));
    }
}

auto Asset::model(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_model != value)
    {
        _model = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("Model");
        }
    }

    return _model;
}

auto Asset::model(std::string val) ->
        std::string
{
    return model(val, false);
}

int Asset::_callback_set_Model(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->model(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_Model =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Asset::subModel() const ->
        std::string
{
    return _subModel;
}

int Asset::_callback_get_SubModel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->subModel();
                    }
                ));
    }
}

auto Asset::subModel(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_subModel != value)
    {
        _subModel = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("SubModel");
        }
    }

    return _subModel;
}

auto Asset::subModel(std::string val) ->
        std::string
{
    return subModel(val, false);
}

int Asset::_callback_set_SubModel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->subModel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_SubModel =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Asset::sparePartNumber() const ->
        std::string
{
    return _sparePartNumber;
}

int Asset::_callback_get_SparePartNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sparePartNumber();
                    }
                ));
    }
}

auto Asset::sparePartNumber(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_sparePartNumber != value)
    {
        _sparePartNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Asset_interface.property_changed("SparePartNumber");
        }
    }

    return _sparePartNumber;
}

auto Asset::sparePartNumber(std::string val) ->
        std::string
{
    return sparePartNumber(val, false);
}

int Asset::_callback_set_SparePartNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Asset*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->sparePartNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Asset
{
static const auto _property_SparePartNumber =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Asset::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PartNumber")
    {
        auto& v = std::get<std::string>(val);
        partNumber(v, skipSignal);
        return;
    }
    if (_name == "SerialNumber")
    {
        auto& v = std::get<std::string>(val);
        serialNumber(v, skipSignal);
        return;
    }
    if (_name == "Manufacturer")
    {
        auto& v = std::get<std::string>(val);
        manufacturer(v, skipSignal);
        return;
    }
    if (_name == "BuildDate")
    {
        auto& v = std::get<std::string>(val);
        buildDate(v, skipSignal);
        return;
    }
    if (_name == "Model")
    {
        auto& v = std::get<std::string>(val);
        model(v, skipSignal);
        return;
    }
    if (_name == "SubModel")
    {
        auto& v = std::get<std::string>(val);
        subModel(v, skipSignal);
        return;
    }
    if (_name == "SparePartNumber")
    {
        auto& v = std::get<std::string>(val);
        sparePartNumber(v, skipSignal);
        return;
    }
}

auto Asset::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PartNumber")
    {
        return partNumber();
    }
    if (_name == "SerialNumber")
    {
        return serialNumber();
    }
    if (_name == "Manufacturer")
    {
        return manufacturer();
    }
    if (_name == "BuildDate")
    {
        return buildDate();
    }
    if (_name == "Model")
    {
        return model();
    }
    if (_name == "SubModel")
    {
        return subModel();
    }
    if (_name == "SparePartNumber")
    {
        return sparePartNumber();
    }

    return PropertiesVariant();
}


const vtable_t Asset::_vtable[] = {
    vtable::start(),
    vtable::property("PartNumber",
                     details::Asset::_property_PartNumber
                        .data(),
                     _callback_get_PartNumber,
                     _callback_set_PartNumber,
                     vtable::property_::emits_change),
    vtable::property("SerialNumber",
                     details::Asset::_property_SerialNumber
                        .data(),
                     _callback_get_SerialNumber,
                     _callback_set_SerialNumber,
                     vtable::property_::emits_change),
    vtable::property("Manufacturer",
                     details::Asset::_property_Manufacturer
                        .data(),
                     _callback_get_Manufacturer,
                     _callback_set_Manufacturer,
                     vtable::property_::emits_change),
    vtable::property("BuildDate",
                     details::Asset::_property_BuildDate
                        .data(),
                     _callback_get_BuildDate,
                     _callback_set_BuildDate,
                     vtable::property_::emits_change),
    vtable::property("Model",
                     details::Asset::_property_Model
                        .data(),
                     _callback_get_Model,
                     _callback_set_Model,
                     vtable::property_::emits_change),
    vtable::property("SubModel",
                     details::Asset::_property_SubModel
                        .data(),
                     _callback_get_SubModel,
                     _callback_set_SubModel,
                     vtable::property_::emits_change),
    vtable::property("SparePartNumber",
                     details::Asset::_property_SparePartNumber
                        .data(),
                     _callback_get_SparePartNumber,
                     _callback_set_SparePartNumber,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

