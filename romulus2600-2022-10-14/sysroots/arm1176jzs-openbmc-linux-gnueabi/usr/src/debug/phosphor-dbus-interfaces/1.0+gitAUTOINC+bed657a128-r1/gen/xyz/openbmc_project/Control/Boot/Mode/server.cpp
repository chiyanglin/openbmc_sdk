#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Boot/Mode/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace server
{

Mode::Mode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Boot_Mode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Mode::Mode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Mode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Mode::bootMode() const ->
        Modes
{
    return _bootMode;
}

int Mode::_callback_get_BootMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bootMode();
                    }
                ));
    }
}

auto Mode::bootMode(Modes value,
                                         bool skipSignal) ->
        Modes
{
    if (_bootMode != value)
    {
        _bootMode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Boot_Mode_interface.property_changed("BootMode");
        }
    }

    return _bootMode;
}

auto Mode::bootMode(Modes val) ->
        Modes
{
    return bootMode(val, false);
}

int Mode::_callback_set_BootMode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Modes&& arg)
                    {
                        o->bootMode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Mode
{
static const auto _property_BootMode =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Boot::server::Mode::Modes>());
}
}

void Mode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BootMode")
    {
        auto& v = std::get<Modes>(val);
        bootMode(v, skipSignal);
        return;
    }
}

auto Mode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BootMode")
    {
        return bootMode();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Mode::Modes */
static const std::tuple<const char*, Mode::Modes> mappingModeModes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Mode.Modes.Regular",                 Mode::Modes::Regular ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Mode.Modes.Safe",                 Mode::Modes::Safe ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Mode.Modes.Setup",                 Mode::Modes::Setup ),
        };

} // anonymous namespace

auto Mode::convertStringToModes(const std::string& s) noexcept ->
        std::optional<Modes>
{
    auto i = std::find_if(
            std::begin(mappingModeModes),
            std::end(mappingModeModes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingModeModes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Mode::convertModesFromString(const std::string& s) ->
        Modes
{
    auto r = convertStringToModes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Mode::convertModesToString(Mode::Modes v)
{
    auto i = std::find_if(
            std::begin(mappingModeModes),
            std::end(mappingModeModes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingModeModes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Mode::_vtable[] = {
    vtable::start(),
    vtable::property("BootMode",
                     details::Mode::_property_BootMode
                        .data(),
                     _callback_get_BootMode,
                     _callback_set_BootMode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

