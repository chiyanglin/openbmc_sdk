#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Boot/RebootPolicy/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace server
{

RebootPolicy::RebootPolicy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Boot_RebootPolicy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

RebootPolicy::RebootPolicy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : RebootPolicy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto RebootPolicy::autoReboot() const ->
        bool
{
    return _autoReboot;
}

int RebootPolicy::_callback_get_AutoReboot(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RebootPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->autoReboot();
                    }
                ));
    }
}

auto RebootPolicy::autoReboot(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_autoReboot != value)
    {
        _autoReboot = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Boot_RebootPolicy_interface.property_changed("AutoReboot");
        }
    }

    return _autoReboot;
}

auto RebootPolicy::autoReboot(bool val) ->
        bool
{
    return autoReboot(val, false);
}

int RebootPolicy::_callback_set_AutoReboot(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RebootPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->autoReboot(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RebootPolicy
{
static const auto _property_AutoReboot =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void RebootPolicy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AutoReboot")
    {
        auto& v = std::get<bool>(val);
        autoReboot(v, skipSignal);
        return;
    }
}

auto RebootPolicy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AutoReboot")
    {
        return autoReboot();
    }

    return PropertiesVariant();
}


const vtable_t RebootPolicy::_vtable[] = {
    vtable::start(),
    vtable::property("AutoReboot",
                     details::RebootPolicy::_property_AutoReboot
                        .data(),
                     _callback_get_AutoReboot,
                     _callback_set_AutoReboot,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

