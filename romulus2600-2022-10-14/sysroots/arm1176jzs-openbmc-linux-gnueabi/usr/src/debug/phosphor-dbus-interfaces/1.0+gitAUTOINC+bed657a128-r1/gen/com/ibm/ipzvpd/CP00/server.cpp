#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/CP00/server.hpp>













namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

CP00::CP00(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_CP00_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CP00::CP00(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CP00(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CP00::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int CP00::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto CP00::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto CP00::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int CP00::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int CP00::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto CP00::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto CP00::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int CP00::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::pg() const ->
        std::vector<uint8_t>
{
    return _pg;
}

int CP00::_callback_get_PG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pg();
                    }
                ));
    }
}

auto CP00::pg(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pg != value)
    {
        _pg = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("PG");
        }
    }

    return _pg;
}

auto CP00::pg(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pg(val, false);
}

int CP00::_callback_set_PG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pg(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_PG =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::mk() const ->
        std::vector<uint8_t>
{
    return _mk;
}

int CP00::_callback_get_MK(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mk();
                    }
                ));
    }
}

auto CP00::mk(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_mk != value)
    {
        _mk = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("MK");
        }
    }

    return _mk;
}

auto CP00::mk(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return mk(val, false);
}

int CP00::_callback_set_MK(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->mk(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_MK =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::pdg() const ->
        std::vector<uint8_t>
{
    return _pdg;
}

int CP00::_callback_get_PD_G(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdg();
                    }
                ));
    }
}

auto CP00::pdg(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdg != value)
    {
        _pdg = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("PD_G");
        }
    }

    return _pdg;
}

auto CP00::pdg(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdg(val, false);
}

int CP00::_callback_set_PD_G(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdg(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_PD_G =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::pdr() const ->
        std::vector<uint8_t>
{
    return _pdr;
}

int CP00::_callback_get_PD_R(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdr();
                    }
                ));
    }
}

auto CP00::pdr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdr != value)
    {
        _pdr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("PD_R");
        }
    }

    return _pdr;
}

auto CP00::pdr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdr(val, false);
}

int CP00::_callback_set_PD_R(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_PD_R =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::sb() const ->
        std::vector<uint8_t>
{
    return _sb;
}

int CP00::_callback_get_SB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sb();
                    }
                ));
    }
}

auto CP00::sb(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_sb != value)
    {
        _sb = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("SB");
        }
    }

    return _sb;
}

auto CP00::sb(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return sb(val, false);
}

int CP00::_callback_set_SB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->sb(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_SB =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::pz() const ->
        std::vector<uint8_t>
{
    return _pz;
}

int CP00::_callback_get_PZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pz();
                    }
                ));
    }
}

auto CP00::pz(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pz != value)
    {
        _pz = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("PZ");
        }
    }

    return _pz;
}

auto CP00::pz(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pz(val, false);
}

int CP00::_callback_set_PZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_PZ =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::aw() const ->
        std::vector<uint8_t>
{
    return _aw;
}

int CP00::_callback_get_AW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->aw();
                    }
                ));
    }
}

auto CP00::aw(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_aw != value)
    {
        _aw = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("AW");
        }
    }

    return _aw;
}

auto CP00::aw(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return aw(val, false);
}

int CP00::_callback_set_AW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->aw(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_AW =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::pdp() const ->
        std::vector<uint8_t>
{
    return _pdp;
}

int CP00::_callback_get_PD_P(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdp();
                    }
                ));
    }
}

auto CP00::pdp(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdp != value)
    {
        _pdp = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("PD_P");
        }
    }

    return _pdp;
}

auto CP00::pdp(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdp(val, false);
}

int CP00::_callback_set_PD_P(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdp(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_PD_P =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int CP00::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto CP00::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto CP00::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int CP00::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto CP00::d5() const ->
        std::vector<uint8_t>
{
    return _d5;
}

int CP00::_callback_get_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d5();
                    }
                ));
    }
}

auto CP00::d5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d5 != value)
    {
        _d5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_CP00_interface.property_changed("D5");
        }
    }

    return _d5;
}

auto CP00::d5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d5(val, false);
}

int CP00::_callback_set_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CP00*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CP00
{
static const auto _property_D5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void CP00::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "PG")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pg(v, skipSignal);
        return;
    }
    if (_name == "MK")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        mk(v, skipSignal);
        return;
    }
    if (_name == "PD_G")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdg(v, skipSignal);
        return;
    }
    if (_name == "PD_R")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdr(v, skipSignal);
        return;
    }
    if (_name == "SB")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        sb(v, skipSignal);
        return;
    }
    if (_name == "PZ")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pz(v, skipSignal);
        return;
    }
    if (_name == "AW")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        aw(v, skipSignal);
        return;
    }
    if (_name == "PD_P")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdp(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
    if (_name == "D5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d5(v, skipSignal);
        return;
    }
}

auto CP00::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "PG")
    {
        return pg();
    }
    if (_name == "MK")
    {
        return mk();
    }
    if (_name == "PD_G")
    {
        return pdg();
    }
    if (_name == "PD_R")
    {
        return pdr();
    }
    if (_name == "SB")
    {
        return sb();
    }
    if (_name == "PZ")
    {
        return pz();
    }
    if (_name == "AW")
    {
        return aw();
    }
    if (_name == "PD_P")
    {
        return pdp();
    }
    if (_name == "D4")
    {
        return d4();
    }
    if (_name == "D5")
    {
        return d5();
    }

    return PropertiesVariant();
}


const vtable_t CP00::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::CP00::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::CP00::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("PG",
                     details::CP00::_property_PG
                        .data(),
                     _callback_get_PG,
                     _callback_set_PG,
                     vtable::property_::emits_change),
    vtable::property("MK",
                     details::CP00::_property_MK
                        .data(),
                     _callback_get_MK,
                     _callback_set_MK,
                     vtable::property_::emits_change),
    vtable::property("PD_G",
                     details::CP00::_property_PD_G
                        .data(),
                     _callback_get_PD_G,
                     _callback_set_PD_G,
                     vtable::property_::emits_change),
    vtable::property("PD_R",
                     details::CP00::_property_PD_R
                        .data(),
                     _callback_get_PD_R,
                     _callback_set_PD_R,
                     vtable::property_::emits_change),
    vtable::property("SB",
                     details::CP00::_property_SB
                        .data(),
                     _callback_get_SB,
                     _callback_set_SB,
                     vtable::property_::emits_change),
    vtable::property("PZ",
                     details::CP00::_property_PZ
                        .data(),
                     _callback_get_PZ,
                     _callback_set_PZ,
                     vtable::property_::emits_change),
    vtable::property("AW",
                     details::CP00::_property_AW
                        .data(),
                     _callback_get_AW,
                     _callback_set_AW,
                     vtable::property_::emits_change),
    vtable::property("PD_P",
                     details::CP00::_property_PD_P
                        .data(),
                     _callback_get_PD_P,
                     _callback_set_PD_P,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::CP00::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::property("D5",
                     details::CP00::_property_D5
                        .data(),
                     _callback_get_D5,
                     _callback_set_D5,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

