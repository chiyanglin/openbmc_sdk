#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/DHCPConfiguration/server.hpp>






namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

DHCPConfiguration::DHCPConfiguration(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_DHCPConfiguration_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

DHCPConfiguration::DHCPConfiguration(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : DHCPConfiguration(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto DHCPConfiguration::dnsEnabled() const ->
        bool
{
    return _dnsEnabled;
}

int DHCPConfiguration::_callback_get_DNSEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dnsEnabled();
                    }
                ));
    }
}

auto DHCPConfiguration::dnsEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_dnsEnabled != value)
    {
        _dnsEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_DHCPConfiguration_interface.property_changed("DNSEnabled");
        }
    }

    return _dnsEnabled;
}

auto DHCPConfiguration::dnsEnabled(bool val) ->
        bool
{
    return dnsEnabled(val, false);
}

int DHCPConfiguration::_callback_set_DNSEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->dnsEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DHCPConfiguration
{
static const auto _property_DNSEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto DHCPConfiguration::ntpEnabled() const ->
        bool
{
    return _ntpEnabled;
}

int DHCPConfiguration::_callback_get_NTPEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ntpEnabled();
                    }
                ));
    }
}

auto DHCPConfiguration::ntpEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_ntpEnabled != value)
    {
        _ntpEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_DHCPConfiguration_interface.property_changed("NTPEnabled");
        }
    }

    return _ntpEnabled;
}

auto DHCPConfiguration::ntpEnabled(bool val) ->
        bool
{
    return ntpEnabled(val, false);
}

int DHCPConfiguration::_callback_set_NTPEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->ntpEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DHCPConfiguration
{
static const auto _property_NTPEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto DHCPConfiguration::hostNameEnabled() const ->
        bool
{
    return _hostNameEnabled;
}

int DHCPConfiguration::_callback_get_HostNameEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hostNameEnabled();
                    }
                ));
    }
}

auto DHCPConfiguration::hostNameEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hostNameEnabled != value)
    {
        _hostNameEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_DHCPConfiguration_interface.property_changed("HostNameEnabled");
        }
    }

    return _hostNameEnabled;
}

auto DHCPConfiguration::hostNameEnabled(bool val) ->
        bool
{
    return hostNameEnabled(val, false);
}

int DHCPConfiguration::_callback_set_HostNameEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hostNameEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DHCPConfiguration
{
static const auto _property_HostNameEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto DHCPConfiguration::domainEnabled() const ->
        bool
{
    return _domainEnabled;
}

int DHCPConfiguration::_callback_get_DomainEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->domainEnabled();
                    }
                ));
    }
}

auto DHCPConfiguration::domainEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_domainEnabled != value)
    {
        _domainEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_DHCPConfiguration_interface.property_changed("DomainEnabled");
        }
    }

    return _domainEnabled;
}

auto DHCPConfiguration::domainEnabled(bool val) ->
        bool
{
    return domainEnabled(val, false);
}

int DHCPConfiguration::_callback_set_DomainEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->domainEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DHCPConfiguration
{
static const auto _property_DomainEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto DHCPConfiguration::sendHostNameEnabled() const ->
        bool
{
    return _sendHostNameEnabled;
}

int DHCPConfiguration::_callback_get_SendHostNameEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sendHostNameEnabled();
                    }
                ));
    }
}

auto DHCPConfiguration::sendHostNameEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_sendHostNameEnabled != value)
    {
        _sendHostNameEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_DHCPConfiguration_interface.property_changed("SendHostNameEnabled");
        }
    }

    return _sendHostNameEnabled;
}

auto DHCPConfiguration::sendHostNameEnabled(bool val) ->
        bool
{
    return sendHostNameEnabled(val, false);
}

int DHCPConfiguration::_callback_set_SendHostNameEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DHCPConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->sendHostNameEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DHCPConfiguration
{
static const auto _property_SendHostNameEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void DHCPConfiguration::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "DNSEnabled")
    {
        auto& v = std::get<bool>(val);
        dnsEnabled(v, skipSignal);
        return;
    }
    if (_name == "NTPEnabled")
    {
        auto& v = std::get<bool>(val);
        ntpEnabled(v, skipSignal);
        return;
    }
    if (_name == "HostNameEnabled")
    {
        auto& v = std::get<bool>(val);
        hostNameEnabled(v, skipSignal);
        return;
    }
    if (_name == "DomainEnabled")
    {
        auto& v = std::get<bool>(val);
        domainEnabled(v, skipSignal);
        return;
    }
    if (_name == "SendHostNameEnabled")
    {
        auto& v = std::get<bool>(val);
        sendHostNameEnabled(v, skipSignal);
        return;
    }
}

auto DHCPConfiguration::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "DNSEnabled")
    {
        return dnsEnabled();
    }
    if (_name == "NTPEnabled")
    {
        return ntpEnabled();
    }
    if (_name == "HostNameEnabled")
    {
        return hostNameEnabled();
    }
    if (_name == "DomainEnabled")
    {
        return domainEnabled();
    }
    if (_name == "SendHostNameEnabled")
    {
        return sendHostNameEnabled();
    }

    return PropertiesVariant();
}


const vtable_t DHCPConfiguration::_vtable[] = {
    vtable::start(),
    vtable::property("DNSEnabled",
                     details::DHCPConfiguration::_property_DNSEnabled
                        .data(),
                     _callback_get_DNSEnabled,
                     _callback_set_DNSEnabled,
                     vtable::property_::emits_change),
    vtable::property("NTPEnabled",
                     details::DHCPConfiguration::_property_NTPEnabled
                        .data(),
                     _callback_get_NTPEnabled,
                     _callback_set_NTPEnabled,
                     vtable::property_::emits_change),
    vtable::property("HostNameEnabled",
                     details::DHCPConfiguration::_property_HostNameEnabled
                        .data(),
                     _callback_get_HostNameEnabled,
                     _callback_set_HostNameEnabled,
                     vtable::property_::emits_change),
    vtable::property("DomainEnabled",
                     details::DHCPConfiguration::_property_DomainEnabled
                        .data(),
                     _callback_get_DomainEnabled,
                     _callback_set_DomainEnabled,
                     vtable::property_::emits_change),
    vtable::property("SendHostNameEnabled",
                     details::DHCPConfiguration::_property_SendHostNameEnabled
                        .data(),
                     _callback_get_SendHostNameEnabled,
                     _callback_set_SendHostNameEnabled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

