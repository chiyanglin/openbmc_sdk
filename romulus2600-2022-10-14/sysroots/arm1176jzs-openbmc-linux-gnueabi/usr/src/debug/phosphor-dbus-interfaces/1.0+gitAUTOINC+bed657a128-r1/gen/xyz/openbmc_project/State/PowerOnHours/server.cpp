#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/PowerOnHours/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

PowerOnHours::PowerOnHours(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_PowerOnHours_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PowerOnHours::PowerOnHours(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PowerOnHours(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PowerOnHours::pohCounter() const ->
        uint32_t
{
    return _pohCounter;
}

int PowerOnHours::_callback_get_POHCounter(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerOnHours*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pohCounter();
                    }
                ));
    }
}

auto PowerOnHours::pohCounter(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_pohCounter != value)
    {
        _pohCounter = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_PowerOnHours_interface.property_changed("POHCounter");
        }
    }

    return _pohCounter;
}

auto PowerOnHours::pohCounter(uint32_t val) ->
        uint32_t
{
    return pohCounter(val, false);
}

int PowerOnHours::_callback_set_POHCounter(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerOnHours*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->pohCounter(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerOnHours
{
static const auto _property_POHCounter =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void PowerOnHours::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "POHCounter")
    {
        auto& v = std::get<uint32_t>(val);
        pohCounter(v, skipSignal);
        return;
    }
}

auto PowerOnHours::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "POHCounter")
    {
        return pohCounter();
    }

    return PropertiesVariant();
}


const vtable_t PowerOnHours::_vtable[] = {
    vtable::start(),
    vtable::property("POHCounter",
                     details::PowerOnHours::_property_POHCounter
                        .data(),
                     _callback_get_POHCounter,
                     _callback_set_POHCounter,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

