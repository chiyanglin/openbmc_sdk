#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/NetworkInterface/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

NetworkInterface::NetworkInterface(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_NetworkInterface_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

NetworkInterface::NetworkInterface(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : NetworkInterface(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto NetworkInterface::macAddress() const ->
        std::string
{
    return _macAddress;
}

int NetworkInterface::_callback_get_MACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NetworkInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->macAddress();
                    }
                ));
    }
}

auto NetworkInterface::macAddress(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_macAddress != value)
    {
        _macAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_NetworkInterface_interface.property_changed("MACAddress");
        }
    }

    return _macAddress;
}

auto NetworkInterface::macAddress(std::string val) ->
        std::string
{
    return macAddress(val, false);
}

int NetworkInterface::_callback_set_MACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NetworkInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->macAddress(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace NetworkInterface
{
static const auto _property_MACAddress =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void NetworkInterface::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MACAddress")
    {
        auto& v = std::get<std::string>(val);
        macAddress(v, skipSignal);
        return;
    }
}

auto NetworkInterface::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MACAddress")
    {
        return macAddress();
    }

    return PropertiesVariant();
}


const vtable_t NetworkInterface::_vtable[] = {
    vtable::start(),
    vtable::property("MACAddress",
                     details::NetworkInterface::_property_MACAddress
                        .data(),
                     _callback_get_MACAddress,
                     _callback_set_MACAddress,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

