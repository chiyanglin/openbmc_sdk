#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/MCTP/Endpoint/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace MCTP
{
namespace server
{

Endpoint::Endpoint(bus_t& bus, const char* path)
        : _xyz_openbmc_project_MCTP_Endpoint_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Endpoint::Endpoint(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Endpoint(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Endpoint::networkId() const ->
        size_t
{
    return _networkId;
}

int Endpoint::_callback_get_NetworkId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Endpoint*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->networkId();
                    }
                ));
    }
}

auto Endpoint::networkId(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_networkId != value)
    {
        _networkId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_MCTP_Endpoint_interface.property_changed("NetworkId");
        }
    }

    return _networkId;
}

auto Endpoint::networkId(size_t val) ->
        size_t
{
    return networkId(val, false);
}

int Endpoint::_callback_set_NetworkId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Endpoint*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->networkId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Endpoint
{
static const auto _property_NetworkId =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto Endpoint::eid() const ->
        size_t
{
    return _eid;
}

int Endpoint::_callback_get_EID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Endpoint*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->eid();
                    }
                ));
    }
}

auto Endpoint::eid(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_eid != value)
    {
        _eid = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_MCTP_Endpoint_interface.property_changed("EID");
        }
    }

    return _eid;
}

auto Endpoint::eid(size_t val) ->
        size_t
{
    return eid(val, false);
}

int Endpoint::_callback_set_EID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Endpoint*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->eid(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Endpoint
{
static const auto _property_EID =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto Endpoint::supportedMessageTypes() const ->
        std::vector<uint8_t>
{
    return _supportedMessageTypes;
}

int Endpoint::_callback_get_SupportedMessageTypes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Endpoint*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->supportedMessageTypes();
                    }
                ));
    }
}

auto Endpoint::supportedMessageTypes(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_supportedMessageTypes != value)
    {
        _supportedMessageTypes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_MCTP_Endpoint_interface.property_changed("SupportedMessageTypes");
        }
    }

    return _supportedMessageTypes;
}

auto Endpoint::supportedMessageTypes(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return supportedMessageTypes(val, false);
}

int Endpoint::_callback_set_SupportedMessageTypes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Endpoint*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->supportedMessageTypes(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Endpoint
{
static const auto _property_SupportedMessageTypes =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void Endpoint::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "NetworkId")
    {
        auto& v = std::get<size_t>(val);
        networkId(v, skipSignal);
        return;
    }
    if (_name == "EID")
    {
        auto& v = std::get<size_t>(val);
        eid(v, skipSignal);
        return;
    }
    if (_name == "SupportedMessageTypes")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        supportedMessageTypes(v, skipSignal);
        return;
    }
}

auto Endpoint::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "NetworkId")
    {
        return networkId();
    }
    if (_name == "EID")
    {
        return eid();
    }
    if (_name == "SupportedMessageTypes")
    {
        return supportedMessageTypes();
    }

    return PropertiesVariant();
}


const vtable_t Endpoint::_vtable[] = {
    vtable::start(),
    vtable::property("NetworkId",
                     details::Endpoint::_property_NetworkId
                        .data(),
                     _callback_get_NetworkId,
                     _callback_set_NetworkId,
                     vtable::property_::emits_change),
    vtable::property("EID",
                     details::Endpoint::_property_EID
                        .data(),
                     _callback_get_EID,
                     _callback_set_EID,
                     vtable::property_::emits_change),
    vtable::property("SupportedMessageTypes",
                     details::Endpoint::_property_SupportedMessageTypes
                        .data(),
                     _callback_get_SupportedMessageTypes,
                     _callback_set_SupportedMessageTypes,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace MCTP
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

