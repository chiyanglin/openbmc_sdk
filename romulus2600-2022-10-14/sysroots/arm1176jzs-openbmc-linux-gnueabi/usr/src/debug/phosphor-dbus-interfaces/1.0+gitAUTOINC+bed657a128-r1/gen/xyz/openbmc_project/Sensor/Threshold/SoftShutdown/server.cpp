#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Threshold/SoftShutdown/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

SoftShutdown::SoftShutdown(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SoftShutdown::SoftShutdown(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SoftShutdown(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}



void SoftShutdown::softShutdownHighAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface;
    auto m = i.new_signal("SoftShutdownHighAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace SoftShutdown
{
static const auto _signal_SoftShutdownHighAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void SoftShutdown::softShutdownHighAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface;
    auto m = i.new_signal("SoftShutdownHighAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace SoftShutdown
{
static const auto _signal_SoftShutdownHighAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void SoftShutdown::softShutdownLowAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface;
    auto m = i.new_signal("SoftShutdownLowAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace SoftShutdown
{
static const auto _signal_SoftShutdownLowAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void SoftShutdown::softShutdownLowAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface;
    auto m = i.new_signal("SoftShutdownLowAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace SoftShutdown
{
static const auto _signal_SoftShutdownLowAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}


auto SoftShutdown::softShutdownHigh() const ->
        double
{
    return _softShutdownHigh;
}

int SoftShutdown::_callback_get_SoftShutdownHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->softShutdownHigh();
                    }
                ));
    }
}

auto SoftShutdown::softShutdownHigh(double value,
                                         bool skipSignal) ->
        double
{
    if (_softShutdownHigh != value)
    {
        _softShutdownHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface.property_changed("SoftShutdownHigh");
        }
    }

    return _softShutdownHigh;
}

auto SoftShutdown::softShutdownHigh(double val) ->
        double
{
    return softShutdownHigh(val, false);
}

int SoftShutdown::_callback_set_SoftShutdownHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->softShutdownHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SoftShutdown
{
static const auto _property_SoftShutdownHigh =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto SoftShutdown::softShutdownLow() const ->
        double
{
    return _softShutdownLow;
}

int SoftShutdown::_callback_get_SoftShutdownLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->softShutdownLow();
                    }
                ));
    }
}

auto SoftShutdown::softShutdownLow(double value,
                                         bool skipSignal) ->
        double
{
    if (_softShutdownLow != value)
    {
        _softShutdownLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface.property_changed("SoftShutdownLow");
        }
    }

    return _softShutdownLow;
}

auto SoftShutdown::softShutdownLow(double val) ->
        double
{
    return softShutdownLow(val, false);
}

int SoftShutdown::_callback_set_SoftShutdownLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->softShutdownLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SoftShutdown
{
static const auto _property_SoftShutdownLow =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto SoftShutdown::softShutdownAlarmHigh() const ->
        bool
{
    return _softShutdownAlarmHigh;
}

int SoftShutdown::_callback_get_SoftShutdownAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->softShutdownAlarmHigh();
                    }
                ));
    }
}

auto SoftShutdown::softShutdownAlarmHigh(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_softShutdownAlarmHigh != value)
    {
        _softShutdownAlarmHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface.property_changed("SoftShutdownAlarmHigh");
        }
    }

    return _softShutdownAlarmHigh;
}

auto SoftShutdown::softShutdownAlarmHigh(bool val) ->
        bool
{
    return softShutdownAlarmHigh(val, false);
}

int SoftShutdown::_callback_set_SoftShutdownAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->softShutdownAlarmHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SoftShutdown
{
static const auto _property_SoftShutdownAlarmHigh =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SoftShutdown::softShutdownAlarmLow() const ->
        bool
{
    return _softShutdownAlarmLow;
}

int SoftShutdown::_callback_get_SoftShutdownAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->softShutdownAlarmLow();
                    }
                ));
    }
}

auto SoftShutdown::softShutdownAlarmLow(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_softShutdownAlarmLow != value)
    {
        _softShutdownAlarmLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface.property_changed("SoftShutdownAlarmLow");
        }
    }

    return _softShutdownAlarmLow;
}

auto SoftShutdown::softShutdownAlarmLow(bool val) ->
        bool
{
    return softShutdownAlarmLow(val, false);
}

int SoftShutdown::_callback_set_SoftShutdownAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftShutdown*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->softShutdownAlarmLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SoftShutdown
{
static const auto _property_SoftShutdownAlarmLow =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void SoftShutdown::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SoftShutdownHigh")
    {
        auto& v = std::get<double>(val);
        softShutdownHigh(v, skipSignal);
        return;
    }
    if (_name == "SoftShutdownLow")
    {
        auto& v = std::get<double>(val);
        softShutdownLow(v, skipSignal);
        return;
    }
    if (_name == "SoftShutdownAlarmHigh")
    {
        auto& v = std::get<bool>(val);
        softShutdownAlarmHigh(v, skipSignal);
        return;
    }
    if (_name == "SoftShutdownAlarmLow")
    {
        auto& v = std::get<bool>(val);
        softShutdownAlarmLow(v, skipSignal);
        return;
    }
}

auto SoftShutdown::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SoftShutdownHigh")
    {
        return softShutdownHigh();
    }
    if (_name == "SoftShutdownLow")
    {
        return softShutdownLow();
    }
    if (_name == "SoftShutdownAlarmHigh")
    {
        return softShutdownAlarmHigh();
    }
    if (_name == "SoftShutdownAlarmLow")
    {
        return softShutdownAlarmLow();
    }

    return PropertiesVariant();
}


const vtable_t SoftShutdown::_vtable[] = {
    vtable::start(),

    vtable::signal("SoftShutdownHighAlarmAsserted",
                   details::SoftShutdown::_signal_SoftShutdownHighAlarmAsserted
                        .data()),

    vtable::signal("SoftShutdownHighAlarmDeasserted",
                   details::SoftShutdown::_signal_SoftShutdownHighAlarmDeasserted
                        .data()),

    vtable::signal("SoftShutdownLowAlarmAsserted",
                   details::SoftShutdown::_signal_SoftShutdownLowAlarmAsserted
                        .data()),

    vtable::signal("SoftShutdownLowAlarmDeasserted",
                   details::SoftShutdown::_signal_SoftShutdownLowAlarmDeasserted
                        .data()),
    vtable::property("SoftShutdownHigh",
                     details::SoftShutdown::_property_SoftShutdownHigh
                        .data(),
                     _callback_get_SoftShutdownHigh,
                     _callback_set_SoftShutdownHigh,
                     vtable::property_::emits_change),
    vtable::property("SoftShutdownLow",
                     details::SoftShutdown::_property_SoftShutdownLow
                        .data(),
                     _callback_get_SoftShutdownLow,
                     _callback_set_SoftShutdownLow,
                     vtable::property_::emits_change),
    vtable::property("SoftShutdownAlarmHigh",
                     details::SoftShutdown::_property_SoftShutdownAlarmHigh
                        .data(),
                     _callback_get_SoftShutdownAlarmHigh,
                     _callback_set_SoftShutdownAlarmHigh,
                     vtable::property_::emits_change),
    vtable::property("SoftShutdownAlarmLow",
                     details::SoftShutdown::_property_SoftShutdownAlarmLow
                        .data(),
                     _callback_get_SoftShutdownAlarmLow,
                     _callback_set_SoftShutdownAlarmLow,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

