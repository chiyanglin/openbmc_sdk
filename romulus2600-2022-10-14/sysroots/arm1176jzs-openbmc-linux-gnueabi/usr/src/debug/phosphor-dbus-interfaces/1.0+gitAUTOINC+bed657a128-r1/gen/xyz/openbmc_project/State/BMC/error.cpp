#include <xyz/openbmc_project/State/BMC/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace BMC
{
namespace Error
{
const char* MultiUserTargetFailure::name() const noexcept
{
    return errName;
}
const char* MultiUserTargetFailure::description() const noexcept
{
    return errDesc;
}
const char* MultiUserTargetFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace BMC
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

