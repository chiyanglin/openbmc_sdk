#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/CSR/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace CSR
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_CSR_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_GenerateCSR(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& alternativeNames, std::string&& challengePassword, std::string&& city, std::string&& commonName, std::string&& contactPerson, std::string&& country, std::string&& email, std::string&& givenName, std::string&& initials, int64_t&& keyBitLength, std::string&& keyCurveId, std::string&& keyPairAlgorithm, std::vector<std::string>&& keyUsage, std::string&& organization, std::string&& organizationalUnit, std::string&& state, std::string&& surname, std::string&& unstructuredName)
                    {
                        return o->generateCSR(
                                alternativeNames, challengePassword, city, commonName, contactPerson, country, email, givenName, initials, keyBitLength, keyCurveId, keyPairAlgorithm, keyUsage, organization, organizationalUnit, state, surname, unstructuredName);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_GenerateCSR =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::string>, std::string, std::string, std::string, std::string, std::string, std::string, std::string, std::string, int64_t, std::string, std::string, std::vector<std::string>, std::string, std::string, std::string, std::string, std::string>());
static const auto _return_GenerateCSR =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
}
}




const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("GenerateCSR",
                   details::Create::_param_GenerateCSR
                        .data(),
                   details::Create::_return_GenerateCSR
                        .data(),
                   _callback_GenerateCSR),
    vtable::end()
};

} // namespace server
} // namespace CSR
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

