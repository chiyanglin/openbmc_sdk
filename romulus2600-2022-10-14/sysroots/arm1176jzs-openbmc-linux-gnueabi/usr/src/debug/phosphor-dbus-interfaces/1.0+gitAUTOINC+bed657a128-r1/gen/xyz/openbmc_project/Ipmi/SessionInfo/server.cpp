#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Ipmi/SessionInfo/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Ipmi
{
namespace server
{

SessionInfo::SessionInfo(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Ipmi_SessionInfo_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SessionInfo::SessionInfo(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SessionInfo(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SessionInfo::sessionHandle() const ->
        uint8_t
{
    return _sessionHandle;
}

int SessionInfo::_callback_get_SessionHandle(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sessionHandle();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::sessionHandle(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_sessionHandle != value)
    {
        _sessionHandle = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("SessionHandle");
        }
    }

    return _sessionHandle;
}

auto SessionInfo::sessionHandle(uint8_t val) ->
        uint8_t
{
    return sessionHandle(val, false);
}

int SessionInfo::_callback_set_SessionHandle(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->sessionHandle(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_SessionHandle =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SessionInfo::channelNum() const ->
        uint8_t
{
    return _channelNum;
}

int SessionInfo::_callback_get_ChannelNum(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->channelNum();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::channelNum(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_channelNum != value)
    {
        _channelNum = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("ChannelNum");
        }
    }

    return _channelNum;
}

auto SessionInfo::channelNum(uint8_t val) ->
        uint8_t
{
    return channelNum(val, false);
}

int SessionInfo::_callback_set_ChannelNum(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->channelNum(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_ChannelNum =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SessionInfo::currentPrivilege() const ->
        uint8_t
{
    return _currentPrivilege;
}

int SessionInfo::_callback_get_CurrentPrivilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentPrivilege();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::currentPrivilege(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_currentPrivilege != value)
    {
        _currentPrivilege = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("CurrentPrivilege");
        }
    }

    return _currentPrivilege;
}

auto SessionInfo::currentPrivilege(uint8_t val) ->
        uint8_t
{
    return currentPrivilege(val, false);
}

int SessionInfo::_callback_set_CurrentPrivilege(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->currentPrivilege(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_CurrentPrivilege =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SessionInfo::remoteIPAddr() const ->
        uint32_t
{
    return _remoteIPAddr;
}

int SessionInfo::_callback_get_RemoteIPAddr(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->remoteIPAddr();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::remoteIPAddr(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_remoteIPAddr != value)
    {
        _remoteIPAddr = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("RemoteIPAddr");
        }
    }

    return _remoteIPAddr;
}

auto SessionInfo::remoteIPAddr(uint32_t val) ->
        uint32_t
{
    return remoteIPAddr(val, false);
}

int SessionInfo::_callback_set_RemoteIPAddr(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->remoteIPAddr(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_RemoteIPAddr =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto SessionInfo::remotePort() const ->
        uint16_t
{
    return _remotePort;
}

int SessionInfo::_callback_get_RemotePort(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->remotePort();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::remotePort(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_remotePort != value)
    {
        _remotePort = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("RemotePort");
        }
    }

    return _remotePort;
}

auto SessionInfo::remotePort(uint16_t val) ->
        uint16_t
{
    return remotePort(val, false);
}

int SessionInfo::_callback_set_RemotePort(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->remotePort(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_RemotePort =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto SessionInfo::remoteMACAddress() const ->
        std::vector<uint8_t>
{
    return _remoteMACAddress;
}

int SessionInfo::_callback_get_RemoteMACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->remoteMACAddress();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::remoteMACAddress(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_remoteMACAddress != value)
    {
        _remoteMACAddress = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("RemoteMACAddress");
        }
    }

    return _remoteMACAddress;
}

auto SessionInfo::remoteMACAddress(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return remoteMACAddress(val, false);
}

int SessionInfo::_callback_set_RemoteMACAddress(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->remoteMACAddress(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_RemoteMACAddress =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto SessionInfo::userID() const ->
        uint8_t
{
    return _userID;
}

int SessionInfo::_callback_get_UserID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->userID();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::userID(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_userID != value)
    {
        _userID = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("UserID");
        }
    }

    return _userID;
}

auto SessionInfo::userID(uint8_t val) ->
        uint8_t
{
    return userID(val, false);
}

int SessionInfo::_callback_set_UserID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->userID(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_UserID =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto SessionInfo::state() const ->
        uint8_t
{
    return _state;
}

int SessionInfo::_callback_get_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->state();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto SessionInfo::state(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_state != value)
    {
        _state = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_SessionInfo_interface.property_changed("State");
        }
    }

    return _state;
}

auto SessionInfo::state(uint8_t val) ->
        uint8_t
{
    return state(val, false);
}

int SessionInfo::_callback_set_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SessionInfo*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->state(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace SessionInfo
{
static const auto _property_State =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void SessionInfo::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SessionHandle")
    {
        auto& v = std::get<uint8_t>(val);
        sessionHandle(v, skipSignal);
        return;
    }
    if (_name == "ChannelNum")
    {
        auto& v = std::get<uint8_t>(val);
        channelNum(v, skipSignal);
        return;
    }
    if (_name == "CurrentPrivilege")
    {
        auto& v = std::get<uint8_t>(val);
        currentPrivilege(v, skipSignal);
        return;
    }
    if (_name == "RemoteIPAddr")
    {
        auto& v = std::get<uint32_t>(val);
        remoteIPAddr(v, skipSignal);
        return;
    }
    if (_name == "RemotePort")
    {
        auto& v = std::get<uint16_t>(val);
        remotePort(v, skipSignal);
        return;
    }
    if (_name == "RemoteMACAddress")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        remoteMACAddress(v, skipSignal);
        return;
    }
    if (_name == "UserID")
    {
        auto& v = std::get<uint8_t>(val);
        userID(v, skipSignal);
        return;
    }
    if (_name == "State")
    {
        auto& v = std::get<uint8_t>(val);
        state(v, skipSignal);
        return;
    }
}

auto SessionInfo::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SessionHandle")
    {
        return sessionHandle();
    }
    if (_name == "ChannelNum")
    {
        return channelNum();
    }
    if (_name == "CurrentPrivilege")
    {
        return currentPrivilege();
    }
    if (_name == "RemoteIPAddr")
    {
        return remoteIPAddr();
    }
    if (_name == "RemotePort")
    {
        return remotePort();
    }
    if (_name == "RemoteMACAddress")
    {
        return remoteMACAddress();
    }
    if (_name == "UserID")
    {
        return userID();
    }
    if (_name == "State")
    {
        return state();
    }

    return PropertiesVariant();
}


const vtable_t SessionInfo::_vtable[] = {
    vtable::start(),
    vtable::property("SessionHandle",
                     details::SessionInfo::_property_SessionHandle
                        .data(),
                     _callback_get_SessionHandle,
                     _callback_set_SessionHandle,
                     vtable::property_::emits_change),
    vtable::property("ChannelNum",
                     details::SessionInfo::_property_ChannelNum
                        .data(),
                     _callback_get_ChannelNum,
                     _callback_set_ChannelNum,
                     vtable::property_::emits_change),
    vtable::property("CurrentPrivilege",
                     details::SessionInfo::_property_CurrentPrivilege
                        .data(),
                     _callback_get_CurrentPrivilege,
                     _callback_set_CurrentPrivilege,
                     vtable::property_::emits_change),
    vtable::property("RemoteIPAddr",
                     details::SessionInfo::_property_RemoteIPAddr
                        .data(),
                     _callback_get_RemoteIPAddr,
                     _callback_set_RemoteIPAddr,
                     vtable::property_::emits_change),
    vtable::property("RemotePort",
                     details::SessionInfo::_property_RemotePort
                        .data(),
                     _callback_get_RemotePort,
                     _callback_set_RemotePort,
                     vtable::property_::emits_change),
    vtable::property("RemoteMACAddress",
                     details::SessionInfo::_property_RemoteMACAddress
                        .data(),
                     _callback_get_RemoteMACAddress,
                     _callback_set_RemoteMACAddress,
                     vtable::property_::emits_change),
    vtable::property("UserID",
                     details::SessionInfo::_property_UserID
                        .data(),
                     _callback_get_UserID,
                     _callback_set_UserID,
                     vtable::property_::emits_change),
    vtable::property("State",
                     details::SessionInfo::_property_State
                        .data(),
                     _callback_get_State,
                     _callback_set_State,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Ipmi
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

