#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/EthernetInterface/server.hpp>










#include <xyz/openbmc_project/Common/error.hpp>







#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

EthernetInterface::EthernetInterface(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_EthernetInterface_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

EthernetInterface::EthernetInterface(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : EthernetInterface(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto EthernetInterface::interfaceName() const ->
        std::string
{
    return _interfaceName;
}

int EthernetInterface::_callback_get_InterfaceName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->interfaceName();
                    }
                ));
    }
}

auto EthernetInterface::interfaceName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_interfaceName != value)
    {
        _interfaceName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("InterfaceName");
        }
    }

    return _interfaceName;
}

auto EthernetInterface::interfaceName(std::string val) ->
        std::string
{
    return interfaceName(val, false);
}


namespace details
{
namespace EthernetInterface
{
static const auto _property_InterfaceName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto EthernetInterface::speed() const ->
        uint32_t
{
    return _speed;
}

int EthernetInterface::_callback_get_Speed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->speed();
                    }
                ));
    }
}

auto EthernetInterface::speed(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_speed != value)
    {
        _speed = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("Speed");
        }
    }

    return _speed;
}

auto EthernetInterface::speed(uint32_t val) ->
        uint32_t
{
    return speed(val, false);
}


namespace details
{
namespace EthernetInterface
{
static const auto _property_Speed =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto EthernetInterface::autoNeg() const ->
        bool
{
    return _autoNeg;
}

int EthernetInterface::_callback_get_AutoNeg(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->autoNeg();
                    }
                ));
    }
}

auto EthernetInterface::autoNeg(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_autoNeg != value)
    {
        _autoNeg = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("AutoNeg");
        }
    }

    return _autoNeg;
}

auto EthernetInterface::autoNeg(bool val) ->
        bool
{
    return autoNeg(val, false);
}


namespace details
{
namespace EthernetInterface
{
static const auto _property_AutoNeg =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto EthernetInterface::mtu() const ->
        size_t
{
    return _mtu;
}

int EthernetInterface::_callback_get_MTU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mtu();
                    }
                ));
    }
}

auto EthernetInterface::mtu(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_mtu != value)
    {
        _mtu = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("MTU");
        }
    }

    return _mtu;
}

auto EthernetInterface::mtu(size_t val) ->
        size_t
{
    return mtu(val, false);
}

int EthernetInterface::_callback_set_MTU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->mtu(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_MTU =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto EthernetInterface::domainName() const ->
        std::vector<std::string>
{
    return _domainName;
}

int EthernetInterface::_callback_get_DomainName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->domainName();
                    }
                ));
    }
}

auto EthernetInterface::domainName(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_domainName != value)
    {
        _domainName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("DomainName");
        }
    }

    return _domainName;
}

auto EthernetInterface::domainName(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return domainName(val, false);
}

int EthernetInterface::_callback_set_DomainName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->domainName(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_DomainName =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto EthernetInterface::dhcpEnabled() const ->
        DHCPConf
{
    return _dhcpEnabled;
}

int EthernetInterface::_callback_get_DHCPEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dhcpEnabled();
                    }
                ));
    }
}

auto EthernetInterface::dhcpEnabled(DHCPConf value,
                                         bool skipSignal) ->
        DHCPConf
{
    if (_dhcpEnabled != value)
    {
        _dhcpEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("DHCPEnabled");
        }
    }

    return _dhcpEnabled;
}

auto EthernetInterface::dhcpEnabled(DHCPConf val) ->
        DHCPConf
{
    return dhcpEnabled(val, false);
}

int EthernetInterface::_callback_set_DHCPEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](DHCPConf&& arg)
                    {
                        o->dhcpEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_DHCPEnabled =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface::DHCPConf>());
}
}

auto EthernetInterface::dhcp4() const ->
        bool
{
    return _dhcp4;
}

int EthernetInterface::_callback_get_DHCP4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dhcp4();
                    }
                ));
    }
}

auto EthernetInterface::dhcp4(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_dhcp4 != value)
    {
        _dhcp4 = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("DHCP4");
        }
    }

    return _dhcp4;
}

auto EthernetInterface::dhcp4(bool val) ->
        bool
{
    return dhcp4(val, false);
}

int EthernetInterface::_callback_set_DHCP4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->dhcp4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_DHCP4 =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto EthernetInterface::dhcp6() const ->
        bool
{
    return _dhcp6;
}

int EthernetInterface::_callback_get_DHCP6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dhcp6();
                    }
                ));
    }
}

auto EthernetInterface::dhcp6(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_dhcp6 != value)
    {
        _dhcp6 = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("DHCP6");
        }
    }

    return _dhcp6;
}

auto EthernetInterface::dhcp6(bool val) ->
        bool
{
    return dhcp6(val, false);
}

int EthernetInterface::_callback_set_DHCP6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->dhcp6(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_DHCP6 =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto EthernetInterface::nameservers() const ->
        std::vector<std::string>
{
    return _nameservers;
}

int EthernetInterface::_callback_get_Nameservers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nameservers();
                    }
                ));
    }
}

auto EthernetInterface::nameservers(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_nameservers != value)
    {
        _nameservers = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("Nameservers");
        }
    }

    return _nameservers;
}

auto EthernetInterface::nameservers(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return nameservers(val, false);
}


namespace details
{
namespace EthernetInterface
{
static const auto _property_Nameservers =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto EthernetInterface::staticNameServers() const ->
        std::vector<std::string>
{
    return _staticNameServers;
}

int EthernetInterface::_callback_get_StaticNameServers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->staticNameServers();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto EthernetInterface::staticNameServers(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_staticNameServers != value)
    {
        _staticNameServers = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("StaticNameServers");
        }
    }

    return _staticNameServers;
}

auto EthernetInterface::staticNameServers(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return staticNameServers(val, false);
}

int EthernetInterface::_callback_set_StaticNameServers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->staticNameServers(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_StaticNameServers =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto EthernetInterface::ntpServers() const ->
        std::vector<std::string>
{
    return _ntpServers;
}

int EthernetInterface::_callback_get_NTPServers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ntpServers();
                    }
                ));
    }
}

auto EthernetInterface::ntpServers(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_ntpServers != value)
    {
        _ntpServers = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("NTPServers");
        }
    }

    return _ntpServers;
}

auto EthernetInterface::ntpServers(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return ntpServers(val, false);
}

int EthernetInterface::_callback_set_NTPServers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->ntpServers(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_NTPServers =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto EthernetInterface::staticNTPServers() const ->
        std::vector<std::string>
{
    return _staticNTPServers;
}

int EthernetInterface::_callback_get_StaticNTPServers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->staticNTPServers();
                    }
                ));
    }
}

auto EthernetInterface::staticNTPServers(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_staticNTPServers != value)
    {
        _staticNTPServers = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("StaticNTPServers");
        }
    }

    return _staticNTPServers;
}

auto EthernetInterface::staticNTPServers(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return staticNTPServers(val, false);
}

int EthernetInterface::_callback_set_StaticNTPServers(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->staticNTPServers(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_StaticNTPServers =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto EthernetInterface::linkLocalAutoConf() const ->
        LinkLocalConf
{
    return _linkLocalAutoConf;
}

int EthernetInterface::_callback_get_LinkLocalAutoConf(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->linkLocalAutoConf();
                    }
                ));
    }
}

auto EthernetInterface::linkLocalAutoConf(LinkLocalConf value,
                                         bool skipSignal) ->
        LinkLocalConf
{
    if (_linkLocalAutoConf != value)
    {
        _linkLocalAutoConf = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("LinkLocalAutoConf");
        }
    }

    return _linkLocalAutoConf;
}

auto EthernetInterface::linkLocalAutoConf(LinkLocalConf val) ->
        LinkLocalConf
{
    return linkLocalAutoConf(val, false);
}

int EthernetInterface::_callback_set_LinkLocalAutoConf(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](LinkLocalConf&& arg)
                    {
                        o->linkLocalAutoConf(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_LinkLocalAutoConf =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::server::EthernetInterface::LinkLocalConf>());
}
}

auto EthernetInterface::ipv6AcceptRA() const ->
        bool
{
    return _ipv6AcceptRA;
}

int EthernetInterface::_callback_get_IPv6AcceptRA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ipv6AcceptRA();
                    }
                ));
    }
}

auto EthernetInterface::ipv6AcceptRA(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_ipv6AcceptRA != value)
    {
        _ipv6AcceptRA = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("IPv6AcceptRA");
        }
    }

    return _ipv6AcceptRA;
}

auto EthernetInterface::ipv6AcceptRA(bool val) ->
        bool
{
    return ipv6AcceptRA(val, false);
}

int EthernetInterface::_callback_set_IPv6AcceptRA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->ipv6AcceptRA(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_IPv6AcceptRA =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto EthernetInterface::nicEnabled() const ->
        bool
{
    return _nicEnabled;
}

int EthernetInterface::_callback_get_NICEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nicEnabled();
                    }
                ));
    }
}

auto EthernetInterface::nicEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_nicEnabled != value)
    {
        _nicEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("NICEnabled");
        }
    }

    return _nicEnabled;
}

auto EthernetInterface::nicEnabled(bool val) ->
        bool
{
    return nicEnabled(val, false);
}

int EthernetInterface::_callback_set_NICEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->nicEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_NICEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto EthernetInterface::linkUp() const ->
        bool
{
    return _linkUp;
}

int EthernetInterface::_callback_get_LinkUp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->linkUp();
                    }
                ));
    }
}

auto EthernetInterface::linkUp(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_linkUp != value)
    {
        _linkUp = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("LinkUp");
        }
    }

    return _linkUp;
}

auto EthernetInterface::linkUp(bool val) ->
        bool
{
    return linkUp(val, false);
}


namespace details
{
namespace EthernetInterface
{
static const auto _property_LinkUp =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto EthernetInterface::defaultGateway() const ->
        std::string
{
    return _defaultGateway;
}

int EthernetInterface::_callback_get_DefaultGateway(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->defaultGateway();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto EthernetInterface::defaultGateway(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_defaultGateway != value)
    {
        _defaultGateway = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("DefaultGateway");
        }
    }

    return _defaultGateway;
}

auto EthernetInterface::defaultGateway(std::string val) ->
        std::string
{
    return defaultGateway(val, false);
}

int EthernetInterface::_callback_set_DefaultGateway(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->defaultGateway(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_DefaultGateway =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto EthernetInterface::defaultGateway6() const ->
        std::string
{
    return _defaultGateway6;
}

int EthernetInterface::_callback_get_DefaultGateway6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->defaultGateway6();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto EthernetInterface::defaultGateway6(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_defaultGateway6 != value)
    {
        _defaultGateway6 = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.property_changed("DefaultGateway6");
        }
    }

    return _defaultGateway6;
}

auto EthernetInterface::defaultGateway6(std::string val) ->
        std::string
{
    return defaultGateway6(val, false);
}

int EthernetInterface::_callback_set_DefaultGateway6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EthernetInterface*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->defaultGateway6(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace EthernetInterface
{
static const auto _property_DefaultGateway6 =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void EthernetInterface::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "InterfaceName")
    {
        auto& v = std::get<std::string>(val);
        interfaceName(v, skipSignal);
        return;
    }
    if (_name == "Speed")
    {
        auto& v = std::get<uint32_t>(val);
        speed(v, skipSignal);
        return;
    }
    if (_name == "AutoNeg")
    {
        auto& v = std::get<bool>(val);
        autoNeg(v, skipSignal);
        return;
    }
    if (_name == "MTU")
    {
        auto& v = std::get<size_t>(val);
        mtu(v, skipSignal);
        return;
    }
    if (_name == "DomainName")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        domainName(v, skipSignal);
        return;
    }
    if (_name == "DHCPEnabled")
    {
        auto& v = std::get<DHCPConf>(val);
        dhcpEnabled(v, skipSignal);
        return;
    }
    if (_name == "DHCP4")
    {
        auto& v = std::get<bool>(val);
        dhcp4(v, skipSignal);
        return;
    }
    if (_name == "DHCP6")
    {
        auto& v = std::get<bool>(val);
        dhcp6(v, skipSignal);
        return;
    }
    if (_name == "Nameservers")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        nameservers(v, skipSignal);
        return;
    }
    if (_name == "StaticNameServers")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        staticNameServers(v, skipSignal);
        return;
    }
    if (_name == "NTPServers")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        ntpServers(v, skipSignal);
        return;
    }
    if (_name == "StaticNTPServers")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        staticNTPServers(v, skipSignal);
        return;
    }
    if (_name == "LinkLocalAutoConf")
    {
        auto& v = std::get<LinkLocalConf>(val);
        linkLocalAutoConf(v, skipSignal);
        return;
    }
    if (_name == "IPv6AcceptRA")
    {
        auto& v = std::get<bool>(val);
        ipv6AcceptRA(v, skipSignal);
        return;
    }
    if (_name == "NICEnabled")
    {
        auto& v = std::get<bool>(val);
        nicEnabled(v, skipSignal);
        return;
    }
    if (_name == "LinkUp")
    {
        auto& v = std::get<bool>(val);
        linkUp(v, skipSignal);
        return;
    }
    if (_name == "DefaultGateway")
    {
        auto& v = std::get<std::string>(val);
        defaultGateway(v, skipSignal);
        return;
    }
    if (_name == "DefaultGateway6")
    {
        auto& v = std::get<std::string>(val);
        defaultGateway6(v, skipSignal);
        return;
    }
}

auto EthernetInterface::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "InterfaceName")
    {
        return interfaceName();
    }
    if (_name == "Speed")
    {
        return speed();
    }
    if (_name == "AutoNeg")
    {
        return autoNeg();
    }
    if (_name == "MTU")
    {
        return mtu();
    }
    if (_name == "DomainName")
    {
        return domainName();
    }
    if (_name == "DHCPEnabled")
    {
        return dhcpEnabled();
    }
    if (_name == "DHCP4")
    {
        return dhcp4();
    }
    if (_name == "DHCP6")
    {
        return dhcp6();
    }
    if (_name == "Nameservers")
    {
        return nameservers();
    }
    if (_name == "StaticNameServers")
    {
        return staticNameServers();
    }
    if (_name == "NTPServers")
    {
        return ntpServers();
    }
    if (_name == "StaticNTPServers")
    {
        return staticNTPServers();
    }
    if (_name == "LinkLocalAutoConf")
    {
        return linkLocalAutoConf();
    }
    if (_name == "IPv6AcceptRA")
    {
        return ipv6AcceptRA();
    }
    if (_name == "NICEnabled")
    {
        return nicEnabled();
    }
    if (_name == "LinkUp")
    {
        return linkUp();
    }
    if (_name == "DefaultGateway")
    {
        return defaultGateway();
    }
    if (_name == "DefaultGateway6")
    {
        return defaultGateway6();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for EthernetInterface::LinkLocalConf */
static const std::tuple<const char*, EthernetInterface::LinkLocalConf> mappingEthernetInterfaceLinkLocalConf[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.LinkLocalConf.fallback",                 EthernetInterface::LinkLocalConf::fallback ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.LinkLocalConf.both",                 EthernetInterface::LinkLocalConf::both ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.LinkLocalConf.v4",                 EthernetInterface::LinkLocalConf::v4 ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.LinkLocalConf.v6",                 EthernetInterface::LinkLocalConf::v6 ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.LinkLocalConf.none",                 EthernetInterface::LinkLocalConf::none ),
        };

} // anonymous namespace

auto EthernetInterface::convertStringToLinkLocalConf(const std::string& s) noexcept ->
        std::optional<LinkLocalConf>
{
    auto i = std::find_if(
            std::begin(mappingEthernetInterfaceLinkLocalConf),
            std::end(mappingEthernetInterfaceLinkLocalConf),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingEthernetInterfaceLinkLocalConf) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto EthernetInterface::convertLinkLocalConfFromString(const std::string& s) ->
        LinkLocalConf
{
    auto r = convertStringToLinkLocalConf(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string EthernetInterface::convertLinkLocalConfToString(EthernetInterface::LinkLocalConf v)
{
    auto i = std::find_if(
            std::begin(mappingEthernetInterfaceLinkLocalConf),
            std::end(mappingEthernetInterfaceLinkLocalConf),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingEthernetInterfaceLinkLocalConf))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for EthernetInterface::DHCPConf */
static const std::tuple<const char*, EthernetInterface::DHCPConf> mappingEthernetInterfaceDHCPConf[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.DHCPConf.both",                 EthernetInterface::DHCPConf::both ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.DHCPConf.v4v6stateless",                 EthernetInterface::DHCPConf::v4v6stateless ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.DHCPConf.v6",                 EthernetInterface::DHCPConf::v6 ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.DHCPConf.v6stateless",                 EthernetInterface::DHCPConf::v6stateless ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.DHCPConf.v4",                 EthernetInterface::DHCPConf::v4 ),
            std::make_tuple( "xyz.openbmc_project.Network.EthernetInterface.DHCPConf.none",                 EthernetInterface::DHCPConf::none ),
        };

} // anonymous namespace

auto EthernetInterface::convertStringToDHCPConf(const std::string& s) noexcept ->
        std::optional<DHCPConf>
{
    auto i = std::find_if(
            std::begin(mappingEthernetInterfaceDHCPConf),
            std::end(mappingEthernetInterfaceDHCPConf),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingEthernetInterfaceDHCPConf) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto EthernetInterface::convertDHCPConfFromString(const std::string& s) ->
        DHCPConf
{
    auto r = convertStringToDHCPConf(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string EthernetInterface::convertDHCPConfToString(EthernetInterface::DHCPConf v)
{
    auto i = std::find_if(
            std::begin(mappingEthernetInterfaceDHCPConf),
            std::end(mappingEthernetInterfaceDHCPConf),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingEthernetInterfaceDHCPConf))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t EthernetInterface::_vtable[] = {
    vtable::start(),
    vtable::property("InterfaceName",
                     details::EthernetInterface::_property_InterfaceName
                        .data(),
                     _callback_get_InterfaceName,
                     vtable::property_::const_),
    vtable::property("Speed",
                     details::EthernetInterface::_property_Speed
                        .data(),
                     _callback_get_Speed,
                     vtable::property_::emits_change),
    vtable::property("AutoNeg",
                     details::EthernetInterface::_property_AutoNeg
                        .data(),
                     _callback_get_AutoNeg,
                     vtable::property_::emits_change),
    vtable::property("MTU",
                     details::EthernetInterface::_property_MTU
                        .data(),
                     _callback_get_MTU,
                     _callback_set_MTU,
                     vtable::property_::emits_change),
    vtable::property("DomainName",
                     details::EthernetInterface::_property_DomainName
                        .data(),
                     _callback_get_DomainName,
                     _callback_set_DomainName,
                     vtable::property_::emits_change),
    vtable::property("DHCPEnabled",
                     details::EthernetInterface::_property_DHCPEnabled
                        .data(),
                     _callback_get_DHCPEnabled,
                     _callback_set_DHCPEnabled,
                     vtable::property_::emits_change),
    vtable::property("DHCP4",
                     details::EthernetInterface::_property_DHCP4
                        .data(),
                     _callback_get_DHCP4,
                     _callback_set_DHCP4,
                     vtable::property_::emits_change),
    vtable::property("DHCP6",
                     details::EthernetInterface::_property_DHCP6
                        .data(),
                     _callback_get_DHCP6,
                     _callback_set_DHCP6,
                     vtable::property_::emits_change),
    vtable::property("Nameservers",
                     details::EthernetInterface::_property_Nameservers
                        .data(),
                     _callback_get_Nameservers,
                     vtable::property_::emits_change),
    vtable::property("StaticNameServers",
                     details::EthernetInterface::_property_StaticNameServers
                        .data(),
                     _callback_get_StaticNameServers,
                     _callback_set_StaticNameServers,
                     vtable::property_::emits_change),
    vtable::property("NTPServers",
                     details::EthernetInterface::_property_NTPServers
                        .data(),
                     _callback_get_NTPServers,
                     _callback_set_NTPServers,
                     vtable::property_::emits_change),
    vtable::property("StaticNTPServers",
                     details::EthernetInterface::_property_StaticNTPServers
                        .data(),
                     _callback_get_StaticNTPServers,
                     _callback_set_StaticNTPServers,
                     vtable::property_::emits_change),
    vtable::property("LinkLocalAutoConf",
                     details::EthernetInterface::_property_LinkLocalAutoConf
                        .data(),
                     _callback_get_LinkLocalAutoConf,
                     _callback_set_LinkLocalAutoConf,
                     vtable::property_::emits_change),
    vtable::property("IPv6AcceptRA",
                     details::EthernetInterface::_property_IPv6AcceptRA
                        .data(),
                     _callback_get_IPv6AcceptRA,
                     _callback_set_IPv6AcceptRA,
                     vtable::property_::emits_change),
    vtable::property("NICEnabled",
                     details::EthernetInterface::_property_NICEnabled
                        .data(),
                     _callback_get_NICEnabled,
                     _callback_set_NICEnabled,
                     vtable::property_::emits_change),
    vtable::property("LinkUp",
                     details::EthernetInterface::_property_LinkUp
                        .data(),
                     _callback_get_LinkUp,
                     vtable::property_::emits_change),
    vtable::property("DefaultGateway",
                     details::EthernetInterface::_property_DefaultGateway
                        .data(),
                     _callback_get_DefaultGateway,
                     _callback_set_DefaultGateway,
                     vtable::property_::emits_change),
    vtable::property("DefaultGateway6",
                     details::EthernetInterface::_property_DefaultGateway6
                        .data(),
                     _callback_get_DefaultGateway6,
                     _callback_set_DefaultGateway6,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

