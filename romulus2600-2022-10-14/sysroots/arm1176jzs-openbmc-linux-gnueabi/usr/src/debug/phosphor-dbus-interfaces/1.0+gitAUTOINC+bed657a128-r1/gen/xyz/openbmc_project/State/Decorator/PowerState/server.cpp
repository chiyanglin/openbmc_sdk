#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Decorator/PowerState/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Decorator
{
namespace server
{

PowerState::PowerState(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Decorator_PowerState_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PowerState::PowerState(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PowerState(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PowerState::powerState() const ->
        State
{
    return _powerState;
}

int PowerState::_callback_get_PowerState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerState*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerState();
                    }
                ));
    }
}

auto PowerState::powerState(State value,
                                         bool skipSignal) ->
        State
{
    if (_powerState != value)
    {
        _powerState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Decorator_PowerState_interface.property_changed("PowerState");
        }
    }

    return _powerState;
}

auto PowerState::powerState(State val) ->
        State
{
    return powerState(val, false);
}

int PowerState::_callback_set_PowerState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerState*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](State&& arg)
                    {
                        o->powerState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerState
{
static const auto _property_PowerState =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::Decorator::server::PowerState::State>());
}
}

void PowerState::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PowerState")
    {
        auto& v = std::get<State>(val);
        powerState(v, skipSignal);
        return;
    }
}

auto PowerState::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PowerState")
    {
        return powerState();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for PowerState::State */
static const std::tuple<const char*, PowerState::State> mappingPowerStateState[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerState.State.On",                 PowerState::State::On ),
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerState.State.Off",                 PowerState::State::Off ),
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerState.State.PoweringOn",                 PowerState::State::PoweringOn ),
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerState.State.PoweringOff",                 PowerState::State::PoweringOff ),
            std::make_tuple( "xyz.openbmc_project.State.Decorator.PowerState.State.Unknown",                 PowerState::State::Unknown ),
        };

} // anonymous namespace

auto PowerState::convertStringToState(const std::string& s) noexcept ->
        std::optional<State>
{
    auto i = std::find_if(
            std::begin(mappingPowerStateState),
            std::end(mappingPowerStateState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPowerStateState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PowerState::convertStateFromString(const std::string& s) ->
        State
{
    auto r = convertStringToState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PowerState::convertStateToString(PowerState::State v)
{
    auto i = std::find_if(
            std::begin(mappingPowerStateState),
            std::end(mappingPowerStateState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPowerStateState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t PowerState::_vtable[] = {
    vtable::start(),
    vtable::property("PowerState",
                     details::PowerState::_property_PowerState
                        .data(),
                     _callback_get_PowerState,
                     _callback_set_PowerState,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

