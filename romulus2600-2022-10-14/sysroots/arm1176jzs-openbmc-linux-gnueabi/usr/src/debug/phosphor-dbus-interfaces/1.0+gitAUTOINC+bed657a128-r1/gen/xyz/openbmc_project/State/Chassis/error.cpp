#include <xyz/openbmc_project/State/Chassis/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Chassis
{
namespace Error
{
const char* PowerOnFailure::name() const noexcept
{
    return errName;
}
const char* PowerOnFailure::description() const noexcept
{
    return errDesc;
}
const char* PowerOnFailure::what() const noexcept
{
    return errWhat;
}
const char* PowerOffFailure::name() const noexcept
{
    return errName;
}
const char* PowerOffFailure::description() const noexcept
{
    return errDesc;
}
const char* PowerOffFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Chassis
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

