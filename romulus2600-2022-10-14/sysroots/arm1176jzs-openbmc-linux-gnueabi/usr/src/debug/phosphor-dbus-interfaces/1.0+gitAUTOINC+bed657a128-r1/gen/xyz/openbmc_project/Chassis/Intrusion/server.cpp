#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Intrusion/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace server
{

Intrusion::Intrusion(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Intrusion_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Intrusion::Intrusion(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Intrusion(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Intrusion::status() const ->
        std::string
{
    return _status;
}

int Intrusion::_callback_get_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Intrusion*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->status();
                    }
                ));
    }
}

auto Intrusion::status(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_status != value)
    {
        _status = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Intrusion_interface.property_changed("Status");
        }
    }

    return _status;
}

auto Intrusion::status(std::string val) ->
        std::string
{
    return status(val, false);
}

int Intrusion::_callback_set_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Intrusion*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->status(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Intrusion
{
static const auto _property_Status =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Intrusion::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Status")
    {
        auto& v = std::get<std::string>(val);
        status(v, skipSignal);
        return;
    }
}

auto Intrusion::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Status")
    {
        return status();
    }

    return PropertiesVariant();
}


const vtable_t Intrusion::_vtable[] = {
    vtable::start(),
    vtable::property("Status",
                     details::Intrusion::_property_Status
                        .data(),
                     _callback_get_Status,
                     _callback_set_Status,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

