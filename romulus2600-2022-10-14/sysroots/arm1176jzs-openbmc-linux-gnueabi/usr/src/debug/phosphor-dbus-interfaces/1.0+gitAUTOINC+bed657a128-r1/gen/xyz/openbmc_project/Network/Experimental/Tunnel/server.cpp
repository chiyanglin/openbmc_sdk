#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/Experimental/Tunnel/server.hpp>














namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Experimental
{
namespace server
{

Tunnel::Tunnel(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_Experimental_Tunnel_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Tunnel::Tunnel(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Tunnel(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Tunnel::interfaceName() const ->
        std::string
{
    return _interfaceName;
}

int Tunnel::_callback_get_InterfaceName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->interfaceName();
                    }
                ));
    }
}

auto Tunnel::interfaceName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_interfaceName != value)
    {
        _interfaceName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("InterfaceName");
        }
    }

    return _interfaceName;
}

auto Tunnel::interfaceName(std::string val) ->
        std::string
{
    return interfaceName(val, false);
}

int Tunnel::_callback_set_InterfaceName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->interfaceName(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_InterfaceName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Tunnel::local() const ->
        std::string
{
    return _local;
}

int Tunnel::_callback_get_Local(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->local();
                    }
                ));
    }
}

auto Tunnel::local(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_local != value)
    {
        _local = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("Local");
        }
    }

    return _local;
}

auto Tunnel::local(std::string val) ->
        std::string
{
    return local(val, false);
}

int Tunnel::_callback_set_Local(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->local(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_Local =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Tunnel::remote() const ->
        std::string
{
    return _remote;
}

int Tunnel::_callback_get_Remote(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->remote();
                    }
                ));
    }
}

auto Tunnel::remote(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_remote != value)
    {
        _remote = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("Remote");
        }
    }

    return _remote;
}

auto Tunnel::remote(std::string val) ->
        std::string
{
    return remote(val, false);
}

int Tunnel::_callback_set_Remote(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->remote(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_Remote =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Tunnel::tos() const ->
        uint32_t
{
    return _tos;
}

int Tunnel::_callback_get_TOS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tos();
                    }
                ));
    }
}

auto Tunnel::tos(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_tos != value)
    {
        _tos = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("TOS");
        }
    }

    return _tos;
}

auto Tunnel::tos(uint32_t val) ->
        uint32_t
{
    return tos(val, false);
}

int Tunnel::_callback_set_TOS(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->tos(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_TOS =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Tunnel::ttl() const ->
        uint32_t
{
    return _ttl;
}

int Tunnel::_callback_get_TTL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ttl();
                    }
                ));
    }
}

auto Tunnel::ttl(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_ttl != value)
    {
        _ttl = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("TTL");
        }
    }

    return _ttl;
}

auto Tunnel::ttl(uint32_t val) ->
        uint32_t
{
    return ttl(val, false);
}

int Tunnel::_callback_set_TTL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->ttl(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_TTL =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Tunnel::discoverPathMTU() const ->
        bool
{
    return _discoverPathMTU;
}

int Tunnel::_callback_get_DiscoverPathMTU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->discoverPathMTU();
                    }
                ));
    }
}

auto Tunnel::discoverPathMTU(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_discoverPathMTU != value)
    {
        _discoverPathMTU = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("DiscoverPathMTU");
        }
    }

    return _discoverPathMTU;
}

auto Tunnel::discoverPathMTU(bool val) ->
        bool
{
    return discoverPathMTU(val, false);
}

int Tunnel::_callback_set_DiscoverPathMTU(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->discoverPathMTU(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_DiscoverPathMTU =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Tunnel::ipv6FlowLabel() const ->
        uint32_t
{
    return _ipv6FlowLabel;
}

int Tunnel::_callback_get_IPv6FlowLabel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ipv6FlowLabel();
                    }
                ));
    }
}

auto Tunnel::ipv6FlowLabel(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_ipv6FlowLabel != value)
    {
        _ipv6FlowLabel = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("IPv6FlowLabel");
        }
    }

    return _ipv6FlowLabel;
}

auto Tunnel::ipv6FlowLabel(uint32_t val) ->
        uint32_t
{
    return ipv6FlowLabel(val, false);
}

int Tunnel::_callback_set_IPv6FlowLabel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->ipv6FlowLabel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_IPv6FlowLabel =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Tunnel::copyDSCP() const ->
        bool
{
    return _copyDSCP;
}

int Tunnel::_callback_get_CopyDSCP(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->copyDSCP();
                    }
                ));
    }
}

auto Tunnel::copyDSCP(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_copyDSCP != value)
    {
        _copyDSCP = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("CopyDSCP");
        }
    }

    return _copyDSCP;
}

auto Tunnel::copyDSCP(bool val) ->
        bool
{
    return copyDSCP(val, false);
}

int Tunnel::_callback_set_CopyDSCP(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->copyDSCP(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_CopyDSCP =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Tunnel::encapsulationLimit() const ->
        uint32_t
{
    return _encapsulationLimit;
}

int Tunnel::_callback_get_EncapsulationLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->encapsulationLimit();
                    }
                ));
    }
}

auto Tunnel::encapsulationLimit(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_encapsulationLimit != value)
    {
        _encapsulationLimit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("EncapsulationLimit");
        }
    }

    return _encapsulationLimit;
}

auto Tunnel::encapsulationLimit(uint32_t val) ->
        uint32_t
{
    return encapsulationLimit(val, false);
}

int Tunnel::_callback_set_EncapsulationLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->encapsulationLimit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_EncapsulationLimit =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Tunnel::key() const ->
        std::string
{
    return _key;
}

int Tunnel::_callback_get_Key(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->key();
                    }
                ));
    }
}

auto Tunnel::key(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_key != value)
    {
        _key = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("Key");
        }
    }

    return _key;
}

auto Tunnel::key(std::string val) ->
        std::string
{
    return key(val, false);
}

int Tunnel::_callback_set_Key(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->key(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_Key =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Tunnel::inputKey() const ->
        std::string
{
    return _inputKey;
}

int Tunnel::_callback_get_InputKey(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->inputKey();
                    }
                ));
    }
}

auto Tunnel::inputKey(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_inputKey != value)
    {
        _inputKey = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("InputKey");
        }
    }

    return _inputKey;
}

auto Tunnel::inputKey(std::string val) ->
        std::string
{
    return inputKey(val, false);
}

int Tunnel::_callback_set_InputKey(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->inputKey(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_InputKey =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Tunnel::outputKey() const ->
        std::string
{
    return _outputKey;
}

int Tunnel::_callback_get_OutputKey(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->outputKey();
                    }
                ));
    }
}

auto Tunnel::outputKey(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_outputKey != value)
    {
        _outputKey = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("OutputKey");
        }
    }

    return _outputKey;
}

auto Tunnel::outputKey(std::string val) ->
        std::string
{
    return outputKey(val, false);
}

int Tunnel::_callback_set_OutputKey(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->outputKey(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_OutputKey =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Tunnel::mode() const ->
        IPv6Tunnel
{
    return _mode;
}

int Tunnel::_callback_get_Mode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mode();
                    }
                ));
    }
}

auto Tunnel::mode(IPv6Tunnel value,
                                         bool skipSignal) ->
        IPv6Tunnel
{
    if (_mode != value)
    {
        _mode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Tunnel_interface.property_changed("Mode");
        }
    }

    return _mode;
}

auto Tunnel::mode(IPv6Tunnel val) ->
        IPv6Tunnel
{
    return mode(val, false);
}

int Tunnel::_callback_set_Mode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Tunnel*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](IPv6Tunnel&& arg)
                    {
                        o->mode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Tunnel
{
static const auto _property_Mode =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::Experimental::server::Tunnel::IPv6Tunnel>());
}
}

void Tunnel::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "InterfaceName")
    {
        auto& v = std::get<std::string>(val);
        interfaceName(v, skipSignal);
        return;
    }
    if (_name == "Local")
    {
        auto& v = std::get<std::string>(val);
        local(v, skipSignal);
        return;
    }
    if (_name == "Remote")
    {
        auto& v = std::get<std::string>(val);
        remote(v, skipSignal);
        return;
    }
    if (_name == "TOS")
    {
        auto& v = std::get<uint32_t>(val);
        tos(v, skipSignal);
        return;
    }
    if (_name == "TTL")
    {
        auto& v = std::get<uint32_t>(val);
        ttl(v, skipSignal);
        return;
    }
    if (_name == "DiscoverPathMTU")
    {
        auto& v = std::get<bool>(val);
        discoverPathMTU(v, skipSignal);
        return;
    }
    if (_name == "IPv6FlowLabel")
    {
        auto& v = std::get<uint32_t>(val);
        ipv6FlowLabel(v, skipSignal);
        return;
    }
    if (_name == "CopyDSCP")
    {
        auto& v = std::get<bool>(val);
        copyDSCP(v, skipSignal);
        return;
    }
    if (_name == "EncapsulationLimit")
    {
        auto& v = std::get<uint32_t>(val);
        encapsulationLimit(v, skipSignal);
        return;
    }
    if (_name == "Key")
    {
        auto& v = std::get<std::string>(val);
        key(v, skipSignal);
        return;
    }
    if (_name == "InputKey")
    {
        auto& v = std::get<std::string>(val);
        inputKey(v, skipSignal);
        return;
    }
    if (_name == "OutputKey")
    {
        auto& v = std::get<std::string>(val);
        outputKey(v, skipSignal);
        return;
    }
    if (_name == "Mode")
    {
        auto& v = std::get<IPv6Tunnel>(val);
        mode(v, skipSignal);
        return;
    }
}

auto Tunnel::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "InterfaceName")
    {
        return interfaceName();
    }
    if (_name == "Local")
    {
        return local();
    }
    if (_name == "Remote")
    {
        return remote();
    }
    if (_name == "TOS")
    {
        return tos();
    }
    if (_name == "TTL")
    {
        return ttl();
    }
    if (_name == "DiscoverPathMTU")
    {
        return discoverPathMTU();
    }
    if (_name == "IPv6FlowLabel")
    {
        return ipv6FlowLabel();
    }
    if (_name == "CopyDSCP")
    {
        return copyDSCP();
    }
    if (_name == "EncapsulationLimit")
    {
        return encapsulationLimit();
    }
    if (_name == "Key")
    {
        return key();
    }
    if (_name == "InputKey")
    {
        return inputKey();
    }
    if (_name == "OutputKey")
    {
        return outputKey();
    }
    if (_name == "Mode")
    {
        return mode();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Tunnel::IPv6Tunnel */
static const std::tuple<const char*, Tunnel::IPv6Tunnel> mappingTunnelIPv6Tunnel[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Tunnel.IPv6Tunnel.ip6ip6",                 Tunnel::IPv6Tunnel::ip6ip6 ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Tunnel.IPv6Tunnel.ipip6",                 Tunnel::IPv6Tunnel::ipip6 ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Tunnel.IPv6Tunnel.any",                 Tunnel::IPv6Tunnel::any ),
        };

} // anonymous namespace

auto Tunnel::convertStringToIPv6Tunnel(const std::string& s) noexcept ->
        std::optional<IPv6Tunnel>
{
    auto i = std::find_if(
            std::begin(mappingTunnelIPv6Tunnel),
            std::end(mappingTunnelIPv6Tunnel),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingTunnelIPv6Tunnel) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Tunnel::convertIPv6TunnelFromString(const std::string& s) ->
        IPv6Tunnel
{
    auto r = convertStringToIPv6Tunnel(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Tunnel::convertIPv6TunnelToString(Tunnel::IPv6Tunnel v)
{
    auto i = std::find_if(
            std::begin(mappingTunnelIPv6Tunnel),
            std::end(mappingTunnelIPv6Tunnel),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingTunnelIPv6Tunnel))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Tunnel::_vtable[] = {
    vtable::start(),
    vtable::property("InterfaceName",
                     details::Tunnel::_property_InterfaceName
                        .data(),
                     _callback_get_InterfaceName,
                     _callback_set_InterfaceName,
                     vtable::property_::emits_change),
    vtable::property("Local",
                     details::Tunnel::_property_Local
                        .data(),
                     _callback_get_Local,
                     _callback_set_Local,
                     vtable::property_::emits_change),
    vtable::property("Remote",
                     details::Tunnel::_property_Remote
                        .data(),
                     _callback_get_Remote,
                     _callback_set_Remote,
                     vtable::property_::emits_change),
    vtable::property("TOS",
                     details::Tunnel::_property_TOS
                        .data(),
                     _callback_get_TOS,
                     _callback_set_TOS,
                     vtable::property_::emits_change),
    vtable::property("TTL",
                     details::Tunnel::_property_TTL
                        .data(),
                     _callback_get_TTL,
                     _callback_set_TTL,
                     vtable::property_::emits_change),
    vtable::property("DiscoverPathMTU",
                     details::Tunnel::_property_DiscoverPathMTU
                        .data(),
                     _callback_get_DiscoverPathMTU,
                     _callback_set_DiscoverPathMTU,
                     vtable::property_::emits_change),
    vtable::property("IPv6FlowLabel",
                     details::Tunnel::_property_IPv6FlowLabel
                        .data(),
                     _callback_get_IPv6FlowLabel,
                     _callback_set_IPv6FlowLabel,
                     vtable::property_::emits_change),
    vtable::property("CopyDSCP",
                     details::Tunnel::_property_CopyDSCP
                        .data(),
                     _callback_get_CopyDSCP,
                     _callback_set_CopyDSCP,
                     vtable::property_::emits_change),
    vtable::property("EncapsulationLimit",
                     details::Tunnel::_property_EncapsulationLimit
                        .data(),
                     _callback_get_EncapsulationLimit,
                     _callback_set_EncapsulationLimit,
                     vtable::property_::emits_change),
    vtable::property("Key",
                     details::Tunnel::_property_Key
                        .data(),
                     _callback_get_Key,
                     _callback_set_Key,
                     vtable::property_::emits_change),
    vtable::property("InputKey",
                     details::Tunnel::_property_InputKey
                        .data(),
                     _callback_get_InputKey,
                     _callback_set_InputKey,
                     vtable::property_::emits_change),
    vtable::property("OutputKey",
                     details::Tunnel::_property_OutputKey
                        .data(),
                     _callback_get_OutputKey,
                     _callback_set_OutputKey,
                     vtable::property_::emits_change),
    vtable::property("Mode",
                     details::Tunnel::_property_Mode
                        .data(),
                     _callback_get_Mode,
                     _callback_set_Mode,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Experimental
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

