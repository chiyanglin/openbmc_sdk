#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/server.hpp>























namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

PersistentMemory::PersistentMemory(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PersistentMemory::PersistentMemory(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PersistentMemory(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PersistentMemory::moduleManufacturerID() const ->
        uint16_t
{
    return _moduleManufacturerID;
}

int PersistentMemory::_callback_get_ModuleManufacturerID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->moduleManufacturerID();
                    }
                ));
    }
}

auto PersistentMemory::moduleManufacturerID(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_moduleManufacturerID != value)
    {
        _moduleManufacturerID = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("ModuleManufacturerID");
        }
    }

    return _moduleManufacturerID;
}

auto PersistentMemory::moduleManufacturerID(uint16_t val) ->
        uint16_t
{
    return moduleManufacturerID(val, false);
}

int PersistentMemory::_callback_set_ModuleManufacturerID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->moduleManufacturerID(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_ModuleManufacturerID =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto PersistentMemory::moduleProductID() const ->
        uint16_t
{
    return _moduleProductID;
}

int PersistentMemory::_callback_get_ModuleProductID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->moduleProductID();
                    }
                ));
    }
}

auto PersistentMemory::moduleProductID(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_moduleProductID != value)
    {
        _moduleProductID = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("ModuleProductID");
        }
    }

    return _moduleProductID;
}

auto PersistentMemory::moduleProductID(uint16_t val) ->
        uint16_t
{
    return moduleProductID(val, false);
}

int PersistentMemory::_callback_set_ModuleProductID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->moduleProductID(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_ModuleProductID =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto PersistentMemory::subsystemVendorID() const ->
        uint16_t
{
    return _subsystemVendorID;
}

int PersistentMemory::_callback_get_SubsystemVendorID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->subsystemVendorID();
                    }
                ));
    }
}

auto PersistentMemory::subsystemVendorID(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_subsystemVendorID != value)
    {
        _subsystemVendorID = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("SubsystemVendorID");
        }
    }

    return _subsystemVendorID;
}

auto PersistentMemory::subsystemVendorID(uint16_t val) ->
        uint16_t
{
    return subsystemVendorID(val, false);
}

int PersistentMemory::_callback_set_SubsystemVendorID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->subsystemVendorID(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_SubsystemVendorID =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto PersistentMemory::subsystemDeviceID() const ->
        uint16_t
{
    return _subsystemDeviceID;
}

int PersistentMemory::_callback_get_SubsystemDeviceID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->subsystemDeviceID();
                    }
                ));
    }
}

auto PersistentMemory::subsystemDeviceID(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_subsystemDeviceID != value)
    {
        _subsystemDeviceID = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("SubsystemDeviceID");
        }
    }

    return _subsystemDeviceID;
}

auto PersistentMemory::subsystemDeviceID(uint16_t val) ->
        uint16_t
{
    return subsystemDeviceID(val, false);
}

int PersistentMemory::_callback_set_SubsystemDeviceID(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->subsystemDeviceID(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_SubsystemDeviceID =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto PersistentMemory::volatileRegionSizeLimitInKiB() const ->
        uint64_t
{
    return _volatileRegionSizeLimitInKiB;
}

int PersistentMemory::_callback_get_VolatileRegionSizeLimitInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->volatileRegionSizeLimitInKiB();
                    }
                ));
    }
}

auto PersistentMemory::volatileRegionSizeLimitInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_volatileRegionSizeLimitInKiB != value)
    {
        _volatileRegionSizeLimitInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("VolatileRegionSizeLimitInKiB");
        }
    }

    return _volatileRegionSizeLimitInKiB;
}

auto PersistentMemory::volatileRegionSizeLimitInKiB(uint64_t val) ->
        uint64_t
{
    return volatileRegionSizeLimitInKiB(val, false);
}

int PersistentMemory::_callback_set_VolatileRegionSizeLimitInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->volatileRegionSizeLimitInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_VolatileRegionSizeLimitInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::pmRegionSizeLimitInKiB() const ->
        uint64_t
{
    return _pmRegionSizeLimitInKiB;
}

int PersistentMemory::_callback_get_PmRegionSizeLimitInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pmRegionSizeLimitInKiB();
                    }
                ));
    }
}

auto PersistentMemory::pmRegionSizeLimitInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_pmRegionSizeLimitInKiB != value)
    {
        _pmRegionSizeLimitInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("PmRegionSizeLimitInKiB");
        }
    }

    return _pmRegionSizeLimitInKiB;
}

auto PersistentMemory::pmRegionSizeLimitInKiB(uint64_t val) ->
        uint64_t
{
    return pmRegionSizeLimitInKiB(val, false);
}

int PersistentMemory::_callback_set_PmRegionSizeLimitInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->pmRegionSizeLimitInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_PmRegionSizeLimitInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::volatileSizeInKiB() const ->
        uint64_t
{
    return _volatileSizeInKiB;
}

int PersistentMemory::_callback_get_VolatileSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->volatileSizeInKiB();
                    }
                ));
    }
}

auto PersistentMemory::volatileSizeInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_volatileSizeInKiB != value)
    {
        _volatileSizeInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("VolatileSizeInKiB");
        }
    }

    return _volatileSizeInKiB;
}

auto PersistentMemory::volatileSizeInKiB(uint64_t val) ->
        uint64_t
{
    return volatileSizeInKiB(val, false);
}

int PersistentMemory::_callback_set_VolatileSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->volatileSizeInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_VolatileSizeInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::pmSizeInKiB() const ->
        uint64_t
{
    return _pmSizeInKiB;
}

int PersistentMemory::_callback_get_PmSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pmSizeInKiB();
                    }
                ));
    }
}

auto PersistentMemory::pmSizeInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_pmSizeInKiB != value)
    {
        _pmSizeInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("PmSizeInKiB");
        }
    }

    return _pmSizeInKiB;
}

auto PersistentMemory::pmSizeInKiB(uint64_t val) ->
        uint64_t
{
    return pmSizeInKiB(val, false);
}

int PersistentMemory::_callback_set_PmSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->pmSizeInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_PmSizeInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::cacheSizeInKiB() const ->
        uint64_t
{
    return _cacheSizeInKiB;
}

int PersistentMemory::_callback_get_CacheSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->cacheSizeInKiB();
                    }
                ));
    }
}

auto PersistentMemory::cacheSizeInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_cacheSizeInKiB != value)
    {
        _cacheSizeInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("CacheSizeInKiB");
        }
    }

    return _cacheSizeInKiB;
}

auto PersistentMemory::cacheSizeInKiB(uint64_t val) ->
        uint64_t
{
    return cacheSizeInKiB(val, false);
}

int PersistentMemory::_callback_set_CacheSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->cacheSizeInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_CacheSizeInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::volatileRegionMaxSizeInKiB() const ->
        uint64_t
{
    return _volatileRegionMaxSizeInKiB;
}

int PersistentMemory::_callback_get_VolatileRegionMaxSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->volatileRegionMaxSizeInKiB();
                    }
                ));
    }
}

auto PersistentMemory::volatileRegionMaxSizeInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_volatileRegionMaxSizeInKiB != value)
    {
        _volatileRegionMaxSizeInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("VolatileRegionMaxSizeInKiB");
        }
    }

    return _volatileRegionMaxSizeInKiB;
}

auto PersistentMemory::volatileRegionMaxSizeInKiB(uint64_t val) ->
        uint64_t
{
    return volatileRegionMaxSizeInKiB(val, false);
}

int PersistentMemory::_callback_set_VolatileRegionMaxSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->volatileRegionMaxSizeInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_VolatileRegionMaxSizeInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::pmRegionMaxSizeInKiB() const ->
        uint64_t
{
    return _pmRegionMaxSizeInKiB;
}

int PersistentMemory::_callback_get_PmRegionMaxSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pmRegionMaxSizeInKiB();
                    }
                ));
    }
}

auto PersistentMemory::pmRegionMaxSizeInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_pmRegionMaxSizeInKiB != value)
    {
        _pmRegionMaxSizeInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("PmRegionMaxSizeInKiB");
        }
    }

    return _pmRegionMaxSizeInKiB;
}

auto PersistentMemory::pmRegionMaxSizeInKiB(uint64_t val) ->
        uint64_t
{
    return pmRegionMaxSizeInKiB(val, false);
}

int PersistentMemory::_callback_set_PmRegionMaxSizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->pmRegionMaxSizeInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_PmRegionMaxSizeInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::allocationIncrementInKiB() const ->
        uint64_t
{
    return _allocationIncrementInKiB;
}

int PersistentMemory::_callback_get_AllocationIncrementInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allocationIncrementInKiB();
                    }
                ));
    }
}

auto PersistentMemory::allocationIncrementInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_allocationIncrementInKiB != value)
    {
        _allocationIncrementInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("AllocationIncrementInKiB");
        }
    }

    return _allocationIncrementInKiB;
}

auto PersistentMemory::allocationIncrementInKiB(uint64_t val) ->
        uint64_t
{
    return allocationIncrementInKiB(val, false);
}

int PersistentMemory::_callback_set_AllocationIncrementInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->allocationIncrementInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_AllocationIncrementInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::allocationAlignmentInKiB() const ->
        uint64_t
{
    return _allocationAlignmentInKiB;
}

int PersistentMemory::_callback_get_AllocationAlignmentInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allocationAlignmentInKiB();
                    }
                ));
    }
}

auto PersistentMemory::allocationAlignmentInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_allocationAlignmentInKiB != value)
    {
        _allocationAlignmentInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("AllocationAlignmentInKiB");
        }
    }

    return _allocationAlignmentInKiB;
}

auto PersistentMemory::allocationAlignmentInKiB(uint64_t val) ->
        uint64_t
{
    return allocationAlignmentInKiB(val, false);
}

int PersistentMemory::_callback_set_AllocationAlignmentInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->allocationAlignmentInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_AllocationAlignmentInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto PersistentMemory::volatileRegionNumberLimit() const ->
        uint32_t
{
    return _volatileRegionNumberLimit;
}

int PersistentMemory::_callback_get_VolatileRegionNumberLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->volatileRegionNumberLimit();
                    }
                ));
    }
}

auto PersistentMemory::volatileRegionNumberLimit(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_volatileRegionNumberLimit != value)
    {
        _volatileRegionNumberLimit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("VolatileRegionNumberLimit");
        }
    }

    return _volatileRegionNumberLimit;
}

auto PersistentMemory::volatileRegionNumberLimit(uint32_t val) ->
        uint32_t
{
    return volatileRegionNumberLimit(val, false);
}

int PersistentMemory::_callback_set_VolatileRegionNumberLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->volatileRegionNumberLimit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_VolatileRegionNumberLimit =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto PersistentMemory::pmRegionNumberLimit() const ->
        uint32_t
{
    return _pmRegionNumberLimit;
}

int PersistentMemory::_callback_get_PmRegionNumberLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pmRegionNumberLimit();
                    }
                ));
    }
}

auto PersistentMemory::pmRegionNumberLimit(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_pmRegionNumberLimit != value)
    {
        _pmRegionNumberLimit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("PmRegionNumberLimit");
        }
    }

    return _pmRegionNumberLimit;
}

auto PersistentMemory::pmRegionNumberLimit(uint32_t val) ->
        uint32_t
{
    return pmRegionNumberLimit(val, false);
}

int PersistentMemory::_callback_set_PmRegionNumberLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->pmRegionNumberLimit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_PmRegionNumberLimit =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto PersistentMemory::spareDeviceCount() const ->
        uint32_t
{
    return _spareDeviceCount;
}

int PersistentMemory::_callback_get_SpareDeviceCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->spareDeviceCount();
                    }
                ));
    }
}

auto PersistentMemory::spareDeviceCount(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_spareDeviceCount != value)
    {
        _spareDeviceCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("SpareDeviceCount");
        }
    }

    return _spareDeviceCount;
}

auto PersistentMemory::spareDeviceCount(uint32_t val) ->
        uint32_t
{
    return spareDeviceCount(val, false);
}

int PersistentMemory::_callback_set_SpareDeviceCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->spareDeviceCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_SpareDeviceCount =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto PersistentMemory::isSpareDeviceInUse() const ->
        bool
{
    return _isSpareDeviceInUse;
}

int PersistentMemory::_callback_get_IsSpareDeviceInUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->isSpareDeviceInUse();
                    }
                ));
    }
}

auto PersistentMemory::isSpareDeviceInUse(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_isSpareDeviceInUse != value)
    {
        _isSpareDeviceInUse = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("IsSpareDeviceInUse");
        }
    }

    return _isSpareDeviceInUse;
}

auto PersistentMemory::isSpareDeviceInUse(bool val) ->
        bool
{
    return isSpareDeviceInUse(val, false);
}

int PersistentMemory::_callback_set_IsSpareDeviceInUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->isSpareDeviceInUse(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_IsSpareDeviceInUse =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PersistentMemory::isRankSpareEnabled() const ->
        bool
{
    return _isRankSpareEnabled;
}

int PersistentMemory::_callback_get_IsRankSpareEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->isRankSpareEnabled();
                    }
                ));
    }
}

auto PersistentMemory::isRankSpareEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_isRankSpareEnabled != value)
    {
        _isRankSpareEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("IsRankSpareEnabled");
        }
    }

    return _isRankSpareEnabled;
}

auto PersistentMemory::isRankSpareEnabled(bool val) ->
        bool
{
    return isRankSpareEnabled(val, false);
}

int PersistentMemory::_callback_set_IsRankSpareEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->isRankSpareEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_IsRankSpareEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PersistentMemory::maxAveragePowerLimitmW() const ->
        std::vector<uint32_t>
{
    return _maxAveragePowerLimitmW;
}

int PersistentMemory::_callback_get_MaxAveragePowerLimitmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxAveragePowerLimitmW();
                    }
                ));
    }
}

auto PersistentMemory::maxAveragePowerLimitmW(std::vector<uint32_t> value,
                                         bool skipSignal) ->
        std::vector<uint32_t>
{
    if (_maxAveragePowerLimitmW != value)
    {
        _maxAveragePowerLimitmW = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("MaxAveragePowerLimitmW");
        }
    }

    return _maxAveragePowerLimitmW;
}

auto PersistentMemory::maxAveragePowerLimitmW(std::vector<uint32_t> val) ->
        std::vector<uint32_t>
{
    return maxAveragePowerLimitmW(val, false);
}

int PersistentMemory::_callback_set_MaxAveragePowerLimitmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint32_t>&& arg)
                    {
                        o->maxAveragePowerLimitmW(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_MaxAveragePowerLimitmW =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint32_t>>());
}
}

auto PersistentMemory::currentSecurityState() const ->
        std::string
{
    return _currentSecurityState;
}

int PersistentMemory::_callback_get_CurrentSecurityState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentSecurityState();
                    }
                ));
    }
}

auto PersistentMemory::currentSecurityState(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_currentSecurityState != value)
    {
        _currentSecurityState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("CurrentSecurityState");
        }
    }

    return _currentSecurityState;
}

auto PersistentMemory::currentSecurityState(std::string val) ->
        std::string
{
    return currentSecurityState(val, false);
}

int PersistentMemory::_callback_set_CurrentSecurityState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->currentSecurityState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_CurrentSecurityState =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PersistentMemory::configurationLocked() const ->
        bool
{
    return _configurationLocked;
}

int PersistentMemory::_callback_get_ConfigurationLocked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->configurationLocked();
                    }
                ));
    }
}

auto PersistentMemory::configurationLocked(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_configurationLocked != value)
    {
        _configurationLocked = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("ConfigurationLocked");
        }
    }

    return _configurationLocked;
}

auto PersistentMemory::configurationLocked(bool val) ->
        bool
{
    return configurationLocked(val, false);
}

int PersistentMemory::_callback_set_ConfigurationLocked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->configurationLocked(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_ConfigurationLocked =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PersistentMemory::allowedMemoryModes() const ->
        std::vector<MemoryModes>
{
    return _allowedMemoryModes;
}

int PersistentMemory::_callback_get_AllowedMemoryModes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allowedMemoryModes();
                    }
                ));
    }
}

auto PersistentMemory::allowedMemoryModes(std::vector<MemoryModes> value,
                                         bool skipSignal) ->
        std::vector<MemoryModes>
{
    if (_allowedMemoryModes != value)
    {
        _allowedMemoryModes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.property_changed("AllowedMemoryModes");
        }
    }

    return _allowedMemoryModes;
}

auto PersistentMemory::allowedMemoryModes(std::vector<MemoryModes> val) ->
        std::vector<MemoryModes>
{
    return allowedMemoryModes(val, false);
}

int PersistentMemory::_callback_set_AllowedMemoryModes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PersistentMemory*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<MemoryModes>&& arg)
                    {
                        o->allowedMemoryModes(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PersistentMemory
{
static const auto _property_AllowedMemoryModes =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::xyz::openbmc_project::Inventory::Item::server::PersistentMemory::MemoryModes>>());
}
}

void PersistentMemory::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ModuleManufacturerID")
    {
        auto& v = std::get<uint16_t>(val);
        moduleManufacturerID(v, skipSignal);
        return;
    }
    if (_name == "ModuleProductID")
    {
        auto& v = std::get<uint16_t>(val);
        moduleProductID(v, skipSignal);
        return;
    }
    if (_name == "SubsystemVendorID")
    {
        auto& v = std::get<uint16_t>(val);
        subsystemVendorID(v, skipSignal);
        return;
    }
    if (_name == "SubsystemDeviceID")
    {
        auto& v = std::get<uint16_t>(val);
        subsystemDeviceID(v, skipSignal);
        return;
    }
    if (_name == "VolatileRegionSizeLimitInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        volatileRegionSizeLimitInKiB(v, skipSignal);
        return;
    }
    if (_name == "PmRegionSizeLimitInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        pmRegionSizeLimitInKiB(v, skipSignal);
        return;
    }
    if (_name == "VolatileSizeInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        volatileSizeInKiB(v, skipSignal);
        return;
    }
    if (_name == "PmSizeInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        pmSizeInKiB(v, skipSignal);
        return;
    }
    if (_name == "CacheSizeInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        cacheSizeInKiB(v, skipSignal);
        return;
    }
    if (_name == "VolatileRegionMaxSizeInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        volatileRegionMaxSizeInKiB(v, skipSignal);
        return;
    }
    if (_name == "PmRegionMaxSizeInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        pmRegionMaxSizeInKiB(v, skipSignal);
        return;
    }
    if (_name == "AllocationIncrementInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        allocationIncrementInKiB(v, skipSignal);
        return;
    }
    if (_name == "AllocationAlignmentInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        allocationAlignmentInKiB(v, skipSignal);
        return;
    }
    if (_name == "VolatileRegionNumberLimit")
    {
        auto& v = std::get<uint32_t>(val);
        volatileRegionNumberLimit(v, skipSignal);
        return;
    }
    if (_name == "PmRegionNumberLimit")
    {
        auto& v = std::get<uint32_t>(val);
        pmRegionNumberLimit(v, skipSignal);
        return;
    }
    if (_name == "SpareDeviceCount")
    {
        auto& v = std::get<uint32_t>(val);
        spareDeviceCount(v, skipSignal);
        return;
    }
    if (_name == "IsSpareDeviceInUse")
    {
        auto& v = std::get<bool>(val);
        isSpareDeviceInUse(v, skipSignal);
        return;
    }
    if (_name == "IsRankSpareEnabled")
    {
        auto& v = std::get<bool>(val);
        isRankSpareEnabled(v, skipSignal);
        return;
    }
    if (_name == "MaxAveragePowerLimitmW")
    {
        auto& v = std::get<std::vector<uint32_t>>(val);
        maxAveragePowerLimitmW(v, skipSignal);
        return;
    }
    if (_name == "CurrentSecurityState")
    {
        auto& v = std::get<std::string>(val);
        currentSecurityState(v, skipSignal);
        return;
    }
    if (_name == "ConfigurationLocked")
    {
        auto& v = std::get<bool>(val);
        configurationLocked(v, skipSignal);
        return;
    }
    if (_name == "AllowedMemoryModes")
    {
        auto& v = std::get<std::vector<MemoryModes>>(val);
        allowedMemoryModes(v, skipSignal);
        return;
    }
}

auto PersistentMemory::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ModuleManufacturerID")
    {
        return moduleManufacturerID();
    }
    if (_name == "ModuleProductID")
    {
        return moduleProductID();
    }
    if (_name == "SubsystemVendorID")
    {
        return subsystemVendorID();
    }
    if (_name == "SubsystemDeviceID")
    {
        return subsystemDeviceID();
    }
    if (_name == "VolatileRegionSizeLimitInKiB")
    {
        return volatileRegionSizeLimitInKiB();
    }
    if (_name == "PmRegionSizeLimitInKiB")
    {
        return pmRegionSizeLimitInKiB();
    }
    if (_name == "VolatileSizeInKiB")
    {
        return volatileSizeInKiB();
    }
    if (_name == "PmSizeInKiB")
    {
        return pmSizeInKiB();
    }
    if (_name == "CacheSizeInKiB")
    {
        return cacheSizeInKiB();
    }
    if (_name == "VolatileRegionMaxSizeInKiB")
    {
        return volatileRegionMaxSizeInKiB();
    }
    if (_name == "PmRegionMaxSizeInKiB")
    {
        return pmRegionMaxSizeInKiB();
    }
    if (_name == "AllocationIncrementInKiB")
    {
        return allocationIncrementInKiB();
    }
    if (_name == "AllocationAlignmentInKiB")
    {
        return allocationAlignmentInKiB();
    }
    if (_name == "VolatileRegionNumberLimit")
    {
        return volatileRegionNumberLimit();
    }
    if (_name == "PmRegionNumberLimit")
    {
        return pmRegionNumberLimit();
    }
    if (_name == "SpareDeviceCount")
    {
        return spareDeviceCount();
    }
    if (_name == "IsSpareDeviceInUse")
    {
        return isSpareDeviceInUse();
    }
    if (_name == "IsRankSpareEnabled")
    {
        return isRankSpareEnabled();
    }
    if (_name == "MaxAveragePowerLimitmW")
    {
        return maxAveragePowerLimitmW();
    }
    if (_name == "CurrentSecurityState")
    {
        return currentSecurityState();
    }
    if (_name == "ConfigurationLocked")
    {
        return configurationLocked();
    }
    if (_name == "AllowedMemoryModes")
    {
        return allowedMemoryModes();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for PersistentMemory::MemoryModes */
static const std::tuple<const char*, PersistentMemory::MemoryModes> mappingPersistentMemoryMemoryModes[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PersistentMemory.MemoryModes.Volatile",                 PersistentMemory::MemoryModes::Volatile ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PersistentMemory.MemoryModes.Persistent",                 PersistentMemory::MemoryModes::Persistent ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.PersistentMemory.MemoryModes.Block",                 PersistentMemory::MemoryModes::Block ),
        };

} // anonymous namespace

auto PersistentMemory::convertStringToMemoryModes(const std::string& s) noexcept ->
        std::optional<MemoryModes>
{
    auto i = std::find_if(
            std::begin(mappingPersistentMemoryMemoryModes),
            std::end(mappingPersistentMemoryMemoryModes),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPersistentMemoryMemoryModes) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PersistentMemory::convertMemoryModesFromString(const std::string& s) ->
        MemoryModes
{
    auto r = convertStringToMemoryModes(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PersistentMemory::convertMemoryModesToString(PersistentMemory::MemoryModes v)
{
    auto i = std::find_if(
            std::begin(mappingPersistentMemoryMemoryModes),
            std::end(mappingPersistentMemoryMemoryModes),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPersistentMemoryMemoryModes))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t PersistentMemory::_vtable[] = {
    vtable::start(),
    vtable::property("ModuleManufacturerID",
                     details::PersistentMemory::_property_ModuleManufacturerID
                        .data(),
                     _callback_get_ModuleManufacturerID,
                     _callback_set_ModuleManufacturerID,
                     vtable::property_::emits_change),
    vtable::property("ModuleProductID",
                     details::PersistentMemory::_property_ModuleProductID
                        .data(),
                     _callback_get_ModuleProductID,
                     _callback_set_ModuleProductID,
                     vtable::property_::emits_change),
    vtable::property("SubsystemVendorID",
                     details::PersistentMemory::_property_SubsystemVendorID
                        .data(),
                     _callback_get_SubsystemVendorID,
                     _callback_set_SubsystemVendorID,
                     vtable::property_::emits_change),
    vtable::property("SubsystemDeviceID",
                     details::PersistentMemory::_property_SubsystemDeviceID
                        .data(),
                     _callback_get_SubsystemDeviceID,
                     _callback_set_SubsystemDeviceID,
                     vtable::property_::emits_change),
    vtable::property("VolatileRegionSizeLimitInKiB",
                     details::PersistentMemory::_property_VolatileRegionSizeLimitInKiB
                        .data(),
                     _callback_get_VolatileRegionSizeLimitInKiB,
                     _callback_set_VolatileRegionSizeLimitInKiB,
                     vtable::property_::emits_change),
    vtable::property("PmRegionSizeLimitInKiB",
                     details::PersistentMemory::_property_PmRegionSizeLimitInKiB
                        .data(),
                     _callback_get_PmRegionSizeLimitInKiB,
                     _callback_set_PmRegionSizeLimitInKiB,
                     vtable::property_::emits_change),
    vtable::property("VolatileSizeInKiB",
                     details::PersistentMemory::_property_VolatileSizeInKiB
                        .data(),
                     _callback_get_VolatileSizeInKiB,
                     _callback_set_VolatileSizeInKiB,
                     vtable::property_::emits_change),
    vtable::property("PmSizeInKiB",
                     details::PersistentMemory::_property_PmSizeInKiB
                        .data(),
                     _callback_get_PmSizeInKiB,
                     _callback_set_PmSizeInKiB,
                     vtable::property_::emits_change),
    vtable::property("CacheSizeInKiB",
                     details::PersistentMemory::_property_CacheSizeInKiB
                        .data(),
                     _callback_get_CacheSizeInKiB,
                     _callback_set_CacheSizeInKiB,
                     vtable::property_::emits_change),
    vtable::property("VolatileRegionMaxSizeInKiB",
                     details::PersistentMemory::_property_VolatileRegionMaxSizeInKiB
                        .data(),
                     _callback_get_VolatileRegionMaxSizeInKiB,
                     _callback_set_VolatileRegionMaxSizeInKiB,
                     vtable::property_::emits_change),
    vtable::property("PmRegionMaxSizeInKiB",
                     details::PersistentMemory::_property_PmRegionMaxSizeInKiB
                        .data(),
                     _callback_get_PmRegionMaxSizeInKiB,
                     _callback_set_PmRegionMaxSizeInKiB,
                     vtable::property_::emits_change),
    vtable::property("AllocationIncrementInKiB",
                     details::PersistentMemory::_property_AllocationIncrementInKiB
                        .data(),
                     _callback_get_AllocationIncrementInKiB,
                     _callback_set_AllocationIncrementInKiB,
                     vtable::property_::emits_change),
    vtable::property("AllocationAlignmentInKiB",
                     details::PersistentMemory::_property_AllocationAlignmentInKiB
                        .data(),
                     _callback_get_AllocationAlignmentInKiB,
                     _callback_set_AllocationAlignmentInKiB,
                     vtable::property_::emits_change),
    vtable::property("VolatileRegionNumberLimit",
                     details::PersistentMemory::_property_VolatileRegionNumberLimit
                        .data(),
                     _callback_get_VolatileRegionNumberLimit,
                     _callback_set_VolatileRegionNumberLimit,
                     vtable::property_::emits_change),
    vtable::property("PmRegionNumberLimit",
                     details::PersistentMemory::_property_PmRegionNumberLimit
                        .data(),
                     _callback_get_PmRegionNumberLimit,
                     _callback_set_PmRegionNumberLimit,
                     vtable::property_::emits_change),
    vtable::property("SpareDeviceCount",
                     details::PersistentMemory::_property_SpareDeviceCount
                        .data(),
                     _callback_get_SpareDeviceCount,
                     _callback_set_SpareDeviceCount,
                     vtable::property_::emits_change),
    vtable::property("IsSpareDeviceInUse",
                     details::PersistentMemory::_property_IsSpareDeviceInUse
                        .data(),
                     _callback_get_IsSpareDeviceInUse,
                     _callback_set_IsSpareDeviceInUse,
                     vtable::property_::emits_change),
    vtable::property("IsRankSpareEnabled",
                     details::PersistentMemory::_property_IsRankSpareEnabled
                        .data(),
                     _callback_get_IsRankSpareEnabled,
                     _callback_set_IsRankSpareEnabled,
                     vtable::property_::emits_change),
    vtable::property("MaxAveragePowerLimitmW",
                     details::PersistentMemory::_property_MaxAveragePowerLimitmW
                        .data(),
                     _callback_get_MaxAveragePowerLimitmW,
                     _callback_set_MaxAveragePowerLimitmW,
                     vtable::property_::emits_change),
    vtable::property("CurrentSecurityState",
                     details::PersistentMemory::_property_CurrentSecurityState
                        .data(),
                     _callback_get_CurrentSecurityState,
                     _callback_set_CurrentSecurityState,
                     vtable::property_::emits_change),
    vtable::property("ConfigurationLocked",
                     details::PersistentMemory::_property_ConfigurationLocked
                        .data(),
                     _callback_get_ConfigurationLocked,
                     _callback_set_ConfigurationLocked,
                     vtable::property_::emits_change),
    vtable::property("AllowedMemoryModes",
                     details::PersistentMemory::_property_AllowedMemoryModes
                        .data(),
                     _callback_get_AllowedMemoryModes,
                     _callback_set_AllowedMemoryModes,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

