#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VCEN/server.hpp>








namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VCEN::VCEN(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VCEN_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VCEN::VCEN(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VCEN(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VCEN::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VCEN::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VCEN::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VCEN::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VCEN::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCEN::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VCEN::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VCEN::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VCEN::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VCEN::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCEN::se() const ->
        std::vector<uint8_t>
{
    return _se;
}

int VCEN::_callback_get_SE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->se();
                    }
                ));
    }
}

auto VCEN::se(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_se != value)
    {
        _se = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("SE");
        }
    }

    return _se;
}

auto VCEN::se(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return se(val, false);
}

int VCEN::_callback_set_SE(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->se(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_SE =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCEN::tm() const ->
        std::vector<uint8_t>
{
    return _tm;
}

int VCEN::_callback_get_TM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tm();
                    }
                ));
    }
}

auto VCEN::tm(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tm != value)
    {
        _tm = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("TM");
        }
    }

    return _tm;
}

auto VCEN::tm(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tm(val, false);
}

int VCEN::_callback_set_TM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tm(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_TM =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCEN::fc() const ->
        std::vector<uint8_t>
{
    return _fc;
}

int VCEN::_callback_get_FC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fc();
                    }
                ));
    }
}

auto VCEN::fc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_fc != value)
    {
        _fc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("FC");
        }
    }

    return _fc;
}

auto VCEN::fc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return fc(val, false);
}

int VCEN::_callback_set_FC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->fc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_FC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCEN::rg() const ->
        std::vector<uint8_t>
{
    return _rg;
}

int VCEN::_callback_get_RG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rg();
                    }
                ));
    }
}

auto VCEN::rg(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rg != value)
    {
        _rg = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("RG");
        }
    }

    return _rg;
}

auto VCEN::rg(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rg(val, false);
}

int VCEN::_callback_set_RG(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rg(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_RG =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VCEN::rb() const ->
        std::vector<uint8_t>
{
    return _rb;
}

int VCEN::_callback_get_RB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rb();
                    }
                ));
    }
}

auto VCEN::rb(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rb != value)
    {
        _rb = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VCEN_interface.property_changed("RB");
        }
    }

    return _rb;
}

auto VCEN::rb(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rb(val, false);
}

int VCEN::_callback_set_RB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VCEN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rb(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VCEN
{
static const auto _property_RB =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VCEN::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "SE")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        se(v, skipSignal);
        return;
    }
    if (_name == "TM")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tm(v, skipSignal);
        return;
    }
    if (_name == "FC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        fc(v, skipSignal);
        return;
    }
    if (_name == "RG")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rg(v, skipSignal);
        return;
    }
    if (_name == "RB")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rb(v, skipSignal);
        return;
    }
}

auto VCEN::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "SE")
    {
        return se();
    }
    if (_name == "TM")
    {
        return tm();
    }
    if (_name == "FC")
    {
        return fc();
    }
    if (_name == "RG")
    {
        return rg();
    }
    if (_name == "RB")
    {
        return rb();
    }

    return PropertiesVariant();
}


const vtable_t VCEN::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VCEN::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VCEN::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("SE",
                     details::VCEN::_property_SE
                        .data(),
                     _callback_get_SE,
                     _callback_set_SE,
                     vtable::property_::emits_change),
    vtable::property("TM",
                     details::VCEN::_property_TM
                        .data(),
                     _callback_get_TM,
                     _callback_set_TM,
                     vtable::property_::emits_change),
    vtable::property("FC",
                     details::VCEN::_property_FC
                        .data(),
                     _callback_get_FC,
                     _callback_set_FC,
                     vtable::property_::emits_change),
    vtable::property("RG",
                     details::VCEN::_property_RG
                        .data(),
                     _callback_get_RG,
                     _callback_set_RG,
                     vtable::property_::emits_change),
    vtable::property("RB",
                     details::VCEN::_property_RB
                        .data(),
                     _callback_get_RB,
                     _callback_set_RB,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

