#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace server
{

class PowerManagementPolicy
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PowerManagementPolicy() = delete;
        PowerManagementPolicy(const PowerManagementPolicy&) = delete;
        PowerManagementPolicy& operator=(const PowerManagementPolicy&) = delete;
        PowerManagementPolicy(PowerManagementPolicy&&) = delete;
        PowerManagementPolicy& operator=(PowerManagementPolicy&&) = delete;
        virtual ~PowerManagementPolicy() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PowerManagementPolicy(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PowerManagementPolicy(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of PolicyEnabled */
        virtual bool policyEnabled() const;
        /** Set value of PolicyEnabled with option to skip sending signal */
        virtual bool policyEnabled(bool value,
               bool skipSignal);
        /** Set value of PolicyEnabled */
        virtual bool policyEnabled(bool value);
        /** Get value of MaxTDPmW */
        virtual uint32_t maxTDPmW() const;
        /** Set value of MaxTDPmW with option to skip sending signal */
        virtual uint32_t maxTDPmW(uint32_t value,
               bool skipSignal);
        /** Set value of MaxTDPmW */
        virtual uint32_t maxTDPmW(uint32_t value);
        /** Get value of PeakPowerBudgetmW */
        virtual uint32_t peakPowerBudgetmW() const;
        /** Set value of PeakPowerBudgetmW with option to skip sending signal */
        virtual uint32_t peakPowerBudgetmW(uint32_t value,
               bool skipSignal);
        /** Set value of PeakPowerBudgetmW */
        virtual uint32_t peakPowerBudgetmW(uint32_t value);
        /** Get value of AveragePowerBudgetmW */
        virtual uint32_t averagePowerBudgetmW() const;
        /** Set value of AveragePowerBudgetmW with option to skip sending signal */
        virtual uint32_t averagePowerBudgetmW(uint32_t value,
               bool skipSignal);
        /** Set value of AveragePowerBudgetmW */
        virtual uint32_t averagePowerBudgetmW(uint32_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PersistentMemory.PowerManagementPolicy";

    private:

        /** @brief sd-bus callback for get-property 'PolicyEnabled' */
        static int _callback_get_PolicyEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PolicyEnabled' */
        static int _callback_set_PolicyEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxTDPmW' */
        static int _callback_get_MaxTDPmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxTDPmW' */
        static int _callback_set_MaxTDPmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PeakPowerBudgetmW' */
        static int _callback_get_PeakPowerBudgetmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PeakPowerBudgetmW' */
        static int _callback_set_PeakPowerBudgetmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AveragePowerBudgetmW' */
        static int _callback_get_AveragePowerBudgetmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AveragePowerBudgetmW' */
        static int _callback_set_AveragePowerBudgetmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface;
        sdbusplus::SdBusInterface *_intf;

        bool _policyEnabled{};
        uint32_t _maxTDPmW{};
        uint32_t _peakPowerBudgetmW{};
        uint32_t _averagePowerBudgetmW{};

};


} // namespace server
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

