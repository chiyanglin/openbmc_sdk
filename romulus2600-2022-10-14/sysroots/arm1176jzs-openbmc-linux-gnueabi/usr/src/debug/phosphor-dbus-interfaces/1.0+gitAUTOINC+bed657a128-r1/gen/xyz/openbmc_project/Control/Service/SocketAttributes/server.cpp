#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Service/SocketAttributes/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Service
{
namespace server
{

SocketAttributes::SocketAttributes(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Service_SocketAttributes_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SocketAttributes::SocketAttributes(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SocketAttributes(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SocketAttributes::port() const ->
        uint16_t
{
    return _port;
}

int SocketAttributes::_callback_get_Port(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SocketAttributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->port();
                    }
                ));
    }
}

auto SocketAttributes::port(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_port != value)
    {
        _port = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Service_SocketAttributes_interface.property_changed("Port");
        }
    }

    return _port;
}

auto SocketAttributes::port(uint16_t val) ->
        uint16_t
{
    return port(val, false);
}

int SocketAttributes::_callback_set_Port(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SocketAttributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->port(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SocketAttributes
{
static const auto _property_Port =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

void SocketAttributes::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Port")
    {
        auto& v = std::get<uint16_t>(val);
        port(v, skipSignal);
        return;
    }
}

auto SocketAttributes::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Port")
    {
        return port();
    }

    return PropertiesVariant();
}


const vtable_t SocketAttributes::_vtable[] = {
    vtable::start(),
    vtable::property("Port",
                     details::SocketAttributes::_property_Port
                        .data(),
                     _callback_get_Port,
                     _callback_set_Port,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Service
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

