#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Volume/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Volume::Volume(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Volume_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Volume::Volume(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Volume(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Volume::_callback_FormatLuks(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& password, FilesystemType&& type)
                    {
                        return o->formatLuks(
                                password, type);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::UnsupportedRequest& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Volume
{
static const auto _param_FormatLuks =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>, sdbusplus::xyz::openbmc_project::Inventory::Item::server::Volume::FilesystemType>());
static const auto _return_FormatLuks =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Volume::_callback_Erase(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](EraseMethod&& eraseType)
                    {
                        return o->erase(
                                eraseType);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Volume
{
static const auto _param_Erase =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::xyz::openbmc_project::Inventory::Item::server::Volume::EraseMethod>());
static const auto _return_Erase =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Volume::_callback_Lock(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lock(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::UnsupportedRequest& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Volume
{
static const auto _param_Lock =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Lock =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Volume::_callback_Unlock(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& password)
                    {
                        return o->unlock(
                                password);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Volume
{
static const auto _param_Unlock =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
static const auto _return_Unlock =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Volume::_callback_ChangePassword(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& oldPassword, std::vector<uint8_t>&& newPassword)
                    {
                        return o->changePassword(
                                oldPassword, newPassword);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Volume
{
static const auto _param_ChangePassword =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>, std::vector<uint8_t>>());
static const auto _return_ChangePassword =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}



auto Volume::locked() const ->
        bool
{
    return _locked;
}

int Volume::_callback_get_Locked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->locked();
                    }
                ));
    }
}

auto Volume::locked(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_locked != value)
    {
        _locked = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Volume_interface.property_changed("Locked");
        }
    }

    return _locked;
}

auto Volume::locked(bool val) ->
        bool
{
    return locked(val, false);
}

int Volume::_callback_set_Locked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Volume*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->locked(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Volume
{
static const auto _property_Locked =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Volume::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Locked")
    {
        auto& v = std::get<bool>(val);
        locked(v, skipSignal);
        return;
    }
}

auto Volume::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Locked")
    {
        return locked();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Volume::EraseMethod */
static const std::tuple<const char*, Volume::EraseMethod> mappingVolumeEraseMethod[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.CryptoErase",                 Volume::EraseMethod::CryptoErase ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.VerifyGeometry",                 Volume::EraseMethod::VerifyGeometry ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.LogicalOverWrite",                 Volume::EraseMethod::LogicalOverWrite ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.LogicalVerify",                 Volume::EraseMethod::LogicalVerify ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.VendorSanitize",                 Volume::EraseMethod::VendorSanitize ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.ZeroOverWrite",                 Volume::EraseMethod::ZeroOverWrite ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.ZeroVerify",                 Volume::EraseMethod::ZeroVerify ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.EraseMethod.SecuredLocked",                 Volume::EraseMethod::SecuredLocked ),
        };

} // anonymous namespace

auto Volume::convertStringToEraseMethod(const std::string& s) noexcept ->
        std::optional<EraseMethod>
{
    auto i = std::find_if(
            std::begin(mappingVolumeEraseMethod),
            std::end(mappingVolumeEraseMethod),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingVolumeEraseMethod) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Volume::convertEraseMethodFromString(const std::string& s) ->
        EraseMethod
{
    auto r = convertStringToEraseMethod(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Volume::convertEraseMethodToString(Volume::EraseMethod v)
{
    auto i = std::find_if(
            std::begin(mappingVolumeEraseMethod),
            std::end(mappingVolumeEraseMethod),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingVolumeEraseMethod))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Volume::FilesystemType */
static const std::tuple<const char*, Volume::FilesystemType> mappingVolumeFilesystemType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.FilesystemType.ext2",                 Volume::FilesystemType::ext2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.FilesystemType.ext3",                 Volume::FilesystemType::ext3 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.FilesystemType.ext4",                 Volume::FilesystemType::ext4 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Volume.FilesystemType.vfat",                 Volume::FilesystemType::vfat ),
        };

} // anonymous namespace

auto Volume::convertStringToFilesystemType(const std::string& s) noexcept ->
        std::optional<FilesystemType>
{
    auto i = std::find_if(
            std::begin(mappingVolumeFilesystemType),
            std::end(mappingVolumeFilesystemType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingVolumeFilesystemType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Volume::convertFilesystemTypeFromString(const std::string& s) ->
        FilesystemType
{
    auto r = convertStringToFilesystemType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Volume::convertFilesystemTypeToString(Volume::FilesystemType v)
{
    auto i = std::find_if(
            std::begin(mappingVolumeFilesystemType),
            std::end(mappingVolumeFilesystemType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingVolumeFilesystemType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Volume::_vtable[] = {
    vtable::start(),

    vtable::method("FormatLuks",
                   details::Volume::_param_FormatLuks
                        .data(),
                   details::Volume::_return_FormatLuks
                        .data(),
                   _callback_FormatLuks),

    vtable::method("Erase",
                   details::Volume::_param_Erase
                        .data(),
                   details::Volume::_return_Erase
                        .data(),
                   _callback_Erase),

    vtable::method("Lock",
                   details::Volume::_param_Lock
                        .data(),
                   details::Volume::_return_Lock
                        .data(),
                   _callback_Lock),

    vtable::method("Unlock",
                   details::Volume::_param_Unlock
                        .data(),
                   details::Volume::_return_Unlock
                        .data(),
                   _callback_Unlock),

    vtable::method("ChangePassword",
                   details::Volume::_param_ChangePassword
                        .data(),
                   details::Volume::_return_ChangePassword
                        .data(),
                   _callback_ChangePassword),
    vtable::property("Locked",
                     details::Volume::_property_Locked
                        .data(),
                     _callback_get_Locked,
                     _callback_set_Locked,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

