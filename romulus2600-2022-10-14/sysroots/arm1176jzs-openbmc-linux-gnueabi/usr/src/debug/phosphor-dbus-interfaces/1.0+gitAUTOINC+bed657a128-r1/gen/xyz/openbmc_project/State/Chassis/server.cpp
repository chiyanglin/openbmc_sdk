#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Chassis/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

Chassis::Chassis(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Chassis_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Chassis::Chassis(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Chassis(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Chassis::requestedPowerTransition() const ->
        Transition
{
    return _requestedPowerTransition;
}

int Chassis::_callback_get_RequestedPowerTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->requestedPowerTransition();
                    }
                ));
    }
}

auto Chassis::requestedPowerTransition(Transition value,
                                         bool skipSignal) ->
        Transition
{
    if (_requestedPowerTransition != value)
    {
        _requestedPowerTransition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Chassis_interface.property_changed("RequestedPowerTransition");
        }
    }

    return _requestedPowerTransition;
}

auto Chassis::requestedPowerTransition(Transition val) ->
        Transition
{
    return requestedPowerTransition(val, false);
}

int Chassis::_callback_set_RequestedPowerTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Transition&& arg)
                    {
                        o->requestedPowerTransition(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Chassis
{
static const auto _property_RequestedPowerTransition =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Chassis::Transition>());
}
}

auto Chassis::currentPowerState() const ->
        PowerState
{
    return _currentPowerState;
}

int Chassis::_callback_get_CurrentPowerState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentPowerState();
                    }
                ));
    }
}

auto Chassis::currentPowerState(PowerState value,
                                         bool skipSignal) ->
        PowerState
{
    if (_currentPowerState != value)
    {
        _currentPowerState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Chassis_interface.property_changed("CurrentPowerState");
        }
    }

    return _currentPowerState;
}

auto Chassis::currentPowerState(PowerState val) ->
        PowerState
{
    return currentPowerState(val, false);
}

int Chassis::_callback_set_CurrentPowerState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](PowerState&& arg)
                    {
                        o->currentPowerState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Chassis
{
static const auto _property_CurrentPowerState =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Chassis::PowerState>());
}
}

auto Chassis::currentPowerStatus() const ->
        PowerStatus
{
    return _currentPowerStatus;
}

int Chassis::_callback_get_CurrentPowerStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentPowerStatus();
                    }
                ));
    }
}

auto Chassis::currentPowerStatus(PowerStatus value,
                                         bool skipSignal) ->
        PowerStatus
{
    if (_currentPowerStatus != value)
    {
        _currentPowerStatus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Chassis_interface.property_changed("CurrentPowerStatus");
        }
    }

    return _currentPowerStatus;
}

auto Chassis::currentPowerStatus(PowerStatus val) ->
        PowerStatus
{
    return currentPowerStatus(val, false);
}

int Chassis::_callback_set_CurrentPowerStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](PowerStatus&& arg)
                    {
                        o->currentPowerStatus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Chassis
{
static const auto _property_CurrentPowerStatus =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Chassis::PowerStatus>());
}
}

auto Chassis::lastStateChangeTime() const ->
        uint64_t
{
    return _lastStateChangeTime;
}

int Chassis::_callback_get_LastStateChangeTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lastStateChangeTime();
                    }
                ));
    }
}

auto Chassis::lastStateChangeTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_lastStateChangeTime != value)
    {
        _lastStateChangeTime = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Chassis_interface.property_changed("LastStateChangeTime");
        }
    }

    return _lastStateChangeTime;
}

auto Chassis::lastStateChangeTime(uint64_t val) ->
        uint64_t
{
    return lastStateChangeTime(val, false);
}

int Chassis::_callback_set_LastStateChangeTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Chassis*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->lastStateChangeTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Chassis
{
static const auto _property_LastStateChangeTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Chassis::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RequestedPowerTransition")
    {
        auto& v = std::get<Transition>(val);
        requestedPowerTransition(v, skipSignal);
        return;
    }
    if (_name == "CurrentPowerState")
    {
        auto& v = std::get<PowerState>(val);
        currentPowerState(v, skipSignal);
        return;
    }
    if (_name == "CurrentPowerStatus")
    {
        auto& v = std::get<PowerStatus>(val);
        currentPowerStatus(v, skipSignal);
        return;
    }
    if (_name == "LastStateChangeTime")
    {
        auto& v = std::get<uint64_t>(val);
        lastStateChangeTime(v, skipSignal);
        return;
    }
}

auto Chassis::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RequestedPowerTransition")
    {
        return requestedPowerTransition();
    }
    if (_name == "CurrentPowerState")
    {
        return currentPowerState();
    }
    if (_name == "CurrentPowerStatus")
    {
        return currentPowerStatus();
    }
    if (_name == "LastStateChangeTime")
    {
        return lastStateChangeTime();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Chassis::Transition */
static const std::tuple<const char*, Chassis::Transition> mappingChassisTransition[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Chassis.Transition.Off",                 Chassis::Transition::Off ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.Transition.On",                 Chassis::Transition::On ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.Transition.PowerCycle",                 Chassis::Transition::PowerCycle ),
        };

} // anonymous namespace

auto Chassis::convertStringToTransition(const std::string& s) noexcept ->
        std::optional<Transition>
{
    auto i = std::find_if(
            std::begin(mappingChassisTransition),
            std::end(mappingChassisTransition),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingChassisTransition) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Chassis::convertTransitionFromString(const std::string& s) ->
        Transition
{
    auto r = convertStringToTransition(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Chassis::convertTransitionToString(Chassis::Transition v)
{
    auto i = std::find_if(
            std::begin(mappingChassisTransition),
            std::end(mappingChassisTransition),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingChassisTransition))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Chassis::PowerState */
static const std::tuple<const char*, Chassis::PowerState> mappingChassisPowerState[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerState.Off",                 Chassis::PowerState::Off ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerState.TransitioningToOff",                 Chassis::PowerState::TransitioningToOff ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerState.On",                 Chassis::PowerState::On ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerState.TransitioningToOn",                 Chassis::PowerState::TransitioningToOn ),
        };

} // anonymous namespace

auto Chassis::convertStringToPowerState(const std::string& s) noexcept ->
        std::optional<PowerState>
{
    auto i = std::find_if(
            std::begin(mappingChassisPowerState),
            std::end(mappingChassisPowerState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingChassisPowerState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Chassis::convertPowerStateFromString(const std::string& s) ->
        PowerState
{
    auto r = convertStringToPowerState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Chassis::convertPowerStateToString(Chassis::PowerState v)
{
    auto i = std::find_if(
            std::begin(mappingChassisPowerState),
            std::end(mappingChassisPowerState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingChassisPowerState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Chassis::PowerStatus */
static const std::tuple<const char*, Chassis::PowerStatus> mappingChassisPowerStatus[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerStatus.Undefined",                 Chassis::PowerStatus::Undefined ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerStatus.BrownOut",                 Chassis::PowerStatus::BrownOut ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerStatus.UninterruptiblePowerSupply",                 Chassis::PowerStatus::UninterruptiblePowerSupply ),
            std::make_tuple( "xyz.openbmc_project.State.Chassis.PowerStatus.Good",                 Chassis::PowerStatus::Good ),
        };

} // anonymous namespace

auto Chassis::convertStringToPowerStatus(const std::string& s) noexcept ->
        std::optional<PowerStatus>
{
    auto i = std::find_if(
            std::begin(mappingChassisPowerStatus),
            std::end(mappingChassisPowerStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingChassisPowerStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Chassis::convertPowerStatusFromString(const std::string& s) ->
        PowerStatus
{
    auto r = convertStringToPowerStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Chassis::convertPowerStatusToString(Chassis::PowerStatus v)
{
    auto i = std::find_if(
            std::begin(mappingChassisPowerStatus),
            std::end(mappingChassisPowerStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingChassisPowerStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Chassis::_vtable[] = {
    vtable::start(),
    vtable::property("RequestedPowerTransition",
                     details::Chassis::_property_RequestedPowerTransition
                        .data(),
                     _callback_get_RequestedPowerTransition,
                     _callback_set_RequestedPowerTransition,
                     vtable::property_::emits_change),
    vtable::property("CurrentPowerState",
                     details::Chassis::_property_CurrentPowerState
                        .data(),
                     _callback_get_CurrentPowerState,
                     _callback_set_CurrentPowerState,
                     vtable::property_::emits_change),
    vtable::property("CurrentPowerStatus",
                     details::Chassis::_property_CurrentPowerStatus
                        .data(),
                     _callback_get_CurrentPowerStatus,
                     _callback_set_CurrentPowerStatus,
                     vtable::property_::emits_change),
    vtable::property("LastStateChangeTime",
                     details::Chassis::_property_LastStateChangeTime
                        .data(),
                     _callback_get_LastStateChangeTime,
                     _callback_set_LastStateChangeTime,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

