#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/Ldap/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Ldap
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_Ldap_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_CreateConfig(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& ldapServerURI, std::string&& ldapBindDN, std::string&& ldapBaseDN, std::string&& ldapbinddNpassword, SearchScope&& ldapSearchScope, Type&& ldapType, std::string&& groupNameAttribute, std::string&& usernameAttribute)
                    {
                        return o->createConfig(
                                ldapServerURI, ldapBindDN, ldapBaseDN, ldapbinddNpassword, ldapSearchScope, ldapType, groupNameAttribute, usernameAttribute);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NoCACertificate& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_CreateConfig =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string, std::string, std::string, sdbusplus::xyz::openbmc_project::User::Ldap::server::Create::SearchScope, sdbusplus::xyz::openbmc_project::User::Ldap::server::Create::Type, std::string, std::string>());
static const auto _return_CreateConfig =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
}
}




namespace
{
/** String to enum mapping for Create::SearchScope */
static const std::tuple<const char*, Create::SearchScope> mappingCreateSearchScope[] =
        {
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Create.SearchScope.sub",                 Create::SearchScope::sub ),
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Create.SearchScope.one",                 Create::SearchScope::one ),
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Create.SearchScope.base",                 Create::SearchScope::base ),
        };

} // anonymous namespace

auto Create::convertStringToSearchScope(const std::string& s) noexcept ->
        std::optional<SearchScope>
{
    auto i = std::find_if(
            std::begin(mappingCreateSearchScope),
            std::end(mappingCreateSearchScope),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCreateSearchScope) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Create::convertSearchScopeFromString(const std::string& s) ->
        SearchScope
{
    auto r = convertStringToSearchScope(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Create::convertSearchScopeToString(Create::SearchScope v)
{
    auto i = std::find_if(
            std::begin(mappingCreateSearchScope),
            std::end(mappingCreateSearchScope),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCreateSearchScope))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Create::Type */
static const std::tuple<const char*, Create::Type> mappingCreateType[] =
        {
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Create.Type.ActiveDirectory",                 Create::Type::ActiveDirectory ),
            std::make_tuple( "xyz.openbmc_project.User.Ldap.Create.Type.OpenLdap",                 Create::Type::OpenLdap ),
        };

} // anonymous namespace

auto Create::convertStringToType(const std::string& s) noexcept ->
        std::optional<Type>
{
    auto i = std::find_if(
            std::begin(mappingCreateType),
            std::end(mappingCreateType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCreateType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Create::convertTypeFromString(const std::string& s) ->
        Type
{
    auto r = convertStringToType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Create::convertTypeToString(Create::Type v)
{
    auto i = std::find_if(
            std::begin(mappingCreateType),
            std::end(mappingCreateType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCreateType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("CreateConfig",
                   details::Create::_param_CreateConfig
                        .data(),
                   details::Create::_return_CreateConfig
                        .data(),
                   _callback_CreateConfig),
    vtable::end()
};

} // namespace server
} // namespace Ldap
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

