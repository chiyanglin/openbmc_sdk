#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Cacheable/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Cacheable::Cacheable(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Cacheable_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Cacheable::Cacheable(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Cacheable(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Cacheable::cached() const ->
        bool
{
    return _cached;
}

int Cacheable::_callback_get_Cached(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cacheable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->cached();
                    }
                ));
    }
}

auto Cacheable::cached(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_cached != value)
    {
        _cached = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Cacheable_interface.property_changed("Cached");
        }
    }

    return _cached;
}

auto Cacheable::cached(bool val) ->
        bool
{
    return cached(val, false);
}

int Cacheable::_callback_set_Cached(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cacheable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->cached(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cacheable
{
static const auto _property_Cached =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Cacheable::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Cached")
    {
        auto& v = std::get<bool>(val);
        cached(v, skipSignal);
        return;
    }
}

auto Cacheable::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Cached")
    {
        return cached();
    }

    return PropertiesVariant();
}


const vtable_t Cacheable::_vtable[] = {
    vtable::start(),
    vtable::property("Cached",
                     details::Cacheable::_property_Cached
                        .data(),
                     _callback_get_Cached,
                     _callback_set_Cached,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

