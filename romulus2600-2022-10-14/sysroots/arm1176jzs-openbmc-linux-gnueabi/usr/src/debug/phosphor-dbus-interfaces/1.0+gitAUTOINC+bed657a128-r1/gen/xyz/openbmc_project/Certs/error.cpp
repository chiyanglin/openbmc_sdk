#include <xyz/openbmc_project/Certs/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace Error
{
const char* InvalidCertificate::name() const noexcept
{
    return errName;
}
const char* InvalidCertificate::description() const noexcept
{
    return errDesc;
}
const char* InvalidCertificate::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

