#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace intel
{
namespace Protocol
{
namespace PECI
{
namespace server
{

class Raw
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Raw() = delete;
        Raw(const Raw&) = delete;
        Raw& operator=(const Raw&) = delete;
        Raw(Raw&&) = delete;
        Raw& operator=(Raw&&) = delete;
        virtual ~Raw() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Raw(bus_t& bus, const char* path);



        /** @brief Implementation for Send
         *  Send raw PECI command(s) to the CPU
         *
         *  @param[in] device - The path for the PECI device to use for the command
         *  @param[in] commands - An array of byte arrays where each byte array holds the raw bytes for a single PECI command.  The larger array allows sending multiple PECI commands in a single transaction.
         *
         *  @return results[std::vector<std::vector<uint8_t>>] - An array of byte arrays where each byte array holds the raw bytes for a single PECI response corresponding to the command in the command array.
         */
        virtual std::vector<std::vector<uint8_t>> send(
            std::string device,
            std::vector<std::vector<uint8_t>> commands) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _com_intel_Protocol_PECI_Raw_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_intel_Protocol_PECI_Raw_interface.emit_removed();
        }

        static constexpr auto interface = "com.intel.Protocol.PECI.Raw";

    private:

        /** @brief sd-bus callback for Send
         */
        static int _callback_Send(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_intel_Protocol_PECI_Raw_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace PECI
} // namespace Protocol
} // namespace intel
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

