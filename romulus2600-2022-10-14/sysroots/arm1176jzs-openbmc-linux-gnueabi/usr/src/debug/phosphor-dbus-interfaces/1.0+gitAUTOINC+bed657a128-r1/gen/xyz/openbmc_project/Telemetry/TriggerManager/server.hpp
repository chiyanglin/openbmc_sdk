#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>

#include <xyz/openbmc_project/Telemetry/Trigger/server.hpp>

#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

class TriggerManager
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        TriggerManager() = delete;
        TriggerManager(const TriggerManager&) = delete;
        TriggerManager& operator=(const TriggerManager&) = delete;
        TriggerManager(TriggerManager&&) = delete;
        TriggerManager& operator=(TriggerManager&&) = delete;
        virtual ~TriggerManager() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        TriggerManager(bus_t& bus, const char* path);



        /** @brief Implementation for AddTrigger
         *  Create new object that represents Trigger with xyz.openbmc_project.Telemetry.Trigger interface stored in path /xyz/openbmc_project/Telemetry/Triggers/{id} where id is parameter of this method.
         *
         *  @param[in] id - Defines unique identifier of created Trigger object to be exposed over D-Bus. Acceptable formats are: "{Id}", "{Prefix}/{SubId}", "{Prefix}/". If the last variant is used, service will generate unique SubId value by itself.
         *  @param[in] name - Defines user friendly name of created Trigger object.
         *  @param[in] triggerActions - Defines which actions are taken when threshold conditions are met.
         *  @param[in] sensors - Map of sensors that is monitored within trigger. D-Bus sensor path is used as map's key. Its value is a metadata that is used to store user data about sensor. In Redfish, metadata will be set to endpoint corresponding to that sensor.
         *  @param[in] reports - Collection of report names that are updated when threshold conditions are met. This parameter is ignored if triggerActions parameter does not contain 'UpdateReport' action.
         *  @param[in] thresholds - Contains array of numeric or discrete thresholds that are described in xyz.openbmc_project.Telemetry.Trigger interface.
         *
         *  @return triggerPath[sdbusplus::message::object_path] - Path to new trigger -> /xyz/openbmc_project/Telemetry/Triggers/{id}.
         */
        virtual sdbusplus::message::object_path addTrigger(
            std::string id,
            std::string name,
            std::vector<xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction> triggerActions,
            std::vector<std::map<sdbusplus::message::object_path, std::string>> sensors,
            std::vector<sdbusplus::message::object_path> reports,
            std::variant<std::vector<std::tuple<xyz::openbmc_project::Telemetry::server::Trigger::Type, uint64_t, xyz::openbmc_project::Telemetry::server::Trigger::Direction, double>>, std::vector<std::tuple<std::string, xyz::openbmc_project::Telemetry::server::Trigger::Severity, uint64_t, std::string>>> thresholds) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Telemetry_TriggerManager_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Telemetry_TriggerManager_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Telemetry.TriggerManager";

    private:

        /** @brief sd-bus callback for AddTrigger
         */
        static int _callback_AddTrigger(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Telemetry_TriggerManager_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

