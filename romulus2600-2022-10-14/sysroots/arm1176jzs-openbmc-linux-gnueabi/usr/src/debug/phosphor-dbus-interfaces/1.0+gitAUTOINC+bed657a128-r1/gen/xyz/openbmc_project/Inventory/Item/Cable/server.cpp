#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Cable/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Cable::Cable(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Cable_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Cable::Cable(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Cable(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Cable::length() const ->
        double
{
    return _length;
}

int Cable::_callback_get_Length(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->length();
                    }
                ));
    }
}

auto Cable::length(double value,
                                         bool skipSignal) ->
        double
{
    if (_length != value)
    {
        _length = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cable_interface.property_changed("Length");
        }
    }

    return _length;
}

auto Cable::length(double val) ->
        double
{
    return length(val, false);
}

int Cable::_callback_set_Length(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->length(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cable
{
static const auto _property_Length =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Cable::cableTypeDescription() const ->
        std::string
{
    return _cableTypeDescription;
}

int Cable::_callback_get_CableTypeDescription(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->cableTypeDescription();
                    }
                ));
    }
}

auto Cable::cableTypeDescription(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_cableTypeDescription != value)
    {
        _cableTypeDescription = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cable_interface.property_changed("CableTypeDescription");
        }
    }

    return _cableTypeDescription;
}

auto Cable::cableTypeDescription(std::string val) ->
        std::string
{
    return cableTypeDescription(val, false);
}

int Cable::_callback_set_CableTypeDescription(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Cable*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->cableTypeDescription(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Cable
{
static const auto _property_CableTypeDescription =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Cable::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Length")
    {
        auto& v = std::get<double>(val);
        length(v, skipSignal);
        return;
    }
    if (_name == "CableTypeDescription")
    {
        auto& v = std::get<std::string>(val);
        cableTypeDescription(v, skipSignal);
        return;
    }
}

auto Cable::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Length")
    {
        return length();
    }
    if (_name == "CableTypeDescription")
    {
        return cableTypeDescription();
    }

    return PropertiesVariant();
}


const vtable_t Cable::_vtable[] = {
    vtable::start(),
    vtable::property("Length",
                     details::Cable::_property_Length
                        .data(),
                     _callback_get_Length,
                     _callback_set_Length,
                     vtable::property_::emits_change),
    vtable::property("CableTypeDescription",
                     details::Cable::_property_CableTypeDescription
                        .data(),
                     _callback_get_CableTypeDescription,
                     _callback_set_CableTypeDescription,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

