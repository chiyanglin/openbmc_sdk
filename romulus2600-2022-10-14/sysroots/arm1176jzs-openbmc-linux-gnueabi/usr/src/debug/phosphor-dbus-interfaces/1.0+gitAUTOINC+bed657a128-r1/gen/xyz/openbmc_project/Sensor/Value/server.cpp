#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Value/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace server
{

Value::Value(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Value_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Value::Value(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Value(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Value::value() const ->
        double
{
    return _value;
}

int Value::_callback_get_Value(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->value();
                    }
                ));
    }
}

auto Value::value(double value,
                                         bool skipSignal) ->
        double
{
    if (_value != value)
    {
        _value = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Value_interface.property_changed("Value");
        }
    }

    return _value;
}

auto Value::value(double val) ->
        double
{
    return value(val, false);
}

int Value::_callback_set_Value(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->value(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Value
{
static const auto _property_Value =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Value::maxValue() const ->
        double
{
    return _maxValue;
}

int Value::_callback_get_MaxValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxValue();
                    }
                ));
    }
}

auto Value::maxValue(double value,
                                         bool skipSignal) ->
        double
{
    if (_maxValue != value)
    {
        _maxValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Value_interface.property_changed("MaxValue");
        }
    }

    return _maxValue;
}

auto Value::maxValue(double val) ->
        double
{
    return maxValue(val, false);
}

int Value::_callback_set_MaxValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->maxValue(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Value
{
static const auto _property_MaxValue =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Value::minValue() const ->
        double
{
    return _minValue;
}

int Value::_callback_get_MinValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minValue();
                    }
                ));
    }
}

auto Value::minValue(double value,
                                         bool skipSignal) ->
        double
{
    if (_minValue != value)
    {
        _minValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Value_interface.property_changed("MinValue");
        }
    }

    return _minValue;
}

auto Value::minValue(double val) ->
        double
{
    return minValue(val, false);
}

int Value::_callback_set_MinValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->minValue(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Value
{
static const auto _property_MinValue =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Value::unit() const ->
        Unit
{
    return _unit;
}

int Value::_callback_get_Unit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->unit();
                    }
                ));
    }
}

auto Value::unit(Unit value,
                                         bool skipSignal) ->
        Unit
{
    if (_unit != value)
    {
        _unit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Value_interface.property_changed("Unit");
        }
    }

    return _unit;
}

auto Value::unit(Unit val) ->
        Unit
{
    return unit(val, false);
}

int Value::_callback_set_Unit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Value*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Unit&& arg)
                    {
                        o->unit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Value
{
static const auto _property_Unit =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Sensor::server::Value::Unit>());
}
}

void Value::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Value")
    {
        auto& v = std::get<double>(val);
        value(v, skipSignal);
        return;
    }
    if (_name == "MaxValue")
    {
        auto& v = std::get<double>(val);
        maxValue(v, skipSignal);
        return;
    }
    if (_name == "MinValue")
    {
        auto& v = std::get<double>(val);
        minValue(v, skipSignal);
        return;
    }
    if (_name == "Unit")
    {
        auto& v = std::get<Unit>(val);
        unit(v, skipSignal);
        return;
    }
}

auto Value::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Value")
    {
        return value();
    }
    if (_name == "MaxValue")
    {
        return maxValue();
    }
    if (_name == "MinValue")
    {
        return minValue();
    }
    if (_name == "Unit")
    {
        return unit();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Value::Unit */
static const std::tuple<const char*, Value::Unit> mappingValueUnit[] =
        {
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Amperes",                 Value::Unit::Amperes ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.CFM",                 Value::Unit::CFM ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",                 Value::Unit::DegreesC ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Joules",                 Value::Unit::Joules ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Meters",                 Value::Unit::Meters ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Percent",                 Value::Unit::Percent ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.PercentRH",                 Value::Unit::PercentRH ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Pascals",                 Value::Unit::Pascals ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.RPMS",                 Value::Unit::RPMS ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Volts",                 Value::Unit::Volts ),
            std::make_tuple( "xyz.openbmc_project.Sensor.Value.Unit.Watts",                 Value::Unit::Watts ),
        };

} // anonymous namespace

auto Value::convertStringToUnit(const std::string& s) noexcept ->
        std::optional<Unit>
{
    auto i = std::find_if(
            std::begin(mappingValueUnit),
            std::end(mappingValueUnit),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingValueUnit) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Value::convertUnitFromString(const std::string& s) ->
        Unit
{
    auto r = convertStringToUnit(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Value::convertUnitToString(Value::Unit v)
{
    auto i = std::find_if(
            std::begin(mappingValueUnit),
            std::end(mappingValueUnit),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingValueUnit))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Value::_vtable[] = {
    vtable::start(),
    vtable::property("Value",
                     details::Value::_property_Value
                        .data(),
                     _callback_get_Value,
                     _callback_set_Value,
                     vtable::property_::emits_change),
    vtable::property("MaxValue",
                     details::Value::_property_MaxValue
                        .data(),
                     _callback_get_MaxValue,
                     _callback_set_MaxValue,
                     vtable::property_::emits_change),
    vtable::property("MinValue",
                     details::Value::_property_MinValue
                        .data(),
                     _callback_get_MinValue,
                     _callback_set_MinValue,
                     vtable::property_::emits_change),
    vtable::property("Unit",
                     details::Value::_property_Unit
                        .data(),
                     _callback_get_Unit,
                     _callback_set_Unit,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

