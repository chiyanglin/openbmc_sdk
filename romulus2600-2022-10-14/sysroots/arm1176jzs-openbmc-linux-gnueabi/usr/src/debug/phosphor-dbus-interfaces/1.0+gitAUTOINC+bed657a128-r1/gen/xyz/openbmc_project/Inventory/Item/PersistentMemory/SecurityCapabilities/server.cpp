#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/SecurityCapabilities/server.hpp>






namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace server
{

SecurityCapabilities::SecurityCapabilities(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SecurityCapabilities::SecurityCapabilities(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SecurityCapabilities(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SecurityCapabilities::maxPassphraseCount() const ->
        uint32_t
{
    return _maxPassphraseCount;
}

int SecurityCapabilities::_callback_get_MaxPassphraseCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxPassphraseCount();
                    }
                ));
    }
}

auto SecurityCapabilities::maxPassphraseCount(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_maxPassphraseCount != value)
    {
        _maxPassphraseCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.property_changed("MaxPassphraseCount");
        }
    }

    return _maxPassphraseCount;
}

auto SecurityCapabilities::maxPassphraseCount(uint32_t val) ->
        uint32_t
{
    return maxPassphraseCount(val, false);
}

int SecurityCapabilities::_callback_set_MaxPassphraseCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->maxPassphraseCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SecurityCapabilities
{
static const auto _property_MaxPassphraseCount =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto SecurityCapabilities::passphraseCapable() const ->
        bool
{
    return _passphraseCapable;
}

int SecurityCapabilities::_callback_get_PassphraseCapable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->passphraseCapable();
                    }
                ));
    }
}

auto SecurityCapabilities::passphraseCapable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_passphraseCapable != value)
    {
        _passphraseCapable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.property_changed("PassphraseCapable");
        }
    }

    return _passphraseCapable;
}

auto SecurityCapabilities::passphraseCapable(bool val) ->
        bool
{
    return passphraseCapable(val, false);
}

int SecurityCapabilities::_callback_set_PassphraseCapable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->passphraseCapable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SecurityCapabilities
{
static const auto _property_PassphraseCapable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SecurityCapabilities::configurationLockCapable() const ->
        bool
{
    return _configurationLockCapable;
}

int SecurityCapabilities::_callback_get_ConfigurationLockCapable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->configurationLockCapable();
                    }
                ));
    }
}

auto SecurityCapabilities::configurationLockCapable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_configurationLockCapable != value)
    {
        _configurationLockCapable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.property_changed("ConfigurationLockCapable");
        }
    }

    return _configurationLockCapable;
}

auto SecurityCapabilities::configurationLockCapable(bool val) ->
        bool
{
    return configurationLockCapable(val, false);
}

int SecurityCapabilities::_callback_set_ConfigurationLockCapable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->configurationLockCapable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SecurityCapabilities
{
static const auto _property_ConfigurationLockCapable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SecurityCapabilities::dataLockCapable() const ->
        bool
{
    return _dataLockCapable;
}

int SecurityCapabilities::_callback_get_DataLockCapable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dataLockCapable();
                    }
                ));
    }
}

auto SecurityCapabilities::dataLockCapable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_dataLockCapable != value)
    {
        _dataLockCapable = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.property_changed("DataLockCapable");
        }
    }

    return _dataLockCapable;
}

auto SecurityCapabilities::dataLockCapable(bool val) ->
        bool
{
    return dataLockCapable(val, false);
}

int SecurityCapabilities::_callback_set_DataLockCapable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->dataLockCapable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SecurityCapabilities
{
static const auto _property_DataLockCapable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto SecurityCapabilities::passphraseLockLimit() const ->
        uint32_t
{
    return _passphraseLockLimit;
}

int SecurityCapabilities::_callback_get_PassphraseLockLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->passphraseLockLimit();
                    }
                ));
    }
}

auto SecurityCapabilities::passphraseLockLimit(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_passphraseLockLimit != value)
    {
        _passphraseLockLimit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_SecurityCapabilities_interface.property_changed("PassphraseLockLimit");
        }
    }

    return _passphraseLockLimit;
}

auto SecurityCapabilities::passphraseLockLimit(uint32_t val) ->
        uint32_t
{
    return passphraseLockLimit(val, false);
}

int SecurityCapabilities::_callback_set_PassphraseLockLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SecurityCapabilities*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->passphraseLockLimit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SecurityCapabilities
{
static const auto _property_PassphraseLockLimit =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void SecurityCapabilities::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MaxPassphraseCount")
    {
        auto& v = std::get<uint32_t>(val);
        maxPassphraseCount(v, skipSignal);
        return;
    }
    if (_name == "PassphraseCapable")
    {
        auto& v = std::get<bool>(val);
        passphraseCapable(v, skipSignal);
        return;
    }
    if (_name == "ConfigurationLockCapable")
    {
        auto& v = std::get<bool>(val);
        configurationLockCapable(v, skipSignal);
        return;
    }
    if (_name == "DataLockCapable")
    {
        auto& v = std::get<bool>(val);
        dataLockCapable(v, skipSignal);
        return;
    }
    if (_name == "PassphraseLockLimit")
    {
        auto& v = std::get<uint32_t>(val);
        passphraseLockLimit(v, skipSignal);
        return;
    }
}

auto SecurityCapabilities::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MaxPassphraseCount")
    {
        return maxPassphraseCount();
    }
    if (_name == "PassphraseCapable")
    {
        return passphraseCapable();
    }
    if (_name == "ConfigurationLockCapable")
    {
        return configurationLockCapable();
    }
    if (_name == "DataLockCapable")
    {
        return dataLockCapable();
    }
    if (_name == "PassphraseLockLimit")
    {
        return passphraseLockLimit();
    }

    return PropertiesVariant();
}


const vtable_t SecurityCapabilities::_vtable[] = {
    vtable::start(),
    vtable::property("MaxPassphraseCount",
                     details::SecurityCapabilities::_property_MaxPassphraseCount
                        .data(),
                     _callback_get_MaxPassphraseCount,
                     _callback_set_MaxPassphraseCount,
                     vtable::property_::emits_change),
    vtable::property("PassphraseCapable",
                     details::SecurityCapabilities::_property_PassphraseCapable
                        .data(),
                     _callback_get_PassphraseCapable,
                     _callback_set_PassphraseCapable,
                     vtable::property_::emits_change),
    vtable::property("ConfigurationLockCapable",
                     details::SecurityCapabilities::_property_ConfigurationLockCapable
                        .data(),
                     _callback_get_ConfigurationLockCapable,
                     _callback_set_ConfigurationLockCapable,
                     vtable::property_::emits_change),
    vtable::property("DataLockCapable",
                     details::SecurityCapabilities::_property_DataLockCapable
                        .data(),
                     _callback_get_DataLockCapable,
                     _callback_set_DataLockCapable,
                     vtable::property_::emits_change),
    vtable::property("PassphraseLockLimit",
                     details::SecurityCapabilities::_property_PassphraseLockLimit
                        .data(),
                     _callback_get_PassphraseLockLimit,
                     _callback_set_PassphraseLockLimit,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

