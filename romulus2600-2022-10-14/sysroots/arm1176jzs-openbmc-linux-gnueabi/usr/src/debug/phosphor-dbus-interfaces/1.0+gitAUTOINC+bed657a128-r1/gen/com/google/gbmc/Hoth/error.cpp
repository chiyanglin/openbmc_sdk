#include <com/google/gbmc/Hoth/error.hpp>

namespace sdbusplus
{
namespace com
{
namespace google
{
namespace gbmc
{
namespace Hoth
{
namespace Error
{
const char* CommandFailure::name() const noexcept
{
    return errName;
}
const char* CommandFailure::description() const noexcept
{
    return errDesc;
}
const char* CommandFailure::what() const noexcept
{
    return errWhat;
}
const char* ResponseFailure::name() const noexcept
{
    return errName;
}
const char* ResponseFailure::description() const noexcept
{
    return errDesc;
}
const char* ResponseFailure::what() const noexcept
{
    return errWhat;
}
const char* FirmwareFailure::name() const noexcept
{
    return errName;
}
const char* FirmwareFailure::description() const noexcept
{
    return errDesc;
}
const char* FirmwareFailure::what() const noexcept
{
    return errWhat;
}
const char* ResponseNotFound::name() const noexcept
{
    return errName;
}
const char* ResponseNotFound::description() const noexcept
{
    return errDesc;
}
const char* ResponseNotFound::what() const noexcept
{
    return errWhat;
}
const char* InterfaceError::name() const noexcept
{
    return errName;
}
const char* InterfaceError::description() const noexcept
{
    return errDesc;
}
const char* InterfaceError::what() const noexcept
{
    return errWhat;
}
const char* ExpectedInfoNotFound::name() const noexcept
{
    return errName;
}
const char* ExpectedInfoNotFound::description() const noexcept
{
    return errDesc;
}
const char* ExpectedInfoNotFound::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Hoth
} // namespace gbmc
} // namespace google
} // namespace com
} // namespace sdbusplus

