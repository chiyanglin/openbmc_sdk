#include <xyz/openbmc_project/BIOSConfig/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace Common
{
namespace Error
{
const char* AttributeNotFound::name() const noexcept
{
    return errName;
}
const char* AttributeNotFound::description() const noexcept
{
    return errDesc;
}
const char* AttributeNotFound::what() const noexcept
{
    return errWhat;
}
const char* AttributeReadOnly::name() const noexcept
{
    return errName;
}
const char* AttributeReadOnly::description() const noexcept
{
    return errDesc;
}
const char* AttributeReadOnly::what() const noexcept
{
    return errWhat;
}
const char* InvalidCurrentPassword::name() const noexcept
{
    return errName;
}
const char* InvalidCurrentPassword::description() const noexcept
{
    return errDesc;
}
const char* InvalidCurrentPassword::what() const noexcept
{
    return errWhat;
}
const char* PasswordNotSettable::name() const noexcept
{
    return errName;
}
const char* PasswordNotSettable::description() const noexcept
{
    return errDesc;
}
const char* PasswordNotSettable::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Common
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

