#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Control/Power/server.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Control
{
namespace server
{

Power::Power(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Control_Power_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Power::Power(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Power(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Power::_callback_ForcePowerOff(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Power*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->forcePowerOff(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Power
{
static const auto _param_ForcePowerOff =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_ForcePowerOff =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}



auto Power::pGood() const ->
        bool
{
    return _pGood;
}

int Power::_callback_get_PGood(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Power*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pGood();
                    }
                ));
    }
}

auto Power::pGood(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_pGood != value)
    {
        _pGood = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Control_Power_interface.property_changed("PGood");
        }
    }

    return _pGood;
}

auto Power::pGood(bool val) ->
        bool
{
    return pGood(val, false);
}

int Power::_callback_set_PGood(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Power*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->pGood(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Power
{
static const auto _property_PGood =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Power::state() const ->
        int32_t
{
    return _state;
}

int Power::_callback_get_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Power*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->state();
                    }
                ));
    }
}

auto Power::state(int32_t value,
                                         bool skipSignal) ->
        int32_t
{
    if (_state != value)
    {
        _state = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Control_Power_interface.property_changed("State");
        }
    }

    return _state;
}

auto Power::state(int32_t val) ->
        int32_t
{
    return state(val, false);
}

int Power::_callback_set_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Power*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](int32_t&& arg)
                    {
                        o->state(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Power
{
static const auto _property_State =
    utility::tuple_to_array(message::types::type_id<
            int32_t>());
}
}

void Power::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PGood")
    {
        auto& v = std::get<bool>(val);
        pGood(v, skipSignal);
        return;
    }
    if (_name == "State")
    {
        auto& v = std::get<int32_t>(val);
        state(v, skipSignal);
        return;
    }
}

auto Power::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PGood")
    {
        return pGood();
    }
    if (_name == "State")
    {
        return state();
    }

    return PropertiesVariant();
}


const vtable_t Power::_vtable[] = {
    vtable::start(),

    vtable::method("forcePowerOff",
                   details::Power::_param_ForcePowerOff
                        .data(),
                   details::Power::_return_ForcePowerOff
                        .data(),
                   _callback_ForcePowerOff),
    vtable::property("PGood",
                     details::Power::_property_PGood
                        .data(),
                     _callback_get_PGood,
                     _callback_set_PGood,
                     vtable::property_::emits_change),
    vtable::property("State",
                     details::Power::_property_State
                        .data(),
                     _callback_get_State,
                     _callback_set_State,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

