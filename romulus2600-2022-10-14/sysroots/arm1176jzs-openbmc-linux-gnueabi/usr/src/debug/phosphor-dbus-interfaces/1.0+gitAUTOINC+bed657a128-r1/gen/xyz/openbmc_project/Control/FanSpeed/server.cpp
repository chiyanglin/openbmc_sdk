#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/FanSpeed/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

FanSpeed::FanSpeed(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_FanSpeed_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FanSpeed::FanSpeed(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FanSpeed(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FanSpeed::target() const ->
        uint64_t
{
    return _target;
}

int FanSpeed::_callback_get_Target(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanSpeed*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->target();
                    }
                ));
    }
}

auto FanSpeed::target(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_target != value)
    {
        _target = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_FanSpeed_interface.property_changed("Target");
        }
    }

    return _target;
}

auto FanSpeed::target(uint64_t val) ->
        uint64_t
{
    return target(val, false);
}

int FanSpeed::_callback_set_Target(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FanSpeed*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->target(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace FanSpeed
{
static const auto _property_Target =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void FanSpeed::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Target")
    {
        auto& v = std::get<uint64_t>(val);
        target(v, skipSignal);
        return;
    }
}

auto FanSpeed::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Target")
    {
        return target();
    }

    return PropertiesVariant();
}


const vtable_t FanSpeed::_vtable[] = {
    vtable::start(),
    vtable::property("Target",
                     details::FanSpeed::_property_Target
                        .data(),
                     _callback_get_Target,
                     _callback_set_Target,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

