#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

class ApplyTime
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        ApplyTime() = delete;
        ApplyTime(const ApplyTime&) = delete;
        ApplyTime& operator=(const ApplyTime&) = delete;
        ApplyTime(ApplyTime&&) = delete;
        ApplyTime& operator=(ApplyTime&&) = delete;
        virtual ~ApplyTime() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        ApplyTime(bus_t& bus, const char* path);

        enum class RequestedApplyTimes
        {
            Immediate,
            OnReset,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                RequestedApplyTimes>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        ApplyTime(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RequestedApplyTime */
        virtual RequestedApplyTimes requestedApplyTime() const;
        /** Set value of RequestedApplyTime with option to skip sending signal */
        virtual RequestedApplyTimes requestedApplyTime(RequestedApplyTimes value,
               bool skipSignal);
        /** Set value of RequestedApplyTime */
        virtual RequestedApplyTimes requestedApplyTime(RequestedApplyTimes value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.ApplyTime.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static RequestedApplyTimes convertRequestedApplyTimesFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Software.ApplyTime.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<RequestedApplyTimes> convertStringToRequestedApplyTimes(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Software.ApplyTime.<value name>"
         */
        static std::string convertRequestedApplyTimesToString(RequestedApplyTimes e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Software_ApplyTime_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Software_ApplyTime_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Software.ApplyTime";

    private:

        /** @brief sd-bus callback for get-property 'RequestedApplyTime' */
        static int _callback_get_RequestedApplyTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RequestedApplyTime' */
        static int _callback_set_RequestedApplyTime(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Software_ApplyTime_interface;
        sdbusplus::SdBusInterface *_intf;

        RequestedApplyTimes _requestedApplyTime{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type ApplyTime::RequestedApplyTimes.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(ApplyTime::RequestedApplyTimes e)
{
    return ApplyTime::convertRequestedApplyTimesToString(e);
}

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Software::server::ApplyTime::RequestedApplyTimes>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Software::server::ApplyTime::convertStringToRequestedApplyTimes(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Software::server::ApplyTime::RequestedApplyTimes>
{
    static std::string op(xyz::openbmc_project::Software::server::ApplyTime::RequestedApplyTimes value)
    {
        return xyz::openbmc_project::Software::server::ApplyTime::convertRequestedApplyTimesToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

