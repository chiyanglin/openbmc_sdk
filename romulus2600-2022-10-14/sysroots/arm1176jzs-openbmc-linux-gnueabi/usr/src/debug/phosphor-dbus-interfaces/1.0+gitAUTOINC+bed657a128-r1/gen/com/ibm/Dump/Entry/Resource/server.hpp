#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>




#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace Entry
{
namespace server
{

class Resource
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Resource() = delete;
        Resource(const Resource&) = delete;
        Resource& operator=(const Resource&) = delete;
        Resource(Resource&&) = delete;
        Resource& operator=(Resource&&) = delete;
        virtual ~Resource() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Resource(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::string,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Resource(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of SourceDumpId */
        virtual uint32_t sourceDumpId() const;
        /** Set value of SourceDumpId with option to skip sending signal */
        virtual uint32_t sourceDumpId(uint32_t value,
               bool skipSignal);
        /** Set value of SourceDumpId */
        virtual uint32_t sourceDumpId(uint32_t value);
        /** Get value of VSPString */
        virtual std::string vspString() const;
        /** Set value of VSPString with option to skip sending signal */
        virtual std::string vspString(std::string value,
               bool skipSignal);
        /** Set value of VSPString */
        virtual std::string vspString(std::string value);
        /** Get value of Password */
        virtual std::string password() const;
        /** Set value of Password with option to skip sending signal */
        virtual std::string password(std::string value,
               bool skipSignal);
        /** Set value of Password */
        virtual std::string password(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_Dump_Entry_Resource_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_Dump_Entry_Resource_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.Dump.Entry.Resource";

    private:

        /** @brief sd-bus callback for get-property 'SourceDumpId' */
        static int _callback_get_SourceDumpId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SourceDumpId' */
        static int _callback_set_SourceDumpId(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VSPString' */
        static int _callback_get_VSPString(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VSPString' */
        static int _callback_set_VSPString(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Password' */
        static int _callback_get_Password(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Password' */
        static int _callback_set_Password(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_Dump_Entry_Resource_interface;
        sdbusplus::SdBusInterface *_intf;

        uint32_t _sourceDumpId{};
        std::string _vspString{};
        std::string _password{};

};


} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

