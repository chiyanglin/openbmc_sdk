#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/User/PrivilegeMapper/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/User/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

PrivilegeMapper::PrivilegeMapper(bus_t& bus, const char* path)
        : _xyz_openbmc_project_User_PrivilegeMapper_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int PrivilegeMapper::_callback_Create(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PrivilegeMapper*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& groupName, std::string&& privilege)
                    {
                        return o->create(
                                groupName, privilege);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::User::Common::Error::PrivilegeMappingExists& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PrivilegeMapper
{
static const auto _param_Create =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string>());
static const auto _return_Create =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t PrivilegeMapper::_vtable[] = {
    vtable::start(),

    vtable::method("Create",
                   details::PrivilegeMapper::_param_Create
                        .data(),
                   details::PrivilegeMapper::_return_Create
                        .data(),
                   _callback_Create),
    vtable::end()
};

} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

