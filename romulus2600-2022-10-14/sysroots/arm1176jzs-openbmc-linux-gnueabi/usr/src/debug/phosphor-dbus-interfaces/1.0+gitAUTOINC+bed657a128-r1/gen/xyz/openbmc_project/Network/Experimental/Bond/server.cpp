#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/Experimental/Bond/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Experimental
{
namespace server
{

Bond::Bond(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_Experimental_Bond_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Bond::Bond(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Bond(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Bond::interfaceName() const ->
        std::string
{
    return _interfaceName;
}

int Bond::_callback_get_InterfaceName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->interfaceName();
                    }
                ));
    }
}

auto Bond::interfaceName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_interfaceName != value)
    {
        _interfaceName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Bond_interface.property_changed("InterfaceName");
        }
    }

    return _interfaceName;
}

auto Bond::interfaceName(std::string val) ->
        std::string
{
    return interfaceName(val, false);
}

int Bond::_callback_set_InterfaceName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->interfaceName(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Bond
{
static const auto _property_InterfaceName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Bond::bondedInterfaces() const ->
        std::vector<std::string>
{
    return _bondedInterfaces;
}

int Bond::_callback_get_BondedInterfaces(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bondedInterfaces();
                    }
                ));
    }
}

auto Bond::bondedInterfaces(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_bondedInterfaces != value)
    {
        _bondedInterfaces = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Bond_interface.property_changed("BondedInterfaces");
        }
    }

    return _bondedInterfaces;
}

auto Bond::bondedInterfaces(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return bondedInterfaces(val, false);
}

int Bond::_callback_set_BondedInterfaces(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->bondedInterfaces(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Bond
{
static const auto _property_BondedInterfaces =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto Bond::mode() const ->
        BondingMode
{
    return _mode;
}

int Bond::_callback_get_Mode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mode();
                    }
                ));
    }
}

auto Bond::mode(BondingMode value,
                                         bool skipSignal) ->
        BondingMode
{
    if (_mode != value)
    {
        _mode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Bond_interface.property_changed("Mode");
        }
    }

    return _mode;
}

auto Bond::mode(BondingMode val) ->
        BondingMode
{
    return mode(val, false);
}

int Bond::_callback_set_Mode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](BondingMode&& arg)
                    {
                        o->mode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Bond
{
static const auto _property_Mode =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond::BondingMode>());
}
}

auto Bond::transmitHashPolicy() const ->
        HashPolicy
{
    return _transmitHashPolicy;
}

int Bond::_callback_get_TransmitHashPolicy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->transmitHashPolicy();
                    }
                ));
    }
}

auto Bond::transmitHashPolicy(HashPolicy value,
                                         bool skipSignal) ->
        HashPolicy
{
    if (_transmitHashPolicy != value)
    {
        _transmitHashPolicy = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Experimental_Bond_interface.property_changed("TransmitHashPolicy");
        }
    }

    return _transmitHashPolicy;
}

auto Bond::transmitHashPolicy(HashPolicy val) ->
        HashPolicy
{
    return transmitHashPolicy(val, false);
}

int Bond::_callback_set_TransmitHashPolicy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Bond*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](HashPolicy&& arg)
                    {
                        o->transmitHashPolicy(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Bond
{
static const auto _property_TransmitHashPolicy =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::Experimental::server::Bond::HashPolicy>());
}
}

void Bond::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "InterfaceName")
    {
        auto& v = std::get<std::string>(val);
        interfaceName(v, skipSignal);
        return;
    }
    if (_name == "BondedInterfaces")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        bondedInterfaces(v, skipSignal);
        return;
    }
    if (_name == "Mode")
    {
        auto& v = std::get<BondingMode>(val);
        mode(v, skipSignal);
        return;
    }
    if (_name == "TransmitHashPolicy")
    {
        auto& v = std::get<HashPolicy>(val);
        transmitHashPolicy(v, skipSignal);
        return;
    }
}

auto Bond::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "InterfaceName")
    {
        return interfaceName();
    }
    if (_name == "BondedInterfaces")
    {
        return bondedInterfaces();
    }
    if (_name == "Mode")
    {
        return mode();
    }
    if (_name == "TransmitHashPolicy")
    {
        return transmitHashPolicy();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Bond::BondingMode */
static const std::tuple<const char*, Bond::BondingMode> mappingBondBondingMode[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.RoundRobin",                 Bond::BondingMode::RoundRobin ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.ActiveBackup",                 Bond::BondingMode::ActiveBackup ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.XOR",                 Bond::BondingMode::XOR ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.Broadcast",                 Bond::BondingMode::Broadcast ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.Dynamic",                 Bond::BondingMode::Dynamic ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.TLB",                 Bond::BondingMode::TLB ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.BondingMode.ALB",                 Bond::BondingMode::ALB ),
        };

} // anonymous namespace

auto Bond::convertStringToBondingMode(const std::string& s) noexcept ->
        std::optional<BondingMode>
{
    auto i = std::find_if(
            std::begin(mappingBondBondingMode),
            std::end(mappingBondBondingMode),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingBondBondingMode) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Bond::convertBondingModeFromString(const std::string& s) ->
        BondingMode
{
    auto r = convertStringToBondingMode(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Bond::convertBondingModeToString(Bond::BondingMode v)
{
    auto i = std::find_if(
            std::begin(mappingBondBondingMode),
            std::end(mappingBondBondingMode),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingBondBondingMode))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Bond::HashPolicy */
static const std::tuple<const char*, Bond::HashPolicy> mappingBondHashPolicy[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.HashPolicy.Layer2",                 Bond::HashPolicy::Layer2 ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.HashPolicy.Layer2Plus3",                 Bond::HashPolicy::Layer2Plus3 ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.HashPolicy.Layer3Plus4",                 Bond::HashPolicy::Layer3Plus4 ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.HashPolicy.Encap2Plus3",                 Bond::HashPolicy::Encap2Plus3 ),
            std::make_tuple( "xyz.openbmc_project.Network.Experimental.Bond.HashPolicy.Encap3Plus4",                 Bond::HashPolicy::Encap3Plus4 ),
        };

} // anonymous namespace

auto Bond::convertStringToHashPolicy(const std::string& s) noexcept ->
        std::optional<HashPolicy>
{
    auto i = std::find_if(
            std::begin(mappingBondHashPolicy),
            std::end(mappingBondHashPolicy),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingBondHashPolicy) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Bond::convertHashPolicyFromString(const std::string& s) ->
        HashPolicy
{
    auto r = convertStringToHashPolicy(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Bond::convertHashPolicyToString(Bond::HashPolicy v)
{
    auto i = std::find_if(
            std::begin(mappingBondHashPolicy),
            std::end(mappingBondHashPolicy),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingBondHashPolicy))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Bond::_vtable[] = {
    vtable::start(),
    vtable::property("InterfaceName",
                     details::Bond::_property_InterfaceName
                        .data(),
                     _callback_get_InterfaceName,
                     _callback_set_InterfaceName,
                     vtable::property_::emits_change),
    vtable::property("BondedInterfaces",
                     details::Bond::_property_BondedInterfaces
                        .data(),
                     _callback_get_BondedInterfaces,
                     _callback_set_BondedInterfaces,
                     vtable::property_::emits_change),
    vtable::property("Mode",
                     details::Bond::_property_Mode
                        .data(),
                     _callback_get_Mode,
                     _callback_set_Mode,
                     vtable::property_::emits_change),
    vtable::property("TransmitHashPolicy",
                     details::Bond::_property_TransmitHashPolicy
                        .data(),
                     _callback_get_TransmitHashPolicy,
                     _callback_set_TransmitHashPolicy,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Experimental
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

