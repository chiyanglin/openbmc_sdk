#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/IP/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

IP::IP(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_IP_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

IP::IP(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : IP(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto IP::address() const ->
        std::string
{
    return _address;
}

int IP::_callback_get_Address(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->address();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto IP::address(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_address != value)
    {
        _address = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_IP_interface.property_changed("Address");
        }
    }

    return _address;
}

auto IP::address(std::string val) ->
        std::string
{
    return address(val, false);
}

int IP::_callback_set_Address(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->address(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace IP
{
static const auto _property_Address =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto IP::prefixLength() const ->
        uint8_t
{
    return _prefixLength;
}

int IP::_callback_get_PrefixLength(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->prefixLength();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto IP::prefixLength(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_prefixLength != value)
    {
        _prefixLength = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_IP_interface.property_changed("PrefixLength");
        }
    }

    return _prefixLength;
}

auto IP::prefixLength(uint8_t val) ->
        uint8_t
{
    return prefixLength(val, false);
}

int IP::_callback_set_PrefixLength(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->prefixLength(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace IP
{
static const auto _property_PrefixLength =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto IP::origin() const ->
        AddressOrigin
{
    return _origin;
}

int IP::_callback_get_Origin(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->origin();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto IP::origin(AddressOrigin value,
                                         bool skipSignal) ->
        AddressOrigin
{
    if (_origin != value)
    {
        _origin = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_IP_interface.property_changed("Origin");
        }
    }

    return _origin;
}

auto IP::origin(AddressOrigin val) ->
        AddressOrigin
{
    return origin(val, false);
}

int IP::_callback_set_Origin(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](AddressOrigin&& arg)
                    {
                        o->origin(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace IP
{
static const auto _property_Origin =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::server::IP::AddressOrigin>());
}
}

auto IP::gateway() const ->
        std::string
{
    return _gateway;
}

int IP::_callback_get_Gateway(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->gateway();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto IP::gateway(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_gateway != value)
    {
        _gateway = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_IP_interface.property_changed("Gateway");
        }
    }

    return _gateway;
}

auto IP::gateway(std::string val) ->
        std::string
{
    return gateway(val, false);
}

int IP::_callback_set_Gateway(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->gateway(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace IP
{
static const auto _property_Gateway =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto IP::type() const ->
        Protocol
{
    return _type;
}

int IP::_callback_get_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->type();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto IP::type(Protocol value,
                                         bool skipSignal) ->
        Protocol
{
    if (_type != value)
    {
        _type = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_IP_interface.property_changed("Type");
        }
    }

    return _type;
}

auto IP::type(Protocol val) ->
        Protocol
{
    return type(val, false);
}

int IP::_callback_set_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<IP*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Protocol&& arg)
                    {
                        o->type(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace IP
{
static const auto _property_Type =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Network::server::IP::Protocol>());
}
}

void IP::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Address")
    {
        auto& v = std::get<std::string>(val);
        address(v, skipSignal);
        return;
    }
    if (_name == "PrefixLength")
    {
        auto& v = std::get<uint8_t>(val);
        prefixLength(v, skipSignal);
        return;
    }
    if (_name == "Origin")
    {
        auto& v = std::get<AddressOrigin>(val);
        origin(v, skipSignal);
        return;
    }
    if (_name == "Gateway")
    {
        auto& v = std::get<std::string>(val);
        gateway(v, skipSignal);
        return;
    }
    if (_name == "Type")
    {
        auto& v = std::get<Protocol>(val);
        type(v, skipSignal);
        return;
    }
}

auto IP::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Address")
    {
        return address();
    }
    if (_name == "PrefixLength")
    {
        return prefixLength();
    }
    if (_name == "Origin")
    {
        return origin();
    }
    if (_name == "Gateway")
    {
        return gateway();
    }
    if (_name == "Type")
    {
        return type();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for IP::Protocol */
static const std::tuple<const char*, IP::Protocol> mappingIPProtocol[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.IP.Protocol.IPv4",                 IP::Protocol::IPv4 ),
            std::make_tuple( "xyz.openbmc_project.Network.IP.Protocol.IPv6",                 IP::Protocol::IPv6 ),
        };

} // anonymous namespace

auto IP::convertStringToProtocol(const std::string& s) noexcept ->
        std::optional<Protocol>
{
    auto i = std::find_if(
            std::begin(mappingIPProtocol),
            std::end(mappingIPProtocol),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingIPProtocol) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto IP::convertProtocolFromString(const std::string& s) ->
        Protocol
{
    auto r = convertStringToProtocol(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string IP::convertProtocolToString(IP::Protocol v)
{
    auto i = std::find_if(
            std::begin(mappingIPProtocol),
            std::end(mappingIPProtocol),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingIPProtocol))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for IP::AddressOrigin */
static const std::tuple<const char*, IP::AddressOrigin> mappingIPAddressOrigin[] =
        {
            std::make_tuple( "xyz.openbmc_project.Network.IP.AddressOrigin.Static",                 IP::AddressOrigin::Static ),
            std::make_tuple( "xyz.openbmc_project.Network.IP.AddressOrigin.DHCP",                 IP::AddressOrigin::DHCP ),
            std::make_tuple( "xyz.openbmc_project.Network.IP.AddressOrigin.LinkLocal",                 IP::AddressOrigin::LinkLocal ),
            std::make_tuple( "xyz.openbmc_project.Network.IP.AddressOrigin.SLAAC",                 IP::AddressOrigin::SLAAC ),
        };

} // anonymous namespace

auto IP::convertStringToAddressOrigin(const std::string& s) noexcept ->
        std::optional<AddressOrigin>
{
    auto i = std::find_if(
            std::begin(mappingIPAddressOrigin),
            std::end(mappingIPAddressOrigin),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingIPAddressOrigin) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto IP::convertAddressOriginFromString(const std::string& s) ->
        AddressOrigin
{
    auto r = convertStringToAddressOrigin(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string IP::convertAddressOriginToString(IP::AddressOrigin v)
{
    auto i = std::find_if(
            std::begin(mappingIPAddressOrigin),
            std::end(mappingIPAddressOrigin),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingIPAddressOrigin))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t IP::_vtable[] = {
    vtable::start(),
    vtable::property("Address",
                     details::IP::_property_Address
                        .data(),
                     _callback_get_Address,
                     _callback_set_Address,
                     vtable::property_::emits_change),
    vtable::property("PrefixLength",
                     details::IP::_property_PrefixLength
                        .data(),
                     _callback_get_PrefixLength,
                     _callback_set_PrefixLength,
                     vtable::property_::emits_change),
    vtable::property("Origin",
                     details::IP::_property_Origin
                        .data(),
                     _callback_get_Origin,
                     _callback_set_Origin,
                     vtable::property_::emits_change),
    vtable::property("Gateway",
                     details::IP::_property_Gateway
                        .data(),
                     _callback_get_Gateway,
                     _callback_set_Gateway,
                     vtable::property_::emits_change),
    vtable::property("Type",
                     details::IP::_property_Type
                        .data(),
                     _callback_get_Type,
                     _callback_set_Type,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

