#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/Entry/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Syslog
{
namespace Destination
{
namespace Mail
{
namespace server
{

Entry::Entry(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Entry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entry::Entry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Entry::mailto() const ->
        std::string
{
    return _mailto;
}

int Entry::_callback_get_Mailto(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->mailto();
                    }
                ));
    }
}

auto Entry::mailto(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_mailto != value)
    {
        _mailto = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Entry_interface.property_changed("Mailto");
        }
    }

    return _mailto;
}

auto Entry::mailto(std::string val) ->
        std::string
{
    return mailto(val, false);
}


namespace details
{
namespace Entry
{
static const auto _property_Mailto =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Entry::level() const ->
        xyz::openbmc_project::Logging::server::Entry::Level
{
    return _level;
}

int Entry::_callback_get_Level(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->level();
                    }
                ));
    }
}

auto Entry::level(xyz::openbmc_project::Logging::server::Entry::Level value,
                                         bool skipSignal) ->
        xyz::openbmc_project::Logging::server::Entry::Level
{
    if (_level != value)
    {
        _level = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Entry_interface.property_changed("Level");
        }
    }

    return _level;
}

auto Entry::level(xyz::openbmc_project::Logging::server::Entry::Level val) ->
        xyz::openbmc_project::Logging::server::Entry::Level
{
    return level(val, false);
}

int Entry::_callback_set_Level(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](xyz::openbmc_project::Logging::server::Entry::Level&& arg)
                    {
                        o->level(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Level =
    utility::tuple_to_array(message::types::type_id<
            xyz::openbmc_project::Logging::server::Entry::Level>());
}
}

void Entry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Mailto")
    {
        auto& v = std::get<std::string>(val);
        mailto(v, skipSignal);
        return;
    }
    if (_name == "Level")
    {
        auto& v = std::get<xyz::openbmc_project::Logging::server::Entry::Level>(val);
        level(v, skipSignal);
        return;
    }
}

auto Entry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Mailto")
    {
        return mailto();
    }
    if (_name == "Level")
    {
        return level();
    }

    return PropertiesVariant();
}


const vtable_t Entry::_vtable[] = {
    vtable::start(),
    vtable::property("Mailto",
                     details::Entry::_property_Mailto
                        .data(),
                     _callback_get_Mailto,
                     vtable::property_::emits_change),
    vtable::property("Level",
                     details::Entry::_property_Level
                        .data(),
                     _callback_get_Level,
                     _callback_set_Level,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Mail
} // namespace Destination
} // namespace Syslog
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

