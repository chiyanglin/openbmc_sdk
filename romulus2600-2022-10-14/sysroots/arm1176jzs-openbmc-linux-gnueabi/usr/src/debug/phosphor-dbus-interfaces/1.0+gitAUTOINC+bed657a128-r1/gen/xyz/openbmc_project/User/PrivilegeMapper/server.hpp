#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

class PrivilegeMapper
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PrivilegeMapper() = delete;
        PrivilegeMapper(const PrivilegeMapper&) = delete;
        PrivilegeMapper& operator=(const PrivilegeMapper&) = delete;
        PrivilegeMapper(PrivilegeMapper&&) = delete;
        PrivilegeMapper& operator=(PrivilegeMapper&&) = delete;
        virtual ~PrivilegeMapper() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PrivilegeMapper(bus_t& bus, const char* path);



        /** @brief Implementation for Create
         *  Creates a mapping for the group to the privilege.
         *
         *  @param[in] groupName - Group Name to which the privilege is to be assigned. In the case of LDAP, the GroupName will be the LDAP group the user is part of.
         *  @param[in] privilege - The privilege associated with the group. The set of available privileges are xyz.openbmc_project.User.Manager.AllPrivileges. xyz.openbmc_project.Common.Error.InvalidArgument exception will be thrown if the privilege is invalid. Additional documentation on privilege is available here. https://github.com/openbmc/docs/blob/master/architecture/user-management.md
         *
         *  @return path[sdbusplus::message::object_path] - The path for the created privilege mapping object.
         */
        virtual sdbusplus::message::object_path create(
            std::string groupName,
            std::string privilege) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_User_PrivilegeMapper_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_User_PrivilegeMapper_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.User.PrivilegeMapper";

    private:

        /** @brief sd-bus callback for Create
         */
        static int _callback_Create(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_User_PrivilegeMapper_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

