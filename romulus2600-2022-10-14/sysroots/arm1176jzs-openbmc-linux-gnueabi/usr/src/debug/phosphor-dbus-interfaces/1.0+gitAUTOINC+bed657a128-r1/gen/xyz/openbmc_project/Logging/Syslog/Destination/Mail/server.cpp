#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Syslog
{
namespace Destination
{
namespace server
{

Mail::Mail(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Syslog_Destination_Mail_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Mail::Mail(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Mail(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Mail::from() const ->
        std::string
{
    return _from;
}

int Mail::_callback_get_From(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mail*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->from();
                    }
                ));
    }
}

auto Mail::from(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_from != value)
    {
        _from = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Syslog_Destination_Mail_interface.property_changed("From");
        }
    }

    return _from;
}

auto Mail::from(std::string val) ->
        std::string
{
    return from(val, false);
}

int Mail::_callback_set_From(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Mail*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->from(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Mail
{
static const auto _property_From =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Mail::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "From")
    {
        auto& v = std::get<std::string>(val);
        from(v, skipSignal);
        return;
    }
}

auto Mail::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "From")
    {
        return from();
    }

    return PropertiesVariant();
}


const vtable_t Mail::_vtable[] = {
    vtable::start(),
    vtable::property("From",
                     details::Mail::_property_From
                        .data(),
                     _callback_get_From,
                     _callback_set_From,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Destination
} // namespace Syslog
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

