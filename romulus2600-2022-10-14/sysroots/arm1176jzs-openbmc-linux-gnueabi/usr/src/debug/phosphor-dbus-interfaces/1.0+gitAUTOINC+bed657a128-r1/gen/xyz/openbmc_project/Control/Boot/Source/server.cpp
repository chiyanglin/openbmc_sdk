#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Boot/Source/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Boot
{
namespace server
{

Source::Source(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Boot_Source_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Source::Source(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Source(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Source::bootSource() const ->
        Sources
{
    return _bootSource;
}

int Source::_callback_get_BootSource(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Source*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bootSource();
                    }
                ));
    }
}

auto Source::bootSource(Sources value,
                                         bool skipSignal) ->
        Sources
{
    if (_bootSource != value)
    {
        _bootSource = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Boot_Source_interface.property_changed("BootSource");
        }
    }

    return _bootSource;
}

auto Source::bootSource(Sources val) ->
        Sources
{
    return bootSource(val, false);
}

int Source::_callback_set_BootSource(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Source*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Sources&& arg)
                    {
                        o->bootSource(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Source
{
static const auto _property_BootSource =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Boot::server::Source::Sources>());
}
}

void Source::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BootSource")
    {
        auto& v = std::get<Sources>(val);
        bootSource(v, skipSignal);
        return;
    }
}

auto Source::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BootSource")
    {
        return bootSource();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Source::Sources */
static const std::tuple<const char*, Source::Sources> mappingSourceSources[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Source.Sources.Disk",                 Source::Sources::Disk ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Source.Sources.ExternalMedia",                 Source::Sources::ExternalMedia ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Source.Sources.Network",                 Source::Sources::Network ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Source.Sources.Default",                 Source::Sources::Default ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Source.Sources.RemovableMedia",                 Source::Sources::RemovableMedia ),
            std::make_tuple( "xyz.openbmc_project.Control.Boot.Source.Sources.HTTP",                 Source::Sources::HTTP ),
        };

} // anonymous namespace

auto Source::convertStringToSources(const std::string& s) noexcept ->
        std::optional<Sources>
{
    auto i = std::find_if(
            std::begin(mappingSourceSources),
            std::end(mappingSourceSources),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingSourceSources) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Source::convertSourcesFromString(const std::string& s) ->
        Sources
{
    auto r = convertStringToSources(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Source::convertSourcesToString(Source::Sources v)
{
    auto i = std::find_if(
            std::begin(mappingSourceSources),
            std::end(mappingSourceSources),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingSourceSources))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Source::_vtable[] = {
    vtable::start(),
    vtable::property("BootSource",
                     details::Source::_property_BootSource
                        .data(),
                     _callback_get_BootSource,
                     _callback_set_BootSource,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

