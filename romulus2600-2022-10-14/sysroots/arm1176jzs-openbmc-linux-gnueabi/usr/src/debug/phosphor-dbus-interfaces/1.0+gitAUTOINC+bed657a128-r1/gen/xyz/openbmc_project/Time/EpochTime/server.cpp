#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Time/EpochTime/server.hpp>

#include <xyz/openbmc_project/Time/error.hpp>
#include <xyz/openbmc_project/Time/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Time
{
namespace server
{

EpochTime::EpochTime(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Time_EpochTime_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

EpochTime::EpochTime(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : EpochTime(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto EpochTime::elapsed() const ->
        uint64_t
{
    return _elapsed;
}

int EpochTime::_callback_get_Elapsed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EpochTime*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->elapsed();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Time::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Time::Error::Failed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto EpochTime::elapsed(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_elapsed != value)
    {
        _elapsed = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Time_EpochTime_interface.property_changed("Elapsed");
        }
    }

    return _elapsed;
}

auto EpochTime::elapsed(uint64_t val) ->
        uint64_t
{
    return elapsed(val, false);
}

int EpochTime::_callback_set_Elapsed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<EpochTime*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->elapsed(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Time::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Time::Error::Failed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace EpochTime
{
static const auto _property_Elapsed =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void EpochTime::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Elapsed")
    {
        auto& v = std::get<uint64_t>(val);
        elapsed(v, skipSignal);
        return;
    }
}

auto EpochTime::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Elapsed")
    {
        return elapsed();
    }

    return PropertiesVariant();
}


const vtable_t EpochTime::_vtable[] = {
    vtable::start(),
    vtable::property("Elapsed",
                     details::EpochTime::_property_Elapsed
                        .data(),
                     _callback_get_Elapsed,
                     _callback_set_Elapsed,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Time
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

