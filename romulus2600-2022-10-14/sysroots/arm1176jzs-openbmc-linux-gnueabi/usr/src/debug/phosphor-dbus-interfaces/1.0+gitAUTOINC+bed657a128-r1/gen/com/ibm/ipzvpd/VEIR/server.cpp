#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VEIR/server.hpp>



namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VEIR::VEIR(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VEIR_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VEIR::VEIR(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VEIR(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VEIR::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VEIR::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VEIR*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VEIR::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VEIR_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VEIR::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VEIR::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VEIR*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VEIR
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VEIR::pdi() const ->
        std::vector<uint8_t>
{
    return _pdi;
}

int VEIR::_callback_get_PD_I(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VEIR*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->pdi();
                    }
                ));
    }
}

auto VEIR::pdi(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_pdi != value)
    {
        _pdi = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VEIR_interface.property_changed("PD_I");
        }
    }

    return _pdi;
}

auto VEIR::pdi(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return pdi(val, false);
}

int VEIR::_callback_set_PD_I(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VEIR*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->pdi(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VEIR
{
static const auto _property_PD_I =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VEIR::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "PD_I")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        pdi(v, skipSignal);
        return;
    }
}

auto VEIR::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "PD_I")
    {
        return pdi();
    }

    return PropertiesVariant();
}


const vtable_t VEIR::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VEIR::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("PD_I",
                     details::VEIR::_property_PD_I
                        .data(),
                     _callback_get_PD_I,
                     _callback_set_PD_I,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

