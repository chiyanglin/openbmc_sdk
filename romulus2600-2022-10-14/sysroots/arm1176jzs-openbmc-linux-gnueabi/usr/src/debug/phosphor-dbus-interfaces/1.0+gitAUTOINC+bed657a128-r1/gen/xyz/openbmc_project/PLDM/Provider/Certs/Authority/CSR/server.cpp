#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/PLDM/Provider/Certs/Authority/CSR/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace Provider
{
namespace Certs
{
namespace Authority
{
namespace server
{

CSR::CSR(bus_t& bus, const char* path)
        : _xyz_openbmc_project_PLDM_Provider_Certs_Authority_CSR_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CSR::CSR(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CSR(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CSR::csr() const ->
        std::string
{
    return _csr;
}

int CSR::_callback_get_CSR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CSR*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->csr();
                    }
                ));
    }
}

auto CSR::csr(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_csr != value)
    {
        _csr = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_PLDM_Provider_Certs_Authority_CSR_interface.property_changed("CSR");
        }
    }

    return _csr;
}

auto CSR::csr(std::string val) ->
        std::string
{
    return csr(val, false);
}


namespace details
{
namespace CSR
{
static const auto _property_CSR =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void CSR::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CSR")
    {
        auto& v = std::get<std::string>(val);
        csr(v, skipSignal);
        return;
    }
}

auto CSR::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CSR")
    {
        return csr();
    }

    return PropertiesVariant();
}


const vtable_t CSR::_vtable[] = {
    vtable::start(),
    vtable::property("CSR",
                     details::CSR::_property_CSR
                        .data(),
                     _callback_get_CSR,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace Authority
} // namespace Certs
} // namespace Provider
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

