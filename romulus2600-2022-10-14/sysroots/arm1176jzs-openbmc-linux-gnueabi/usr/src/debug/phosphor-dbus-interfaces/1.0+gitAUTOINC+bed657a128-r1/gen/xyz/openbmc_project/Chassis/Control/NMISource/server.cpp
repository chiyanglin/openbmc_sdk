#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Control/NMISource/server.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Control
{
namespace server
{

NMISource::NMISource(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Control_NMISource_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

NMISource::NMISource(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : NMISource(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int NMISource::_callback_NmiEnable(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nmiEnable(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace NMISource
{
static const auto _param_NmiEnable =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_NmiEnable =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}



auto NMISource::bmcSource() const ->
        BMCSourceSignal
{
    return _bmcSource;
}

int NMISource::_callback_get_BMCSource(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->bmcSource();
                    }
                ));
    }
}

auto NMISource::bmcSource(BMCSourceSignal value,
                                         bool skipSignal) ->
        BMCSourceSignal
{
    if (_bmcSource != value)
    {
        _bmcSource = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Chassis_Control_NMISource_interface.property_changed("BMCSource");
        }
    }

    return _bmcSource;
}

auto NMISource::bmcSource(BMCSourceSignal val) ->
        BMCSourceSignal
{
    return bmcSource(val, false);
}

int NMISource::_callback_set_BMCSource(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<NMISource*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](BMCSourceSignal&& arg)
                    {
                        o->bmcSource(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace NMISource
{
static const auto _property_BMCSource =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Chassis::Control::server::NMISource::BMCSourceSignal>());
}
}

void NMISource::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BMCSource")
    {
        auto& v = std::get<BMCSourceSignal>(val);
        bmcSource(v, skipSignal);
        return;
    }
}

auto NMISource::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BMCSource")
    {
        return bmcSource();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for NMISource::BMCSourceSignal */
static const std::tuple<const char*, NMISource::BMCSourceSignal> mappingNMISourceBMCSourceSignal[] =
        {
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.None",                 NMISource::BMCSourceSignal::None ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.FrontPanelButton",                 NMISource::BMCSourceSignal::FrontPanelButton ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.Watchdog",                 NMISource::BMCSourceSignal::Watchdog ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.ChassisCmd",                 NMISource::BMCSourceSignal::ChassisCmd ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.MemoryError",                 NMISource::BMCSourceSignal::MemoryError ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.PciBusError",                 NMISource::BMCSourceSignal::PciBusError ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.PCH",                 NMISource::BMCSourceSignal::PCH ),
            std::make_tuple( "xyz.openbmc_project.Chassis.Control.NMISource.BMCSourceSignal.Chipset",                 NMISource::BMCSourceSignal::Chipset ),
        };

} // anonymous namespace

auto NMISource::convertStringToBMCSourceSignal(const std::string& s) noexcept ->
        std::optional<BMCSourceSignal>
{
    auto i = std::find_if(
            std::begin(mappingNMISourceBMCSourceSignal),
            std::end(mappingNMISourceBMCSourceSignal),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingNMISourceBMCSourceSignal) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto NMISource::convertBMCSourceSignalFromString(const std::string& s) ->
        BMCSourceSignal
{
    auto r = convertStringToBMCSourceSignal(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string NMISource::convertBMCSourceSignalToString(NMISource::BMCSourceSignal v)
{
    auto i = std::find_if(
            std::begin(mappingNMISourceBMCSourceSignal),
            std::end(mappingNMISourceBMCSourceSignal),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingNMISourceBMCSourceSignal))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t NMISource::_vtable[] = {
    vtable::start(),

    vtable::method("nmiEnable",
                   details::NMISource::_param_NmiEnable
                        .data(),
                   details::NMISource::_return_NmiEnable
                        .data(),
                   _callback_NmiEnable),
    vtable::property("BMCSource",
                     details::NMISource::_property_BMCSource
                        .data(),
                     _callback_get_BMCSource,
                     _callback_set_BMCSource,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

