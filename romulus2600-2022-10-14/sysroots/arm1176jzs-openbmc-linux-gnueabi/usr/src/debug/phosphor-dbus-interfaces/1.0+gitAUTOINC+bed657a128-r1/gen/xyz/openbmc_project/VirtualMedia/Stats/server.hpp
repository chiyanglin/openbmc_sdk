#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace VirtualMedia
{
namespace server
{

class Stats
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Stats() = delete;
        Stats(const Stats&) = delete;
        Stats& operator=(const Stats&) = delete;
        Stats(Stats&&) = delete;
        Stats& operator=(Stats&&) = delete;
        virtual ~Stats() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Stats(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Stats(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ReadIO */
        virtual uint64_t readIO() const;
        /** Set value of ReadIO with option to skip sending signal */
        virtual uint64_t readIO(uint64_t value,
               bool skipSignal);
        /** Set value of ReadIO */
        virtual uint64_t readIO(uint64_t value);
        /** Get value of WriteIO */
        virtual uint64_t writeIO() const;
        /** Set value of WriteIO with option to skip sending signal */
        virtual uint64_t writeIO(uint64_t value,
               bool skipSignal);
        /** Set value of WriteIO */
        virtual uint64_t writeIO(uint64_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_VirtualMedia_Stats_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_VirtualMedia_Stats_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.VirtualMedia.Stats";

    private:

        /** @brief sd-bus callback for get-property 'ReadIO' */
        static int _callback_get_ReadIO(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'WriteIO' */
        static int _callback_get_WriteIO(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_VirtualMedia_Stats_interface;
        sdbusplus::SdBusInterface *_intf;

        uint64_t _readIO{};
        uint64_t _writeIO{};

};


} // namespace server
} // namespace VirtualMedia
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

