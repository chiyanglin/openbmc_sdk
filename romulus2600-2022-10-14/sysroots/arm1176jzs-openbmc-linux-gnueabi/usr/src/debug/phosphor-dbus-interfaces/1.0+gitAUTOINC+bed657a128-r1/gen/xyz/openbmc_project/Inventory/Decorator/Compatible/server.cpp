#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Compatible/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Compatible::Compatible(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Compatible_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Compatible::Compatible(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Compatible(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Compatible::names() const ->
        std::vector<std::string>
{
    return _names;
}

int Compatible::_callback_get_Names(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Compatible*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->names();
                    }
                ));
    }
}

auto Compatible::names(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_names != value)
    {
        _names = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Compatible_interface.property_changed("Names");
        }
    }

    return _names;
}

auto Compatible::names(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return names(val, false);
}

int Compatible::_callback_set_Names(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Compatible*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->names(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Compatible
{
static const auto _property_Names =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

void Compatible::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Names")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        names(v, skipSignal);
        return;
    }
}

auto Compatible::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Names")
    {
        return names();
    }

    return PropertiesVariant();
}


const vtable_t Compatible::_vtable[] = {
    vtable::start(),
    vtable::property("Names",
                     details::Compatible::_property_Names
                        .data(),
                     _callback_get_Names,
                     _callback_set_Names,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

