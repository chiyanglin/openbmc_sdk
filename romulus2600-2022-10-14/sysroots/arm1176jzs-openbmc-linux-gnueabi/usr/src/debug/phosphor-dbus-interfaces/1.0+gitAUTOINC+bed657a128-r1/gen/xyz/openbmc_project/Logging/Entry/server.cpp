#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Entry/server.hpp>












namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

Entry::Entry(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Entry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entry::Entry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Entry::_callback_GetEntry(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getEntry(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Entry
{
static const auto _param_GetEntry =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetEntry =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::unix_fd>());
}
}



auto Entry::id() const ->
        uint32_t
{
    return _id;
}

int Entry::_callback_get_Id(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->id();
                    }
                ));
    }
}

auto Entry::id(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_id != value)
    {
        _id = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("Id");
        }
    }

    return _id;
}

auto Entry::id(uint32_t val) ->
        uint32_t
{
    return id(val, false);
}

int Entry::_callback_set_Id(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->id(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Id =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Entry::timestamp() const ->
        uint64_t
{
    return _timestamp;
}

int Entry::_callback_get_Timestamp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timestamp();
                    }
                ));
    }
}

auto Entry::timestamp(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_timestamp != value)
    {
        _timestamp = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("Timestamp");
        }
    }

    return _timestamp;
}

auto Entry::timestamp(uint64_t val) ->
        uint64_t
{
    return timestamp(val, false);
}

int Entry::_callback_set_Timestamp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->timestamp(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Timestamp =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Entry::severity() const ->
        Level
{
    return _severity;
}

int Entry::_callback_get_Severity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->severity();
                    }
                ));
    }
}

auto Entry::severity(Level value,
                                         bool skipSignal) ->
        Level
{
    if (_severity != value)
    {
        _severity = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("Severity");
        }
    }

    return _severity;
}

auto Entry::severity(Level val) ->
        Level
{
    return severity(val, false);
}

int Entry::_callback_set_Severity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Level&& arg)
                    {
                        o->severity(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Severity =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Logging::server::Entry::Level>());
}
}

auto Entry::message() const ->
        std::string
{
    return _message;
}

int Entry::_callback_get_Message(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->message();
                    }
                ));
    }
}

auto Entry::message(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_message != value)
    {
        _message = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("Message");
        }
    }

    return _message;
}

auto Entry::message(std::string val) ->
        std::string
{
    return message(val, false);
}

int Entry::_callback_set_Message(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->message(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Message =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Entry::eventId() const ->
        std::string
{
    return _eventId;
}

int Entry::_callback_get_EventId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->eventId();
                    }
                ));
    }
}

auto Entry::eventId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_eventId != value)
    {
        _eventId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("EventId");
        }
    }

    return _eventId;
}

auto Entry::eventId(std::string val) ->
        std::string
{
    return eventId(val, false);
}

int Entry::_callback_set_EventId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->eventId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_EventId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Entry::additionalData() const ->
        std::vector<std::string>
{
    return _additionalData;
}

int Entry::_callback_get_AdditionalData(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->additionalData();
                    }
                ));
    }
}

auto Entry::additionalData(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_additionalData != value)
    {
        _additionalData = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("AdditionalData");
        }
    }

    return _additionalData;
}

auto Entry::additionalData(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return additionalData(val, false);
}

int Entry::_callback_set_AdditionalData(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->additionalData(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_AdditionalData =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto Entry::resolution() const ->
        std::string
{
    return _resolution;
}

int Entry::_callback_get_Resolution(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->resolution();
                    }
                ));
    }
}

auto Entry::resolution(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_resolution != value)
    {
        _resolution = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("Resolution");
        }
    }

    return _resolution;
}

auto Entry::resolution(std::string val) ->
        std::string
{
    return resolution(val, false);
}

int Entry::_callback_set_Resolution(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->resolution(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Resolution =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Entry::resolved() const ->
        bool
{
    return _resolved;
}

int Entry::_callback_get_Resolved(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->resolved();
                    }
                ));
    }
}

auto Entry::resolved(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_resolved != value)
    {
        _resolved = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("Resolved");
        }
    }

    return _resolved;
}

auto Entry::resolved(bool val) ->
        bool
{
    return resolved(val, false);
}

int Entry::_callback_set_Resolved(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->resolved(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Resolved =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Entry::serviceProviderNotify() const ->
        bool
{
    return _serviceProviderNotify;
}

int Entry::_callback_get_ServiceProviderNotify(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->serviceProviderNotify();
                    }
                ));
    }
}

auto Entry::serviceProviderNotify(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_serviceProviderNotify != value)
    {
        _serviceProviderNotify = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("ServiceProviderNotify");
        }
    }

    return _serviceProviderNotify;
}

auto Entry::serviceProviderNotify(bool val) ->
        bool
{
    return serviceProviderNotify(val, false);
}

int Entry::_callback_set_ServiceProviderNotify(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->serviceProviderNotify(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_ServiceProviderNotify =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Entry::updateTimestamp() const ->
        uint64_t
{
    return _updateTimestamp;
}

int Entry::_callback_get_UpdateTimestamp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->updateTimestamp();
                    }
                ));
    }
}

auto Entry::updateTimestamp(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_updateTimestamp != value)
    {
        _updateTimestamp = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Logging_Entry_interface.property_changed("UpdateTimestamp");
        }
    }

    return _updateTimestamp;
}

auto Entry::updateTimestamp(uint64_t val) ->
        uint64_t
{
    return updateTimestamp(val, false);
}

int Entry::_callback_set_UpdateTimestamp(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->updateTimestamp(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_UpdateTimestamp =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Entry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Id")
    {
        auto& v = std::get<uint32_t>(val);
        id(v, skipSignal);
        return;
    }
    if (_name == "Timestamp")
    {
        auto& v = std::get<uint64_t>(val);
        timestamp(v, skipSignal);
        return;
    }
    if (_name == "Severity")
    {
        auto& v = std::get<Level>(val);
        severity(v, skipSignal);
        return;
    }
    if (_name == "Message")
    {
        auto& v = std::get<std::string>(val);
        message(v, skipSignal);
        return;
    }
    if (_name == "EventId")
    {
        auto& v = std::get<std::string>(val);
        eventId(v, skipSignal);
        return;
    }
    if (_name == "AdditionalData")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        additionalData(v, skipSignal);
        return;
    }
    if (_name == "Resolution")
    {
        auto& v = std::get<std::string>(val);
        resolution(v, skipSignal);
        return;
    }
    if (_name == "Resolved")
    {
        auto& v = std::get<bool>(val);
        resolved(v, skipSignal);
        return;
    }
    if (_name == "ServiceProviderNotify")
    {
        auto& v = std::get<bool>(val);
        serviceProviderNotify(v, skipSignal);
        return;
    }
    if (_name == "UpdateTimestamp")
    {
        auto& v = std::get<uint64_t>(val);
        updateTimestamp(v, skipSignal);
        return;
    }
}

auto Entry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Id")
    {
        return id();
    }
    if (_name == "Timestamp")
    {
        return timestamp();
    }
    if (_name == "Severity")
    {
        return severity();
    }
    if (_name == "Message")
    {
        return message();
    }
    if (_name == "EventId")
    {
        return eventId();
    }
    if (_name == "AdditionalData")
    {
        return additionalData();
    }
    if (_name == "Resolution")
    {
        return resolution();
    }
    if (_name == "Resolved")
    {
        return resolved();
    }
    if (_name == "ServiceProviderNotify")
    {
        return serviceProviderNotify();
    }
    if (_name == "UpdateTimestamp")
    {
        return updateTimestamp();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Entry::Level */
static const std::tuple<const char*, Entry::Level> mappingEntryLevel[] =
        {
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Emergency",                 Entry::Level::Emergency ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Alert",                 Entry::Level::Alert ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Critical",                 Entry::Level::Critical ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Error",                 Entry::Level::Error ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Warning",                 Entry::Level::Warning ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Notice",                 Entry::Level::Notice ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Informational",                 Entry::Level::Informational ),
            std::make_tuple( "xyz.openbmc_project.Logging.Entry.Level.Debug",                 Entry::Level::Debug ),
        };

} // anonymous namespace

auto Entry::convertStringToLevel(const std::string& s) noexcept ->
        std::optional<Level>
{
    auto i = std::find_if(
            std::begin(mappingEntryLevel),
            std::end(mappingEntryLevel),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingEntryLevel) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Entry::convertLevelFromString(const std::string& s) ->
        Level
{
    auto r = convertStringToLevel(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Entry::convertLevelToString(Entry::Level v)
{
    auto i = std::find_if(
            std::begin(mappingEntryLevel),
            std::end(mappingEntryLevel),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingEntryLevel))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Entry::_vtable[] = {
    vtable::start(),

    vtable::method("GetEntry",
                   details::Entry::_param_GetEntry
                        .data(),
                   details::Entry::_return_GetEntry
                        .data(),
                   _callback_GetEntry),
    vtable::property("Id",
                     details::Entry::_property_Id
                        .data(),
                     _callback_get_Id,
                     _callback_set_Id,
                     vtable::property_::emits_change),
    vtable::property("Timestamp",
                     details::Entry::_property_Timestamp
                        .data(),
                     _callback_get_Timestamp,
                     _callback_set_Timestamp,
                     vtable::property_::emits_change),
    vtable::property("Severity",
                     details::Entry::_property_Severity
                        .data(),
                     _callback_get_Severity,
                     _callback_set_Severity,
                     vtable::property_::emits_change),
    vtable::property("Message",
                     details::Entry::_property_Message
                        .data(),
                     _callback_get_Message,
                     _callback_set_Message,
                     vtable::property_::emits_change),
    vtable::property("EventId",
                     details::Entry::_property_EventId
                        .data(),
                     _callback_get_EventId,
                     _callback_set_EventId,
                     vtable::property_::emits_change),
    vtable::property("AdditionalData",
                     details::Entry::_property_AdditionalData
                        .data(),
                     _callback_get_AdditionalData,
                     _callback_set_AdditionalData,
                     vtable::property_::emits_change),
    vtable::property("Resolution",
                     details::Entry::_property_Resolution
                        .data(),
                     _callback_get_Resolution,
                     _callback_set_Resolution,
                     vtable::property_::emits_change),
    vtable::property("Resolved",
                     details::Entry::_property_Resolved
                        .data(),
                     _callback_get_Resolved,
                     _callback_set_Resolved,
                     vtable::property_::emits_change),
    vtable::property("ServiceProviderNotify",
                     details::Entry::_property_ServiceProviderNotify
                        .data(),
                     _callback_get_ServiceProviderNotify,
                     _callback_set_ServiceProviderNotify,
                     vtable::property_::emits_change),
    vtable::property("UpdateTimestamp",
                     details::Entry::_property_UpdateTimestamp
                        .data(),
                     _callback_get_UpdateTimestamp,
                     _callback_set_UpdateTimestamp,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

