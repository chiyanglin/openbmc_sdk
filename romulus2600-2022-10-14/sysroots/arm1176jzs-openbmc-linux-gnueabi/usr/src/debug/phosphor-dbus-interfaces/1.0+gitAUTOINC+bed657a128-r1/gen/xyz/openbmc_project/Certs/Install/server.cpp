#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/Install/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Certs/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

Install::Install(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_Install_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Install::_callback_Install(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Install*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& path)
                    {
                        return o->install(
                                path);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Certs::Error::InvalidCertificate& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Install
{
static const auto _param_Install =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_Install =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
}
}




const vtable_t Install::_vtable[] = {
    vtable::start(),

    vtable::method("Install",
                   details::Install::_param_Install
                        .data(),
                   details::Install::_return_Install
                        .data(),
                   _callback_Install),
    vtable::end()
};

} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

