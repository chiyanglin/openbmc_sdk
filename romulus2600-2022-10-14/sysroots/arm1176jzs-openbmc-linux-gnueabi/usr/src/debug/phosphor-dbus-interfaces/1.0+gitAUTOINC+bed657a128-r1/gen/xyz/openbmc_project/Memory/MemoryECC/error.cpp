#include <xyz/openbmc_project/Memory/MemoryECC/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Memory
{
namespace MemoryECC
{
namespace Error
{
const char* isLoggingLimitReached::name() const noexcept
{
    return errName;
}
const char* isLoggingLimitReached::description() const noexcept
{
    return errDesc;
}
const char* isLoggingLimitReached::what() const noexcept
{
    return errWhat;
}
const char* ceCount::name() const noexcept
{
    return errName;
}
const char* ceCount::description() const noexcept
{
    return errDesc;
}
const char* ceCount::what() const noexcept
{
    return errWhat;
}
const char* ueCount::name() const noexcept
{
    return errName;
}
const char* ueCount::description() const noexcept
{
    return errDesc;
}
const char* ueCount::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace MemoryECC
} // namespace Memory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

