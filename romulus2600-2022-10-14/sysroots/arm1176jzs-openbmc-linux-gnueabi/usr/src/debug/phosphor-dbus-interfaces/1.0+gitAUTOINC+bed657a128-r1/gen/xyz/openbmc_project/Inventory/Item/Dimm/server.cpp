#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Dimm/server.hpp>
















namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Dimm::Dimm(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Dimm_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Dimm::Dimm(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Dimm(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Dimm::memoryDataWidth() const ->
        uint16_t
{
    return _memoryDataWidth;
}

int Dimm::_callback_get_MemoryDataWidth(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryDataWidth();
                    }
                ));
    }
}

auto Dimm::memoryDataWidth(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_memoryDataWidth != value)
    {
        _memoryDataWidth = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryDataWidth");
        }
    }

    return _memoryDataWidth;
}

auto Dimm::memoryDataWidth(uint16_t val) ->
        uint16_t
{
    return memoryDataWidth(val, false);
}

int Dimm::_callback_set_MemoryDataWidth(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->memoryDataWidth(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryDataWidth =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Dimm::memorySizeInKB() const ->
        size_t
{
    return _memorySizeInKB;
}

int Dimm::_callback_get_MemorySizeInKB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memorySizeInKB();
                    }
                ));
    }
}

auto Dimm::memorySizeInKB(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_memorySizeInKB != value)
    {
        _memorySizeInKB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemorySizeInKB");
        }
    }

    return _memorySizeInKB;
}

auto Dimm::memorySizeInKB(size_t val) ->
        size_t
{
    return memorySizeInKB(val, false);
}

int Dimm::_callback_set_MemorySizeInKB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->memorySizeInKB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemorySizeInKB =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto Dimm::memoryDeviceLocator() const ->
        std::string
{
    return _memoryDeviceLocator;
}

int Dimm::_callback_get_MemoryDeviceLocator(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryDeviceLocator();
                    }
                ));
    }
}

auto Dimm::memoryDeviceLocator(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_memoryDeviceLocator != value)
    {
        _memoryDeviceLocator = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryDeviceLocator");
        }
    }

    return _memoryDeviceLocator;
}

auto Dimm::memoryDeviceLocator(std::string val) ->
        std::string
{
    return memoryDeviceLocator(val, false);
}

int Dimm::_callback_set_MemoryDeviceLocator(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->memoryDeviceLocator(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryDeviceLocator =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Dimm::memoryType() const ->
        DeviceType
{
    return _memoryType;
}

int Dimm::_callback_get_MemoryType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryType();
                    }
                ));
    }
}

auto Dimm::memoryType(DeviceType value,
                                         bool skipSignal) ->
        DeviceType
{
    if (_memoryType != value)
    {
        _memoryType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryType");
        }
    }

    return _memoryType;
}

auto Dimm::memoryType(DeviceType val) ->
        DeviceType
{
    return memoryType(val, false);
}

int Dimm::_callback_set_MemoryType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](DeviceType&& arg)
                    {
                        o->memoryType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryType =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm::DeviceType>());
}
}

auto Dimm::memoryTypeDetail() const ->
        std::string
{
    return _memoryTypeDetail;
}

int Dimm::_callback_get_MemoryTypeDetail(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryTypeDetail();
                    }
                ));
    }
}

auto Dimm::memoryTypeDetail(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_memoryTypeDetail != value)
    {
        _memoryTypeDetail = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryTypeDetail");
        }
    }

    return _memoryTypeDetail;
}

auto Dimm::memoryTypeDetail(std::string val) ->
        std::string
{
    return memoryTypeDetail(val, false);
}

int Dimm::_callback_set_MemoryTypeDetail(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->memoryTypeDetail(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryTypeDetail =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Dimm::maxMemorySpeedInMhz() const ->
        uint16_t
{
    return _maxMemorySpeedInMhz;
}

int Dimm::_callback_get_MaxMemorySpeedInMhz(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxMemorySpeedInMhz();
                    }
                ));
    }
}

auto Dimm::maxMemorySpeedInMhz(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_maxMemorySpeedInMhz != value)
    {
        _maxMemorySpeedInMhz = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MaxMemorySpeedInMhz");
        }
    }

    return _maxMemorySpeedInMhz;
}

auto Dimm::maxMemorySpeedInMhz(uint16_t val) ->
        uint16_t
{
    return maxMemorySpeedInMhz(val, false);
}

int Dimm::_callback_set_MaxMemorySpeedInMhz(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->maxMemorySpeedInMhz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MaxMemorySpeedInMhz =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Dimm::memoryAttributes() const ->
        uint8_t
{
    return _memoryAttributes;
}

int Dimm::_callback_get_MemoryAttributes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryAttributes();
                    }
                ));
    }
}

auto Dimm::memoryAttributes(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_memoryAttributes != value)
    {
        _memoryAttributes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryAttributes");
        }
    }

    return _memoryAttributes;
}

auto Dimm::memoryAttributes(uint8_t val) ->
        uint8_t
{
    return memoryAttributes(val, false);
}

int Dimm::_callback_set_MemoryAttributes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->memoryAttributes(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryAttributes =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto Dimm::memoryConfiguredSpeedInMhz() const ->
        uint16_t
{
    return _memoryConfiguredSpeedInMhz;
}

int Dimm::_callback_get_MemoryConfiguredSpeedInMhz(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryConfiguredSpeedInMhz();
                    }
                ));
    }
}

auto Dimm::memoryConfiguredSpeedInMhz(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_memoryConfiguredSpeedInMhz != value)
    {
        _memoryConfiguredSpeedInMhz = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryConfiguredSpeedInMhz");
        }
    }

    return _memoryConfiguredSpeedInMhz;
}

auto Dimm::memoryConfiguredSpeedInMhz(uint16_t val) ->
        uint16_t
{
    return memoryConfiguredSpeedInMhz(val, false);
}

int Dimm::_callback_set_MemoryConfiguredSpeedInMhz(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->memoryConfiguredSpeedInMhz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryConfiguredSpeedInMhz =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Dimm::ecc() const ->
        Ecc
{
    return _ecc;
}

int Dimm::_callback_get_ECC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ecc();
                    }
                ));
    }
}

auto Dimm::ecc(Ecc value,
                                         bool skipSignal) ->
        Ecc
{
    if (_ecc != value)
    {
        _ecc = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("ECC");
        }
    }

    return _ecc;
}

auto Dimm::ecc(Ecc val) ->
        Ecc
{
    return ecc(val, false);
}

int Dimm::_callback_set_ECC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Ecc&& arg)
                    {
                        o->ecc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_ECC =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm::Ecc>());
}
}

auto Dimm::casLatencies() const ->
        uint16_t
{
    return _casLatencies;
}

int Dimm::_callback_get_CASLatencies(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->casLatencies();
                    }
                ));
    }
}

auto Dimm::casLatencies(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_casLatencies != value)
    {
        _casLatencies = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("CASLatencies");
        }
    }

    return _casLatencies;
}

auto Dimm::casLatencies(uint16_t val) ->
        uint16_t
{
    return casLatencies(val, false);
}

int Dimm::_callback_set_CASLatencies(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->casLatencies(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_CASLatencies =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Dimm::revisionCode() const ->
        uint16_t
{
    return _revisionCode;
}

int Dimm::_callback_get_RevisionCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->revisionCode();
                    }
                ));
    }
}

auto Dimm::revisionCode(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_revisionCode != value)
    {
        _revisionCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("RevisionCode");
        }
    }

    return _revisionCode;
}

auto Dimm::revisionCode(uint16_t val) ->
        uint16_t
{
    return revisionCode(val, false);
}

int Dimm::_callback_set_RevisionCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->revisionCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_RevisionCode =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Dimm::formFactor() const ->
        FormFactor
{
    return _formFactor;
}

int Dimm::_callback_get_FormFactor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->formFactor();
                    }
                ));
    }
}

auto Dimm::formFactor(FormFactor value,
                                         bool skipSignal) ->
        FormFactor
{
    if (_formFactor != value)
    {
        _formFactor = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("FormFactor");
        }
    }

    return _formFactor;
}

auto Dimm::formFactor(FormFactor val) ->
        FormFactor
{
    return formFactor(val, false);
}

int Dimm::_callback_set_FormFactor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](FormFactor&& arg)
                    {
                        o->formFactor(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_FormFactor =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm::FormFactor>());
}
}

auto Dimm::memoryTotalWidth() const ->
        uint16_t
{
    return _memoryTotalWidth;
}

int Dimm::_callback_get_MemoryTotalWidth(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryTotalWidth();
                    }
                ));
    }
}

auto Dimm::memoryTotalWidth(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_memoryTotalWidth != value)
    {
        _memoryTotalWidth = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryTotalWidth");
        }
    }

    return _memoryTotalWidth;
}

auto Dimm::memoryTotalWidth(uint16_t val) ->
        uint16_t
{
    return memoryTotalWidth(val, false);
}

int Dimm::_callback_set_MemoryTotalWidth(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->memoryTotalWidth(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryTotalWidth =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto Dimm::allowedSpeedsMT() const ->
        std::vector<uint16_t>
{
    return _allowedSpeedsMT;
}

int Dimm::_callback_get_AllowedSpeedsMT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->allowedSpeedsMT();
                    }
                ));
    }
}

auto Dimm::allowedSpeedsMT(std::vector<uint16_t> value,
                                         bool skipSignal) ->
        std::vector<uint16_t>
{
    if (_allowedSpeedsMT != value)
    {
        _allowedSpeedsMT = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("AllowedSpeedsMT");
        }
    }

    return _allowedSpeedsMT;
}

auto Dimm::allowedSpeedsMT(std::vector<uint16_t> val) ->
        std::vector<uint16_t>
{
    return allowedSpeedsMT(val, false);
}

int Dimm::_callback_set_AllowedSpeedsMT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint16_t>&& arg)
                    {
                        o->allowedSpeedsMT(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_AllowedSpeedsMT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint16_t>>());
}
}

auto Dimm::memoryMedia() const ->
        MemoryTech
{
    return _memoryMedia;
}

int Dimm::_callback_get_MemoryMedia(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryMedia();
                    }
                ));
    }
}

auto Dimm::memoryMedia(MemoryTech value,
                                         bool skipSignal) ->
        MemoryTech
{
    if (_memoryMedia != value)
    {
        _memoryMedia = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_interface.property_changed("MemoryMedia");
        }
    }

    return _memoryMedia;
}

auto Dimm::memoryMedia(MemoryTech val) ->
        MemoryTech
{
    return memoryMedia(val, false);
}

int Dimm::_callback_set_MemoryMedia(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Dimm*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](MemoryTech&& arg)
                    {
                        o->memoryMedia(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Dimm
{
static const auto _property_MemoryMedia =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Inventory::Item::server::Dimm::MemoryTech>());
}
}

void Dimm::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "MemoryDataWidth")
    {
        auto& v = std::get<uint16_t>(val);
        memoryDataWidth(v, skipSignal);
        return;
    }
    if (_name == "MemorySizeInKB")
    {
        auto& v = std::get<size_t>(val);
        memorySizeInKB(v, skipSignal);
        return;
    }
    if (_name == "MemoryDeviceLocator")
    {
        auto& v = std::get<std::string>(val);
        memoryDeviceLocator(v, skipSignal);
        return;
    }
    if (_name == "MemoryType")
    {
        auto& v = std::get<DeviceType>(val);
        memoryType(v, skipSignal);
        return;
    }
    if (_name == "MemoryTypeDetail")
    {
        auto& v = std::get<std::string>(val);
        memoryTypeDetail(v, skipSignal);
        return;
    }
    if (_name == "MaxMemorySpeedInMhz")
    {
        auto& v = std::get<uint16_t>(val);
        maxMemorySpeedInMhz(v, skipSignal);
        return;
    }
    if (_name == "MemoryAttributes")
    {
        auto& v = std::get<uint8_t>(val);
        memoryAttributes(v, skipSignal);
        return;
    }
    if (_name == "MemoryConfiguredSpeedInMhz")
    {
        auto& v = std::get<uint16_t>(val);
        memoryConfiguredSpeedInMhz(v, skipSignal);
        return;
    }
    if (_name == "ECC")
    {
        auto& v = std::get<Ecc>(val);
        ecc(v, skipSignal);
        return;
    }
    if (_name == "CASLatencies")
    {
        auto& v = std::get<uint16_t>(val);
        casLatencies(v, skipSignal);
        return;
    }
    if (_name == "RevisionCode")
    {
        auto& v = std::get<uint16_t>(val);
        revisionCode(v, skipSignal);
        return;
    }
    if (_name == "FormFactor")
    {
        auto& v = std::get<FormFactor>(val);
        formFactor(v, skipSignal);
        return;
    }
    if (_name == "MemoryTotalWidth")
    {
        auto& v = std::get<uint16_t>(val);
        memoryTotalWidth(v, skipSignal);
        return;
    }
    if (_name == "AllowedSpeedsMT")
    {
        auto& v = std::get<std::vector<uint16_t>>(val);
        allowedSpeedsMT(v, skipSignal);
        return;
    }
    if (_name == "MemoryMedia")
    {
        auto& v = std::get<MemoryTech>(val);
        memoryMedia(v, skipSignal);
        return;
    }
}

auto Dimm::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "MemoryDataWidth")
    {
        return memoryDataWidth();
    }
    if (_name == "MemorySizeInKB")
    {
        return memorySizeInKB();
    }
    if (_name == "MemoryDeviceLocator")
    {
        return memoryDeviceLocator();
    }
    if (_name == "MemoryType")
    {
        return memoryType();
    }
    if (_name == "MemoryTypeDetail")
    {
        return memoryTypeDetail();
    }
    if (_name == "MaxMemorySpeedInMhz")
    {
        return maxMemorySpeedInMhz();
    }
    if (_name == "MemoryAttributes")
    {
        return memoryAttributes();
    }
    if (_name == "MemoryConfiguredSpeedInMhz")
    {
        return memoryConfiguredSpeedInMhz();
    }
    if (_name == "ECC")
    {
        return ecc();
    }
    if (_name == "CASLatencies")
    {
        return casLatencies();
    }
    if (_name == "RevisionCode")
    {
        return revisionCode();
    }
    if (_name == "FormFactor")
    {
        return formFactor();
    }
    if (_name == "MemoryTotalWidth")
    {
        return memoryTotalWidth();
    }
    if (_name == "AllowedSpeedsMT")
    {
        return allowedSpeedsMT();
    }
    if (_name == "MemoryMedia")
    {
        return memoryMedia();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Dimm::Ecc */
static const std::tuple<const char*, Dimm::Ecc> mappingDimmEcc[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.Ecc.NoECC",                 Dimm::Ecc::NoECC ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.Ecc.SingleBitECC",                 Dimm::Ecc::SingleBitECC ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.Ecc.MultiBitECC",                 Dimm::Ecc::MultiBitECC ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.Ecc.AddressParity",                 Dimm::Ecc::AddressParity ),
        };

} // anonymous namespace

auto Dimm::convertStringToEcc(const std::string& s) noexcept ->
        std::optional<Ecc>
{
    auto i = std::find_if(
            std::begin(mappingDimmEcc),
            std::end(mappingDimmEcc),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDimmEcc) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Dimm::convertEccFromString(const std::string& s) ->
        Ecc
{
    auto r = convertStringToEcc(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Dimm::convertEccToString(Dimm::Ecc v)
{
    auto i = std::find_if(
            std::begin(mappingDimmEcc),
            std::end(mappingDimmEcc),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDimmEcc))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Dimm::MemoryTech */
static const std::tuple<const char*, Dimm::MemoryTech> mappingDimmMemoryTech[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.Other",                 Dimm::MemoryTech::Other ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.Unknown",                 Dimm::MemoryTech::Unknown ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.DRAM",                 Dimm::MemoryTech::DRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.NVDIMM_N",                 Dimm::MemoryTech::NVDIMM_N ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.NVDIMM_F",                 Dimm::MemoryTech::NVDIMM_F ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.NVDIMM_P",                 Dimm::MemoryTech::NVDIMM_P ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.MemoryTech.IntelOptane",                 Dimm::MemoryTech::IntelOptane ),
        };

} // anonymous namespace

auto Dimm::convertStringToMemoryTech(const std::string& s) noexcept ->
        std::optional<MemoryTech>
{
    auto i = std::find_if(
            std::begin(mappingDimmMemoryTech),
            std::end(mappingDimmMemoryTech),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDimmMemoryTech) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Dimm::convertMemoryTechFromString(const std::string& s) ->
        MemoryTech
{
    auto r = convertStringToMemoryTech(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Dimm::convertMemoryTechToString(Dimm::MemoryTech v)
{
    auto i = std::find_if(
            std::begin(mappingDimmMemoryTech),
            std::end(mappingDimmMemoryTech),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDimmMemoryTech))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Dimm::DeviceType */
static const std::tuple<const char*, Dimm::DeviceType> mappingDimmDeviceType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.Other",                 Dimm::DeviceType::Other ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.Unknown",                 Dimm::DeviceType::Unknown ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DRAM",                 Dimm::DeviceType::DRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.EDRAM",                 Dimm::DeviceType::EDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.VRAM",                 Dimm::DeviceType::VRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.SRAM",                 Dimm::DeviceType::SRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.RAM",                 Dimm::DeviceType::RAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.ROM",                 Dimm::DeviceType::ROM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.FLASH",                 Dimm::DeviceType::FLASH ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.EEPROM",                 Dimm::DeviceType::EEPROM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.FEPROM",                 Dimm::DeviceType::FEPROM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.EPROM",                 Dimm::DeviceType::EPROM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.CDRAM",                 Dimm::DeviceType::CDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.ThreeDRAM",                 Dimm::DeviceType::ThreeDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.SDRAM",                 Dimm::DeviceType::SDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR_SGRAM",                 Dimm::DeviceType::DDR_SGRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.RDRAM",                 Dimm::DeviceType::RDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR",                 Dimm::DeviceType::DDR ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR2",                 Dimm::DeviceType::DDR2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR2_SDRAM_FB_DIMM",                 Dimm::DeviceType::DDR2_SDRAM_FB_DIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.EDO",                 Dimm::DeviceType::EDO ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.FastPageMode",                 Dimm::DeviceType::FastPageMode ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.PipelinedNibble",                 Dimm::DeviceType::PipelinedNibble ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR3",                 Dimm::DeviceType::DDR3 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.FBD2",                 Dimm::DeviceType::FBD2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR4",                 Dimm::DeviceType::DDR4 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.LPDDR_SDRAM",                 Dimm::DeviceType::LPDDR_SDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.LPDDR2_SDRAM",                 Dimm::DeviceType::LPDDR2_SDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.LPDDR3_SDRAM",                 Dimm::DeviceType::LPDDR3_SDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.LPDDR4_SDRAM",                 Dimm::DeviceType::LPDDR4_SDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.Logical",                 Dimm::DeviceType::Logical ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.HBM",                 Dimm::DeviceType::HBM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.HBM2",                 Dimm::DeviceType::HBM2 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.HBM3",                 Dimm::DeviceType::HBM3 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR2_SDRAM_FB_DIMM_PROB",                 Dimm::DeviceType::DDR2_SDRAM_FB_DIMM_PROB ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR4E_SDRAM",                 Dimm::DeviceType::DDR4E_SDRAM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.DDR5",                 Dimm::DeviceType::DDR5 ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.DeviceType.LPDDR5_SDRAM",                 Dimm::DeviceType::LPDDR5_SDRAM ),
        };

} // anonymous namespace

auto Dimm::convertStringToDeviceType(const std::string& s) noexcept ->
        std::optional<DeviceType>
{
    auto i = std::find_if(
            std::begin(mappingDimmDeviceType),
            std::end(mappingDimmDeviceType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDimmDeviceType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Dimm::convertDeviceTypeFromString(const std::string& s) ->
        DeviceType
{
    auto r = convertStringToDeviceType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Dimm::convertDeviceTypeToString(Dimm::DeviceType v)
{
    auto i = std::find_if(
            std::begin(mappingDimmDeviceType),
            std::end(mappingDimmDeviceType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDimmDeviceType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Dimm::FormFactor */
static const std::tuple<const char*, Dimm::FormFactor> mappingDimmFormFactor[] =
        {
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.RDIMM",                 Dimm::FormFactor::RDIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.UDIMM",                 Dimm::FormFactor::UDIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.SO_DIMM",                 Dimm::FormFactor::SO_DIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.LRDIMM",                 Dimm::FormFactor::LRDIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.Mini_RDIMM",                 Dimm::FormFactor::Mini_RDIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.Mini_UDIMM",                 Dimm::FormFactor::Mini_UDIMM ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.SO_RDIMM_72b",                 Dimm::FormFactor::SO_RDIMM_72b ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.SO_UDIMM_72b",                 Dimm::FormFactor::SO_UDIMM_72b ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.SO_DIMM_16b",                 Dimm::FormFactor::SO_DIMM_16b ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.SO_DIMM_32b",                 Dimm::FormFactor::SO_DIMM_32b ),
            std::make_tuple( "xyz.openbmc_project.Inventory.Item.Dimm.FormFactor.Die",                 Dimm::FormFactor::Die ),
        };

} // anonymous namespace

auto Dimm::convertStringToFormFactor(const std::string& s) noexcept ->
        std::optional<FormFactor>
{
    auto i = std::find_if(
            std::begin(mappingDimmFormFactor),
            std::end(mappingDimmFormFactor),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingDimmFormFactor) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Dimm::convertFormFactorFromString(const std::string& s) ->
        FormFactor
{
    auto r = convertStringToFormFactor(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Dimm::convertFormFactorToString(Dimm::FormFactor v)
{
    auto i = std::find_if(
            std::begin(mappingDimmFormFactor),
            std::end(mappingDimmFormFactor),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingDimmFormFactor))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Dimm::_vtable[] = {
    vtable::start(),
    vtable::property("MemoryDataWidth",
                     details::Dimm::_property_MemoryDataWidth
                        .data(),
                     _callback_get_MemoryDataWidth,
                     _callback_set_MemoryDataWidth,
                     vtable::property_::emits_change),
    vtable::property("MemorySizeInKB",
                     details::Dimm::_property_MemorySizeInKB
                        .data(),
                     _callback_get_MemorySizeInKB,
                     _callback_set_MemorySizeInKB,
                     vtable::property_::emits_change),
    vtable::property("MemoryDeviceLocator",
                     details::Dimm::_property_MemoryDeviceLocator
                        .data(),
                     _callback_get_MemoryDeviceLocator,
                     _callback_set_MemoryDeviceLocator,
                     vtable::property_::emits_change),
    vtable::property("MemoryType",
                     details::Dimm::_property_MemoryType
                        .data(),
                     _callback_get_MemoryType,
                     _callback_set_MemoryType,
                     vtable::property_::emits_change),
    vtable::property("MemoryTypeDetail",
                     details::Dimm::_property_MemoryTypeDetail
                        .data(),
                     _callback_get_MemoryTypeDetail,
                     _callback_set_MemoryTypeDetail,
                     vtable::property_::emits_change),
    vtable::property("MaxMemorySpeedInMhz",
                     details::Dimm::_property_MaxMemorySpeedInMhz
                        .data(),
                     _callback_get_MaxMemorySpeedInMhz,
                     _callback_set_MaxMemorySpeedInMhz,
                     vtable::property_::emits_change),
    vtable::property("MemoryAttributes",
                     details::Dimm::_property_MemoryAttributes
                        .data(),
                     _callback_get_MemoryAttributes,
                     _callback_set_MemoryAttributes,
                     vtable::property_::emits_change),
    vtable::property("MemoryConfiguredSpeedInMhz",
                     details::Dimm::_property_MemoryConfiguredSpeedInMhz
                        .data(),
                     _callback_get_MemoryConfiguredSpeedInMhz,
                     _callback_set_MemoryConfiguredSpeedInMhz,
                     vtable::property_::emits_change),
    vtable::property("ECC",
                     details::Dimm::_property_ECC
                        .data(),
                     _callback_get_ECC,
                     _callback_set_ECC,
                     vtable::property_::emits_change),
    vtable::property("CASLatencies",
                     details::Dimm::_property_CASLatencies
                        .data(),
                     _callback_get_CASLatencies,
                     _callback_set_CASLatencies,
                     vtable::property_::emits_change),
    vtable::property("RevisionCode",
                     details::Dimm::_property_RevisionCode
                        .data(),
                     _callback_get_RevisionCode,
                     _callback_set_RevisionCode,
                     vtable::property_::emits_change),
    vtable::property("FormFactor",
                     details::Dimm::_property_FormFactor
                        .data(),
                     _callback_get_FormFactor,
                     _callback_set_FormFactor,
                     vtable::property_::emits_change),
    vtable::property("MemoryTotalWidth",
                     details::Dimm::_property_MemoryTotalWidth
                        .data(),
                     _callback_get_MemoryTotalWidth,
                     _callback_set_MemoryTotalWidth,
                     vtable::property_::emits_change),
    vtable::property("AllowedSpeedsMT",
                     details::Dimm::_property_AllowedSpeedsMT
                        .data(),
                     _callback_get_AllowedSpeedsMT,
                     _callback_set_AllowedSpeedsMT,
                     vtable::property_::emits_change),
    vtable::property("MemoryMedia",
                     details::Dimm::_property_MemoryMedia
                        .data(),
                     _callback_get_MemoryMedia,
                     _callback_set_MemoryMedia,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

