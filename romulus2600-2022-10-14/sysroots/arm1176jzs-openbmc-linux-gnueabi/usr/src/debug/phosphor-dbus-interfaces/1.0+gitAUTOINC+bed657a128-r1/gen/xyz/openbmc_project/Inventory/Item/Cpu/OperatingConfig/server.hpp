#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>








#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Cpu
{
namespace server
{

class OperatingConfig
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        OperatingConfig() = delete;
        OperatingConfig(const OperatingConfig&) = delete;
        OperatingConfig& operator=(const OperatingConfig&) = delete;
        OperatingConfig(OperatingConfig&&) = delete;
        OperatingConfig& operator=(OperatingConfig&&) = delete;
        virtual ~OperatingConfig() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        OperatingConfig(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                size_t,
                std::vector<std::tuple<uint32_t, size_t>>,
                std::vector<std::tuple<uint32_t, std::vector<uint32_t>>>,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        OperatingConfig(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of BaseSpeed */
        virtual uint32_t baseSpeed() const;
        /** Set value of BaseSpeed with option to skip sending signal */
        virtual uint32_t baseSpeed(uint32_t value,
               bool skipSignal);
        /** Set value of BaseSpeed */
        virtual uint32_t baseSpeed(uint32_t value);
        /** Get value of BaseSpeedPrioritySettings */
        virtual std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> baseSpeedPrioritySettings() const;
        /** Set value of BaseSpeedPrioritySettings with option to skip sending signal */
        virtual std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> baseSpeedPrioritySettings(std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> value,
               bool skipSignal);
        /** Set value of BaseSpeedPrioritySettings */
        virtual std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> baseSpeedPrioritySettings(std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> value);
        /** Get value of MaxJunctionTemperature */
        virtual uint32_t maxJunctionTemperature() const;
        /** Set value of MaxJunctionTemperature with option to skip sending signal */
        virtual uint32_t maxJunctionTemperature(uint32_t value,
               bool skipSignal);
        /** Set value of MaxJunctionTemperature */
        virtual uint32_t maxJunctionTemperature(uint32_t value);
        /** Get value of MaxSpeed */
        virtual uint32_t maxSpeed() const;
        /** Set value of MaxSpeed with option to skip sending signal */
        virtual uint32_t maxSpeed(uint32_t value,
               bool skipSignal);
        /** Set value of MaxSpeed */
        virtual uint32_t maxSpeed(uint32_t value);
        /** Get value of PowerLimit */
        virtual uint32_t powerLimit() const;
        /** Set value of PowerLimit with option to skip sending signal */
        virtual uint32_t powerLimit(uint32_t value,
               bool skipSignal);
        /** Set value of PowerLimit */
        virtual uint32_t powerLimit(uint32_t value);
        /** Get value of AvailableCoreCount */
        virtual size_t availableCoreCount() const;
        /** Set value of AvailableCoreCount with option to skip sending signal */
        virtual size_t availableCoreCount(size_t value,
               bool skipSignal);
        /** Set value of AvailableCoreCount */
        virtual size_t availableCoreCount(size_t value);
        /** Get value of TurboProfile */
        virtual std::vector<std::tuple<uint32_t, size_t>> turboProfile() const;
        /** Set value of TurboProfile with option to skip sending signal */
        virtual std::vector<std::tuple<uint32_t, size_t>> turboProfile(std::vector<std::tuple<uint32_t, size_t>> value,
               bool skipSignal);
        /** Set value of TurboProfile */
        virtual std::vector<std::tuple<uint32_t, size_t>> turboProfile(std::vector<std::tuple<uint32_t, size_t>> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.Cpu.OperatingConfig";

    private:

        /** @brief sd-bus callback for get-property 'BaseSpeed' */
        static int _callback_get_BaseSpeed(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'BaseSpeedPrioritySettings' */
        static int _callback_get_BaseSpeedPrioritySettings(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxJunctionTemperature' */
        static int _callback_get_MaxJunctionTemperature(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxSpeed' */
        static int _callback_get_MaxSpeed(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PowerLimit' */
        static int _callback_get_PowerLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AvailableCoreCount' */
        static int _callback_get_AvailableCoreCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TurboProfile' */
        static int _callback_get_TurboProfile(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface;
        sdbusplus::SdBusInterface *_intf;

        uint32_t _baseSpeed{};
        std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> _baseSpeedPrioritySettings{};
        uint32_t _maxJunctionTemperature{};
        uint32_t _maxSpeed{};
        uint32_t _powerLimit{};
        size_t _availableCoreCount{};
        std::vector<std::tuple<uint32_t, size_t>> _turboProfile{};

};


} // namespace server
} // namespace Cpu
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

