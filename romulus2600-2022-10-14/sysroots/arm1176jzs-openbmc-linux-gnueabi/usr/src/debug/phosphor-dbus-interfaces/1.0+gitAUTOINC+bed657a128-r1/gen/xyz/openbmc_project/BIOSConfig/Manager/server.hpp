#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>






#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace BIOSConfig
{
namespace server
{

class Manager
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Manager() = delete;
        Manager(const Manager&) = delete;
        Manager& operator=(const Manager&) = delete;
        Manager(Manager&&) = delete;
        Manager& operator=(Manager&&) = delete;
        virtual ~Manager() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Manager(bus_t& bus, const char* path);

        enum class AttributeType
        {
            Enumeration,
            String,
            Password,
            Integer,
            Boolean,
        };
        enum class ResetFlag
        {
            NoAction,
            FactoryDefaults,
            FailSafeDefaults,
        };
        enum class BoundType
        {
            LowerBound,
            UpperBound,
            ScalarIncrement,
            MinStringLength,
            MaxStringLength,
            OneOf,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                ResetFlag,
                std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>>,
                std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Manager(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for SetAttribute
         *  To set the new value on existing attribute name. It will create or add the pending attributes.
         *
         *  @param[in] attributeName - AttributeName which has to be changed.
         *  @param[in] attributeValue - New attribute value
         */
        virtual void setAttribute(
            std::string attributeName,
            std::variant<int64_t, std::string> attributeValue) = 0;

        /** @brief Implementation for GetAttribute
         *  To get the attribute value of existing attributes.
         *
         *  @param[in] attributeName - To get the bios attribute current values and pending values if previously set by setAttribute or SetPendingAttributes.
         *
         *  @return attributeValueType[AttributeType] - PLDM attribute Type present in PLDM spec
         *  @return currentValue[std::variant<int64_t, std::string>] - The attribute current value.
         *  @return pendingValue[std::variant<int64_t, std::string>] - The pending attribute value if its available.
         */
        virtual std::tuple<AttributeType, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>> getAttribute(
            std::string attributeName) = 0;


        /** Get value of ResetBIOSSettings */
        virtual ResetFlag resetBIOSSettings() const;
        /** Set value of ResetBIOSSettings with option to skip sending signal */
        virtual ResetFlag resetBIOSSettings(ResetFlag value,
               bool skipSignal);
        /** Set value of ResetBIOSSettings */
        virtual ResetFlag resetBIOSSettings(ResetFlag value);
        /** Get value of BaseBIOSTable */
        virtual std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> baseBIOSTable() const;
        /** Set value of BaseBIOSTable with option to skip sending signal */
        virtual std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> baseBIOSTable(std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> value,
               bool skipSignal);
        /** Set value of BaseBIOSTable */
        virtual std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> baseBIOSTable(std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> value);
        /** Get value of PendingAttributes */
        virtual std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> pendingAttributes() const;
        /** Set value of PendingAttributes with option to skip sending signal */
        virtual std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> pendingAttributes(std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> value,
               bool skipSignal);
        /** Set value of PendingAttributes */
        virtual std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> pendingAttributes(std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static AttributeType convertAttributeTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<AttributeType> convertStringToAttributeType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         */
        static std::string convertAttributeTypeToString(AttributeType e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ResetFlag convertResetFlagFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ResetFlag> convertStringToResetFlag(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         */
        static std::string convertResetFlagToString(ResetFlag e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static BoundType convertBoundTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<BoundType> convertStringToBoundType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.BIOSConfig.Manager.<value name>"
         */
        static std::string convertBoundTypeToString(BoundType e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_BIOSConfig_Manager_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_BIOSConfig_Manager_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.BIOSConfig.Manager";

    private:

        /** @brief sd-bus callback for SetAttribute
         */
        static int _callback_SetAttribute(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetAttribute
         */
        static int _callback_GetAttribute(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ResetBIOSSettings' */
        static int _callback_get_ResetBIOSSettings(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ResetBIOSSettings' */
        static int _callback_set_ResetBIOSSettings(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'BaseBIOSTable' */
        static int _callback_get_BaseBIOSTable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'BaseBIOSTable' */
        static int _callback_set_BaseBIOSTable(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PendingAttributes' */
        static int _callback_get_PendingAttributes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PendingAttributes' */
        static int _callback_set_PendingAttributes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_BIOSConfig_Manager_interface;
        sdbusplus::SdBusInterface *_intf;

        ResetFlag _resetBIOSSettings = ResetFlag::NoAction;
        std::map<std::string, std::tuple<AttributeType, bool, std::string, std::string, std::string, std::variant<int64_t, std::string>, std::variant<int64_t, std::string>, std::vector<std::tuple<BoundType, std::variant<int64_t, std::string>>>>> _baseBIOSTable{};
        std::map<std::string, std::tuple<AttributeType, std::variant<int64_t, std::string>>> _pendingAttributes{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Manager::AttributeType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Manager::AttributeType e)
{
    return Manager::convertAttributeTypeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Manager::ResetFlag.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Manager::ResetFlag e)
{
    return Manager::convertResetFlagToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Manager::BoundType.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Manager::BoundType e)
{
    return Manager::convertBoundTypeToString(e);
}

} // namespace server
} // namespace BIOSConfig
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::BIOSConfig::server::Manager::AttributeType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::BIOSConfig::server::Manager::convertStringToAttributeType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::BIOSConfig::server::Manager::AttributeType>
{
    static std::string op(xyz::openbmc_project::BIOSConfig::server::Manager::AttributeType value)
    {
        return xyz::openbmc_project::BIOSConfig::server::Manager::convertAttributeTypeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::BIOSConfig::server::Manager::ResetFlag>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::BIOSConfig::server::Manager::convertStringToResetFlag(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::BIOSConfig::server::Manager::ResetFlag>
{
    static std::string op(xyz::openbmc_project::BIOSConfig::server::Manager::ResetFlag value)
    {
        return xyz::openbmc_project::BIOSConfig::server::Manager::convertResetFlagToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::BIOSConfig::server::Manager::BoundType>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::BIOSConfig::server::Manager::convertStringToBoundType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::BIOSConfig::server::Manager::BoundType>
{
    static std::string op(xyz::openbmc_project::BIOSConfig::server::Manager::BoundType value)
    {
        return xyz::openbmc_project::BIOSConfig::server::Manager::convertBoundTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

