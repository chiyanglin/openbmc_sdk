#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Software/ExtendedVersion/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Software
{
namespace server
{

ExtendedVersion::ExtendedVersion(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Software_ExtendedVersion_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ExtendedVersion::ExtendedVersion(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ExtendedVersion(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ExtendedVersion::extendedVersion() const ->
        std::string
{
    return _extendedVersion;
}

int ExtendedVersion::_callback_get_ExtendedVersion(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ExtendedVersion*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->extendedVersion();
                    }
                ));
    }
}

auto ExtendedVersion::extendedVersion(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_extendedVersion != value)
    {
        _extendedVersion = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Software_ExtendedVersion_interface.property_changed("ExtendedVersion");
        }
    }

    return _extendedVersion;
}

auto ExtendedVersion::extendedVersion(std::string val) ->
        std::string
{
    return extendedVersion(val, false);
}

int ExtendedVersion::_callback_set_ExtendedVersion(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ExtendedVersion*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->extendedVersion(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ExtendedVersion
{
static const auto _property_ExtendedVersion =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void ExtendedVersion::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ExtendedVersion")
    {
        auto& v = std::get<std::string>(val);
        extendedVersion(v, skipSignal);
        return;
    }
}

auto ExtendedVersion::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ExtendedVersion")
    {
        return extendedVersion();
    }

    return PropertiesVariant();
}


const vtable_t ExtendedVersion::_vtable[] = {
    vtable::start(),
    vtable::property("ExtendedVersion",
                     details::ExtendedVersion::_property_ExtendedVersion
                        .data(),
                     _callback_get_ExtendedVersion,
                     _callback_set_ExtendedVersion,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Software
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

