#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/OperatingSystem/Status/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace OperatingSystem
{
namespace server
{

Status::Status(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_OperatingSystem_Status_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Status::Status(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Status(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Status::operatingSystemState() const ->
        OSStatus
{
    return _operatingSystemState;
}

int Status::_callback_get_OperatingSystemState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->operatingSystemState();
                    }
                ));
    }
}

auto Status::operatingSystemState(OSStatus value,
                                         bool skipSignal) ->
        OSStatus
{
    if (_operatingSystemState != value)
    {
        _operatingSystemState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_OperatingSystem_Status_interface.property_changed("OperatingSystemState");
        }
    }

    return _operatingSystemState;
}

auto Status::operatingSystemState(OSStatus val) ->
        OSStatus
{
    return operatingSystemState(val, false);
}

int Status::_callback_set_OperatingSystemState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Status*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](OSStatus&& arg)
                    {
                        o->operatingSystemState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Status
{
static const auto _property_OperatingSystemState =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::OperatingSystem::server::Status::OSStatus>());
}
}

void Status::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "OperatingSystemState")
    {
        auto& v = std::get<OSStatus>(val);
        operatingSystemState(v, skipSignal);
        return;
    }
}

auto Status::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "OperatingSystemState")
    {
        return operatingSystemState();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Status::OSStatus */
static const std::tuple<const char*, Status::OSStatus> mappingStatusOSStatus[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.CBoot",                 Status::OSStatus::CBoot ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.PXEBoot",                 Status::OSStatus::PXEBoot ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.DiagBoot",                 Status::OSStatus::DiagBoot ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.CDROMBoot",                 Status::OSStatus::CDROMBoot ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.ROMBoot",                 Status::OSStatus::ROMBoot ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.BootComplete",                 Status::OSStatus::BootComplete ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.Inactive",                 Status::OSStatus::Inactive ),
            std::make_tuple( "xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.Standby",                 Status::OSStatus::Standby ),
        };

} // anonymous namespace

auto Status::convertStringToOSStatus(const std::string& s) noexcept ->
        std::optional<OSStatus>
{
    auto i = std::find_if(
            std::begin(mappingStatusOSStatus),
            std::end(mappingStatusOSStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingStatusOSStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Status::convertOSStatusFromString(const std::string& s) ->
        OSStatus
{
    auto r = convertStringToOSStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Status::convertOSStatusToString(Status::OSStatus v)
{
    auto i = std::find_if(
            std::begin(mappingStatusOSStatus),
            std::end(mappingStatusOSStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingStatusOSStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Status::_vtable[] = {
    vtable::start(),
    vtable::property("OperatingSystemState",
                     details::Status::_property_OperatingSystemState
                        .data(),
                     _callback_get_OperatingSystemState,
                     _callback_set_OperatingSystemState,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace OperatingSystem
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

