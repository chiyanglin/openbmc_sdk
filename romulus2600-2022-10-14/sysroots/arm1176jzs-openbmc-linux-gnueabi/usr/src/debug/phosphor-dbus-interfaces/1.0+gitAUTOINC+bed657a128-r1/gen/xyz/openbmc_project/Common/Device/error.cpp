#include <xyz/openbmc_project/Common/Device/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace Device
{
namespace Error
{
const char* ReadFailure::name() const noexcept
{
    return errName;
}
const char* ReadFailure::description() const noexcept
{
    return errDesc;
}
const char* ReadFailure::what() const noexcept
{
    return errWhat;
}
const char* WriteFailure::name() const noexcept
{
    return errName;
}
const char* WriteFailure::description() const noexcept
{
    return errDesc;
}
const char* WriteFailure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Device
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

