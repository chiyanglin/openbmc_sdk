#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/Entry/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

Entry::Entry(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_Entry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entry::Entry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Entry::clientCertificate() const ->
        std::string
{
    return _clientCertificate;
}

int Entry::_callback_get_ClientCertificate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->clientCertificate();
                    }
                ));
    }
}

auto Entry::clientCertificate(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_clientCertificate != value)
    {
        _clientCertificate = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Entry_interface.property_changed("ClientCertificate");
        }
    }

    return _clientCertificate;
}

auto Entry::clientCertificate(std::string val) ->
        std::string
{
    return clientCertificate(val, false);
}

int Entry::_callback_set_ClientCertificate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->clientCertificate(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_ClientCertificate =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Entry::status() const ->
        State
{
    return _status;
}

int Entry::_callback_get_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->status();
                    }
                ));
    }
}

auto Entry::status(State value,
                                         bool skipSignal) ->
        State
{
    if (_status != value)
    {
        _status = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Entry_interface.property_changed("Status");
        }
    }

    return _status;
}

auto Entry::status(State val) ->
        State
{
    return status(val, false);
}

int Entry::_callback_set_Status(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](State&& arg)
                    {
                        o->status(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Status =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Certs::server::Entry::State>());
}
}

void Entry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ClientCertificate")
    {
        auto& v = std::get<std::string>(val);
        clientCertificate(v, skipSignal);
        return;
    }
    if (_name == "Status")
    {
        auto& v = std::get<State>(val);
        status(v, skipSignal);
        return;
    }
}

auto Entry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ClientCertificate")
    {
        return clientCertificate();
    }
    if (_name == "Status")
    {
        return status();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Entry::State */
static const std::tuple<const char*, Entry::State> mappingEntryState[] =
        {
            std::make_tuple( "xyz.openbmc_project.Certs.Entry.State.Pending",                 Entry::State::Pending ),
            std::make_tuple( "xyz.openbmc_project.Certs.Entry.State.BadCSR",                 Entry::State::BadCSR ),
            std::make_tuple( "xyz.openbmc_project.Certs.Entry.State.Complete",                 Entry::State::Complete ),
        };

} // anonymous namespace

auto Entry::convertStringToState(const std::string& s) noexcept ->
        std::optional<State>
{
    auto i = std::find_if(
            std::begin(mappingEntryState),
            std::end(mappingEntryState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingEntryState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Entry::convertStateFromString(const std::string& s) ->
        State
{
    auto r = convertStringToState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Entry::convertStateToString(Entry::State v)
{
    auto i = std::find_if(
            std::begin(mappingEntryState),
            std::end(mappingEntryState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingEntryState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Entry::_vtable[] = {
    vtable::start(),
    vtable::property("ClientCertificate",
                     details::Entry::_property_ClientCertificate
                        .data(),
                     _callback_get_ClientCertificate,
                     _callback_set_ClientCertificate,
                     vtable::property_::emits_change),
    vtable::property("Status",
                     details::Entry::_property_Status
                        .data(),
                     _callback_get_Status,
                     _callback_set_Status,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

