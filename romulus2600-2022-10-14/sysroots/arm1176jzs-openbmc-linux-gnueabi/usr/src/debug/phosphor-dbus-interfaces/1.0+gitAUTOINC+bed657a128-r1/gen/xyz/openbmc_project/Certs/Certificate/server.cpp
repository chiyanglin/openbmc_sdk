#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/Certificate/server.hpp>







namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

Certificate::Certificate(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_Certificate_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Certificate::Certificate(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Certificate(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Certificate::certificateString() const ->
        std::string
{
    return _certificateString;
}

int Certificate::_callback_get_CertificateString(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->certificateString();
                    }
                ));
    }
}

auto Certificate::certificateString(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_certificateString != value)
    {
        _certificateString = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Certificate_interface.property_changed("CertificateString");
        }
    }

    return _certificateString;
}

auto Certificate::certificateString(std::string val) ->
        std::string
{
    return certificateString(val, false);
}

int Certificate::_callback_set_CertificateString(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->certificateString(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Certificate
{
static const auto _property_CertificateString =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Certificate::keyUsage() const ->
        std::vector<std::string>
{
    return _keyUsage;
}

int Certificate::_callback_get_KeyUsage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->keyUsage();
                    }
                ));
    }
}

auto Certificate::keyUsage(std::vector<std::string> value,
                                         bool skipSignal) ->
        std::vector<std::string>
{
    if (_keyUsage != value)
    {
        _keyUsage = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Certificate_interface.property_changed("KeyUsage");
        }
    }

    return _keyUsage;
}

auto Certificate::keyUsage(std::vector<std::string> val) ->
        std::vector<std::string>
{
    return keyUsage(val, false);
}

int Certificate::_callback_set_KeyUsage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::string>&& arg)
                    {
                        o->keyUsage(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Certificate
{
static const auto _property_KeyUsage =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::string>>());
}
}

auto Certificate::issuer() const ->
        std::string
{
    return _issuer;
}

int Certificate::_callback_get_Issuer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->issuer();
                    }
                ));
    }
}

auto Certificate::issuer(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_issuer != value)
    {
        _issuer = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Certificate_interface.property_changed("Issuer");
        }
    }

    return _issuer;
}

auto Certificate::issuer(std::string val) ->
        std::string
{
    return issuer(val, false);
}

int Certificate::_callback_set_Issuer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->issuer(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Certificate
{
static const auto _property_Issuer =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Certificate::subject() const ->
        std::string
{
    return _subject;
}

int Certificate::_callback_get_Subject(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->subject();
                    }
                ));
    }
}

auto Certificate::subject(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_subject != value)
    {
        _subject = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Certificate_interface.property_changed("Subject");
        }
    }

    return _subject;
}

auto Certificate::subject(std::string val) ->
        std::string
{
    return subject(val, false);
}

int Certificate::_callback_set_Subject(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->subject(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Certificate
{
static const auto _property_Subject =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Certificate::validNotAfter() const ->
        uint64_t
{
    return _validNotAfter;
}

int Certificate::_callback_get_ValidNotAfter(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->validNotAfter();
                    }
                ));
    }
}

auto Certificate::validNotAfter(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_validNotAfter != value)
    {
        _validNotAfter = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Certificate_interface.property_changed("ValidNotAfter");
        }
    }

    return _validNotAfter;
}

auto Certificate::validNotAfter(uint64_t val) ->
        uint64_t
{
    return validNotAfter(val, false);
}

int Certificate::_callback_set_ValidNotAfter(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->validNotAfter(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Certificate
{
static const auto _property_ValidNotAfter =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Certificate::validNotBefore() const ->
        uint64_t
{
    return _validNotBefore;
}

int Certificate::_callback_get_ValidNotBefore(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->validNotBefore();
                    }
                ));
    }
}

auto Certificate::validNotBefore(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_validNotBefore != value)
    {
        _validNotBefore = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Certs_Certificate_interface.property_changed("ValidNotBefore");
        }
    }

    return _validNotBefore;
}

auto Certificate::validNotBefore(uint64_t val) ->
        uint64_t
{
    return validNotBefore(val, false);
}

int Certificate::_callback_set_ValidNotBefore(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Certificate*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->validNotBefore(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Certificate
{
static const auto _property_ValidNotBefore =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void Certificate::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CertificateString")
    {
        auto& v = std::get<std::string>(val);
        certificateString(v, skipSignal);
        return;
    }
    if (_name == "KeyUsage")
    {
        auto& v = std::get<std::vector<std::string>>(val);
        keyUsage(v, skipSignal);
        return;
    }
    if (_name == "Issuer")
    {
        auto& v = std::get<std::string>(val);
        issuer(v, skipSignal);
        return;
    }
    if (_name == "Subject")
    {
        auto& v = std::get<std::string>(val);
        subject(v, skipSignal);
        return;
    }
    if (_name == "ValidNotAfter")
    {
        auto& v = std::get<uint64_t>(val);
        validNotAfter(v, skipSignal);
        return;
    }
    if (_name == "ValidNotBefore")
    {
        auto& v = std::get<uint64_t>(val);
        validNotBefore(v, skipSignal);
        return;
    }
}

auto Certificate::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CertificateString")
    {
        return certificateString();
    }
    if (_name == "KeyUsage")
    {
        return keyUsage();
    }
    if (_name == "Issuer")
    {
        return issuer();
    }
    if (_name == "Subject")
    {
        return subject();
    }
    if (_name == "ValidNotAfter")
    {
        return validNotAfter();
    }
    if (_name == "ValidNotBefore")
    {
        return validNotBefore();
    }

    return PropertiesVariant();
}


const vtable_t Certificate::_vtable[] = {
    vtable::start(),
    vtable::property("CertificateString",
                     details::Certificate::_property_CertificateString
                        .data(),
                     _callback_get_CertificateString,
                     _callback_set_CertificateString,
                     vtable::property_::emits_change),
    vtable::property("KeyUsage",
                     details::Certificate::_property_KeyUsage
                        .data(),
                     _callback_get_KeyUsage,
                     _callback_set_KeyUsage,
                     vtable::property_::emits_change),
    vtable::property("Issuer",
                     details::Certificate::_property_Issuer
                        .data(),
                     _callback_get_Issuer,
                     _callback_set_Issuer,
                     vtable::property_::emits_change),
    vtable::property("Subject",
                     details::Certificate::_property_Subject
                        .data(),
                     _callback_get_Subject,
                     _callback_set_Subject,
                     vtable::property_::emits_change),
    vtable::property("ValidNotAfter",
                     details::Certificate::_property_ValidNotAfter
                        .data(),
                     _callback_get_ValidNotAfter,
                     _callback_set_ValidNotAfter,
                     vtable::property_::emits_change),
    vtable::property("ValidNotBefore",
                     details::Certificate::_property_ValidNotBefore
                        .data(),
                     _callback_get_ValidNotBefore,
                     _callback_set_ValidNotBefore,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

