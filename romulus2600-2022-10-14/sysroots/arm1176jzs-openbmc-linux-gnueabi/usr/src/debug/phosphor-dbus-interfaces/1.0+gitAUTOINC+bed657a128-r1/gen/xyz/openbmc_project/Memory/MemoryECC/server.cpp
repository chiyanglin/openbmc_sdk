#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Memory/MemoryECC/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Memory
{
namespace server
{

MemoryECC::MemoryECC(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Memory_MemoryECC_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MemoryECC::MemoryECC(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MemoryECC(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MemoryECC::isLoggingLimitReached() const ->
        bool
{
    return _isLoggingLimitReached;
}

int MemoryECC::_callback_get_isLoggingLimitReached(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->isLoggingLimitReached();
                    }
                ));
    }
}

auto MemoryECC::isLoggingLimitReached(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_isLoggingLimitReached != value)
    {
        _isLoggingLimitReached = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Memory_MemoryECC_interface.property_changed("isLoggingLimitReached");
        }
    }

    return _isLoggingLimitReached;
}

auto MemoryECC::isLoggingLimitReached(bool val) ->
        bool
{
    return isLoggingLimitReached(val, false);
}

int MemoryECC::_callback_set_isLoggingLimitReached(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->isLoggingLimitReached(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryECC
{
static const auto _property_isLoggingLimitReached =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto MemoryECC::ceCount() const ->
        int64_t
{
    return _ceCount;
}

int MemoryECC::_callback_get_ceCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ceCount();
                    }
                ));
    }
}

auto MemoryECC::ceCount(int64_t value,
                                         bool skipSignal) ->
        int64_t
{
    if (_ceCount != value)
    {
        _ceCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Memory_MemoryECC_interface.property_changed("ceCount");
        }
    }

    return _ceCount;
}

auto MemoryECC::ceCount(int64_t val) ->
        int64_t
{
    return ceCount(val, false);
}

int MemoryECC::_callback_set_ceCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](int64_t&& arg)
                    {
                        o->ceCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryECC
{
static const auto _property_ceCount =
    utility::tuple_to_array(message::types::type_id<
            int64_t>());
}
}

auto MemoryECC::ueCount() const ->
        int64_t
{
    return _ueCount;
}

int MemoryECC::_callback_get_ueCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ueCount();
                    }
                ));
    }
}

auto MemoryECC::ueCount(int64_t value,
                                         bool skipSignal) ->
        int64_t
{
    if (_ueCount != value)
    {
        _ueCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Memory_MemoryECC_interface.property_changed("ueCount");
        }
    }

    return _ueCount;
}

auto MemoryECC::ueCount(int64_t val) ->
        int64_t
{
    return ueCount(val, false);
}

int MemoryECC::_callback_set_ueCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](int64_t&& arg)
                    {
                        o->ueCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryECC
{
static const auto _property_ueCount =
    utility::tuple_to_array(message::types::type_id<
            int64_t>());
}
}

auto MemoryECC::state() const ->
        ECCStatus
{
    return _state;
}

int MemoryECC::_callback_get_state(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->state();
                    }
                ));
    }
}

auto MemoryECC::state(ECCStatus value,
                                         bool skipSignal) ->
        ECCStatus
{
    if (_state != value)
    {
        _state = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Memory_MemoryECC_interface.property_changed("state");
        }
    }

    return _state;
}

auto MemoryECC::state(ECCStatus val) ->
        ECCStatus
{
    return state(val, false);
}

int MemoryECC::_callback_set_state(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryECC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ECCStatus&& arg)
                    {
                        o->state(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryECC
{
static const auto _property_state =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Memory::server::MemoryECC::ECCStatus>());
}
}

void MemoryECC::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "isLoggingLimitReached")
    {
        auto& v = std::get<bool>(val);
        isLoggingLimitReached(v, skipSignal);
        return;
    }
    if (_name == "ceCount")
    {
        auto& v = std::get<int64_t>(val);
        ceCount(v, skipSignal);
        return;
    }
    if (_name == "ueCount")
    {
        auto& v = std::get<int64_t>(val);
        ueCount(v, skipSignal);
        return;
    }
    if (_name == "state")
    {
        auto& v = std::get<ECCStatus>(val);
        state(v, skipSignal);
        return;
    }
}

auto MemoryECC::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "isLoggingLimitReached")
    {
        return isLoggingLimitReached();
    }
    if (_name == "ceCount")
    {
        return ceCount();
    }
    if (_name == "ueCount")
    {
        return ueCount();
    }
    if (_name == "state")
    {
        return state();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for MemoryECC::ECCStatus */
static const std::tuple<const char*, MemoryECC::ECCStatus> mappingMemoryECCECCStatus[] =
        {
            std::make_tuple( "xyz.openbmc_project.Memory.MemoryECC.ECCStatus.ok",                 MemoryECC::ECCStatus::ok ),
            std::make_tuple( "xyz.openbmc_project.Memory.MemoryECC.ECCStatus.CE",                 MemoryECC::ECCStatus::CE ),
            std::make_tuple( "xyz.openbmc_project.Memory.MemoryECC.ECCStatus.UE",                 MemoryECC::ECCStatus::UE ),
            std::make_tuple( "xyz.openbmc_project.Memory.MemoryECC.ECCStatus.LogFull",                 MemoryECC::ECCStatus::LogFull ),
        };

} // anonymous namespace

auto MemoryECC::convertStringToECCStatus(const std::string& s) noexcept ->
        std::optional<ECCStatus>
{
    auto i = std::find_if(
            std::begin(mappingMemoryECCECCStatus),
            std::end(mappingMemoryECCECCStatus),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingMemoryECCECCStatus) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto MemoryECC::convertECCStatusFromString(const std::string& s) ->
        ECCStatus
{
    auto r = convertStringToECCStatus(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string MemoryECC::convertECCStatusToString(MemoryECC::ECCStatus v)
{
    auto i = std::find_if(
            std::begin(mappingMemoryECCECCStatus),
            std::end(mappingMemoryECCECCStatus),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingMemoryECCECCStatus))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t MemoryECC::_vtable[] = {
    vtable::start(),
    vtable::property("isLoggingLimitReached",
                     details::MemoryECC::_property_isLoggingLimitReached
                        .data(),
                     _callback_get_isLoggingLimitReached,
                     _callback_set_isLoggingLimitReached,
                     vtable::property_::emits_change),
    vtable::property("ceCount",
                     details::MemoryECC::_property_ceCount
                        .data(),
                     _callback_get_ceCount,
                     _callback_set_ceCount,
                     vtable::property_::emits_change),
    vtable::property("ueCount",
                     details::MemoryECC::_property_ueCount
                        .data(),
                     _callback_get_ueCount,
                     _callback_set_ueCount,
                     vtable::property_::emits_change),
    vtable::property("state",
                     details::MemoryECC::_property_state
                        .data(),
                     _callback_get_state,
                     _callback_set_state,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Memory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

