#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Host/server.hpp>

#include <xyz/openbmc_project/Control/Host/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

Host::Host(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Host_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Host::_callback_Execute(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](Command&& command)
                    {
                        return o->execute(
                                command);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Control::Host::Error::CommandNotSupported& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Host
{
static const auto _param_Execute =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::xyz::openbmc_project::Control::server::Host::Command>());
static const auto _return_Execute =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


void Host::commandComplete(
            Command command,
            Result result)
{
    auto& i = _xyz_openbmc_project_Control_Host_interface;
    auto m = i.new_signal("CommandComplete");

    m.append(command, result);
    m.signal_send();
}

namespace details
{
namespace Host
{
static const auto _signal_CommandComplete =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::xyz::openbmc_project::Control::server::Host::Command, sdbusplus::xyz::openbmc_project::Control::server::Host::Result>());
}
}



namespace
{
/** String to enum mapping for Host::Command */
static const std::tuple<const char*, Host::Command> mappingHostCommand[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Host.Command.SoftOff",                 Host::Command::SoftOff ),
            std::make_tuple( "xyz.openbmc_project.Control.Host.Command.Heartbeat",                 Host::Command::Heartbeat ),
        };

} // anonymous namespace

auto Host::convertStringToCommand(const std::string& s) noexcept ->
        std::optional<Command>
{
    auto i = std::find_if(
            std::begin(mappingHostCommand),
            std::end(mappingHostCommand),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHostCommand) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Host::convertCommandFromString(const std::string& s) ->
        Command
{
    auto r = convertStringToCommand(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Host::convertCommandToString(Host::Command v)
{
    auto i = std::find_if(
            std::begin(mappingHostCommand),
            std::end(mappingHostCommand),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHostCommand))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Host::Result */
static const std::tuple<const char*, Host::Result> mappingHostResult[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Host.Result.Success",                 Host::Result::Success ),
            std::make_tuple( "xyz.openbmc_project.Control.Host.Result.Failure",                 Host::Result::Failure ),
        };

} // anonymous namespace

auto Host::convertStringToResult(const std::string& s) noexcept ->
        std::optional<Result>
{
    auto i = std::find_if(
            std::begin(mappingHostResult),
            std::end(mappingHostResult),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHostResult) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Host::convertResultFromString(const std::string& s) ->
        Result
{
    auto r = convertStringToResult(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Host::convertResultToString(Host::Result v)
{
    auto i = std::find_if(
            std::begin(mappingHostResult),
            std::end(mappingHostResult),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHostResult))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Host::_vtable[] = {
    vtable::start(),

    vtable::method("Execute",
                   details::Host::_param_Execute
                        .data(),
                   details::Host::_return_Execute
                        .data(),
                   _callback_Execute),

    vtable::signal("CommandComplete",
                   details::Host::_signal_CommandComplete
                        .data()),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

