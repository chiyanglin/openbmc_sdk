#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Power/ACPIPowerState/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

ACPIPowerState::ACPIPowerState(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Power_ACPIPowerState_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ACPIPowerState::ACPIPowerState(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ACPIPowerState(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ACPIPowerState::sysACPIStatus() const ->
        ACPI
{
    return _sysACPIStatus;
}

int ACPIPowerState::_callback_get_SysACPIStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ACPIPowerState*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sysACPIStatus();
                    }
                ));
    }
}

auto ACPIPowerState::sysACPIStatus(ACPI value,
                                         bool skipSignal) ->
        ACPI
{
    if (_sysACPIStatus != value)
    {
        _sysACPIStatus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_ACPIPowerState_interface.property_changed("SysACPIStatus");
        }
    }

    return _sysACPIStatus;
}

auto ACPIPowerState::sysACPIStatus(ACPI val) ->
        ACPI
{
    return sysACPIStatus(val, false);
}

int ACPIPowerState::_callback_set_SysACPIStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ACPIPowerState*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ACPI&& arg)
                    {
                        o->sysACPIStatus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ACPIPowerState
{
static const auto _property_SysACPIStatus =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState::ACPI>());
}
}

auto ACPIPowerState::devACPIStatus() const ->
        ACPI
{
    return _devACPIStatus;
}

int ACPIPowerState::_callback_get_DevACPIStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ACPIPowerState*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->devACPIStatus();
                    }
                ));
    }
}

auto ACPIPowerState::devACPIStatus(ACPI value,
                                         bool skipSignal) ->
        ACPI
{
    if (_devACPIStatus != value)
    {
        _devACPIStatus = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_ACPIPowerState_interface.property_changed("DevACPIStatus");
        }
    }

    return _devACPIStatus;
}

auto ACPIPowerState::devACPIStatus(ACPI val) ->
        ACPI
{
    return devACPIStatus(val, false);
}

int ACPIPowerState::_callback_set_DevACPIStatus(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ACPIPowerState*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](ACPI&& arg)
                    {
                        o->devACPIStatus(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ACPIPowerState
{
static const auto _property_DevACPIStatus =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Power::server::ACPIPowerState::ACPI>());
}
}

void ACPIPowerState::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SysACPIStatus")
    {
        auto& v = std::get<ACPI>(val);
        sysACPIStatus(v, skipSignal);
        return;
    }
    if (_name == "DevACPIStatus")
    {
        auto& v = std::get<ACPI>(val);
        devACPIStatus(v, skipSignal);
        return;
    }
}

auto ACPIPowerState::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SysACPIStatus")
    {
        return sysACPIStatus();
    }
    if (_name == "DevACPIStatus")
    {
        return devACPIStatus();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for ACPIPowerState::ACPI */
static const std::tuple<const char*, ACPIPowerState::ACPI> mappingACPIPowerStateACPI[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S0_G0_D0",                 ACPIPowerState::ACPI::S0_G0_D0 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S1_D1",                 ACPIPowerState::ACPI::S1_D1 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S2_D2",                 ACPIPowerState::ACPI::S2_D2 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S3_D3",                 ACPIPowerState::ACPI::S3_D3 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S4",                 ACPIPowerState::ACPI::S4 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S5_G2",                 ACPIPowerState::ACPI::S5_G2 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.S4_S5",                 ACPIPowerState::ACPI::S4_S5 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.G3",                 ACPIPowerState::ACPI::G3 ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.SLEEP",                 ACPIPowerState::ACPI::SLEEP ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.G1_SLEEP",                 ACPIPowerState::ACPI::G1_SLEEP ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.OVERRIDE",                 ACPIPowerState::ACPI::OVERRIDE ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.LEGACY_ON",                 ACPIPowerState::ACPI::LEGACY_ON ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.LEGACY_OFF",                 ACPIPowerState::ACPI::LEGACY_OFF ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.ACPIPowerState.ACPI.Unknown",                 ACPIPowerState::ACPI::Unknown ),
        };

} // anonymous namespace

auto ACPIPowerState::convertStringToACPI(const std::string& s) noexcept ->
        std::optional<ACPI>
{
    auto i = std::find_if(
            std::begin(mappingACPIPowerStateACPI),
            std::end(mappingACPIPowerStateACPI),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingACPIPowerStateACPI) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto ACPIPowerState::convertACPIFromString(const std::string& s) ->
        ACPI
{
    auto r = convertStringToACPI(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string ACPIPowerState::convertACPIToString(ACPIPowerState::ACPI v)
{
    auto i = std::find_if(
            std::begin(mappingACPIPowerStateACPI),
            std::end(mappingACPIPowerStateACPI),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingACPIPowerStateACPI))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t ACPIPowerState::_vtable[] = {
    vtable::start(),
    vtable::property("SysACPIStatus",
                     details::ACPIPowerState::_property_SysACPIStatus
                        .data(),
                     _callback_get_SysACPIStatus,
                     _callback_set_SysACPIStatus,
                     vtable::property_::emits_change),
    vtable::property("DevACPIStatus",
                     details::ACPIPowerState::_property_DevACPIStatus
                        .data(),
                     _callback_get_DevACPIStatus,
                     _callback_set_DevACPIStatus,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

