#include <xyz/openbmc_project/Chassis/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Common
{
namespace Error
{
const char* UnsupportedCommand::name() const noexcept
{
    return errName;
}
const char* UnsupportedCommand::description() const noexcept
{
    return errDesc;
}
const char* UnsupportedCommand::what() const noexcept
{
    return errWhat;
}
const char* IOError::name() const noexcept
{
    return errName;
}
const char* IOError::description() const noexcept
{
    return errDesc;
}
const char* IOError::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Common
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

