#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Condition/HostFirmware/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Condition
{
namespace server
{

HostFirmware::HostFirmware(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Condition_HostFirmware_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

HostFirmware::HostFirmware(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : HostFirmware(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto HostFirmware::currentFirmwareCondition() const ->
        FirmwareCondition
{
    return _currentFirmwareCondition;
}

int HostFirmware::_callback_get_CurrentFirmwareCondition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HostFirmware*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentFirmwareCondition();
                    }
                ));
    }
}

auto HostFirmware::currentFirmwareCondition(FirmwareCondition value,
                                         bool skipSignal) ->
        FirmwareCondition
{
    if (_currentFirmwareCondition != value)
    {
        _currentFirmwareCondition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Condition_HostFirmware_interface.property_changed("CurrentFirmwareCondition");
        }
    }

    return _currentFirmwareCondition;
}

auto HostFirmware::currentFirmwareCondition(FirmwareCondition val) ->
        FirmwareCondition
{
    return currentFirmwareCondition(val, false);
}

int HostFirmware::_callback_set_CurrentFirmwareCondition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<HostFirmware*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](FirmwareCondition&& arg)
                    {
                        o->currentFirmwareCondition(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace HostFirmware
{
static const auto _property_CurrentFirmwareCondition =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Condition::server::HostFirmware::FirmwareCondition>());
}
}

void HostFirmware::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CurrentFirmwareCondition")
    {
        auto& v = std::get<FirmwareCondition>(val);
        currentFirmwareCondition(v, skipSignal);
        return;
    }
}

auto HostFirmware::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CurrentFirmwareCondition")
    {
        return currentFirmwareCondition();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for HostFirmware::FirmwareCondition */
static const std::tuple<const char*, HostFirmware::FirmwareCondition> mappingHostFirmwareFirmwareCondition[] =
        {
            std::make_tuple( "xyz.openbmc_project.Condition.HostFirmware.FirmwareCondition.Unknown",                 HostFirmware::FirmwareCondition::Unknown ),
            std::make_tuple( "xyz.openbmc_project.Condition.HostFirmware.FirmwareCondition.Off",                 HostFirmware::FirmwareCondition::Off ),
            std::make_tuple( "xyz.openbmc_project.Condition.HostFirmware.FirmwareCondition.Running",                 HostFirmware::FirmwareCondition::Running ),
        };

} // anonymous namespace

auto HostFirmware::convertStringToFirmwareCondition(const std::string& s) noexcept ->
        std::optional<FirmwareCondition>
{
    auto i = std::find_if(
            std::begin(mappingHostFirmwareFirmwareCondition),
            std::end(mappingHostFirmwareFirmwareCondition),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHostFirmwareFirmwareCondition) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto HostFirmware::convertFirmwareConditionFromString(const std::string& s) ->
        FirmwareCondition
{
    auto r = convertStringToFirmwareCondition(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string HostFirmware::convertFirmwareConditionToString(HostFirmware::FirmwareCondition v)
{
    auto i = std::find_if(
            std::begin(mappingHostFirmwareFirmwareCondition),
            std::end(mappingHostFirmwareFirmwareCondition),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHostFirmwareFirmwareCondition))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t HostFirmware::_vtable[] = {
    vtable::start(),
    vtable::property("CurrentFirmwareCondition",
                     details::HostFirmware::_property_CurrentFirmwareCondition
                        .data(),
                     _callback_get_CurrentFirmwareCondition,
                     _callback_set_CurrentFirmwareCondition,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Condition
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

