#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Revision/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Revision::Revision(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Revision_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Revision::Revision(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Revision(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Revision::version() const ->
        std::string
{
    return _version;
}

int Revision::_callback_get_Version(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Revision*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->version();
                    }
                ));
    }
}

auto Revision::version(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_version != value)
    {
        _version = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Revision_interface.property_changed("Version");
        }
    }

    return _version;
}

auto Revision::version(std::string val) ->
        std::string
{
    return version(val, false);
}

int Revision::_callback_set_Version(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Revision*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->version(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Revision
{
static const auto _property_Version =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Revision::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Version")
    {
        auto& v = std::get<std::string>(val);
        version(v, skipSignal);
        return;
    }
}

auto Revision::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Version")
    {
        return version();
    }

    return PropertiesVariant();
}


const vtable_t Revision::_vtable[] = {
    vtable::start(),
    vtable::property("Version",
                     details::Revision::_property_Version
                        .data(),
                     _callback_get_Version,
                     _callback_set_Version,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

