#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/HardwareIsolation/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/HardwareIsolation/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/HardwareIsolation/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_HardwareIsolation_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_Create(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& isolateHardware, xyz::openbmc_project::HardwareIsolation::server::Entry::Type&& severity)
                    {
                        return o->create(
                                isolateHardware, severity);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::TooManyResources& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::HardwareIsolation::Error::IsolatedAlready& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_Create =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path, xyz::openbmc_project::HardwareIsolation::server::Entry::Type>());
static const auto _return_Create =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}

int Create::_callback_CreateWithErrorLog(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& isolateHardware, xyz::openbmc_project::HardwareIsolation::server::Entry::Type&& severity, sdbusplus::message::object_path&& bmcErrorLog)
                    {
                        return o->createWithErrorLog(
                                isolateHardware, severity, bmcErrorLog);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::TooManyResources& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::HardwareIsolation::Error::IsolatedAlready& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_CreateWithErrorLog =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path, xyz::openbmc_project::HardwareIsolation::server::Entry::Type, sdbusplus::message::object_path>());
static const auto _return_CreateWithErrorLog =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("Create",
                   details::Create::_param_Create
                        .data(),
                   details::Create::_return_Create
                        .data(),
                   _callback_Create),

    vtable::method("CreateWithErrorLog",
                   details::Create::_param_CreateWithErrorLog
                        .data(),
                   details::Create::_return_CreateWithErrorLog
                        .data(),
                   _callback_CreateWithErrorLog),
    vtable::end()
};

} // namespace server
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

