#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/SystemConfiguration/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

SystemConfiguration::SystemConfiguration(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_SystemConfiguration_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SystemConfiguration::SystemConfiguration(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SystemConfiguration(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SystemConfiguration::hostName() const ->
        std::string
{
    return _hostName;
}

int SystemConfiguration::_callback_get_HostName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SystemConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hostName();
                    }
                ));
    }
}

auto SystemConfiguration::hostName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_hostName != value)
    {
        _hostName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_SystemConfiguration_interface.property_changed("HostName");
        }
    }

    return _hostName;
}

auto SystemConfiguration::hostName(std::string val) ->
        std::string
{
    return hostName(val, false);
}

int SystemConfiguration::_callback_set_HostName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SystemConfiguration*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->hostName(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SystemConfiguration
{
static const auto _property_HostName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void SystemConfiguration::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "HostName")
    {
        auto& v = std::get<std::string>(val);
        hostName(v, skipSignal);
        return;
    }
}

auto SystemConfiguration::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "HostName")
    {
        return hostName();
    }

    return PropertiesVariant();
}


const vtable_t SystemConfiguration::_vtable[] = {
    vtable::start(),
    vtable::property("HostName",
                     details::SystemConfiguration::_property_HostName
                        .data(),
                     _callback_get_HostName,
                     _callback_set_HostName,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

