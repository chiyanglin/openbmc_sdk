#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/IPMI/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

IPMI::IPMI(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_IPMI_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int IPMI::_callback_IpmiSelAdd(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<IPMI*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& message, sdbusplus::message::object_path&& path, std::vector<uint8_t>&& selData, bool&& assert, uint16_t&& generatorID)
                    {
                        return o->ipmiSelAdd(
                                message, path, selData, assert, generatorID);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace IPMI
{
static const auto _param_IpmiSelAdd =
        utility::tuple_to_array(message::types::type_id<
                std::string, sdbusplus::message::object_path, std::vector<uint8_t>, bool, uint16_t>());
static const auto _return_IpmiSelAdd =
        utility::tuple_to_array(message::types::type_id<
                uint16_t>());
}
}

int IPMI::_callback_AddExtended(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<IPMI*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& message, sdbusplus::message::object_path&& path, std::vector<uint8_t>&& selData, uint16_t&& generatorID, uint8_t&& eventType, uint8_t&& sensorType, uint8_t&& sensorNum)
                    {
                        return o->addExtended(
                                message, path, selData, generatorID, eventType, sensorType, sensorNum);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace IPMI
{
static const auto _param_AddExtended =
        utility::tuple_to_array(message::types::type_id<
                std::string, sdbusplus::message::object_path, std::vector<uint8_t>, uint16_t, uint8_t, uint8_t, uint8_t>());
static const auto _return_AddExtended =
        utility::tuple_to_array(message::types::type_id<
                uint16_t>());
}
}

int IPMI::_callback_IpmiSelAddOem(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<IPMI*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& message, std::vector<uint8_t>&& selData, uint8_t&& recordType)
                    {
                        return o->ipmiSelAddOem(
                                message, selData, recordType);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace IPMI
{
static const auto _param_IpmiSelAddOem =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::vector<uint8_t>, uint8_t>());
static const auto _return_IpmiSelAddOem =
        utility::tuple_to_array(message::types::type_id<
                uint16_t>());
}
}

int IPMI::_callback_Clear(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<IPMI*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->clear(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace IPMI
{
static const auto _param_Clear =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Clear =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t IPMI::_vtable[] = {
    vtable::start(),

    vtable::method("IpmiSelAdd",
                   details::IPMI::_param_IpmiSelAdd
                        .data(),
                   details::IPMI::_return_IpmiSelAdd
                        .data(),
                   _callback_IpmiSelAdd),

    vtable::method("AddExtended",
                   details::IPMI::_param_AddExtended
                        .data(),
                   details::IPMI::_return_AddExtended
                        .data(),
                   _callback_AddExtended),

    vtable::method("IpmiSelAddOem",
                   details::IPMI::_param_IpmiSelAddOem
                        .data(),
                   details::IPMI::_return_IpmiSelAddOem
                        .data(),
                   _callback_IpmiSelAddOem),

    vtable::method("Clear",
                   details::IPMI::_param_Clear
                        .data(),
                   details::IPMI::_return_Clear
                        .data(),
                   _callback_Clear),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

