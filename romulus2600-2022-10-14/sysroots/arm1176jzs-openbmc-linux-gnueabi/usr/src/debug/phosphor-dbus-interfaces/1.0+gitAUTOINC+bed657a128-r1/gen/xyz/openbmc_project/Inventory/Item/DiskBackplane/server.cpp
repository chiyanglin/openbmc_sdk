#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/DiskBackplane/server.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

DiskBackplane::DiskBackplane(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_DiskBackplane_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}





const vtable_t DiskBackplane::_vtable[] = {
    vtable::start(),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

