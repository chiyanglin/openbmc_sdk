#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/AssetTag/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

AssetTag::AssetTag(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_AssetTag_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

AssetTag::AssetTag(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : AssetTag(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto AssetTag::assetTag() const ->
        std::string
{
    return _assetTag;
}

int AssetTag::_callback_get_AssetTag(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AssetTag*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->assetTag();
                    }
                ));
    }
}

auto AssetTag::assetTag(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_assetTag != value)
    {
        _assetTag = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_AssetTag_interface.property_changed("AssetTag");
        }
    }

    return _assetTag;
}

auto AssetTag::assetTag(std::string val) ->
        std::string
{
    return assetTag(val, false);
}

int AssetTag::_callback_set_AssetTag(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<AssetTag*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->assetTag(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace AssetTag
{
static const auto _property_AssetTag =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void AssetTag::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "AssetTag")
    {
        auto& v = std::get<std::string>(val);
        assetTag(v, skipSignal);
        return;
    }
}

auto AssetTag::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "AssetTag")
    {
        return assetTag();
    }

    return PropertiesVariant();
}


const vtable_t AssetTag::_vtable[] = {
    vtable::start(),
    vtable::property("AssetTag",
                     details::AssetTag::_property_AssetTag
                        .data(),
                     _callback_get_AssetTag,
                     _callback_set_AssetTag,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

