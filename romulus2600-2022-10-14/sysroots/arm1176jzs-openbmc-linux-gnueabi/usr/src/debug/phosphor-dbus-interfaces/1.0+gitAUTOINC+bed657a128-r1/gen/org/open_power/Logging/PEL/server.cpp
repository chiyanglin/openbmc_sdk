#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/open_power/Logging/PEL/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>


#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Logging
{
namespace server
{

PEL::PEL(bus_t& bus, const char* path)
        : _org_open_power_Logging_PEL_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int PEL::_callback_GetPEL(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& pelID)
                    {
                        return o->getPEL(
                                pelID);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_GetPEL =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
static const auto _return_GetPEL =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::unix_fd>());
}
}

int PEL::_callback_GetPELFromOBMCID(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& obmcLogID)
                    {
                        return o->getPELFromOBMCID(
                                obmcLogID);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_GetPELFromOBMCID =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
static const auto _return_GetPELFromOBMCID =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int PEL::_callback_HostAck(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& pelID)
                    {
                        return o->hostAck(
                                pelID);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_HostAck =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
static const auto _return_HostAck =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int PEL::_callback_HostReject(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& pelID, RejectionReason&& reason)
                    {
                        return o->hostReject(
                                pelID, reason);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_HostReject =
        utility::tuple_to_array(message::types::type_id<
                uint32_t, sdbusplus::org::open_power::Logging::server::PEL::RejectionReason>());
static const auto _return_HostReject =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int PEL::_callback_CreatePELWithFFDCFiles(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& message, xyz::openbmc_project::Logging::server::Entry::Level&& severity, std::map<std::string, std::string>&& additionalData, std::vector<std::tuple<xyz::openbmc_project::Logging::server::Create::FFDCFormat, uint8_t, uint8_t, sdbusplus::message::unix_fd>>&& ffdc)
                    {
                        return o->createPELWithFFDCFiles(
                                message, severity, additionalData, ffdc);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_CreatePELWithFFDCFiles =
        utility::tuple_to_array(message::types::type_id<
                std::string, xyz::openbmc_project::Logging::server::Entry::Level, std::map<std::string, std::string>, std::vector<std::tuple<xyz::openbmc_project::Logging::server::Create::FFDCFormat, uint8_t, uint8_t, sdbusplus::message::unix_fd>>>());
static const auto _return_CreatePELWithFFDCFiles =
        utility::tuple_to_array(message::types::type_id<
                std::tuple<uint32_t, uint32_t>>());
}
}

int PEL::_callback_GetPELIdFromBMCLogId(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& bmcLogId)
                    {
                        return o->getPELIdFromBMCLogId(
                                bmcLogId);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_GetPELIdFromBMCLogId =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
static const auto _return_GetPELIdFromBMCLogId =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int PEL::_callback_GetBMCLogIdFromPELId(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& pelId)
                    {
                        return o->getBMCLogIdFromPELId(
                                pelId);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_GetBMCLogIdFromPELId =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
static const auto _return_GetBMCLogIdFromPELId =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
}
}

int PEL::_callback_GetPELJSON(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PEL*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint32_t&& bmcLogId)
                    {
                        return o->getPELJSON(
                                bmcLogId);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PEL
{
static const auto _param_GetPELJSON =
        utility::tuple_to_array(message::types::type_id<
                uint32_t>());
static const auto _return_GetPELJSON =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
}
}




namespace
{
/** String to enum mapping for PEL::RejectionReason */
static const std::tuple<const char*, PEL::RejectionReason> mappingPELRejectionReason[] =
        {
            std::make_tuple( "org.open_power.Logging.PEL.RejectionReason.BadPEL",                 PEL::RejectionReason::BadPEL ),
            std::make_tuple( "org.open_power.Logging.PEL.RejectionReason.HostFull",                 PEL::RejectionReason::HostFull ),
        };

} // anonymous namespace

auto PEL::convertStringToRejectionReason(const std::string& s) noexcept ->
        std::optional<RejectionReason>
{
    auto i = std::find_if(
            std::begin(mappingPELRejectionReason),
            std::end(mappingPELRejectionReason),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPELRejectionReason) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto PEL::convertRejectionReasonFromString(const std::string& s) ->
        RejectionReason
{
    auto r = convertStringToRejectionReason(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string PEL::convertRejectionReasonToString(PEL::RejectionReason v)
{
    auto i = std::find_if(
            std::begin(mappingPELRejectionReason),
            std::end(mappingPELRejectionReason),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPELRejectionReason))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t PEL::_vtable[] = {
    vtable::start(),

    vtable::method("GetPEL",
                   details::PEL::_param_GetPEL
                        .data(),
                   details::PEL::_return_GetPEL
                        .data(),
                   _callback_GetPEL),

    vtable::method("GetPELFromOBMCID",
                   details::PEL::_param_GetPELFromOBMCID
                        .data(),
                   details::PEL::_return_GetPELFromOBMCID
                        .data(),
                   _callback_GetPELFromOBMCID),

    vtable::method("HostAck",
                   details::PEL::_param_HostAck
                        .data(),
                   details::PEL::_return_HostAck
                        .data(),
                   _callback_HostAck),

    vtable::method("HostReject",
                   details::PEL::_param_HostReject
                        .data(),
                   details::PEL::_return_HostReject
                        .data(),
                   _callback_HostReject),

    vtable::method("CreatePELWithFFDCFiles",
                   details::PEL::_param_CreatePELWithFFDCFiles
                        .data(),
                   details::PEL::_return_CreatePELWithFFDCFiles
                        .data(),
                   _callback_CreatePELWithFFDCFiles),

    vtable::method("GetPELIdFromBMCLogId",
                   details::PEL::_param_GetPELIdFromBMCLogId
                        .data(),
                   details::PEL::_return_GetPELIdFromBMCLogId
                        .data(),
                   _callback_GetPELIdFromBMCLogId),

    vtable::method("GetBMCLogIdFromPELId",
                   details::PEL::_param_GetBMCLogIdFromPELId
                        .data(),
                   details::PEL::_return_GetBMCLogIdFromPELId
                        .data(),
                   _callback_GetBMCLogIdFromPELId),

    vtable::method("GetPELJSON",
                   details::PEL::_param_GetPELJSON
                        .data(),
                   details::PEL::_return_GetPELJSON
                        .data(),
                   _callback_GetPELJSON),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace open_power
} // namespace org
} // namespace sdbusplus

