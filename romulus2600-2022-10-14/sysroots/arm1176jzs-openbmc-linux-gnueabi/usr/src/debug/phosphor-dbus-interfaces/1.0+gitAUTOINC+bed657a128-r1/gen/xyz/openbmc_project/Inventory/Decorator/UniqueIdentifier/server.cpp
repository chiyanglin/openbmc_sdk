#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/UniqueIdentifier/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

UniqueIdentifier::UniqueIdentifier(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_UniqueIdentifier_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

UniqueIdentifier::UniqueIdentifier(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : UniqueIdentifier(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto UniqueIdentifier::uniqueIdentifier() const ->
        std::string
{
    return _uniqueIdentifier;
}

int UniqueIdentifier::_callback_get_UniqueIdentifier(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UniqueIdentifier*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->uniqueIdentifier();
                    }
                ));
    }
}

auto UniqueIdentifier::uniqueIdentifier(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_uniqueIdentifier != value)
    {
        _uniqueIdentifier = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_UniqueIdentifier_interface.property_changed("UniqueIdentifier");
        }
    }

    return _uniqueIdentifier;
}

auto UniqueIdentifier::uniqueIdentifier(std::string val) ->
        std::string
{
    return uniqueIdentifier(val, false);
}

int UniqueIdentifier::_callback_set_UniqueIdentifier(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UniqueIdentifier*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->uniqueIdentifier(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UniqueIdentifier
{
static const auto _property_UniqueIdentifier =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void UniqueIdentifier::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "UniqueIdentifier")
    {
        auto& v = std::get<std::string>(val);
        uniqueIdentifier(v, skipSignal);
        return;
    }
}

auto UniqueIdentifier::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "UniqueIdentifier")
    {
        return uniqueIdentifier();
    }

    return PropertiesVariant();
}


const vtable_t UniqueIdentifier::_vtable[] = {
    vtable::start(),
    vtable::property("UniqueIdentifier",
                     details::UniqueIdentifier::_property_UniqueIdentifier
                        .data(),
                     _callback_get_UniqueIdentifier,
                     _callback_set_UniqueIdentifier,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

