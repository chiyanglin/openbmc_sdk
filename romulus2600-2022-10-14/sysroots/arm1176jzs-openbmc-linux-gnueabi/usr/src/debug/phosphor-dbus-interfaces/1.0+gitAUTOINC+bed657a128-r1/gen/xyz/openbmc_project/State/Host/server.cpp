#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Host/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

Host::Host(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Host_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Host::Host(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Host(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Host::requestedHostTransition() const ->
        Transition
{
    return _requestedHostTransition;
}

int Host::_callback_get_RequestedHostTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->requestedHostTransition();
                    }
                ));
    }
}

auto Host::requestedHostTransition(Transition value,
                                         bool skipSignal) ->
        Transition
{
    if (_requestedHostTransition != value)
    {
        _requestedHostTransition = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Host_interface.property_changed("RequestedHostTransition");
        }
    }

    return _requestedHostTransition;
}

auto Host::requestedHostTransition(Transition val) ->
        Transition
{
    return requestedHostTransition(val, false);
}

int Host::_callback_set_RequestedHostTransition(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Transition&& arg)
                    {
                        o->requestedHostTransition(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Host
{
static const auto _property_RequestedHostTransition =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Host::Transition>());
}
}

auto Host::currentHostState() const ->
        HostState
{
    return _currentHostState;
}

int Host::_callback_get_CurrentHostState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentHostState();
                    }
                ));
    }
}

auto Host::currentHostState(HostState value,
                                         bool skipSignal) ->
        HostState
{
    if (_currentHostState != value)
    {
        _currentHostState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Host_interface.property_changed("CurrentHostState");
        }
    }

    return _currentHostState;
}

auto Host::currentHostState(HostState val) ->
        HostState
{
    return currentHostState(val, false);
}

int Host::_callback_set_CurrentHostState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](HostState&& arg)
                    {
                        o->currentHostState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Host
{
static const auto _property_CurrentHostState =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Host::HostState>());
}
}

auto Host::restartCause() const ->
        RestartCause
{
    return _restartCause;
}

int Host::_callback_get_RestartCause(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->restartCause();
                    }
                ));
    }
}

auto Host::restartCause(RestartCause value,
                                         bool skipSignal) ->
        RestartCause
{
    if (_restartCause != value)
    {
        _restartCause = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Host_interface.property_changed("RestartCause");
        }
    }

    return _restartCause;
}

auto Host::restartCause(RestartCause val) ->
        RestartCause
{
    return restartCause(val, false);
}

int Host::_callback_set_RestartCause(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Host*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](RestartCause&& arg)
                    {
                        o->restartCause(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Host
{
static const auto _property_RestartCause =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::State::server::Host::RestartCause>());
}
}

void Host::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RequestedHostTransition")
    {
        auto& v = std::get<Transition>(val);
        requestedHostTransition(v, skipSignal);
        return;
    }
    if (_name == "CurrentHostState")
    {
        auto& v = std::get<HostState>(val);
        currentHostState(v, skipSignal);
        return;
    }
    if (_name == "RestartCause")
    {
        auto& v = std::get<RestartCause>(val);
        restartCause(v, skipSignal);
        return;
    }
}

auto Host::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RequestedHostTransition")
    {
        return requestedHostTransition();
    }
    if (_name == "CurrentHostState")
    {
        return currentHostState();
    }
    if (_name == "RestartCause")
    {
        return restartCause();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Host::Transition */
static const std::tuple<const char*, Host::Transition> mappingHostTransition[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Host.Transition.Off",                 Host::Transition::Off ),
            std::make_tuple( "xyz.openbmc_project.State.Host.Transition.On",                 Host::Transition::On ),
            std::make_tuple( "xyz.openbmc_project.State.Host.Transition.Reboot",                 Host::Transition::Reboot ),
            std::make_tuple( "xyz.openbmc_project.State.Host.Transition.GracefulWarmReboot",                 Host::Transition::GracefulWarmReboot ),
            std::make_tuple( "xyz.openbmc_project.State.Host.Transition.ForceWarmReboot",                 Host::Transition::ForceWarmReboot ),
        };

} // anonymous namespace

auto Host::convertStringToTransition(const std::string& s) noexcept ->
        std::optional<Transition>
{
    auto i = std::find_if(
            std::begin(mappingHostTransition),
            std::end(mappingHostTransition),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHostTransition) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Host::convertTransitionFromString(const std::string& s) ->
        Transition
{
    auto r = convertStringToTransition(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Host::convertTransitionToString(Host::Transition v)
{
    auto i = std::find_if(
            std::begin(mappingHostTransition),
            std::end(mappingHostTransition),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHostTransition))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Host::HostState */
static const std::tuple<const char*, Host::HostState> mappingHostHostState[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.Off",                 Host::HostState::Off ),
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.TransitioningToOff",                 Host::HostState::TransitioningToOff ),
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.Standby",                 Host::HostState::Standby ),
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.Running",                 Host::HostState::Running ),
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.TransitioningToRunning",                 Host::HostState::TransitioningToRunning ),
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.Quiesced",                 Host::HostState::Quiesced ),
            std::make_tuple( "xyz.openbmc_project.State.Host.HostState.DiagnosticMode",                 Host::HostState::DiagnosticMode ),
        };

} // anonymous namespace

auto Host::convertStringToHostState(const std::string& s) noexcept ->
        std::optional<HostState>
{
    auto i = std::find_if(
            std::begin(mappingHostHostState),
            std::end(mappingHostHostState),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHostHostState) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Host::convertHostStateFromString(const std::string& s) ->
        HostState
{
    auto r = convertStringToHostState(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Host::convertHostStateToString(Host::HostState v)
{
    auto i = std::find_if(
            std::begin(mappingHostHostState),
            std::end(mappingHostHostState),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHostHostState))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Host::RestartCause */
static const std::tuple<const char*, Host::RestartCause> mappingHostRestartCause[] =
        {
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.Unknown",                 Host::RestartCause::Unknown ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.RemoteCommand",                 Host::RestartCause::RemoteCommand ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.ResetButton",                 Host::RestartCause::ResetButton ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.PowerButton",                 Host::RestartCause::PowerButton ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.WatchdogTimer",                 Host::RestartCause::WatchdogTimer ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.PowerPolicyAlwaysOn",                 Host::RestartCause::PowerPolicyAlwaysOn ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.PowerPolicyPreviousState",                 Host::RestartCause::PowerPolicyPreviousState ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.SoftReset",                 Host::RestartCause::SoftReset ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.ScheduledPowerOn",                 Host::RestartCause::ScheduledPowerOn ),
            std::make_tuple( "xyz.openbmc_project.State.Host.RestartCause.HostCrash",                 Host::RestartCause::HostCrash ),
        };

} // anonymous namespace

auto Host::convertStringToRestartCause(const std::string& s) noexcept ->
        std::optional<RestartCause>
{
    auto i = std::find_if(
            std::begin(mappingHostRestartCause),
            std::end(mappingHostRestartCause),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingHostRestartCause) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Host::convertRestartCauseFromString(const std::string& s) ->
        RestartCause
{
    auto r = convertStringToRestartCause(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Host::convertRestartCauseToString(Host::RestartCause v)
{
    auto i = std::find_if(
            std::begin(mappingHostRestartCause),
            std::end(mappingHostRestartCause),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingHostRestartCause))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Host::_vtable[] = {
    vtable::start(),
    vtable::property("RequestedHostTransition",
                     details::Host::_property_RequestedHostTransition
                        .data(),
                     _callback_get_RequestedHostTransition,
                     _callback_set_RequestedHostTransition,
                     vtable::property_::emits_change),
    vtable::property("CurrentHostState",
                     details::Host::_property_CurrentHostState
                        .data(),
                     _callback_get_CurrentHostState,
                     _callback_set_CurrentHostState,
                     vtable::property_::emits_change),
    vtable::property("RestartCause",
                     details::Host::_property_RestartCause
                        .data(),
                     _callback_get_RestartCause,
                     _callback_set_RestartCause,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

