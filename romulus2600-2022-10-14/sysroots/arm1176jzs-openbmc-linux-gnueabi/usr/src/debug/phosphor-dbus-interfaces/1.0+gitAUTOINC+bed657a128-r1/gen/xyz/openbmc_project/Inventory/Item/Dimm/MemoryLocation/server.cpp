#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Dimm/MemoryLocation/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Dimm
{
namespace server
{

MemoryLocation::MemoryLocation(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MemoryLocation::MemoryLocation(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MemoryLocation(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto MemoryLocation::socket() const ->
        uint8_t
{
    return _socket;
}

int MemoryLocation::_callback_get_Socket(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->socket();
                    }
                ));
    }
}

auto MemoryLocation::socket(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_socket != value)
    {
        _socket = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface.property_changed("Socket");
        }
    }

    return _socket;
}

auto MemoryLocation::socket(uint8_t val) ->
        uint8_t
{
    return socket(val, false);
}

int MemoryLocation::_callback_set_Socket(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->socket(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryLocation
{
static const auto _property_Socket =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto MemoryLocation::memoryController() const ->
        uint8_t
{
    return _memoryController;
}

int MemoryLocation::_callback_get_MemoryController(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryController();
                    }
                ));
    }
}

auto MemoryLocation::memoryController(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_memoryController != value)
    {
        _memoryController = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface.property_changed("MemoryController");
        }
    }

    return _memoryController;
}

auto MemoryLocation::memoryController(uint8_t val) ->
        uint8_t
{
    return memoryController(val, false);
}

int MemoryLocation::_callback_set_MemoryController(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->memoryController(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryLocation
{
static const auto _property_MemoryController =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto MemoryLocation::channel() const ->
        uint8_t
{
    return _channel;
}

int MemoryLocation::_callback_get_Channel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->channel();
                    }
                ));
    }
}

auto MemoryLocation::channel(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_channel != value)
    {
        _channel = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface.property_changed("Channel");
        }
    }

    return _channel;
}

auto MemoryLocation::channel(uint8_t val) ->
        uint8_t
{
    return channel(val, false);
}

int MemoryLocation::_callback_set_Channel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->channel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryLocation
{
static const auto _property_Channel =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto MemoryLocation::slot() const ->
        uint8_t
{
    return _slot;
}

int MemoryLocation::_callback_get_Slot(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->slot();
                    }
                ));
    }
}

auto MemoryLocation::slot(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_slot != value)
    {
        _slot = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Dimm_MemoryLocation_interface.property_changed("Slot");
        }
    }

    return _slot;
}

auto MemoryLocation::slot(uint8_t val) ->
        uint8_t
{
    return slot(val, false);
}

int MemoryLocation::_callback_set_Slot(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MemoryLocation*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->slot(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MemoryLocation
{
static const auto _property_Slot =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void MemoryLocation::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Socket")
    {
        auto& v = std::get<uint8_t>(val);
        socket(v, skipSignal);
        return;
    }
    if (_name == "MemoryController")
    {
        auto& v = std::get<uint8_t>(val);
        memoryController(v, skipSignal);
        return;
    }
    if (_name == "Channel")
    {
        auto& v = std::get<uint8_t>(val);
        channel(v, skipSignal);
        return;
    }
    if (_name == "Slot")
    {
        auto& v = std::get<uint8_t>(val);
        slot(v, skipSignal);
        return;
    }
}

auto MemoryLocation::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Socket")
    {
        return socket();
    }
    if (_name == "MemoryController")
    {
        return memoryController();
    }
    if (_name == "Channel")
    {
        return channel();
    }
    if (_name == "Slot")
    {
        return slot();
    }

    return PropertiesVariant();
}


const vtable_t MemoryLocation::_vtable[] = {
    vtable::start(),
    vtable::property("Socket",
                     details::MemoryLocation::_property_Socket
                        .data(),
                     _callback_get_Socket,
                     _callback_set_Socket,
                     vtable::property_::emits_change),
    vtable::property("MemoryController",
                     details::MemoryLocation::_property_MemoryController
                        .data(),
                     _callback_get_MemoryController,
                     _callback_set_MemoryController,
                     vtable::property_::emits_change),
    vtable::property("Channel",
                     details::MemoryLocation::_property_Channel
                        .data(),
                     _callback_get_Channel,
                     _callback_set_Channel,
                     vtable::property_::emits_change),
    vtable::property("Slot",
                     details::MemoryLocation::_property_Slot
                        .data(),
                     _callback_get_Slot,
                     _callback_set_Slot,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Dimm
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

