#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Chassis/Buttons/ID/server.hpp>

#include <xyz/openbmc_project/Chassis/Common/error.hpp>
#include <xyz/openbmc_project/Chassis/Common/error.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Chassis
{
namespace Buttons
{
namespace server
{

ID::ID(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Chassis_Buttons_ID_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int ID::_callback_SimPress(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<ID*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->simPress(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::UnsupportedCommand& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Chassis::Common::Error::IOError& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace ID
{
static const auto _param_SimPress =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_SimPress =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}


void ID::released(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_ID_interface;
    auto m = i.new_signal("Released");

    m.append();
    m.signal_send();
}

namespace details
{
namespace ID
{
static const auto _signal_Released =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

void ID::pressed(
            )
{
    auto& i = _xyz_openbmc_project_Chassis_Buttons_ID_interface;
    auto m = i.new_signal("Pressed");

    m.append();
    m.signal_send();
}

namespace details
{
namespace ID
{
static const auto _signal_Pressed =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}



const vtable_t ID::_vtable[] = {
    vtable::start(),

    vtable::method("simPress",
                   details::ID::_param_SimPress
                        .data(),
                   details::ID::_return_SimPress
                        .data(),
                   _callback_SimPress),

    vtable::signal("Released",
                   details::ID::_signal_Released
                        .data()),

    vtable::signal("Pressed",
                   details::ID::_signal_Pressed
                        .data()),
    vtable::end()
};

} // namespace server
} // namespace Buttons
} // namespace Chassis
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

