#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/UTIL/server.hpp>






















namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

UTIL::UTIL(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_UTIL_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

UTIL::UTIL(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : UTIL(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto UTIL::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int UTIL::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto UTIL::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto UTIL::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int UTIL::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d0() const ->
        std::vector<uint8_t>
{
    return _d0;
}

int UTIL::_callback_get_D0(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d0();
                    }
                ));
    }
}

auto UTIL::d0(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d0 != value)
    {
        _d0 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D0");
        }
    }

    return _d0;
}

auto UTIL::d0(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d0(val, false);
}

int UTIL::_callback_set_D0(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d0(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D0 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d1() const ->
        std::vector<uint8_t>
{
    return _d1;
}

int UTIL::_callback_get_D1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d1();
                    }
                ));
    }
}

auto UTIL::d1(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d1 != value)
    {
        _d1 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D1");
        }
    }

    return _d1;
}

auto UTIL::d1(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d1(val, false);
}

int UTIL::_callback_set_D1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d1(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D1 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d2() const ->
        std::vector<uint8_t>
{
    return _d2;
}

int UTIL::_callback_get_D2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d2();
                    }
                ));
    }
}

auto UTIL::d2(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d2 != value)
    {
        _d2 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D2");
        }
    }

    return _d2;
}

auto UTIL::d2(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d2(val, false);
}

int UTIL::_callback_set_D2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d2(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D2 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d3() const ->
        std::vector<uint8_t>
{
    return _d3;
}

int UTIL::_callback_get_D3(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d3();
                    }
                ));
    }
}

auto UTIL::d3(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d3 != value)
    {
        _d3 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D3");
        }
    }

    return _d3;
}

auto UTIL::d3(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d3(val, false);
}

int UTIL::_callback_set_D3(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d3(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D3 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int UTIL::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto UTIL::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto UTIL::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int UTIL::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d5() const ->
        std::vector<uint8_t>
{
    return _d5;
}

int UTIL::_callback_get_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d5();
                    }
                ));
    }
}

auto UTIL::d5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d5 != value)
    {
        _d5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D5");
        }
    }

    return _d5;
}

auto UTIL::d5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d5(val, false);
}

int UTIL::_callback_set_D5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d6() const ->
        std::vector<uint8_t>
{
    return _d6;
}

int UTIL::_callback_get_D6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d6();
                    }
                ));
    }
}

auto UTIL::d6(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d6 != value)
    {
        _d6 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D6");
        }
    }

    return _d6;
}

auto UTIL::d6(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d6(val, false);
}

int UTIL::_callback_set_D6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d6(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D6 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d7() const ->
        std::vector<uint8_t>
{
    return _d7;
}

int UTIL::_callback_get_D7(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d7();
                    }
                ));
    }
}

auto UTIL::d7(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d7 != value)
    {
        _d7 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D7");
        }
    }

    return _d7;
}

auto UTIL::d7(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d7(val, false);
}

int UTIL::_callback_set_D7(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d7(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D7 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d8() const ->
        std::vector<uint8_t>
{
    return _d8;
}

int UTIL::_callback_get_D8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d8();
                    }
                ));
    }
}

auto UTIL::d8(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d8 != value)
    {
        _d8 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D8");
        }
    }

    return _d8;
}

auto UTIL::d8(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d8(val, false);
}

int UTIL::_callback_set_D8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d8(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D8 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::d9() const ->
        std::vector<uint8_t>
{
    return _d9;
}

int UTIL::_callback_get_D9(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d9();
                    }
                ));
    }
}

auto UTIL::d9(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d9 != value)
    {
        _d9 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("D9");
        }
    }

    return _d9;
}

auto UTIL::d9(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d9(val, false);
}

int UTIL::_callback_set_D9(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d9(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_D9 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f0() const ->
        std::vector<uint8_t>
{
    return _f0;
}

int UTIL::_callback_get_F0(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f0();
                    }
                ));
    }
}

auto UTIL::f0(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f0 != value)
    {
        _f0 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F0");
        }
    }

    return _f0;
}

auto UTIL::f0(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f0(val, false);
}

int UTIL::_callback_set_F0(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f0(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F0 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f1() const ->
        std::vector<uint8_t>
{
    return _f1;
}

int UTIL::_callback_get_F1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f1();
                    }
                ));
    }
}

auto UTIL::f1(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f1 != value)
    {
        _f1 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F1");
        }
    }

    return _f1;
}

auto UTIL::f1(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f1(val, false);
}

int UTIL::_callback_set_F1(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f1(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F1 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f2() const ->
        std::vector<uint8_t>
{
    return _f2;
}

int UTIL::_callback_get_F2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f2();
                    }
                ));
    }
}

auto UTIL::f2(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f2 != value)
    {
        _f2 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F2");
        }
    }

    return _f2;
}

auto UTIL::f2(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f2(val, false);
}

int UTIL::_callback_set_F2(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f2(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F2 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f3() const ->
        std::vector<uint8_t>
{
    return _f3;
}

int UTIL::_callback_get_F3(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f3();
                    }
                ));
    }
}

auto UTIL::f3(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f3 != value)
    {
        _f3 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F3");
        }
    }

    return _f3;
}

auto UTIL::f3(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f3(val, false);
}

int UTIL::_callback_set_F3(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f3(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F3 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f4() const ->
        std::vector<uint8_t>
{
    return _f4;
}

int UTIL::_callback_get_F4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f4();
                    }
                ));
    }
}

auto UTIL::f4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f4 != value)
    {
        _f4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F4");
        }
    }

    return _f4;
}

auto UTIL::f4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f4(val, false);
}

int UTIL::_callback_set_F4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f5() const ->
        std::vector<uint8_t>
{
    return _f5;
}

int UTIL::_callback_get_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f5();
                    }
                ));
    }
}

auto UTIL::f5(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f5 != value)
    {
        _f5 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F5");
        }
    }

    return _f5;
}

auto UTIL::f5(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f5(val, false);
}

int UTIL::_callback_set_F5(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f5(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F5 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f6() const ->
        std::vector<uint8_t>
{
    return _f6;
}

int UTIL::_callback_get_F6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f6();
                    }
                ));
    }
}

auto UTIL::f6(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f6 != value)
    {
        _f6 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F6");
        }
    }

    return _f6;
}

auto UTIL::f6(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f6(val, false);
}

int UTIL::_callback_set_F6(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f6(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F6 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f7() const ->
        std::vector<uint8_t>
{
    return _f7;
}

int UTIL::_callback_get_F7(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f7();
                    }
                ));
    }
}

auto UTIL::f7(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f7 != value)
    {
        _f7 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F7");
        }
    }

    return _f7;
}

auto UTIL::f7(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f7(val, false);
}

int UTIL::_callback_set_F7(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f7(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F7 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f8() const ->
        std::vector<uint8_t>
{
    return _f8;
}

int UTIL::_callback_get_F8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f8();
                    }
                ));
    }
}

auto UTIL::f8(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f8 != value)
    {
        _f8 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F8");
        }
    }

    return _f8;
}

auto UTIL::f8(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f8(val, false);
}

int UTIL::_callback_set_F8(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f8(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F8 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto UTIL::f9() const ->
        std::vector<uint8_t>
{
    return _f9;
}

int UTIL::_callback_get_F9(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->f9();
                    }
                ));
    }
}

auto UTIL::f9(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_f9 != value)
    {
        _f9 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_UTIL_interface.property_changed("F9");
        }
    }

    return _f9;
}

auto UTIL::f9(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return f9(val, false);
}

int UTIL::_callback_set_F9(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<UTIL*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->f9(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace UTIL
{
static const auto _property_F9 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void UTIL::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "D0")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d0(v, skipSignal);
        return;
    }
    if (_name == "D1")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d1(v, skipSignal);
        return;
    }
    if (_name == "D2")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d2(v, skipSignal);
        return;
    }
    if (_name == "D3")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d3(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
    if (_name == "D5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d5(v, skipSignal);
        return;
    }
    if (_name == "D6")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d6(v, skipSignal);
        return;
    }
    if (_name == "D7")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d7(v, skipSignal);
        return;
    }
    if (_name == "D8")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d8(v, skipSignal);
        return;
    }
    if (_name == "D9")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d9(v, skipSignal);
        return;
    }
    if (_name == "F0")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f0(v, skipSignal);
        return;
    }
    if (_name == "F1")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f1(v, skipSignal);
        return;
    }
    if (_name == "F2")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f2(v, skipSignal);
        return;
    }
    if (_name == "F3")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f3(v, skipSignal);
        return;
    }
    if (_name == "F4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f4(v, skipSignal);
        return;
    }
    if (_name == "F5")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f5(v, skipSignal);
        return;
    }
    if (_name == "F6")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f6(v, skipSignal);
        return;
    }
    if (_name == "F7")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f7(v, skipSignal);
        return;
    }
    if (_name == "F8")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f8(v, skipSignal);
        return;
    }
    if (_name == "F9")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        f9(v, skipSignal);
        return;
    }
}

auto UTIL::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "D0")
    {
        return d0();
    }
    if (_name == "D1")
    {
        return d1();
    }
    if (_name == "D2")
    {
        return d2();
    }
    if (_name == "D3")
    {
        return d3();
    }
    if (_name == "D4")
    {
        return d4();
    }
    if (_name == "D5")
    {
        return d5();
    }
    if (_name == "D6")
    {
        return d6();
    }
    if (_name == "D7")
    {
        return d7();
    }
    if (_name == "D8")
    {
        return d8();
    }
    if (_name == "D9")
    {
        return d9();
    }
    if (_name == "F0")
    {
        return f0();
    }
    if (_name == "F1")
    {
        return f1();
    }
    if (_name == "F2")
    {
        return f2();
    }
    if (_name == "F3")
    {
        return f3();
    }
    if (_name == "F4")
    {
        return f4();
    }
    if (_name == "F5")
    {
        return f5();
    }
    if (_name == "F6")
    {
        return f6();
    }
    if (_name == "F7")
    {
        return f7();
    }
    if (_name == "F8")
    {
        return f8();
    }
    if (_name == "F9")
    {
        return f9();
    }

    return PropertiesVariant();
}


const vtable_t UTIL::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::UTIL::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("D0",
                     details::UTIL::_property_D0
                        .data(),
                     _callback_get_D0,
                     _callback_set_D0,
                     vtable::property_::emits_change),
    vtable::property("D1",
                     details::UTIL::_property_D1
                        .data(),
                     _callback_get_D1,
                     _callback_set_D1,
                     vtable::property_::emits_change),
    vtable::property("D2",
                     details::UTIL::_property_D2
                        .data(),
                     _callback_get_D2,
                     _callback_set_D2,
                     vtable::property_::emits_change),
    vtable::property("D3",
                     details::UTIL::_property_D3
                        .data(),
                     _callback_get_D3,
                     _callback_set_D3,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::UTIL::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::property("D5",
                     details::UTIL::_property_D5
                        .data(),
                     _callback_get_D5,
                     _callback_set_D5,
                     vtable::property_::emits_change),
    vtable::property("D6",
                     details::UTIL::_property_D6
                        .data(),
                     _callback_get_D6,
                     _callback_set_D6,
                     vtable::property_::emits_change),
    vtable::property("D7",
                     details::UTIL::_property_D7
                        .data(),
                     _callback_get_D7,
                     _callback_set_D7,
                     vtable::property_::emits_change),
    vtable::property("D8",
                     details::UTIL::_property_D8
                        .data(),
                     _callback_get_D8,
                     _callback_set_D8,
                     vtable::property_::emits_change),
    vtable::property("D9",
                     details::UTIL::_property_D9
                        .data(),
                     _callback_get_D9,
                     _callback_set_D9,
                     vtable::property_::emits_change),
    vtable::property("F0",
                     details::UTIL::_property_F0
                        .data(),
                     _callback_get_F0,
                     _callback_set_F0,
                     vtable::property_::emits_change),
    vtable::property("F1",
                     details::UTIL::_property_F1
                        .data(),
                     _callback_get_F1,
                     _callback_set_F1,
                     vtable::property_::emits_change),
    vtable::property("F2",
                     details::UTIL::_property_F2
                        .data(),
                     _callback_get_F2,
                     _callback_set_F2,
                     vtable::property_::emits_change),
    vtable::property("F3",
                     details::UTIL::_property_F3
                        .data(),
                     _callback_get_F3,
                     _callback_set_F3,
                     vtable::property_::emits_change),
    vtable::property("F4",
                     details::UTIL::_property_F4
                        .data(),
                     _callback_get_F4,
                     _callback_set_F4,
                     vtable::property_::emits_change),
    vtable::property("F5",
                     details::UTIL::_property_F5
                        .data(),
                     _callback_get_F5,
                     _callback_set_F5,
                     vtable::property_::emits_change),
    vtable::property("F6",
                     details::UTIL::_property_F6
                        .data(),
                     _callback_get_F6,
                     _callback_set_F6,
                     vtable::property_::emits_change),
    vtable::property("F7",
                     details::UTIL::_property_F7
                        .data(),
                     _callback_get_F7,
                     _callback_set_F7,
                     vtable::property_::emits_change),
    vtable::property("F8",
                     details::UTIL::_property_F8
                        .data(),
                     _callback_get_F8,
                     _callback_set_F8,
                     vtable::property_::emits_change),
    vtable::property("F9",
                     details::UTIL::_property_F9
                        .data(),
                     _callback_get_F9,
                     _callback_set_F9,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

