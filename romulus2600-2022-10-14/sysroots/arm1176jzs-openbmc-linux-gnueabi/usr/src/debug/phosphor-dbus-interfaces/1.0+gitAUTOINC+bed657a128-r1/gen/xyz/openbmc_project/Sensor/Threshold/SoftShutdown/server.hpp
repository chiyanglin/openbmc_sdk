#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

class SoftShutdown
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        SoftShutdown() = delete;
        SoftShutdown(const SoftShutdown&) = delete;
        SoftShutdown& operator=(const SoftShutdown&) = delete;
        SoftShutdown(SoftShutdown&&) = delete;
        SoftShutdown& operator=(SoftShutdown&&) = delete;
        virtual ~SoftShutdown() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        SoftShutdown(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        SoftShutdown(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** @brief Send signal 'SoftShutdownHighAlarmAsserted'
         *
         *  The high threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void softShutdownHighAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'SoftShutdownHighAlarmDeasserted'
         *
         *  The high threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void softShutdownHighAlarmDeasserted(
            double sensorValue);

        /** @brief Send signal 'SoftShutdownLowAlarmAsserted'
         *
         *  The low threshold alarm asserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void softShutdownLowAlarmAsserted(
            double sensorValue);

        /** @brief Send signal 'SoftShutdownLowAlarmDeasserted'
         *
         *  The low threshold alarm deasserted.
         *
         *  @param[in] sensorValue - The sensor value that triggered the alarm change.
         */
        void softShutdownLowAlarmDeasserted(
            double sensorValue);

        /** Get value of SoftShutdownHigh */
        virtual double softShutdownHigh() const;
        /** Set value of SoftShutdownHigh with option to skip sending signal */
        virtual double softShutdownHigh(double value,
               bool skipSignal);
        /** Set value of SoftShutdownHigh */
        virtual double softShutdownHigh(double value);
        /** Get value of SoftShutdownLow */
        virtual double softShutdownLow() const;
        /** Set value of SoftShutdownLow with option to skip sending signal */
        virtual double softShutdownLow(double value,
               bool skipSignal);
        /** Set value of SoftShutdownLow */
        virtual double softShutdownLow(double value);
        /** Get value of SoftShutdownAlarmHigh */
        virtual bool softShutdownAlarmHigh() const;
        /** Set value of SoftShutdownAlarmHigh with option to skip sending signal */
        virtual bool softShutdownAlarmHigh(bool value,
               bool skipSignal);
        /** Set value of SoftShutdownAlarmHigh */
        virtual bool softShutdownAlarmHigh(bool value);
        /** Get value of SoftShutdownAlarmLow */
        virtual bool softShutdownAlarmLow() const;
        /** Set value of SoftShutdownAlarmLow with option to skip sending signal */
        virtual bool softShutdownAlarmLow(bool value,
               bool skipSignal);
        /** Set value of SoftShutdownAlarmLow */
        virtual bool softShutdownAlarmLow(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Sensor.Threshold.SoftShutdown";

    private:

        /** @brief sd-bus callback for get-property 'SoftShutdownHigh' */
        static int _callback_get_SoftShutdownHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SoftShutdownHigh' */
        static int _callback_set_SoftShutdownHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SoftShutdownLow' */
        static int _callback_get_SoftShutdownLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SoftShutdownLow' */
        static int _callback_set_SoftShutdownLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SoftShutdownAlarmHigh' */
        static int _callback_get_SoftShutdownAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SoftShutdownAlarmHigh' */
        static int _callback_set_SoftShutdownAlarmHigh(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SoftShutdownAlarmLow' */
        static int _callback_get_SoftShutdownAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SoftShutdownAlarmLow' */
        static int _callback_set_SoftShutdownAlarmLow(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Sensor_Threshold_SoftShutdown_interface;
        sdbusplus::SdBusInterface *_intf;

        double _softShutdownHigh = std::numeric_limits<double>::quiet_NaN();
        double _softShutdownLow = std::numeric_limits<double>::quiet_NaN();
        bool _softShutdownAlarmHigh{};
        bool _softShutdownAlarmLow{};

};


} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

