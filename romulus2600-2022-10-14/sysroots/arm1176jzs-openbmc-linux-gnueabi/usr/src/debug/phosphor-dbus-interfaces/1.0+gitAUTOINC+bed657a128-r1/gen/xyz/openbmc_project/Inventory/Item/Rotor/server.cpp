#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Rotor/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

Rotor::Rotor(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Rotor_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Rotor::Rotor(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Rotor(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Rotor::nominalVoltage() const ->
        double
{
    return _nominalVoltage;
}

int Rotor::_callback_get_NominalVoltage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Rotor*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nominalVoltage();
                    }
                ));
    }
}

auto Rotor::nominalVoltage(double value,
                                         bool skipSignal) ->
        double
{
    if (_nominalVoltage != value)
    {
        _nominalVoltage = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Rotor_interface.property_changed("NominalVoltage");
        }
    }

    return _nominalVoltage;
}

auto Rotor::nominalVoltage(double val) ->
        double
{
    return nominalVoltage(val, false);
}

int Rotor::_callback_set_NominalVoltage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Rotor*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->nominalVoltage(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Rotor
{
static const auto _property_NominalVoltage =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Rotor::nominalCurrent() const ->
        double
{
    return _nominalCurrent;
}

int Rotor::_callback_get_NominalCurrent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Rotor*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nominalCurrent();
                    }
                ));
    }
}

auto Rotor::nominalCurrent(double value,
                                         bool skipSignal) ->
        double
{
    if (_nominalCurrent != value)
    {
        _nominalCurrent = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Rotor_interface.property_changed("NominalCurrent");
        }
    }

    return _nominalCurrent;
}

auto Rotor::nominalCurrent(double val) ->
        double
{
    return nominalCurrent(val, false);
}

int Rotor::_callback_set_NominalCurrent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Rotor*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->nominalCurrent(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Rotor
{
static const auto _property_NominalCurrent =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Rotor::nominalRPM() const ->
        double
{
    return _nominalRPM;
}

int Rotor::_callback_get_NominalRPM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Rotor*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nominalRPM();
                    }
                ));
    }
}

auto Rotor::nominalRPM(double value,
                                         bool skipSignal) ->
        double
{
    if (_nominalRPM != value)
    {
        _nominalRPM = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Rotor_interface.property_changed("NominalRPM");
        }
    }

    return _nominalRPM;
}

auto Rotor::nominalRPM(double val) ->
        double
{
    return nominalRPM(val, false);
}

int Rotor::_callback_set_NominalRPM(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Rotor*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->nominalRPM(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Rotor
{
static const auto _property_NominalRPM =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

void Rotor::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "NominalVoltage")
    {
        auto& v = std::get<double>(val);
        nominalVoltage(v, skipSignal);
        return;
    }
    if (_name == "NominalCurrent")
    {
        auto& v = std::get<double>(val);
        nominalCurrent(v, skipSignal);
        return;
    }
    if (_name == "NominalRPM")
    {
        auto& v = std::get<double>(val);
        nominalRPM(v, skipSignal);
        return;
    }
}

auto Rotor::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "NominalVoltage")
    {
        return nominalVoltage();
    }
    if (_name == "NominalCurrent")
    {
        return nominalCurrent();
    }
    if (_name == "NominalRPM")
    {
        return nominalRPM();
    }

    return PropertiesVariant();
}


const vtable_t Rotor::_vtable[] = {
    vtable::start(),
    vtable::property("NominalVoltage",
                     details::Rotor::_property_NominalVoltage
                        .data(),
                     _callback_get_NominalVoltage,
                     _callback_set_NominalVoltage,
                     vtable::property_::emits_change),
    vtable::property("NominalCurrent",
                     details::Rotor::_property_NominalCurrent
                        .data(),
                     _callback_get_NominalCurrent,
                     _callback_set_NominalCurrent,
                     vtable::property_::emits_change),
    vtable::property("NominalRPM",
                     details::Rotor::_property_NominalRPM
                        .data(),
                     _callback_get_NominalRPM,
                     _callback_set_NominalRPM,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

