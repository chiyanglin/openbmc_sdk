#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace SystemdTarget
{
namespace Error
{

struct Failure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.SystemdTarget.Error.Failure";
    static constexpr auto errDesc =
            "A systemd target has failed";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.SystemdTarget.Error.Failure: A systemd target has failed";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace SystemdTarget
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

