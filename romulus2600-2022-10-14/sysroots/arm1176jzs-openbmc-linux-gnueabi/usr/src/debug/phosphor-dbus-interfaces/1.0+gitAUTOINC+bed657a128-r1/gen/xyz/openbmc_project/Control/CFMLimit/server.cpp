#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/CFMLimit/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

CFMLimit::CFMLimit(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_CFMLimit_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CFMLimit::CFMLimit(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CFMLimit(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CFMLimit::limit() const ->
        double
{
    return _limit;
}

int CFMLimit::_callback_get_Limit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CFMLimit*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->limit();
                    }
                ));
    }
}

auto CFMLimit::limit(double value,
                                         bool skipSignal) ->
        double
{
    if (_limit != value)
    {
        _limit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_CFMLimit_interface.property_changed("Limit");
        }
    }

    return _limit;
}

auto CFMLimit::limit(double val) ->
        double
{
    return limit(val, false);
}

int CFMLimit::_callback_set_Limit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CFMLimit*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->limit(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CFMLimit
{
static const auto _property_Limit =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

void CFMLimit::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Limit")
    {
        auto& v = std::get<double>(val);
        limit(v, skipSignal);
        return;
    }
}

auto CFMLimit::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Limit")
    {
        return limit();
    }

    return PropertiesVariant();
}


const vtable_t CFMLimit::_vtable[] = {
    vtable::start(),
    vtable::property("Limit",
                     details::CFMLimit::_property_Limit
                        .data(),
                     _callback_get_Limit,
                     _callback_set_Limit,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

