#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Host/NMI/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Host
{
namespace server
{

NMI::NMI(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Host_NMI_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int NMI::_callback_NMI(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<NMI*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nmi(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace NMI
{
static const auto _param_NMI =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_NMI =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t NMI::_vtable[] = {
    vtable::start(),

    vtable::method("NMI",
                   details::NMI::_param_NMI
                        .data(),
                   details::NMI::_return_NMI
                        .data(),
                   _callback_NMI),
    vtable::end()
};

} // namespace server
} // namespace Host
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

