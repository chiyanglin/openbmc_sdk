#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/Client/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

Client::Client(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_Client_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Client::Client(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Client(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Client::address() const ->
        std::string
{
    return _address;
}

int Client::_callback_get_Address(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Client*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->address();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Client::address(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_address != value)
    {
        _address = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Client_interface.property_changed("Address");
        }
    }

    return _address;
}

auto Client::address(std::string val) ->
        std::string
{
    return address(val, false);
}

int Client::_callback_set_Address(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Client*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->address(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Client
{
static const auto _property_Address =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Client::port() const ->
        uint16_t
{
    return _port;
}

int Client::_callback_get_Port(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Client*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->port();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto Client::port(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_port != value)
    {
        _port = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_Client_interface.property_changed("Port");
        }
    }

    return _port;
}

auto Client::port(uint16_t val) ->
        uint16_t
{
    return port(val, false);
}

int Client::_callback_set_Port(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Client*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->port(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace Client
{
static const auto _property_Port =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

void Client::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Address")
    {
        auto& v = std::get<std::string>(val);
        address(v, skipSignal);
        return;
    }
    if (_name == "Port")
    {
        auto& v = std::get<uint16_t>(val);
        port(v, skipSignal);
        return;
    }
}

auto Client::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Address")
    {
        return address();
    }
    if (_name == "Port")
    {
        return port();
    }

    return PropertiesVariant();
}


const vtable_t Client::_vtable[] = {
    vtable::start(),
    vtable::property("Address",
                     details::Client::_property_Address
                        .data(),
                     _callback_get_Address,
                     _callback_set_Address,
                     vtable::property_::emits_change),
    vtable::property("Port",
                     details::Client::_property_Port
                        .data(),
                     _callback_get_Port,
                     _callback_set_Port,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

