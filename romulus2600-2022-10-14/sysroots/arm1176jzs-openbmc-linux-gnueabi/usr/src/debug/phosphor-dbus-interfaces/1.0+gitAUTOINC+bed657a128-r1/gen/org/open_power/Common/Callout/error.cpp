#include <org/open_power/Common/Callout/error.hpp>

namespace sdbusplus
{
namespace org
{
namespace open_power
{
namespace Common
{
namespace Callout
{
namespace Error
{
const char* Procedure::name() const noexcept
{
    return errName;
}
const char* Procedure::description() const noexcept
{
    return errDesc;
}
const char* Procedure::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Callout
} // namespace Common
} // namespace open_power
} // namespace org
} // namespace sdbusplus

