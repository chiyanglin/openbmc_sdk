#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/CLEI/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

CLEI::CLEI(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_CLEI_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

CLEI::CLEI(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : CLEI(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto CLEI::cleiNumber() const ->
        std::string
{
    return _cleiNumber;
}

int CLEI::_callback_get_CLEINumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CLEI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->cleiNumber();
                    }
                ));
    }
}

auto CLEI::cleiNumber(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_cleiNumber != value)
    {
        _cleiNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_CLEI_interface.property_changed("CLEINumber");
        }
    }

    return _cleiNumber;
}

auto CLEI::cleiNumber(std::string val) ->
        std::string
{
    return cleiNumber(val, false);
}

int CLEI::_callback_set_CLEINumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<CLEI*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->cleiNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace CLEI
{
static const auto _property_CLEINumber =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void CLEI::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CLEINumber")
    {
        auto& v = std::get<std::string>(val);
        cleiNumber(v, skipSignal);
        return;
    }
}

auto CLEI::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CLEINumber")
    {
        return cleiNumber();
    }

    return PropertiesVariant();
}


const vtable_t CLEI::_vtable[] = {
    vtable::start(),
    vtable::property("CLEINumber",
                     details::CLEI::_property_CLEINumber
                        .data(),
                     _callback_get_CLEINumber,
                     _callback_set_CLEINumber,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

