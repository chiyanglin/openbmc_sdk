#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/Cpu/OperatingConfig/server.hpp>








namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace Cpu
{
namespace server
{

OperatingConfig::OperatingConfig(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

OperatingConfig::OperatingConfig(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : OperatingConfig(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto OperatingConfig::baseSpeed() const ->
        uint32_t
{
    return _baseSpeed;
}

int OperatingConfig::_callback_get_BaseSpeed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->baseSpeed();
                    }
                ));
    }
}

auto OperatingConfig::baseSpeed(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_baseSpeed != value)
    {
        _baseSpeed = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("BaseSpeed");
        }
    }

    return _baseSpeed;
}

auto OperatingConfig::baseSpeed(uint32_t val) ->
        uint32_t
{
    return baseSpeed(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_BaseSpeed =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto OperatingConfig::baseSpeedPrioritySettings() const ->
        std::vector<std::tuple<uint32_t, std::vector<uint32_t>>>
{
    return _baseSpeedPrioritySettings;
}

int OperatingConfig::_callback_get_BaseSpeedPrioritySettings(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->baseSpeedPrioritySettings();
                    }
                ));
    }
}

auto OperatingConfig::baseSpeedPrioritySettings(std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> value,
                                         bool skipSignal) ->
        std::vector<std::tuple<uint32_t, std::vector<uint32_t>>>
{
    if (_baseSpeedPrioritySettings != value)
    {
        _baseSpeedPrioritySettings = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("BaseSpeedPrioritySettings");
        }
    }

    return _baseSpeedPrioritySettings;
}

auto OperatingConfig::baseSpeedPrioritySettings(std::vector<std::tuple<uint32_t, std::vector<uint32_t>>> val) ->
        std::vector<std::tuple<uint32_t, std::vector<uint32_t>>>
{
    return baseSpeedPrioritySettings(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_BaseSpeedPrioritySettings =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::tuple<uint32_t, std::vector<uint32_t>>>>());
}
}

auto OperatingConfig::maxJunctionTemperature() const ->
        uint32_t
{
    return _maxJunctionTemperature;
}

int OperatingConfig::_callback_get_MaxJunctionTemperature(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxJunctionTemperature();
                    }
                ));
    }
}

auto OperatingConfig::maxJunctionTemperature(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_maxJunctionTemperature != value)
    {
        _maxJunctionTemperature = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("MaxJunctionTemperature");
        }
    }

    return _maxJunctionTemperature;
}

auto OperatingConfig::maxJunctionTemperature(uint32_t val) ->
        uint32_t
{
    return maxJunctionTemperature(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_MaxJunctionTemperature =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto OperatingConfig::maxSpeed() const ->
        uint32_t
{
    return _maxSpeed;
}

int OperatingConfig::_callback_get_MaxSpeed(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxSpeed();
                    }
                ));
    }
}

auto OperatingConfig::maxSpeed(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_maxSpeed != value)
    {
        _maxSpeed = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("MaxSpeed");
        }
    }

    return _maxSpeed;
}

auto OperatingConfig::maxSpeed(uint32_t val) ->
        uint32_t
{
    return maxSpeed(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_MaxSpeed =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto OperatingConfig::powerLimit() const ->
        uint32_t
{
    return _powerLimit;
}

int OperatingConfig::_callback_get_PowerLimit(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerLimit();
                    }
                ));
    }
}

auto OperatingConfig::powerLimit(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_powerLimit != value)
    {
        _powerLimit = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("PowerLimit");
        }
    }

    return _powerLimit;
}

auto OperatingConfig::powerLimit(uint32_t val) ->
        uint32_t
{
    return powerLimit(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_PowerLimit =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto OperatingConfig::availableCoreCount() const ->
        size_t
{
    return _availableCoreCount;
}

int OperatingConfig::_callback_get_AvailableCoreCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->availableCoreCount();
                    }
                ));
    }
}

auto OperatingConfig::availableCoreCount(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_availableCoreCount != value)
    {
        _availableCoreCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("AvailableCoreCount");
        }
    }

    return _availableCoreCount;
}

auto OperatingConfig::availableCoreCount(size_t val) ->
        size_t
{
    return availableCoreCount(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_AvailableCoreCount =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto OperatingConfig::turboProfile() const ->
        std::vector<std::tuple<uint32_t, size_t>>
{
    return _turboProfile;
}

int OperatingConfig::_callback_get_TurboProfile(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<OperatingConfig*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->turboProfile();
                    }
                ));
    }
}

auto OperatingConfig::turboProfile(std::vector<std::tuple<uint32_t, size_t>> value,
                                         bool skipSignal) ->
        std::vector<std::tuple<uint32_t, size_t>>
{
    if (_turboProfile != value)
    {
        _turboProfile = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_Cpu_OperatingConfig_interface.property_changed("TurboProfile");
        }
    }

    return _turboProfile;
}

auto OperatingConfig::turboProfile(std::vector<std::tuple<uint32_t, size_t>> val) ->
        std::vector<std::tuple<uint32_t, size_t>>
{
    return turboProfile(val, false);
}


namespace details
{
namespace OperatingConfig
{
static const auto _property_TurboProfile =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::tuple<uint32_t, size_t>>>());
}
}

void OperatingConfig::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "BaseSpeed")
    {
        auto& v = std::get<uint32_t>(val);
        baseSpeed(v, skipSignal);
        return;
    }
    if (_name == "BaseSpeedPrioritySettings")
    {
        auto& v = std::get<std::vector<std::tuple<uint32_t, std::vector<uint32_t>>>>(val);
        baseSpeedPrioritySettings(v, skipSignal);
        return;
    }
    if (_name == "MaxJunctionTemperature")
    {
        auto& v = std::get<uint32_t>(val);
        maxJunctionTemperature(v, skipSignal);
        return;
    }
    if (_name == "MaxSpeed")
    {
        auto& v = std::get<uint32_t>(val);
        maxSpeed(v, skipSignal);
        return;
    }
    if (_name == "PowerLimit")
    {
        auto& v = std::get<uint32_t>(val);
        powerLimit(v, skipSignal);
        return;
    }
    if (_name == "AvailableCoreCount")
    {
        auto& v = std::get<size_t>(val);
        availableCoreCount(v, skipSignal);
        return;
    }
    if (_name == "TurboProfile")
    {
        auto& v = std::get<std::vector<std::tuple<uint32_t, size_t>>>(val);
        turboProfile(v, skipSignal);
        return;
    }
}

auto OperatingConfig::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "BaseSpeed")
    {
        return baseSpeed();
    }
    if (_name == "BaseSpeedPrioritySettings")
    {
        return baseSpeedPrioritySettings();
    }
    if (_name == "MaxJunctionTemperature")
    {
        return maxJunctionTemperature();
    }
    if (_name == "MaxSpeed")
    {
        return maxSpeed();
    }
    if (_name == "PowerLimit")
    {
        return powerLimit();
    }
    if (_name == "AvailableCoreCount")
    {
        return availableCoreCount();
    }
    if (_name == "TurboProfile")
    {
        return turboProfile();
    }

    return PropertiesVariant();
}


const vtable_t OperatingConfig::_vtable[] = {
    vtable::start(),
    vtable::property("BaseSpeed",
                     details::OperatingConfig::_property_BaseSpeed
                        .data(),
                     _callback_get_BaseSpeed,
                     vtable::property_::emits_change),
    vtable::property("BaseSpeedPrioritySettings",
                     details::OperatingConfig::_property_BaseSpeedPrioritySettings
                        .data(),
                     _callback_get_BaseSpeedPrioritySettings,
                     vtable::property_::emits_change),
    vtable::property("MaxJunctionTemperature",
                     details::OperatingConfig::_property_MaxJunctionTemperature
                        .data(),
                     _callback_get_MaxJunctionTemperature,
                     vtable::property_::emits_change),
    vtable::property("MaxSpeed",
                     details::OperatingConfig::_property_MaxSpeed
                        .data(),
                     _callback_get_MaxSpeed,
                     vtable::property_::emits_change),
    vtable::property("PowerLimit",
                     details::OperatingConfig::_property_PowerLimit
                        .data(),
                     _callback_get_PowerLimit,
                     vtable::property_::emits_change),
    vtable::property("AvailableCoreCount",
                     details::OperatingConfig::_property_AvailableCoreCount
                        .data(),
                     _callback_get_AvailableCoreCount,
                     vtable::property_::emits_change),
    vtable::property("TurboProfile",
                     details::OperatingConfig::_property_TurboProfile
                        .data(),
                     _callback_get_TurboProfile,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Cpu
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

