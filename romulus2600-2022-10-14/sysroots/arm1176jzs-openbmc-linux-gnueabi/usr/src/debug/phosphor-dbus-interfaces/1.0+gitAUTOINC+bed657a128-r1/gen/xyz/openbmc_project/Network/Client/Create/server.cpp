#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/Client/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Client
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_Client_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_Client(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& address, uint16_t&& port)
                    {
                        return o->client(
                                address, port);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_Client =
        utility::tuple_to_array(message::types::type_id<
                std::string, uint16_t>());
static const auto _return_Client =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
}
}




const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("Client",
                   details::Create::_param_Client
                        .data(),
                   details::Create::_return_Client
                        .data(),
                   _callback_Client),
    vtable::end()
};

} // namespace server
} // namespace Client
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

