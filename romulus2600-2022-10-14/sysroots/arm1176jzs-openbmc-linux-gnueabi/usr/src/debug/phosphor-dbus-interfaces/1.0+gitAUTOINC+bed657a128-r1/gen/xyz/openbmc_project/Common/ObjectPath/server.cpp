#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Common/ObjectPath/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace server
{

ObjectPath::ObjectPath(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Common_ObjectPath_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

ObjectPath::ObjectPath(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : ObjectPath(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto ObjectPath::path() const ->
        std::string
{
    return _path;
}

int ObjectPath::_callback_get_Path(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ObjectPath*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->path();
                    }
                ));
    }
}

auto ObjectPath::path(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_path != value)
    {
        _path = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Common_ObjectPath_interface.property_changed("Path");
        }
    }

    return _path;
}

auto ObjectPath::path(std::string val) ->
        std::string
{
    return path(val, false);
}

int ObjectPath::_callback_set_Path(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<ObjectPath*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->path(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace ObjectPath
{
static const auto _property_Path =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void ObjectPath::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Path")
    {
        auto& v = std::get<std::string>(val);
        path(v, skipSignal);
        return;
    }
}

auto ObjectPath::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Path")
    {
        return path();
    }

    return PropertiesVariant();
}


const vtable_t ObjectPath::_vtable[] = {
    vtable::start(),
    vtable::property("Path",
                     details::ObjectPath::_property_Path
                        .data(),
                     _callback_get_Path,
                     _callback_set_Path,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

