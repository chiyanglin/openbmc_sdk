#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

class CRP0
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        CRP0() = delete;
        CRP0(const CRP0&) = delete;
        CRP0& operator=(const CRP0&) = delete;
        CRP0(CRP0&&) = delete;
        CRP0& operator=(CRP0&&) = delete;
        virtual ~CRP0() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        CRP0(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::vector<uint8_t>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        CRP0(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of RT */
        virtual std::vector<uint8_t> rt() const;
        /** Set value of RT with option to skip sending signal */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of RT */
        virtual std::vector<uint8_t> rt(std::vector<uint8_t> value);
        /** Get value of VD */
        virtual std::vector<uint8_t> vd() const;
        /** Set value of VD with option to skip sending signal */
        virtual std::vector<uint8_t> vd(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of VD */
        virtual std::vector<uint8_t> vd(std::vector<uint8_t> value);
        /** Get value of ED */
        virtual std::vector<uint8_t> ed() const;
        /** Set value of ED with option to skip sending signal */
        virtual std::vector<uint8_t> ed(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of ED */
        virtual std::vector<uint8_t> ed(std::vector<uint8_t> value);
        /** Get value of TE */
        virtual std::vector<uint8_t> te() const;
        /** Set value of TE with option to skip sending signal */
        virtual std::vector<uint8_t> te(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of TE */
        virtual std::vector<uint8_t> te(std::vector<uint8_t> value);
        /** Get value of DD */
        virtual std::vector<uint8_t> dd() const;
        /** Set value of DD with option to skip sending signal */
        virtual std::vector<uint8_t> dd(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DD */
        virtual std::vector<uint8_t> dd(std::vector<uint8_t> value);
        /** Get value of DN */
        virtual std::vector<uint8_t> dn() const;
        /** Set value of DN with option to skip sending signal */
        virtual std::vector<uint8_t> dn(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DN */
        virtual std::vector<uint8_t> dn(std::vector<uint8_t> value);
        /** Get value of IQ */
        virtual std::vector<uint8_t> iq() const;
        /** Set value of IQ with option to skip sending signal */
        virtual std::vector<uint8_t> iq(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of IQ */
        virtual std::vector<uint8_t> iq(std::vector<uint8_t> value);
        /** Get value of TC */
        virtual std::vector<uint8_t> tc() const;
        /** Set value of TC with option to skip sending signal */
        virtual std::vector<uint8_t> tc(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of TC */
        virtual std::vector<uint8_t> tc(std::vector<uint8_t> value);
        /** Get value of TA */
        virtual std::vector<uint8_t> ta() const;
        /** Set value of TA with option to skip sending signal */
        virtual std::vector<uint8_t> ta(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of TA */
        virtual std::vector<uint8_t> ta(std::vector<uint8_t> value);
        /** Get value of DO */
        virtual std::vector<uint8_t> do_() const;
        /** Set value of DO with option to skip sending signal */
        virtual std::vector<uint8_t> do_(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of DO */
        virtual std::vector<uint8_t> do_(std::vector<uint8_t> value);
        /** Get value of PD_W */
        virtual std::vector<uint8_t> pdw() const;
        /** Set value of PD_W with option to skip sending signal */
        virtual std::vector<uint8_t> pdw(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PD_W */
        virtual std::vector<uint8_t> pdw(std::vector<uint8_t> value);
        /** Get value of PD_V */
        virtual std::vector<uint8_t> pdv() const;
        /** Set value of PD_V with option to skip sending signal */
        virtual std::vector<uint8_t> pdv(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of PD_V */
        virtual std::vector<uint8_t> pdv(std::vector<uint8_t> value);
        /** Get value of D4 */
        virtual std::vector<uint8_t> d4() const;
        /** Set value of D4 with option to skip sending signal */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D4 */
        virtual std::vector<uint8_t> d4(std::vector<uint8_t> value);
        /** Get value of D5 */
        virtual std::vector<uint8_t> d5() const;
        /** Set value of D5 with option to skip sending signal */
        virtual std::vector<uint8_t> d5(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D5 */
        virtual std::vector<uint8_t> d5(std::vector<uint8_t> value);
        /** Get value of D8 */
        virtual std::vector<uint8_t> d8() const;
        /** Set value of D8 with option to skip sending signal */
        virtual std::vector<uint8_t> d8(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of D8 */
        virtual std::vector<uint8_t> d8(std::vector<uint8_t> value);
        /** Get value of F5 */
        virtual std::vector<uint8_t> f5() const;
        /** Set value of F5 with option to skip sending signal */
        virtual std::vector<uint8_t> f5(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of F5 */
        virtual std::vector<uint8_t> f5(std::vector<uint8_t> value);
        /** Get value of CI */
        virtual std::vector<uint8_t> ci() const;
        /** Set value of CI with option to skip sending signal */
        virtual std::vector<uint8_t> ci(std::vector<uint8_t> value,
               bool skipSignal);
        /** Set value of CI */
        virtual std::vector<uint8_t> ci(std::vector<uint8_t> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _com_ibm_ipzvpd_CRP0_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _com_ibm_ipzvpd_CRP0_interface.emit_removed();
        }

        static constexpr auto interface = "com.ibm.ipzvpd.CRP0";

    private:

        /** @brief sd-bus callback for get-property 'RT' */
        static int _callback_get_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'RT' */
        static int _callback_set_RT(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VD' */
        static int _callback_get_VD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VD' */
        static int _callback_set_VD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ED' */
        static int _callback_get_ED(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ED' */
        static int _callback_set_ED(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TE' */
        static int _callback_get_TE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TE' */
        static int _callback_set_TE(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DD' */
        static int _callback_get_DD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DD' */
        static int _callback_set_DD(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DN' */
        static int _callback_get_DN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DN' */
        static int _callback_set_DN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IQ' */
        static int _callback_get_IQ(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IQ' */
        static int _callback_set_IQ(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TC' */
        static int _callback_get_TC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TC' */
        static int _callback_set_TC(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'TA' */
        static int _callback_get_TA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'TA' */
        static int _callback_set_TA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DO' */
        static int _callback_get_DO(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DO' */
        static int _callback_set_DO(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PD_W' */
        static int _callback_get_PD_W(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PD_W' */
        static int _callback_set_PD_W(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PD_V' */
        static int _callback_get_PD_V(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PD_V' */
        static int _callback_set_PD_V(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D4' */
        static int _callback_get_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D4' */
        static int _callback_set_D4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D5' */
        static int _callback_get_D5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D5' */
        static int _callback_set_D5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'D8' */
        static int _callback_get_D8(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'D8' */
        static int _callback_set_D8(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'F5' */
        static int _callback_get_F5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'F5' */
        static int _callback_set_F5(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CI' */
        static int _callback_get_CI(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CI' */
        static int _callback_set_CI(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _com_ibm_ipzvpd_CRP0_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<uint8_t> _rt{};
        std::vector<uint8_t> _vd{};
        std::vector<uint8_t> _ed{};
        std::vector<uint8_t> _te{};
        std::vector<uint8_t> _dd{};
        std::vector<uint8_t> _dn{};
        std::vector<uint8_t> _iq{};
        std::vector<uint8_t> _tc{};
        std::vector<uint8_t> _ta{};
        std::vector<uint8_t> _do_{};
        std::vector<uint8_t> _pdw{};
        std::vector<uint8_t> _pdv{};
        std::vector<uint8_t> _d4{};
        std::vector<uint8_t> _d5{};
        std::vector<uint8_t> _d8{};
        std::vector<uint8_t> _f5{};
        std::vector<uint8_t> _ci{};

};


} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

