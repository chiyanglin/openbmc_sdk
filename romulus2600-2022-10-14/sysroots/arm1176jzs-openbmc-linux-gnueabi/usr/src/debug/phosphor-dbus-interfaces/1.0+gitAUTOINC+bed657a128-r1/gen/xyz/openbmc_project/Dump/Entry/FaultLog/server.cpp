#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Dump/Entry/FaultLog/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Entry
{
namespace server
{

FaultLog::FaultLog(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Dump_Entry_FaultLog_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FaultLog::FaultLog(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FaultLog(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FaultLog::type() const ->
        FaultDataType
{
    return _type;
}

int FaultLog::_callback_get_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FaultLog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->type();
                    }
                ));
    }
}

auto FaultLog::type(FaultDataType value,
                                         bool skipSignal) ->
        FaultDataType
{
    if (_type != value)
    {
        _type = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_FaultLog_interface.property_changed("Type");
        }
    }

    return _type;
}

auto FaultLog::type(FaultDataType val) ->
        FaultDataType
{
    return type(val, false);
}


namespace details
{
namespace FaultLog
{
static const auto _property_Type =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Dump::Entry::server::FaultLog::FaultDataType>());
}
}

auto FaultLog::additionalTypeName() const ->
        std::string
{
    return _additionalTypeName;
}

int FaultLog::_callback_get_AdditionalTypeName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FaultLog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->additionalTypeName();
                    }
                ));
    }
}

auto FaultLog::additionalTypeName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_additionalTypeName != value)
    {
        _additionalTypeName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_FaultLog_interface.property_changed("AdditionalTypeName");
        }
    }

    return _additionalTypeName;
}

auto FaultLog::additionalTypeName(std::string val) ->
        std::string
{
    return additionalTypeName(val, false);
}


namespace details
{
namespace FaultLog
{
static const auto _property_AdditionalTypeName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto FaultLog::primaryLogId() const ->
        std::string
{
    return _primaryLogId;
}

int FaultLog::_callback_get_PrimaryLogId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FaultLog*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->primaryLogId();
                    }
                ));
    }
}

auto FaultLog::primaryLogId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_primaryLogId != value)
    {
        _primaryLogId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_FaultLog_interface.property_changed("PrimaryLogId");
        }
    }

    return _primaryLogId;
}

auto FaultLog::primaryLogId(std::string val) ->
        std::string
{
    return primaryLogId(val, false);
}


namespace details
{
namespace FaultLog
{
static const auto _property_PrimaryLogId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void FaultLog::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Type")
    {
        auto& v = std::get<FaultDataType>(val);
        type(v, skipSignal);
        return;
    }
    if (_name == "AdditionalTypeName")
    {
        auto& v = std::get<std::string>(val);
        additionalTypeName(v, skipSignal);
        return;
    }
    if (_name == "PrimaryLogId")
    {
        auto& v = std::get<std::string>(val);
        primaryLogId(v, skipSignal);
        return;
    }
}

auto FaultLog::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Type")
    {
        return type();
    }
    if (_name == "AdditionalTypeName")
    {
        return additionalTypeName();
    }
    if (_name == "PrimaryLogId")
    {
        return primaryLogId();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for FaultLog::FaultDataType */
static const std::tuple<const char*, FaultLog::FaultDataType> mappingFaultLogFaultDataType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Dump.Entry.FaultLog.FaultDataType.CPER",                 FaultLog::FaultDataType::CPER ),
            std::make_tuple( "xyz.openbmc_project.Dump.Entry.FaultLog.FaultDataType.Crashdump",                 FaultLog::FaultDataType::Crashdump ),
        };

} // anonymous namespace

auto FaultLog::convertStringToFaultDataType(const std::string& s) noexcept ->
        std::optional<FaultDataType>
{
    auto i = std::find_if(
            std::begin(mappingFaultLogFaultDataType),
            std::end(mappingFaultLogFaultDataType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingFaultLogFaultDataType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto FaultLog::convertFaultDataTypeFromString(const std::string& s) ->
        FaultDataType
{
    auto r = convertStringToFaultDataType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string FaultLog::convertFaultDataTypeToString(FaultLog::FaultDataType v)
{
    auto i = std::find_if(
            std::begin(mappingFaultLogFaultDataType),
            std::end(mappingFaultLogFaultDataType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingFaultLogFaultDataType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t FaultLog::_vtable[] = {
    vtable::start(),
    vtable::property("Type",
                     details::FaultLog::_property_Type
                        .data(),
                     _callback_get_Type,
                     vtable::property_::const_),
    vtable::property("AdditionalTypeName",
                     details::FaultLog::_property_AdditionalTypeName
                        .data(),
                     _callback_get_AdditionalTypeName,
                     vtable::property_::const_),
    vtable::property("PrimaryLogId",
                     details::FaultLog::_property_PrimaryLogId
                        .data(),
                     _callback_get_PrimaryLogId,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

