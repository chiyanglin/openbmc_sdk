#include <xyz/openbmc_project/Sensor/Threshold/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace Error
{
const char* CriticalHigh::name() const noexcept
{
    return errName;
}
const char* CriticalHigh::description() const noexcept
{
    return errDesc;
}
const char* CriticalHigh::what() const noexcept
{
    return errWhat;
}
const char* CriticalLow::name() const noexcept
{
    return errName;
}
const char* CriticalLow::description() const noexcept
{
    return errDesc;
}
const char* CriticalLow::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

