#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace Device
{
namespace Error
{

struct ReadFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Device.Error.ReadFailure";
    static constexpr auto errDesc =
            "Failed to read from the device.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Device.Error.ReadFailure: Failed to read from the device.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct WriteFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.Common.Device.Error.WriteFailure";
    static constexpr auto errDesc =
            "Failed to write to device.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.Common.Device.Error.WriteFailure: Failed to write to device.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Device
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

