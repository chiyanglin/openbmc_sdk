#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>







#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace server
{

class Attributes
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Attributes() = delete;
        Attributes(const Attributes&) = delete;
        Attributes& operator=(const Attributes&) = delete;
        Attributes(Attributes&&) = delete;
        Attributes& operator=(Attributes&&) = delete;
        virtual ~Attributes() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Attributes(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string,
                std::vector<std::string>>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Attributes(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of UserGroups */
        virtual std::vector<std::string> userGroups() const;
        /** Set value of UserGroups with option to skip sending signal */
        virtual std::vector<std::string> userGroups(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of UserGroups */
        virtual std::vector<std::string> userGroups(std::vector<std::string> value);
        /** Get value of UserPrivilege */
        virtual std::string userPrivilege() const;
        /** Set value of UserPrivilege with option to skip sending signal */
        virtual std::string userPrivilege(std::string value,
               bool skipSignal);
        /** Set value of UserPrivilege */
        virtual std::string userPrivilege(std::string value);
        /** Get value of UserEnabled */
        virtual bool userEnabled() const;
        /** Set value of UserEnabled with option to skip sending signal */
        virtual bool userEnabled(bool value,
               bool skipSignal);
        /** Set value of UserEnabled */
        virtual bool userEnabled(bool value);
        /** Get value of UserLockedForFailedAttempt */
        virtual bool userLockedForFailedAttempt() const;
        /** Set value of UserLockedForFailedAttempt with option to skip sending signal */
        virtual bool userLockedForFailedAttempt(bool value,
               bool skipSignal);
        /** Set value of UserLockedForFailedAttempt */
        virtual bool userLockedForFailedAttempt(bool value);
        /** Get value of RemoteUser */
        virtual bool remoteUser() const;
        /** Set value of RemoteUser with option to skip sending signal */
        virtual bool remoteUser(bool value,
               bool skipSignal);
        /** Set value of RemoteUser */
        virtual bool remoteUser(bool value);
        /** Get value of UserPasswordExpired */
        virtual bool userPasswordExpired() const;
        /** Set value of UserPasswordExpired with option to skip sending signal */
        virtual bool userPasswordExpired(bool value,
               bool skipSignal);
        /** Set value of UserPasswordExpired */
        virtual bool userPasswordExpired(bool value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_User_Attributes_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_User_Attributes_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.User.Attributes";

    private:

        /** @brief sd-bus callback for get-property 'UserGroups' */
        static int _callback_get_UserGroups(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UserGroups' */
        static int _callback_set_UserGroups(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UserPrivilege' */
        static int _callback_get_UserPrivilege(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UserPrivilege' */
        static int _callback_set_UserPrivilege(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UserEnabled' */
        static int _callback_get_UserEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UserEnabled' */
        static int _callback_set_UserEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UserLockedForFailedAttempt' */
        static int _callback_get_UserLockedForFailedAttempt(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UserLockedForFailedAttempt' */
        static int _callback_set_UserLockedForFailedAttempt(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'RemoteUser' */
        static int _callback_get_RemoteUser(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UserPasswordExpired' */
        static int _callback_get_UserPasswordExpired(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_User_Attributes_interface;
        sdbusplus::SdBusInterface *_intf;

        std::vector<std::string> _userGroups{};
        std::string _userPrivilege{};
        bool _userEnabled{};
        bool _userLockedForFailedAttempt{};
        bool _remoteUser{};
        bool _userPasswordExpired{};

};


} // namespace server
} // namespace User
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

