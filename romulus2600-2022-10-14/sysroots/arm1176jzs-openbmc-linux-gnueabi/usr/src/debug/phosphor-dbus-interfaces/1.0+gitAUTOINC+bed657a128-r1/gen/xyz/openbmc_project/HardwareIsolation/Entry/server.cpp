#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/HardwareIsolation/Entry/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace HardwareIsolation
{
namespace server
{

Entry::Entry(bus_t& bus, const char* path)
        : _xyz_openbmc_project_HardwareIsolation_Entry_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Entry::Entry(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Entry(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Entry::severity() const ->
        Type
{
    return _severity;
}

int Entry::_callback_get_Severity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->severity();
                    }
                ));
    }
}

auto Entry::severity(Type value,
                                         bool skipSignal) ->
        Type
{
    if (_severity != value)
    {
        _severity = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_HardwareIsolation_Entry_interface.property_changed("Severity");
        }
    }

    return _severity;
}

auto Entry::severity(Type val) ->
        Type
{
    return severity(val, false);
}

int Entry::_callback_set_Severity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Type&& arg)
                    {
                        o->severity(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Severity =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::HardwareIsolation::server::Entry::Type>());
}
}

auto Entry::resolved() const ->
        bool
{
    return _resolved;
}

int Entry::_callback_get_Resolved(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->resolved();
                    }
                ));
    }
}

auto Entry::resolved(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_resolved != value)
    {
        _resolved = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_HardwareIsolation_Entry_interface.property_changed("Resolved");
        }
    }

    return _resolved;
}

auto Entry::resolved(bool val) ->
        bool
{
    return resolved(val, false);
}

int Entry::_callback_set_Resolved(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Entry*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->resolved(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Entry
{
static const auto _property_Resolved =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Entry::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Severity")
    {
        auto& v = std::get<Type>(val);
        severity(v, skipSignal);
        return;
    }
    if (_name == "Resolved")
    {
        auto& v = std::get<bool>(val);
        resolved(v, skipSignal);
        return;
    }
}

auto Entry::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Severity")
    {
        return severity();
    }
    if (_name == "Resolved")
    {
        return resolved();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Entry::Type */
static const std::tuple<const char*, Entry::Type> mappingEntryType[] =
        {
            std::make_tuple( "xyz.openbmc_project.HardwareIsolation.Entry.Type.Critical",                 Entry::Type::Critical ),
            std::make_tuple( "xyz.openbmc_project.HardwareIsolation.Entry.Type.Warning",                 Entry::Type::Warning ),
            std::make_tuple( "xyz.openbmc_project.HardwareIsolation.Entry.Type.Manual",                 Entry::Type::Manual ),
        };

} // anonymous namespace

auto Entry::convertStringToType(const std::string& s) noexcept ->
        std::optional<Type>
{
    auto i = std::find_if(
            std::begin(mappingEntryType),
            std::end(mappingEntryType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingEntryType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Entry::convertTypeFromString(const std::string& s) ->
        Type
{
    auto r = convertStringToType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Entry::convertTypeToString(Entry::Type v)
{
    auto i = std::find_if(
            std::begin(mappingEntryType),
            std::end(mappingEntryType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingEntryType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Entry::_vtable[] = {
    vtable::start(),
    vtable::property("Severity",
                     details::Entry::_property_Severity
                        .data(),
                     _callback_get_Severity,
                     _callback_set_Severity,
                     vtable::property_::emits_change),
    vtable::property("Resolved",
                     details::Entry::_property_Resolved
                        .data(),
                     _callback_get_Resolved,
                     _callback_set_Resolved,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace HardwareIsolation
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

