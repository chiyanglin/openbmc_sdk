#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>









#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace User
{
namespace Ldap
{
namespace server
{

class Config
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Config() = delete;
        Config(const Config&) = delete;
        Config& operator=(const Config&) = delete;
        Config(Config&&) = delete;
        Config& operator=(Config&&) = delete;
        virtual ~Config() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Config(bus_t& bus, const char* path);

        enum class SearchScope
        {
            sub,
            one,
            base,
        };
        enum class Type
        {
            ActiveDirectory,
            OpenLdap,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                SearchScope,
                Type,
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        Config(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of LDAPServerURI */
        virtual std::string ldapServerURI() const;
        /** Set value of LDAPServerURI with option to skip sending signal */
        virtual std::string ldapServerURI(std::string value,
               bool skipSignal);
        /** Set value of LDAPServerURI */
        virtual std::string ldapServerURI(std::string value);
        /** Get value of LDAPBindDN */
        virtual std::string ldapBindDN() const;
        /** Set value of LDAPBindDN with option to skip sending signal */
        virtual std::string ldapBindDN(std::string value,
               bool skipSignal);
        /** Set value of LDAPBindDN */
        virtual std::string ldapBindDN(std::string value);
        /** Get value of LDAPBindDNPassword */
        virtual std::string ldapBindDNPassword() const;
        /** Set value of LDAPBindDNPassword with option to skip sending signal */
        virtual std::string ldapBindDNPassword(std::string value,
               bool skipSignal);
        /** Set value of LDAPBindDNPassword */
        virtual std::string ldapBindDNPassword(std::string value);
        /** Get value of LDAPBaseDN */
        virtual std::string ldapBaseDN() const;
        /** Set value of LDAPBaseDN with option to skip sending signal */
        virtual std::string ldapBaseDN(std::string value,
               bool skipSignal);
        /** Set value of LDAPBaseDN */
        virtual std::string ldapBaseDN(std::string value);
        /** Get value of LDAPSearchScope */
        virtual SearchScope ldapSearchScope() const;
        /** Set value of LDAPSearchScope with option to skip sending signal */
        virtual SearchScope ldapSearchScope(SearchScope value,
               bool skipSignal);
        /** Set value of LDAPSearchScope */
        virtual SearchScope ldapSearchScope(SearchScope value);
        /** Get value of LDAPType */
        virtual Type ldapType() const;
        /** Set value of LDAPType with option to skip sending signal */
        virtual Type ldapType(Type value,
               bool skipSignal);
        /** Set value of LDAPType */
        virtual Type ldapType(Type value);
        /** Get value of GroupNameAttribute */
        virtual std::string groupNameAttribute() const;
        /** Set value of GroupNameAttribute with option to skip sending signal */
        virtual std::string groupNameAttribute(std::string value,
               bool skipSignal);
        /** Set value of GroupNameAttribute */
        virtual std::string groupNameAttribute(std::string value);
        /** Get value of UserNameAttribute */
        virtual std::string userNameAttribute() const;
        /** Set value of UserNameAttribute with option to skip sending signal */
        virtual std::string userNameAttribute(std::string value,
               bool skipSignal);
        /** Set value of UserNameAttribute */
        virtual std::string userNameAttribute(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Config.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static SearchScope convertSearchScopeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Config.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<SearchScope> convertStringToSearchScope(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.User.Ldap.Config.<value name>"
         */
        static std::string convertSearchScopeToString(SearchScope e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Config.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Type convertTypeFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.User.Ldap.Config.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Type> convertStringToType(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.User.Ldap.Config.<value name>"
         */
        static std::string convertTypeToString(Type e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_User_Ldap_Config_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.User.Ldap.Config";

    private:

        /** @brief sd-bus callback for get-property 'LDAPServerURI' */
        static int _callback_get_LDAPServerURI(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LDAPServerURI' */
        static int _callback_set_LDAPServerURI(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LDAPBindDN' */
        static int _callback_get_LDAPBindDN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LDAPBindDN' */
        static int _callback_set_LDAPBindDN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LDAPBindDNPassword' */
        static int _callback_get_LDAPBindDNPassword(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LDAPBindDNPassword' */
        static int _callback_set_LDAPBindDNPassword(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LDAPBaseDN' */
        static int _callback_get_LDAPBaseDN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LDAPBaseDN' */
        static int _callback_set_LDAPBaseDN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LDAPSearchScope' */
        static int _callback_get_LDAPSearchScope(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LDAPSearchScope' */
        static int _callback_set_LDAPSearchScope(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LDAPType' */
        static int _callback_get_LDAPType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LDAPType' */
        static int _callback_set_LDAPType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'GroupNameAttribute' */
        static int _callback_get_GroupNameAttribute(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'GroupNameAttribute' */
        static int _callback_set_GroupNameAttribute(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'UserNameAttribute' */
        static int _callback_get_UserNameAttribute(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UserNameAttribute' */
        static int _callback_set_UserNameAttribute(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_User_Ldap_Config_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _ldapServerURI{};
        std::string _ldapBindDN{};
        std::string _ldapBindDNPassword{};
        std::string _ldapBaseDN{};
        SearchScope _ldapSearchScope = SearchScope::sub;
        Type _ldapType{};
        std::string _groupNameAttribute{};
        std::string _userNameAttribute{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Config::SearchScope.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Config::SearchScope e)
{
    return Config::convertSearchScopeToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Config::Type.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Config::Type e)
{
    return Config::convertTypeToString(e);
}

} // namespace server
} // namespace Ldap
} // namespace User
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::User::Ldap::server::Config::SearchScope>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::User::Ldap::server::Config::convertStringToSearchScope(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::User::Ldap::server::Config::SearchScope>
{
    static std::string op(xyz::openbmc_project::User::Ldap::server::Config::SearchScope value)
    {
        return xyz::openbmc_project::User::Ldap::server::Config::convertSearchScopeToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::User::Ldap::server::Config::Type>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::User::Ldap::server::Config::convertStringToType(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::User::Ldap::server::Config::Type>
{
    static std::string op(xyz::openbmc_project::User::Ldap::server::Config::Type value)
    {
        return xyz::openbmc_project::User::Ldap::server::Config::convertTypeToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

