#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Dump/Entry/System/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Entry
{
namespace server
{

System::System(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Dump_Entry_System_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

System::System(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : System(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto System::sourceDumpId() const ->
        uint32_t
{
    return _sourceDumpId;
}

int System::_callback_get_SourceDumpId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<System*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sourceDumpId();
                    }
                ));
    }
}

auto System::sourceDumpId(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_sourceDumpId != value)
    {
        _sourceDumpId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Dump_Entry_System_interface.property_changed("SourceDumpId");
        }
    }

    return _sourceDumpId;
}

auto System::sourceDumpId(uint32_t val) ->
        uint32_t
{
    return sourceDumpId(val, false);
}

int System::_callback_set_SourceDumpId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<System*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->sourceDumpId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace System
{
static const auto _property_SourceDumpId =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void System::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SourceDumpId")
    {
        auto& v = std::get<uint32_t>(val);
        sourceDumpId(v, skipSignal);
        return;
    }
}

auto System::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SourceDumpId")
    {
        return sourceDumpId();
    }

    return PropertiesVariant();
}


const vtable_t System::_vtable[] = {
    vtable::start(),
    vtable::property("SourceDumpId",
                     details::System::_property_SourceDumpId
                        .data(),
                     _callback_get_SourceDumpId,
                     _callback_set_SourceDumpId,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

