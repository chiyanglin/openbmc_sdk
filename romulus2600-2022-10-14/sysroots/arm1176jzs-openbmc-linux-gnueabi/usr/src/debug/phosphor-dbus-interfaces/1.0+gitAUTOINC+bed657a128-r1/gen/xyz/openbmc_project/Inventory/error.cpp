#include <xyz/openbmc_project/Inventory/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Error
{
const char* NotPresent::name() const noexcept
{
    return errName;
}
const char* NotPresent::description() const noexcept
{
    return errDesc;
}
const char* NotPresent::what() const noexcept
{
    return errWhat;
}
const char* Nonfunctional::name() const noexcept
{
    return errName;
}
const char* Nonfunctional::description() const noexcept
{
    return errDesc;
}
const char* Nonfunctional::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

