#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Smbios/MDR_V2/server.hpp>

#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>

#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>

#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>

#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>

#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>

#include <xyz/openbmc_project/Smbios/MDR_V2/error.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Smbios
{
namespace server
{

MDR_V2::MDR_V2(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Smbios_MDR_V2_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

MDR_V2::MDR_V2(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : MDR_V2(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int MDR_V2::_callback_GetDirectoryInformation(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& dirIndex)
                    {
                        return o->getDirectoryInformation(
                                dirIndex);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Smbios::MDR_V2::Error::InvalidParameter& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_GetDirectoryInformation =
        utility::tuple_to_array(message::types::type_id<
                uint8_t>());
static const auto _return_GetDirectoryInformation =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int MDR_V2::_callback_GetDataInformation(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& idIndex)
                    {
                        return o->getDataInformation(
                                idIndex);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Smbios::MDR_V2::Error::InvalidParameter& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_GetDataInformation =
        utility::tuple_to_array(message::types::type_id<
                uint8_t>());
static const auto _return_GetDataInformation =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int MDR_V2::_callback_SendDirectoryInformation(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& dirVersion, uint8_t&& dirIndex, uint8_t&& returnedEntries, uint8_t&& remainingEntries, std::vector<uint8_t>&& dirEntry)
                    {
                        return o->sendDirectoryInformation(
                                dirVersion, dirIndex, returnedEntries, remainingEntries, dirEntry);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Smbios::MDR_V2::Error::InvalidParameter& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_SendDirectoryInformation =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint8_t, uint8_t, uint8_t, std::vector<uint8_t>>());
static const auto _return_SendDirectoryInformation =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}

int MDR_V2::_callback_GetDataOffer(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->getDataOffer(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Smbios::MDR_V2::Error::UpdateInProgress& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_GetDataOffer =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_GetDataOffer =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
}
}

int MDR_V2::_callback_SendDataInformation(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& idIndex, uint8_t&& flag, uint32_t&& dataLen, uint32_t&& dataVer, uint32_t&& timeStamp)
                    {
                        return o->sendDataInformation(
                                idIndex, flag, dataLen, dataVer, timeStamp);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Smbios::MDR_V2::Error::InvalidParameter& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_SendDataInformation =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint8_t, uint32_t, uint32_t, uint32_t>());
static const auto _return_SendDataInformation =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}

int MDR_V2::_callback_FindIdIndex(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& dataInfo)
                    {
                        return o->findIdIndex(
                                dataInfo);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Smbios::MDR_V2::Error::InvalidId& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_FindIdIndex =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint8_t>>());
static const auto _return_FindIdIndex =
        utility::tuple_to_array(message::types::type_id<
                int32_t>());
}
}

int MDR_V2::_callback_AgentSynchronizeData(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->agentSynchronizeData(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_AgentSynchronizeData =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_AgentSynchronizeData =
        utility::tuple_to_array(message::types::type_id<
                bool>());
}
}

int MDR_V2::_callback_SynchronizeDirectoryCommonData(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& idIndex, uint32_t&& size)
                    {
                        return o->synchronizeDirectoryCommonData(
                                idIndex, size);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace MDR_V2
{
static const auto _param_SynchronizeDirectoryCommonData =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint32_t>());
static const auto _return_SynchronizeDirectoryCommonData =
        utility::tuple_to_array(message::types::type_id<
                std::vector<uint32_t>>());
}
}



auto MDR_V2::directoryEntries() const ->
        uint8_t
{
    return _directoryEntries;
}

int MDR_V2::_callback_get_DirectoryEntries(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->directoryEntries();
                    }
                ));
    }
}

auto MDR_V2::directoryEntries(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_directoryEntries != value)
    {
        _directoryEntries = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Smbios_MDR_V2_interface.property_changed("DirectoryEntries");
        }
    }

    return _directoryEntries;
}

auto MDR_V2::directoryEntries(uint8_t val) ->
        uint8_t
{
    return directoryEntries(val, false);
}

int MDR_V2::_callback_set_DirectoryEntries(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<MDR_V2*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->directoryEntries(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace MDR_V2
{
static const auto _property_DirectoryEntries =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

void MDR_V2::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "DirectoryEntries")
    {
        auto& v = std::get<uint8_t>(val);
        directoryEntries(v, skipSignal);
        return;
    }
}

auto MDR_V2::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "DirectoryEntries")
    {
        return directoryEntries();
    }

    return PropertiesVariant();
}


const vtable_t MDR_V2::_vtable[] = {
    vtable::start(),

    vtable::method("GetDirectoryInformation",
                   details::MDR_V2::_param_GetDirectoryInformation
                        .data(),
                   details::MDR_V2::_return_GetDirectoryInformation
                        .data(),
                   _callback_GetDirectoryInformation),

    vtable::method("GetDataInformation",
                   details::MDR_V2::_param_GetDataInformation
                        .data(),
                   details::MDR_V2::_return_GetDataInformation
                        .data(),
                   _callback_GetDataInformation),

    vtable::method("SendDirectoryInformation",
                   details::MDR_V2::_param_SendDirectoryInformation
                        .data(),
                   details::MDR_V2::_return_SendDirectoryInformation
                        .data(),
                   _callback_SendDirectoryInformation),

    vtable::method("GetDataOffer",
                   details::MDR_V2::_param_GetDataOffer
                        .data(),
                   details::MDR_V2::_return_GetDataOffer
                        .data(),
                   _callback_GetDataOffer),

    vtable::method("SendDataInformation",
                   details::MDR_V2::_param_SendDataInformation
                        .data(),
                   details::MDR_V2::_return_SendDataInformation
                        .data(),
                   _callback_SendDataInformation),

    vtable::method("FindIdIndex",
                   details::MDR_V2::_param_FindIdIndex
                        .data(),
                   details::MDR_V2::_return_FindIdIndex
                        .data(),
                   _callback_FindIdIndex),

    vtable::method("AgentSynchronizeData",
                   details::MDR_V2::_param_AgentSynchronizeData
                        .data(),
                   details::MDR_V2::_return_AgentSynchronizeData
                        .data(),
                   _callback_AgentSynchronizeData),

    vtable::method("SynchronizeDirectoryCommonData",
                   details::MDR_V2::_param_SynchronizeDirectoryCommonData
                        .data(),
                   details::MDR_V2::_return_SynchronizeDirectoryCommonData
                        .data(),
                   _callback_SynchronizeDirectoryCommonData),
    vtable::property("DirectoryEntries",
                     details::MDR_V2::_property_DirectoryEntries
                        .data(),
                     _callback_get_DirectoryEntries,
                     _callback_set_DirectoryEntries,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Smbios
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

