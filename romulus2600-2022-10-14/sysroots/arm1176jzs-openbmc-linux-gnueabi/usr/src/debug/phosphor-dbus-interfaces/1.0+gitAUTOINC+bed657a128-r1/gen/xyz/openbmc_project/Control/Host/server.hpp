#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

class Host
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Host() = delete;
        Host(const Host&) = delete;
        Host& operator=(const Host&) = delete;
        Host(Host&&) = delete;
        Host& operator=(Host&&) = delete;
        virtual ~Host() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Host(bus_t& bus, const char* path);

        enum class Command
        {
            SoftOff,
            Heartbeat,
        };
        enum class Result
        {
            Success,
            Failure,
        };


        /** @brief Implementation for Execute
         *  Execute the requested command by the caller. This command will be processed in first in first out order. See the Command enum description below for details on all supported commands.
         *
         *  @param[in] command - Requested command to execute against the host
         */
        virtual void execute(
            Command command) = 0;


        /** @brief Send signal 'CommandComplete'
         *
         *  Signal indicating that a Command has completed
         *
         *  @param[in] command - Executed command
         *  @param[in] result - Result of the command execution
         */
        void commandComplete(
            Command command,
            Result result);


        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Host.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Command convertCommandFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Host.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Command> convertStringToCommand(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.Host.<value name>"
         */
        static std::string convertCommandToString(Command e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Host.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static Result convertResultFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Host.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<Result> convertStringToResult(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.Host.<value name>"
         */
        static std::string convertResultToString(Result e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Host_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Host_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Host";

    private:

        /** @brief sd-bus callback for Execute
         */
        static int _callback_Execute(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Host_interface;
        sdbusplus::SdBusInterface *_intf;


};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Host::Command.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Host::Command e)
{
    return Host::convertCommandToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type Host::Result.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(Host::Result e)
{
    return Host::convertResultToString(e);
}

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Control::server::Host::Command>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::server::Host::convertStringToCommand(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::server::Host::Command>
{
    static std::string op(xyz::openbmc_project::Control::server::Host::Command value)
    {
        return xyz::openbmc_project::Control::server::Host::convertCommandToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Control::server::Host::Result>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::server::Host::convertStringToResult(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::server::Host::Result>
{
    static std::string op(xyz::openbmc_project::Control::server::Host::Result value)
    {
        return xyz::openbmc_project::Control::server::Host::convertResultToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

