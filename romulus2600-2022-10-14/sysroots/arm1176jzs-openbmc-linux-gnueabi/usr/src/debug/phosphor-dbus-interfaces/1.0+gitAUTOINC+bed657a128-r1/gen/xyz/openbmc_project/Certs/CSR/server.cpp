#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/CSR/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

CSR::CSR(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_CSR_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int CSR::_callback_CSR(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<CSR*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->csr(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace CSR
{
static const auto _param_CSR =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_CSR =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
}
}




const vtable_t CSR::_vtable[] = {
    vtable::start(),

    vtable::method("CSR",
                   details::CSR::_param_CSR
                        .data(),
                   details::CSR::_return_CSR
                        .data(),
                   _callback_CSR),
    vtable::end()
};

} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

