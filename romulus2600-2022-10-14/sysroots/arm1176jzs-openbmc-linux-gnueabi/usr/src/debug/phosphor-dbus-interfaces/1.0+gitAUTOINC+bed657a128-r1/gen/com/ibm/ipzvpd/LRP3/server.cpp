#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/LRP3/server.hpp>





namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

LRP3::LRP3(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_LRP3_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

LRP3::LRP3(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : LRP3(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto LRP3::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int LRP3::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto LRP3::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP3_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto LRP3::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int LRP3::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP3
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LRP3::vd() const ->
        std::vector<uint8_t>
{
    return _vd;
}

int LRP3::_callback_get_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vd();
                    }
                ));
    }
}

auto LRP3::vd(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vd != value)
    {
        _vd = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP3_interface.property_changed("VD");
        }
    }

    return _vd;
}

auto LRP3::vd(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vd(val, false);
}

int LRP3::_callback_set_VD(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vd(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP3
{
static const auto _property_VD =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LRP3::tc() const ->
        std::vector<uint8_t>
{
    return _tc;
}

int LRP3::_callback_get_TC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->tc();
                    }
                ));
    }
}

auto LRP3::tc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_tc != value)
    {
        _tc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP3_interface.property_changed("TC");
        }
    }

    return _tc;
}

auto LRP3::tc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return tc(val, false);
}

int LRP3::_callback_set_TC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->tc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP3
{
static const auto _property_TC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LRP3::d4() const ->
        std::vector<uint8_t>
{
    return _d4;
}

int LRP3::_callback_get_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->d4();
                    }
                ));
    }
}

auto LRP3::d4(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_d4 != value)
    {
        _d4 = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LRP3_interface.property_changed("D4");
        }
    }

    return _d4;
}

auto LRP3::d4(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return d4(val, false);
}

int LRP3::_callback_set_D4(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LRP3*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->d4(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LRP3
{
static const auto _property_D4 =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void LRP3::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VD")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vd(v, skipSignal);
        return;
    }
    if (_name == "TC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        tc(v, skipSignal);
        return;
    }
    if (_name == "D4")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        d4(v, skipSignal);
        return;
    }
}

auto LRP3::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VD")
    {
        return vd();
    }
    if (_name == "TC")
    {
        return tc();
    }
    if (_name == "D4")
    {
        return d4();
    }

    return PropertiesVariant();
}


const vtable_t LRP3::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::LRP3::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VD",
                     details::LRP3::_property_VD
                        .data(),
                     _callback_get_VD,
                     _callback_set_VD,
                     vtable::property_::emits_change),
    vtable::property("TC",
                     details::LRP3::_property_TC
                        .data(),
                     _callback_get_TC,
                     _callback_set_TC,
                     vtable::property_::emits_change),
    vtable::property("D4",
                     details::LRP3::_property_D4
                        .data(),
                     _callback_get_D4,
                     _callback_set_D4,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

