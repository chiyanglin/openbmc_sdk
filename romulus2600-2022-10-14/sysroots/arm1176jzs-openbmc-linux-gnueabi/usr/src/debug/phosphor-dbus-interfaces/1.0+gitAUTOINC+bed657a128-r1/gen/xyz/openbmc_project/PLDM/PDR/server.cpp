#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/PLDM/PDR/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace server
{

PDR::PDR(bus_t& bus, const char* path)
        : _xyz_openbmc_project_PLDM_PDR_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int PDR::_callback_FindStateEffecterPDR(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PDR*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& tid, uint16_t&& entityID, uint16_t&& stateSetId)
                    {
                        return o->findStateEffecterPDR(
                                tid, entityID, stateSetId);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PDR
{
static const auto _param_FindStateEffecterPDR =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint16_t, uint16_t>());
static const auto _return_FindStateEffecterPDR =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::vector<uint8_t>>>());
}
}

int PDR::_callback_FindStateSensorPDR(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PDR*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint8_t&& tid, uint16_t&& entityID, uint16_t&& stateSetId)
                    {
                        return o->findStateSensorPDR(
                                tid, entityID, stateSetId);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace PDR
{
static const auto _param_FindStateSensorPDR =
        utility::tuple_to_array(message::types::type_id<
                uint8_t, uint16_t, uint16_t>());
static const auto _return_FindStateSensorPDR =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::vector<uint8_t>>>());
}
}




const vtable_t PDR::_vtable[] = {
    vtable::start(),

    vtable::method("FindStateEffecterPDR",
                   details::PDR::_param_FindStateEffecterPDR
                        .data(),
                   details::PDR::_return_FindStateEffecterPDR
                        .data(),
                   _callback_FindStateEffecterPDR),

    vtable::method("FindStateSensorPDR",
                   details::PDR::_param_FindStateSensorPDR
                        .data(),
                   details::PDR::_return_FindStateSensorPDR
                        .data(),
                   _callback_FindStateSensorPDR),
    vtable::end()
};

} // namespace server
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

