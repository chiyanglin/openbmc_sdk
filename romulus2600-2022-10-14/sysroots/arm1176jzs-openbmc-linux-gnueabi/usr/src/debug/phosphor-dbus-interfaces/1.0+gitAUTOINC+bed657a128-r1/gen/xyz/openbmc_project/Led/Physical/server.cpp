#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Led/Physical/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Led
{
namespace server
{

Physical::Physical(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Led_Physical_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Physical::Physical(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Physical(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Physical::state() const ->
        Action
{
    return _state;
}

int Physical::_callback_get_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->state();
                    }
                ));
    }
}

auto Physical::state(Action value,
                                         bool skipSignal) ->
        Action
{
    if (_state != value)
    {
        _state = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Led_Physical_interface.property_changed("State");
        }
    }

    return _state;
}

auto Physical::state(Action val) ->
        Action
{
    return state(val, false);
}

int Physical::_callback_set_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Action&& arg)
                    {
                        o->state(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Physical
{
static const auto _property_State =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Led::server::Physical::Action>());
}
}

auto Physical::dutyOn() const ->
        uint8_t
{
    return _dutyOn;
}

int Physical::_callback_get_DutyOn(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dutyOn();
                    }
                ));
    }
}

auto Physical::dutyOn(uint8_t value,
                                         bool skipSignal) ->
        uint8_t
{
    if (_dutyOn != value)
    {
        _dutyOn = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Led_Physical_interface.property_changed("DutyOn");
        }
    }

    return _dutyOn;
}

auto Physical::dutyOn(uint8_t val) ->
        uint8_t
{
    return dutyOn(val, false);
}

int Physical::_callback_set_DutyOn(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint8_t&& arg)
                    {
                        o->dutyOn(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Physical
{
static const auto _property_DutyOn =
    utility::tuple_to_array(message::types::type_id<
            uint8_t>());
}
}

auto Physical::color() const ->
        Palette
{
    return _color;
}

int Physical::_callback_get_Color(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->color();
                    }
                ));
    }
}

auto Physical::color(Palette value,
                                         bool skipSignal) ->
        Palette
{
    if (_color != value)
    {
        _color = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Led_Physical_interface.property_changed("Color");
        }
    }

    return _color;
}

auto Physical::color(Palette val) ->
        Palette
{
    return color(val, false);
}

int Physical::_callback_set_Color(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Palette&& arg)
                    {
                        o->color(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Physical
{
static const auto _property_Color =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Led::server::Physical::Palette>());
}
}

auto Physical::period() const ->
        uint16_t
{
    return _period;
}

int Physical::_callback_get_Period(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->period();
                    }
                ));
    }
}

auto Physical::period(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_period != value)
    {
        _period = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Led_Physical_interface.property_changed("Period");
        }
    }

    return _period;
}

auto Physical::period(uint16_t val) ->
        uint16_t
{
    return period(val, false);
}

int Physical::_callback_set_Period(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Physical*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->period(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Physical
{
static const auto _property_Period =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

void Physical::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "State")
    {
        auto& v = std::get<Action>(val);
        state(v, skipSignal);
        return;
    }
    if (_name == "DutyOn")
    {
        auto& v = std::get<uint8_t>(val);
        dutyOn(v, skipSignal);
        return;
    }
    if (_name == "Color")
    {
        auto& v = std::get<Palette>(val);
        color(v, skipSignal);
        return;
    }
    if (_name == "Period")
    {
        auto& v = std::get<uint16_t>(val);
        period(v, skipSignal);
        return;
    }
}

auto Physical::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "State")
    {
        return state();
    }
    if (_name == "DutyOn")
    {
        return dutyOn();
    }
    if (_name == "Color")
    {
        return color();
    }
    if (_name == "Period")
    {
        return period();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Physical::Action */
static const std::tuple<const char*, Physical::Action> mappingPhysicalAction[] =
        {
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Action.Off",                 Physical::Action::Off ),
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Action.On",                 Physical::Action::On ),
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Action.Blink",                 Physical::Action::Blink ),
        };

} // anonymous namespace

auto Physical::convertStringToAction(const std::string& s) noexcept ->
        std::optional<Action>
{
    auto i = std::find_if(
            std::begin(mappingPhysicalAction),
            std::end(mappingPhysicalAction),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPhysicalAction) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Physical::convertActionFromString(const std::string& s) ->
        Action
{
    auto r = convertStringToAction(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Physical::convertActionToString(Physical::Action v)
{
    auto i = std::find_if(
            std::begin(mappingPhysicalAction),
            std::end(mappingPhysicalAction),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPhysicalAction))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Physical::Palette */
static const std::tuple<const char*, Physical::Palette> mappingPhysicalPalette[] =
        {
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Palette.Unknown",                 Physical::Palette::Unknown ),
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Palette.Red",                 Physical::Palette::Red ),
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Palette.Green",                 Physical::Palette::Green ),
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Palette.Blue",                 Physical::Palette::Blue ),
            std::make_tuple( "xyz.openbmc_project.Led.Physical.Palette.Yellow",                 Physical::Palette::Yellow ),
        };

} // anonymous namespace

auto Physical::convertStringToPalette(const std::string& s) noexcept ->
        std::optional<Palette>
{
    auto i = std::find_if(
            std::begin(mappingPhysicalPalette),
            std::end(mappingPhysicalPalette),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingPhysicalPalette) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Physical::convertPaletteFromString(const std::string& s) ->
        Palette
{
    auto r = convertStringToPalette(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Physical::convertPaletteToString(Physical::Palette v)
{
    auto i = std::find_if(
            std::begin(mappingPhysicalPalette),
            std::end(mappingPhysicalPalette),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingPhysicalPalette))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Physical::_vtable[] = {
    vtable::start(),
    vtable::property("State",
                     details::Physical::_property_State
                        .data(),
                     _callback_get_State,
                     _callback_set_State,
                     vtable::property_::emits_change),
    vtable::property("DutyOn",
                     details::Physical::_property_DutyOn
                        .data(),
                     _callback_get_DutyOn,
                     _callback_set_DutyOn,
                     vtable::property_::emits_change),
    vtable::property("Color",
                     details::Physical::_property_Color
                        .data(),
                     _callback_get_Color,
                     _callback_set_Color,
                     vtable::property_::emits_change),
    vtable::property("Period",
                     details::Physical::_property_Period
                        .data(),
                     _callback_get_Period,
                     _callback_set_Period,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Led
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

