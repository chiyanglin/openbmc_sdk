#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Certs/Replace/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Certs/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Certs
{
namespace server
{

Replace::Replace(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Certs_Replace_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Replace::_callback_Replace(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Replace*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& path)
                    {
                        return o->replace(
                                path);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Certs::Error::InvalidCertificate& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Replace
{
static const auto _param_Replace =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_Replace =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t Replace::_vtable[] = {
    vtable::start(),

    vtable::method("Replace",
                   details::Replace::_param_Replace
                        .data(),
                   details::Replace::_return_Replace
                        .data(),
                   _callback_Replace),
    vtable::end()
};

} // namespace server
} // namespace Certs
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

