#pragma once

#include <cerrno>
#include <sdbusplus/exception.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Host
{
namespace Error
{

struct SoftOffTimeout final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Host.Error.SoftOffTimeout";
    static constexpr auto errDesc =
            "Host did not shutdown within configured time.";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Host.Error.SoftOffTimeout: Host did not shutdown within configured time.";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct HostStartFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Host.Error.HostStartFailure";
    static constexpr auto errDesc =
            "The systemd obmc-host-start.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Host.Error.HostStartFailure: The systemd obmc-host-start.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct HostMinStartFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Host.Error.HostMinStartFailure";
    static constexpr auto errDesc =
            "The systemd obmc-host-start-min.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Host.Error.HostMinStartFailure: The systemd obmc-host-start-min.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct HostShutdownFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Host.Error.HostShutdownFailure";
    static constexpr auto errDesc =
            "The systemd obmc-host-shutdown.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Host.Error.HostShutdownFailure: The systemd obmc-host-shutdown.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct HostStopFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Host.Error.HostStopFailure";
    static constexpr auto errDesc =
            "The systemd obmc-host-stop.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Host.Error.HostStopFailure: The systemd obmc-host-stop.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

struct HostRebootFailure final :
        public sdbusplus::exception::generated_exception
{
    static constexpr auto errName = "xyz.openbmc_project.State.Host.Error.HostRebootFailure";
    static constexpr auto errDesc =
            "The systemd obmc-host-reboot.target failed to complete";
    static constexpr auto errWhat =
            "xyz.openbmc_project.State.Host.Error.HostRebootFailure: The systemd obmc-host-reboot.target failed to complete";

    const char* name() const noexcept override;
    const char* description() const noexcept override;
    const char* what() const noexcept override;
};

} // namespace Error
} // namespace Host
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

