#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>





#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

class VoltageRegulatorControl
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        VoltageRegulatorControl() = delete;
        VoltageRegulatorControl(const VoltageRegulatorControl&) = delete;
        VoltageRegulatorControl& operator=(const VoltageRegulatorControl&) = delete;
        VoltageRegulatorControl(VoltageRegulatorControl&&) = delete;
        VoltageRegulatorControl& operator=(VoltageRegulatorControl&&) = delete;
        virtual ~VoltageRegulatorControl() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        VoltageRegulatorControl(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                double>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        VoltageRegulatorControl(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of Voltage */
        virtual double voltage() const;
        /** Set value of Voltage with option to skip sending signal */
        virtual double voltage(double value,
               bool skipSignal);
        /** Set value of Voltage */
        virtual double voltage(double value);
        /** Get value of MaxValue */
        virtual double maxValue() const;
        /** Set value of MaxValue with option to skip sending signal */
        virtual double maxValue(double value,
               bool skipSignal);
        /** Set value of MaxValue */
        virtual double maxValue(double value);
        /** Get value of MinValue */
        virtual double minValue() const;
        /** Set value of MinValue with option to skip sending signal */
        virtual double minValue(double value,
               bool skipSignal);
        /** Set value of MinValue */
        virtual double minValue(double value);
        /** Get value of Resolution */
        virtual double resolution() const;
        /** Set value of Resolution with option to skip sending signal */
        virtual double resolution(double value,
               bool skipSignal);
        /** Set value of Resolution */
        virtual double resolution(double value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_VoltageRegulatorControl_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_VoltageRegulatorControl_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.VoltageRegulatorControl";

    private:

        /** @brief sd-bus callback for get-property 'Voltage' */
        static int _callback_get_Voltage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Voltage' */
        static int _callback_set_Voltage(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxValue' */
        static int _callback_get_MaxValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MinValue' */
        static int _callback_get_MinValue(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Resolution' */
        static int _callback_get_Resolution(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_VoltageRegulatorControl_interface;
        sdbusplus::SdBusInterface *_intf;

        double _voltage{};
        double _maxValue{};
        double _minValue{};
        double _resolution{};

};


} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

