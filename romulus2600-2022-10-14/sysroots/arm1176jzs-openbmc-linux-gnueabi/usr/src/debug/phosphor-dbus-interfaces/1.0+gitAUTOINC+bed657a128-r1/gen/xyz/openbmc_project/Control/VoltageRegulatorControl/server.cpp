#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/VoltageRegulatorControl/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

VoltageRegulatorControl::VoltageRegulatorControl(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_VoltageRegulatorControl_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VoltageRegulatorControl::VoltageRegulatorControl(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VoltageRegulatorControl(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VoltageRegulatorControl::voltage() const ->
        double
{
    return _voltage;
}

int VoltageRegulatorControl::_callback_get_Voltage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorControl*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->voltage();
                    }
                ));
    }
}

auto VoltageRegulatorControl::voltage(double value,
                                         bool skipSignal) ->
        double
{
    if (_voltage != value)
    {
        _voltage = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_VoltageRegulatorControl_interface.property_changed("Voltage");
        }
    }

    return _voltage;
}

auto VoltageRegulatorControl::voltage(double val) ->
        double
{
    return voltage(val, false);
}

int VoltageRegulatorControl::_callback_set_Voltage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorControl*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->voltage(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VoltageRegulatorControl
{
static const auto _property_Voltage =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto VoltageRegulatorControl::maxValue() const ->
        double
{
    return _maxValue;
}

int VoltageRegulatorControl::_callback_get_MaxValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorControl*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxValue();
                    }
                ));
    }
}

auto VoltageRegulatorControl::maxValue(double value,
                                         bool skipSignal) ->
        double
{
    if (_maxValue != value)
    {
        _maxValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_VoltageRegulatorControl_interface.property_changed("MaxValue");
        }
    }

    return _maxValue;
}

auto VoltageRegulatorControl::maxValue(double val) ->
        double
{
    return maxValue(val, false);
}


namespace details
{
namespace VoltageRegulatorControl
{
static const auto _property_MaxValue =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto VoltageRegulatorControl::minValue() const ->
        double
{
    return _minValue;
}

int VoltageRegulatorControl::_callback_get_MinValue(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorControl*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->minValue();
                    }
                ));
    }
}

auto VoltageRegulatorControl::minValue(double value,
                                         bool skipSignal) ->
        double
{
    if (_minValue != value)
    {
        _minValue = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_VoltageRegulatorControl_interface.property_changed("MinValue");
        }
    }

    return _minValue;
}

auto VoltageRegulatorControl::minValue(double val) ->
        double
{
    return minValue(val, false);
}


namespace details
{
namespace VoltageRegulatorControl
{
static const auto _property_MinValue =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto VoltageRegulatorControl::resolution() const ->
        double
{
    return _resolution;
}

int VoltageRegulatorControl::_callback_get_Resolution(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VoltageRegulatorControl*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->resolution();
                    }
                ));
    }
}

auto VoltageRegulatorControl::resolution(double value,
                                         bool skipSignal) ->
        double
{
    if (_resolution != value)
    {
        _resolution = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_VoltageRegulatorControl_interface.property_changed("Resolution");
        }
    }

    return _resolution;
}

auto VoltageRegulatorControl::resolution(double val) ->
        double
{
    return resolution(val, false);
}


namespace details
{
namespace VoltageRegulatorControl
{
static const auto _property_Resolution =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

void VoltageRegulatorControl::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Voltage")
    {
        auto& v = std::get<double>(val);
        voltage(v, skipSignal);
        return;
    }
    if (_name == "MaxValue")
    {
        auto& v = std::get<double>(val);
        maxValue(v, skipSignal);
        return;
    }
    if (_name == "MinValue")
    {
        auto& v = std::get<double>(val);
        minValue(v, skipSignal);
        return;
    }
    if (_name == "Resolution")
    {
        auto& v = std::get<double>(val);
        resolution(v, skipSignal);
        return;
    }
}

auto VoltageRegulatorControl::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Voltage")
    {
        return voltage();
    }
    if (_name == "MaxValue")
    {
        return maxValue();
    }
    if (_name == "MinValue")
    {
        return minValue();
    }
    if (_name == "Resolution")
    {
        return resolution();
    }

    return PropertiesVariant();
}


const vtable_t VoltageRegulatorControl::_vtable[] = {
    vtable::start(),
    vtable::property("Voltage",
                     details::VoltageRegulatorControl::_property_Voltage
                        .data(),
                     _callback_get_Voltage,
                     _callback_set_Voltage,
                     vtable::property_::emits_change),
    vtable::property("MaxValue",
                     details::VoltageRegulatorControl::_property_MaxValue
                        .data(),
                     _callback_get_MaxValue,
                     vtable::property_::emits_change),
    vtable::property("MinValue",
                     details::VoltageRegulatorControl::_property_MinValue
                        .data(),
                     _callback_get_MinValue,
                     vtable::property_::emits_change),
    vtable::property("Resolution",
                     details::VoltageRegulatorControl::_property_Resolution
                        .data(),
                     _callback_get_Resolution,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

