#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/FieldMode/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

FieldMode::FieldMode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_FieldMode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

FieldMode::FieldMode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : FieldMode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto FieldMode::fieldModeEnabled() const ->
        bool
{
    return _fieldModeEnabled;
}

int FieldMode::_callback_get_FieldModeEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FieldMode*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fieldModeEnabled();
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
}

auto FieldMode::fieldModeEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_fieldModeEnabled != value)
    {
        _fieldModeEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_FieldMode_interface.property_changed("FieldModeEnabled");
        }
    }

    return _fieldModeEnabled;
}

auto FieldMode::fieldModeEnabled(bool val) ->
        bool
{
    return fieldModeEnabled(val, false);
}

int FieldMode::_callback_set_FieldModeEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<FieldMode*>(context);

    try
    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->fieldModeEnabled(std::move(arg));
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return true;
}

namespace details
{
namespace FieldMode
{
static const auto _property_FieldModeEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void FieldMode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "FieldModeEnabled")
    {
        auto& v = std::get<bool>(val);
        fieldModeEnabled(v, skipSignal);
        return;
    }
}

auto FieldMode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "FieldModeEnabled")
    {
        return fieldModeEnabled();
    }

    return PropertiesVariant();
}


const vtable_t FieldMode::_vtable[] = {
    vtable::start(),
    vtable::property("FieldModeEnabled",
                     details::FieldMode::_property_FieldModeEnabled
                        .data(),
                     _callback_get_FieldModeEnabled,
                     _callback_set_FieldModeEnabled,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

