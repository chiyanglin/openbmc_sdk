#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VMSC/server.hpp>



namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VMSC::VMSC(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VMSC_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VMSC::VMSC(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VMSC(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VMSC::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VMSC::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMSC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VMSC::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMSC_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VMSC::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VMSC::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMSC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMSC
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VMSC::in() const ->
        std::vector<uint8_t>
{
    return _in;
}

int VMSC::_callback_get_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMSC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->in();
                    }
                ));
    }
}

auto VMSC::in(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_in != value)
    {
        _in = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VMSC_interface.property_changed("IN");
        }
    }

    return _in;
}

auto VMSC::in(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return in(val, false);
}

int VMSC::_callback_set_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VMSC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->in(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VMSC
{
static const auto _property_IN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VMSC::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "IN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        in(v, skipSignal);
        return;
    }
}

auto VMSC::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "IN")
    {
        return in();
    }

    return PropertiesVariant();
}


const vtable_t VMSC::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VMSC::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("IN",
                     details::VMSC::_property_IN
                        .data(),
                     _callback_get_IN,
                     _callback_set_IN,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

