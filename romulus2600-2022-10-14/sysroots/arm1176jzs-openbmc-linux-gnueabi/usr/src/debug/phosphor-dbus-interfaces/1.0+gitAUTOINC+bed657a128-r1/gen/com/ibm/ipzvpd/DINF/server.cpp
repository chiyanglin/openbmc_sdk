#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/DINF/server.hpp>




namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

DINF::DINF(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_DINF_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

DINF::DINF(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : DINF(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto DINF::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int DINF::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DINF*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto DINF::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_DINF_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto DINF::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int DINF::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DINF*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DINF
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto DINF::ri() const ->
        std::vector<uint8_t>
{
    return _ri;
}

int DINF::_callback_get_RI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DINF*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->ri();
                    }
                ));
    }
}

auto DINF::ri(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_ri != value)
    {
        _ri = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_DINF_interface.property_changed("RI");
        }
    }

    return _ri;
}

auto DINF::ri(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return ri(val, false);
}

int DINF::_callback_set_RI(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DINF*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->ri(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DINF
{
static const auto _property_RI =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto DINF::fl() const ->
        std::vector<uint8_t>
{
    return _fl;
}

int DINF::_callback_get_FL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DINF*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fl();
                    }
                ));
    }
}

auto DINF::fl(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_fl != value)
    {
        _fl = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_DINF_interface.property_changed("FL");
        }
    }

    return _fl;
}

auto DINF::fl(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return fl(val, false);
}

int DINF::_callback_set_FL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<DINF*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->fl(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace DINF
{
static const auto _property_FL =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void DINF::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "RI")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        ri(v, skipSignal);
        return;
    }
    if (_name == "FL")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        fl(v, skipSignal);
        return;
    }
}

auto DINF::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "RI")
    {
        return ri();
    }
    if (_name == "FL")
    {
        return fl();
    }

    return PropertiesVariant();
}


const vtable_t DINF::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::DINF::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("RI",
                     details::DINF::_property_RI
                        .data(),
                     _callback_get_RI,
                     _callback_set_RI,
                     vtable::property_::emits_change),
    vtable::property("FL",
                     details::DINF::_property_FL
                        .data(),
                     _callback_get_FL,
                     _callback_set_FL,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

