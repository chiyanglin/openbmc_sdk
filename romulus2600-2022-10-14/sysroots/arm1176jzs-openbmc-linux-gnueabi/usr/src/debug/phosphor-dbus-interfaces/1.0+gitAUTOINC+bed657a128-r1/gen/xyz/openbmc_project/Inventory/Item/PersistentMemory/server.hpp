#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>























#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

class PersistentMemory
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        PersistentMemory() = delete;
        PersistentMemory(const PersistentMemory&) = delete;
        PersistentMemory& operator=(const PersistentMemory&) = delete;
        PersistentMemory(PersistentMemory&&) = delete;
        PersistentMemory& operator=(PersistentMemory&&) = delete;
        virtual ~PersistentMemory() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        PersistentMemory(bus_t& bus, const char* path);

        enum class MemoryModes
        {
            Volatile,
            Persistent,
            Block,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                bool,
                std::string,
                std::vector<MemoryModes>,
                std::vector<uint32_t>,
                uint16_t,
                uint32_t,
                uint64_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        PersistentMemory(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ModuleManufacturerID */
        virtual uint16_t moduleManufacturerID() const;
        /** Set value of ModuleManufacturerID with option to skip sending signal */
        virtual uint16_t moduleManufacturerID(uint16_t value,
               bool skipSignal);
        /** Set value of ModuleManufacturerID */
        virtual uint16_t moduleManufacturerID(uint16_t value);
        /** Get value of ModuleProductID */
        virtual uint16_t moduleProductID() const;
        /** Set value of ModuleProductID with option to skip sending signal */
        virtual uint16_t moduleProductID(uint16_t value,
               bool skipSignal);
        /** Set value of ModuleProductID */
        virtual uint16_t moduleProductID(uint16_t value);
        /** Get value of SubsystemVendorID */
        virtual uint16_t subsystemVendorID() const;
        /** Set value of SubsystemVendorID with option to skip sending signal */
        virtual uint16_t subsystemVendorID(uint16_t value,
               bool skipSignal);
        /** Set value of SubsystemVendorID */
        virtual uint16_t subsystemVendorID(uint16_t value);
        /** Get value of SubsystemDeviceID */
        virtual uint16_t subsystemDeviceID() const;
        /** Set value of SubsystemDeviceID with option to skip sending signal */
        virtual uint16_t subsystemDeviceID(uint16_t value,
               bool skipSignal);
        /** Set value of SubsystemDeviceID */
        virtual uint16_t subsystemDeviceID(uint16_t value);
        /** Get value of VolatileRegionSizeLimitInKiB */
        virtual uint64_t volatileRegionSizeLimitInKiB() const;
        /** Set value of VolatileRegionSizeLimitInKiB with option to skip sending signal */
        virtual uint64_t volatileRegionSizeLimitInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of VolatileRegionSizeLimitInKiB */
        virtual uint64_t volatileRegionSizeLimitInKiB(uint64_t value);
        /** Get value of PmRegionSizeLimitInKiB */
        virtual uint64_t pmRegionSizeLimitInKiB() const;
        /** Set value of PmRegionSizeLimitInKiB with option to skip sending signal */
        virtual uint64_t pmRegionSizeLimitInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of PmRegionSizeLimitInKiB */
        virtual uint64_t pmRegionSizeLimitInKiB(uint64_t value);
        /** Get value of VolatileSizeInKiB */
        virtual uint64_t volatileSizeInKiB() const;
        /** Set value of VolatileSizeInKiB with option to skip sending signal */
        virtual uint64_t volatileSizeInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of VolatileSizeInKiB */
        virtual uint64_t volatileSizeInKiB(uint64_t value);
        /** Get value of PmSizeInKiB */
        virtual uint64_t pmSizeInKiB() const;
        /** Set value of PmSizeInKiB with option to skip sending signal */
        virtual uint64_t pmSizeInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of PmSizeInKiB */
        virtual uint64_t pmSizeInKiB(uint64_t value);
        /** Get value of CacheSizeInKiB */
        virtual uint64_t cacheSizeInKiB() const;
        /** Set value of CacheSizeInKiB with option to skip sending signal */
        virtual uint64_t cacheSizeInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of CacheSizeInKiB */
        virtual uint64_t cacheSizeInKiB(uint64_t value);
        /** Get value of VolatileRegionMaxSizeInKiB */
        virtual uint64_t volatileRegionMaxSizeInKiB() const;
        /** Set value of VolatileRegionMaxSizeInKiB with option to skip sending signal */
        virtual uint64_t volatileRegionMaxSizeInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of VolatileRegionMaxSizeInKiB */
        virtual uint64_t volatileRegionMaxSizeInKiB(uint64_t value);
        /** Get value of PmRegionMaxSizeInKiB */
        virtual uint64_t pmRegionMaxSizeInKiB() const;
        /** Set value of PmRegionMaxSizeInKiB with option to skip sending signal */
        virtual uint64_t pmRegionMaxSizeInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of PmRegionMaxSizeInKiB */
        virtual uint64_t pmRegionMaxSizeInKiB(uint64_t value);
        /** Get value of AllocationIncrementInKiB */
        virtual uint64_t allocationIncrementInKiB() const;
        /** Set value of AllocationIncrementInKiB with option to skip sending signal */
        virtual uint64_t allocationIncrementInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of AllocationIncrementInKiB */
        virtual uint64_t allocationIncrementInKiB(uint64_t value);
        /** Get value of AllocationAlignmentInKiB */
        virtual uint64_t allocationAlignmentInKiB() const;
        /** Set value of AllocationAlignmentInKiB with option to skip sending signal */
        virtual uint64_t allocationAlignmentInKiB(uint64_t value,
               bool skipSignal);
        /** Set value of AllocationAlignmentInKiB */
        virtual uint64_t allocationAlignmentInKiB(uint64_t value);
        /** Get value of VolatileRegionNumberLimit */
        virtual uint32_t volatileRegionNumberLimit() const;
        /** Set value of VolatileRegionNumberLimit with option to skip sending signal */
        virtual uint32_t volatileRegionNumberLimit(uint32_t value,
               bool skipSignal);
        /** Set value of VolatileRegionNumberLimit */
        virtual uint32_t volatileRegionNumberLimit(uint32_t value);
        /** Get value of PmRegionNumberLimit */
        virtual uint32_t pmRegionNumberLimit() const;
        /** Set value of PmRegionNumberLimit with option to skip sending signal */
        virtual uint32_t pmRegionNumberLimit(uint32_t value,
               bool skipSignal);
        /** Set value of PmRegionNumberLimit */
        virtual uint32_t pmRegionNumberLimit(uint32_t value);
        /** Get value of SpareDeviceCount */
        virtual uint32_t spareDeviceCount() const;
        /** Set value of SpareDeviceCount with option to skip sending signal */
        virtual uint32_t spareDeviceCount(uint32_t value,
               bool skipSignal);
        /** Set value of SpareDeviceCount */
        virtual uint32_t spareDeviceCount(uint32_t value);
        /** Get value of IsSpareDeviceInUse */
        virtual bool isSpareDeviceInUse() const;
        /** Set value of IsSpareDeviceInUse with option to skip sending signal */
        virtual bool isSpareDeviceInUse(bool value,
               bool skipSignal);
        /** Set value of IsSpareDeviceInUse */
        virtual bool isSpareDeviceInUse(bool value);
        /** Get value of IsRankSpareEnabled */
        virtual bool isRankSpareEnabled() const;
        /** Set value of IsRankSpareEnabled with option to skip sending signal */
        virtual bool isRankSpareEnabled(bool value,
               bool skipSignal);
        /** Set value of IsRankSpareEnabled */
        virtual bool isRankSpareEnabled(bool value);
        /** Get value of MaxAveragePowerLimitmW */
        virtual std::vector<uint32_t> maxAveragePowerLimitmW() const;
        /** Set value of MaxAveragePowerLimitmW with option to skip sending signal */
        virtual std::vector<uint32_t> maxAveragePowerLimitmW(std::vector<uint32_t> value,
               bool skipSignal);
        /** Set value of MaxAveragePowerLimitmW */
        virtual std::vector<uint32_t> maxAveragePowerLimitmW(std::vector<uint32_t> value);
        /** Get value of CurrentSecurityState */
        virtual std::string currentSecurityState() const;
        /** Set value of CurrentSecurityState with option to skip sending signal */
        virtual std::string currentSecurityState(std::string value,
               bool skipSignal);
        /** Set value of CurrentSecurityState */
        virtual std::string currentSecurityState(std::string value);
        /** Get value of ConfigurationLocked */
        virtual bool configurationLocked() const;
        /** Set value of ConfigurationLocked with option to skip sending signal */
        virtual bool configurationLocked(bool value,
               bool skipSignal);
        /** Set value of ConfigurationLocked */
        virtual bool configurationLocked(bool value);
        /** Get value of AllowedMemoryModes */
        virtual std::vector<MemoryModes> allowedMemoryModes() const;
        /** Set value of AllowedMemoryModes with option to skip sending signal */
        virtual std::vector<MemoryModes> allowedMemoryModes(std::vector<MemoryModes> value,
               bool skipSignal);
        /** Set value of AllowedMemoryModes */
        virtual std::vector<MemoryModes> allowedMemoryModes(std::vector<MemoryModes> value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.PersistentMemory.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static MemoryModes convertMemoryModesFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Inventory.Item.PersistentMemory.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<MemoryModes> convertStringToMemoryModes(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Inventory.Item.PersistentMemory.<value name>"
         */
        static std::string convertMemoryModesToString(MemoryModes e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Item.PersistentMemory";

    private:

        /** @brief sd-bus callback for get-property 'ModuleManufacturerID' */
        static int _callback_get_ModuleManufacturerID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ModuleManufacturerID' */
        static int _callback_set_ModuleManufacturerID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ModuleProductID' */
        static int _callback_get_ModuleProductID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ModuleProductID' */
        static int _callback_set_ModuleProductID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SubsystemVendorID' */
        static int _callback_get_SubsystemVendorID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SubsystemVendorID' */
        static int _callback_set_SubsystemVendorID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SubsystemDeviceID' */
        static int _callback_get_SubsystemDeviceID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SubsystemDeviceID' */
        static int _callback_set_SubsystemDeviceID(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VolatileRegionSizeLimitInKiB' */
        static int _callback_get_VolatileRegionSizeLimitInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VolatileRegionSizeLimitInKiB' */
        static int _callback_set_VolatileRegionSizeLimitInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PmRegionSizeLimitInKiB' */
        static int _callback_get_PmRegionSizeLimitInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PmRegionSizeLimitInKiB' */
        static int _callback_set_PmRegionSizeLimitInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VolatileSizeInKiB' */
        static int _callback_get_VolatileSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VolatileSizeInKiB' */
        static int _callback_set_VolatileSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PmSizeInKiB' */
        static int _callback_get_PmSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PmSizeInKiB' */
        static int _callback_set_PmSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CacheSizeInKiB' */
        static int _callback_get_CacheSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CacheSizeInKiB' */
        static int _callback_set_CacheSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VolatileRegionMaxSizeInKiB' */
        static int _callback_get_VolatileRegionMaxSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VolatileRegionMaxSizeInKiB' */
        static int _callback_set_VolatileRegionMaxSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PmRegionMaxSizeInKiB' */
        static int _callback_get_PmRegionMaxSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PmRegionMaxSizeInKiB' */
        static int _callback_set_PmRegionMaxSizeInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AllocationIncrementInKiB' */
        static int _callback_get_AllocationIncrementInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AllocationIncrementInKiB' */
        static int _callback_set_AllocationIncrementInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AllocationAlignmentInKiB' */
        static int _callback_get_AllocationAlignmentInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AllocationAlignmentInKiB' */
        static int _callback_set_AllocationAlignmentInKiB(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'VolatileRegionNumberLimit' */
        static int _callback_get_VolatileRegionNumberLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'VolatileRegionNumberLimit' */
        static int _callback_set_VolatileRegionNumberLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PmRegionNumberLimit' */
        static int _callback_get_PmRegionNumberLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PmRegionNumberLimit' */
        static int _callback_set_PmRegionNumberLimit(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SpareDeviceCount' */
        static int _callback_get_SpareDeviceCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SpareDeviceCount' */
        static int _callback_set_SpareDeviceCount(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IsSpareDeviceInUse' */
        static int _callback_get_IsSpareDeviceInUse(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IsSpareDeviceInUse' */
        static int _callback_set_IsSpareDeviceInUse(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IsRankSpareEnabled' */
        static int _callback_get_IsRankSpareEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IsRankSpareEnabled' */
        static int _callback_set_IsRankSpareEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MaxAveragePowerLimitmW' */
        static int _callback_get_MaxAveragePowerLimitmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MaxAveragePowerLimitmW' */
        static int _callback_set_MaxAveragePowerLimitmW(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'CurrentSecurityState' */
        static int _callback_get_CurrentSecurityState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'CurrentSecurityState' */
        static int _callback_set_CurrentSecurityState(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ConfigurationLocked' */
        static int _callback_get_ConfigurationLocked(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ConfigurationLocked' */
        static int _callback_set_ConfigurationLocked(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AllowedMemoryModes' */
        static int _callback_get_AllowedMemoryModes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AllowedMemoryModes' */
        static int _callback_set_AllowedMemoryModes(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Item_PersistentMemory_interface;
        sdbusplus::SdBusInterface *_intf;

        uint16_t _moduleManufacturerID{};
        uint16_t _moduleProductID{};
        uint16_t _subsystemVendorID{};
        uint16_t _subsystemDeviceID{};
        uint64_t _volatileRegionSizeLimitInKiB{};
        uint64_t _pmRegionSizeLimitInKiB{};
        uint64_t _volatileSizeInKiB{};
        uint64_t _pmSizeInKiB{};
        uint64_t _cacheSizeInKiB{};
        uint64_t _volatileRegionMaxSizeInKiB{};
        uint64_t _pmRegionMaxSizeInKiB{};
        uint64_t _allocationIncrementInKiB{};
        uint64_t _allocationAlignmentInKiB{};
        uint32_t _volatileRegionNumberLimit{};
        uint32_t _pmRegionNumberLimit{};
        uint32_t _spareDeviceCount{};
        bool _isSpareDeviceInUse{};
        bool _isRankSpareEnabled{};
        std::vector<uint32_t> _maxAveragePowerLimitmW{};
        std::string _currentSecurityState{};
        bool _configurationLocked{};
        std::vector<MemoryModes> _allowedMemoryModes{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type PersistentMemory::MemoryModes.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(PersistentMemory::MemoryModes e)
{
    return PersistentMemory::convertMemoryModesToString(e);
}

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Inventory::Item::server::PersistentMemory::MemoryModes>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Inventory::Item::server::PersistentMemory::convertStringToMemoryModes(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Inventory::Item::server::PersistentMemory::MemoryModes>
{
    static std::string op(xyz::openbmc_project::Inventory::Item::server::PersistentMemory::MemoryModes value)
    {
        return xyz::openbmc_project::Inventory::Item::server::PersistentMemory::convertMemoryModesToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

