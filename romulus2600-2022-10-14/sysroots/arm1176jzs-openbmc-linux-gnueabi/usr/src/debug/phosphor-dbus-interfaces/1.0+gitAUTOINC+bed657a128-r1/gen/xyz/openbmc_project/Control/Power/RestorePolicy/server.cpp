#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Power/RestorePolicy/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

RestorePolicy::RestorePolicy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Power_RestorePolicy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

RestorePolicy::RestorePolicy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : RestorePolicy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto RestorePolicy::powerRestorePolicy() const ->
        Policy
{
    return _powerRestorePolicy;
}

int RestorePolicy::_callback_get_PowerRestorePolicy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RestorePolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerRestorePolicy();
                    }
                ));
    }
}

auto RestorePolicy::powerRestorePolicy(Policy value,
                                         bool skipSignal) ->
        Policy
{
    if (_powerRestorePolicy != value)
    {
        _powerRestorePolicy = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_RestorePolicy_interface.property_changed("PowerRestorePolicy");
        }
    }

    return _powerRestorePolicy;
}

auto RestorePolicy::powerRestorePolicy(Policy val) ->
        Policy
{
    return powerRestorePolicy(val, false);
}

int RestorePolicy::_callback_set_PowerRestorePolicy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RestorePolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](Policy&& arg)
                    {
                        o->powerRestorePolicy(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RestorePolicy
{
static const auto _property_PowerRestorePolicy =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Control::Power::server::RestorePolicy::Policy>());
}
}

auto RestorePolicy::powerRestoreDelay() const ->
        uint64_t
{
    return _powerRestoreDelay;
}

int RestorePolicy::_callback_get_PowerRestoreDelay(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RestorePolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerRestoreDelay();
                    }
                ));
    }
}

auto RestorePolicy::powerRestoreDelay(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_powerRestoreDelay != value)
    {
        _powerRestoreDelay = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Power_RestorePolicy_interface.property_changed("PowerRestoreDelay");
        }
    }

    return _powerRestoreDelay;
}

auto RestorePolicy::powerRestoreDelay(uint64_t val) ->
        uint64_t
{
    return powerRestoreDelay(val, false);
}

int RestorePolicy::_callback_set_PowerRestoreDelay(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<RestorePolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->powerRestoreDelay(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace RestorePolicy
{
static const auto _property_PowerRestoreDelay =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

void RestorePolicy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PowerRestorePolicy")
    {
        auto& v = std::get<Policy>(val);
        powerRestorePolicy(v, skipSignal);
        return;
    }
    if (_name == "PowerRestoreDelay")
    {
        auto& v = std::get<uint64_t>(val);
        powerRestoreDelay(v, skipSignal);
        return;
    }
}

auto RestorePolicy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PowerRestorePolicy")
    {
        return powerRestorePolicy();
    }
    if (_name == "PowerRestoreDelay")
    {
        return powerRestoreDelay();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for RestorePolicy::Policy */
static const std::tuple<const char*, RestorePolicy::Policy> mappingRestorePolicyPolicy[] =
        {
            std::make_tuple( "xyz.openbmc_project.Control.Power.RestorePolicy.Policy.None",                 RestorePolicy::Policy::None ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.RestorePolicy.Policy.AlwaysOn",                 RestorePolicy::Policy::AlwaysOn ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.RestorePolicy.Policy.AlwaysOff",                 RestorePolicy::Policy::AlwaysOff ),
            std::make_tuple( "xyz.openbmc_project.Control.Power.RestorePolicy.Policy.Restore",                 RestorePolicy::Policy::Restore ),
        };

} // anonymous namespace

auto RestorePolicy::convertStringToPolicy(const std::string& s) noexcept ->
        std::optional<Policy>
{
    auto i = std::find_if(
            std::begin(mappingRestorePolicyPolicy),
            std::end(mappingRestorePolicyPolicy),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingRestorePolicyPolicy) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto RestorePolicy::convertPolicyFromString(const std::string& s) ->
        Policy
{
    auto r = convertStringToPolicy(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string RestorePolicy::convertPolicyToString(RestorePolicy::Policy v)
{
    auto i = std::find_if(
            std::begin(mappingRestorePolicyPolicy),
            std::end(mappingRestorePolicyPolicy),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingRestorePolicyPolicy))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t RestorePolicy::_vtable[] = {
    vtable::start(),
    vtable::property("PowerRestorePolicy",
                     details::RestorePolicy::_property_PowerRestorePolicy
                        .data(),
                     _callback_get_PowerRestorePolicy,
                     _callback_set_PowerRestorePolicy,
                     vtable::property_::emits_change),
    vtable::property("PowerRestoreDelay",
                     details::RestorePolicy::_property_PowerRestoreDelay
                        .data(),
                     _callback_get_PowerRestoreDelay,
                     _callback_set_PowerRestoreDelay,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

