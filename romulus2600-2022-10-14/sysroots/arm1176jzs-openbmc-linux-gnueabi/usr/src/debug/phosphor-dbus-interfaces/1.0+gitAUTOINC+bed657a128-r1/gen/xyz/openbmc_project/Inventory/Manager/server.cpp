#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Manager/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace server
{

Manager::Manager(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Manager_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Manager::_callback_Notify(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Manager*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::map<sdbusplus::message::object_path, std::map<std::string, std::map<std::string, std::variant<bool, size_t, int64_t, std::string, std::vector<uint8_t>>>>>&& object)
                    {
                        return o->notify(
                                object);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Manager
{
static const auto _param_Notify =
        utility::tuple_to_array(message::types::type_id<
                std::map<sdbusplus::message::object_path, std::map<std::string, std::map<std::string, std::variant<bool, size_t, int64_t, std::string, std::vector<uint8_t>>>>>>());
static const auto _return_Notify =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t Manager::_vtable[] = {
    vtable::start(),

    vtable::method("Notify",
                   details::Manager::_param_Notify
                        .data(),
                   details::Manager::_return_Notify
                        .data(),
                   _callback_Notify),
    vtable::end()
};

} // namespace server
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

