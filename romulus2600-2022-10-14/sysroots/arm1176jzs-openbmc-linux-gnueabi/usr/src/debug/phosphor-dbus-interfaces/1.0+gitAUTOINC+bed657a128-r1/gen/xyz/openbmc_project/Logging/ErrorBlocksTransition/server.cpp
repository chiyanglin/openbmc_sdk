#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/ErrorBlocksTransition/server.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace server
{

ErrorBlocksTransition::ErrorBlocksTransition(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_ErrorBlocksTransition_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}





const vtable_t ErrorBlocksTransition::_vtable[] = {
    vtable::start(),
    vtable::end()
};

} // namespace server
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

