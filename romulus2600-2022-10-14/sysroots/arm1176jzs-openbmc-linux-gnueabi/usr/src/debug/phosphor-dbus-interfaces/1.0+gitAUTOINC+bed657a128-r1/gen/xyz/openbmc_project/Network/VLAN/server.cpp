#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/VLAN/server.hpp>



namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

VLAN::VLAN(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_VLAN_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VLAN::VLAN(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VLAN(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VLAN::interfaceName() const ->
        std::string
{
    return _interfaceName;
}

int VLAN::_callback_get_InterfaceName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VLAN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->interfaceName();
                    }
                ));
    }
}

auto VLAN::interfaceName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_interfaceName != value)
    {
        _interfaceName = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_VLAN_interface.property_changed("InterfaceName");
        }
    }

    return _interfaceName;
}

auto VLAN::interfaceName(std::string val) ->
        std::string
{
    return interfaceName(val, false);
}


namespace details
{
namespace VLAN
{
static const auto _property_InterfaceName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto VLAN::id() const ->
        uint32_t
{
    return _id;
}

int VLAN::_callback_get_Id(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VLAN*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->id();
                    }
                ));
    }
}

auto VLAN::id(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_id != value)
    {
        _id = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Network_VLAN_interface.property_changed("Id");
        }
    }

    return _id;
}

auto VLAN::id(uint32_t val) ->
        uint32_t
{
    return id(val, false);
}


namespace details
{
namespace VLAN
{
static const auto _property_Id =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void VLAN::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "InterfaceName")
    {
        auto& v = std::get<std::string>(val);
        interfaceName(v, skipSignal);
        return;
    }
    if (_name == "Id")
    {
        auto& v = std::get<uint32_t>(val);
        id(v, skipSignal);
        return;
    }
}

auto VLAN::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "InterfaceName")
    {
        return interfaceName();
    }
    if (_name == "Id")
    {
        return id();
    }

    return PropertiesVariant();
}


const vtable_t VLAN::_vtable[] = {
    vtable::start(),
    vtable::property("InterfaceName",
                     details::VLAN::_property_InterfaceName
                        .data(),
                     _callback_get_InterfaceName,
                     vtable::property_::const_),
    vtable::property("Id",
                     details::VLAN::_property_Id
                        .data(),
                     _callback_get_Id,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

