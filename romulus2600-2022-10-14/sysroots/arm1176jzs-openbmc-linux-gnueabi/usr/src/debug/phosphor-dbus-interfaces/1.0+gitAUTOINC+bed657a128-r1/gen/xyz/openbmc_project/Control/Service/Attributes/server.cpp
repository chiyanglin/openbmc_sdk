#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/Service/Attributes/server.hpp>




namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Service
{
namespace server
{

Attributes::Attributes(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_Service_Attributes_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Attributes::Attributes(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Attributes(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Attributes::enabled() const ->
        bool
{
    return _enabled;
}

int Attributes::_callback_get_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->enabled();
                    }
                ));
    }
}

auto Attributes::enabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_enabled != value)
    {
        _enabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Service_Attributes_interface.property_changed("Enabled");
        }
    }

    return _enabled;
}

auto Attributes::enabled(bool val) ->
        bool
{
    return enabled(val, false);
}

int Attributes::_callback_set_Enabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->enabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_Enabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Attributes::masked() const ->
        bool
{
    return _masked;
}

int Attributes::_callback_get_Masked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->masked();
                    }
                ));
    }
}

auto Attributes::masked(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_masked != value)
    {
        _masked = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Service_Attributes_interface.property_changed("Masked");
        }
    }

    return _masked;
}

auto Attributes::masked(bool val) ->
        bool
{
    return masked(val, false);
}

int Attributes::_callback_set_Masked(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->masked(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_Masked =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Attributes::running() const ->
        bool
{
    return _running;
}

int Attributes::_callback_get_Running(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->running();
                    }
                ));
    }
}

auto Attributes::running(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_running != value)
    {
        _running = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_Service_Attributes_interface.property_changed("Running");
        }
    }

    return _running;
}

auto Attributes::running(bool val) ->
        bool
{
    return running(val, false);
}

int Attributes::_callback_set_Running(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Attributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->running(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Attributes
{
static const auto _property_Running =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Attributes::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Enabled")
    {
        auto& v = std::get<bool>(val);
        enabled(v, skipSignal);
        return;
    }
    if (_name == "Masked")
    {
        auto& v = std::get<bool>(val);
        masked(v, skipSignal);
        return;
    }
    if (_name == "Running")
    {
        auto& v = std::get<bool>(val);
        running(v, skipSignal);
        return;
    }
}

auto Attributes::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Enabled")
    {
        return enabled();
    }
    if (_name == "Masked")
    {
        return masked();
    }
    if (_name == "Running")
    {
        return running();
    }

    return PropertiesVariant();
}


const vtable_t Attributes::_vtable[] = {
    vtable::start(),
    vtable::property("Enabled",
                     details::Attributes::_property_Enabled
                        .data(),
                     _callback_get_Enabled,
                     _callback_set_Enabled,
                     vtable::property_::emits_change),
    vtable::property("Masked",
                     details::Attributes::_property_Masked
                        .data(),
                     _callback_get_Masked,
                     _callback_set_Masked,
                     vtable::property_::emits_change),
    vtable::property("Running",
                     details::Attributes::_property_Running
                        .data(),
                     _callback_get_Running,
                     _callback_set_Running,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Service
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

