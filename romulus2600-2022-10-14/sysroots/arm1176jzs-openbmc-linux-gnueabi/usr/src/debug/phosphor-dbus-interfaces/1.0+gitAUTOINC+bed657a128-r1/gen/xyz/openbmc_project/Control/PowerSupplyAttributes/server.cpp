#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Control/PowerSupplyAttributes/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace server
{

PowerSupplyAttributes::PowerSupplyAttributes(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Control_PowerSupplyAttributes_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PowerSupplyAttributes::PowerSupplyAttributes(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PowerSupplyAttributes(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PowerSupplyAttributes::deratingFactor() const ->
        uint32_t
{
    return _deratingFactor;
}

int PowerSupplyAttributes::_callback_get_DeratingFactor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerSupplyAttributes*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->deratingFactor();
                    }
                ));
    }
}

auto PowerSupplyAttributes::deratingFactor(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_deratingFactor != value)
    {
        _deratingFactor = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Control_PowerSupplyAttributes_interface.property_changed("DeratingFactor");
        }
    }

    return _deratingFactor;
}

auto PowerSupplyAttributes::deratingFactor(uint32_t val) ->
        uint32_t
{
    return deratingFactor(val, false);
}


namespace details
{
namespace PowerSupplyAttributes
{
static const auto _property_DeratingFactor =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void PowerSupplyAttributes::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "DeratingFactor")
    {
        auto& v = std::get<uint32_t>(val);
        deratingFactor(v, skipSignal);
        return;
    }
}

auto PowerSupplyAttributes::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "DeratingFactor")
    {
        return deratingFactor();
    }

    return PropertiesVariant();
}


const vtable_t PowerSupplyAttributes::_vtable[] = {
    vtable::start(),
    vtable::property("DeratingFactor",
                     details::PowerSupplyAttributes::_property_DeratingFactor
                        .data(),
                     _callback_get_DeratingFactor,
                     vtable::property_::const_),
    vtable::end()
};

} // namespace server
} // namespace Control
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

