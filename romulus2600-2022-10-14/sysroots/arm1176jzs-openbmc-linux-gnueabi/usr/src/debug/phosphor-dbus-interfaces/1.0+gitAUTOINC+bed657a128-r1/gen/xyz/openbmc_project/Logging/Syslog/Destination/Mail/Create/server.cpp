#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Logging/Syslog/Destination/Mail/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace Syslog
{
namespace Destination
{
namespace Mail
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Logging_Syslog_Destination_Mail_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_Create(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& mailto, xyz::openbmc_project::Logging::server::Entry::Level&& level)
                    {
                        return o->create(
                                mailto, level);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_Create =
        utility::tuple_to_array(message::types::type_id<
                std::string, xyz::openbmc_project::Logging::server::Entry::Level>());
static const auto _return_Create =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("Create",
                   details::Create::_param_Create
                        .data(),
                   details::Create::_return_Create
                        .data(),
                   _callback_Create),
    vtable::end()
};

} // namespace server
} // namespace Mail
} // namespace Destination
} // namespace Syslog
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

