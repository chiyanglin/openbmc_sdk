#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Dump/Entry/BMC/server.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Dump
{
namespace Entry
{
namespace server
{

BMC::BMC(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Dump_Entry_BMC_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}





const vtable_t BMC::_vtable[] = {
    vtable::start(),
    vtable::end()
};

} // namespace server
} // namespace Entry
} // namespace Dump
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

