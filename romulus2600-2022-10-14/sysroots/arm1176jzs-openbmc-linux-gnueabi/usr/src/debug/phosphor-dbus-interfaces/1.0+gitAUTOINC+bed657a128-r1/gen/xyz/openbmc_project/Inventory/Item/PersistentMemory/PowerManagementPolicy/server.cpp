#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/PowerManagementPolicy/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace server
{

PowerManagementPolicy::PowerManagementPolicy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PowerManagementPolicy::PowerManagementPolicy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PowerManagementPolicy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PowerManagementPolicy::policyEnabled() const ->
        bool
{
    return _policyEnabled;
}

int PowerManagementPolicy::_callback_get_PolicyEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->policyEnabled();
                    }
                ));
    }
}

auto PowerManagementPolicy::policyEnabled(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_policyEnabled != value)
    {
        _policyEnabled = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface.property_changed("PolicyEnabled");
        }
    }

    return _policyEnabled;
}

auto PowerManagementPolicy::policyEnabled(bool val) ->
        bool
{
    return policyEnabled(val, false);
}

int PowerManagementPolicy::_callback_set_PolicyEnabled(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->policyEnabled(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerManagementPolicy
{
static const auto _property_PolicyEnabled =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto PowerManagementPolicy::maxTDPmW() const ->
        uint32_t
{
    return _maxTDPmW;
}

int PowerManagementPolicy::_callback_get_MaxTDPmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxTDPmW();
                    }
                ));
    }
}

auto PowerManagementPolicy::maxTDPmW(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_maxTDPmW != value)
    {
        _maxTDPmW = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface.property_changed("MaxTDPmW");
        }
    }

    return _maxTDPmW;
}

auto PowerManagementPolicy::maxTDPmW(uint32_t val) ->
        uint32_t
{
    return maxTDPmW(val, false);
}

int PowerManagementPolicy::_callback_set_MaxTDPmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->maxTDPmW(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerManagementPolicy
{
static const auto _property_MaxTDPmW =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto PowerManagementPolicy::peakPowerBudgetmW() const ->
        uint32_t
{
    return _peakPowerBudgetmW;
}

int PowerManagementPolicy::_callback_get_PeakPowerBudgetmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->peakPowerBudgetmW();
                    }
                ));
    }
}

auto PowerManagementPolicy::peakPowerBudgetmW(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_peakPowerBudgetmW != value)
    {
        _peakPowerBudgetmW = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface.property_changed("PeakPowerBudgetmW");
        }
    }

    return _peakPowerBudgetmW;
}

auto PowerManagementPolicy::peakPowerBudgetmW(uint32_t val) ->
        uint32_t
{
    return peakPowerBudgetmW(val, false);
}

int PowerManagementPolicy::_callback_set_PeakPowerBudgetmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->peakPowerBudgetmW(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerManagementPolicy
{
static const auto _property_PeakPowerBudgetmW =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto PowerManagementPolicy::averagePowerBudgetmW() const ->
        uint32_t
{
    return _averagePowerBudgetmW;
}

int PowerManagementPolicy::_callback_get_AveragePowerBudgetmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->averagePowerBudgetmW();
                    }
                ));
    }
}

auto PowerManagementPolicy::averagePowerBudgetmW(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_averagePowerBudgetmW != value)
    {
        _averagePowerBudgetmW = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_PowerManagementPolicy_interface.property_changed("AveragePowerBudgetmW");
        }
    }

    return _averagePowerBudgetmW;
}

auto PowerManagementPolicy::averagePowerBudgetmW(uint32_t val) ->
        uint32_t
{
    return averagePowerBudgetmW(val, false);
}

int PowerManagementPolicy::_callback_set_AveragePowerBudgetmW(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PowerManagementPolicy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->averagePowerBudgetmW(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PowerManagementPolicy
{
static const auto _property_AveragePowerBudgetmW =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

void PowerManagementPolicy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PolicyEnabled")
    {
        auto& v = std::get<bool>(val);
        policyEnabled(v, skipSignal);
        return;
    }
    if (_name == "MaxTDPmW")
    {
        auto& v = std::get<uint32_t>(val);
        maxTDPmW(v, skipSignal);
        return;
    }
    if (_name == "PeakPowerBudgetmW")
    {
        auto& v = std::get<uint32_t>(val);
        peakPowerBudgetmW(v, skipSignal);
        return;
    }
    if (_name == "AveragePowerBudgetmW")
    {
        auto& v = std::get<uint32_t>(val);
        averagePowerBudgetmW(v, skipSignal);
        return;
    }
}

auto PowerManagementPolicy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PolicyEnabled")
    {
        return policyEnabled();
    }
    if (_name == "MaxTDPmW")
    {
        return maxTDPmW();
    }
    if (_name == "PeakPowerBudgetmW")
    {
        return peakPowerBudgetmW();
    }
    if (_name == "AveragePowerBudgetmW")
    {
        return averagePowerBudgetmW();
    }

    return PropertiesVariant();
}


const vtable_t PowerManagementPolicy::_vtable[] = {
    vtable::start(),
    vtable::property("PolicyEnabled",
                     details::PowerManagementPolicy::_property_PolicyEnabled
                        .data(),
                     _callback_get_PolicyEnabled,
                     _callback_set_PolicyEnabled,
                     vtable::property_::emits_change),
    vtable::property("MaxTDPmW",
                     details::PowerManagementPolicy::_property_MaxTDPmW
                        .data(),
                     _callback_get_MaxTDPmW,
                     _callback_set_MaxTDPmW,
                     vtable::property_::emits_change),
    vtable::property("PeakPowerBudgetmW",
                     details::PowerManagementPolicy::_property_PeakPowerBudgetmW
                        .data(),
                     _callback_get_PeakPowerBudgetmW,
                     _callback_set_PeakPowerBudgetmW,
                     vtable::property_::emits_change),
    vtable::property("AveragePowerBudgetmW",
                     details::PowerManagementPolicy::_property_AveragePowerBudgetmW
                        .data(),
                     _callback_get_AveragePowerBudgetmW,
                     _callback_set_AveragePowerBudgetmW,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

