#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Accuracy/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace server
{

Accuracy::Accuracy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Accuracy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Accuracy::Accuracy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Accuracy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Accuracy::accuracy() const ->
        double
{
    return _accuracy;
}

int Accuracy::_callback_get_Accuracy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Accuracy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->accuracy();
                    }
                ));
    }
}

auto Accuracy::accuracy(double value,
                                         bool skipSignal) ->
        double
{
    if (_accuracy != value)
    {
        _accuracy = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Accuracy_interface.property_changed("Accuracy");
        }
    }

    return _accuracy;
}

auto Accuracy::accuracy(double val) ->
        double
{
    return accuracy(val, false);
}


namespace details
{
namespace Accuracy
{
static const auto _property_Accuracy =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

void Accuracy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Accuracy")
    {
        auto& v = std::get<double>(val);
        accuracy(v, skipSignal);
        return;
    }
}

auto Accuracy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Accuracy")
    {
        return accuracy();
    }

    return PropertiesVariant();
}


const vtable_t Accuracy::_vtable[] = {
    vtable::start(),
    vtable::property("Accuracy",
                     details::Accuracy::_property_Accuracy
                        .data(),
                     _callback_get_Accuracy,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

