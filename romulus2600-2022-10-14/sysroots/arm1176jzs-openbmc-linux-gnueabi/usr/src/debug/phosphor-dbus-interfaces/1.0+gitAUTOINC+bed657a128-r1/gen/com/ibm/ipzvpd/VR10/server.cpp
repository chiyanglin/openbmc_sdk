#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VR10/server.hpp>






namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VR10::VR10(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VR10_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VR10::VR10(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VR10(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VR10::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VR10::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VR10::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VR10_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VR10::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VR10::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VR10
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VR10::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VR10::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VR10::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VR10_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VR10::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VR10::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VR10
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VR10::dc() const ->
        std::vector<uint8_t>
{
    return _dc;
}

int VR10::_callback_get_DC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dc();
                    }
                ));
    }
}

auto VR10::dc(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dc != value)
    {
        _dc = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VR10_interface.property_changed("DC");
        }
    }

    return _dc;
}

auto VR10::dc(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dc(val, false);
}

int VR10::_callback_set_DC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dc(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VR10
{
static const auto _property_DC =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VR10::fl() const ->
        std::vector<uint8_t>
{
    return _fl;
}

int VR10::_callback_get_FL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->fl();
                    }
                ));
    }
}

auto VR10::fl(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_fl != value)
    {
        _fl = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VR10_interface.property_changed("FL");
        }
    }

    return _fl;
}

auto VR10::fl(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return fl(val, false);
}

int VR10::_callback_set_FL(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->fl(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VR10
{
static const auto _property_FL =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VR10::wa() const ->
        std::vector<uint8_t>
{
    return _wa;
}

int VR10::_callback_get_WA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->wa();
                    }
                ));
    }
}

auto VR10::wa(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_wa != value)
    {
        _wa = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VR10_interface.property_changed("WA");
        }
    }

    return _wa;
}

auto VR10::wa(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return wa(val, false);
}

int VR10::_callback_set_WA(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VR10*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->wa(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VR10
{
static const auto _property_WA =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VR10::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "DC")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dc(v, skipSignal);
        return;
    }
    if (_name == "FL")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        fl(v, skipSignal);
        return;
    }
    if (_name == "WA")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        wa(v, skipSignal);
        return;
    }
}

auto VR10::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "DC")
    {
        return dc();
    }
    if (_name == "FL")
    {
        return fl();
    }
    if (_name == "WA")
    {
        return wa();
    }

    return PropertiesVariant();
}


const vtable_t VR10::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VR10::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VR10::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("DC",
                     details::VR10::_property_DC
                        .data(),
                     _callback_get_DC,
                     _callback_set_DC,
                     vtable::property_::emits_change),
    vtable::property("FL",
                     details::VR10::_property_FL
                        .data(),
                     _callback_get_FL,
                     _callback_set_FL,
                     vtable::property_::emits_change),
    vtable::property("WA",
                     details::VR10::_property_WA
                        .data(),
                     _callback_get_WA,
                     _callback_set_WA,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

