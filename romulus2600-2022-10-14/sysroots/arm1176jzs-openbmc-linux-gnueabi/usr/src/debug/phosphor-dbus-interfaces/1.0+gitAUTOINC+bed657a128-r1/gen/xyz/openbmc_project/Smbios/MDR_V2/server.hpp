#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>










#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Smbios
{
namespace server
{

class MDR_V2
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        MDR_V2() = delete;
        MDR_V2(const MDR_V2&) = delete;
        MDR_V2& operator=(const MDR_V2&) = delete;
        MDR_V2(MDR_V2&&) = delete;
        MDR_V2& operator=(MDR_V2&&) = delete;
        virtual ~MDR_V2() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        MDR_V2(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                uint8_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        MDR_V2(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);


        /** @brief Implementation for GetDirectoryInformation
         *  Get the directory with directory index.
         *
         *  @param[in] dirIndex - Directory index of SMBIOS.
         *
         *  @return dir[std::vector<uint8_t>] - Directory of agent.
         */
        virtual std::vector<uint8_t> getDirectoryInformation(
            uint8_t dirIndex) = 0;

        /** @brief Implementation for GetDataInformation
         *  Get the data info with id index and data set ID.
         *
         *  @param[in] idIndex - Index of SMBIOS directory.
         *
         *  @return dataInfo[std::vector<uint8_t>] - Data information of SMBIOS.
         */
        virtual std::vector<uint8_t> getDataInformation(
            uint8_t idIndex) = 0;

        /** @brief Implementation for SendDirectoryInformation
         *  Send directory information to SMBIOS directory.
         *
         *  @param[in] dirVersion - A counter which increments each time directory updated.
         *  @param[in] dirIndex - Directory index of SMBIOS.
         *  @param[in] returnedEntries - Indicates number of directory entries.
         *  @param[in] remainingEntries - Remaining entries which are higher than index in this transfer.
         *  @param[in] dirEntry - Data set ID of SMBIOS table.
         *
         *  @return status[bool] - Need to continue directory transmisson or not.
         */
        virtual bool sendDirectoryInformation(
            uint8_t dirVersion,
            uint8_t dirIndex,
            uint8_t returnedEntries,
            uint8_t remainingEntries,
            std::vector<uint8_t> dirEntry) = 0;

        /** @brief Implementation for GetDataOffer
         *  Get data set ID.
         *
         *  @return offer[std::vector<uint8_t>] - Data set ID.
         */
        virtual std::vector<uint8_t> getDataOffer(
            ) = 0;

        /** @brief Implementation for SendDataInformation
         *  Send data information with directory index.
         *
         *  @param[in] idIndex - Index of SMBIOS directory.
         *  @param[in] flag - Valid flag to set dir entry status.
         *  @param[in] dataLen - The length of the data in bytes.
         *  @param[in] dataVer - The version number of this data.
         *  @param[in] timeStamp - Timestamp determinded by the agent.
         *
         *  @return status[bool] - Whether data changes.
         */
        virtual bool sendDataInformation(
            uint8_t idIndex,
            uint8_t flag,
            uint32_t dataLen,
            uint32_t dataVer,
            uint32_t timeStamp) = 0;

        /** @brief Implementation for FindIdIndex
         *  Find id index by data info.
         *
         *  @param[in] dataInfo - Data info of data entry.
         *
         *  @return idIndex[int32_t] - Id index of data entry.
         */
        virtual int32_t findIdIndex(
            std::vector<uint8_t> dataInfo) = 0;

        /** @brief Implementation for AgentSynchronizeData
         *  Synchronize SMBIOS data from file.
         *
         *  @return status[bool] - Whether synchronization succeed or not.
         */
        virtual bool agentSynchronizeData(
            ) = 0;

        /** @brief Implementation for SynchronizeDirectoryCommonData
         *  Synchronize directory common data.
         *
         *  @param[in] idIndex - Index of SMBIOS directory.
         *  @param[in] size - Size of data that BIOS prepare to transfer.
         *
         *  @return commonData[std::vector<uint32_t>] - Directory common data includes data size, version and timestamp.
         */
        virtual std::vector<uint32_t> synchronizeDirectoryCommonData(
            uint8_t idIndex,
            uint32_t size) = 0;


        /** Get value of DirectoryEntries */
        virtual uint8_t directoryEntries() const;
        /** Set value of DirectoryEntries with option to skip sending signal */
        virtual uint8_t directoryEntries(uint8_t value,
               bool skipSignal);
        /** Set value of DirectoryEntries */
        virtual uint8_t directoryEntries(uint8_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Smbios_MDR_V2_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Smbios_MDR_V2_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Smbios.MDR_V2";

    private:

        /** @brief sd-bus callback for GetDirectoryInformation
         */
        static int _callback_GetDirectoryInformation(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetDataInformation
         */
        static int _callback_GetDataInformation(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SendDirectoryInformation
         */
        static int _callback_SendDirectoryInformation(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for GetDataOffer
         */
        static int _callback_GetDataOffer(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SendDataInformation
         */
        static int _callback_SendDataInformation(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for FindIdIndex
         */
        static int _callback_FindIdIndex(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for AgentSynchronizeData
         */
        static int _callback_AgentSynchronizeData(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for SynchronizeDirectoryCommonData
         */
        static int _callback_SynchronizeDirectoryCommonData(
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DirectoryEntries' */
        static int _callback_get_DirectoryEntries(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DirectoryEntries' */
        static int _callback_set_DirectoryEntries(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Smbios_MDR_V2_interface;
        sdbusplus::SdBusInterface *_intf;

        uint8_t _directoryEntries{};

};


} // namespace server
} // namespace Smbios
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

