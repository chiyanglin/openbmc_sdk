#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Sensor/Threshold/Warning/server.hpp>









namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Sensor
{
namespace Threshold
{
namespace server
{

Warning::Warning(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Sensor_Threshold_Warning_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Warning::Warning(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Warning(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}



void Warning::warningHighAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Warning_interface;
    auto m = i.new_signal("WarningHighAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Warning
{
static const auto _signal_WarningHighAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void Warning::warningHighAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Warning_interface;
    auto m = i.new_signal("WarningHighAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Warning
{
static const auto _signal_WarningHighAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void Warning::warningLowAlarmAsserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Warning_interface;
    auto m = i.new_signal("WarningLowAlarmAsserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Warning
{
static const auto _signal_WarningLowAlarmAsserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}

void Warning::warningLowAlarmDeasserted(
            double sensorValue)
{
    auto& i = _xyz_openbmc_project_Sensor_Threshold_Warning_interface;
    auto m = i.new_signal("WarningLowAlarmDeasserted");

    m.append(sensorValue);
    m.signal_send();
}

namespace details
{
namespace Warning
{
static const auto _signal_WarningLowAlarmDeasserted =
        utility::tuple_to_array(message::types::type_id<
                double>());
}
}


auto Warning::warningHigh() const ->
        double
{
    return _warningHigh;
}

int Warning::_callback_get_WarningHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->warningHigh();
                    }
                ));
    }
}

auto Warning::warningHigh(double value,
                                         bool skipSignal) ->
        double
{
    if (_warningHigh != value)
    {
        _warningHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Warning_interface.property_changed("WarningHigh");
        }
    }

    return _warningHigh;
}

auto Warning::warningHigh(double val) ->
        double
{
    return warningHigh(val, false);
}

int Warning::_callback_set_WarningHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->warningHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Warning
{
static const auto _property_WarningHigh =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Warning::warningLow() const ->
        double
{
    return _warningLow;
}

int Warning::_callback_get_WarningLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->warningLow();
                    }
                ));
    }
}

auto Warning::warningLow(double value,
                                         bool skipSignal) ->
        double
{
    if (_warningLow != value)
    {
        _warningLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Warning_interface.property_changed("WarningLow");
        }
    }

    return _warningLow;
}

auto Warning::warningLow(double val) ->
        double
{
    return warningLow(val, false);
}

int Warning::_callback_set_WarningLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->warningLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Warning
{
static const auto _property_WarningLow =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Warning::warningAlarmHigh() const ->
        bool
{
    return _warningAlarmHigh;
}

int Warning::_callback_get_WarningAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->warningAlarmHigh();
                    }
                ));
    }
}

auto Warning::warningAlarmHigh(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_warningAlarmHigh != value)
    {
        _warningAlarmHigh = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Warning_interface.property_changed("WarningAlarmHigh");
        }
    }

    return _warningAlarmHigh;
}

auto Warning::warningAlarmHigh(bool val) ->
        bool
{
    return warningAlarmHigh(val, false);
}

int Warning::_callback_set_WarningAlarmHigh(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->warningAlarmHigh(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Warning
{
static const auto _property_WarningAlarmHigh =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Warning::warningAlarmLow() const ->
        bool
{
    return _warningAlarmLow;
}

int Warning::_callback_get_WarningAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->warningAlarmLow();
                    }
                ));
    }
}

auto Warning::warningAlarmLow(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_warningAlarmLow != value)
    {
        _warningAlarmLow = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Sensor_Threshold_Warning_interface.property_changed("WarningAlarmLow");
        }
    }

    return _warningAlarmLow;
}

auto Warning::warningAlarmLow(bool val) ->
        bool
{
    return warningAlarmLow(val, false);
}

int Warning::_callback_set_WarningAlarmLow(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Warning*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->warningAlarmLow(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Warning
{
static const auto _property_WarningAlarmLow =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Warning::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "WarningHigh")
    {
        auto& v = std::get<double>(val);
        warningHigh(v, skipSignal);
        return;
    }
    if (_name == "WarningLow")
    {
        auto& v = std::get<double>(val);
        warningLow(v, skipSignal);
        return;
    }
    if (_name == "WarningAlarmHigh")
    {
        auto& v = std::get<bool>(val);
        warningAlarmHigh(v, skipSignal);
        return;
    }
    if (_name == "WarningAlarmLow")
    {
        auto& v = std::get<bool>(val);
        warningAlarmLow(v, skipSignal);
        return;
    }
}

auto Warning::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "WarningHigh")
    {
        return warningHigh();
    }
    if (_name == "WarningLow")
    {
        return warningLow();
    }
    if (_name == "WarningAlarmHigh")
    {
        return warningAlarmHigh();
    }
    if (_name == "WarningAlarmLow")
    {
        return warningAlarmLow();
    }

    return PropertiesVariant();
}


const vtable_t Warning::_vtable[] = {
    vtable::start(),

    vtable::signal("WarningHighAlarmAsserted",
                   details::Warning::_signal_WarningHighAlarmAsserted
                        .data()),

    vtable::signal("WarningHighAlarmDeasserted",
                   details::Warning::_signal_WarningHighAlarmDeasserted
                        .data()),

    vtable::signal("WarningLowAlarmAsserted",
                   details::Warning::_signal_WarningLowAlarmAsserted
                        .data()),

    vtable::signal("WarningLowAlarmDeasserted",
                   details::Warning::_signal_WarningLowAlarmDeasserted
                        .data()),
    vtable::property("WarningHigh",
                     details::Warning::_property_WarningHigh
                        .data(),
                     _callback_get_WarningHigh,
                     _callback_set_WarningHigh,
                     vtable::property_::emits_change),
    vtable::property("WarningLow",
                     details::Warning::_property_WarningLow
                        .data(),
                     _callback_get_WarningLow,
                     _callback_set_WarningLow,
                     vtable::property_::emits_change),
    vtable::property("WarningAlarmHigh",
                     details::Warning::_property_WarningAlarmHigh
                        .data(),
                     _callback_get_WarningAlarmHigh,
                     _callback_set_WarningAlarmHigh,
                     vtable::property_::emits_change),
    vtable::property("WarningAlarmLow",
                     details::Warning::_property_WarningAlarmLow
                        .data(),
                     _callback_get_WarningAlarmLow,
                     _callback_set_WarningAlarmLow,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Threshold
} // namespace Sensor
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

