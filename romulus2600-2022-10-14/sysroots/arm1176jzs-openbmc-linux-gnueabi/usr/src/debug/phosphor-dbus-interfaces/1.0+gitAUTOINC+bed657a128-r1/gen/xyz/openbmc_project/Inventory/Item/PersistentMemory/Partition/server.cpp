#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/PersistentMemory/Partition/server.hpp>






namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace PersistentMemory
{
namespace server
{

Partition::Partition(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Partition::Partition(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Partition(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Partition::partitionId() const ->
        std::string
{
    return _partitionId;
}

int Partition::_callback_get_PartitionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->partitionId();
                    }
                ));
    }
}

auto Partition::partitionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_partitionId != value)
    {
        _partitionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.property_changed("PartitionId");
        }
    }

    return _partitionId;
}

auto Partition::partitionId(std::string val) ->
        std::string
{
    return partitionId(val, false);
}

int Partition::_callback_set_PartitionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->partitionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Partition
{
static const auto _property_PartitionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Partition::sizeInKiB() const ->
        uint64_t
{
    return _sizeInKiB;
}

int Partition::_callback_get_SizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sizeInKiB();
                    }
                ));
    }
}

auto Partition::sizeInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_sizeInKiB != value)
    {
        _sizeInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.property_changed("SizeInKiB");
        }
    }

    return _sizeInKiB;
}

auto Partition::sizeInKiB(uint64_t val) ->
        uint64_t
{
    return sizeInKiB(val, false);
}

int Partition::_callback_set_SizeInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->sizeInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Partition
{
static const auto _property_SizeInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Partition::memoryClassification() const ->
        std::string
{
    return _memoryClassification;
}

int Partition::_callback_get_MemoryClassification(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->memoryClassification();
                    }
                ));
    }
}

auto Partition::memoryClassification(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_memoryClassification != value)
    {
        _memoryClassification = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.property_changed("MemoryClassification");
        }
    }

    return _memoryClassification;
}

auto Partition::memoryClassification(std::string val) ->
        std::string
{
    return memoryClassification(val, false);
}

int Partition::_callback_set_MemoryClassification(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->memoryClassification(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Partition
{
static const auto _property_MemoryClassification =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Partition::offsetInKiB() const ->
        uint64_t
{
    return _offsetInKiB;
}

int Partition::_callback_get_OffsetInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->offsetInKiB();
                    }
                ));
    }
}

auto Partition::offsetInKiB(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_offsetInKiB != value)
    {
        _offsetInKiB = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.property_changed("OffsetInKiB");
        }
    }

    return _offsetInKiB;
}

auto Partition::offsetInKiB(uint64_t val) ->
        uint64_t
{
    return offsetInKiB(val, false);
}

int Partition::_callback_set_OffsetInKiB(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->offsetInKiB(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Partition
{
static const auto _property_OffsetInKiB =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Partition::passphraseState() const ->
        bool
{
    return _passphraseState;
}

int Partition::_callback_get_PassphraseState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->passphraseState();
                    }
                ));
    }
}

auto Partition::passphraseState(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_passphraseState != value)
    {
        _passphraseState = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PersistentMemory_Partition_interface.property_changed("PassphraseState");
        }
    }

    return _passphraseState;
}

auto Partition::passphraseState(bool val) ->
        bool
{
    return passphraseState(val, false);
}

int Partition::_callback_set_PassphraseState(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Partition*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->passphraseState(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Partition
{
static const auto _property_PassphraseState =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

void Partition::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "PartitionId")
    {
        auto& v = std::get<std::string>(val);
        partitionId(v, skipSignal);
        return;
    }
    if (_name == "SizeInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        sizeInKiB(v, skipSignal);
        return;
    }
    if (_name == "MemoryClassification")
    {
        auto& v = std::get<std::string>(val);
        memoryClassification(v, skipSignal);
        return;
    }
    if (_name == "OffsetInKiB")
    {
        auto& v = std::get<uint64_t>(val);
        offsetInKiB(v, skipSignal);
        return;
    }
    if (_name == "PassphraseState")
    {
        auto& v = std::get<bool>(val);
        passphraseState(v, skipSignal);
        return;
    }
}

auto Partition::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "PartitionId")
    {
        return partitionId();
    }
    if (_name == "SizeInKiB")
    {
        return sizeInKiB();
    }
    if (_name == "MemoryClassification")
    {
        return memoryClassification();
    }
    if (_name == "OffsetInKiB")
    {
        return offsetInKiB();
    }
    if (_name == "PassphraseState")
    {
        return passphraseState();
    }

    return PropertiesVariant();
}


const vtable_t Partition::_vtable[] = {
    vtable::start(),
    vtable::property("PartitionId",
                     details::Partition::_property_PartitionId
                        .data(),
                     _callback_get_PartitionId,
                     _callback_set_PartitionId,
                     vtable::property_::emits_change),
    vtable::property("SizeInKiB",
                     details::Partition::_property_SizeInKiB
                        .data(),
                     _callback_get_SizeInKiB,
                     _callback_set_SizeInKiB,
                     vtable::property_::emits_change),
    vtable::property("MemoryClassification",
                     details::Partition::_property_MemoryClassification
                        .data(),
                     _callback_get_MemoryClassification,
                     _callback_set_MemoryClassification,
                     vtable::property_::emits_change),
    vtable::property("OffsetInKiB",
                     details::Partition::_property_OffsetInKiB
                        .data(),
                     _callback_get_OffsetInKiB,
                     _callback_set_OffsetInKiB,
                     vtable::property_::emits_change),
    vtable::property("PassphraseState",
                     details::Partition::_property_PassphraseState
                        .data(),
                     _callback_get_PassphraseState,
                     _callback_set_PassphraseState,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace PersistentMemory
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

