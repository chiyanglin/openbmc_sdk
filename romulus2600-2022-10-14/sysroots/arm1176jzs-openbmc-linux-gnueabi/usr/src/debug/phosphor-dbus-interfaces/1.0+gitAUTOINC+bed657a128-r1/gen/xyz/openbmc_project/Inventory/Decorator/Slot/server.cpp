#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Decorator/Slot/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

Slot::Slot(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Decorator_Slot_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Slot::Slot(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Slot(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Slot::slotNumber() const ->
        size_t
{
    return _slotNumber;
}

int Slot::_callback_get_SlotNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Slot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->slotNumber();
                    }
                ));
    }
}

auto Slot::slotNumber(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_slotNumber != value)
    {
        _slotNumber = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Decorator_Slot_interface.property_changed("SlotNumber");
        }
    }

    return _slotNumber;
}

auto Slot::slotNumber(size_t val) ->
        size_t
{
    return slotNumber(val, false);
}

int Slot::_callback_set_SlotNumber(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Slot*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->slotNumber(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Slot
{
static const auto _property_SlotNumber =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

void Slot::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "SlotNumber")
    {
        auto& v = std::get<size_t>(val);
        slotNumber(v, skipSignal);
        return;
    }
}

auto Slot::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "SlotNumber")
    {
        return slotNumber();
    }

    return PropertiesVariant();
}


const vtable_t Slot::_vtable[] = {
    vtable::start(),
    vtable::property("SlotNumber",
                     details::Slot::_property_SlotNumber
                        .data(),
                     _callback_get_SlotNumber,
                     _callback_set_SlotNumber,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

