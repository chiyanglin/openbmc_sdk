#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/Dump/Create/server.hpp>

namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace Dump
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _com_ibm_Dump_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}





namespace
{
/** String to enum mapping for Create::CreateParameters */
static const std::tuple<const char*, Create::CreateParameters> mappingCreateCreateParameters[] =
        {
            std::make_tuple( "com.ibm.Dump.Create.CreateParameters.VSPString",                 Create::CreateParameters::VSPString ),
            std::make_tuple( "com.ibm.Dump.Create.CreateParameters.Password",                 Create::CreateParameters::Password ),
            std::make_tuple( "com.ibm.Dump.Create.CreateParameters.ErrorLogId",                 Create::CreateParameters::ErrorLogId ),
            std::make_tuple( "com.ibm.Dump.Create.CreateParameters.DumpType",                 Create::CreateParameters::DumpType ),
            std::make_tuple( "com.ibm.Dump.Create.CreateParameters.FailingUnitId",                 Create::CreateParameters::FailingUnitId ),
        };

} // anonymous namespace

auto Create::convertStringToCreateParameters(const std::string& s) noexcept ->
        std::optional<CreateParameters>
{
    auto i = std::find_if(
            std::begin(mappingCreateCreateParameters),
            std::end(mappingCreateCreateParameters),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCreateCreateParameters) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Create::convertCreateParametersFromString(const std::string& s) ->
        CreateParameters
{
    auto r = convertStringToCreateParameters(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Create::convertCreateParametersToString(Create::CreateParameters v)
{
    auto i = std::find_if(
            std::begin(mappingCreateCreateParameters),
            std::end(mappingCreateCreateParameters),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCreateCreateParameters))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Create::DumpType */
static const std::tuple<const char*, Create::DumpType> mappingCreateDumpType[] =
        {
            std::make_tuple( "com.ibm.Dump.Create.DumpType.Hostboot",                 Create::DumpType::Hostboot ),
            std::make_tuple( "com.ibm.Dump.Create.DumpType.Hardware",                 Create::DumpType::Hardware ),
        };

} // anonymous namespace

auto Create::convertStringToDumpType(const std::string& s) noexcept ->
        std::optional<DumpType>
{
    auto i = std::find_if(
            std::begin(mappingCreateDumpType),
            std::end(mappingCreateDumpType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingCreateDumpType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Create::convertDumpTypeFromString(const std::string& s) ->
        DumpType
{
    auto r = convertStringToDumpType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Create::convertDumpTypeToString(Create::DumpType v)
{
    auto i = std::find_if(
            std::begin(mappingCreateDumpType),
            std::end(mappingCreateDumpType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingCreateDumpType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Create::_vtable[] = {
    vtable::start(),
    vtable::end()
};

} // namespace server
} // namespace Dump
} // namespace ibm
} // namespace com
} // namespace sdbusplus

