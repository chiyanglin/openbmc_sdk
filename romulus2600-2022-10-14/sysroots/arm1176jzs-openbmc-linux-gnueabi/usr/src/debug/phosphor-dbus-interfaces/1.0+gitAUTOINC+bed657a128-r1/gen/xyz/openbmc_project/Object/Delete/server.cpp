#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Object/Delete/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Object
{
namespace server
{

Delete::Delete(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Object_Delete_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Delete::_callback_Delete(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Delete*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->delete_(
                                );
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InternalFailure& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::NotAllowed& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::Unavailable& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InsufficientPermission& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Delete
{
static const auto _param_Delete =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Delete =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}




const vtable_t Delete::_vtable[] = {
    vtable::start(),

    vtable::method("Delete",
                   details::Delete::_param_Delete
                        .data(),
                   details::Delete::_return_Delete
                        .data(),
                   _callback_Delete),
    vtable::end()
};

} // namespace server
} // namespace Object
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

