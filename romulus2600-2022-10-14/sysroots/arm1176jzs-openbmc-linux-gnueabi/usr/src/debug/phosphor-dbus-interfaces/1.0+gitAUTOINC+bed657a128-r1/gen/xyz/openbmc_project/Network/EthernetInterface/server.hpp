#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace server
{

class EthernetInterface
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        EthernetInterface() = delete;
        EthernetInterface(const EthernetInterface&) = delete;
        EthernetInterface& operator=(const EthernetInterface&) = delete;
        EthernetInterface(EthernetInterface&&) = delete;
        EthernetInterface& operator=(EthernetInterface&&) = delete;
        virtual ~EthernetInterface() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        EthernetInterface(bus_t& bus, const char* path);

        enum class LinkLocalConf
        {
            fallback,
            both,
            v4,
            v6,
            none,
        };
        enum class DHCPConf
        {
            both,
            v4v6stateless,
            v6,
            v6stateless,
            v4,
            none,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                DHCPConf,
                LinkLocalConf,
                bool,
                size_t,
                std::string,
                std::vector<std::string>,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        EthernetInterface(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of InterfaceName */
        virtual std::string interfaceName() const;
        /** Set value of InterfaceName with option to skip sending signal */
        virtual std::string interfaceName(std::string value,
               bool skipSignal);
        /** Set value of InterfaceName */
        virtual std::string interfaceName(std::string value);
        /** Get value of Speed */
        virtual uint32_t speed() const;
        /** Set value of Speed with option to skip sending signal */
        virtual uint32_t speed(uint32_t value,
               bool skipSignal);
        /** Set value of Speed */
        virtual uint32_t speed(uint32_t value);
        /** Get value of AutoNeg */
        virtual bool autoNeg() const;
        /** Set value of AutoNeg with option to skip sending signal */
        virtual bool autoNeg(bool value,
               bool skipSignal);
        /** Set value of AutoNeg */
        virtual bool autoNeg(bool value);
        /** Get value of MTU */
        virtual size_t mtu() const;
        /** Set value of MTU with option to skip sending signal */
        virtual size_t mtu(size_t value,
               bool skipSignal);
        /** Set value of MTU */
        virtual size_t mtu(size_t value);
        /** Get value of DomainName */
        virtual std::vector<std::string> domainName() const;
        /** Set value of DomainName with option to skip sending signal */
        virtual std::vector<std::string> domainName(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of DomainName */
        virtual std::vector<std::string> domainName(std::vector<std::string> value);
        /** Get value of DHCPEnabled */
        virtual DHCPConf dhcpEnabled() const;
        /** Set value of DHCPEnabled with option to skip sending signal */
        virtual DHCPConf dhcpEnabled(DHCPConf value,
               bool skipSignal);
        /** Set value of DHCPEnabled */
        virtual DHCPConf dhcpEnabled(DHCPConf value);
        /** Get value of DHCP4 */
        virtual bool dhcp4() const;
        /** Set value of DHCP4 with option to skip sending signal */
        virtual bool dhcp4(bool value,
               bool skipSignal);
        /** Set value of DHCP4 */
        virtual bool dhcp4(bool value);
        /** Get value of DHCP6 */
        virtual bool dhcp6() const;
        /** Set value of DHCP6 with option to skip sending signal */
        virtual bool dhcp6(bool value,
               bool skipSignal);
        /** Set value of DHCP6 */
        virtual bool dhcp6(bool value);
        /** Get value of Nameservers */
        virtual std::vector<std::string> nameservers() const;
        /** Set value of Nameservers with option to skip sending signal */
        virtual std::vector<std::string> nameservers(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of Nameservers */
        virtual std::vector<std::string> nameservers(std::vector<std::string> value);
        /** Get value of StaticNameServers */
        virtual std::vector<std::string> staticNameServers() const;
        /** Set value of StaticNameServers with option to skip sending signal */
        virtual std::vector<std::string> staticNameServers(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of StaticNameServers */
        virtual std::vector<std::string> staticNameServers(std::vector<std::string> value);
        /** Get value of NTPServers */
        virtual std::vector<std::string> ntpServers() const;
        /** Set value of NTPServers with option to skip sending signal */
        virtual std::vector<std::string> ntpServers(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of NTPServers */
        virtual std::vector<std::string> ntpServers(std::vector<std::string> value);
        /** Get value of StaticNTPServers */
        virtual std::vector<std::string> staticNTPServers() const;
        /** Set value of StaticNTPServers with option to skip sending signal */
        virtual std::vector<std::string> staticNTPServers(std::vector<std::string> value,
               bool skipSignal);
        /** Set value of StaticNTPServers */
        virtual std::vector<std::string> staticNTPServers(std::vector<std::string> value);
        /** Get value of LinkLocalAutoConf */
        virtual LinkLocalConf linkLocalAutoConf() const;
        /** Set value of LinkLocalAutoConf with option to skip sending signal */
        virtual LinkLocalConf linkLocalAutoConf(LinkLocalConf value,
               bool skipSignal);
        /** Set value of LinkLocalAutoConf */
        virtual LinkLocalConf linkLocalAutoConf(LinkLocalConf value);
        /** Get value of IPv6AcceptRA */
        virtual bool ipv6AcceptRA() const;
        /** Set value of IPv6AcceptRA with option to skip sending signal */
        virtual bool ipv6AcceptRA(bool value,
               bool skipSignal);
        /** Set value of IPv6AcceptRA */
        virtual bool ipv6AcceptRA(bool value);
        /** Get value of NICEnabled */
        virtual bool nicEnabled() const;
        /** Set value of NICEnabled with option to skip sending signal */
        virtual bool nicEnabled(bool value,
               bool skipSignal);
        /** Set value of NICEnabled */
        virtual bool nicEnabled(bool value);
        /** Get value of LinkUp */
        virtual bool linkUp() const;
        /** Set value of LinkUp with option to skip sending signal */
        virtual bool linkUp(bool value,
               bool skipSignal);
        /** Set value of LinkUp */
        virtual bool linkUp(bool value);
        /** Get value of DefaultGateway */
        virtual std::string defaultGateway() const;
        /** Set value of DefaultGateway with option to skip sending signal */
        virtual std::string defaultGateway(std::string value,
               bool skipSignal);
        /** Set value of DefaultGateway */
        virtual std::string defaultGateway(std::string value);
        /** Get value of DefaultGateway6 */
        virtual std::string defaultGateway6() const;
        /** Set value of DefaultGateway6 with option to skip sending signal */
        virtual std::string defaultGateway6(std::string value,
               bool skipSignal);
        /** Set value of DefaultGateway6 */
        virtual std::string defaultGateway6(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.EthernetInterface.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static LinkLocalConf convertLinkLocalConfFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.EthernetInterface.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<LinkLocalConf> convertStringToLinkLocalConf(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Network.EthernetInterface.<value name>"
         */
        static std::string convertLinkLocalConfToString(LinkLocalConf e);
        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.EthernetInterface.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static DHCPConf convertDHCPConfFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Network.EthernetInterface.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<DHCPConf> convertStringToDHCPConf(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Network.EthernetInterface.<value name>"
         */
        static std::string convertDHCPConfToString(DHCPConf e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Network_EthernetInterface_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Network.EthernetInterface";

    private:

        /** @brief sd-bus callback for get-property 'InterfaceName' */
        static int _callback_get_InterfaceName(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Speed' */
        static int _callback_get_Speed(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AutoNeg' */
        static int _callback_get_AutoNeg(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'MTU' */
        static int _callback_get_MTU(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'MTU' */
        static int _callback_set_MTU(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DomainName' */
        static int _callback_get_DomainName(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DomainName' */
        static int _callback_set_DomainName(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DHCPEnabled' */
        static int _callback_get_DHCPEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DHCPEnabled' */
        static int _callback_set_DHCPEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DHCP4' */
        static int _callback_get_DHCP4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DHCP4' */
        static int _callback_set_DHCP4(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DHCP6' */
        static int _callback_get_DHCP6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DHCP6' */
        static int _callback_set_DHCP6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Nameservers' */
        static int _callback_get_Nameservers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'StaticNameServers' */
        static int _callback_get_StaticNameServers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'StaticNameServers' */
        static int _callback_set_StaticNameServers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'NTPServers' */
        static int _callback_get_NTPServers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NTPServers' */
        static int _callback_set_NTPServers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'StaticNTPServers' */
        static int _callback_get_StaticNTPServers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'StaticNTPServers' */
        static int _callback_set_StaticNTPServers(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LinkLocalAutoConf' */
        static int _callback_get_LinkLocalAutoConf(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'LinkLocalAutoConf' */
        static int _callback_set_LinkLocalAutoConf(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IPv6AcceptRA' */
        static int _callback_get_IPv6AcceptRA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IPv6AcceptRA' */
        static int _callback_set_IPv6AcceptRA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'NICEnabled' */
        static int _callback_get_NICEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'NICEnabled' */
        static int _callback_set_NICEnabled(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'LinkUp' */
        static int _callback_get_LinkUp(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DefaultGateway' */
        static int _callback_get_DefaultGateway(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DefaultGateway' */
        static int _callback_set_DefaultGateway(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DefaultGateway6' */
        static int _callback_get_DefaultGateway6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DefaultGateway6' */
        static int _callback_set_DefaultGateway6(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Network_EthernetInterface_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _interfaceName{};
        uint32_t _speed{};
        bool _autoNeg{};
        size_t _mtu = 0;
        std::vector<std::string> _domainName{};
        DHCPConf _dhcpEnabled{};
        bool _dhcp4{};
        bool _dhcp6{};
        std::vector<std::string> _nameservers{};
        std::vector<std::string> _staticNameServers{};
        std::vector<std::string> _ntpServers{};
        std::vector<std::string> _staticNTPServers{};
        LinkLocalConf _linkLocalAutoConf{};
        bool _ipv6AcceptRA{};
        bool _nicEnabled{};
        bool _linkUp{};
        std::string _defaultGateway{};
        std::string _defaultGateway6{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type EthernetInterface::LinkLocalConf.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(EthernetInterface::LinkLocalConf e)
{
    return EthernetInterface::convertLinkLocalConfToString(e);
}
/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type EthernetInterface::DHCPConf.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(EthernetInterface::DHCPConf e)
{
    return EthernetInterface::convertDHCPConfToString(e);
}

} // namespace server
} // namespace Network
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Network::server::EthernetInterface::LinkLocalConf>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Network::server::EthernetInterface::convertStringToLinkLocalConf(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Network::server::EthernetInterface::LinkLocalConf>
{
    static std::string op(xyz::openbmc_project::Network::server::EthernetInterface::LinkLocalConf value)
    {
        return xyz::openbmc_project::Network::server::EthernetInterface::convertLinkLocalConfToString(value);
    }
};
template <>
struct convert_from_string<xyz::openbmc_project::Network::server::EthernetInterface::DHCPConf>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Network::server::EthernetInterface::convertStringToDHCPConf(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Network::server::EthernetInterface::DHCPConf>
{
    static std::string op(xyz::openbmc_project::Network::server::EthernetInterface::DHCPConf value)
    {
        return xyz::openbmc_project::Network::server::EthernetInterface::convertDHCPConfToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

