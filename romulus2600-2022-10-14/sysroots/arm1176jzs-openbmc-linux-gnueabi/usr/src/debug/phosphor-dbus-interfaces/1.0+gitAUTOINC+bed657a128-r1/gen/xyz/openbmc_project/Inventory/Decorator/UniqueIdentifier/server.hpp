#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Decorator
{
namespace server
{

class UniqueIdentifier
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        UniqueIdentifier() = delete;
        UniqueIdentifier(const UniqueIdentifier&) = delete;
        UniqueIdentifier& operator=(const UniqueIdentifier&) = delete;
        UniqueIdentifier(UniqueIdentifier&&) = delete;
        UniqueIdentifier& operator=(UniqueIdentifier&&) = delete;
        virtual ~UniqueIdentifier() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        UniqueIdentifier(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::string>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        UniqueIdentifier(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of UniqueIdentifier */
        virtual std::string uniqueIdentifier() const;
        /** Set value of UniqueIdentifier with option to skip sending signal */
        virtual std::string uniqueIdentifier(std::string value,
               bool skipSignal);
        /** Set value of UniqueIdentifier */
        virtual std::string uniqueIdentifier(std::string value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Decorator_UniqueIdentifier_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Decorator_UniqueIdentifier_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Decorator.UniqueIdentifier";

    private:

        /** @brief sd-bus callback for get-property 'UniqueIdentifier' */
        static int _callback_get_UniqueIdentifier(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'UniqueIdentifier' */
        static int _callback_set_UniqueIdentifier(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Decorator_UniqueIdentifier_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _uniqueIdentifier{};

};


} // namespace server
} // namespace Decorator
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

