#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/BMCRedundancy/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace server
{

BMCRedundancy::BMCRedundancy(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_BMCRedundancy_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

BMCRedundancy::BMCRedundancy(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : BMCRedundancy(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto BMCRedundancy::activePrimaryBMC() const ->
        sdbusplus::message::object_path
{
    return _activePrimaryBMC;
}

int BMCRedundancy::_callback_get_ActivePrimaryBMC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMCRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->activePrimaryBMC();
                    }
                ));
    }
}

auto BMCRedundancy::activePrimaryBMC(sdbusplus::message::object_path value,
                                         bool skipSignal) ->
        sdbusplus::message::object_path
{
    if (_activePrimaryBMC != value)
    {
        _activePrimaryBMC = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_BMCRedundancy_interface.property_changed("ActivePrimaryBMC");
        }
    }

    return _activePrimaryBMC;
}

auto BMCRedundancy::activePrimaryBMC(sdbusplus::message::object_path val) ->
        sdbusplus::message::object_path
{
    return activePrimaryBMC(val, false);
}

int BMCRedundancy::_callback_set_ActivePrimaryBMC(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<BMCRedundancy*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](sdbusplus::message::object_path&& arg)
                    {
                        o->activePrimaryBMC(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace BMCRedundancy
{
static const auto _property_ActivePrimaryBMC =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::message::object_path>());
}
}

void BMCRedundancy::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ActivePrimaryBMC")
    {
        auto& v = std::get<sdbusplus::message::object_path>(val);
        activePrimaryBMC(v, skipSignal);
        return;
    }
}

auto BMCRedundancy::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ActivePrimaryBMC")
    {
        return activePrimaryBMC();
    }

    return PropertiesVariant();
}


const vtable_t BMCRedundancy::_vtable[] = {
    vtable::start(),
    vtable::property("ActivePrimaryBMC",
                     details::BMCRedundancy::_property_ActivePrimaryBMC
                        .data(),
                     _callback_get_ActivePrimaryBMC,
                     _callback_set_ActivePrimaryBMC,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

