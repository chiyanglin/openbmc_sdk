#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/LXR0/server.hpp>




namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

LXR0::LXR0(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_LXR0_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

LXR0::LXR0(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : LXR0(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto LXR0::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int LXR0::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LXR0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto LXR0::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LXR0_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto LXR0::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int LXR0::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LXR0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LXR0
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LXR0::vz() const ->
        std::vector<uint8_t>
{
    return _vz;
}

int LXR0::_callback_get_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LXR0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vz();
                    }
                ));
    }
}

auto LXR0::vz(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_vz != value)
    {
        _vz = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LXR0_interface.property_changed("VZ");
        }
    }

    return _vz;
}

auto LXR0::vz(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return vz(val, false);
}

int LXR0::_callback_set_VZ(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LXR0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->vz(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LXR0
{
static const auto _property_VZ =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto LXR0::lx() const ->
        std::vector<uint8_t>
{
    return _lx;
}

int LXR0::_callback_get_LX(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LXR0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lx();
                    }
                ));
    }
}

auto LXR0::lx(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_lx != value)
    {
        _lx = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_LXR0_interface.property_changed("LX");
        }
    }

    return _lx;
}

auto LXR0::lx(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return lx(val, false);
}

int LXR0::_callback_set_LX(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<LXR0*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->lx(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace LXR0
{
static const auto _property_LX =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void LXR0::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "VZ")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        vz(v, skipSignal);
        return;
    }
    if (_name == "LX")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        lx(v, skipSignal);
        return;
    }
}

auto LXR0::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "VZ")
    {
        return vz();
    }
    if (_name == "LX")
    {
        return lx();
    }

    return PropertiesVariant();
}


const vtable_t LXR0::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::LXR0::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("VZ",
                     details::LXR0::_property_VZ
                        .data(),
                     _callback_get_VZ,
                     _callback_set_VZ,
                     vtable::property_::emits_change),
    vtable::property("LX",
                     details::LXR0::_property_LX
                        .data(),
                     _callback_get_LX,
                     _callback_set_LX,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

