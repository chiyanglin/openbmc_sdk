#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace PLDM
{
namespace server
{

class Event
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Event() = delete;
        Event(const Event&) = delete;
        Event& operator=(const Event&) = delete;
        Event(Event&&) = delete;
        Event& operator=(Event&&) = delete;
        virtual ~Event() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Event(bus_t& bus, const char* path);




        /** @brief Send signal 'StateSensorEvent'
         *
         *  Signal indicating that a state sensor change EventMessage is received. More information about properties can be found at DSP0248 version 1.2.0 table 19.
         *
         *  @param[in] tid - A terminus id.
         *  @param[in] sensorID - The sensorID is the value that is used in PDRs and PLDM sensor access commands to identify and access a particular sensor within a terminus.
         *  @param[in] sensorOffset - Identifies which state sensor within a composite state sensor the event is being returned for.
         *  @param[in] eventState - The event state value from the state change that triggered the event message.
         *  @param[in] previousEventState - The event state value for the state from which the present event state was entered.
         */
        void stateSensorEvent(
            uint8_t tid,
            uint16_t sensorID,
            uint8_t sensorOffset,
            uint8_t eventState,
            uint8_t previousEventState);



        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_PLDM_Event_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_PLDM_Event_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.PLDM.Event";

    private:


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_PLDM_Event_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace PLDM
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

