#include <xyz/openbmc_project/Logging/SEL/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Logging
{
namespace SEL
{
namespace Error
{
const char* Created::name() const noexcept
{
    return errName;
}
const char* Created::description() const noexcept
{
    return errDesc;
}
const char* Created::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace SEL
} // namespace Logging
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

