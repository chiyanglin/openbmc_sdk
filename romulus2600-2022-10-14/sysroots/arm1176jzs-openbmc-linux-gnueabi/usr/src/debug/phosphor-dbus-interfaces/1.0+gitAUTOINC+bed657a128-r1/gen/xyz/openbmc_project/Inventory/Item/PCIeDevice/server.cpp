#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Inventory/Item/PCIeDevice/server.hpp>







































































namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Item
{
namespace server
{

PCIeDevice::PCIeDevice(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PCIeDevice::PCIeDevice(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PCIeDevice(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto PCIeDevice::deviceType() const ->
        std::string
{
    return _deviceType;
}

int PCIeDevice::_callback_get_DeviceType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->deviceType();
                    }
                ));
    }
}

auto PCIeDevice::deviceType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_deviceType != value)
    {
        _deviceType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("DeviceType");
        }
    }

    return _deviceType;
}

auto PCIeDevice::deviceType(std::string val) ->
        std::string
{
    return deviceType(val, false);
}

int PCIeDevice::_callback_set_DeviceType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->deviceType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_DeviceType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::generationInUse() const ->
        xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations
{
    return _generationInUse;
}

int PCIeDevice::_callback_get_GenerationInUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->generationInUse();
                    }
                ));
    }
}

auto PCIeDevice::generationInUse(xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations value,
                                         bool skipSignal) ->
        xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations
{
    if (_generationInUse != value)
    {
        _generationInUse = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("GenerationInUse");
        }
    }

    return _generationInUse;
}

auto PCIeDevice::generationInUse(xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations val) ->
        xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations
{
    return generationInUse(val, false);
}

int PCIeDevice::_callback_set_GenerationInUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations&& arg)
                    {
                        o->generationInUse(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_GenerationInUse =
    utility::tuple_to_array(message::types::type_id<
            xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>());
}
}

auto PCIeDevice::generationSupported() const ->
        xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations
{
    return _generationSupported;
}

int PCIeDevice::_callback_get_GenerationSupported(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->generationSupported();
                    }
                ));
    }
}

auto PCIeDevice::generationSupported(xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations value,
                                         bool skipSignal) ->
        xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations
{
    if (_generationSupported != value)
    {
        _generationSupported = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("GenerationSupported");
        }
    }

    return _generationSupported;
}

auto PCIeDevice::generationSupported(xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations val) ->
        xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations
{
    return generationSupported(val, false);
}

int PCIeDevice::_callback_set_GenerationSupported(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations&& arg)
                    {
                        o->generationSupported(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_GenerationSupported =
    utility::tuple_to_array(message::types::type_id<
            xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>());
}
}

auto PCIeDevice::function0ClassCode() const ->
        std::string
{
    return _function0ClassCode;
}

int PCIeDevice::_callback_get_Function0ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function0ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0ClassCode != value)
    {
        _function0ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0ClassCode");
        }
    }

    return _function0ClassCode;
}

auto PCIeDevice::function0ClassCode(std::string val) ->
        std::string
{
    return function0ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function0ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0DeviceClass() const ->
        std::string
{
    return _function0DeviceClass;
}

int PCIeDevice::_callback_get_Function0DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function0DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0DeviceClass != value)
    {
        _function0DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0DeviceClass");
        }
    }

    return _function0DeviceClass;
}

auto PCIeDevice::function0DeviceClass(std::string val) ->
        std::string
{
    return function0DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function0DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0DeviceId() const ->
        std::string
{
    return _function0DeviceId;
}

int PCIeDevice::_callback_get_Function0DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function0DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0DeviceId != value)
    {
        _function0DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0DeviceId");
        }
    }

    return _function0DeviceId;
}

auto PCIeDevice::function0DeviceId(std::string val) ->
        std::string
{
    return function0DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function0DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0FunctionType() const ->
        std::string
{
    return _function0FunctionType;
}

int PCIeDevice::_callback_get_Function0FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function0FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0FunctionType != value)
    {
        _function0FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0FunctionType");
        }
    }

    return _function0FunctionType;
}

auto PCIeDevice::function0FunctionType(std::string val) ->
        std::string
{
    return function0FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function0FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0RevisionId() const ->
        std::string
{
    return _function0RevisionId;
}

int PCIeDevice::_callback_get_Function0RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function0RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0RevisionId != value)
    {
        _function0RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0RevisionId");
        }
    }

    return _function0RevisionId;
}

auto PCIeDevice::function0RevisionId(std::string val) ->
        std::string
{
    return function0RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function0RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0SubsystemId() const ->
        std::string
{
    return _function0SubsystemId;
}

int PCIeDevice::_callback_get_Function0SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function0SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0SubsystemId != value)
    {
        _function0SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0SubsystemId");
        }
    }

    return _function0SubsystemId;
}

auto PCIeDevice::function0SubsystemId(std::string val) ->
        std::string
{
    return function0SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function0SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0SubsystemVendorId() const ->
        std::string
{
    return _function0SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function0SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function0SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0SubsystemVendorId != value)
    {
        _function0SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0SubsystemVendorId");
        }
    }

    return _function0SubsystemVendorId;
}

auto PCIeDevice::function0SubsystemVendorId(std::string val) ->
        std::string
{
    return function0SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function0SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function0VendorId() const ->
        std::string
{
    return _function0VendorId;
}

int PCIeDevice::_callback_get_Function0VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function0VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function0VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function0VendorId != value)
    {
        _function0VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function0VendorId");
        }
    }

    return _function0VendorId;
}

auto PCIeDevice::function0VendorId(std::string val) ->
        std::string
{
    return function0VendorId(val, false);
}

int PCIeDevice::_callback_set_Function0VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function0VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function0VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1ClassCode() const ->
        std::string
{
    return _function1ClassCode;
}

int PCIeDevice::_callback_get_Function1ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function1ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1ClassCode != value)
    {
        _function1ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1ClassCode");
        }
    }

    return _function1ClassCode;
}

auto PCIeDevice::function1ClassCode(std::string val) ->
        std::string
{
    return function1ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function1ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1DeviceClass() const ->
        std::string
{
    return _function1DeviceClass;
}

int PCIeDevice::_callback_get_Function1DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function1DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1DeviceClass != value)
    {
        _function1DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1DeviceClass");
        }
    }

    return _function1DeviceClass;
}

auto PCIeDevice::function1DeviceClass(std::string val) ->
        std::string
{
    return function1DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function1DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1DeviceId() const ->
        std::string
{
    return _function1DeviceId;
}

int PCIeDevice::_callback_get_Function1DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function1DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1DeviceId != value)
    {
        _function1DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1DeviceId");
        }
    }

    return _function1DeviceId;
}

auto PCIeDevice::function1DeviceId(std::string val) ->
        std::string
{
    return function1DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function1DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1FunctionType() const ->
        std::string
{
    return _function1FunctionType;
}

int PCIeDevice::_callback_get_Function1FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function1FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1FunctionType != value)
    {
        _function1FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1FunctionType");
        }
    }

    return _function1FunctionType;
}

auto PCIeDevice::function1FunctionType(std::string val) ->
        std::string
{
    return function1FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function1FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1RevisionId() const ->
        std::string
{
    return _function1RevisionId;
}

int PCIeDevice::_callback_get_Function1RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function1RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1RevisionId != value)
    {
        _function1RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1RevisionId");
        }
    }

    return _function1RevisionId;
}

auto PCIeDevice::function1RevisionId(std::string val) ->
        std::string
{
    return function1RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function1RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1SubsystemId() const ->
        std::string
{
    return _function1SubsystemId;
}

int PCIeDevice::_callback_get_Function1SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function1SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1SubsystemId != value)
    {
        _function1SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1SubsystemId");
        }
    }

    return _function1SubsystemId;
}

auto PCIeDevice::function1SubsystemId(std::string val) ->
        std::string
{
    return function1SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function1SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1SubsystemVendorId() const ->
        std::string
{
    return _function1SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function1SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function1SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1SubsystemVendorId != value)
    {
        _function1SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1SubsystemVendorId");
        }
    }

    return _function1SubsystemVendorId;
}

auto PCIeDevice::function1SubsystemVendorId(std::string val) ->
        std::string
{
    return function1SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function1SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function1VendorId() const ->
        std::string
{
    return _function1VendorId;
}

int PCIeDevice::_callback_get_Function1VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function1VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function1VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function1VendorId != value)
    {
        _function1VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function1VendorId");
        }
    }

    return _function1VendorId;
}

auto PCIeDevice::function1VendorId(std::string val) ->
        std::string
{
    return function1VendorId(val, false);
}

int PCIeDevice::_callback_set_Function1VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function1VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function1VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2ClassCode() const ->
        std::string
{
    return _function2ClassCode;
}

int PCIeDevice::_callback_get_Function2ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function2ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2ClassCode != value)
    {
        _function2ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2ClassCode");
        }
    }

    return _function2ClassCode;
}

auto PCIeDevice::function2ClassCode(std::string val) ->
        std::string
{
    return function2ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function2ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2DeviceClass() const ->
        std::string
{
    return _function2DeviceClass;
}

int PCIeDevice::_callback_get_Function2DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function2DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2DeviceClass != value)
    {
        _function2DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2DeviceClass");
        }
    }

    return _function2DeviceClass;
}

auto PCIeDevice::function2DeviceClass(std::string val) ->
        std::string
{
    return function2DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function2DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2DeviceId() const ->
        std::string
{
    return _function2DeviceId;
}

int PCIeDevice::_callback_get_Function2DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function2DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2DeviceId != value)
    {
        _function2DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2DeviceId");
        }
    }

    return _function2DeviceId;
}

auto PCIeDevice::function2DeviceId(std::string val) ->
        std::string
{
    return function2DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function2DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2FunctionType() const ->
        std::string
{
    return _function2FunctionType;
}

int PCIeDevice::_callback_get_Function2FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function2FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2FunctionType != value)
    {
        _function2FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2FunctionType");
        }
    }

    return _function2FunctionType;
}

auto PCIeDevice::function2FunctionType(std::string val) ->
        std::string
{
    return function2FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function2FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2RevisionId() const ->
        std::string
{
    return _function2RevisionId;
}

int PCIeDevice::_callback_get_Function2RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function2RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2RevisionId != value)
    {
        _function2RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2RevisionId");
        }
    }

    return _function2RevisionId;
}

auto PCIeDevice::function2RevisionId(std::string val) ->
        std::string
{
    return function2RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function2RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2SubsystemId() const ->
        std::string
{
    return _function2SubsystemId;
}

int PCIeDevice::_callback_get_Function2SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function2SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2SubsystemId != value)
    {
        _function2SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2SubsystemId");
        }
    }

    return _function2SubsystemId;
}

auto PCIeDevice::function2SubsystemId(std::string val) ->
        std::string
{
    return function2SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function2SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2SubsystemVendorId() const ->
        std::string
{
    return _function2SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function2SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function2SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2SubsystemVendorId != value)
    {
        _function2SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2SubsystemVendorId");
        }
    }

    return _function2SubsystemVendorId;
}

auto PCIeDevice::function2SubsystemVendorId(std::string val) ->
        std::string
{
    return function2SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function2SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function2VendorId() const ->
        std::string
{
    return _function2VendorId;
}

int PCIeDevice::_callback_get_Function2VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function2VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function2VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function2VendorId != value)
    {
        _function2VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function2VendorId");
        }
    }

    return _function2VendorId;
}

auto PCIeDevice::function2VendorId(std::string val) ->
        std::string
{
    return function2VendorId(val, false);
}

int PCIeDevice::_callback_set_Function2VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function2VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function2VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3ClassCode() const ->
        std::string
{
    return _function3ClassCode;
}

int PCIeDevice::_callback_get_Function3ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function3ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3ClassCode != value)
    {
        _function3ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3ClassCode");
        }
    }

    return _function3ClassCode;
}

auto PCIeDevice::function3ClassCode(std::string val) ->
        std::string
{
    return function3ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function3ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3DeviceClass() const ->
        std::string
{
    return _function3DeviceClass;
}

int PCIeDevice::_callback_get_Function3DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function3DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3DeviceClass != value)
    {
        _function3DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3DeviceClass");
        }
    }

    return _function3DeviceClass;
}

auto PCIeDevice::function3DeviceClass(std::string val) ->
        std::string
{
    return function3DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function3DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3DeviceId() const ->
        std::string
{
    return _function3DeviceId;
}

int PCIeDevice::_callback_get_Function3DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function3DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3DeviceId != value)
    {
        _function3DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3DeviceId");
        }
    }

    return _function3DeviceId;
}

auto PCIeDevice::function3DeviceId(std::string val) ->
        std::string
{
    return function3DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function3DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3FunctionType() const ->
        std::string
{
    return _function3FunctionType;
}

int PCIeDevice::_callback_get_Function3FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function3FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3FunctionType != value)
    {
        _function3FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3FunctionType");
        }
    }

    return _function3FunctionType;
}

auto PCIeDevice::function3FunctionType(std::string val) ->
        std::string
{
    return function3FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function3FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3RevisionId() const ->
        std::string
{
    return _function3RevisionId;
}

int PCIeDevice::_callback_get_Function3RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function3RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3RevisionId != value)
    {
        _function3RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3RevisionId");
        }
    }

    return _function3RevisionId;
}

auto PCIeDevice::function3RevisionId(std::string val) ->
        std::string
{
    return function3RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function3RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3SubsystemId() const ->
        std::string
{
    return _function3SubsystemId;
}

int PCIeDevice::_callback_get_Function3SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function3SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3SubsystemId != value)
    {
        _function3SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3SubsystemId");
        }
    }

    return _function3SubsystemId;
}

auto PCIeDevice::function3SubsystemId(std::string val) ->
        std::string
{
    return function3SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function3SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3SubsystemVendorId() const ->
        std::string
{
    return _function3SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function3SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function3SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3SubsystemVendorId != value)
    {
        _function3SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3SubsystemVendorId");
        }
    }

    return _function3SubsystemVendorId;
}

auto PCIeDevice::function3SubsystemVendorId(std::string val) ->
        std::string
{
    return function3SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function3SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function3VendorId() const ->
        std::string
{
    return _function3VendorId;
}

int PCIeDevice::_callback_get_Function3VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function3VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function3VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function3VendorId != value)
    {
        _function3VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function3VendorId");
        }
    }

    return _function3VendorId;
}

auto PCIeDevice::function3VendorId(std::string val) ->
        std::string
{
    return function3VendorId(val, false);
}

int PCIeDevice::_callback_set_Function3VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function3VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function3VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4ClassCode() const ->
        std::string
{
    return _function4ClassCode;
}

int PCIeDevice::_callback_get_Function4ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function4ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4ClassCode != value)
    {
        _function4ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4ClassCode");
        }
    }

    return _function4ClassCode;
}

auto PCIeDevice::function4ClassCode(std::string val) ->
        std::string
{
    return function4ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function4ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4DeviceClass() const ->
        std::string
{
    return _function4DeviceClass;
}

int PCIeDevice::_callback_get_Function4DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function4DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4DeviceClass != value)
    {
        _function4DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4DeviceClass");
        }
    }

    return _function4DeviceClass;
}

auto PCIeDevice::function4DeviceClass(std::string val) ->
        std::string
{
    return function4DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function4DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4DeviceId() const ->
        std::string
{
    return _function4DeviceId;
}

int PCIeDevice::_callback_get_Function4DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function4DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4DeviceId != value)
    {
        _function4DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4DeviceId");
        }
    }

    return _function4DeviceId;
}

auto PCIeDevice::function4DeviceId(std::string val) ->
        std::string
{
    return function4DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function4DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4FunctionType() const ->
        std::string
{
    return _function4FunctionType;
}

int PCIeDevice::_callback_get_Function4FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function4FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4FunctionType != value)
    {
        _function4FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4FunctionType");
        }
    }

    return _function4FunctionType;
}

auto PCIeDevice::function4FunctionType(std::string val) ->
        std::string
{
    return function4FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function4FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4RevisionId() const ->
        std::string
{
    return _function4RevisionId;
}

int PCIeDevice::_callback_get_Function4RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function4RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4RevisionId != value)
    {
        _function4RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4RevisionId");
        }
    }

    return _function4RevisionId;
}

auto PCIeDevice::function4RevisionId(std::string val) ->
        std::string
{
    return function4RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function4RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4SubsystemId() const ->
        std::string
{
    return _function4SubsystemId;
}

int PCIeDevice::_callback_get_Function4SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function4SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4SubsystemId != value)
    {
        _function4SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4SubsystemId");
        }
    }

    return _function4SubsystemId;
}

auto PCIeDevice::function4SubsystemId(std::string val) ->
        std::string
{
    return function4SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function4SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4SubsystemVendorId() const ->
        std::string
{
    return _function4SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function4SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function4SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4SubsystemVendorId != value)
    {
        _function4SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4SubsystemVendorId");
        }
    }

    return _function4SubsystemVendorId;
}

auto PCIeDevice::function4SubsystemVendorId(std::string val) ->
        std::string
{
    return function4SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function4SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function4VendorId() const ->
        std::string
{
    return _function4VendorId;
}

int PCIeDevice::_callback_get_Function4VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function4VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function4VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function4VendorId != value)
    {
        _function4VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function4VendorId");
        }
    }

    return _function4VendorId;
}

auto PCIeDevice::function4VendorId(std::string val) ->
        std::string
{
    return function4VendorId(val, false);
}

int PCIeDevice::_callback_set_Function4VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function4VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function4VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5ClassCode() const ->
        std::string
{
    return _function5ClassCode;
}

int PCIeDevice::_callback_get_Function5ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function5ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5ClassCode != value)
    {
        _function5ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5ClassCode");
        }
    }

    return _function5ClassCode;
}

auto PCIeDevice::function5ClassCode(std::string val) ->
        std::string
{
    return function5ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function5ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5DeviceClass() const ->
        std::string
{
    return _function5DeviceClass;
}

int PCIeDevice::_callback_get_Function5DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function5DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5DeviceClass != value)
    {
        _function5DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5DeviceClass");
        }
    }

    return _function5DeviceClass;
}

auto PCIeDevice::function5DeviceClass(std::string val) ->
        std::string
{
    return function5DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function5DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5DeviceId() const ->
        std::string
{
    return _function5DeviceId;
}

int PCIeDevice::_callback_get_Function5DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function5DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5DeviceId != value)
    {
        _function5DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5DeviceId");
        }
    }

    return _function5DeviceId;
}

auto PCIeDevice::function5DeviceId(std::string val) ->
        std::string
{
    return function5DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function5DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5FunctionType() const ->
        std::string
{
    return _function5FunctionType;
}

int PCIeDevice::_callback_get_Function5FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function5FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5FunctionType != value)
    {
        _function5FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5FunctionType");
        }
    }

    return _function5FunctionType;
}

auto PCIeDevice::function5FunctionType(std::string val) ->
        std::string
{
    return function5FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function5FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5RevisionId() const ->
        std::string
{
    return _function5RevisionId;
}

int PCIeDevice::_callback_get_Function5RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function5RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5RevisionId != value)
    {
        _function5RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5RevisionId");
        }
    }

    return _function5RevisionId;
}

auto PCIeDevice::function5RevisionId(std::string val) ->
        std::string
{
    return function5RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function5RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5SubsystemId() const ->
        std::string
{
    return _function5SubsystemId;
}

int PCIeDevice::_callback_get_Function5SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function5SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5SubsystemId != value)
    {
        _function5SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5SubsystemId");
        }
    }

    return _function5SubsystemId;
}

auto PCIeDevice::function5SubsystemId(std::string val) ->
        std::string
{
    return function5SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function5SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5SubsystemVendorId() const ->
        std::string
{
    return _function5SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function5SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function5SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5SubsystemVendorId != value)
    {
        _function5SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5SubsystemVendorId");
        }
    }

    return _function5SubsystemVendorId;
}

auto PCIeDevice::function5SubsystemVendorId(std::string val) ->
        std::string
{
    return function5SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function5SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function5VendorId() const ->
        std::string
{
    return _function5VendorId;
}

int PCIeDevice::_callback_get_Function5VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function5VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function5VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function5VendorId != value)
    {
        _function5VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function5VendorId");
        }
    }

    return _function5VendorId;
}

auto PCIeDevice::function5VendorId(std::string val) ->
        std::string
{
    return function5VendorId(val, false);
}

int PCIeDevice::_callback_set_Function5VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function5VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function5VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6ClassCode() const ->
        std::string
{
    return _function6ClassCode;
}

int PCIeDevice::_callback_get_Function6ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function6ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6ClassCode != value)
    {
        _function6ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6ClassCode");
        }
    }

    return _function6ClassCode;
}

auto PCIeDevice::function6ClassCode(std::string val) ->
        std::string
{
    return function6ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function6ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6DeviceClass() const ->
        std::string
{
    return _function6DeviceClass;
}

int PCIeDevice::_callback_get_Function6DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function6DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6DeviceClass != value)
    {
        _function6DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6DeviceClass");
        }
    }

    return _function6DeviceClass;
}

auto PCIeDevice::function6DeviceClass(std::string val) ->
        std::string
{
    return function6DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function6DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6DeviceId() const ->
        std::string
{
    return _function6DeviceId;
}

int PCIeDevice::_callback_get_Function6DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function6DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6DeviceId != value)
    {
        _function6DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6DeviceId");
        }
    }

    return _function6DeviceId;
}

auto PCIeDevice::function6DeviceId(std::string val) ->
        std::string
{
    return function6DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function6DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6FunctionType() const ->
        std::string
{
    return _function6FunctionType;
}

int PCIeDevice::_callback_get_Function6FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function6FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6FunctionType != value)
    {
        _function6FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6FunctionType");
        }
    }

    return _function6FunctionType;
}

auto PCIeDevice::function6FunctionType(std::string val) ->
        std::string
{
    return function6FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function6FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6RevisionId() const ->
        std::string
{
    return _function6RevisionId;
}

int PCIeDevice::_callback_get_Function6RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function6RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6RevisionId != value)
    {
        _function6RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6RevisionId");
        }
    }

    return _function6RevisionId;
}

auto PCIeDevice::function6RevisionId(std::string val) ->
        std::string
{
    return function6RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function6RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6SubsystemId() const ->
        std::string
{
    return _function6SubsystemId;
}

int PCIeDevice::_callback_get_Function6SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function6SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6SubsystemId != value)
    {
        _function6SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6SubsystemId");
        }
    }

    return _function6SubsystemId;
}

auto PCIeDevice::function6SubsystemId(std::string val) ->
        std::string
{
    return function6SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function6SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6SubsystemVendorId() const ->
        std::string
{
    return _function6SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function6SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function6SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6SubsystemVendorId != value)
    {
        _function6SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6SubsystemVendorId");
        }
    }

    return _function6SubsystemVendorId;
}

auto PCIeDevice::function6SubsystemVendorId(std::string val) ->
        std::string
{
    return function6SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function6SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function6VendorId() const ->
        std::string
{
    return _function6VendorId;
}

int PCIeDevice::_callback_get_Function6VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function6VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function6VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function6VendorId != value)
    {
        _function6VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function6VendorId");
        }
    }

    return _function6VendorId;
}

auto PCIeDevice::function6VendorId(std::string val) ->
        std::string
{
    return function6VendorId(val, false);
}

int PCIeDevice::_callback_set_Function6VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function6VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function6VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7ClassCode() const ->
        std::string
{
    return _function7ClassCode;
}

int PCIeDevice::_callback_get_Function7ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7ClassCode();
                    }
                ));
    }
}

auto PCIeDevice::function7ClassCode(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7ClassCode != value)
    {
        _function7ClassCode = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7ClassCode");
        }
    }

    return _function7ClassCode;
}

auto PCIeDevice::function7ClassCode(std::string val) ->
        std::string
{
    return function7ClassCode(val, false);
}

int PCIeDevice::_callback_set_Function7ClassCode(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7ClassCode(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7ClassCode =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7DeviceClass() const ->
        std::string
{
    return _function7DeviceClass;
}

int PCIeDevice::_callback_get_Function7DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7DeviceClass();
                    }
                ));
    }
}

auto PCIeDevice::function7DeviceClass(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7DeviceClass != value)
    {
        _function7DeviceClass = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7DeviceClass");
        }
    }

    return _function7DeviceClass;
}

auto PCIeDevice::function7DeviceClass(std::string val) ->
        std::string
{
    return function7DeviceClass(val, false);
}

int PCIeDevice::_callback_set_Function7DeviceClass(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7DeviceClass(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7DeviceClass =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7DeviceId() const ->
        std::string
{
    return _function7DeviceId;
}

int PCIeDevice::_callback_get_Function7DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7DeviceId();
                    }
                ));
    }
}

auto PCIeDevice::function7DeviceId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7DeviceId != value)
    {
        _function7DeviceId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7DeviceId");
        }
    }

    return _function7DeviceId;
}

auto PCIeDevice::function7DeviceId(std::string val) ->
        std::string
{
    return function7DeviceId(val, false);
}

int PCIeDevice::_callback_set_Function7DeviceId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7DeviceId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7DeviceId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7FunctionType() const ->
        std::string
{
    return _function7FunctionType;
}

int PCIeDevice::_callback_get_Function7FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7FunctionType();
                    }
                ));
    }
}

auto PCIeDevice::function7FunctionType(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7FunctionType != value)
    {
        _function7FunctionType = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7FunctionType");
        }
    }

    return _function7FunctionType;
}

auto PCIeDevice::function7FunctionType(std::string val) ->
        std::string
{
    return function7FunctionType(val, false);
}

int PCIeDevice::_callback_set_Function7FunctionType(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7FunctionType(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7FunctionType =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7RevisionId() const ->
        std::string
{
    return _function7RevisionId;
}

int PCIeDevice::_callback_get_Function7RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7RevisionId();
                    }
                ));
    }
}

auto PCIeDevice::function7RevisionId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7RevisionId != value)
    {
        _function7RevisionId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7RevisionId");
        }
    }

    return _function7RevisionId;
}

auto PCIeDevice::function7RevisionId(std::string val) ->
        std::string
{
    return function7RevisionId(val, false);
}

int PCIeDevice::_callback_set_Function7RevisionId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7RevisionId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7RevisionId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7SubsystemId() const ->
        std::string
{
    return _function7SubsystemId;
}

int PCIeDevice::_callback_get_Function7SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7SubsystemId();
                    }
                ));
    }
}

auto PCIeDevice::function7SubsystemId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7SubsystemId != value)
    {
        _function7SubsystemId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7SubsystemId");
        }
    }

    return _function7SubsystemId;
}

auto PCIeDevice::function7SubsystemId(std::string val) ->
        std::string
{
    return function7SubsystemId(val, false);
}

int PCIeDevice::_callback_set_Function7SubsystemId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7SubsystemId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7SubsystemId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7SubsystemVendorId() const ->
        std::string
{
    return _function7SubsystemVendorId;
}

int PCIeDevice::_callback_get_Function7SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7SubsystemVendorId();
                    }
                ));
    }
}

auto PCIeDevice::function7SubsystemVendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7SubsystemVendorId != value)
    {
        _function7SubsystemVendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7SubsystemVendorId");
        }
    }

    return _function7SubsystemVendorId;
}

auto PCIeDevice::function7SubsystemVendorId(std::string val) ->
        std::string
{
    return function7SubsystemVendorId(val, false);
}

int PCIeDevice::_callback_set_Function7SubsystemVendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7SubsystemVendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7SubsystemVendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::function7VendorId() const ->
        std::string
{
    return _function7VendorId;
}

int PCIeDevice::_callback_get_Function7VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->function7VendorId();
                    }
                ));
    }
}

auto PCIeDevice::function7VendorId(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_function7VendorId != value)
    {
        _function7VendorId = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Function7VendorId");
        }
    }

    return _function7VendorId;
}

auto PCIeDevice::function7VendorId(std::string val) ->
        std::string
{
    return function7VendorId(val, false);
}

int PCIeDevice::_callback_set_Function7VendorId(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->function7VendorId(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Function7VendorId =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::manufacturer() const ->
        std::string
{
    return _manufacturer;
}

int PCIeDevice::_callback_get_Manufacturer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->manufacturer();
                    }
                ));
    }
}

auto PCIeDevice::manufacturer(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_manufacturer != value)
    {
        _manufacturer = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("Manufacturer");
        }
    }

    return _manufacturer;
}

auto PCIeDevice::manufacturer(std::string val) ->
        std::string
{
    return manufacturer(val, false);
}

int PCIeDevice::_callback_set_Manufacturer(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->manufacturer(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_Manufacturer =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto PCIeDevice::maxLanes() const ->
        size_t
{
    return _maxLanes;
}

int PCIeDevice::_callback_get_MaxLanes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxLanes();
                    }
                ));
    }
}

auto PCIeDevice::maxLanes(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_maxLanes != value)
    {
        _maxLanes = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("MaxLanes");
        }
    }

    return _maxLanes;
}

auto PCIeDevice::maxLanes(size_t val) ->
        size_t
{
    return maxLanes(val, false);
}

int PCIeDevice::_callback_set_MaxLanes(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->maxLanes(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_MaxLanes =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

auto PCIeDevice::lanesInUse() const ->
        size_t
{
    return _lanesInUse;
}

int PCIeDevice::_callback_get_LanesInUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->lanesInUse();
                    }
                ));
    }
}

auto PCIeDevice::lanesInUse(size_t value,
                                         bool skipSignal) ->
        size_t
{
    if (_lanesInUse != value)
    {
        _lanesInUse = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Inventory_Item_PCIeDevice_interface.property_changed("LanesInUse");
        }
    }

    return _lanesInUse;
}

auto PCIeDevice::lanesInUse(size_t val) ->
        size_t
{
    return lanesInUse(val, false);
}

int PCIeDevice::_callback_set_LanesInUse(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PCIeDevice*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](size_t&& arg)
                    {
                        o->lanesInUse(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PCIeDevice
{
static const auto _property_LanesInUse =
    utility::tuple_to_array(message::types::type_id<
            size_t>());
}
}

void PCIeDevice::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "DeviceType")
    {
        auto& v = std::get<std::string>(val);
        deviceType(v, skipSignal);
        return;
    }
    if (_name == "GenerationInUse")
    {
        auto& v = std::get<xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>(val);
        generationInUse(v, skipSignal);
        return;
    }
    if (_name == "GenerationSupported")
    {
        auto& v = std::get<xyz::openbmc_project::Inventory::Item::server::PCIeSlot::Generations>(val);
        generationSupported(v, skipSignal);
        return;
    }
    if (_name == "Function0ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function0ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function0DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function0DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function0DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function0DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function0FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function0FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function0RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function0RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function0SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function0SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function0SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function0SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function0VendorId")
    {
        auto& v = std::get<std::string>(val);
        function0VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function1ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function1ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function1DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function1DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function1DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function1DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function1FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function1FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function1RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function1RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function1SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function1SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function1SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function1SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function1VendorId")
    {
        auto& v = std::get<std::string>(val);
        function1VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function2ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function2ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function2DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function2DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function2DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function2DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function2FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function2FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function2RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function2RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function2SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function2SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function2SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function2SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function2VendorId")
    {
        auto& v = std::get<std::string>(val);
        function2VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function3ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function3ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function3DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function3DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function3DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function3DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function3FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function3FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function3RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function3RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function3SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function3SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function3SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function3SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function3VendorId")
    {
        auto& v = std::get<std::string>(val);
        function3VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function4ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function4ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function4DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function4DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function4DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function4DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function4FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function4FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function4RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function4RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function4SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function4SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function4SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function4SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function4VendorId")
    {
        auto& v = std::get<std::string>(val);
        function4VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function5ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function5ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function5DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function5DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function5DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function5DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function5FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function5FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function5RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function5RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function5SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function5SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function5SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function5SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function5VendorId")
    {
        auto& v = std::get<std::string>(val);
        function5VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function6ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function6ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function6DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function6DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function6DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function6DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function6FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function6FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function6RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function6RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function6SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function6SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function6SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function6SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function6VendorId")
    {
        auto& v = std::get<std::string>(val);
        function6VendorId(v, skipSignal);
        return;
    }
    if (_name == "Function7ClassCode")
    {
        auto& v = std::get<std::string>(val);
        function7ClassCode(v, skipSignal);
        return;
    }
    if (_name == "Function7DeviceClass")
    {
        auto& v = std::get<std::string>(val);
        function7DeviceClass(v, skipSignal);
        return;
    }
    if (_name == "Function7DeviceId")
    {
        auto& v = std::get<std::string>(val);
        function7DeviceId(v, skipSignal);
        return;
    }
    if (_name == "Function7FunctionType")
    {
        auto& v = std::get<std::string>(val);
        function7FunctionType(v, skipSignal);
        return;
    }
    if (_name == "Function7RevisionId")
    {
        auto& v = std::get<std::string>(val);
        function7RevisionId(v, skipSignal);
        return;
    }
    if (_name == "Function7SubsystemId")
    {
        auto& v = std::get<std::string>(val);
        function7SubsystemId(v, skipSignal);
        return;
    }
    if (_name == "Function7SubsystemVendorId")
    {
        auto& v = std::get<std::string>(val);
        function7SubsystemVendorId(v, skipSignal);
        return;
    }
    if (_name == "Function7VendorId")
    {
        auto& v = std::get<std::string>(val);
        function7VendorId(v, skipSignal);
        return;
    }
    if (_name == "Manufacturer")
    {
        auto& v = std::get<std::string>(val);
        manufacturer(v, skipSignal);
        return;
    }
    if (_name == "MaxLanes")
    {
        auto& v = std::get<size_t>(val);
        maxLanes(v, skipSignal);
        return;
    }
    if (_name == "LanesInUse")
    {
        auto& v = std::get<size_t>(val);
        lanesInUse(v, skipSignal);
        return;
    }
}

auto PCIeDevice::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "DeviceType")
    {
        return deviceType();
    }
    if (_name == "GenerationInUse")
    {
        return generationInUse();
    }
    if (_name == "GenerationSupported")
    {
        return generationSupported();
    }
    if (_name == "Function0ClassCode")
    {
        return function0ClassCode();
    }
    if (_name == "Function0DeviceClass")
    {
        return function0DeviceClass();
    }
    if (_name == "Function0DeviceId")
    {
        return function0DeviceId();
    }
    if (_name == "Function0FunctionType")
    {
        return function0FunctionType();
    }
    if (_name == "Function0RevisionId")
    {
        return function0RevisionId();
    }
    if (_name == "Function0SubsystemId")
    {
        return function0SubsystemId();
    }
    if (_name == "Function0SubsystemVendorId")
    {
        return function0SubsystemVendorId();
    }
    if (_name == "Function0VendorId")
    {
        return function0VendorId();
    }
    if (_name == "Function1ClassCode")
    {
        return function1ClassCode();
    }
    if (_name == "Function1DeviceClass")
    {
        return function1DeviceClass();
    }
    if (_name == "Function1DeviceId")
    {
        return function1DeviceId();
    }
    if (_name == "Function1FunctionType")
    {
        return function1FunctionType();
    }
    if (_name == "Function1RevisionId")
    {
        return function1RevisionId();
    }
    if (_name == "Function1SubsystemId")
    {
        return function1SubsystemId();
    }
    if (_name == "Function1SubsystemVendorId")
    {
        return function1SubsystemVendorId();
    }
    if (_name == "Function1VendorId")
    {
        return function1VendorId();
    }
    if (_name == "Function2ClassCode")
    {
        return function2ClassCode();
    }
    if (_name == "Function2DeviceClass")
    {
        return function2DeviceClass();
    }
    if (_name == "Function2DeviceId")
    {
        return function2DeviceId();
    }
    if (_name == "Function2FunctionType")
    {
        return function2FunctionType();
    }
    if (_name == "Function2RevisionId")
    {
        return function2RevisionId();
    }
    if (_name == "Function2SubsystemId")
    {
        return function2SubsystemId();
    }
    if (_name == "Function2SubsystemVendorId")
    {
        return function2SubsystemVendorId();
    }
    if (_name == "Function2VendorId")
    {
        return function2VendorId();
    }
    if (_name == "Function3ClassCode")
    {
        return function3ClassCode();
    }
    if (_name == "Function3DeviceClass")
    {
        return function3DeviceClass();
    }
    if (_name == "Function3DeviceId")
    {
        return function3DeviceId();
    }
    if (_name == "Function3FunctionType")
    {
        return function3FunctionType();
    }
    if (_name == "Function3RevisionId")
    {
        return function3RevisionId();
    }
    if (_name == "Function3SubsystemId")
    {
        return function3SubsystemId();
    }
    if (_name == "Function3SubsystemVendorId")
    {
        return function3SubsystemVendorId();
    }
    if (_name == "Function3VendorId")
    {
        return function3VendorId();
    }
    if (_name == "Function4ClassCode")
    {
        return function4ClassCode();
    }
    if (_name == "Function4DeviceClass")
    {
        return function4DeviceClass();
    }
    if (_name == "Function4DeviceId")
    {
        return function4DeviceId();
    }
    if (_name == "Function4FunctionType")
    {
        return function4FunctionType();
    }
    if (_name == "Function4RevisionId")
    {
        return function4RevisionId();
    }
    if (_name == "Function4SubsystemId")
    {
        return function4SubsystemId();
    }
    if (_name == "Function4SubsystemVendorId")
    {
        return function4SubsystemVendorId();
    }
    if (_name == "Function4VendorId")
    {
        return function4VendorId();
    }
    if (_name == "Function5ClassCode")
    {
        return function5ClassCode();
    }
    if (_name == "Function5DeviceClass")
    {
        return function5DeviceClass();
    }
    if (_name == "Function5DeviceId")
    {
        return function5DeviceId();
    }
    if (_name == "Function5FunctionType")
    {
        return function5FunctionType();
    }
    if (_name == "Function5RevisionId")
    {
        return function5RevisionId();
    }
    if (_name == "Function5SubsystemId")
    {
        return function5SubsystemId();
    }
    if (_name == "Function5SubsystemVendorId")
    {
        return function5SubsystemVendorId();
    }
    if (_name == "Function5VendorId")
    {
        return function5VendorId();
    }
    if (_name == "Function6ClassCode")
    {
        return function6ClassCode();
    }
    if (_name == "Function6DeviceClass")
    {
        return function6DeviceClass();
    }
    if (_name == "Function6DeviceId")
    {
        return function6DeviceId();
    }
    if (_name == "Function6FunctionType")
    {
        return function6FunctionType();
    }
    if (_name == "Function6RevisionId")
    {
        return function6RevisionId();
    }
    if (_name == "Function6SubsystemId")
    {
        return function6SubsystemId();
    }
    if (_name == "Function6SubsystemVendorId")
    {
        return function6SubsystemVendorId();
    }
    if (_name == "Function6VendorId")
    {
        return function6VendorId();
    }
    if (_name == "Function7ClassCode")
    {
        return function7ClassCode();
    }
    if (_name == "Function7DeviceClass")
    {
        return function7DeviceClass();
    }
    if (_name == "Function7DeviceId")
    {
        return function7DeviceId();
    }
    if (_name == "Function7FunctionType")
    {
        return function7FunctionType();
    }
    if (_name == "Function7RevisionId")
    {
        return function7RevisionId();
    }
    if (_name == "Function7SubsystemId")
    {
        return function7SubsystemId();
    }
    if (_name == "Function7SubsystemVendorId")
    {
        return function7SubsystemVendorId();
    }
    if (_name == "Function7VendorId")
    {
        return function7VendorId();
    }
    if (_name == "Manufacturer")
    {
        return manufacturer();
    }
    if (_name == "MaxLanes")
    {
        return maxLanes();
    }
    if (_name == "LanesInUse")
    {
        return lanesInUse();
    }

    return PropertiesVariant();
}


const vtable_t PCIeDevice::_vtable[] = {
    vtable::start(),
    vtable::property("DeviceType",
                     details::PCIeDevice::_property_DeviceType
                        .data(),
                     _callback_get_DeviceType,
                     _callback_set_DeviceType,
                     vtable::property_::emits_change),
    vtable::property("GenerationInUse",
                     details::PCIeDevice::_property_GenerationInUse
                        .data(),
                     _callback_get_GenerationInUse,
                     _callback_set_GenerationInUse,
                     vtable::property_::emits_change),
    vtable::property("GenerationSupported",
                     details::PCIeDevice::_property_GenerationSupported
                        .data(),
                     _callback_get_GenerationSupported,
                     _callback_set_GenerationSupported,
                     vtable::property_::emits_change),
    vtable::property("Function0ClassCode",
                     details::PCIeDevice::_property_Function0ClassCode
                        .data(),
                     _callback_get_Function0ClassCode,
                     _callback_set_Function0ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function0DeviceClass",
                     details::PCIeDevice::_property_Function0DeviceClass
                        .data(),
                     _callback_get_Function0DeviceClass,
                     _callback_set_Function0DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function0DeviceId",
                     details::PCIeDevice::_property_Function0DeviceId
                        .data(),
                     _callback_get_Function0DeviceId,
                     _callback_set_Function0DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function0FunctionType",
                     details::PCIeDevice::_property_Function0FunctionType
                        .data(),
                     _callback_get_Function0FunctionType,
                     _callback_set_Function0FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function0RevisionId",
                     details::PCIeDevice::_property_Function0RevisionId
                        .data(),
                     _callback_get_Function0RevisionId,
                     _callback_set_Function0RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function0SubsystemId",
                     details::PCIeDevice::_property_Function0SubsystemId
                        .data(),
                     _callback_get_Function0SubsystemId,
                     _callback_set_Function0SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function0SubsystemVendorId",
                     details::PCIeDevice::_property_Function0SubsystemVendorId
                        .data(),
                     _callback_get_Function0SubsystemVendorId,
                     _callback_set_Function0SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function0VendorId",
                     details::PCIeDevice::_property_Function0VendorId
                        .data(),
                     _callback_get_Function0VendorId,
                     _callback_set_Function0VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function1ClassCode",
                     details::PCIeDevice::_property_Function1ClassCode
                        .data(),
                     _callback_get_Function1ClassCode,
                     _callback_set_Function1ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function1DeviceClass",
                     details::PCIeDevice::_property_Function1DeviceClass
                        .data(),
                     _callback_get_Function1DeviceClass,
                     _callback_set_Function1DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function1DeviceId",
                     details::PCIeDevice::_property_Function1DeviceId
                        .data(),
                     _callback_get_Function1DeviceId,
                     _callback_set_Function1DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function1FunctionType",
                     details::PCIeDevice::_property_Function1FunctionType
                        .data(),
                     _callback_get_Function1FunctionType,
                     _callback_set_Function1FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function1RevisionId",
                     details::PCIeDevice::_property_Function1RevisionId
                        .data(),
                     _callback_get_Function1RevisionId,
                     _callback_set_Function1RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function1SubsystemId",
                     details::PCIeDevice::_property_Function1SubsystemId
                        .data(),
                     _callback_get_Function1SubsystemId,
                     _callback_set_Function1SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function1SubsystemVendorId",
                     details::PCIeDevice::_property_Function1SubsystemVendorId
                        .data(),
                     _callback_get_Function1SubsystemVendorId,
                     _callback_set_Function1SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function1VendorId",
                     details::PCIeDevice::_property_Function1VendorId
                        .data(),
                     _callback_get_Function1VendorId,
                     _callback_set_Function1VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function2ClassCode",
                     details::PCIeDevice::_property_Function2ClassCode
                        .data(),
                     _callback_get_Function2ClassCode,
                     _callback_set_Function2ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function2DeviceClass",
                     details::PCIeDevice::_property_Function2DeviceClass
                        .data(),
                     _callback_get_Function2DeviceClass,
                     _callback_set_Function2DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function2DeviceId",
                     details::PCIeDevice::_property_Function2DeviceId
                        .data(),
                     _callback_get_Function2DeviceId,
                     _callback_set_Function2DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function2FunctionType",
                     details::PCIeDevice::_property_Function2FunctionType
                        .data(),
                     _callback_get_Function2FunctionType,
                     _callback_set_Function2FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function2RevisionId",
                     details::PCIeDevice::_property_Function2RevisionId
                        .data(),
                     _callback_get_Function2RevisionId,
                     _callback_set_Function2RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function2SubsystemId",
                     details::PCIeDevice::_property_Function2SubsystemId
                        .data(),
                     _callback_get_Function2SubsystemId,
                     _callback_set_Function2SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function2SubsystemVendorId",
                     details::PCIeDevice::_property_Function2SubsystemVendorId
                        .data(),
                     _callback_get_Function2SubsystemVendorId,
                     _callback_set_Function2SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function2VendorId",
                     details::PCIeDevice::_property_Function2VendorId
                        .data(),
                     _callback_get_Function2VendorId,
                     _callback_set_Function2VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function3ClassCode",
                     details::PCIeDevice::_property_Function3ClassCode
                        .data(),
                     _callback_get_Function3ClassCode,
                     _callback_set_Function3ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function3DeviceClass",
                     details::PCIeDevice::_property_Function3DeviceClass
                        .data(),
                     _callback_get_Function3DeviceClass,
                     _callback_set_Function3DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function3DeviceId",
                     details::PCIeDevice::_property_Function3DeviceId
                        .data(),
                     _callback_get_Function3DeviceId,
                     _callback_set_Function3DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function3FunctionType",
                     details::PCIeDevice::_property_Function3FunctionType
                        .data(),
                     _callback_get_Function3FunctionType,
                     _callback_set_Function3FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function3RevisionId",
                     details::PCIeDevice::_property_Function3RevisionId
                        .data(),
                     _callback_get_Function3RevisionId,
                     _callback_set_Function3RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function3SubsystemId",
                     details::PCIeDevice::_property_Function3SubsystemId
                        .data(),
                     _callback_get_Function3SubsystemId,
                     _callback_set_Function3SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function3SubsystemVendorId",
                     details::PCIeDevice::_property_Function3SubsystemVendorId
                        .data(),
                     _callback_get_Function3SubsystemVendorId,
                     _callback_set_Function3SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function3VendorId",
                     details::PCIeDevice::_property_Function3VendorId
                        .data(),
                     _callback_get_Function3VendorId,
                     _callback_set_Function3VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function4ClassCode",
                     details::PCIeDevice::_property_Function4ClassCode
                        .data(),
                     _callback_get_Function4ClassCode,
                     _callback_set_Function4ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function4DeviceClass",
                     details::PCIeDevice::_property_Function4DeviceClass
                        .data(),
                     _callback_get_Function4DeviceClass,
                     _callback_set_Function4DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function4DeviceId",
                     details::PCIeDevice::_property_Function4DeviceId
                        .data(),
                     _callback_get_Function4DeviceId,
                     _callback_set_Function4DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function4FunctionType",
                     details::PCIeDevice::_property_Function4FunctionType
                        .data(),
                     _callback_get_Function4FunctionType,
                     _callback_set_Function4FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function4RevisionId",
                     details::PCIeDevice::_property_Function4RevisionId
                        .data(),
                     _callback_get_Function4RevisionId,
                     _callback_set_Function4RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function4SubsystemId",
                     details::PCIeDevice::_property_Function4SubsystemId
                        .data(),
                     _callback_get_Function4SubsystemId,
                     _callback_set_Function4SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function4SubsystemVendorId",
                     details::PCIeDevice::_property_Function4SubsystemVendorId
                        .data(),
                     _callback_get_Function4SubsystemVendorId,
                     _callback_set_Function4SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function4VendorId",
                     details::PCIeDevice::_property_Function4VendorId
                        .data(),
                     _callback_get_Function4VendorId,
                     _callback_set_Function4VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function5ClassCode",
                     details::PCIeDevice::_property_Function5ClassCode
                        .data(),
                     _callback_get_Function5ClassCode,
                     _callback_set_Function5ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function5DeviceClass",
                     details::PCIeDevice::_property_Function5DeviceClass
                        .data(),
                     _callback_get_Function5DeviceClass,
                     _callback_set_Function5DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function5DeviceId",
                     details::PCIeDevice::_property_Function5DeviceId
                        .data(),
                     _callback_get_Function5DeviceId,
                     _callback_set_Function5DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function5FunctionType",
                     details::PCIeDevice::_property_Function5FunctionType
                        .data(),
                     _callback_get_Function5FunctionType,
                     _callback_set_Function5FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function5RevisionId",
                     details::PCIeDevice::_property_Function5RevisionId
                        .data(),
                     _callback_get_Function5RevisionId,
                     _callback_set_Function5RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function5SubsystemId",
                     details::PCIeDevice::_property_Function5SubsystemId
                        .data(),
                     _callback_get_Function5SubsystemId,
                     _callback_set_Function5SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function5SubsystemVendorId",
                     details::PCIeDevice::_property_Function5SubsystemVendorId
                        .data(),
                     _callback_get_Function5SubsystemVendorId,
                     _callback_set_Function5SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function5VendorId",
                     details::PCIeDevice::_property_Function5VendorId
                        .data(),
                     _callback_get_Function5VendorId,
                     _callback_set_Function5VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function6ClassCode",
                     details::PCIeDevice::_property_Function6ClassCode
                        .data(),
                     _callback_get_Function6ClassCode,
                     _callback_set_Function6ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function6DeviceClass",
                     details::PCIeDevice::_property_Function6DeviceClass
                        .data(),
                     _callback_get_Function6DeviceClass,
                     _callback_set_Function6DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function6DeviceId",
                     details::PCIeDevice::_property_Function6DeviceId
                        .data(),
                     _callback_get_Function6DeviceId,
                     _callback_set_Function6DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function6FunctionType",
                     details::PCIeDevice::_property_Function6FunctionType
                        .data(),
                     _callback_get_Function6FunctionType,
                     _callback_set_Function6FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function6RevisionId",
                     details::PCIeDevice::_property_Function6RevisionId
                        .data(),
                     _callback_get_Function6RevisionId,
                     _callback_set_Function6RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function6SubsystemId",
                     details::PCIeDevice::_property_Function6SubsystemId
                        .data(),
                     _callback_get_Function6SubsystemId,
                     _callback_set_Function6SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function6SubsystemVendorId",
                     details::PCIeDevice::_property_Function6SubsystemVendorId
                        .data(),
                     _callback_get_Function6SubsystemVendorId,
                     _callback_set_Function6SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function6VendorId",
                     details::PCIeDevice::_property_Function6VendorId
                        .data(),
                     _callback_get_Function6VendorId,
                     _callback_set_Function6VendorId,
                     vtable::property_::emits_change),
    vtable::property("Function7ClassCode",
                     details::PCIeDevice::_property_Function7ClassCode
                        .data(),
                     _callback_get_Function7ClassCode,
                     _callback_set_Function7ClassCode,
                     vtable::property_::emits_change),
    vtable::property("Function7DeviceClass",
                     details::PCIeDevice::_property_Function7DeviceClass
                        .data(),
                     _callback_get_Function7DeviceClass,
                     _callback_set_Function7DeviceClass,
                     vtable::property_::emits_change),
    vtable::property("Function7DeviceId",
                     details::PCIeDevice::_property_Function7DeviceId
                        .data(),
                     _callback_get_Function7DeviceId,
                     _callback_set_Function7DeviceId,
                     vtable::property_::emits_change),
    vtable::property("Function7FunctionType",
                     details::PCIeDevice::_property_Function7FunctionType
                        .data(),
                     _callback_get_Function7FunctionType,
                     _callback_set_Function7FunctionType,
                     vtable::property_::emits_change),
    vtable::property("Function7RevisionId",
                     details::PCIeDevice::_property_Function7RevisionId
                        .data(),
                     _callback_get_Function7RevisionId,
                     _callback_set_Function7RevisionId,
                     vtable::property_::emits_change),
    vtable::property("Function7SubsystemId",
                     details::PCIeDevice::_property_Function7SubsystemId
                        .data(),
                     _callback_get_Function7SubsystemId,
                     _callback_set_Function7SubsystemId,
                     vtable::property_::emits_change),
    vtable::property("Function7SubsystemVendorId",
                     details::PCIeDevice::_property_Function7SubsystemVendorId
                        .data(),
                     _callback_get_Function7SubsystemVendorId,
                     _callback_set_Function7SubsystemVendorId,
                     vtable::property_::emits_change),
    vtable::property("Function7VendorId",
                     details::PCIeDevice::_property_Function7VendorId
                        .data(),
                     _callback_get_Function7VendorId,
                     _callback_set_Function7VendorId,
                     vtable::property_::emits_change),
    vtable::property("Manufacturer",
                     details::PCIeDevice::_property_Manufacturer
                        .data(),
                     _callback_get_Manufacturer,
                     _callback_set_Manufacturer,
                     vtable::property_::emits_change),
    vtable::property("MaxLanes",
                     details::PCIeDevice::_property_MaxLanes
                        .data(),
                     _callback_get_MaxLanes,
                     _callback_set_MaxLanes,
                     vtable::property_::emits_change),
    vtable::property("LanesInUse",
                     details::PCIeDevice::_property_LanesInUse
                        .data(),
                     _callback_get_LanesInUse,
                     _callback_set_LanesInUse,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Item
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

