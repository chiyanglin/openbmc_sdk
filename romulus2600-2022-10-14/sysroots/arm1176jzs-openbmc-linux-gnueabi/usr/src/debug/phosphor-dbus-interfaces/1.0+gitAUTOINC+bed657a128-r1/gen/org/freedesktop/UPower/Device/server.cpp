#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <org/freedesktop/UPower/Device/server.hpp>

































namespace sdbusplus
{
namespace org
{
namespace freedesktop
{
namespace UPower
{
namespace server
{

Device::Device(bus_t& bus, const char* path)
        : _org_freedesktop_UPower_Device_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Device::Device(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Device(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int Device::_callback_Refresh(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->refresh(
                                );
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Device
{
static const auto _param_Refresh =
        utility::tuple_to_array(std::make_tuple('\0'));
static const auto _return_Refresh =
        utility::tuple_to_array(std::make_tuple('\0'));
}
}

int Device::_callback_GetHistory(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& type, uint32_t&& timespan, uint32_t&& resolution)
                    {
                        return o->getHistory(
                                type, timespan, resolution);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Device
{
static const auto _param_GetHistory =
        utility::tuple_to_array(message::types::type_id<
                std::string, uint32_t, uint32_t>());
static const auto _return_GetHistory =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::tuple<uint32_t, double, uint32_t>>>());
}
}

int Device::_callback_GetStatistics(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& type)
                    {
                        return o->getStatistics(
                                type);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace Device
{
static const auto _param_GetStatistics =
        utility::tuple_to_array(message::types::type_id<
                std::string>());
static const auto _return_GetStatistics =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::tuple<double, double>>>());
}
}



auto Device::nativePath() const ->
        std::string
{
    return _nativePath;
}

int Device::_callback_get_NativePath(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->nativePath();
                    }
                ));
    }
}

auto Device::nativePath(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_nativePath != value)
    {
        _nativePath = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("NativePath");
        }
    }

    return _nativePath;
}

auto Device::nativePath(std::string val) ->
        std::string
{
    return nativePath(val, false);
}

int Device::_callback_set_NativePath(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->nativePath(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_NativePath =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Device::vendor() const ->
        std::string
{
    return _vendor;
}

int Device::_callback_get_Vendor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->vendor();
                    }
                ));
    }
}

auto Device::vendor(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_vendor != value)
    {
        _vendor = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Vendor");
        }
    }

    return _vendor;
}

auto Device::vendor(std::string val) ->
        std::string
{
    return vendor(val, false);
}

int Device::_callback_set_Vendor(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->vendor(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Vendor =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Device::model() const ->
        std::string
{
    return _model;
}

int Device::_callback_get_Model(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->model();
                    }
                ));
    }
}

auto Device::model(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_model != value)
    {
        _model = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Model");
        }
    }

    return _model;
}

auto Device::model(std::string val) ->
        std::string
{
    return model(val, false);
}

int Device::_callback_set_Model(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->model(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Model =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Device::serial() const ->
        std::string
{
    return _serial;
}

int Device::_callback_get_Serial(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->serial();
                    }
                ));
    }
}

auto Device::serial(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_serial != value)
    {
        _serial = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Serial");
        }
    }

    return _serial;
}

auto Device::serial(std::string val) ->
        std::string
{
    return serial(val, false);
}

int Device::_callback_set_Serial(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->serial(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Serial =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

auto Device::updateTime() const ->
        uint64_t
{
    return _updateTime;
}

int Device::_callback_get_UpdateTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->updateTime();
                    }
                ));
    }
}

auto Device::updateTime(uint64_t value,
                                         bool skipSignal) ->
        uint64_t
{
    if (_updateTime != value)
    {
        _updateTime = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("UpdateTime");
        }
    }

    return _updateTime;
}

auto Device::updateTime(uint64_t val) ->
        uint64_t
{
    return updateTime(val, false);
}

int Device::_callback_set_UpdateTime(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint64_t&& arg)
                    {
                        o->updateTime(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_UpdateTime =
    utility::tuple_to_array(message::types::type_id<
            uint64_t>());
}
}

auto Device::type() const ->
        uint32_t
{
    return _type;
}

int Device::_callback_get_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->type();
                    }
                ));
    }
}

auto Device::type(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_type != value)
    {
        _type = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Type");
        }
    }

    return _type;
}

auto Device::type(uint32_t val) ->
        uint32_t
{
    return type(val, false);
}

int Device::_callback_set_Type(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->type(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Type =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Device::powerSupply() const ->
        bool
{
    return _powerSupply;
}

int Device::_callback_get_PowerSupply(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->powerSupply();
                    }
                ));
    }
}

auto Device::powerSupply(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_powerSupply != value)
    {
        _powerSupply = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("PowerSupply");
        }
    }

    return _powerSupply;
}

auto Device::powerSupply(bool val) ->
        bool
{
    return powerSupply(val, false);
}

int Device::_callback_set_PowerSupply(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->powerSupply(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_PowerSupply =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Device::hasHistory() const ->
        bool
{
    return _hasHistory;
}

int Device::_callback_get_HasHistory(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hasHistory();
                    }
                ));
    }
}

auto Device::hasHistory(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hasHistory != value)
    {
        _hasHistory = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("HasHistory");
        }
    }

    return _hasHistory;
}

auto Device::hasHistory(bool val) ->
        bool
{
    return hasHistory(val, false);
}

int Device::_callback_set_HasHistory(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hasHistory(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_HasHistory =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Device::hasStatistics() const ->
        bool
{
    return _hasStatistics;
}

int Device::_callback_get_HasStatistics(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->hasStatistics();
                    }
                ));
    }
}

auto Device::hasStatistics(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_hasStatistics != value)
    {
        _hasStatistics = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("HasStatistics");
        }
    }

    return _hasStatistics;
}

auto Device::hasStatistics(bool val) ->
        bool
{
    return hasStatistics(val, false);
}

int Device::_callback_set_HasStatistics(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->hasStatistics(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_HasStatistics =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Device::online() const ->
        bool
{
    return _online;
}

int Device::_callback_get_Online(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->online();
                    }
                ));
    }
}

auto Device::online(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_online != value)
    {
        _online = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Online");
        }
    }

    return _online;
}

auto Device::online(bool val) ->
        bool
{
    return online(val, false);
}

int Device::_callback_set_Online(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->online(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Online =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Device::energy() const ->
        double
{
    return _energy;
}

int Device::_callback_get_Energy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->energy();
                    }
                ));
    }
}

auto Device::energy(double value,
                                         bool skipSignal) ->
        double
{
    if (_energy != value)
    {
        _energy = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Energy");
        }
    }

    return _energy;
}

auto Device::energy(double val) ->
        double
{
    return energy(val, false);
}

int Device::_callback_set_Energy(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->energy(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Energy =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::energyEmpty() const ->
        double
{
    return _energyEmpty;
}

int Device::_callback_get_EnergyEmpty(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->energyEmpty();
                    }
                ));
    }
}

auto Device::energyEmpty(double value,
                                         bool skipSignal) ->
        double
{
    if (_energyEmpty != value)
    {
        _energyEmpty = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("EnergyEmpty");
        }
    }

    return _energyEmpty;
}

auto Device::energyEmpty(double val) ->
        double
{
    return energyEmpty(val, false);
}

int Device::_callback_set_EnergyEmpty(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->energyEmpty(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_EnergyEmpty =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::energyFull() const ->
        double
{
    return _energyFull;
}

int Device::_callback_get_EnergyFull(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->energyFull();
                    }
                ));
    }
}

auto Device::energyFull(double value,
                                         bool skipSignal) ->
        double
{
    if (_energyFull != value)
    {
        _energyFull = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("EnergyFull");
        }
    }

    return _energyFull;
}

auto Device::energyFull(double val) ->
        double
{
    return energyFull(val, false);
}

int Device::_callback_set_EnergyFull(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->energyFull(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_EnergyFull =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::energyFullDesign() const ->
        double
{
    return _energyFullDesign;
}

int Device::_callback_get_EnergyFullDesign(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->energyFullDesign();
                    }
                ));
    }
}

auto Device::energyFullDesign(double value,
                                         bool skipSignal) ->
        double
{
    if (_energyFullDesign != value)
    {
        _energyFullDesign = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("EnergyFullDesign");
        }
    }

    return _energyFullDesign;
}

auto Device::energyFullDesign(double val) ->
        double
{
    return energyFullDesign(val, false);
}

int Device::_callback_set_EnergyFullDesign(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->energyFullDesign(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_EnergyFullDesign =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::energyRate() const ->
        double
{
    return _energyRate;
}

int Device::_callback_get_EnergyRate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->energyRate();
                    }
                ));
    }
}

auto Device::energyRate(double value,
                                         bool skipSignal) ->
        double
{
    if (_energyRate != value)
    {
        _energyRate = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("EnergyRate");
        }
    }

    return _energyRate;
}

auto Device::energyRate(double val) ->
        double
{
    return energyRate(val, false);
}

int Device::_callback_set_EnergyRate(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->energyRate(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_EnergyRate =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::voltage() const ->
        double
{
    return _voltage;
}

int Device::_callback_get_Voltage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->voltage();
                    }
                ));
    }
}

auto Device::voltage(double value,
                                         bool skipSignal) ->
        double
{
    if (_voltage != value)
    {
        _voltage = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Voltage");
        }
    }

    return _voltage;
}

auto Device::voltage(double val) ->
        double
{
    return voltage(val, false);
}

int Device::_callback_set_Voltage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->voltage(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Voltage =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::luminosity() const ->
        double
{
    return _luminosity;
}

int Device::_callback_get_Luminosity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->luminosity();
                    }
                ));
    }
}

auto Device::luminosity(double value,
                                         bool skipSignal) ->
        double
{
    if (_luminosity != value)
    {
        _luminosity = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Luminosity");
        }
    }

    return _luminosity;
}

auto Device::luminosity(double val) ->
        double
{
    return luminosity(val, false);
}

int Device::_callback_set_Luminosity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->luminosity(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Luminosity =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::timeToEmpty() const ->
        int64_t
{
    return _timeToEmpty;
}

int Device::_callback_get_TimeToEmpty(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timeToEmpty();
                    }
                ));
    }
}

auto Device::timeToEmpty(int64_t value,
                                         bool skipSignal) ->
        int64_t
{
    if (_timeToEmpty != value)
    {
        _timeToEmpty = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("TimeToEmpty");
        }
    }

    return _timeToEmpty;
}

auto Device::timeToEmpty(int64_t val) ->
        int64_t
{
    return timeToEmpty(val, false);
}

int Device::_callback_set_TimeToEmpty(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](int64_t&& arg)
                    {
                        o->timeToEmpty(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_TimeToEmpty =
    utility::tuple_to_array(message::types::type_id<
            int64_t>());
}
}

auto Device::timeToFull() const ->
        int64_t
{
    return _timeToFull;
}

int Device::_callback_get_TimeToFull(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->timeToFull();
                    }
                ));
    }
}

auto Device::timeToFull(int64_t value,
                                         bool skipSignal) ->
        int64_t
{
    if (_timeToFull != value)
    {
        _timeToFull = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("TimeToFull");
        }
    }

    return _timeToFull;
}

auto Device::timeToFull(int64_t val) ->
        int64_t
{
    return timeToFull(val, false);
}

int Device::_callback_set_TimeToFull(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](int64_t&& arg)
                    {
                        o->timeToFull(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_TimeToFull =
    utility::tuple_to_array(message::types::type_id<
            int64_t>());
}
}

auto Device::percentage() const ->
        double
{
    return _percentage;
}

int Device::_callback_get_Percentage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->percentage();
                    }
                ));
    }
}

auto Device::percentage(double value,
                                         bool skipSignal) ->
        double
{
    if (_percentage != value)
    {
        _percentage = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Percentage");
        }
    }

    return _percentage;
}

auto Device::percentage(double val) ->
        double
{
    return percentage(val, false);
}

int Device::_callback_set_Percentage(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->percentage(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Percentage =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::temperature() const ->
        double
{
    return _temperature;
}

int Device::_callback_get_Temperature(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->temperature();
                    }
                ));
    }
}

auto Device::temperature(double value,
                                         bool skipSignal) ->
        double
{
    if (_temperature != value)
    {
        _temperature = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Temperature");
        }
    }

    return _temperature;
}

auto Device::temperature(double val) ->
        double
{
    return temperature(val, false);
}

int Device::_callback_set_Temperature(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->temperature(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Temperature =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::isPresent() const ->
        bool
{
    return _isPresent;
}

int Device::_callback_get_IsPresent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->isPresent();
                    }
                ));
    }
}

auto Device::isPresent(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_isPresent != value)
    {
        _isPresent = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("IsPresent");
        }
    }

    return _isPresent;
}

auto Device::isPresent(bool val) ->
        bool
{
    return isPresent(val, false);
}

int Device::_callback_set_IsPresent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->isPresent(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_IsPresent =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Device::state() const ->
        uint32_t
{
    return _state;
}

int Device::_callback_get_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->state();
                    }
                ));
    }
}

auto Device::state(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_state != value)
    {
        _state = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("State");
        }
    }

    return _state;
}

auto Device::state(uint32_t val) ->
        uint32_t
{
    return state(val, false);
}

int Device::_callback_set_State(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->state(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_State =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Device::isRechargeable() const ->
        bool
{
    return _isRechargeable;
}

int Device::_callback_get_IsRechargeable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->isRechargeable();
                    }
                ));
    }
}

auto Device::isRechargeable(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_isRechargeable != value)
    {
        _isRechargeable = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("IsRechargeable");
        }
    }

    return _isRechargeable;
}

auto Device::isRechargeable(bool val) ->
        bool
{
    return isRechargeable(val, false);
}

int Device::_callback_set_IsRechargeable(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->isRechargeable(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_IsRechargeable =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Device::capacity() const ->
        double
{
    return _capacity;
}

int Device::_callback_get_Capacity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->capacity();
                    }
                ));
    }
}

auto Device::capacity(double value,
                                         bool skipSignal) ->
        double
{
    if (_capacity != value)
    {
        _capacity = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Capacity");
        }
    }

    return _capacity;
}

auto Device::capacity(double val) ->
        double
{
    return capacity(val, false);
}

int Device::_callback_set_Capacity(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](double&& arg)
                    {
                        o->capacity(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Capacity =
    utility::tuple_to_array(message::types::type_id<
            double>());
}
}

auto Device::technology() const ->
        uint32_t
{
    return _technology;
}

int Device::_callback_get_Technology(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->technology();
                    }
                ));
    }
}

auto Device::technology(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_technology != value)
    {
        _technology = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("Technology");
        }
    }

    return _technology;
}

auto Device::technology(uint32_t val) ->
        uint32_t
{
    return technology(val, false);
}

int Device::_callback_set_Technology(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->technology(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_Technology =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Device::warningLevel() const ->
        uint32_t
{
    return _warningLevel;
}

int Device::_callback_get_WarningLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->warningLevel();
                    }
                ));
    }
}

auto Device::warningLevel(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_warningLevel != value)
    {
        _warningLevel = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("WarningLevel");
        }
    }

    return _warningLevel;
}

auto Device::warningLevel(uint32_t val) ->
        uint32_t
{
    return warningLevel(val, false);
}

int Device::_callback_set_WarningLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->warningLevel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_WarningLevel =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Device::batteryLevel() const ->
        uint32_t
{
    return _batteryLevel;
}

int Device::_callback_get_BatteryLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->batteryLevel();
                    }
                ));
    }
}

auto Device::batteryLevel(uint32_t value,
                                         bool skipSignal) ->
        uint32_t
{
    if (_batteryLevel != value)
    {
        _batteryLevel = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("BatteryLevel");
        }
    }

    return _batteryLevel;
}

auto Device::batteryLevel(uint32_t val) ->
        uint32_t
{
    return batteryLevel(val, false);
}

int Device::_callback_set_BatteryLevel(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint32_t&& arg)
                    {
                        o->batteryLevel(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_BatteryLevel =
    utility::tuple_to_array(message::types::type_id<
            uint32_t>());
}
}

auto Device::iconName() const ->
        std::string
{
    return _iconName;
}

int Device::_callback_get_IconName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->iconName();
                    }
                ));
    }
}

auto Device::iconName(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_iconName != value)
    {
        _iconName = value;
        if (!skipSignal)
        {
            _org_freedesktop_UPower_Device_interface.property_changed("IconName");
        }
    }

    return _iconName;
}

auto Device::iconName(std::string val) ->
        std::string
{
    return iconName(val, false);
}

int Device::_callback_set_IconName(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Device*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->iconName(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Device
{
static const auto _property_IconName =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Device::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "NativePath")
    {
        auto& v = std::get<std::string>(val);
        nativePath(v, skipSignal);
        return;
    }
    if (_name == "Vendor")
    {
        auto& v = std::get<std::string>(val);
        vendor(v, skipSignal);
        return;
    }
    if (_name == "Model")
    {
        auto& v = std::get<std::string>(val);
        model(v, skipSignal);
        return;
    }
    if (_name == "Serial")
    {
        auto& v = std::get<std::string>(val);
        serial(v, skipSignal);
        return;
    }
    if (_name == "UpdateTime")
    {
        auto& v = std::get<uint64_t>(val);
        updateTime(v, skipSignal);
        return;
    }
    if (_name == "Type")
    {
        auto& v = std::get<uint32_t>(val);
        type(v, skipSignal);
        return;
    }
    if (_name == "PowerSupply")
    {
        auto& v = std::get<bool>(val);
        powerSupply(v, skipSignal);
        return;
    }
    if (_name == "HasHistory")
    {
        auto& v = std::get<bool>(val);
        hasHistory(v, skipSignal);
        return;
    }
    if (_name == "HasStatistics")
    {
        auto& v = std::get<bool>(val);
        hasStatistics(v, skipSignal);
        return;
    }
    if (_name == "Online")
    {
        auto& v = std::get<bool>(val);
        online(v, skipSignal);
        return;
    }
    if (_name == "Energy")
    {
        auto& v = std::get<double>(val);
        energy(v, skipSignal);
        return;
    }
    if (_name == "EnergyEmpty")
    {
        auto& v = std::get<double>(val);
        energyEmpty(v, skipSignal);
        return;
    }
    if (_name == "EnergyFull")
    {
        auto& v = std::get<double>(val);
        energyFull(v, skipSignal);
        return;
    }
    if (_name == "EnergyFullDesign")
    {
        auto& v = std::get<double>(val);
        energyFullDesign(v, skipSignal);
        return;
    }
    if (_name == "EnergyRate")
    {
        auto& v = std::get<double>(val);
        energyRate(v, skipSignal);
        return;
    }
    if (_name == "Voltage")
    {
        auto& v = std::get<double>(val);
        voltage(v, skipSignal);
        return;
    }
    if (_name == "Luminosity")
    {
        auto& v = std::get<double>(val);
        luminosity(v, skipSignal);
        return;
    }
    if (_name == "TimeToEmpty")
    {
        auto& v = std::get<int64_t>(val);
        timeToEmpty(v, skipSignal);
        return;
    }
    if (_name == "TimeToFull")
    {
        auto& v = std::get<int64_t>(val);
        timeToFull(v, skipSignal);
        return;
    }
    if (_name == "Percentage")
    {
        auto& v = std::get<double>(val);
        percentage(v, skipSignal);
        return;
    }
    if (_name == "Temperature")
    {
        auto& v = std::get<double>(val);
        temperature(v, skipSignal);
        return;
    }
    if (_name == "IsPresent")
    {
        auto& v = std::get<bool>(val);
        isPresent(v, skipSignal);
        return;
    }
    if (_name == "State")
    {
        auto& v = std::get<uint32_t>(val);
        state(v, skipSignal);
        return;
    }
    if (_name == "IsRechargeable")
    {
        auto& v = std::get<bool>(val);
        isRechargeable(v, skipSignal);
        return;
    }
    if (_name == "Capacity")
    {
        auto& v = std::get<double>(val);
        capacity(v, skipSignal);
        return;
    }
    if (_name == "Technology")
    {
        auto& v = std::get<uint32_t>(val);
        technology(v, skipSignal);
        return;
    }
    if (_name == "WarningLevel")
    {
        auto& v = std::get<uint32_t>(val);
        warningLevel(v, skipSignal);
        return;
    }
    if (_name == "BatteryLevel")
    {
        auto& v = std::get<uint32_t>(val);
        batteryLevel(v, skipSignal);
        return;
    }
    if (_name == "IconName")
    {
        auto& v = std::get<std::string>(val);
        iconName(v, skipSignal);
        return;
    }
}

auto Device::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "NativePath")
    {
        return nativePath();
    }
    if (_name == "Vendor")
    {
        return vendor();
    }
    if (_name == "Model")
    {
        return model();
    }
    if (_name == "Serial")
    {
        return serial();
    }
    if (_name == "UpdateTime")
    {
        return updateTime();
    }
    if (_name == "Type")
    {
        return type();
    }
    if (_name == "PowerSupply")
    {
        return powerSupply();
    }
    if (_name == "HasHistory")
    {
        return hasHistory();
    }
    if (_name == "HasStatistics")
    {
        return hasStatistics();
    }
    if (_name == "Online")
    {
        return online();
    }
    if (_name == "Energy")
    {
        return energy();
    }
    if (_name == "EnergyEmpty")
    {
        return energyEmpty();
    }
    if (_name == "EnergyFull")
    {
        return energyFull();
    }
    if (_name == "EnergyFullDesign")
    {
        return energyFullDesign();
    }
    if (_name == "EnergyRate")
    {
        return energyRate();
    }
    if (_name == "Voltage")
    {
        return voltage();
    }
    if (_name == "Luminosity")
    {
        return luminosity();
    }
    if (_name == "TimeToEmpty")
    {
        return timeToEmpty();
    }
    if (_name == "TimeToFull")
    {
        return timeToFull();
    }
    if (_name == "Percentage")
    {
        return percentage();
    }
    if (_name == "Temperature")
    {
        return temperature();
    }
    if (_name == "IsPresent")
    {
        return isPresent();
    }
    if (_name == "State")
    {
        return state();
    }
    if (_name == "IsRechargeable")
    {
        return isRechargeable();
    }
    if (_name == "Capacity")
    {
        return capacity();
    }
    if (_name == "Technology")
    {
        return technology();
    }
    if (_name == "WarningLevel")
    {
        return warningLevel();
    }
    if (_name == "BatteryLevel")
    {
        return batteryLevel();
    }
    if (_name == "IconName")
    {
        return iconName();
    }

    return PropertiesVariant();
}


const vtable_t Device::_vtable[] = {
    vtable::start(),

    vtable::method("Refresh",
                   details::Device::_param_Refresh
                        .data(),
                   details::Device::_return_Refresh
                        .data(),
                   _callback_Refresh),

    vtable::method("GetHistory",
                   details::Device::_param_GetHistory
                        .data(),
                   details::Device::_return_GetHistory
                        .data(),
                   _callback_GetHistory),

    vtable::method("GetStatistics",
                   details::Device::_param_GetStatistics
                        .data(),
                   details::Device::_return_GetStatistics
                        .data(),
                   _callback_GetStatistics),
    vtable::property("NativePath",
                     details::Device::_property_NativePath
                        .data(),
                     _callback_get_NativePath,
                     _callback_set_NativePath,
                     vtable::property_::emits_change),
    vtable::property("Vendor",
                     details::Device::_property_Vendor
                        .data(),
                     _callback_get_Vendor,
                     _callback_set_Vendor,
                     vtable::property_::emits_change),
    vtable::property("Model",
                     details::Device::_property_Model
                        .data(),
                     _callback_get_Model,
                     _callback_set_Model,
                     vtable::property_::emits_change),
    vtable::property("Serial",
                     details::Device::_property_Serial
                        .data(),
                     _callback_get_Serial,
                     _callback_set_Serial,
                     vtable::property_::emits_change),
    vtable::property("UpdateTime",
                     details::Device::_property_UpdateTime
                        .data(),
                     _callback_get_UpdateTime,
                     _callback_set_UpdateTime,
                     vtable::property_::emits_change),
    vtable::property("Type",
                     details::Device::_property_Type
                        .data(),
                     _callback_get_Type,
                     _callback_set_Type,
                     vtable::property_::emits_change),
    vtable::property("PowerSupply",
                     details::Device::_property_PowerSupply
                        .data(),
                     _callback_get_PowerSupply,
                     _callback_set_PowerSupply,
                     vtable::property_::emits_change),
    vtable::property("HasHistory",
                     details::Device::_property_HasHistory
                        .data(),
                     _callback_get_HasHistory,
                     _callback_set_HasHistory,
                     vtable::property_::emits_change),
    vtable::property("HasStatistics",
                     details::Device::_property_HasStatistics
                        .data(),
                     _callback_get_HasStatistics,
                     _callback_set_HasStatistics,
                     vtable::property_::emits_change),
    vtable::property("Online",
                     details::Device::_property_Online
                        .data(),
                     _callback_get_Online,
                     _callback_set_Online,
                     vtable::property_::emits_change),
    vtable::property("Energy",
                     details::Device::_property_Energy
                        .data(),
                     _callback_get_Energy,
                     _callback_set_Energy,
                     vtable::property_::emits_change),
    vtable::property("EnergyEmpty",
                     details::Device::_property_EnergyEmpty
                        .data(),
                     _callback_get_EnergyEmpty,
                     _callback_set_EnergyEmpty,
                     vtable::property_::emits_change),
    vtable::property("EnergyFull",
                     details::Device::_property_EnergyFull
                        .data(),
                     _callback_get_EnergyFull,
                     _callback_set_EnergyFull,
                     vtable::property_::emits_change),
    vtable::property("EnergyFullDesign",
                     details::Device::_property_EnergyFullDesign
                        .data(),
                     _callback_get_EnergyFullDesign,
                     _callback_set_EnergyFullDesign,
                     vtable::property_::emits_change),
    vtable::property("EnergyRate",
                     details::Device::_property_EnergyRate
                        .data(),
                     _callback_get_EnergyRate,
                     _callback_set_EnergyRate,
                     vtable::property_::emits_change),
    vtable::property("Voltage",
                     details::Device::_property_Voltage
                        .data(),
                     _callback_get_Voltage,
                     _callback_set_Voltage,
                     vtable::property_::emits_change),
    vtable::property("Luminosity",
                     details::Device::_property_Luminosity
                        .data(),
                     _callback_get_Luminosity,
                     _callback_set_Luminosity,
                     vtable::property_::emits_change),
    vtable::property("TimeToEmpty",
                     details::Device::_property_TimeToEmpty
                        .data(),
                     _callback_get_TimeToEmpty,
                     _callback_set_TimeToEmpty,
                     vtable::property_::emits_change),
    vtable::property("TimeToFull",
                     details::Device::_property_TimeToFull
                        .data(),
                     _callback_get_TimeToFull,
                     _callback_set_TimeToFull,
                     vtable::property_::emits_change),
    vtable::property("Percentage",
                     details::Device::_property_Percentage
                        .data(),
                     _callback_get_Percentage,
                     _callback_set_Percentage,
                     vtable::property_::emits_change),
    vtable::property("Temperature",
                     details::Device::_property_Temperature
                        .data(),
                     _callback_get_Temperature,
                     _callback_set_Temperature,
                     vtable::property_::emits_change),
    vtable::property("IsPresent",
                     details::Device::_property_IsPresent
                        .data(),
                     _callback_get_IsPresent,
                     _callback_set_IsPresent,
                     vtable::property_::emits_change),
    vtable::property("State",
                     details::Device::_property_State
                        .data(),
                     _callback_get_State,
                     _callback_set_State,
                     vtable::property_::emits_change),
    vtable::property("IsRechargeable",
                     details::Device::_property_IsRechargeable
                        .data(),
                     _callback_get_IsRechargeable,
                     _callback_set_IsRechargeable,
                     vtable::property_::emits_change),
    vtable::property("Capacity",
                     details::Device::_property_Capacity
                        .data(),
                     _callback_get_Capacity,
                     _callback_set_Capacity,
                     vtable::property_::emits_change),
    vtable::property("Technology",
                     details::Device::_property_Technology
                        .data(),
                     _callback_get_Technology,
                     _callback_set_Technology,
                     vtable::property_::emits_change),
    vtable::property("WarningLevel",
                     details::Device::_property_WarningLevel
                        .data(),
                     _callback_get_WarningLevel,
                     _callback_set_WarningLevel,
                     vtable::property_::emits_change),
    vtable::property("BatteryLevel",
                     details::Device::_property_BatteryLevel
                        .data(),
                     _callback_get_BatteryLevel,
                     _callback_set_BatteryLevel,
                     vtable::property_::emits_change),
    vtable::property("IconName",
                     details::Device::_property_IconName
                        .data(),
                     _callback_get_IconName,
                     _callback_set_IconName,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace UPower
} // namespace freedesktop
} // namespace org
} // namespace sdbusplus

