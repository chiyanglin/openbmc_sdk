#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <com/ibm/ipzvpd/VSRC/server.hpp>




namespace sdbusplus
{
namespace com
{
namespace ibm
{
namespace ipzvpd
{
namespace server
{

VSRC::VSRC(bus_t& bus, const char* path)
        : _com_ibm_ipzvpd_VSRC_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

VSRC::VSRC(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : VSRC(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto VSRC::rt() const ->
        std::vector<uint8_t>
{
    return _rt;
}

int VSRC::_callback_get_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSRC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->rt();
                    }
                ));
    }
}

auto VSRC::rt(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_rt != value)
    {
        _rt = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSRC_interface.property_changed("RT");
        }
    }

    return _rt;
}

auto VSRC::rt(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return rt(val, false);
}

int VSRC::_callback_set_RT(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSRC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->rt(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSRC
{
static const auto _property_RT =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSRC::dr() const ->
        std::vector<uint8_t>
{
    return _dr;
}

int VSRC::_callback_get_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSRC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->dr();
                    }
                ));
    }
}

auto VSRC::dr(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_dr != value)
    {
        _dr = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSRC_interface.property_changed("DR");
        }
    }

    return _dr;
}

auto VSRC::dr(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return dr(val, false);
}

int VSRC::_callback_set_DR(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSRC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->dr(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSRC
{
static const auto _property_DR =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

auto VSRC::in() const ->
        std::vector<uint8_t>
{
    return _in;
}

int VSRC::_callback_get_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSRC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->in();
                    }
                ));
    }
}

auto VSRC::in(std::vector<uint8_t> value,
                                         bool skipSignal) ->
        std::vector<uint8_t>
{
    if (_in != value)
    {
        _in = value;
        if (!skipSignal)
        {
            _com_ibm_ipzvpd_VSRC_interface.property_changed("IN");
        }
    }

    return _in;
}

auto VSRC::in(std::vector<uint8_t> val) ->
        std::vector<uint8_t>
{
    return in(val, false);
}

int VSRC::_callback_set_IN(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<VSRC*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<uint8_t>&& arg)
                    {
                        o->in(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace VSRC
{
static const auto _property_IN =
    utility::tuple_to_array(message::types::type_id<
            std::vector<uint8_t>>());
}
}

void VSRC::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "RT")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        rt(v, skipSignal);
        return;
    }
    if (_name == "DR")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        dr(v, skipSignal);
        return;
    }
    if (_name == "IN")
    {
        auto& v = std::get<std::vector<uint8_t>>(val);
        in(v, skipSignal);
        return;
    }
}

auto VSRC::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "RT")
    {
        return rt();
    }
    if (_name == "DR")
    {
        return dr();
    }
    if (_name == "IN")
    {
        return in();
    }

    return PropertiesVariant();
}


const vtable_t VSRC::_vtable[] = {
    vtable::start(),
    vtable::property("RT",
                     details::VSRC::_property_RT
                        .data(),
                     _callback_get_RT,
                     _callback_set_RT,
                     vtable::property_::emits_change),
    vtable::property("DR",
                     details::VSRC::_property_DR
                        .data(),
                     _callback_get_DR,
                     _callback_set_DR,
                     vtable::property_::emits_change),
    vtable::property("IN",
                     details::VSRC::_property_IN
                        .data(),
                     _callback_get_IN,
                     _callback_set_IN,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace ipzvpd
} // namespace ibm
} // namespace com
} // namespace sdbusplus

