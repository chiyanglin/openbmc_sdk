#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>
















#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Inventory
{
namespace Source
{
namespace PLDM
{
namespace server
{

class FRU
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        FRU() = delete;
        FRU(const FRU&) = delete;
        FRU& operator=(const FRU&) = delete;
        FRU(FRU&&) = delete;
        FRU& operator=(FRU&&) = delete;
        virtual ~FRU() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        FRU(bus_t& bus, const char* path);


        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                std::string,
                uint32_t>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        FRU(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of ChassisType */
        virtual std::string chassisType() const;
        /** Set value of ChassisType with option to skip sending signal */
        virtual std::string chassisType(std::string value,
               bool skipSignal);
        /** Set value of ChassisType */
        virtual std::string chassisType(std::string value);
        /** Get value of Model */
        virtual std::string model() const;
        /** Set value of Model with option to skip sending signal */
        virtual std::string model(std::string value,
               bool skipSignal);
        /** Set value of Model */
        virtual std::string model(std::string value);
        /** Get value of PN */
        virtual std::string pn() const;
        /** Set value of PN with option to skip sending signal */
        virtual std::string pn(std::string value,
               bool skipSignal);
        /** Set value of PN */
        virtual std::string pn(std::string value);
        /** Get value of SN */
        virtual std::string sn() const;
        /** Set value of SN with option to skip sending signal */
        virtual std::string sn(std::string value,
               bool skipSignal);
        /** Set value of SN */
        virtual std::string sn(std::string value);
        /** Get value of Manufacturer */
        virtual std::string manufacturer() const;
        /** Set value of Manufacturer with option to skip sending signal */
        virtual std::string manufacturer(std::string value,
               bool skipSignal);
        /** Set value of Manufacturer */
        virtual std::string manufacturer(std::string value);
        /** Get value of ManufacturerDate */
        virtual std::string manufacturerDate() const;
        /** Set value of ManufacturerDate with option to skip sending signal */
        virtual std::string manufacturerDate(std::string value,
               bool skipSignal);
        /** Set value of ManufacturerDate */
        virtual std::string manufacturerDate(std::string value);
        /** Get value of Vendor */
        virtual std::string vendor() const;
        /** Set value of Vendor with option to skip sending signal */
        virtual std::string vendor(std::string value,
               bool skipSignal);
        /** Set value of Vendor */
        virtual std::string vendor(std::string value);
        /** Get value of Name */
        virtual std::string name() const;
        /** Set value of Name with option to skip sending signal */
        virtual std::string name(std::string value,
               bool skipSignal);
        /** Set value of Name */
        virtual std::string name(std::string value);
        /** Get value of SKU */
        virtual std::string sku() const;
        /** Set value of SKU with option to skip sending signal */
        virtual std::string sku(std::string value,
               bool skipSignal);
        /** Set value of SKU */
        virtual std::string sku(std::string value);
        /** Get value of Version */
        virtual std::string version() const;
        /** Set value of Version with option to skip sending signal */
        virtual std::string version(std::string value,
               bool skipSignal);
        /** Set value of Version */
        virtual std::string version(std::string value);
        /** Get value of AssetTag */
        virtual std::string assetTag() const;
        /** Set value of AssetTag with option to skip sending signal */
        virtual std::string assetTag(std::string value,
               bool skipSignal);
        /** Set value of AssetTag */
        virtual std::string assetTag(std::string value);
        /** Get value of Description */
        virtual std::string description() const;
        /** Set value of Description with option to skip sending signal */
        virtual std::string description(std::string value,
               bool skipSignal);
        /** Set value of Description */
        virtual std::string description(std::string value);
        /** Get value of ECLevel */
        virtual std::string ecLevel() const;
        /** Set value of ECLevel with option to skip sending signal */
        virtual std::string ecLevel(std::string value,
               bool skipSignal);
        /** Set value of ECLevel */
        virtual std::string ecLevel(std::string value);
        /** Get value of Other */
        virtual std::string other() const;
        /** Set value of Other with option to skip sending signal */
        virtual std::string other(std::string value,
               bool skipSignal);
        /** Set value of Other */
        virtual std::string other(std::string value);
        /** Get value of IANA */
        virtual uint32_t iana() const;
        /** Set value of IANA with option to skip sending signal */
        virtual uint32_t iana(uint32_t value,
               bool skipSignal);
        /** Set value of IANA */
        virtual uint32_t iana(uint32_t value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);


        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Inventory.Source.PLDM.FRU";

    private:

        /** @brief sd-bus callback for get-property 'ChassisType' */
        static int _callback_get_ChassisType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ChassisType' */
        static int _callback_set_ChassisType(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Model' */
        static int _callback_get_Model(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Model' */
        static int _callback_set_Model(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'PN' */
        static int _callback_get_PN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'PN' */
        static int _callback_set_PN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SN' */
        static int _callback_get_SN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SN' */
        static int _callback_set_SN(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Manufacturer' */
        static int _callback_get_Manufacturer(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Manufacturer' */
        static int _callback_set_Manufacturer(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ManufacturerDate' */
        static int _callback_get_ManufacturerDate(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ManufacturerDate' */
        static int _callback_set_ManufacturerDate(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Vendor' */
        static int _callback_get_Vendor(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Vendor' */
        static int _callback_set_Vendor(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Name' */
        static int _callback_get_Name(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Name' */
        static int _callback_set_Name(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'SKU' */
        static int _callback_get_SKU(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SKU' */
        static int _callback_set_SKU(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Version' */
        static int _callback_get_Version(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Version' */
        static int _callback_set_Version(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'AssetTag' */
        static int _callback_get_AssetTag(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'AssetTag' */
        static int _callback_set_AssetTag(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Description' */
        static int _callback_get_Description(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Description' */
        static int _callback_set_Description(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'ECLevel' */
        static int _callback_get_ECLevel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'ECLevel' */
        static int _callback_set_ECLevel(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'Other' */
        static int _callback_get_Other(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'Other' */
        static int _callback_set_Other(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'IANA' */
        static int _callback_get_IANA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'IANA' */
        static int _callback_set_IANA(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Inventory_Source_PLDM_FRU_interface;
        sdbusplus::SdBusInterface *_intf;

        std::string _chassisType{};
        std::string _model{};
        std::string _pn{};
        std::string _sn{};
        std::string _manufacturer{};
        std::string _manufacturerDate{};
        std::string _vendor{};
        std::string _name{};
        std::string _sku{};
        std::string _version{};
        std::string _assetTag{};
        std::string _description{};
        std::string _ecLevel{};
        std::string _other{};
        uint32_t _iana{};

};


} // namespace server
} // namespace PLDM
} // namespace Source
} // namespace Inventory
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

