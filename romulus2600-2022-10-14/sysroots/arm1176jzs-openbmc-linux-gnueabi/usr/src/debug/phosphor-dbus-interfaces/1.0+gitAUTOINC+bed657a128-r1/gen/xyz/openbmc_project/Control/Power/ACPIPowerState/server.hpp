#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>



#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Control
{
namespace Power
{
namespace server
{

class ACPIPowerState
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        ACPIPowerState() = delete;
        ACPIPowerState(const ACPIPowerState&) = delete;
        ACPIPowerState& operator=(const ACPIPowerState&) = delete;
        ACPIPowerState(ACPIPowerState&&) = delete;
        ACPIPowerState& operator=(ACPIPowerState&&) = delete;
        virtual ~ACPIPowerState() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        ACPIPowerState(bus_t& bus, const char* path);

        enum class ACPI
        {
            S0_G0_D0,
            S1_D1,
            S2_D2,
            S3_D3,
            S4,
            S5_G2,
            S4_S5,
            G3,
            SLEEP,
            G1_SLEEP,
            OVERRIDE,
            LEGACY_ON,
            LEGACY_OFF,
            Unknown,
        };

        using PropertiesVariant = sdbusplus::utility::dedup_variant_t<
                ACPI>;

        /** @brief Constructor to initialize the object from a map of
         *         properties.
         *
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         *  @param[in] vals - Map of property name to value for initialization.
         */
        ACPIPowerState(bus_t& bus, const char* path,
                     const std::map<std::string, PropertiesVariant>& vals,
                     bool skipSignal = false);



        /** Get value of SysACPIStatus */
        virtual ACPI sysACPIStatus() const;
        /** Set value of SysACPIStatus with option to skip sending signal */
        virtual ACPI sysACPIStatus(ACPI value,
               bool skipSignal);
        /** Set value of SysACPIStatus */
        virtual ACPI sysACPIStatus(ACPI value);
        /** Get value of DevACPIStatus */
        virtual ACPI devACPIStatus() const;
        /** Set value of DevACPIStatus with option to skip sending signal */
        virtual ACPI devACPIStatus(ACPI value,
               bool skipSignal);
        /** Set value of DevACPIStatus */
        virtual ACPI devACPIStatus(ACPI value);

        /** @brief Sets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @param[in] val - A variant containing the value to set.
         */
        void setPropertyByName(const std::string& _name,
                               const PropertiesVariant& val,
                               bool skipSignal = false);

        /** @brief Gets a property by name.
         *  @param[in] _name - A string representation of the property name.
         *  @return - A variant containing the value of the property.
         */
        PropertiesVariant getPropertyByName(const std::string& _name);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Power.ACPIPowerState.<value name>"
         *  @return - The enum value.
         *
         *  @note Throws if string is not a valid mapping.
         */
        static ACPI convertACPIFromString(const std::string& s);

        /** @brief Convert a string to an appropriate enum value.
         *  @param[in] s - The string to convert in the form of
         *                 "xyz.openbmc_project.Control.Power.ACPIPowerState.<value name>"
         *  @return - The enum value or std::nullopt
         */
        static std::optional<ACPI> convertStringToACPI(
                const std::string& s) noexcept;

        /** @brief Convert an enum value to a string.
         *  @param[in] e - The enum to convert to a string.
         *  @return - The string conversion in the form of
         *            "xyz.openbmc_project.Control.Power.ACPIPowerState.<value name>"
         */
        static std::string convertACPIToString(ACPI e);

        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Control_Power_ACPIPowerState_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Control_Power_ACPIPowerState_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Control.Power.ACPIPowerState";

    private:

        /** @brief sd-bus callback for get-property 'SysACPIStatus' */
        static int _callback_get_SysACPIStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'SysACPIStatus' */
        static int _callback_set_SysACPIStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);

        /** @brief sd-bus callback for get-property 'DevACPIStatus' */
        static int _callback_get_DevACPIStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);
        /** @brief sd-bus callback for set-property 'DevACPIStatus' */
        static int _callback_set_DevACPIStatus(
            sd_bus*, const char*, const char*, const char*,
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Control_Power_ACPIPowerState_interface;
        sdbusplus::SdBusInterface *_intf;

        ACPI _sysACPIStatus{};
        ACPI _devACPIStatus{};

};

/* Specialization of sdbusplus::server::convertForMessage
 * for enum-type ACPIPowerState::ACPI.
 *
 * This converts from the enum to a constant c-string representing the enum.
 *
 * @param[in] e - Enum value to convert.
 * @return C-string representing the name for the enum value.
 */
inline std::string convertForMessage(ACPIPowerState::ACPI e)
{
    return ACPIPowerState::convertACPIToString(e);
}

} // namespace server
} // namespace Power
} // namespace Control
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
template <>
struct convert_from_string<xyz::openbmc_project::Control::Power::server::ACPIPowerState::ACPI>
{
    static auto op(const std::string& value) noexcept
    {
        return xyz::openbmc_project::Control::Power::server::ACPIPowerState::convertStringToACPI(value);
    }
};

template <>
struct convert_to_string<xyz::openbmc_project::Control::Power::server::ACPIPowerState::ACPI>
{
    static std::string op(xyz::openbmc_project::Control::Power::server::ACPIPowerState::ACPI value)
    {
        return xyz::openbmc_project::Control::Power::server::ACPIPowerState::convertACPIToString(value);
    }
};
} // namespace message::details
} // namespace sdbusplus

