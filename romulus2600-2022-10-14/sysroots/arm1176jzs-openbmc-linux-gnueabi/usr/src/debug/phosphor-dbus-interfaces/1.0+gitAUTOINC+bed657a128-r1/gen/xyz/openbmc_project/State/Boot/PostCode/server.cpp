#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/State/Boot/PostCode/server.hpp>





namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace State
{
namespace Boot
{
namespace server
{

PostCode::PostCode(bus_t& bus, const char* path)
        : _xyz_openbmc_project_State_Boot_PostCode_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

PostCode::PostCode(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : PostCode(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}


int PostCode::_callback_GetPostCodesWithTimeStamp(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PostCode*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint16_t&& index)
                    {
                        return o->getPostCodesWithTimeStamp(
                                index);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace PostCode
{
static const auto _param_GetPostCodesWithTimeStamp =
        utility::tuple_to_array(message::types::type_id<
                uint16_t>());
static const auto _return_GetPostCodesWithTimeStamp =
        utility::tuple_to_array(message::types::type_id<
                std::map<uint64_t, std::tuple<uint64_t, std::vector<uint8_t>>>>());
}
}

int PostCode::_callback_GetPostCodes(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<PostCode*>(context);

    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](uint16_t&& index)
                    {
                        return o->getPostCodes(
                                index);
                    }
                ));
    }

    return 0;
}

namespace details
{
namespace PostCode
{
static const auto _param_GetPostCodes =
        utility::tuple_to_array(message::types::type_id<
                uint16_t>());
static const auto _return_GetPostCodes =
        utility::tuple_to_array(message::types::type_id<
                std::vector<std::tuple<uint64_t, std::vector<uint8_t>>>>());
}
}



auto PostCode::currentBootCycleCount() const ->
        uint16_t
{
    return _currentBootCycleCount;
}

int PostCode::_callback_get_CurrentBootCycleCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PostCode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->currentBootCycleCount();
                    }
                ));
    }
}

auto PostCode::currentBootCycleCount(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_currentBootCycleCount != value)
    {
        _currentBootCycleCount = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Boot_PostCode_interface.property_changed("CurrentBootCycleCount");
        }
    }

    return _currentBootCycleCount;
}

auto PostCode::currentBootCycleCount(uint16_t val) ->
        uint16_t
{
    return currentBootCycleCount(val, false);
}

int PostCode::_callback_set_CurrentBootCycleCount(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PostCode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->currentBootCycleCount(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PostCode
{
static const auto _property_CurrentBootCycleCount =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

auto PostCode::maxBootCycleNum() const ->
        uint16_t
{
    return _maxBootCycleNum;
}

int PostCode::_callback_get_MaxBootCycleNum(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PostCode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->maxBootCycleNum();
                    }
                ));
    }
}

auto PostCode::maxBootCycleNum(uint16_t value,
                                         bool skipSignal) ->
        uint16_t
{
    if (_maxBootCycleNum != value)
    {
        _maxBootCycleNum = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_State_Boot_PostCode_interface.property_changed("MaxBootCycleNum");
        }
    }

    return _maxBootCycleNum;
}

auto PostCode::maxBootCycleNum(uint16_t val) ->
        uint16_t
{
    return maxBootCycleNum(val, false);
}

int PostCode::_callback_set_MaxBootCycleNum(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<PostCode*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](uint16_t&& arg)
                    {
                        o->maxBootCycleNum(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace PostCode
{
static const auto _property_MaxBootCycleNum =
    utility::tuple_to_array(message::types::type_id<
            uint16_t>());
}
}

void PostCode::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "CurrentBootCycleCount")
    {
        auto& v = std::get<uint16_t>(val);
        currentBootCycleCount(v, skipSignal);
        return;
    }
    if (_name == "MaxBootCycleNum")
    {
        auto& v = std::get<uint16_t>(val);
        maxBootCycleNum(v, skipSignal);
        return;
    }
}

auto PostCode::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "CurrentBootCycleCount")
    {
        return currentBootCycleCount();
    }
    if (_name == "MaxBootCycleNum")
    {
        return maxBootCycleNum();
    }

    return PropertiesVariant();
}


const vtable_t PostCode::_vtable[] = {
    vtable::start(),

    vtable::method("GetPostCodesWithTimeStamp",
                   details::PostCode::_param_GetPostCodesWithTimeStamp
                        .data(),
                   details::PostCode::_return_GetPostCodesWithTimeStamp
                        .data(),
                   _callback_GetPostCodesWithTimeStamp),

    vtable::method("GetPostCodes",
                   details::PostCode::_param_GetPostCodes
                        .data(),
                   details::PostCode::_return_GetPostCodes
                        .data(),
                   _callback_GetPostCodes),
    vtable::property("CurrentBootCycleCount",
                     details::PostCode::_property_CurrentBootCycleCount
                        .data(),
                     _callback_get_CurrentBootCycleCount,
                     _callback_set_CurrentBootCycleCount,
                     vtable::property_::emits_change),
    vtable::property("MaxBootCycleNum",
                     details::PostCode::_property_MaxBootCycleNum
                        .data(),
                     _callback_get_MaxBootCycleNum,
                     _callback_set_MaxBootCycleNum,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Boot
} // namespace State
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

