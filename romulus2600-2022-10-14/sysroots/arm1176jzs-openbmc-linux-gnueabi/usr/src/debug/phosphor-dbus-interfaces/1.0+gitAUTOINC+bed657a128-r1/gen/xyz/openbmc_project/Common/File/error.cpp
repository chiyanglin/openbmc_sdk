#include <xyz/openbmc_project/Common/File/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Common
{
namespace File
{
namespace Error
{
const char* Open::name() const noexcept
{
    return errName;
}
const char* Open::description() const noexcept
{
    return errDesc;
}
const char* Open::what() const noexcept
{
    return errWhat;
}
const char* Seek::name() const noexcept
{
    return errName;
}
const char* Seek::description() const noexcept
{
    return errDesc;
}
const char* Seek::what() const noexcept
{
    return errWhat;
}
const char* Write::name() const noexcept
{
    return errName;
}
const char* Write::description() const noexcept
{
    return errDesc;
}
const char* Write::what() const noexcept
{
    return errWhat;
}
const char* Read::name() const noexcept
{
    return errName;
}
const char* Read::description() const noexcept
{
    return errDesc;
}
const char* Read::what() const noexcept
{
    return errWhat;
}

} // namespace Error
} // namespace File
} // namespace Common
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

