#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Telemetry/Trigger/server.hpp>








namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Telemetry
{
namespace server
{

Trigger::Trigger(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Telemetry_Trigger_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

Trigger::Trigger(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : Trigger(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto Trigger::discrete() const ->
        bool
{
    return _discrete;
}

int Trigger::_callback_get_Discrete(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->discrete();
                    }
                ));
    }
}

auto Trigger::discrete(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_discrete != value)
    {
        _discrete = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("Discrete");
        }
    }

    return _discrete;
}

auto Trigger::discrete(bool val) ->
        bool
{
    return discrete(val, false);
}


namespace details
{
namespace Trigger
{
static const auto _property_Discrete =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Trigger::triggerActions() const ->
        std::vector<TriggerAction>
{
    return _triggerActions;
}

int Trigger::_callback_get_TriggerActions(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->triggerActions();
                    }
                ));
    }
}

auto Trigger::triggerActions(std::vector<TriggerAction> value,
                                         bool skipSignal) ->
        std::vector<TriggerAction>
{
    if (_triggerActions != value)
    {
        _triggerActions = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("TriggerActions");
        }
    }

    return _triggerActions;
}

auto Trigger::triggerActions(std::vector<TriggerAction> val) ->
        std::vector<TriggerAction>
{
    return triggerActions(val, false);
}


namespace details
{
namespace Trigger
{
static const auto _property_TriggerActions =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::xyz::openbmc_project::Telemetry::server::Trigger::TriggerAction>>());
}
}

auto Trigger::persistent() const ->
        bool
{
    return _persistent;
}

int Trigger::_callback_get_Persistent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->persistent();
                    }
                ));
    }
}

auto Trigger::persistent(bool value,
                                         bool skipSignal) ->
        bool
{
    if (_persistent != value)
    {
        _persistent = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("Persistent");
        }
    }

    return _persistent;
}

auto Trigger::persistent(bool val) ->
        bool
{
    return persistent(val, false);
}

int Trigger::_callback_set_Persistent(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](bool&& arg)
                    {
                        o->persistent(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Trigger
{
static const auto _property_Persistent =
    utility::tuple_to_array(message::types::type_id<
            bool>());
}
}

auto Trigger::reports() const ->
        std::vector<sdbusplus::message::object_path>
{
    return _reports;
}

int Trigger::_callback_get_Reports(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->reports();
                    }
                ));
    }
}

auto Trigger::reports(std::vector<sdbusplus::message::object_path> value,
                                         bool skipSignal) ->
        std::vector<sdbusplus::message::object_path>
{
    if (_reports != value)
    {
        _reports = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("Reports");
        }
    }

    return _reports;
}

auto Trigger::reports(std::vector<sdbusplus::message::object_path> val) ->
        std::vector<sdbusplus::message::object_path>
{
    return reports(val, false);
}

int Trigger::_callback_set_Reports(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<sdbusplus::message::object_path>&& arg)
                    {
                        o->reports(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Trigger
{
static const auto _property_Reports =
    utility::tuple_to_array(message::types::type_id<
            std::vector<sdbusplus::message::object_path>>());
}
}

auto Trigger::sensors() const ->
        std::vector<std::map<sdbusplus::message::object_path, std::string>>
{
    return _sensors;
}

int Trigger::_callback_get_Sensors(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->sensors();
                    }
                ));
    }
}

auto Trigger::sensors(std::vector<std::map<sdbusplus::message::object_path, std::string>> value,
                                         bool skipSignal) ->
        std::vector<std::map<sdbusplus::message::object_path, std::string>>
{
    if (_sensors != value)
    {
        _sensors = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("Sensors");
        }
    }

    return _sensors;
}

auto Trigger::sensors(std::vector<std::map<sdbusplus::message::object_path, std::string>> val) ->
        std::vector<std::map<sdbusplus::message::object_path, std::string>>
{
    return sensors(val, false);
}

int Trigger::_callback_set_Sensors(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::vector<std::map<sdbusplus::message::object_path, std::string>>&& arg)
                    {
                        o->sensors(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Trigger
{
static const auto _property_Sensors =
    utility::tuple_to_array(message::types::type_id<
            std::vector<std::map<sdbusplus::message::object_path, std::string>>>());
}
}

auto Trigger::thresholds() const ->
        std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>>
{
    return _thresholds;
}

int Trigger::_callback_get_Thresholds(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->thresholds();
                    }
                ));
    }
}

auto Trigger::thresholds(std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> value,
                                         bool skipSignal) ->
        std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>>
{
    if (_thresholds != value)
    {
        _thresholds = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("Thresholds");
        }
    }

    return _thresholds;
}

auto Trigger::thresholds(std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>> val) ->
        std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>>
{
    return thresholds(val, false);
}

int Trigger::_callback_set_Thresholds(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>>&& arg)
                    {
                        o->thresholds(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Trigger
{
static const auto _property_Thresholds =
    utility::tuple_to_array(message::types::type_id<
            std::variant<std::vector<std::tuple<sdbusplus::xyz::openbmc_project::Telemetry::server::Trigger::Type, uint64_t, sdbusplus::xyz::openbmc_project::Telemetry::server::Trigger::Direction, double>>, std::vector<std::tuple<std::string, sdbusplus::xyz::openbmc_project::Telemetry::server::Trigger::Severity, uint64_t, std::string>>>>());
}
}

auto Trigger::name() const ->
        std::string
{
    return _name;
}

int Trigger::_callback_get_Name(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->name();
                    }
                ));
    }
}

auto Trigger::name(std::string value,
                                         bool skipSignal) ->
        std::string
{
    if (_name != value)
    {
        _name = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Telemetry_Trigger_interface.property_changed("Name");
        }
    }

    return _name;
}

auto Trigger::name(std::string val) ->
        std::string
{
    return name(val, false);
}

int Trigger::_callback_set_Name(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<Trigger*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](std::string&& arg)
                    {
                        o->name(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace Trigger
{
static const auto _property_Name =
    utility::tuple_to_array(message::types::type_id<
            std::string>());
}
}

void Trigger::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "Discrete")
    {
        auto& v = std::get<bool>(val);
        discrete(v, skipSignal);
        return;
    }
    if (_name == "TriggerActions")
    {
        auto& v = std::get<std::vector<TriggerAction>>(val);
        triggerActions(v, skipSignal);
        return;
    }
    if (_name == "Persistent")
    {
        auto& v = std::get<bool>(val);
        persistent(v, skipSignal);
        return;
    }
    if (_name == "Reports")
    {
        auto& v = std::get<std::vector<sdbusplus::message::object_path>>(val);
        reports(v, skipSignal);
        return;
    }
    if (_name == "Sensors")
    {
        auto& v = std::get<std::vector<std::map<sdbusplus::message::object_path, std::string>>>(val);
        sensors(v, skipSignal);
        return;
    }
    if (_name == "Thresholds")
    {
        auto& v = std::get<std::variant<std::vector<std::tuple<Type, uint64_t, Direction, double>>, std::vector<std::tuple<std::string, Severity, uint64_t, std::string>>>>(val);
        thresholds(v, skipSignal);
        return;
    }
    if (_name == "Name")
    {
        auto& v = std::get<std::string>(val);
        name(v, skipSignal);
        return;
    }
}

auto Trigger::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "Discrete")
    {
        return discrete();
    }
    if (_name == "TriggerActions")
    {
        return triggerActions();
    }
    if (_name == "Persistent")
    {
        return persistent();
    }
    if (_name == "Reports")
    {
        return reports();
    }
    if (_name == "Sensors")
    {
        return sensors();
    }
    if (_name == "Thresholds")
    {
        return thresholds();
    }
    if (_name == "Name")
    {
        return name();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for Trigger::TriggerAction */
static const std::tuple<const char*, Trigger::TriggerAction> mappingTriggerTriggerAction[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.TriggerAction.LogToJournal",                 Trigger::TriggerAction::LogToJournal ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.TriggerAction.LogToRedfishEventLog",                 Trigger::TriggerAction::LogToRedfishEventLog ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.TriggerAction.UpdateReport",                 Trigger::TriggerAction::UpdateReport ),
        };

} // anonymous namespace

auto Trigger::convertStringToTriggerAction(const std::string& s) noexcept ->
        std::optional<TriggerAction>
{
    auto i = std::find_if(
            std::begin(mappingTriggerTriggerAction),
            std::end(mappingTriggerTriggerAction),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingTriggerTriggerAction) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Trigger::convertTriggerActionFromString(const std::string& s) ->
        TriggerAction
{
    auto r = convertStringToTriggerAction(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Trigger::convertTriggerActionToString(Trigger::TriggerAction v)
{
    auto i = std::find_if(
            std::begin(mappingTriggerTriggerAction),
            std::end(mappingTriggerTriggerAction),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingTriggerTriggerAction))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Trigger::Type */
static const std::tuple<const char*, Trigger::Type> mappingTriggerType[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Type.LowerCritical",                 Trigger::Type::LowerCritical ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Type.LowerWarning",                 Trigger::Type::LowerWarning ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Type.UpperWarning",                 Trigger::Type::UpperWarning ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Type.UpperCritical",                 Trigger::Type::UpperCritical ),
        };

} // anonymous namespace

auto Trigger::convertStringToType(const std::string& s) noexcept ->
        std::optional<Type>
{
    auto i = std::find_if(
            std::begin(mappingTriggerType),
            std::end(mappingTriggerType),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingTriggerType) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Trigger::convertTypeFromString(const std::string& s) ->
        Type
{
    auto r = convertStringToType(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Trigger::convertTypeToString(Trigger::Type v)
{
    auto i = std::find_if(
            std::begin(mappingTriggerType),
            std::end(mappingTriggerType),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingTriggerType))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Trigger::Direction */
static const std::tuple<const char*, Trigger::Direction> mappingTriggerDirection[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Direction.Either",                 Trigger::Direction::Either ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Direction.Decreasing",                 Trigger::Direction::Decreasing ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Direction.Increasing",                 Trigger::Direction::Increasing ),
        };

} // anonymous namespace

auto Trigger::convertStringToDirection(const std::string& s) noexcept ->
        std::optional<Direction>
{
    auto i = std::find_if(
            std::begin(mappingTriggerDirection),
            std::end(mappingTriggerDirection),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingTriggerDirection) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Trigger::convertDirectionFromString(const std::string& s) ->
        Direction
{
    auto r = convertStringToDirection(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Trigger::convertDirectionToString(Trigger::Direction v)
{
    auto i = std::find_if(
            std::begin(mappingTriggerDirection),
            std::end(mappingTriggerDirection),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingTriggerDirection))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

namespace
{
/** String to enum mapping for Trigger::Severity */
static const std::tuple<const char*, Trigger::Severity> mappingTriggerSeverity[] =
        {
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Severity.OK",                 Trigger::Severity::OK ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Severity.Warning",                 Trigger::Severity::Warning ),
            std::make_tuple( "xyz.openbmc_project.Telemetry.Trigger.Severity.Critical",                 Trigger::Severity::Critical ),
        };

} // anonymous namespace

auto Trigger::convertStringToSeverity(const std::string& s) noexcept ->
        std::optional<Severity>
{
    auto i = std::find_if(
            std::begin(mappingTriggerSeverity),
            std::end(mappingTriggerSeverity),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingTriggerSeverity) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto Trigger::convertSeverityFromString(const std::string& s) ->
        Severity
{
    auto r = convertStringToSeverity(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string Trigger::convertSeverityToString(Trigger::Severity v)
{
    auto i = std::find_if(
            std::begin(mappingTriggerSeverity),
            std::end(mappingTriggerSeverity),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingTriggerSeverity))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t Trigger::_vtable[] = {
    vtable::start(),
    vtable::property("Discrete",
                     details::Trigger::_property_Discrete
                        .data(),
                     _callback_get_Discrete,
                     vtable::property_::const_),
    vtable::property("TriggerActions",
                     details::Trigger::_property_TriggerActions
                        .data(),
                     _callback_get_TriggerActions,
                     vtable::property_::const_),
    vtable::property("Persistent",
                     details::Trigger::_property_Persistent
                        .data(),
                     _callback_get_Persistent,
                     _callback_set_Persistent,
                     vtable::property_::emits_change),
    vtable::property("Reports",
                     details::Trigger::_property_Reports
                        .data(),
                     _callback_get_Reports,
                     _callback_set_Reports,
                     vtable::property_::emits_change),
    vtable::property("Sensors",
                     details::Trigger::_property_Sensors
                        .data(),
                     _callback_get_Sensors,
                     _callback_set_Sensors,
                     vtable::property_::emits_change),
    vtable::property("Thresholds",
                     details::Trigger::_property_Thresholds
                        .data(),
                     _callback_get_Thresholds,
                     _callback_set_Thresholds,
                     vtable::property_::emits_change),
    vtable::property("Name",
                     details::Trigger::_property_Name
                        .data(),
                     _callback_get_Name,
                     _callback_set_Name,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Telemetry
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

