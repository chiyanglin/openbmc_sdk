#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/VLAN/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>
#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace VLAN
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_VLAN_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_VLAN(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& interfaceName, uint32_t&& id)
                    {
                        return o->vlan(
                                interfaceName, id);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::ResourceNotFound& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_VLAN =
        utility::tuple_to_array(message::types::type_id<
                std::string, uint32_t>());
static const auto _return_VLAN =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("VLAN",
                   details::Create::_param_VLAN
                        .data(),
                   details::Create::_return_VLAN
                        .data(),
                   _callback_VLAN),
    vtable::end()
};

} // namespace server
} // namespace VLAN
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

