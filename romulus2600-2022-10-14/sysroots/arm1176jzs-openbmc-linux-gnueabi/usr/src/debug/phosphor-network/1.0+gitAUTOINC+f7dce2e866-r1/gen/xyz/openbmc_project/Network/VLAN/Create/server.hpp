#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace VLAN
{
namespace server
{

class Create
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        Create() = delete;
        Create(const Create&) = delete;
        Create& operator=(const Create&) = delete;
        Create(Create&&) = delete;
        Create& operator=(Create&&) = delete;
        virtual ~Create() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        Create(bus_t& bus, const char* path);



        /** @brief Implementation for VLAN
         *  Create VLANInterface Object.
         *
         *  @param[in] interfaceName - Name of the interface.
         *  @param[in] id - VLAN Identifier.
         *
         *  @return path[sdbusplus::message::object_path] - The path for the created VLAN object.
         */
        virtual sdbusplus::message::object_path vlan(
            std::string interfaceName,
            uint32_t id) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Network_VLAN_Create_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Network_VLAN_Create_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Network.VLAN.Create";

    private:

        /** @brief sd-bus callback for VLAN
         */
        static int _callback_VLAN(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Network_VLAN_Create_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace VLAN
} // namespace Network
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

