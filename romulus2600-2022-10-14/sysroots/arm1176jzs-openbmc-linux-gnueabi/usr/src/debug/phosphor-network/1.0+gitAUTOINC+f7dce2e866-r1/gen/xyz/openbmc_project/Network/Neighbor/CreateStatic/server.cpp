#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/Neighbor/CreateStatic/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Neighbor
{
namespace server
{

CreateStatic::CreateStatic(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_Neighbor_CreateStatic_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int CreateStatic::_callback_Neighbor(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<CreateStatic*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](std::string&& ipAddress, std::string&& macAddress)
                    {
                        return o->neighbor(
                                ipAddress, macAddress);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace CreateStatic
{
static const auto _param_Neighbor =
        utility::tuple_to_array(message::types::type_id<
                std::string, std::string>());
static const auto _return_Neighbor =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t CreateStatic::_vtable[] = {
    vtable::start(),

    vtable::method("Neighbor",
                   details::CreateStatic::_param_Neighbor
                        .data(),
                   details::CreateStatic::_return_Neighbor
                        .data(),
                   _callback_Neighbor),
    vtable::end()
};

} // namespace server
} // namespace Neighbor
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

