#pragma once
#include <limits>
#include <map>
#include <optional>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/server.hpp>
#include <sdbusplus/utility/dedup_variant.hpp>
#include <string>
#include <systemd/sd-bus.h>
#include <tuple>


#ifndef SDBUSPP_NEW_CAMELCASE
#define SDBUSPP_NEW_CAMELCASE 1
#endif

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace Neighbor
{
namespace server
{

class CreateStatic
{
    public:
        /* Define all of the basic class operations:
         *     Not allowed:
         *         - Default constructor to avoid nullptrs.
         *         - Copy operations due to internal unique_ptr.
         *         - Move operations due to 'this' being registered as the
         *           'context' with sdbus.
         *     Allowed:
         *         - Destructor.
         */
        CreateStatic() = delete;
        CreateStatic(const CreateStatic&) = delete;
        CreateStatic& operator=(const CreateStatic&) = delete;
        CreateStatic(CreateStatic&&) = delete;
        CreateStatic& operator=(CreateStatic&&) = delete;
        virtual ~CreateStatic() = default;

        /** @brief Constructor to put object onto bus at a dbus path.
         *  @param[in] bus - Bus to attach to.
         *  @param[in] path - Path to attach at.
         */
        CreateStatic(bus_t& bus, const char* path);



        /** @brief Implementation for Neighbor
         *  Create a static neighbor entry.
         *
         *  @param[in] ipAddress - IP Address.
         *  @param[in] macAddress - MAC Address.
         *
         *  @return path[sdbusplus::message::object_path] - The path for the created neighbor object.
         */
        virtual sdbusplus::message::object_path neighbor(
            std::string ipAddress,
            std::string macAddress) = 0;




        /** @brief Emit interface added */
        void emit_added()
        {
            _xyz_openbmc_project_Network_Neighbor_CreateStatic_interface.emit_added();
        }

        /** @brief Emit interface removed */
        void emit_removed()
        {
            _xyz_openbmc_project_Network_Neighbor_CreateStatic_interface.emit_removed();
        }

        static constexpr auto interface = "xyz.openbmc_project.Network.Neighbor.CreateStatic";

    private:

        /** @brief sd-bus callback for Neighbor
         */
        static int _callback_Neighbor(
            sd_bus_message*, void*, sd_bus_error*);


        static const vtable_t _vtable[];
        sdbusplus::server::interface_t
                _xyz_openbmc_project_Network_Neighbor_CreateStatic_interface;
        sdbusplus::SdBusInterface *_intf;


};


} // namespace server
} // namespace Neighbor
} // namespace Network
} // namespace openbmc_project
} // namespace xyz

namespace message::details
{
} // namespace message::details
} // namespace sdbusplus

