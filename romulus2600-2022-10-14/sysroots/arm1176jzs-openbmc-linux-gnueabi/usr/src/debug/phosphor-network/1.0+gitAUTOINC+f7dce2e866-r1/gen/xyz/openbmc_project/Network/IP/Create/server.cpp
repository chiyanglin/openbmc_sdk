#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Network/IP/Create/server.hpp>

#include <xyz/openbmc_project/Common/error.hpp>

namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Network
{
namespace IP
{
namespace server
{

Create::Create(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Network_IP_Create_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}


int Create::_callback_IP(
        sd_bus_message* msg, void* context, sd_bus_error* error)
{
    auto o = static_cast<Create*>(context);

    try
    {
        return sdbusplus::sdbuspp::method_callback(
                msg, o->_intf, error,
                std::function(
                    [=](xyz::openbmc_project::Network::server::IP::Protocol&& protocolType, std::string&& address, uint8_t&& prefixLength, std::string&& gateway)
                    {
                        return o->ip(
                                protocolType, address, prefixLength, gateway);
                    }
                ));
    }
    catch(const sdbusplus::xyz::openbmc_project::Common::Error::InvalidArgument& e)
    {
        return o->_intf->sd_bus_error_set(error, e.name(), e.description());
    }

    return 0;
}

namespace details
{
namespace Create
{
static const auto _param_IP =
        utility::tuple_to_array(message::types::type_id<
                xyz::openbmc_project::Network::server::IP::Protocol, std::string, uint8_t, std::string>());
static const auto _return_IP =
        utility::tuple_to_array(message::types::type_id<
                sdbusplus::message::object_path>());
}
}




const vtable_t Create::_vtable[] = {
    vtable::start(),

    vtable::method("IP",
                   details::Create::_param_IP
                        .data(),
                   details::Create::_return_IP
                        .data(),
                   _callback_IP),
    vtable::end()
};

} // namespace server
} // namespace IP
} // namespace Network
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

