// !!! WARNING: This is a GENERATED Code..Please do NOT Edit !!!

#include "sensordatahandler.hpp"

#include <ipmid/types.hpp>

namespace ipmi {
namespace sensor {

extern const IdInfoMap __attribute__((init_priority(101))) sensors = {
{2,{

        .entityType = 34,
        .instance = 1,
        .sensorType = 15,
        .sensorPath = "/xyz/openbmc_project/state/host0",
        .sensorInterface = "xyz.openbmc_project.State.Boot.Progress",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = set::eventdata2,
        .getFunc = get::eventdata2,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameProperty,
        .propertyInterfaces = {
            {"xyz.openbmc_project.State.Boot.Progress",{
                    {"BootProgress",{
                    {
                    },
                    {
                        { 0,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.Boot.Progress.ProgressStages.Unspecified"),
                            }
                        },
                        { 1,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.Boot.Progress.ProgressStages.MemoryInit"),
                            }
                        },
                        { 3,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.Boot.Progress.ProgressStages.SecondaryProcInit"),
                            }
                        },
                        { 7,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.Boot.Progress.ProgressStages.PCIInit"),
                            }
                        },
                        { 19,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.Boot.Progress.ProgressStages.OSStart"),
                            }
                        },
                        { 20,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.Boot.Progress.ProgressStages.MotherboardInit"),
                            }
                        },
                    }}},
            }},
     },
}},
{3,{

        .entityType = 210,
        .instance = 1,
        .sensorType = 7,
        .sensorPath = "/org/open_power/control/occ0",
        .sensorInterface = "org.open_power.OCC.Status",
        .sensorReadingType = 9,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = set::assertion,
        .getFunc = get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"org.open_power.OCC.Status",{
                    {"OccActive",{
                    {
                    },
                    {
                        { 0,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                        { 1,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
     },
}},
{4,{

        .entityType = 210,
        .instance = 2,
        .sensorType = 7,
        .sensorPath = "/org/open_power/control/occ1",
        .sensorInterface = "org.open_power.OCC.Status",
        .sensorReadingType = 9,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = set::assertion,
        .getFunc = get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"org.open_power.OCC.Status",{
                    {"OccActive",{
                    {
                    },
                    {
                        { 0,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                        { 1,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
     },
}},
{8,{

        .entityType = 3,
        .instance = 1,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{9,{

        .entityType = 3,
        .instance = 2,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{11,{

        .entityType = 32,
        .instance = 1,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm0",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{12,{

        .entityType = 32,
        .instance = 2,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm1",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{13,{

        .entityType = 32,
        .instance = 3,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm2",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{14,{

        .entityType = 32,
        .instance = 4,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm3",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{15,{

        .entityType = 32,
        .instance = 5,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm4",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{16,{

        .entityType = 32,
        .instance = 6,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm5",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{17,{

        .entityType = 32,
        .instance = 7,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm6",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{18,{

        .entityType = 32,
        .instance = 8,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm7",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{19,{

        .entityType = 32,
        .instance = 9,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm8",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{20,{

        .entityType = 32,
        .instance = 10,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm9",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{21,{

        .entityType = 32,
        .instance = 11,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm10",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{22,{

        .entityType = 32,
        .instance = 12,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm11",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{23,{

        .entityType = 32,
        .instance = 13,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm12",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{24,{

        .entityType = 32,
        .instance = 14,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm13",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{25,{

        .entityType = 32,
        .instance = 15,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm14",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{26,{

        .entityType = 32,
        .instance = 16,
        .sensorType = 12,
        .sensorPath = "/system/chassis/motherboard/dimm15",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 6,{
                                SkipAssertion::NONE,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 6,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 4,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{27,{

        .entityType = 32,
        .instance = 17,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm0_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{28,{

        .entityType = 32,
        .instance = 18,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm1_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{29,{

        .entityType = 32,
        .instance = 25,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm2_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{30,{

        .entityType = 32,
        .instance = 26,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm3_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{31,{

        .entityType = 32,
        .instance = 27,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm4_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{32,{

        .entityType = 32,
        .instance = 28,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm5_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{33,{

        .entityType = 32,
        .instance = 29,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm6_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{34,{

        .entityType = 32,
        .instance = 30,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm7_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{35,{

        .entityType = 32,
        .instance = 31,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm8_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{36,{

        .entityType = 32,
        .instance = 32,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm9_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{37,{

        .entityType = 32,
        .instance = 19,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm10_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{38,{

        .entityType = 32,
        .instance = 20,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm11_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{39,{

        .entityType = 32,
        .instance = 21,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm12_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{40,{

        .entityType = 32,
        .instance = 22,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm13_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{41,{

        .entityType = 32,
        .instance = 23,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm14_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{42,{

        .entityType = 32,
        .instance = 24,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/dimm15_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{43,{

        .entityType = 208,
        .instance = 1,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core0",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{44,{

        .entityType = 208,
        .instance = 2,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core1",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{45,{

        .entityType = 208,
        .instance = 3,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core2",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{46,{

        .entityType = 208,
        .instance = 4,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core3",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{47,{

        .entityType = 208,
        .instance = 5,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core4",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{48,{

        .entityType = 208,
        .instance = 6,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core5",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{49,{

        .entityType = 208,
        .instance = 7,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core6",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{50,{

        .entityType = 208,
        .instance = 8,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core7",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{51,{

        .entityType = 208,
        .instance = 9,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core8",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{52,{

        .entityType = 208,
        .instance = 10,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core9",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{53,{

        .entityType = 208,
        .instance = 11,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core10",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{54,{

        .entityType = 208,
        .instance = 12,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core11",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{55,{

        .entityType = 208,
        .instance = 13,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core12",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{56,{

        .entityType = 208,
        .instance = 14,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core13",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{57,{

        .entityType = 208,
        .instance = 15,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core14",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{58,{

        .entityType = 208,
        .instance = 16,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core15",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{59,{

        .entityType = 208,
        .instance = 17,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core16",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{60,{

        .entityType = 208,
        .instance = 18,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core17",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{61,{

        .entityType = 208,
        .instance = 19,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core18",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{62,{

        .entityType = 208,
        .instance = 20,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core19",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{63,{

        .entityType = 208,
        .instance = 21,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core20",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{64,{

        .entityType = 208,
        .instance = 22,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core21",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{65,{

        .entityType = 208,
        .instance = 23,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core22",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{66,{

        .entityType = 208,
        .instance = 24,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu0/core23",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{67,{

        .entityType = 208,
        .instance = 25,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core0",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{68,{

        .entityType = 208,
        .instance = 26,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core1",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{69,{

        .entityType = 208,
        .instance = 27,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core2",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{70,{

        .entityType = 208,
        .instance = 28,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core3",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{71,{

        .entityType = 208,
        .instance = 29,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core4",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{72,{

        .entityType = 208,
        .instance = 30,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core5",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{73,{

        .entityType = 208,
        .instance = 31,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core6",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{74,{

        .entityType = 208,
        .instance = 32,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core7",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{75,{

        .entityType = 208,
        .instance = 33,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core8",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{76,{

        .entityType = 208,
        .instance = 34,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core9",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{77,{

        .entityType = 208,
        .instance = 35,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core10",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{78,{

        .entityType = 208,
        .instance = 36,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core11",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{79,{

        .entityType = 208,
        .instance = 37,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core12",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{80,{

        .entityType = 208,
        .instance = 38,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core13",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{81,{

        .entityType = 208,
        .instance = 39,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core14",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{82,{

        .entityType = 208,
        .instance = 40,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core15",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{83,{

        .entityType = 208,
        .instance = 41,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core16",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{84,{

        .entityType = 208,
        .instance = 42,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core17",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{85,{

        .entityType = 208,
        .instance = 43,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core18",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{86,{

        .entityType = 208,
        .instance = 44,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core19",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{87,{

        .entityType = 208,
        .instance = 45,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core20",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{88,{

        .entityType = 208,
        .instance = 46,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core21",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{89,{

        .entityType = 208,
        .instance = 47,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core22",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{90,{

        .entityType = 208,
        .instance = 48,
        .sensorType = 7,
        .sensorPath = "/system/chassis/motherboard/cpu1/core23",
        .sensorInterface = "xyz.openbmc_project.Inventory.Manager",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = notify::assertion,
        .getFunc = inventory::get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameParentLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Inventory.Item",{
                    {"Present",{
                    {
                    },
                    {
                        { 7,{
                                SkipAssertion::DEASSERT,
                            true,
                            false,
                            }
                        },
                    }}},
            }},
            {"xyz.openbmc_project.State.Decorator.OperationalStatus",{
                    {"Functional",{
                    {
                        { 7,{
                                true,
                                false,
                            }
                        },
                    },
                    {
                        { 8,{
                                SkipAssertion::NONE,
                            false,
                            true,
                            }
                        },
                    }}},
            }},
     },
}},
{91,{

        .entityType = 208,
        .instance = 49,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core0_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{92,{

        .entityType = 208,
        .instance = 50,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core1_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{93,{

        .entityType = 208,
        .instance = 51,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core2_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{94,{

        .entityType = 208,
        .instance = 52,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core3_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{95,{

        .entityType = 208,
        .instance = 53,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core4_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{96,{

        .entityType = 208,
        .instance = 54,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core5_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{97,{

        .entityType = 208,
        .instance = 55,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core6_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{98,{

        .entityType = 208,
        .instance = 56,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core7_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{99,{

        .entityType = 208,
        .instance = 57,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core8_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{100,{

        .entityType = 208,
        .instance = 58,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core9_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{101,{

        .entityType = 208,
        .instance = 59,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core10_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{102,{

        .entityType = 208,
        .instance = 60,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core11_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{103,{

        .entityType = 208,
        .instance = 61,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core12_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{104,{

        .entityType = 208,
        .instance = 62,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core13_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{105,{

        .entityType = 208,
        .instance = 63,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core14_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{106,{

        .entityType = 208,
        .instance = 64,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core15_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{107,{

        .entityType = 208,
        .instance = 65,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core16_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{108,{

        .entityType = 208,
        .instance = 66,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core17_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{109,{

        .entityType = 208,
        .instance = 67,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core18_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{110,{

        .entityType = 208,
        .instance = 68,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core19_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{111,{

        .entityType = 208,
        .instance = 69,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core20_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{112,{

        .entityType = 208,
        .instance = 70,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core21_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{113,{

        .entityType = 208,
        .instance = 71,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core22_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{114,{

        .entityType = 208,
        .instance = 72,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p0_core23_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{115,{

        .entityType = 208,
        .instance = 73,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core0_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{116,{

        .entityType = 208,
        .instance = 74,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core1_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{117,{

        .entityType = 208,
        .instance = 75,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core2_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{118,{

        .entityType = 208,
        .instance = 76,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core3_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{119,{

        .entityType = 208,
        .instance = 77,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core4_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{120,{

        .entityType = 208,
        .instance = 78,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core5_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{121,{

        .entityType = 208,
        .instance = 79,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core6_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{122,{

        .entityType = 208,
        .instance = 80,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core7_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{123,{

        .entityType = 208,
        .instance = 81,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core8_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{124,{

        .entityType = 208,
        .instance = 82,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core9_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{125,{

        .entityType = 208,
        .instance = 83,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core10_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{126,{

        .entityType = 208,
        .instance = 84,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core11_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{127,{

        .entityType = 208,
        .instance = 85,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core12_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{128,{

        .entityType = 208,
        .instance = 86,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core13_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{129,{

        .entityType = 208,
        .instance = 87,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core14_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{130,{

        .entityType = 208,
        .instance = 88,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core15_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{131,{

        .entityType = 208,
        .instance = 89,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core16_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{132,{

        .entityType = 208,
        .instance = 90,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core17_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{133,{

        .entityType = 208,
        .instance = 91,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core18_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{134,{

        .entityType = 208,
        .instance = 92,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core19_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{135,{

        .entityType = 208,
        .instance = 93,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core20_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{136,{

        .entityType = 208,
        .instance = 94,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core21_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{137,{

        .entityType = 208,
        .instance = 95,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core22_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{138,{

        .entityType = 208,
        .instance = 96,
        .sensorType = 1,
        .sensorPath = "/xyz/openbmc_project/sensors/temperature/p1_core23_temp",
        .sensorInterface = "xyz.openbmc_project.Sensor.Value",
        .sensorReadingType = 1,
        .coefficientM = 1,
        .coefficientB = -127,
        .exponentB = 0,
        .scaledOffset = -127,
        .exponentR = 0,
        .hasScale = true,
        .scale = -3,
        .sensorUnits1 = 0,
        .unit = "xyz.openbmc_project.Sensor.Value.Unit.DegreesC",
        .updateFunc = set::readingData<double>,
        .getFunc = get::readingData<double>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameLeaf,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Sensor.Value",{
                    {"Value",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{139,{

        .entityType = 34,
        .instance = 2,
        .sensorType = 195,
        .sensorPath = "/xyz/openbmc_project/state/host0",
        .sensorInterface = "xyz.openbmc_project.Control.Boot.RebootAttempts",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = set::readingAssertion<uint32_t>,
        .getFunc = get::readingAssertion<uint32_t>,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameProperty,
        .propertyInterfaces = {
            {"xyz.openbmc_project.Control.Boot.RebootAttempts",{
                    {"AttemptsLeft",{
                    {
                    },
                    {
                        { 255,{
                                }},
                    }}},
            }},
     },
}},
{145,{

        .entityType = 35,
        .instance = 1,
        .sensorType = 31,
        .sensorPath = "/xyz/openbmc_project/state/host0",
        .sensorInterface = "xyz.openbmc_project.State.OperatingSystem.Status",
        .sensorReadingType = 111,
        .coefficientM = 1,
        .coefficientB = 0,
        .exponentB = 0,
        .scaledOffset = 0,
        .exponentR = 0,
        .hasScale = false,
        .scale = 0,
        .sensorUnits1 = 0,
        .unit = "",
        .updateFunc = set::assertion,
        .getFunc = get::assertion,
        .mutability = Mutability(Mutability::Write|Mutability::Read),
        .sensorName = "",
        .sensorNameFunc = get::nameProperty,
        .propertyInterfaces = {
            {"xyz.openbmc_project.State.OperatingSystem.Status",{
                    {"OperatingSystemState",{
                    {
                    },
                    {
                        { 1,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.CBoot"),
                            }
                        },
                        { 2,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.PXEBoot"),
                            }
                        },
                        { 3,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.DiagBoot"),
                            }
                        },
                        { 4,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.CDROMBoot"),
                            }
                        },
                        { 5,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.ROMBoot"),
                            }
                        },
                        { 6,{
                                SkipAssertion::NONE,
                            std::string("xyz.openbmc_project.State.OperatingSystem.Status.OSStatus.BootComplete"),
                            }
                        },
                    }}},
            }},
     },
}},
};

} // namespace sensor
} // namespace ipmi
