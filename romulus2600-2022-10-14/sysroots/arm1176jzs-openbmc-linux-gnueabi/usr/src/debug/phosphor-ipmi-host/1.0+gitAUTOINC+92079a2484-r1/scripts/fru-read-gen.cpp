// !!! WARNING: This is a GENERATED Code..Please do NOT Edit !!!
#include <iostream>
#include "fruread.hpp"

extern const FruMap __attribute__((init_priority(101))) frus = {
   {1,{


         {3, 1, "/system/chassis/motherboard/cpu0",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Board",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Board",
                        "Manufacturer",
                     ""
                 }},
                    {"PartNumber",{
                        "Board",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Board",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "Board",
                        "Custom Field 2",
                     "\x3a"
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Board",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {2,{


         {3, 2, "/system/chassis/motherboard/cpu1",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Board",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Board",
                        "Manufacturer",
                     ""
                 }},
                    {"PartNumber",{
                        "Board",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Board",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "Board",
                        "Custom Field 2",
                     "\x3a"
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Board",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {3,{


         {7, 1, "/system",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"Model",{
                        "Chassis",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Chassis",
                        "Serial Number",
                     ""
                 }},
             }},
        }},

         {7, 1, "/system/chassis/motherboard",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Board",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Board",
                        "Manufacturer",
                     ""
                 }},
                    {"PartNumber",{
                        "Board",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Board",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Board",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {4,{


         {32, 1, "/system/chassis/motherboard/dimm0",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {5,{


         {32, 2, "/system/chassis/motherboard/dimm1",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {6,{


         {32, 3, "/system/chassis/motherboard/dimm2",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {7,{


         {32, 4, "/system/chassis/motherboard/dimm3",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {8,{


         {32, 5, "/system/chassis/motherboard/dimm4",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {9,{


         {32, 6, "/system/chassis/motherboard/dimm5",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {10,{


         {32, 7, "/system/chassis/motherboard/dimm6",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {11,{


         {32, 8, "/system/chassis/motherboard/dimm7",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {12,{


         {32, 9, "/system/chassis/motherboard/dimm8",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {13,{


         {32, 10, "/system/chassis/motherboard/dimm9",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {14,{


         {32, 11, "/system/chassis/motherboard/dimm10",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {15,{


         {32, 12, "/system/chassis/motherboard/dimm11",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {16,{


         {32, 13, "/system/chassis/motherboard/dimm12",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {17,{


         {32, 14, "/system/chassis/motherboard/dimm13",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {18,{


         {32, 15, "/system/chassis/motherboard/dimm14",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {19,{


         {32, 16, "/system/chassis/motherboard/dimm15",{
             {"xyz.openbmc_project.Inventory.Decorator.Asset",{
                    {"BuildDate",{
                        "Product",
                        "Mfg Date",
                     ""
                 }},
                    {"Manufacturer",{
                        "Product",
                        "Manufacturer",
                     ""
                 }},
                    {"Model",{
                        "Product",
                        "Model Number",
                     ""
                 }},
                    {"PartNumber",{
                        "Product",
                        "Part Number",
                     ""
                 }},
                    {"SerialNumber",{
                        "Product",
                        "Serial Number",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Decorator.Revision",{
                    {"Version",{
                        "",
                        "Version",
                     ""
                 }},
             }},
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Name",
                     ""
                 }},
             }},
        }},
   }},
   {50,{


         {29, 1, "/system/chassis/motherboard/fan0",{
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Product Name",
                     ""
                 }},
             }},
        }},
   }},
   {51,{


         {29, 2, "/system/chassis/motherboard/fan1",{
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Product Name",
                     ""
                 }},
             }},
        }},
   }},
   {52,{


         {29, 3, "/system/chassis/motherboard/fan2",{
             {"xyz.openbmc_project.Inventory.Item",{
                    {"PrettyName",{
                        "Product",
                        "Product Name",
                     ""
                 }},
             }},
        }},
   }},
};
