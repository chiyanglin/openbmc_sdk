#include <algorithm>
#include <map>
#include <sdbusplus/exception.hpp>
#include <sdbusplus/sdbus.hpp>
#include <sdbusplus/sdbuspp_support/server.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <tuple>

#include <xyz/openbmc_project/Ipmi/Internal/SoftPowerOff/server.hpp>


namespace sdbusplus
{
namespace xyz
{
namespace openbmc_project
{
namespace Ipmi
{
namespace Internal
{
namespace server
{

SoftPowerOff::SoftPowerOff(bus_t& bus, const char* path)
        : _xyz_openbmc_project_Ipmi_Internal_SoftPowerOff_interface(
                bus, path, interface, _vtable, this), _intf(bus.getInterface())
{
}

SoftPowerOff::SoftPowerOff(bus_t& bus, const char* path,
                           const std::map<std::string, PropertiesVariant>& vals,
                           bool skipSignal)
        : SoftPowerOff(bus, path)
{
    for (const auto& v : vals)
    {
        setPropertyByName(v.first, v.second, skipSignal);
    }
}




auto SoftPowerOff::responseReceived() const ->
        HostResponse
{
    return _responseReceived;
}

int SoftPowerOff::_callback_get_ResponseReceived(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* reply, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftPowerOff*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                reply, o->_intf, error,
                std::function(
                    [=]()
                    {
                        return o->responseReceived();
                    }
                ));
    }
}

auto SoftPowerOff::responseReceived(HostResponse value,
                                         bool skipSignal) ->
        HostResponse
{
    if (_responseReceived != value)
    {
        _responseReceived = value;
        if (!skipSignal)
        {
            _xyz_openbmc_project_Ipmi_Internal_SoftPowerOff_interface.property_changed("ResponseReceived");
        }
    }

    return _responseReceived;
}

auto SoftPowerOff::responseReceived(HostResponse val) ->
        HostResponse
{
    return responseReceived(val, false);
}

int SoftPowerOff::_callback_set_ResponseReceived(
        sd_bus* /*bus*/, const char* /*path*/, const char* /*interface*/,
        const char* /*property*/, sd_bus_message* value, void* context,
        sd_bus_error* error)
{
    auto o = static_cast<SoftPowerOff*>(context);

    {
        return sdbusplus::sdbuspp::property_callback(
                value, o->_intf, error,
                std::function(
                    [=](HostResponse&& arg)
                    {
                        o->responseReceived(std::move(arg));
                    }
                ));
    }

    return true;
}

namespace details
{
namespace SoftPowerOff
{
static const auto _property_ResponseReceived =
    utility::tuple_to_array(message::types::type_id<
            sdbusplus::xyz::openbmc_project::Ipmi::Internal::server::SoftPowerOff::HostResponse>());
}
}

void SoftPowerOff::setPropertyByName(const std::string& _name,
                                     const PropertiesVariant& val,
                                     bool skipSignal)
{
    if (_name == "ResponseReceived")
    {
        auto& v = std::get<HostResponse>(val);
        responseReceived(v, skipSignal);
        return;
    }
}

auto SoftPowerOff::getPropertyByName(const std::string& _name) ->
        PropertiesVariant
{
    if (_name == "ResponseReceived")
    {
        return responseReceived();
    }

    return PropertiesVariant();
}


namespace
{
/** String to enum mapping for SoftPowerOff::HostResponse */
static const std::tuple<const char*, SoftPowerOff::HostResponse> mappingSoftPowerOffHostResponse[] =
        {
            std::make_tuple( "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff.HostResponse.NotApplicable",                 SoftPowerOff::HostResponse::NotApplicable ),
            std::make_tuple( "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff.HostResponse.SoftOffReceived",                 SoftPowerOff::HostResponse::SoftOffReceived ),
            std::make_tuple( "xyz.openbmc_project.Ipmi.Internal.SoftPowerOff.HostResponse.HostShutdown",                 SoftPowerOff::HostResponse::HostShutdown ),
        };

} // anonymous namespace

auto SoftPowerOff::convertStringToHostResponse(const std::string& s) noexcept ->
        std::optional<HostResponse>
{
    auto i = std::find_if(
            std::begin(mappingSoftPowerOffHostResponse),
            std::end(mappingSoftPowerOffHostResponse),
            [&s](auto& e){ return 0 == strcmp(s.c_str(), std::get<0>(e)); } );
    if (std::end(mappingSoftPowerOffHostResponse) == i)
    {
        return std::nullopt;
    }
    else
    {
        return std::get<1>(*i);
    }
}

auto SoftPowerOff::convertHostResponseFromString(const std::string& s) ->
        HostResponse
{
    auto r = convertStringToHostResponse(s);

    if (!r)
    {
        throw sdbusplus::exception::InvalidEnumString();
    }
    else
    {
        return *r;
    }
}

std::string SoftPowerOff::convertHostResponseToString(SoftPowerOff::HostResponse v)
{
    auto i = std::find_if(
            std::begin(mappingSoftPowerOffHostResponse),
            std::end(mappingSoftPowerOffHostResponse),
            [v](auto& e){ return v == std::get<1>(e); });
    if (i == std::end(mappingSoftPowerOffHostResponse))
    {
        throw std::invalid_argument(std::to_string(static_cast<int>(v)));
    }
    return std::get<0>(*i);
}

const vtable_t SoftPowerOff::_vtable[] = {
    vtable::start(),
    vtable::property("ResponseReceived",
                     details::SoftPowerOff::_property_ResponseReceived
                        .data(),
                     _callback_get_ResponseReceived,
                     _callback_set_ResponseReceived,
                     vtable::property_::emits_change),
    vtable::end()
};

} // namespace server
} // namespace Internal
} // namespace Ipmi
} // namespace openbmc_project
} // namespace xyz
} // namespace sdbusplus

